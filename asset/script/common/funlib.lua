--- If a then x else y
function e.iif(a, x, y)
    if a then return x else return y end
end 
--- If t then t[k] else nil
function e.get(t, k)
    if t then return t[k] else return nil end
end
--- Get sign by index, i.e. (i + b) % 2 * 2 - 1
function e.index2sign(i, b)
    return (i + b) % 2 * 2 - 1
end
--- Round
function e.round(x)
    return math.floor(x + 0.5)
end
--- Array concatenation
function e.array_concat(...)
    local t = {}
    for n = 1, select('#', ...) do
        local arg = select(n, ...)
        if type(arg) == 'table' then
            for _, v in ipairs(arg) do
                t[#t + 1] = v
            end
        else
            t[#t + 1] = arg
        end
    end
    return t
end
--- Vector add (A + B)
function e.add(a, b)
    assert(a and b)
    local r = {}
    for i = 1, math.min(#a, #b) do
        r[i] = a[i] + b[i]
    end
    return r
end
--- Vector multiply and add (A + B * C)
function e.mad(a, b, c)
    assert(a and b and c)
    local r = {}
    if type(c) == 'table' then
        for i = 1, math.min(#a, #b, #c) do
            r[i] = a[i] + b[i] * c[i]
        end
    else 
        for i = 1, math.min(#a, #b) do
            r[i] = a[i] + b[i] * c
        end
    end
    return r
end
--- Linear interpolation A * (1 - X) + B * X
function e.lerp(a, b, x)
    assert(a and b and x)
    return a + (b - a) * x
end
--- Cross product
function e.cross(lhs, rhs)
    return 
    {
        lhs[2] * rhs[3] - lhs[3] * rhs[2],
        lhs[3] * rhs[1] - lhs[1] * rhs[3],
        lhs[1] * rhs[2] - lhs[2] * rhs[1]
    }
end
--- Get vector length
function e.length(v)
    local l = 0
    for i = 1, #v do
        l = l + v[i]^2
    end
    return math.sqrt(l)
end
--- Normalize vector
function e.normalize(v)
    local l = e.length(v)
    local r = {}
    for i = 1, #v do
        r[i] = v[i] / l
    end
    return r
end
--- Square interpolation from A to B with curvature Q
function e.qlerp(t, q, a, b)
    assert(t and q and a and b)
    return q * t * t + (b - a - q) * t + a
end
--- Compare tables
function e.compare_tables(t1, t2)
    local num = 0
    for i = 1, #t1 do
        if t1[i] == t2[i] then
            num = num + 1
        end
    end
    return #t1 - num
end
--- Print table
function e.print_table(tbl, indent)
    if not indent then indent = 0 end
    for k, v in pairs(tbl) do
        local formatting = string.rep("  ", indent) .. k .. ": "
        if type(v) == "table" then
            e.info(formatting)
            e.print_table(v, indent + 1)
        elseif type(v) == 'boolean' then
            e.info(formatting .. tostring(v))      
        else
            e.info(formatting .. v)
        end
    end
end
