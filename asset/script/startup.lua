-- Starting
e.info('Starting...')

-- User code
e.info('User code executing...')

e.importModule('architect/common/script/generator_helpers')
e.disableTextureCompression()
e.generateFolder('architect/nature/plants/grass')
e.generateFolder('architect/nature/plants/birch_tree')
e.generateFolder('architect/worlds')
