#constant Const
    Float4 vQuadPosition;
    Float4 vTexturePosition;
    Float4 vWorldPosition;
    Int4 vTextureSize01;
#resource texture2D(Float) Pattern
#resource texture2D(Float) HeightMap
#resource texture2D(Float4) NormalMap
#resource texture2D(Float4) Texture0
#resource texture2D(Float4) Texture1
#layout Empty
#output Position
    Float4 Position;
    Float2 TexturePosition;
    Float2 WorldPosition;
#vertex vertex(layout = Empty, output = Position)
#   include "Auto.Quad"
#main
    float2 vPosition = GetQuadVertex(vQuadPosition.xy, vQuadPosition.zw, inVertexID);
    outPosition = float4(textureToScreen(vPosition), 0.5, 1.0);
    outTexturePosition = GetQuadVertex(vTexturePosition.xy, vTexturePosition.zw, inVertexID);
    outWorldPosition = GetQuadVertex(vWorldPosition.xy, vWorldPosition.zw, inVertexID);
    
#target Multimap
    Height;
    Normal;

#region FunBicubic
    int2 WrapTexCoord(in int2 st, in int2 pad, in int2 size)
    {
        int2 ij = (st + pad) % size;
        return ij + step(int2(1, 1), -ij) * size;
    }
    float4 FilterBicubic(in float2 ij, in int2 size, in int lod, inout Texture2D tex)
    {
        int2 xy = int2(floor(ij));
    #define GET(I, J) fetchTextureLod(tex, WrapTexCoord(xy, int2(I, J), size), lod)
        float2 normxy = ij - xy;
        float2 st0 = ((2.0 - normxy) * normxy - 1.0) * normxy;
        float2 st1 = (3.0 * normxy - 5.0) * normxy * normxy + 2.0;
        float2 st2 = ((4.0 - 3.0 * normxy) * normxy + 1.0) * normxy;
        float2 st3 = (normxy - 1.0) * normxy * normxy;

        float4 row0 = st0.x * GET(-1, -1)+ st1.x * GET(0, -1)+ st2.x * GET(1, -1)+ st3.x * GET(2, -1);
        float4 row1 = st0.x * GET(-1, 0) + st1.x * GET(0, 0) + st2.x * GET(1, 0) + st3.x * GET(2, 0);
        float4 row2 = st0.x * GET(-1, 1) + st1.x * GET(0, 1) + st2.x * GET(1, 1) + st3.x * GET(2, 1);
        float4 row3 = st0.x * GET(-1, 2) + st1.x * GET(0, 2) + st2.x * GET(1, 2) + st3.x * GET(2, 2);

        return 0.25 * (st0.y*row0 + st1.y*row1 + st2.y*row2 + st3.y*row3);	
    #undef GET
    }
#endregion

#pixel pixelFillHeight(input = Position, target = Multimap)
#main
    outHeight = 1.0;
    outNormal = float4(0, 1, 0, 0);

#pixel pixelPaintHeight(input = Position, target = Multimap)
#   include "FunBicubic"
#main
    float  blendFactor = smoothstep(0.0, 1.0, sampleTexture(Pattern, inTexturePosition).x);
    
    float  srcHeight = sampleTexture(HeightMap, inTexturePosition).x;
    float3 srcNormal = sampleTexture(NormalMap, inTexturePosition).xyz;
    
    float  destHeight = FilterBicubic(inWorldPosition / 32.0, vTextureSize01.xy, 0, Texture0).x * 2.0;
    
    outHeight.x = lerp(srcHeight, destHeight, blendFactor);
    outNormal.xyz = srcNormal;

#program FillHeight(vertex = vertex, pixel = pixelFillHeight)
#program PaintHeight(vertex = vertex, pixel = pixelPaintHeight)
