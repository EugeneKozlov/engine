#resource texture2D(Float4) Texture
#layout Empty
#output Position
    Float4 Position;
#vertex vertex(layout = Empty, output = Position)
#   include "Auto.Quad"
#main
    float2 vTexturePos = GetQuadVertex(-1.0, 1.0, inVertexID);
    outPosition = float4(vTexturePos, 0.5, 1.0);

#target Value
    Value;

#pixel pixel(input = Position, target = Value)
#main
    outValue = sampleTextureLod(Texture, float2(0.5, 0.5), 666);

#program Bake(vertex = vertex, pixel = pixel)
