local detail = {}
function detail.generateWorld(gen)
    local size = 1024
    gen:setSize(size, size)

    -- Materials
    local materials = {}
    materials['grass'] = gen:addMaterial('architect/nature/plants/grass/bluegrass_terrain_diffuse')
    local function makeLayer(mat1, mat2)
        return {materials[mat1], materials[mat2]}
    end

    -- Ocean
    --gen:defaultHeight(-20.0)
    gen:defaultHeight(0.0)

    -- Ground
    if false then
        local groundPattern = gen:addConvexPattern(50.0, { {-800, -800}, {-800, 800}, {800, 800}, {800, -800} })
        local groundGeometry = gen:addGapiTerrainBrush(groundPattern, 'architect/generator/world/brushes_effect', 'PaintHeight')
        groundGeometry:setTexture(0, gen:noiseTexture())
        local groundColor = gen:addLayerBrush(groundPattern, makeLayer('grass', 'grass'), {0, 1})
    end

    -- Forest
    local objectGenerator = gen:addObjectGenerator()
    if false then
        objectGenerator:addRectangle(-40, -404, 404, 404, 30.0, 13.0, 'architect/nature/plants/birch_tree/f29/model')
        objectGenerator:addRectangle(-40, -404, 404, 404, 20.0, 10.0, 'architect/nature/plants/birch_tree/f19/model')
        objectGenerator:addRectangle(-40, -404, 404, 404, 15.0, 8.0, 'architect/nature/plants/birch_tree/f11/model')
        objectGenerator:addRectangle(-40, -404, 404, 404, 6.0,  4.0, 'architect/nature/plants/birch_tree/f5/model')
    else
        local size = 500
        local num = 100
        for i = 1, num do
            local x = math.random(-size, size)
            local y = math.random(-size, size)
            local radius = math.random(50, 100)
            objectGenerator:addCircle(x, y, radius, 30.0, 13.0, 'architect/nature/plants/birch_tree/f29/model')
            objectGenerator:addCircle(x, y, radius + 10, 20.0, 10.0, 'architect/nature/plants/birch_tree/f19/model')
            objectGenerator:addCircle(x, y, radius + 20, 15.0, 8.0, 'architect/nature/plants/birch_tree/f11/model')
            objectGenerator:addCircle(x, y, radius + 30, 6.0,  4.0, 'architect/nature/plants/birch_tree/f5/model')
        end
    end
end
function main()
    gen.generateWorld(false, 'test', detail.generateWorld)
end
