local detail = {}
function detail.bluegrassParam(seed)
    assert(seed)
    return e.array_concat({
        {seed, 0.5, 0.05, 0.0},
        cg.getColor4f('grass_palette', 'grass_base'),
        cg.getColor4f('grass_palette', 'grass_edge'),
        cg.getColor4f('grass_palette', 'ground_base'),
        cg.getColor4f('grass_palette', 'ground_second')
    })
end
function main()
    gen.generateTexture(false,
        'bluegrass_terrain', true, false, 
        1, nil,
        1024, 1024, false, 1.0, false,
        'generate_textures', 'BluegrassTerrain', detail.bluegrassParam(42));
        
    gen.generateTexture(false,
        'bluegrass', true, false, 
        4096, nil,
        1024, 1024, false, 1.0, false,
        'generate_textures', 'Bluegrass', detail.bluegrassParam(42));

    local lodInfo = {{5, 5}}
    gen.generateTree(false, 'bluegrass', lodInfo, nil)
end