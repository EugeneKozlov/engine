#constant Size
    Float4 cvSize;
#constant Parameters
    Float4[32] cvParam;
#layout Empty
#output PositionTexture
    Float4 Position;
    Float2 TexCoord;
    Float4 Param;
#target Multimap
    Color;
    Normal;
    Material;

#vertex vertex(layout = Empty, output = PositionTexture)
#region
    #include "Auto.Quad"
#main
    float2 vPos = GetQuadVertex(0.0, 1.0, inVertexID);
    outPosition = float4(textureToScreen(vPos), 0.5, 1.0);
    outTexCoord = vPos;
    outParam = 0.0;
#endregion

/// @name Bluegrass
/// @{
#vertex vertexBluegrass(layout = Empty, output = PositionTexture)
#region
    #include "Auto.Quad"
    #include "Math.Common"
    #include "Math.Random"
    #include "Math.Perlin2D"
    #include "Generate.Base"
    #include "Generate.LinearStripA"
    
#main
    // Constants
    InstanceLinearStripParam sParam;
    sParam.cvLocation       = float2(0.1, 0.9);
    sParam.cfStep           = 0.02;
    sParam.cfLocationNoise  = 0.02;
    sParam.cvWidth          = 0.01;
    sParam.cvHeight         = float2(0.8, 1.0);
    sParam.cfAngleNoise     = 7.0;

    // Compute location in the grid
    float2 vLocation;
    float2 vRotation;
    float2 vSize;
    float fClass;
    bool bCollapse = !ComputeInstanceLinearStrip(inInstanceID, sParam, cvParam[0].x, vLocation, vRotation, vSize, fClass);
    
    float2 vTexCoord = GetQuadVertex(0.0, 1.0, inVertexID);
    float2 vPos = textureToScreen(bCollapse ? 0.0 : Transform(vTexCoord, vLocation, 0.0, vRotation, vSize));
    outPosition = float4(vPos, inInstanceID / 16384.0, 1.0);
    outTexCoord = vTexCoord;
    outParam = float4(fClass, 0.0, 0.0, 0.0);
#endregion
#pixel pixelBluegrass(input = PositionTexture, target = Multimap)
#region
    #include "Math.Common"
    #include "Math.Random"
    #include "Generate.Base"
    #include "Generate.PolyShape"

    #define cvNormal    cvParam[0].yz
    #define cGrassBase  cvParam[1]
    #define cGrassEdge  cvParam[2]
#main
    // Compute base color
    float2 vTexel = inTexCoord - float2(0.5, 0.0);
    float fIntensity = DrawPolyShape(vTexel, float4(1.0, 10.0, 1.0, 1.0));
    if (fIntensity == 0)
        discard;
    
    // Compute normal
    float2 vNoise = Random2(inParam.x) * cvNormal;
    float3 vNormal = RandomNormal(vNoise);
    
    // Fill maps
    outColor.rgb = lerp(cGrassEdge.rgb, cGrassBase.rgb, fIntensity);
    outColor.a = 1.0;
    outNormal = float4(vNormal * 0.5 + 0.5, 1.0);
    outMaterial = float4(1.0 - fIntensity, 0.0, 0.0, 0.0);
#endregion
#program Bluegrass(vertex = vertexBluegrass, pixel = pixelBluegrass)
#pixel pixelBluegrassTerrain(input = PositionTexture, target = Multimap)
#region
    #include "Math.Common"
    #include "Math.Random"
    #include "Math.Perlin2D"

    #define cfSeed          cvParam[0].x
    #define cvNormal        cvParam[0].yz
    #define cfEdge          cvParam[0].w
    #define cGrassBase      cvParam[1]
    #define cGrassEdge      cvParam[2]
    #define cGroundBase     cvParam[3]
    #define cGroundSecond   cvParam[4]
#main    
    // Compute main fade
    float fMainFade = PerlinNoise2Dx4(
        inTexCoord + cfSeed, 
        16.0,
        float4(1.0, 2.0, 4.0, 8.0),
        float4(1.0, 0.5, 0.25, 0.125));
    fMainFade = max(0.0, lerp(cfEdge, 1.0, fMainFade));
    
    // Compute local fade
    float fLocalFade = PerlinNoise2Dx4(
        inTexCoord + cfSeed, 
        32.0,
        float4(1.0, 2.0, 4.0, 8.0),
        float4(1.0, 0.5, 0.25, 0.125));
    fLocalFade = Sharpen(fLocalFade, 3);
    
    // Compute mix noise
    float fMixNoise = PerlinNoise2Dx4(
        inTexCoord + cfSeed, 
        64.0,
        float4(1.0, 2.0, 4.0, 8.0),
        float4(1.0, 0.5, 0.25, 0.125));

    float4 vGrassColor = lerp(cGrassBase, cGrassEdge, fLocalFade);
    float4 vGroundColor = lerp(cGroundBase, cGroundSecond, fLocalFade);
    float4 vColor = NoiseLerp(vGrassColor, vGroundColor, fMixNoise, fMainFade, 0.2);
    //float4 vColor = vGrassColor;

    // Compute normal
    float3 vNormal = RandomNormal(0.0);
    
    // Fill maps
    outColor.rgb = vColor;
    outColor.a = 1.0;
    outNormal = float4(vNormal * 0.5 + 0.5, 1.0);
    outMaterial = float4(0.0, 0.0, 0.0, 0.0);
#endregion
#program BluegrassTerrain(vertex = vertex, pixel = pixelBluegrassTerrain)
/// @}
