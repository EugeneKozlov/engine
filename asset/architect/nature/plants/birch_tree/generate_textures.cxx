#constant Size
    Float4 cvSize;
#constant Parameters
    Float4[32] cvParam;
#resource texture2D(Float4) Source0
#resource texture2D(Float4) Source1
#layout Empty
#output PositionTexture
    Float4 Position;
    Float2 TexCoord;
    Float4 Param;
#target Multimap
    Color;
    Normal;
    Material;

#vertex vertex(layout = Empty, output = PositionTexture)
#region
    #include "Auto.Quad"
#main
    float2 vPos = GetQuadVertex(0.0, 1.0, inVertexID);
    outPosition = float4(textureToScreen(vPos), 0.5, 1.0);
    outTexCoord = vPos;
    outParam = 0.0;
#endregion

/// @name Leaf strip
#pixel pixelBirchLeaf(input = PositionTexture, target = Multimap)
#region
    #include "Math.Common"
    #include "Math.Random"
    #include "Math.Perlin2D"
    #include "Generate.Base"
    #include "Generate.SinCurve"
    #include "Generate.PolyShape"
    #include "Generate.SegmentedLeafA"

    #define cvSeed          cvParam[0].x
    
    #define cLeafMain   cvParam[1]
    #define cLeafEdge   cvParam[2]
    #define cRib        cvParam[3]

    float4 GenerateClasses(float fSeed, float2 vPixel)
    {
        float4 vParamLeaf[3];
        vParamLeaf[0] = float4(30, 2.5, 80.0, 20.0);
        vParamLeaf[1] = float4(0.05, 0.93, 8.0, 1.3);
        vParamLeaf[2] = float4(1.0, 10.0, 0.1, 1.0);
        
        float4 vParamRibs[4];
        vParamRibs[0] = float4(9, 0.7, 1.0, 11.0);
        vParamRibs[1] = float4(0.0, 0.05, 0.9, 0.9);
        vParamRibs[2] = float4(0.03, 0.02, 0.7, -0.05);
        vParamRibs[3] = float4(70.0, 50.0, 0, 0);
        
        float2 vScale = float2(0.77, 1.0);

        // Compute classes
        float fLeafFactor = DrawSegmentedLeafA(vPixel / vScale, vParamLeaf);
        float fRibFactor = DrawSegmentedRibsA(vPixel / vScale, vParamRibs);
        
        float4 vClasses = 0.0;
        if (fLeafFactor > 0.05)
        {
            vClasses.y = min(1.0, fRibFactor);
            vClasses.z = min(1.0, fLeafFactor);
            vClasses.w = Random(fSeed);
        }
        return vClasses;
    }
    void ComputeMaps(
        out float4 ovColor, out float ofHeight,
        float4 vClasses, float2 vPixel)
    {
        // Compute alpha
        ovColor = step(0.05, MaxOf(vClasses.xz));
        ofHeight = 0.0;
        
        // Compute color
        if (ovColor.a > 0.5)
        {
            // Paint leafs and ribs
            float4 vLeafColor = lerp(cLeafEdge, cLeafMain, vClasses.z);
            ovColor = lerp(vLeafColor, cRib, vClasses.y);
            ovColor.rgb += 0.03 * Random3(Random(vPixel));
            
            // Normal is unused
            
            // Compute height
            float fLeafHeight = 0.1;
            
            // Compute low-frequency leaf noise
            float fRadius = length(vPixel);
            float2 vRotation = MakeRotation(sign(vPixel.x) * 60.0);
            float2 vRadial = Rotate(vPixel, vRotation);
            float fNoiseL = PerlinNoise2Dx4(
                vRadial + vClasses.w, 
                float2(10.0, 3.0),
                float4(1.0, 2.0, 4.0, 8.0),
                float4(1.0, 0.5, 0.25, 0.125));
            fLeafHeight += 0.4 * fNoiseL;
            
            // Paint height
            ofHeight = fLeafHeight - 0.3 * vClasses.y;
        }
    }
#main
    // Current pixel
    float2 vPixel = inTexCoord - float2(0.5, 0.0);
    
    float4 vClasses = clamp(GenerateClasses(cvSeed, vPixel), 0.0, 1.0);
    
    // Compute maps
    float4 vColor;
    float fHeight;
    float fMaterial;
    ComputeMaps(vColor, fHeight, vClasses, vPixel);
    
    // Fill maps
    outColor = vColor;
    outNormal = 0.0;
    outMaterial = float4(fHeight, 0.0, 0.0, 1.0);
#endregion
#program BirchLeaf(vertex = vertex, pixel = pixelBirchLeaf)
/// @}

/// @name Foliage
/// @{
#vertex vertexBirchFoliage(layout = Empty, output = PositionTexture)
#region
    #include "Auto.Quad"
    #include "Math.Common"
    #include "Math.Random"
    #include "Math.Perlin2D"
    #include "Generate.Base"
    #include "Generate.RadialCloudA"
#main
    // Constants
    const float fScale = cvParam[1].z;
    const float fDensity = cvParam[1].w;
    InstanceRadialCloudParam sParam;
    sParam.cfStep           = 0.016 * fScale / fDensity;
    sParam.cfPosNoise       = 0.007 * fScale / fDensity;
    sParam.cfMinAngle       = -30.0;
    sParam.cfMaxAngle       = 30.0;
    sParam.cvSize           = 1.0 / 50.0 * fScale;
    sParam.cvRotCenter      = float2(0.0, 0.0);
    sParam.cfBeginDensity   = 1.0;
    sParam.cfEndDensity     = 0.2;
    sParam.cvPerlin         = float2(7.0, 3.0);
    
    // Compute location in the grid
    float2 vLocation;
    float2 vRotation;
    float fClass;
    bool bCollapse = !ComputeInstanceRadialCloud(inInstanceID, sParam, cvParam[0].x, vLocation, vRotation, fClass);
    
    float2 vTexCoord = GetQuadVertex(0.0, 1.0, inVertexID);
    float2 vPos = textureToScreen(bCollapse ? 0.0 : Transform(vTexCoord, vLocation, 0.0, vRotation, sParam.cvSize));
    outPosition = float4(vPos, inInstanceID / 16384.0, 1.0);
    outTexCoord = vTexCoord;
    outParam = float4(fClass, 0.0, 0.0, 0.0);
#endregion
#pixel pixelBirchFoliage(input = PositionTexture, target = Multimap)
#region
    #include "Math.Common"
    #include "Math.Random"
    #include "Math.Perlin2D"

    #define cvSeed          cvParam[0].x
    #define cvNormalMain    cvParam[1].x
    #define cvNormalRandom	cvParam[1].y
#main
    float4 vDiffuseColor = sampleTexture(Source0, inTexCoord);
    float4 vMaterialInfo = sampleTexture(Source1, inTexCoord);
    if (vDiffuseColor.a < 0.05)
        discard;
    
    // Compute normal
    float fNormalNoise = PerlinNoise2Dx4(
        inTexCoord + inParam.x, 
        1.0,
        float4(1.0, 2.0, 4.0, 8.0),
        float4(1.0, 0.5, 0.25, 0.125));
        
    float2 vNoise = (Random2(inParam.x) * 2.0 - 1.0) * cvNormalMain;
    vNoise.xy += (Random2(inParam.x + 1.0) * 2.0 - 1.0) * (fNormalNoise * 2.0 - 1.0) * cvNormalRandom;
    float3 vNormal = RandomNormal(vNoise);
    
    // Fill maps
    outColor.rgb = vDiffuseColor.rgb;
    outColor.a = 1.0;
    outNormal = float4(vNormal * 0.5 + 0.5, 1.0);
    outMaterial = vMaterialInfo;
#endregion
#program BirchFoliage(vertex = vertexBirchFoliage, pixel = pixelBirchFoliage)
/// @}

/// @name Bark
/// @{
#pixel pixelBirchBark(input = PositionTexture, target = Multimap)
#region
    #include "Math.Common"
    #include "Math.Random"
    #include "Math.Perlin2D"
    #include "Generate.Base"

    #define cvSeed          cvParam[0].x
    #define cvBaseScale     cvParam[1].xy
    #define cfCrackEdge     cvParam[1].z
    #define cfTickEdge      cvParam[1].w
    #define cvCrackScale    cvParam[2].xy
    #define cvTickScale     cvParam[2].zw
    
    #define cWhiteA cvParam[3]
    #define cWhiteB cvParam[4]
    #define cCrackA cvParam[5]
    #define cCrackB cvParam[6]
    #define cTick   cvParam[7]

    void ComputeMaps(
        out float4 ovColor, out float3 ovNormal, out float ofHeight,
        float2 vPixel, float fSeed)
    {
        float4 vRandom = Random4(fSeed);
        float2 vSaltPixel = vPixel + fSeed;
        
        // Compute main cracks
        float fCrackMain = PerlinNoise2Dx4(
            vPixel + vRandom.x, 
            cvCrackScale * cvBaseScale,
            float4(1.0, 2.0, 4.0, 8.0),
            float4(1.0, 0.5, 0.25, 0.125));
        fCrackMain = max(0.0, lerp(cfCrackEdge, 1.0, fCrackMain));
        
        // Compute ticks
        float fTick = PerlinNoise2Dx4(
            vPixel + vRandom.x, 
            cvTickScale * cvBaseScale,
            float4(1.0, 2.0, 4.0, 8.0),
            float4(1.0, 0.5, 0.25, 0.125));
        fTick = max(0.0, lerp(cfTickEdge, 1.0, fTick));

        // Mixing noises
        float fWhiteNoise = PerlinNoise2Dx4(
            vPixel + vRandom.x, 
            8.0 * cvBaseScale,
            float4(1.0, 2.0, 4.0, 8.0),
            float4(1.0, 0.5, 0.25, 0.125));
            
        float fCrackNoise = PerlinNoise2Dx4(
            vPixel + vRandom.y, 
            8.0 * float2(4.0, 1.0) * cvBaseScale,
            float4(1.0, 2.0, 4.0, 8.0),
            float4(1.0, 0.5, 0.25, 0.125));
            
        float fMixNoise = PerlinNoise2Dx4(
            vPixel + vRandom.z, 
            16.0 * float2(2.0, 1.0) * cvBaseScale,
            float4(1.0, 2.0, 4.0, 8.0),
            float4(1.0, 0.5, 0.25, 0.125));
            
        float4 vBaseColor = lerp(cWhiteA, cWhiteB, Sharpen(fWhiteNoise, 3));
        float4 vWhiteColor = lerp(vBaseColor, cTick, Sharpen(fTick, 3));
        float4 vCrackColor = lerp(cCrackA, cCrackB, Sharpen(fCrackNoise, 3));
        float4 vColor = NoiseLerp(vWhiteColor, vCrackColor, fMixNoise, fCrackMain, 0.2);

        // Paint color
        ovColor = vColor + (2.0 * Random(vPixel) - 1.0) * 0.005;
        
        // Paint flat normal
        ovNormal = float3(0.0, 0.0, 1.0);
        
        // Paint height
        float fCrachHeight = 1.0 - fCrackNoise * 0.5;
        float fWhiteHeight = 0.05 * fWhiteNoise + fTick * 0.5;
        ofHeight = NoiseLerp(fWhiteHeight, fCrachHeight, fMixNoise, fCrackMain, 0.2).x;
    }
#main
    float4 vColor;
    float3 vNormal;
    float fHeight;
    ComputeMaps(
        vColor, vNormal, fHeight,
        inTexCoord, Random(cvSeed));

    outColor = vColor;
    outNormal = float4(vNormal * 0.5 + 0.5, 1.0);
    outMaterial = float4(fHeight, 0.0, 0.0, 1.0);
#endregion
#program BirchBark(vertex = vertex, pixel = pixelBirchBark)
/// @}
