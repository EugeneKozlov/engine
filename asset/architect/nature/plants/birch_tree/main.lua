local detail = {}
function detail.leafParam(seed)
    assert(seed)
    return e.array_concat({
        {seed, 0.0, 0.0, 0.0},
        cg.getColor4f('foliage_palette', 'leaf_main'),
        cg.getColor4f('foliage_palette', 'leaf_edge'),
        cg.getColor4f('foliage_palette', 'rib_main')
    })
end
function detail.foliageParam(seed, scale, density)
    assert(seed)
    scale = scale or 1.0
    return e.array_concat({
        {seed, 0.0, 0.0, 0.0},
        {1.0, 0.7, scale, density},
    })
end
function detail.barkParam(seed, crackEdge, tickEdge, noiseScale)
    assert(seed)
    assert(crackEdge and tickEdge)
    assert(noiseScale and #noiseScale == 4)
    return e.array_concat({
        {seed, 0.0, 0.0, 0.0},
        {1.0, 4.0, crackEdge, tickEdge},
        noiseScale,
        cg.getColor4f('bark_palette', 'white_a'),
        cg.getColor4f('bark_palette', 'white_b'),
        cg.getColor4f('bark_palette', 'crack_a'),
        cg.getColor4f('bark_palette', 'crack_b'),
        cg.getColor4f('bark_palette', 'tick')
    })
end
function main()
    gen.generateTexture(false,
        'barkA', true, false, 
        nil, nil,
        256, 1024, false, 3.0, true,
        'generate_textures', 'BirchBark', detail.barkParam(42, -0.4, -0.5, {4.0, 16.0, 8.0, 64.0}));
        
    gen.generateTexture(false,
        'barkD', true, false, 
        nil, nil,
        256, 1024, false, 3.0, true,
        'generate_textures', 'BirchBark', detail.barkParam(42, -0.2, -0.5, {4.0, 8.0, 8.0, 64.0}));
        
    gen.generateTexture(false,
        'barkF', true, false, 
        nil, nil,
        256, 1024, false, 3.0, true,
        'generate_textures', 'BirchBark', detail.barkParam(42, -0.1, -0.5, {4.0, 8.0, 8.0, 64.0}));

    gen.generateTexture(false,
        'leaf', false, true, 
        nil, nil,
        64, 64, true, 1.0, false,
        'generate_textures', 'BirchLeaf', detail.leafParam(42));
        
    gen.generateTexture(false,
        'foliage2', true, false, 
        4096, {cg.loadAsset(cg.Asset.Texture, 'leaf_diffuse'), cg.loadAsset(cg.Asset.Texture, 'leaf_material')},
        512, 512, true, 1.0, false,
        'generate_textures', 'BirchFoliage', detail.foliageParam(42, 2.0, 1.0));

    gen.generateTexture(false,
        'foliage4', true, false, 
        4096, {cg.loadAsset(cg.Asset.Texture, 'leaf_diffuse'), cg.loadAsset(cg.Asset.Texture, 'leaf_material')},
        1024, 1024, true, 1.0, false,
        'generate_textures', 'BirchFoliage', detail.foliageParam(42, 1.0, 1.0));

    local lodInfo = {{5, 10}, {5, 5}}
    gen.generateTree(false, 'f29', lodInfo, 1024)
    gen.generateTree(false, 'f19', lodInfo, 1024)
    gen.generateTree(false, 'f11', lodInfo, 1024)
    gen.generateTree(false, 'f5', lodInfo, 1024)
end