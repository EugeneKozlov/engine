gen = {}
function gen.generateTexture(
    forceGenerate,
    name, 
    hasNormal, hasMaterial,
    numInstances, inputResources,
    outputWidth, outputHeight,
    ditherAlpha, heightScale, isWrapped,
    effectName, programName, parameters
)
    -- Check input
    assert(name)
    numInstances = numInstances or 1
    inputResources = inputResources or {}
    assert(outputWidth and outputHeight and outputWidth > 0 and outputHeight > 0)
    assert(effectName and programName and parameters)
    
    -- Check maps
    local diffuseMapName = name .. '_diffuse'
    local normalMapName = hasNormal and (name .. '_normal') or ''
    local materialMapName = hasMaterial and (name .. '_material') or ''
    local mapsReady = 
        cg.isFileExist(diffuseMapName .. '.dds')
        and (not hasNormal or cg.isFileExist(normalMapName .. '.dds'))
        and (not hasMaterial or cg.isFileExist(materialMapName .. '.dds'))
    
    -- Generate maps
    if not mapsReady
    or forceGenerate
    then
        
        -- Get utility
        local utilityEffect = cg.loadAsset(cg.Asset.Effect, 'architect/common/effect/texture_utility')
        assert(utilityEffect)

        -- Get generator
        local gen = cg.texture()
        
        -- Set diffuse output (simple or alpha)
        if ditherAlpha then
            gen:addOutputMipmapResource(cg.TextureType.Regular, cg.Compression.BC3, utilityEffect, 'Alpha', {})
        else
            gen:addOutputMipmapResource(cg.TextureType.Regular, cg.Compression.BC1, utilityEffect, 'Simple', {})
        end
        gen:addOutputMipmapResource(cg.TextureType.Regular, cg.Compression.No, utilityEffect, 'Simple', {})
        gen:addOutputMipmapResource(cg.TextureType.Regular, cg.Compression.BC1, utilityEffect, 'Simple', {})
        
        -- Add post-process event to heal alpha
        gen:addPostProcess(0, utilityEffect, 'HealAlpha', {16})
        
        -- Add post-process event to heal normal map
        gen:addPostProcess(1, utilityEffect, 'HealNormalMap', {16})
        
        -- Start generation
        gen:begin(outputWidth, outputHeight)
        
        -- Load input resources
        for i = 1, #inputResources do
            gen:addInputResource('Source' .. (i - 1), inputResources[i])
        end
        
        -- Setup main generator
        local mainEffect = cg.loadAsset(cg.Asset.Effect, effectName)
        assert(mainEffect)
        gen:setGenerator(mainEffect, programName, parameters)
        
        -- Diffuse color
        local diffuseMap = cg.createAsset(cg.Asset.Texture, diffuseMapName)
        cg.setWrapping(diffuseMap, isWrapped)
        -- Normal map without details
        local normalMapRaw = cg.createAsset(cg.Asset.Texture, '')
        cg.setWrapping(normalMapRaw, isWrapped)
        -- Height map
        local materialMap = cg.createAsset(cg.Asset.Texture, materialMapName)
        cg.setWrapping(materialMap, isWrapped)
        
        -- Generate texture
        gen:generate(numInstances)
        gen:save({diffuseMap, normalMapRaw, materialMap})
        
        -- Bake heigth details to normal map
        if hasNormal then
            -- Start generation
            gen:addOutputMipmapResource(cg.TextureType.Regular, cg.Compression.BC1, utilityEffect, 'Simple', {})
            gen:begin(outputWidth, outputHeight)
        
            -- Setup normal generator
            gen:setGenerator(utilityEffect, 'EtchNormalMap', {heightScale})
            gen:addInputResource('Source0', normalMapRaw)
            gen:addInputResource('Source1', materialMap)
            
            -- Final normal map
            local normalMap = cg.createAsset(cg.Asset.Texture, normalMapName)
            cg.setWrapping(normalMap, isWrapped)

            -- Generate normal map
            gen:generate(1)
            gen:save({normalMap})
        end
        
        -- Flush generated assets
        cg.flushGeneratedAssets()
    end
end
function gen.generateTree(
    forceGenerate,
    name,
    lods,
    proxyTextureSize
)
    assert(name)
    assert(lods)
    
    -- Check LOD meshes
    local meshNames = {}
    local lodsReady = true
    for i = 1, #lods do
        meshNames[i] = name .. '/lod' .. (i - 1)
        lodsReady = lodsReady and cg.isFileExist(meshNames[i] .. '.bin')
    end
    
    -- Generate LOD meshes
    local treeName = name .. '/tree'
    local modelName = name .. '/model'
    if not lodsReady
    or forceGenerate
    then
        local gen = cg.tree()
        
        -- Set tree asset to generate
        gen:setPrototype(cg.loadAsset(cg.Asset.Tree, treeName))
        
        -- Set all LODs
        for i = 1, #lods do
            gen:addLod(table.unpack(lods[i]))
        end
        
        -- Create output meshes
        local outputs = {}
        for i = 1, #lods do
            outputs[i] = cg.createAsset(cg.Asset.Mesh, meshNames[i])
        end
        
        -- Generate meshes
        gen:generate(outputs)
        
        -- Flush all
        cg.flushGeneratedAssets()
    end
    
    -- Generate proxy
    if proxyTextureSize then
        local proxyModelName = name .. '/proxy'
        local proxyDiffuseTexture = name .. '/proxy_diffuse'
        local proxyNormalTexture = name .. '/proxy_normal'
        if not cg.isFileExist(proxyModelName .. '.bin')
        or not cg.isFileExist(proxyDiffuseTexture .. '.dds')
        or not cg.isFileExist(proxyNormalTexture .. '.dds') 
        or forceGenerate
        then
            local gen = cg.proxy()
            
            -- Load utility effect
            local utilityEffect = cg.loadAsset(cg.Asset.Effect, 'architect/common/effect/texture_utility')
            assert(utilityEffect)
            
            -- Load model
            local sourceModel = cg.loadAsset(cg.Asset.Model, modelName)
            assert(sourceModel)
            
            -- Initialize proxy generator
            gen:resizeOutput(proxyTextureSize, proxyTextureSize)
            gen:setSourceModel(sourceModel)
            gen:setModelType(cg.ModelType.Cylinder8d8s)
            gen:setColorOutput(cg.Compression.BC3, utilityEffect, 'Alpha', {}, utilityEffect, 'HealAlpha', {16})
            gen:setNormalOutput(cg.Compression.BC3, utilityEffect, 'Simple', {}, utilityEffect, 'HealNormalMap', {16})
            
            -- Create outputs
            local outputMesh = cg.createAsset(cg.Asset.Mesh, proxyModelName)
            local outputColor = cg.createAsset(cg.Asset.Texture, proxyDiffuseTexture)
            local outputNormal = cg.createAsset(cg.Asset.Texture, proxyNormalTexture)
            gen:generate(outputMesh, outputColor, outputNormal)
            
            -- Flush generated assets
            cg.flushGeneratedAssets()
        end
    end
end
function gen.generateWorld(
    forceGenerate,
    name,
    main,
    config
)
    assert(name)
    assert(main)
    
    if not cg.isFileExist(name .. '.dat')
    or forceGenerate
    then
        -- Start generator
        local gen = cg.world()
        
        -- Create config
        local worldConfig = config
        if not worldConfig
        then
            worldConfig = cg.WorldRegionConfig()
            worldConfig.chunkWidth = 32
            worldConfig.regionWidth = 512
            worldConfig.regionHeightMap = 32
            worldConfig.regionNormalMap = 128
            worldConfig.regionColorMap = 128
        end
        
        -- Generate and save
        local worldAsset = cg.createAsset(cg.Asset.World, name)
        gen:begin(worldAsset, worldConfig)
        main(gen)
        gen:save()
    end
end