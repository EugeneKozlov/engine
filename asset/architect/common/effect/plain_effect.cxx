#constant PerBatch
    Float4x4 mModelViewProj;
    Float4x4 mModelView;
    Float4 vLightDirection;
    Float4 vLightColor;
#resource texture2D(Float4) Diffuse
#resource shadow2D(Float) Shadow
#layout Colored
    Float4 Position;
    Float4 Color;
#output Colored
    Float4 Position;
    Float4 Color;
#vertex vertexColored(layout = Colored, output = Colored)
#main
    outPosition = mul(mModelViewProj, inPosition);
    outColor = inColor;
    
#layout Textured 
    Float4 Position;
    Float2 TexCoord;
    Float4 Color;
#output Textured
    Float4 Position;
    Float2 TexCoord;
    Float4 Color;
#vertex vertexTextured(layout = Textured, output = Textured)
#main
    outPosition = mul(mModelViewProj, inPosition);
    outColor = inColor;
    outTexCoord = inTexCoord;

#layout Lighted
    Float3 Position;
    Float2 TexCoord;
    Float3 Normal;
#vertex vertexLighted(layout = Lighted, output = Textured)
#main
    outPosition = mul(mModelViewProj, float4(inPosition, 1.0));
    float4 normal = mul(mModelView, float4(inNormal, 0.0));
    float lightFactor = max(dot(vLightDirection.xyz, normal.xyz), 0);
    float lightIntensity = lerp(vLightDirection.w, 1.0, lightFactor);
    outColor = lightIntensity * vLightColor;
    outTexCoord = inTexCoord;
    
#target Color
    Color;

#pixel pixelColored(input = Colored, target = Color)
#main
    outColor = inColor;
    
#pixel pixelTextured(input = Textured, target = Color)
#main
    float4 color = sampleTexture(Diffuse, inTexCoord);
    color.rgb /= color.a;
    color *= inColor;
    
    if (color.a LESS_THAN 0.5)
        discard;
    outColor = color;

#program Colored(vertex = vertexColored, pixel = pixelColored)
#program Textured(vertex = vertexTextured, pixel = pixelTextured)
#program Lighted(vertex = vertexLighted, pixel = pixelTextured)
