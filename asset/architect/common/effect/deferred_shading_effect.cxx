#constant Constant
    Float2 vSize;
#constant PerLight
    Float4x4 mViewToLight;
    Float4 vColor;
    Float4 vDirection;
    Float4 vCameraInfo;
    Float4 vNearFar;
#resource texture2D(Float) DepthMap
#resource texture2D(Float4) GBuffer0
#resource texture2D(Float4) GBuffer1
#resource texture2D(Float4) GBuffer2
#resource shadow2D(Float) ShadowMap
#layout Empty
#output PixelInput
    Float4 Position;
    Float4 ViewSpacePosition;
    Float4 TexCoord;
    
#vertex FullscreenVertex(layout = Empty, output = PixelInput)
#   include "Auto.Quad"
#main
    float2 vTexturePos = GetQuadVertex(0.0, 1.0, inVertexID);
    float2 vScreenPos = textureToScreen(vTexturePos);
    
    // position
    outTexCoord = vTexturePos.xyxy * float4(1.0, 1.0, vSize);
    outPosition = float4(vScreenPos, 0.001, 1.0);
    outViewSpacePosition = float4(vCameraInfo.xy * vScreenPos, 1.0, 1.0);
    
#target Color
    Color;
    
#pixel FinalPixel(input = PixelInput, target = Color)
#main
    float4 color = sampleTexture(GBuffer2, inTexCoord.xy);
    outColor = float4(color.xyz, 1.0);

#pixel DirectionalPixel(input = PixelInput, target = Color)
    #include "Material.PBR"
    #include "Lighting.PBR"
#main
    int2 vTexel = int2(inTexCoord.z, inTexCoord.w);
    
    // Restore Z
    const float cfNearZ = vCameraInfo.z;
    const float cfFarZ = vCameraInfo.w;
    float fDepth = fetchTexture(DepthMap, vTexel).x;
    float fViewZ = 2.0 * cfNearZ * cfFarZ / (cfFarZ + cfNearZ - (2.0 * fDepth - 1.0) * (cfFarZ - cfNearZ));
    
    // HACK: Keep last cascade open
    const bool cbLastCascade = vNearFar.w > 100;
    
    // Fade cascades
    float fCascadeFadeIn = smoothstep(vNearFar.x, vNearFar.y, fViewZ);
    float fCascadeFadeOut = 1.0 - smoothstep(vNearFar.z, vNearFar.w, fViewZ);
    float fCascadeSmooth = cbLastCascade ? fCascadeFadeIn : min(fCascadeFadeIn, fCascadeFadeOut);
    float fShadowFade = cbLastCascade ? fCascadeFadeOut : 1.0;
    
    // Discard out-of-cascade
    if (fCascadeSmooth LEQUAL_THAN 0.0)
        discard;
    
    // Compute view-space position
    float4 vPositionVS = inViewSpacePosition * float4(fViewZ, fViewZ, fViewZ, 1.0);
    
    // Read G-buffer
    float4 vGBuffer0 = fetchTexture(GBuffer0, vTexel);
    float4 vGBuffer1 = fetchTexture(GBuffer1, vTexel);
    
    // Decode color-param
    float3 vBaseColor = vGBuffer0.rgb;
    float fParam = vGBuffer0.a;
    
    // Decode matrial ID and normal-spec
    float fMaterial = vGBuffer1.a;
    vGBuffer1.rgb = vGBuffer1.rgb * 2.0 - 1.0;
    vGBuffer1.a = length(vGBuffer1.rgb);
    float fOneMinusRoughness = clamp(2.0 * vGBuffer1.a - 1.0, 0.0, 1.0);
    float3 vNormal = vGBuffer1.rgb / vGBuffer1.a;
    
    // Decode material parameter
    float fReflectivity = fMaterial == 1.0 ? fParam : C_MIN_REFLECTIVITY;
    float fOneMinusThickness = fMaterial == 0.0 ? fParam : 0.0;
    
    // Compute color
    LightDesc sLightDesc;
    sLightDesc.color = float3(1.0, 1.0, 1.0) * PI;
    sLightDesc.dir = vDirection;
    sLightDesc.ndotl = max(DotClamped(vDirection, vNormal), fOneMinusThickness * DotClamped(vDirection, -vNormal));
    IndirectLightDesc sIndirectLightDesc;
    sIndirectLightDesc.diffuse = 0.0;
    sIndirectLightDesc.specular = 0.0;
    float vOneMinusReflectivity = 1.0 - fReflectivity;
    float3 vEyeVec = normalize(vPositionVS.xyz / vPositionVS.w);
    float4 vPBRColor = ComputePhysicalBased(vBaseColor * vOneMinusReflectivity, vBaseColor * fReflectivity, 
        vOneMinusReflectivity, fOneMinusRoughness, vNormal, -vEyeVec, sLightDesc, sIndirectLightDesc);
    
    // Compute shadow
    float4 vPositionLS = mul(mViewToLight, vPositionVS);
    vPositionLS /= vPositionLS.w;
    
    float fShadow = 0.0;
    for (int i = -1; i <= 1; ++i)
        for (int j = -1; j <= 1; ++j)
            fShadow += sampleShadowOffset(ShadowMap, vPositionLS.xy, vPositionLS.z, int2(i, j)).x;
    fShadow /= 9.0;
    
    outColor.rgb = vPBRColor.rgb * fCascadeSmooth * lerp(1.0, fShadow, fShadowFade) * 0.8;
    outColor.a = 1.0;

#program Final(vertex = FullscreenVertex, pixel = FinalPixel)
#program Directional(vertex = FullscreenVertex, pixel = DirectionalPixel)

/// Skybox program
/// TODO: Move to separate effect
#region Sky
    #define	EPS	0.1
    /// Perez zenith func
    float3 perezZenith ( float t, float thetaSun )
    {
        const float	pi = 3.1415926;
        const float4	cx1 = float4 ( 0,       0.00209, -0.00375, 0.00165  );
        const float4	cx2 = float4 ( 0.00394, -0.03202, 0.06377, -0.02903 );
        const float4	cx3 = float4 ( 0.25886, 0.06052, -0.21196, 0.11693  );
        const float4	cy1 = float4 ( 0.0,     0.00317, -0.00610, 0.00275  );
        const float4	cy2 = float4 ( 0.00516, -0.04153, 0.08970, -0.04214 );
        const float4	cy3 = float4 ( 0.26688, 0.06670, -0.26756, 0.15346  );

        float	t2    = t*t;							// turbidity squared
        float	chi   = (4.0 / 9.0 - t / 120.0 ) * (pi - 2.0 * thetaSun );
        float4	theta = float4 ( 1, thetaSun, thetaSun*thetaSun, thetaSun*thetaSun*thetaSun );

        float	Y = (4.0453 * t - 4.9710) * tan ( chi ) - 0.2155 * t + 2.4192;
        float	x = t2 * dot ( cx1, theta ) + t * dot ( cx2, theta ) + dot ( cx3, theta );
        float	y = t2 * dot ( cy1, theta ) + t * dot ( cy2, theta ) + dot ( cy3, theta );

        return float3 ( Y, x, y );
    }

    /// Perez allweather func (turbidity, cosTheta, cosGamma)
    float3	perezFunc ( float t, float cosTheta, float cosGamma )
    {
        float	gamma      = acos ( cosGamma );
        float	cosGammaSq = cosGamma * cosGamma;
        float	aY =  0.17872 * t - 1.46303;
        float	bY = -0.35540 * t + 0.42749;
        float	cY = -0.02266 * t + 5.32505;
        float	dY =  0.12064 * t - 2.57705;
        float	eY = -0.06696 * t + 0.37027;
        float	ax = -0.01925 * t - 0.25922;
        float	bx = -0.06651 * t + 0.00081;
        float	cx = -0.00041 * t + 0.21247;
        float	dx = -0.06409 * t - 0.89887;
        float	ex = -0.00325 * t + 0.04517;
        float	ay = -0.01669 * t - 0.26078;
        float	by = -0.09495 * t + 0.00921;
        float	cy = -0.00792 * t + 0.21023;
        float	dy = -0.04405 * t - 1.65369;
        float	ey = -0.01092 * t + 0.05291;

        return float3 ( (1.0 + aY * exp(bY/cosTheta)) * (1.0 + cY * exp(dY * gamma) + eY*cosGammaSq),
                      (1.0 + ax * exp(bx/cosTheta)) * (1.0 + cx * exp(dx * gamma) + ex*cosGammaSq),
                      (1.0 + ay * exp(by/cosTheta)) * (1.0 + cy * exp(dy * gamma) + ey*cosGammaSq) );
    }

    /// Draw sky
    float3	perezSky ( float t, float cosTheta, float cosGamma, float cosThetaSun )
    {
        float	thetaSun = acos        ( cosThetaSun );
        float3	zenith   = perezZenith ( t, thetaSun );
        float3	clrYxy   = zenith * perezFunc ( t, cosTheta, cosGamma ) / perezFunc ( t, 1.0, cosThetaSun );
        
        clrYxy [0] *= smoothstep ( 0.0, EPS, cosThetaSun );			// make sure when thetaSun > PI/2 we have black color
        
        return clrYxy;
    }

    float3 PerezSky(float3 vEye, float3 vLight, float fTurbidity)
    {
        float fLightEye = dot(vLight, vEye);
        return perezSky(fTurbidity, max(vEye.y, 0.0) + 0.05, fLightEye, vLight.y);
    }
#endregion
#region Color
    float3 ConvertYxyToRgb(float3 vYxy)
    {
        static const float cfColorExp = 25.0;
        
        float3	clrYxy = float3 ( vYxy );
                                                        // now rescale Y component
        clrYxy [0] = 1.0 - exp ( -clrYxy [0] / cfColorExp );

        float	ratio    = clrYxy [0] / clrYxy [2];		// Y / y = X + Y + Z
        float3	XYZ;

        XYZ.x = clrYxy [1] * ratio;						// X = x * ratio
        XYZ.y = clrYxy [0];								// Y = Y
        XYZ.z = ratio - XYZ.x - XYZ.y;					// Z = ratio - X - Y

        const	float3	rCoeffs = float3 ( 3.240479, -1.53715, -0.49853  );
        const	float3	gCoeffs = float3 ( -0.969256, 1.875991, 0.041556 );
        const	float3	bCoeffs = float3 ( 0.055684, -0.204043, 1.057311 );

        return	float3 ( dot ( rCoeffs, XYZ ), dot ( gCoeffs, XYZ ), dot ( bCoeffs, XYZ ) );
    }

#endregion
#include "Material.DeclareRenderTargets"
#constant SkyboxParam
    Float4x4 mViewProj;
    Float4 vSunPositionTurbidity;
#output SkyboxOutput
    Float4 Position;
    Float4 PositionWS;
#vertex SkyboxVertex(layout = Empty, output = SkyboxOutput)
#region
#   include "Auto.Cube"
#main
    float4 vPositionWS = float4(GetCubeVertex(inVertexID), 1.0);
    float4 vPositionPS = mul(vPositionWS, mViewProj);
    vPositionPS = vPositionPS.xyww;
    
    // position
    outPosition = vPositionPS;
    outPositionWS = vPositionWS;
#endregion
#pixel SkyboxPixel(input = SkyboxOutput, target = GeometryTarget)
#region
#   include "Sky"
#   include "Color"
#main
    outBaseColor = 0.0;
    outNormal = 0.0;
    
    float3 vEye = normalize(inPositionWS.xyz);
    float3 vLight = normalize(vSunPositionTurbidity.xyz);
    float fLightEye = dot(vLight, vEye);
    float3 vColorYxy = PerezSky(vEye, vLight, vSunPositionTurbidity.w);
    
    float3 vSkyColor = clamp(ConvertYxyToRgb(vColorYxy), 0.0, 1.0);
    float3 vSunColor = float3(1.0, 1.0, 1.0);
    float fSunFactor = smoothstep(0.9995, 0.9996, fLightEye);
    outHdrColor.rgb = lerp(vSkyColor, vSunColor, fSunFactor);
    //outHdrColor.rgb = vEye;
    outHdrColor.a = 1.0;
#endregion
#program SkyboxProgram(vertex = SkyboxVertex, pixel = SkyboxPixel)
