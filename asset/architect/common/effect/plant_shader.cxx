#constant ModelUniformBuffer
    Float4[4] cvParam;
    
#resource texture2D(Float4) DiffuseMap
#resource texture2D(Float4) NormalMap
#layout VertexTangent
    Float3 Position;
    Float2 Uv;
    Float3 Normal;
    Float3 Tangent;
    Float3 Binormal;
    Float4 Wind;
    
    Float4 ModelA;
    Float4 ModelB;
    Float4 ModelC;
    Float2 Fade;
    Float4 WindDirection;
    Float4 WindParam;

#region Billboard
    float3 MakeOrtho(in float3 vAxis, in float3 vVector)
    {
        float3 vFlat = vVector - vAxis * dot(vVector, vAxis);
        return SafeNormalizeX(vFlat);
    }
    float ComputeVisibility(
        bool bIsVertical,
        float3 vEye, float3 vModelUp, float3 vNormal,
        float3 fFadeBegin, float3 fFadeEnd)
    {
        float3 vFlatEye = MakeOrtho(vModelUp, vEye);
        float3 vFlatNormal = MakeOrtho(vModelUp, vNormal);
        
        float3 vSource;
        vSource.x = dot(vFlatEye, vFlatNormal);
        vSource.yz = dot(vEye, vModelUp);
        
        float3 vFades = Unlerp(fFadeBegin, fFadeEnd, vSource);
        
        float fSwitchFactor = vFades.x;
        float fFadeFactor = bIsVertical ? (1.0 - vFades.y) : vFades.z;
        
        return min(fFadeFactor, fSwitchFactor);
    }
#endregion
#region main.Material
#endregion
#region main.Pass
    // I_PROGRAM            string  Program name
    // I_TARGET             string  Pixel shader target

    #include "VertexShader"
    #if I_MATERIAL == C_MATERIAL_PROXY
    #   include "GeometryShader"
    #endif
    #include "PixelShader"
    #if I_MATERIAL == C_MATERIAL_PROXY
    #   program I_PROGRAM(vertex=VS_NAME(I_PROGRAM), geometry=GS_NAME(I_PROGRAM), pixel=PS_NAME(I_PROGRAM))
    #else
    #   program I_PROGRAM(vertex=VS_NAME(I_PROGRAM), pixel=PS_NAME(I_PROGRAM))
    #endif
#endregion
#region VertexShader
    #output PS_INPUT(I_PROGRAM)
        Float4 Position;
        Float2 Uv;
        #if I_PASS != C_PASS_DEPTH
            Float3 Normal;
            Float3 Tangent;
            Float3 Binormal;
        #endif
        Float2 Fade;
    #vertex VS_NAME(I_PROGRAM)(layout = VertexTangent, output = PS_INPUT(I_PROGRAM))
        #include "Math.Wave"
        #include "Material.Wind"    
        #include "Billboard"
    #main       
        // Instance location
        float3x3 mModelMatrix = float3x3(inModelA.xyz, inModelB.xyz, inModelC.xyz);
        float3 vModelPosition = float3(inModelA.w, inModelB.w, inModelC.w);
        float3 vWindDirection = inWindDirection.xyz * 2.0 - 1.0;
        
        float3 vNormalWS = mul(mModelMatrix, float4(inNormal, 0.0));
        float3 vModelUp = normalize(float3(mModelMatrix[0][1], mModelMatrix[1][1], mModelMatrix[2][1]));
        #if I_MATERIAL == C_MATERIAL_PROXY
            float cfEdgeMainMagnitude = cvParam[2].y;
            float cfEdgeTurbulenceMagnitude = cvParam[2].z;
            float cfEdgeFrequency = cvParam[2].w;
            
            float cfBranchFrequency = 0.0;
            float cfBranchPhaseModifier = 0.0;
            float cfBranchMainMagnitude = 0.0;
            float cfBranchTurbulenceMagnitude = 0.0;

            float cfTrunkMagnitude = cvParam[2].x;
            
            float3 cvFadeBegin = cvParam[0].yzw;
            float3 cvFadeEnd = cvParam[1].yzw;
            
            float3 vEye = normalize(vCameraPosition - vModelPosition);
            float fTextureFade = ComputeVisibility(
                inNormal.y == 0.0,
                vEye,
                vModelUp, vNormalWS, 
                cvFadeBegin, cvFadeEnd);
        #else
            float cfEdgeMainMagnitude = cvParam[0].y;
            float cfEdgeTurbulenceMagnitude = cvParam[0].z;
            float cfEdgeFrequency = cvParam[0].w;
            
            float cfBranchFrequency = cvParam[1].x;
            float cfBranchPhaseModifier = cvParam[1].y;
            float cfBranchMainMagnitude = cvParam[1].z;
            float cfBranchTurbulenceMagnitude = cvParam[1].w;

            float cfTrunkMagnitude = cvParam[2].x;
            
            float fTextureFade = 1.0;
        #endif
        
        // Preserve shape
        float3 vPositionWS = mul(mModelMatrix, inPosition);
        
        // Apply wind
        float2 vEdgeMagnitude = inWindParam.xw * float2(cfEdgeMainMagnitude, cfEdgeTurbulenceMagnitude);
        vPositionWS = FrondEdgeWind(
            vModelPosition, vPositionWS, vNormalWS, 
            inWind.w * max(vEdgeMagnitude.x, vEdgeMagnitude.y), 
            cfEdgeFrequency,
            vTime.x);
        vPositionWS = GlobalWind(
            vModelPosition, vPositionWS, vNormalWS,
            inWind.x * cfTrunkMagnitude, 
            inWind.y * cfBranchMainMagnitude,
            inWindParam.x,
            inWindParam.y,
            inWindParam.z,
            inWind.z * cfBranchPhaseModifier,
            vTime.x, 
            vWindDirection);
        vPositionWS = BranchTurbulenceWind(
            vModelPosition, vModelUp, vPositionWS, 
            inWind.y * cfBranchTurbulenceMagnitude * inWindParam.w, 
            cfBranchFrequency,
            inWind.z, 
            vTime.x, 
            vWindDirection);
            
        // Move proxy to prevent bad shading
        #if I_MATERIAL == C_MATERIAL_PROXY && I_PASS == C_PASS_DEPTH
            float cfShadowPad = cvParam[2].x;
            vPositionWS -= vEye * cfShadowPad;
        #endif
        
        // Compute final position
        vPositionWS += vModelPosition;
        float3 vPositionVS = mul(mView, float4(vPositionWS, 1.0));
        
        outPosition = mul(mProjection, float4(vPositionVS, 1.0));
        outUv = inUv;
        
        #if I_PASS != C_PASS_DEPTH
            outNormal = mul(mView, float4(vNormalWS, 0.0));
            outTangent = mul(mView, float4(mul(mModelMatrix, inTangent), 0.0));
            outBinormal = mul(mView, float4(mul(mModelMatrix, inBinormal), 0.0));
        #endif

        outFade.x = inFade.x;
        outFade.y = min(inFade.y, fTextureFade);
#endregion
#region GeometryShader
    #geometry GS_NAME(I_PROGRAM)(\
        input = PS_INPUT(I_PROGRAM),  input_primitive = triangle, \
        output = PS_INPUT(I_PROGRAM), output_primitive = triangle_strip, output_size = 3)
    #main
        float fade = inFade(0).y + inFade(1).y + inFade(2).y;
        if (fade != 0)
        {
            outPosition = inPosition(0);
            outUv = inUv(0);
            outFade = inFade(0);        
            #if I_PASS != C_PASS_DEPTH
                outNormal = inNormal(0);
                outTangent = inTangent(0);
                outBinormal = inBinormal(0);
            #endif
            emitVertex;
            
            outPosition = inPosition(1);
            outUv = inUv(1);
            outFade = inFade(1);        
            #if I_PASS != C_PASS_DEPTH
                outNormal = inNormal(1);
                outTangent = inTangent(1);
                outBinormal = inBinormal(1);
            #endif
            emitVertex;
            
            outPosition = inPosition(2);
            outUv = inUv(2);
            outFade = inFade(2);        
            #if I_PASS != C_PASS_DEPTH
                outNormal = inNormal(2);
                outTangent = inTangent(2);
                outBinormal = inBinormal(2);
            #endif
            emitVertex;
            
            endPrimitive;
        }
#endregion
#region PixelShader
    #pixel PS_NAME(I_PROGRAM)(input = PS_INPUT(I_PROGRAM), target = I_TARGET)
        #include "Math.Random"
        #include "Material.PBR"
    #main
        // Dithering for proxies
        float cfDithering = cvParam[0].x;
        float fProxyFade = Random(floor(inUv * cfDithering));
        if (fProxyFade GREATEN_THAN inFade.y)
            discard;
        
        // Main dithering
        float2 vScreenCoord = inPosition.xy;
        float fMainFade = Random(floor(vScreenCoord));
        if (fMainFade GREATEN_THAN inFade.x OR fMainFade + 1.0 LESS_THAN inFade.x)
            discard;
        
        // Cutout transparent
        float4 vDiffuseColor = sampleTexture(DiffuseMap, inUv);
        #if I_MATERIAL != C_MATERIAL_SOLID
            if (vDiffuseColor.a LESS_THAN 0.5)
                discard;
        #endif
        
        // Material parameters
        const float cvOpacity = 1.0;
        const float cvReflectivity = C_MIN_REFLECTIVITY;
        const float cvSmoothness = 0.1;
        #if I_MATERIAL == C_MATERIAL_TRANSPARENT
            const float cfMaterial = 0.0;
            const float cfParameter = cvOpacity;
        #elif I_MATERIAL == C_MATERIAL_SOLID
            const float cfMaterial = 1.0;
            const float cfParameter = cvReflectivity;
        #endif
        
        #if I_PASS != C_PASS_DEPTH
            // Sample normal map
            float4 vNormalColor = sampleTexture(NormalMap, inUv);
            float3 vNormalMap = vNormalColor.xyz * 2.0 - 1.0;
            
            // Read material for proxy
            #if I_MATERIAL == C_MATERIAL_PROXY
                float cfMaterial = vNormalColor.a;
                float cfParameter = lerp(cvOpacity, cvReflectivity, cfMaterial);
            #endif
            
            #if I_MATERIAL == C_MATERIAL_TRANSPARENT
                float fFlipNormal = isFrontFace ? 1.0 : -1.0;
            #else
                float fFlipNormal = 1.0;
            #endif
            
            float3 vNormal = normalize(inTangent * vNormalMap.x + inBinormal * vNormalMap.y + inNormal * vNormalMap.z * fFlipNormal);
            
            #if I_PASS != C_PASS_PROXY
                outBaseColor.rgb = vDiffuseColor.rgb;
                outBaseColor.a = cfParameter;
                outNormal.rgb = vNormal * (cvSmoothness * 0.5 + 0.5) * 0.5 + 0.5;
                outNormal.a = cfMaterial;
                outHdrColor.rgb = outBaseColor.rgb * 0.2;
                outHdrColor.a = 1.0;
            #else
                outColor.rgb = vDiffuseColor.rgb;
                outColor.a = 1.0;
                outNormal.rgb = normalize(vNormal) * float3(0.5, 0.5, -0.5) + 0.5;
                outNormal.a = cfMaterial;
            #endif
        #endif
#endregion

#include "Material.DeclareHeader"
#include "Material.DeclareRenderTargets"

#define C_MATERIAL_SOLID        1
#define C_MATERIAL_TRANSPARENT  2
#define C_MATERIAL_PROXY        3

#redefine I_MATERIAL_NAME   Solid
#redefine I_MATERIAL        C_MATERIAL_SOLID
#redefine I_HAS_PROXY       1
#include "Material.Add"

#redefine I_MATERIAL_NAME   Transparent
#redefine I_MATERIAL        C_MATERIAL_TRANSPARENT
#redefine I_HAS_PROXY       1
#include "Material.Add"

#redefine I_MATERIAL_NAME   Proxy
#redefine I_MATERIAL        C_MATERIAL_PROXY
#redefine I_HAS_PROXY       0
#include "Material.Add"
