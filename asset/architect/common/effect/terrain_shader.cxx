#constant Constant
    Float4 vWireColor; // Wire color and threshold
#constant PerView
    Float4x4 mViewProj;
    Float4x4 mView;
#constant PerLod
    Float4 vZero; // Region zero (in units)
    Float4 vWidthTess; // Region width, tessellation factor, unused, unused
#constant PerChunk
    Float4 vMad; // Adding to each vertex position (in units), scaling
#resource texture2D(Float4) HeightMap
#resource texture2D(Float4) NormalMap
#resource texture2D(Float4) ColorMap
#resource texture2D(Float4) BlendMap
#resource texture2D(Float) NoiseMap
#resource texture2D(Float4) DiffuseMap0
#resource texture2D(Float4) DiffuseMap1
#layout Terrain
    Float4 Position;
    Float2 LocalTex;
    Int2 Flag;
#output ControlInput
    Float4 Position;
    Float4 TexCoord;
    Float3 CacheTexCoord;
#output GeometryInput
    Float4 Position;
    Float4 TexCoord;
    Float3 CacheTexCoord;
#output EvalInput
    Float4 Position;
    Float4 TexCoord;
    Float3 CacheTexCoord;

#region VertexSource
    // C_MUL_POS - on/off position by ViewProjection multiplication
    // C_PROVIDE_DIST - on/off distance computing
    
    // position
    float4 position = float4(inPosition.xyz * vMad.w + vMad.xyz, 1.0);
    
    // cross texture
    float2 texcoord;
    texcoord = position.xz - vZero.xy + 0.5;
    
    // read height
    int2 integerTexcoord = int2(texcoord.x, texcoord.y);
    position.y = fetchTexture(HeightMap, integerTexcoord).x;
            
    // write
    #if C_MUL_POS != 0
        outPosition = mul(mViewProj, position);
    #else
        outPosition = position;
    #endif
    outTexCoord = float4(position.xz, texcoord / (vWidthTess.x + 1.0));
    outCacheTexCoord.xy = (position.xz + vZero.zw) / vWidthTess.y;
    
    float4 positionVS = mul(mViewProj, position);
    outCacheTexCoord.z = length(positionVS);
#endregion

#redefine C_MUL_POS 1
#vertex vertex(layout = Terrain, output = GeometryInput)
#main
#   include "VertexSource"

#redefine C_MUL_POS 0
#vertex vertexTessellated(layout = Terrain, output = ControlInput)
#main
#   include "VertexSource"

#tess tessellation(\
    input = ControlInput, input_primitive = quad, input_size = 4, output_size = 4, \
    tess = EvalInput, tess_output = ccw, output_spacing = integer, \
    output = GeometryInput)
#const
    float k = vWidthTess.z;
    
    outInnerFactor(0) = k;
    outInnerFactor(1) = k;
    
    outOuterFactor(0) = k;
    outOuterFactor(1) = k;
    outOuterFactor(2) = k;
    outOuterFactor(3) = k;
#control
    outPosition = inPosition(inCurrentVertex);
    outTexCoord = inTexCoord(inCurrentVertex);
    outCacheTexCoord = inCacheTexCoord(inCurrentVertex);
#main
    float4 pos = qlerp(inPosition);
    float4 texcoord = qlerp(inTexCoord);
    float3 cacheTexcoord = qlerp(inCacheTexCoord);
    
    pos.y = sampleTextureLod(HeightMap, texcoord.zw, 0).x;
    
    outPosition = mul(mViewProj, pos);
    outTexCoord = texcoord;
    outCacheTexCoord = cacheTexcoord;

#output PixelInput
    Float4 Position;
    Float4 TexCoord;
    Float3 CacheTexCoord;
#output PixelWiredInput
    Float4 Position;
    Float4 TexCoord;
    Float3 CacheTexCoord;
    Float3 Edges;
#region GetHeights
    float3 GetHeights(in float2 points[3])
    {
        // Compute the edges vectors of the transformed triangle
        float2 edges[3];
        edges[0] = points[1] - points[0];
        edges[1] = points[2] - points[1];
        edges[2] = points[0] - points[2];

        // Store the length of the edges
        float lengths[3];
        lengths[0] = length(edges[0]);
        lengths[1] = length(edges[1]);
        lengths[2] = length(edges[2]);

        // Compute the cos angle of each vertices
        float cosAngles[3];
        cosAngles[0] = dot( -edges[2], edges[0]) / ( lengths[2] * lengths[0] );
        cosAngles[1] = dot( -edges[0], edges[1]) / ( lengths[0] * lengths[1] );
        cosAngles[2] = dot( -edges[1], edges[2]) / ( lengths[1] * lengths[2] );

        // The height for each vertices of the triangle
        float3 heights;
        heights[1] = lengths[0]*sqrt(1 - cosAngles[0]*cosAngles[0]);
        heights[2] = lengths[1]*sqrt(1 - cosAngles[1]*cosAngles[1]);
        heights[0] = lengths[2]*sqrt(1 - cosAngles[2]*cosAngles[2]);
        
        return heights;
    };
#endregion
#geometry geometryWired( \
    input = GeometryInput, input_primitive = triangle, \
    output = PixelWiredInput, output_primitive = triangle_strip, output_size = 3)
    
#   define GetScreenPos(I) (inPosition(I).xy / inPosition(I).w)
#   include "GetHeights"
#main
    float2 points[3];
    points[0] = GetScreenPos((0));
    points[1] = GetScreenPos((1));
    points[2] = GetScreenPos((2));
    float3 heights = GetHeights(points);
    
    // write
    outPosition = inPosition(0);
    outTexCoord = inTexCoord(0);
    outCacheTexCoord = inCacheTexCoord(0);
    outEdges = float3(0, heights[0], 0);
    emitVertex;
    
    outPosition = inPosition(1);
    outTexCoord = inTexCoord(1);
    outCacheTexCoord = inCacheTexCoord(1);
    outEdges = float3(0, 0, heights[1]);
    emitVertex;
    
    outPosition = inPosition(2);
    outTexCoord = inTexCoord(2);
    outCacheTexCoord = inCacheTexCoord(2);
    outEdges = float3(heights[2], 0, 0);
    emitVertex;
    
    endPrimitive;

#target Geometry
    BaseColor;
    Normal;
    HdrColor;
#target Depth
#region ComputeColor
    float3 ComputeColor(in float3 source, in float3 edges)
    {
        float3 dest = source;
        
        // shortest distance between the fragment and the edges
        float dist = min(min(edges.x, edges.y), edges.z) / vWireColor.w;

        // cull
        if(dist LESS_THAN 1.5)
        {
            // remap to [0..2]
            dist = clamp(dist + 0.5, 0, 2);

            // compute alpha as exp2(-2(x)^2).
            dist *= dist;
            float alpha_smooth = exp2(-2*dist);

            // Standard wire color
            const float alpha = 1.0;
            dest = lerp(source, vWireColor.xyz, alpha * alpha_smooth);
        }
        
        return dest;
    }
#endregion
#region PixelSource
    // C_WIRED - on/off wireframe
    // C_BLEND - on/off alpha blending 
    // C_DIFFUSE - on/off detailed diffuse texturing
    
    
    float2 blending = sampleTexture(BlendMap, inTexCoord.zw).xy;
    #if C_BLEND != 0
        if (blending.y LESS_THAN 0.05)
            discard;
    #endif

    float noiseA = sampleTexture(NoiseMap, inTexCoord.xy * 0.2134).x;
    float noiseB = sampleTexture(NoiseMap, inTexCoord.xy * 0.05341).x;
    float noiseC = sampleTexture(NoiseMap, inTexCoord.xy * 0.002).x;
    float noiseFactor = lerp(0.75, 1.25, noiseA * noiseB * noiseC);

    float3 cachedColor = sampleTexture(ColorMap, inCacheTexCoord.xy).rgb;
    //float3 diffuseColor = float3(0.423, 0.427, 0.161);
    float3 diffuseColor = cachedColor;
    
    #if C_DIFFUSE != 0
        float3 diffuse0 = sampleTexture(DiffuseMap0, inTexCoord.xy).rgb;
        float3 diffuse1 = sampleTexture(DiffuseMap1, inTexCoord.xy).rgb;
        
        float scaledBlending = lerp(-0.4, 1.4, blending.x);
        float blendFactor = smoothstep(scaledBlending - 0.2, scaledBlending + 0.2, noiseB);
        diffuseColor = lerp(diffuse1, diffuse0, blendFactor);
        
        float mixWithCache = Unlerp(15.0, 30.0, inCacheTexCoord.z);
        diffuseColor = lerp(diffuseColor, cachedColor, mixWithCache);
    #endif
        
    diffuseColor *= noiseFactor;
    
    // Add wire
    float3 resultColor = diffuseColor;
    #if C_WIRED != 0
        resultColor = ComputeColor(resultColor, inEdges);
    #endif
    
    //float3 resultColor = diffuseColor * colorFactor;
    //float3 normalWS = normalize(float3(0.0, 1.0, 0.0) + resultColor * 0.1);
    float3 normalWS = float3(0.0, 1.0, 0.0);
    float4 normal = mul(mView, float4(normalWS, 0.0));
    
    float reflectivity = 0.01;
    float smoothness = 0.3;
    #if C_BLEND != 0
        float scaledAlpha = lerp(-0.2, 1.2, blending.y);
        float alpha = 1.0 - smoothstep(scaledAlpha - 0.2, scaledAlpha + 0.2, noiseB);
        
        outBaseColor.rgb = resultColor;
        outBaseColor.a = alpha;
        outNormal.rgb = normal.xyz * (smoothness * 0.5 + 0.5) * 0.5 + 0.5;
        outNormal.a = alpha;
        outHdrColor.rgb = resultColor * 0.2;
        outHdrColor.a = alpha;
    #else
        outBaseColor.rgb = resultColor;
        outBaseColor.a = reflectivity;
        outNormal.rgb = normal.xyz * (smoothness * 0.5 + 0.5) * 0.5 + 0.5;
        outNormal.a = 1.0;
        outHdrColor.rgb = resultColor * 0.2;
        outHdrColor.a = 1.0;
    #endif
#endregion

#region PixelShaderPasses
    #redefine C_DIFFUSE 1

    #redefine C_BLEND 0
    #pixel C_NAME_BASE_PASS(input = C_PIXEL_INPUT, target = Geometry)
    #   include "ComputeColor"
    #main
    #   include "PixelSource"

    #redefine C_BLEND 1
    #pixel C_NAME_SECOND_PASS(input = C_PIXEL_INPUT, target = Geometry)
    #   include "ComputeColor"
    #main
    #   include "PixelSource"

    #redefine C_BLEND 0
    #redefine C_DIFFUSE 0
    #pixel C_NAME_CACHED_PASS(input = C_PIXEL_INPUT, target = Geometry)
    #   include "ComputeColor"
    #main
    #   include "PixelSource"
#endregion

#redefine C_WIRED 0
#redefine C_NAME_BASE_PASS pixelBasePass
#redefine C_NAME_SECOND_PASS pixelSecondPass
#redefine C_NAME_CACHED_PASS pixelCachedColor
#redefine C_PIXEL_INPUT PixelInput
#include "PixelShaderPasses"

#redefine C_WIRED 1
#redefine C_NAME_BASE_PASS pixelBasePassWired
#redefine C_NAME_SECOND_PASS pixelSecondPassWired
#redefine C_NAME_CACHED_PASS pixelCachedColorWired
#redefine C_PIXEL_INPUT PixelWiredInput
#include "PixelShaderPasses"

#program DetailBasePass(vertex = vertex, pixel = pixelBasePass)
#program DetailSecondPass(vertex = vertex, pixel = pixelSecondPass)
#program CachedColor(vertex = vertex, pixel = pixelCachedColor)

#program DetailBasePassWired(vertex = vertex, geometry = geometryWired, pixel = pixelBasePassWired)
#program DetailSecondPassWired(vertex = vertex, geometry = geometryWired, pixel = pixelSecondPassWired)
#program CachedColorWired(vertex = vertex, geometry = geometryWired, pixel = pixelCachedColorWired)

#program DetailBasePassTess(vertex = vertex, tess = tessellation, pixel = pixelBasePass)
#program DetailSecondPassTess(vertex = vertex, tess = tessellation, pixel = pixelSecondPass)
#program CachedColorTess(vertex = vertex, tess = tessellation, pixel = pixelCachedColor)

#program DetailBasePassWiredTess(vertex = vertex, tess = tessellation, geometry = geometryWired, pixel = pixelBasePassWired)
#program DetailSecondPassWiredTess(vertex = vertex, tess = tessellation, geometry = geometryWired, pixel = pixelSecondPassWired)
#program CachedColorWiredTess(vertex = vertex, tess = tessellation, geometry = geometryWired, pixel = pixelCachedColorWired)
