#constant Size
    Float4 vSize;
#constant Parameters
    Float4[32] vParam;
#resource texture2D(Float4) Source0
#resource texture2D(Float4) Source1
#layout Empty
#output PositionTexture
    Float4 Position;
    Float2 TexCoord;
#target Color
    Color;

#vertex vertexEmpty(layout = Empty, output = PositionTexture)
#region
#main
    float2 positions[4] = @float2{
        float2(1.0, 0.0),
        float2(0.0, 0.0),
        float2(1.0, 1.0),
        float2(0.0, 1.0)
    };
    float2 pos = positions[inVertexID];
    outPosition = float4(textureToScreen(pos), 0.5, 1.0);
    outTexCoord = pos;
#endregion

#pixel pixelMipmapSimple(input = PositionTexture, target = Color)
#region
#main
    float2 texCoord = floor(inTexCoord * vSize.xy - 0.5);
    int2 texel = int2(texCoord.x, texCoord.y);
    float4 c00 = fetchTexture(Source0, texel + int2(0, 0));
    float4 c01 = fetchTexture(Source0, texel + int2(0, 1));
    float4 c10 = fetchTexture(Source0, texel + int2(1, 0));
    float4 c11 = fetchTexture(Source0, texel + int2(1, 1));
    float4 color = (c00 + c01 + c10 + c11) * 0.25;
    outColor = color;
#endregion

#pixel pixelMipmapAlpha(input = PositionTexture, target = Color)
#region
    #include "Math.Random"
    float3 LodColor(float fLod)
    {
        if (fLod < 1.0)
            return float3(1.0, 0.0, 0.0);
        else if (fLod < 2.0)
            return float3(1.0, 0.5, 0.0);
        else if (fLod < 3.0)
            return float3(1.0, 1.0, 0.0);
        else if (fLod < 4.0)
            return float3(0.5, 1.0, 0.0);
        else if (fLod < 5.0)
            return float3(0.0, 1.0, 0.0);
        else if (fLod < 6.0)
            return float3(0.0, 1.0, 0.5);
        else if (fLod < 7.0)
            return float3(0.0, 1.0, 1.0);
        else if (fLod < 8.0)
            return float3(0.0, 0.5, 1.0);
        else if (fLod < 9.0)
            return float3(0.0, 0.0, 1.0);
        else if (fLod < 10.0)
            return float3(0.5, 0.0, 1.0);
        else if (fLod < 11.0)
            return float3(1.0, 0.0, 1.0);
        else 
            return float3(0.0, 0.0, 0.0);
    }
#main
    float2 vTexCoord = floor(inTexCoord * vSize.xy - 0.5);
    int2 vTexel = int2(vTexCoord.x, vTexCoord.y);
    float4 vColor00 = fetchTexture(Source0, vTexel + int2(0, 0));
    float4 vColor01 = fetchTexture(Source0, vTexel + int2(0, 1));
    float4 vColor10 = fetchTexture(Source0, vTexel + int2(1, 0));
    float4 vColor11 = fetchTexture(Source0, vTexel + int2(1, 1));
    
    float4 vColor = (vColor00 + vColor01 + vColor10 + vColor11) * 0.25;
    float fRandom = Random(inTexCoord);
    outColor.rgb = vColor.rgb;
    outColor.a = step(fRandom, vColor.a * 1.02 - 0.01);
#endregion

#pixel pixelHealAlpha(input = PositionTexture, target = Color)
#region
#main
    float cfRadius = vParam[0].x;

    float2 texel = floor(inTexCoord * vSize.xy - 0.5);
    float2 fromTexel = max(float2(0, 0), texel - cfRadius);
    float2 toTexel = min(vSize.xy - 1, texel + cfRadius);
    
    // Do not change alpha when heal
    float fBaseAlpha = fetchTexture(Source0, texel).a;
    float minDist = cfRadius + 1;
    float4 minValue = float4(0.0, 0.0, 0.0, fBaseAlpha);
    
    float2 idx;
    for (idx.x = fromTexel.x; idx.x LEQUAL_THAN toTexel.x; ++idx.x)
    {
        for (idx.y = fromTexel.y; idx.y LEQUAL_THAN toTexel.y; ++idx.y)
        {
            float dist = length(idx - texel);
            float4 value = fetchTexture(Source0, int2(idx.x, idx.y));
            if (value.a GREATEN_THAN 0.01 AND dist LESS_THAN minDist)
            {
                minDist = dist;
                minValue.rgb = value.rgb;
            }
        }
    }
    outColor = minValue;
#endregion

#pixel pixelHealNormalMap(input = PositionTexture, target = Color)
#region
#main
    float cfRadius = vParam[0].x;
    
    float2 texel = floor(inTexCoord * vSize.xy - 0.5);
    float2 fromTexel = max(float2(0, 0), texel - cfRadius);
    float2 toTexel = min(vSize.xy - 1, texel + cfRadius);
    
    float minDist = cfRadius + 1;
    float4 minValue = float4(0.5, 0.5, 1.0, 1.0);
    
    float2 idx;
    for (idx.x = fromTexel.x; idx.x LEQUAL_THAN toTexel.x; ++idx.x)
    {
        for (idx.y = fromTexel.y; idx.y LEQUAL_THAN toTexel.y; ++idx.y)
        {
            float dist = length(idx - texel);
            float4 value = fetchTexture(Source0, int2(idx.x, idx.y));
            if (length(value.rgb) GREATEN_THAN 0.01 AND dist LESS_THAN minDist)
            {
                minDist = dist;
                minValue = value;
            }
        }
    }
    outColor = minValue;
#endregion

#pixel pixelEtchNormalMap(input = PositionTexture, target = Color)
#region
#main
    #define cvHeightScale vParam[0].x

    float2 vTexel = 1.0 / vSize.xy;
    
    // Load maps
    float4 vSourceNormal = sampleTexture(Source0, inTexCoord);
    float4 vHeight0N = sampleTexture(Source1, inTexCoord + vTexel * int2(0, -1));
    float4 vHeightN0 = sampleTexture(Source1, inTexCoord + vTexel * int2(-1, 0));
    float4 vHeight0P = sampleTexture(Source1, inTexCoord + vTexel * int2(0, 1));
    float4 vHeightP0 = sampleTexture(Source1, inTexCoord + vTexel * int2(1, 0));

    // Compute delta
    float3 vX = normalize(float3(2.0, 0.0, cvHeightScale * (vHeightP0.x - vHeightN0.x)));
    float3 vY = normalize(float3(0.0, 2.0, cvHeightScale * (vHeight0P.x - vHeight0N.x)));
    float3 vDetailNormal = normalize(cross(vX, vY));
    
    // Compute new normal
    float3 vNormal = vSourceNormal.rgb * 2.0 - 1.0;
    float3 vBinormal = normalize(cross(vNormal, float3(1.0, 0.0, 0.0)));
    float3 vTangent = normalize(cross(vBinormal, vNormal));
    
    float3 vDestNormal = vDetailNormal.x * vTangent + vDetailNormal.y * vBinormal + vDetailNormal.z * vNormal;
    outColor.rgb = vDestNormal * 0.5 + 0.5;
    outColor.a = 1.0;
#endregion

#program Simple(vertex = vertexEmpty, pixel = pixelMipmapSimple)
#program Alpha(vertex = vertexEmpty, pixel = pixelMipmapAlpha)
#program HealAlpha(vertex = vertexEmpty, pixel = pixelHealAlpha)
#program HealNormalMap(vertex = vertexEmpty, pixel = pixelHealNormalMap)
#program EtchNormalMap(vertex = vertexEmpty, pixel = pixelEtchNormalMap)
