#region Generate.Base
    /// Test if pixel is outside of area [-0.5, 0]-[0.5, 1]
    bool IsOutside(float2 vPixel)
    {
        return vPixel.y < 0.0 || vPixel.y > 1.0 || abs(vPixel.x) > 0.5;
    }
    /// Test if pixel is outside of area [-0.5+eps, eps]-[0.5-eps, 1-eps]
    bool IsOutside(float2 vPixel, float fEps)
    {
        return vPixel.y < fEps || vPixel.y > 1.0 - fEps || abs(vPixel.x) > 0.5 - fEps;
    }
#endregion
#region Generate.SinCurve
    /// Compute sin wave
    float MultiSinWave(float x, float2 fFreq)
    {
        float2 v = cos(x * fFreq) - 1.0;
        return v.x + v.y;
    }
    /// Compute attenuated wave
    float AttenSinWave(float x, float4 vParam)
    {
        #define P_FREQ  vParam.xy    ///< Frequences
        #define P_MAG   vParam.zw    ///< Magnitudes
        
        float k = lerp(P_MAG.x, P_MAG.y, x);
        return k * MultiSinWave(x, P_FREQ);
        
        #undef P_FREQ
        #undef P_MAG
    }
    /// Draw sinusoidal curve
    float DrawSinCurve(in float2 vPixel, in float4 vParam[2])
    {
        if (IsOutside(vPixel)) return 0.0;
            
        #define P_WAVE_INFO         vParam[0]    ///< Parameters for AttenSinWave
        #define P_INV_WIDTH_BEGIN   vParam[1].x  ///< Begin width
        #define P_INV_WIDTH_END     vParam[1].y  ///< End width
        #define P_ATTEN             vParam[1].z  ///< Attenuation percent
                    
        // compute distance
        float fDistance = abs(vPixel.x - AttenSinWave(vPixel.y, P_WAVE_INFO));
        
        // compute width
        float fAtten = Unlerp(1.0 * P_ATTEN, 1.0, vPixel.y);
        float fWidthInv = lerp(P_INV_WIDTH_BEGIN, P_INV_WIDTH_END, vPixel.y);
        
        return max(0.0, 1.0 - fDistance * fWidthInv - fAtten * fAtten);
        
        #undef P_WAVE_INFO
        #undef P_INV_WIDTH_BEGIN
        #undef P_INV_WIDTH_END
        #undef P_ATTEN
    }
#endregion
#region Generate.PolyShape
    /// Draw polynomial shape
    float DrawPolyShape(in float2 vPixel, in float4 vParam)
    {
        if (IsOutside(vPixel)) return 0.0;
        
        // P_POW_1 < P_POW_2
        #define P_POW_1         vParam.x ///< Positive power of poly
        #define P_POW_2         vParam.y ///< Negative power of poly
        #define P_POW_BEGIN     vParam.z ///< Power to modify intensity at the begin
        #define P_BEGIN_WIDTH   vParam.w ///< Width at the begin
        
        float xx = abs(vPixel.x * 2.0) - P_BEGIN_WIDTH * (1.0 - vPixel.y);
        float v = pow(vPixel.y, P_POW_1) - pow(vPixel.y, P_POW_2) - xx;
        float ym = pow(P_POW_1 / P_POW_2, 1 / (P_POW_2 - P_POW_1));
        float xm = pow(ym, P_POW_1) - pow(ym, P_POW_2);
        float p = lerp(P_POW_BEGIN, 1.0, vPixel.y);
        
        return max(0.0, v) / (xm * p);
        
        #undef P_POW_1
        #undef P_POW_2
        #undef P_POW_BEGIN
        #undef P_BEGIN_WIDTH
    }
#endregion

#region Generate.SegmentedLeafA
    float DrawSegmentedLeafA(in float2 vPixel, in float4 vParam[3])
    {
        if (IsOutside(vPixel)) return 0.0;
        
        #define P_NUM_SEGMENTS  vParam[0].x  ///< Number of segments
        #define P_WIDTH_SCALE   vParam[0].y  ///< Leaf segments scale
        #define P_BEGIN_ANGLE   vParam[0].z  ///< Segments angle at leaf begin
        #define P_END_ANGLE     vParam[0].w  ///< Segments angle at leaf end
        
        // P_POW_1 < P_POW_2
        #define P_BEGIN_POS     vParam[1].x  ///< Begin position
        #define P_END_POS       vParam[1].y  ///< End position
        #define P_POW_1         vParam[1].z  ///< Positive power for length computing
        #define P_POW_2         vParam[1].w  ///< Negative power for length computing
        
        #define P_SHAPE         vParam[2]    ///< Leaf shape parameters. Default: float4(1.0, 10.0, 0.1, 1.0)
        
        float fLengthScaleY = pow(P_POW_1 / P_POW_2, 1 / (P_POW_2 - P_POW_1));
        float fLengthScale = pow(fLengthScaleY, P_POW_1) - pow(fLengthScaleY, P_POW_2);
        
        float color = 0.0;
        for (float idx = 0; idx < P_NUM_SEGMENTS; ++idx)
        {
            float factor = (idx + 0.5) / P_NUM_SEGMENTS;
            float2 position = float2(0.0, lerp(P_BEGIN_POS, P_END_POS, factor));
            float angle = lerp(P_BEGIN_ANGLE, P_END_ANGLE, factor);
            float lenFactor = 1.0 - position.y / P_END_POS;
            float len = (pow(lenFactor, P_POW_1) - pow(lenFactor, P_POW_2)) / fLengthScale;
            float2 scale = float2(P_WIDTH_SCALE / P_NUM_SEGMENTS, 0.5);
            
            if (vPixel.x < 0.0)
            {
                float2 px1 = UnTransform(vPixel, position, float2(0.0, (len - 1.0) * 0.5), MakeRotation(angle), scale);
                color = max(color, DrawPolyShape(px1, P_SHAPE));
            }
            if (vPixel.x > 0.0)
            {
                float2 px2 = UnTransform(vPixel, position, float2(0.0, (len - 1.0) * 0.5), MakeRotation(-angle), scale);
                color = max(color, DrawPolyShape(px2, P_SHAPE));
            }
        }
        
        return color;
        
        #undef P_NUM_SEGMENTS
        #undef P_WIDTH_SCALE
        #undef P_BEGIN_ANGLE
        #undef P_END_ANGLE
        
        #undef P_BEGIN_POS
        #undef P_END_POS
        #undef P_POW_1
        #undef P_POW_2
        
        #undef P_SHAPE
    }
    float DrawSegmentedRibsA(in float2 vPixel, in float4 vParam[4])
    {
        if (IsOutside(vPixel)) return 0.0;
        
        // P_LENGTH_POW_1 < P_LENGTH_POW_2
        #define P_NUM_RIBS      vParam[0].x  ///< Number of nested ribs
        #define P_LENGTH        vParam[0].y  ///< Nested ribs length multiplier
        #define P_LENGTH_POW_1  vParam[0].z  ///< Positive power for length computing
        #define P_LENGTH_POW_2  vParam[0].w  ///< Negative power for length computing

        #define P_PHASE         vParam[1].x  ///< Ribs right to left phase offset
        #define P_BEGIN         vParam[1].y  ///< Begin of nested ribs
        #define P_END           vParam[1].z  ///< End of nested ribs
        #define P_ATTEN         vParam[1].w  ///< Attenuation

        #define P_WIDTH_BEGIN   vParam[2].x  ///< Width of main rib begin
        #define P_WIDTH_END     vParam[2].y  ///< Width of main rib end
        #define P_WIDTH_SCALE   vParam[2].z  ///< Width scale of nested ribs
        #define P_CURV          vParam[2].w  ///< Curvature
        
        #define P_ANGLE_BEGIN   vParam[3].x  ///< Nested ribs begin angle
        #define P_ANGLE_END     vParam[3].y  ///< Nested ribs begin angle
        
        float4 pp[2];
        pp[0] = float4(1.0, 1.0, 0.0, 0.0);
        pp[1] = float4(1.0 / P_WIDTH_BEGIN, 1.0 / P_WIDTH_END, P_ATTEN, 0);
        float color = DrawSinCurve(vPixel, pp);
        
        float orientation = 1;
        for (float idx = 0; idx < P_NUM_RIBS; ++idx)
        {
            float factor = (idx + 0.5 + (orientation < 0 ? P_PHASE : 0.0)) / P_NUM_RIBS;
            float2 position = float2(0.0, lerp(P_BEGIN, P_END, factor));
            float angle = lerp(P_ANGLE_BEGIN, P_ANGLE_END, position.y) * orientation;
            float lenFactor = 1.0 - position.y;
            float len = (pow(lenFactor, P_LENGTH_POW_1) - pow(lenFactor, P_LENGTH_POW_2)) * P_LENGTH;
            float width = P_WIDTH_BEGIN / lerp(P_WIDTH_BEGIN, P_WIDTH_END, position.y) / P_WIDTH_SCALE;
            
            float2 px = UnTransform(vPixel, position, 0.0, MakeRotation(angle), float2(1.0, len));
            pp[0] = float4(0.8, 0.5, P_CURV, P_CURV) * orientation;
            pp[1] = float4(width / P_WIDTH_BEGIN, width / P_WIDTH_END, P_ATTEN, 0);
            color = max(color, DrawSinCurve(px, pp));
            
            orientation *= -1;
        }
        
        return color;
        
        #undef P_NUM_SEGMENTS
        #undef P_LENGTH
        #undef P_LENGTH_POW_1
        #undef P_LENGTH_POW_2
        
        #undef P_PHASE
        #undef P_BEGIN
        #undef P_END
        #undef P_ATTEN
        
        #undef P_WIDTH_BEGIN
        #undef P_WIDTH_END
        #undef P_WIDTH_SCALE
        #undef P_ANGLE
        
        #undef P_ANGLE_BEGIN
        #undef P_ANGLE_END
    }
#endregion

#region Generate.LeavesStripA
    /// Draw leaf strip
    /// I_DRAW_LEAF - function to draw leaf
    /// I_DRAW_LEAF_COUNT - number of its parameters
    /// I_DRAW_RIBS - function to draw leaf
    /// I_DRAW_RIBS_COUNT - number of its parameters
    void DrawLeavesStripA(
        out float3 ovClasses, out float2 ovLocalPixel, 
        float fSeed, float2 vPixel, float2 vScale, float4 vParam[2], 
        float4 vLeafParam[I_DRAW_LEAF_COUNT], 
        float4 vRibsParam[I_DRAW_RIBS_COUNT])
    {
        // Skip if outside
        ovClasses = 0.0;
        ovLocalPixel = 0.0;
        if (IsOutside(vPixel)) return;
        
        // Parameters
        #define P_NUM           vParam[0].x  ///< Number of leaves
        #define P_POS_NOISE     vParam[0].y  ///< Randomness of leaves positions
        #define P_ANGLE         vParam[0].z  ///< Leaf angle
        #define P_ANGLE_NOISE   vParam[0].w  ///< Leaf angle noise
        
        #define P_LEAF_SIZE     vParam[1].xy ///< Leaf size
        #define P_RIBS_SIZE     vParam[1].zw ///< Ribs size
        
        // Generate leaves
        float fSize = MaxOf(P_LEAF_SIZE);
        for (int i = 0; i < P_NUM; ++i)
        {
            float4 vNoise = Random4(Random(i) + fSeed);
            
            // Compute position
            float2 vPosition = float2(0.0, lerp(fSize / 2, 1.0 - fSize, i / (P_NUM - 1.0))) + (vNoise.xy * 2.0 - 1.0) * P_POS_NOISE;
            
            // Compute rotation
            float fSign = (i + 1 == P_NUM) ? 0.0 : (i % 2 - 0.5) * 2.0;
            float fAngle = fSign * P_ANGLE + P_ANGLE_NOISE * (vNoise.z * 2.0 - 1.0);
            float2 vRotation = MakeRotation(fAngle);
                        
            // Render
            float2 vLocalPixel = UnTransform(vPixel * vScale, vPosition * vScale, 0.0, vRotation, P_LEAF_SIZE);
            float fLeafFactor = I_DRAW_LEAF(vLocalPixel, vLeafParam);
            float fRibFactor = I_DRAW_RIBS(vLocalPixel * P_RIBS_SIZE, vRibsParam);
            if (fLeafFactor > 0.05)
            {
                ovClasses.x = min(1.0, fRibFactor);
                ovClasses.y = min(1.0, fLeafFactor);
                ovClasses.z = vNoise.w;
                ovLocalPixel = vLocalPixel;
            }
        }
        
        #undef P_NUM
        #undef P_POS_NOISE
        #undef P_ANGLE
        #undef P_ANGLE_NOISE
        
        #undef P_LEAF_SIZE
        #undef P_RIBS_SIZE
    }
#endregion
#region Generate.RadialCloudA
    struct InstanceRadialCloudParam
    {
        float cfStep;
        float cfPosNoise;
        float cfMinAngle;
        float cfMaxAngle;
        float2 cvSize;
        float2 cvRotCenter;
        float cfBeginDensity;
        float cfEndDensity;
        float2 cvPerlin;
    };
    bool ComputeInstanceRadialCloud(
        uint iInstance, InstanceRadialCloudParam sParam, float fSeed,
        out float2 ovLocation, out float2 ovRotation, out float ofClass)
    {
        ovLocation = 0.0;
        ovRotation = 0.0;
        ofClass = 0.0;
        
        // Compute base location
        int iStride = int(1.0 / sParam.cfStep);
        ovLocation = float2((iInstance % iStride + 0.5) / iStride, (iInstance / iStride + 0.5) / iStride);
        if (ovLocation.y >= 1.0)
            return false;
        
        // Generate noise
        float4 vNoise = Random4(Random(Random(ovLocation.x) + ovLocation.y) + fSeed);
        
        // Compute position
        ovLocation += float2(-0.5, 0.0) + (vNoise.xy * 2.0 - 1.0) * sParam.cfPosNoise;
        float2 vDirection = SafeNormalizeX(ovLocation - sParam.cvRotCenter);
        
        // Compute angle
        float fAngle = lerp(sParam.cfMinAngle, sParam.cfMaxAngle, vNoise.z);
        ovRotation = Rotate(MakeRotation(fAngle), float2(vDirection.y, -vDirection.x));
        float2 vCenter = ovLocation + float2(-ovRotation.y, ovRotation.x) * sParam.cvSize / 2.0;
        if (IsOutside(vCenter, MaxOf(sParam.cvSize)))
            return false;
        
        // Skip by distance
        float fDistanceFactor = 2.0 * length(ovLocation - float2(0.0, 0.5));
        if (fDistanceFactor > 1.0)
           return false;
        
        // Add some perlin
        float fDensity1 = 1.0 - PerlinNoise2D((ovLocation + fSeed) * sParam.cvPerlin, float2(1.0, 1.0) * sParam.cvPerlin);
        float fDensity2 = lerp(sParam.cfBeginDensity, sParam.cfEndDensity, fDistanceFactor);
        float fDensity = fDensity1 * fDensity2;
        if (vNoise.w >= fDensity)
            return false;
        
        // Render
        ofClass = vNoise.w;
        ovLocation -= float2(-0.5, 0.0);
        return true;
    }
#endregion
#region Generate.LinearStripA
    struct InstanceLinearStripParam
    {
        float2 cvLocation;
        float cfStep;
        float cfLocationNoise;
        float2 cvWidth;
        float2 cvHeight;
        float cfAngleNoise;
    };
    bool ComputeInstanceLinearStrip(
        uint iInstance, InstanceLinearStripParam sParam, float fSeed,
        out float2 ovLocation, out float2 ovRotation, out float2 ovSize, out float ofClass)
    {
        ovLocation = 0.0;
        ovRotation = 0.0;
        ovSize = 0.0;
        ofClass = 0.0;
        
        // Limit instances
        int iCount = int(1.0 / sParam.cfStep);
        if (iInstance >= iCount)
            return false;
        
        // Compute location
        ovLocation = float2(lerp(sParam.cvLocation.x, sParam.cvLocation.y, (iInstance + 0.5) / iCount), 0.0);
        
        // Generate noise
        float4 vNoise = Random4(Random(ovLocation.x) + fSeed);

        // Randomize location
        ovLocation.x -= sParam.cfLocationNoise * vNoise.w;
        
        // Compute rotation
        ovRotation = MakeRotation(sParam.cfAngleNoise * vNoise.x);
        
        // Compute size
        ovSize.x = lerp(sParam.cvWidth.x, sParam.cvWidth.y, vNoise.x);
        ovSize.y = lerp(sParam.cvHeight.x, sParam.cvHeight.y, vNoise.y);
        
        // Compute class
        ofClass = vNoise.z;

        return true;
    }
#endregion
