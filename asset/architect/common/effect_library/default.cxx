#region .Default
    // Concatenations
    #define CONCAT2_NX(X, Y) X##Y
    #define CONCAT3_NX(X, Y, Z) X##Y##Z
    #define CONCAT2(X, Y) CONCAT2_NX(X, Y)
    #define CONCAT3(X, Y, Z) CONCAT3_NX(X, Y, Z)
    
    // TODO: Remove legacy
    #define AND &&
    #define OR ||
    #define LESS_THAN <
    #define LEQUAL_THAN <=
    #define GREATEN_THAN >
    #define GEQUAL_THAN >=
    
    #define PI 3.1415926535897932384626433832795
    #define INF 2147483647
    
    // define functions
    #if defined(HLSL30)
        #define fetchTexture(S, V) S.Load(int3(V, 0))
        #define fetchTextureLod(S, V, L) S.Load(int3(V, L))
        #define sampleTexture(S, V) S.Sample(_##S##_sampler, V)
        #define sampleTextureLod(S, V, L) S.SampleLevel(_##S##_sampler, V, L)
        #define sampleShadow(S, V, D) S.SampleCmpLevelZero(_##S##_sampler, V, D)
        #define sampleShadowOffset(S, V, L, O) S.SampleCmpLevelZero(_##S##_sampler, V, L, O)

        #define textureToScreen(V) ((V) * float2(2.0, -2.0) + float2(-1.0, 1.0))
        #define screenToTexture(V) ((V) * float2(0.5, -0.5) + 0.5)
    #else
        #error Not supported
    #endif

    #define Unlerp(vFrom, vTo, vVector) clamp((vVector - vFrom) / (vTo - vFrom), 0.0, 1.0)
    
    /// @name Encode/decode normal to float2
    /// @{
    float2 EncodeNormal(in float3 vNormal) 
    { 
        return vNormal.xy / sqrt(-8.0 * vNormal.z + 8.0) + 0.5; 
    }
    float3 DecodeNormal(in float2 vNormal) 
    { 
        float2 fenc = 4.0 * vNormal - 2.0; 
        float f = dot(fenc, fenc);
        return float3(fenc * sqrt(max(1.0 - 0.25 * f, 0.0)), 0.5 * f - 1.0); 
    }
    /// @}
    
    /// @name Normalize vectors
    /// @{
    float2 SafeNormalize(float2 vVector)
    {
        float dp2 = max(0.001, dot(vVector, vVector));
        return vVector * rsqrt(dp2);
    }
    float3 SafeNormalize(float3 vVector)
    {
        float dp3 = max(0.001, dot(vVector, vVector));
        return vVector * rsqrt(dp3);
    }
    float2 SafeNormalize0(float2 vVector)
    {
        float dp2 = dot(vVector, vVector);
        return dp2 > 0.00001 ? vVector * rsqrt(dp2) : 0.0;
    }
    float3 SafeNormalize0(float3 vVector)
    {
        float dp3 = dot(vVector, vVector);
        return dp3 > 0.00001 ? vVector * rsqrt(dp3) : 0.0;
    }
    float2 SafeNormalizeX(float2 vVector)
    {
        float dp2 = dot(vVector, vVector);
        return dp2 > 0.00001 ? vVector * rsqrt(dp2) : float2(1.0, 0.0);
    }
    float3 SafeNormalizeX(float3 vVector)
    {
        float dp3 = dot(vVector, vVector);
        return dp3 > 0.00001 ? vVector * rsqrt(dp3) : float3(1.0, 0.0, 0.0);
    }
    /// @}
    
    /// Noise interpolation
    float4 NoiseLerp(float4 vSource0, float4 vSource1, float fNoise, float fFactor, float fSmooth)
    {
        float fScaledFactor = lerp(-2*fSmooth, 1.0 + 2*fSmooth, fFactor);
        float fBlendFactor = smoothstep(fScaledFactor - fSmooth, fScaledFactor + fSmooth, fNoise);
        return lerp(vSource1, vSource0, fBlendFactor);
    }
#endregion
