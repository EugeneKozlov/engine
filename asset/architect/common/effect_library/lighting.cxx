#region Lighting.PBR
    #define PI_DIV_4 (PI / 4)
    #define SQRT_HALF_PI sqrt(2 / PI)
    #define HALF_DIV_PI 1 / (2 * PI)
    
    struct LightDesc
    {
        float3 color;
        float3 dir;
        float  ndotl; // TODO remove
    };
    
    struct IndirectLightDesc
    {
        float3 diffuse;
        float3 specular;
    };
    
    float Pow5(float x)
    {
        return x*x * x*x * x;
    }
    
    float DotClamped(float3 a, float3 b)
    {
        return max(0.0, dot(a, b));
    }
    
    float BlinnTerm(float3 vNormal, float3 vHalfDir)
    {
        return DotClamped(vNormal, vHalfDir);
    }
    
    float3 FresnelTerm(float3 F0, float cosA)
    {
        float t = Pow5(1 - cosA);	// ala Schlick interpoliation
        return F0 + (1-F0) * t;
    }
    
    float3 FresnelLerp(float3 F0, float3 F90, float cosA)
    {
        float t = Pow5(1 - cosA);	// ala Schlick interpoliation
        return lerp(F0, F90, t);
    }

    // Generic Smith-Schlick visibility term
    float SmithVisibilityTerm(float NdotL, float NdotV, float k)
    {
        float gL = NdotL * (1 - k) + k;
        float gV = NdotV * (1 - k) + k;
        return 1.0 / (gL * gV + 1e-4f);
    }

    // Smith-Schlick derived for Beckmann
    float SmithBeckmannVisibilityTerm(float NdotL, float NdotV, float roughness)
    {
        // c = sqrt(2 / Pi)
        float c = SQRT_HALF_PI;
        float k = roughness * roughness * c;
        return SmithVisibilityTerm(NdotL, NdotV, k);
    }

    // Smith-Schlick derived for GGX 
    float SmithGGXVisibilityTerm(float NdotL, float NdotV, float roughness)
    {
        float k = (roughness * roughness) / 2;
        return SmithVisibilityTerm(NdotL, NdotV, k);
    }
    
    float RoughnessToSpecPower(float roughness)
    {
    #if 1
        // from https://s3.amazonaws.com/docs.knaldtech.com/knald/1.0.0/lys_power_drops.html
        float n = 10.0 / log2((1-roughness)*0.968 + 0.03);
        return n * n;

        // NOTE: another approximate approach to match Marmoset gloss curve is to
        // multiply roughness by 0.7599 in the code below (makes SpecPower range 4..N instead of 1..N)
    #else
        float m = roughness * roughness * roughness + 1e-4f;	// follow the same curve as unity_SpecCube
        float n = (2.0 / m) - 2.0;							// http://jbit.net/%7Esparky/academic/mm_brdf.pdf
        n = max(n, 1e-4f);									// prevent possible cases of pow(0,0), which could happen when roughness is 1.0 and NdotH is zero
        return n;
    #endif
    }

    // BlinnPhong normalized as normal distribution function (NDF)
    // for use in micro-facet model: spec=D*G*F
    // http://www.thetenthplanet.de/archives/255
    float NDFBlinnPhongNormalizedTerm(float NdotH, float n)
    {
        // norm = (n+1)/(2*pi)
        float normTerm = (n + 1.0) * HALF_DIV_PI;

        float specTerm = pow(NdotH, n);
        return specTerm * normTerm;
    }

    // BlinnPhong normalized as reflec�tion den�sity func�tion (RDF)
    // ready for use directly as specular: spec=D
    // http://www.thetenthplanet.de/archives/255
    float RDFBlinnPhongNormalizedTerm(float NdotH, float n)
    {
        float normTerm = (n + 2.0) / (8.0 * PI);
        float specTerm = pow(NdotH, n);
        return specTerm * normTerm;
    }

    float GGXTerm (float NdotH, float roughness)
    {
        float a = roughness * roughness;
        float a2 = a * a;
        float d = NdotH * NdotH * (a2 - 1.f) + 1.f;
        return a2 / (PI * d * d);
    }
    
    float4 ComputePhysicalBased(
        float3 diffColor, 
        float3 specColor, 
        float oneMinusReflectivity, 
        float oneMinusRoughness,
        float3 normal, 
        float3 viewDir,
        LightDesc light, 
        IndirectLightDesc gi)
    {
        float roughness = 1 - oneMinusRoughness;
        float3 halfDir = SafeNormalize(light.dir + viewDir);

        float nl = light.ndotl; 
        float nh = BlinnTerm(normal, halfDir);
        float nv = DotClamped(normal, viewDir);
        float lv = DotClamped(light.dir, viewDir);
        float lh = DotClamped(light.dir, halfDir);

    #if 1
        float V = SmithGGXVisibilityTerm(nl, nv, roughness);
        float D = GGXTerm(nh, roughness);
    #else
        float V = SmithBeckmannVisibilityTerm(nl, nv, roughness);
        float D = NDFBlinnPhongNormalizedTerm(nh, RoughnessToSpecPower (roughness));
    #endif

        float nlPow5 = Pow5(1-nl);
        float nvPow5 = Pow5(1-nv);
        float Fd90 = 0.5 + 2 * lh * lh * roughness;
        float disneyDiffuse = (1 + (Fd90-1) * nlPow5) * (1 + (Fd90-1) * nvPow5);

        float specularTerm = max(0, (V * D * nl) / 4);// Torrance-Sparrow model, Fresnel is applied later (for optimization reasons)
        float diffuseTerm = disneyDiffuse * nl / PI;
        
        float grazingTerm = saturate(oneMinusRoughness + (1-oneMinusReflectivity));
        float3 color =	diffColor * (gi.diffuse + light.color * diffuseTerm)
                        + specularTerm * light.color * FresnelTerm (specColor, lh)
                        + gi.specular * FresnelLerp (specColor, grazingTerm, nv);

        return float4(color, 1);
    }
#endregion	
