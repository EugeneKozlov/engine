#region Auto.Quad
    float2 GetQuadVertex(float2 vBegin, float2 vEnd, uint iVertexId)
    {
        float2 vVertices[4] = @float2{
            float2(vEnd.x, vBegin.y),
            float2(vBegin.x, vBegin.y),
            float2(vEnd.x, vEnd.y),
            float2(vBegin.x, vEnd.y)
        };
        return vVertices[iVertexId];
    }
#endregion
#region Auto.Cube
    float3 GetCubeVertex(uint iVertexId)
    {
        float3 vVertices[8] = @float3{
			float3(-1.0, -1.0, -1.0), 
			float3(1.0, -1.0, -1.0), 
			float3(-1.0, -1.0, 1.0), 
			float3(1.0, -1.0, 1.0), 
			float3(-1.0, 1.0, -1.0),
			float3(1.0, 1.0, -1.0), 
			float3(-1.0, 1.0, 1.0), 
			float3(1.0, 1.0, 1.0), 
        };
        uint vIndices[36] = @uint{
            0, 1, 3, 0, 3, 2,
            4, 7, 5, 4, 6, 7, 
            0, 5, 1, 0, 4, 5, 
            2, 3, 7, 2, 7, 6,
            0, 2, 6, 0, 6, 4,
            1, 7, 3, 1, 5, 7 
        };

        return vVertices[vIndices[iVertexId]];
    }
#endregion
