#region Math.Common
    /// Create 2D rotation
    float2 MakeRotation(float fAngle)
    {
        float a = radians(fAngle);
        return float2(cos(a), sin(a));
    }
    /// Rotate vector
    float2 Rotate(float2 v, float2 r)
    {
        return float2(v.x * r.x - v.y * r.y, v.y * r.x + v.x * r.y);
    }
    /// Undo vector rotation
    float2 UnRotate(float2 v, float2 r)
    {
        return float2(v.x * r.x + v.y * r.y, v.y * r.x - v.x * r.y);
    }
    /// Apply transform
    float2 Transform(float2 vVector, float2 vPosition, float2 vOffset, float2 vRotation, float2 vScale)
    {
        return Rotate(vVector * vScale + vOffset, vRotation) + vPosition;
    }
    /// Undo transform
    float2 UnTransform(float2 vVector, float2 vPosition, float2 vOffset, float2 vRotation, float2 vScale)
    {
        return (UnRotate(vVector - vPosition, vRotation) - vOffset) / vScale;
    }
    /// Create random normal
    float3 RandomNormal(float2 vNoise)
    {
        float3 vNormal;
        vNormal.xy = vNoise;
        vNormal.z = sqrt(max(0.0, 1.0 - dot(vNormal.xy, vNormal.xy)));
        return normalize(vNormal);
    }
    /// Make some value fade [0,1] more sharp
    float4 Sharpen(float4 vSource, int iDepth)
    {
        float4 vRet = vSource;
        for (int i = 0; i < iDepth; ++i)
        {
            vRet = smoothstep(0.0, 1.0, vRet);
        }
        return vRet;
    }
    /// Get max of 2 elements
    float MaxOf(float2 v)
    {
        return max(v.x, v.y);
    }
    /// Get max of 3 elements
    float MaxOf(float3 v)
    {
        return max(v.x, max(v.y, v.z));
    }
    /// Get max of 4 elements
    float MaxOf(float4 v)
    {
        return max(max(v.x, v.y), max(v.z, v.w));
    }
#endregion

#region Math.Random
    float Random(in float2 uv)
    {
        float2 noise = (frac(sin(dot(uv, float2(12.9898, 78.233) * 2.0)) * 43758.5453));
        return abs(noise.x + noise.y) * 0.5;
    }
    float Random(in float x)
    {
        return Random(float2(x, x + 1.0));
    }
    float2 Random2(in float x) 
    { 
        return float2(Random(x + 1.0), Random(x + 2.0)); 
    }
    float3 Random3(in float x) 
    { 
        return float3(Random(x + 1.0), Random(x + 2.0), Random(x + 3.0)); 
    }
    float4 Random4(in float x) 
    { 
        return float4(Random(x + 1.0), Random(x + 2.0), Random(x + 3.0), Random(x + 4.0)); 
    }
#endregion

#region Math.Perlin2D
    float4 Mod289(float4 x)
    {
        return x - floor(x * (1.0 / 289.0)) * 289.0;
    }
    float4 Permute(float4 x)
    {
        return Mod289(((x*34.0)+1.0)*x);
    }
    float4 TaylorInvSqrt(float4 r)
    {
        return 1.79284291400159 - 0.85373472095314 * r;
    }
    float2 Fade(float2 t) 
    {
        return t*t*t*(t*(t*6.0-15.0)+10.0);
    }
    float PerlinNoise2D(float2 P, float2 rep)
    {
        float4 Pi = floor(P.xyxy) + float4(0.0, 0.0, 1.0, 1.0);
        float4 Pf = frac(P.xyxy) - float4(0.0, 0.0, 1.0, 1.0);
        Pi = fmod(Pi, rep.xyxy);
        Pi = Mod289(Pi);
        float4 ix = Pi.xzxz;
        float4 iy = Pi.yyww;
        float4 fx = Pf.xzxz;
        float4 fy = Pf.yyww;

        float4 i = Permute(Permute(ix) + iy);

        float4 gx = frac(i * (1.0 / 41.0)) * 2.0 - 1.0 ;
        float4 gy = abs(gx) - 0.5 ;
        float4 tx = floor(gx + 0.5);
        gx = gx - tx;

        float2 g00 = float2(gx.x,gy.x);
        float2 g10 = float2(gx.y,gy.y);
        float2 g01 = float2(gx.z,gy.z);
        float2 g11 = float2(gx.w,gy.w);

        float4 norm = TaylorInvSqrt(float4(dot(g00, g00), dot(g01, g01), dot(g10, g10), dot(g11, g11)));
        g00 *= norm.x;  
        g01 *= norm.y;  
        g10 *= norm.z;  
        g11 *= norm.w;  

        float n00 = dot(g00, float2(fx.x, fy.x));
        float n10 = dot(g10, float2(fx.y, fy.y));
        float n01 = dot(g01, float2(fx.z, fy.z));
        float n11 = dot(g11, float2(fx.w, fy.w));

        float2 fade_xy = Fade(Pf.xy);
        float2 n_x = lerp(float2(n00, n01), float2(n10, n11), fade_xy.x);
        float n_xy = lerp(n_x.x, n_x.y, fade_xy.y);
        return 2.3 * n_xy;
    }
    float PerlinNoise2Dx4(float2 vPixel, float2 vScale, float4 vOct, float4 vMag)
    {
        float4 vNoise;
        vNoise.x = PerlinNoise2D(vPixel * vScale * vOct.x, vOct.x * vScale);
        vNoise.y = PerlinNoise2D(vPixel * vScale * vOct.y, vOct.y * vScale);
        vNoise.z = PerlinNoise2D(vPixel * vScale * vOct.z, vOct.z * vScale);
        vNoise.w = PerlinNoise2D(vPixel * vScale * vOct.w, vOct.w * vScale);
        float fMax = dot(vMag, 1.0);
        return dot(0.5 + 0.5 * vNoise, vMag) / fMax;
    }
#endregion

#region Math.Wave
    float4 CubicSmooth(float4 vData)
    {
        return vData * vData * (3.0 - 2.0 * vData);
    }
    float4 TriangleWave(float4 vData)
    {
        return abs((frac(vData + 0.5) * 2.0) - 1.0);
    }
    float4 SmoothTriangleWave(float4 vData)
    {
        return (CubicSmooth(TriangleWave(vData)) - 0.5) * 2.0;
    }
#endregion
