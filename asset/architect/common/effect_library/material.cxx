#region Material.Wind
    float3 GlobalWind(
        float3 vModelPosition, 
        float3 vPosition,
        float3 vNormal,
        float fMainAdherence,
        float fBranchAdherence,
        float fWindMain,
        float fWindPulseMagnitude,
        float fWindPulseFrequency,
        float fBranchPhase,
        float fTime, 
        float3 vWindDirection)
    {
        // Preserve shape
        float fPositionLength = length(vPosition);
                        
        // Primary oscillation
        float2 vPhases = float2(fBranchPhase, 0.0);
        float2 fScaledTime = fTime * fWindPulseFrequency * float2(1.0, 0.8);
        float4 vOscillations = SmoothTriangleWave(vModelPosition.xyxy + fScaledTime.xyxy + vPhases.xxyy);
        float2 vOsc = vOscillations.xz + vOscillations.yw * vOscillations.yw;
        float2 vPulse = vOsc * fWindPulseMagnitude * 0.5;
        float fMoveAmount = vPulse.x * fBranchAdherence + (fWindMain + vPulse.y) * fMainAdherence;

        // Move xz component
        vPosition.xz += vWindDirection.xz * fMoveAmount;
        
        // Restore position
        return normalize(vPosition) * fPositionLength;
    }
    float3 BranchTurbulenceWind(
        float3 vModelPosition, 
        float3 vModelUp,
        float3 vPosition,
        float fTurbulence,
        float fFrequency,
        float fBranchPhase,
        float fTime,
        float3 vWindDirection)
    {
        float fObjectPhase = dot(vModelPosition, 1.0);
        float4 vWavesIn = fTime * fFrequency + fObjectPhase + fBranchPhase;
        float4 vWaves = SmoothTriangleWave(vWavesIn) * 0.5 + 0.5;
        
        vPosition += vModelUp * vWaves.x * fTurbulence;
        //vPosition += vWindDirection * vWaves.x * fTurbulence;
        return vPosition;
    }
    float3 FrondEdgeWind(
        float3 vModelPosition, 
        float3 vPosition,
        float3 vNormal,
        float fEdgeMagnitude,
        float fEdgeFrequency,
        float fTime)
    {
        float fObjectPhase = dot(vModelPosition, 1.0);
        float fVertexPhase = dot(vPosition, 1.0);
        const float4 vFreq = float4(1.575, 0.793, 0.375, 0.193);
        float4 vWavesIn = vFreq * fTime * fEdgeFrequency + fObjectPhase + fVertexPhase;
        float4 vWaves = SmoothTriangleWave(vWavesIn);
        
        vPosition += vNormal * dot(vWaves, float4(1.0, 0.5, 0.25, 0.125)) * fEdgeMagnitude;
        return vPosition;
    }
#endregion

#region Material.DeclareHeader
    #define VS_NAME(NAME) CONCAT2(NAME, _VertexShader)
    #define GS_NAME(NAME) CONCAT2(NAME, _GeometryShader)
    #define PS_NAME(NAME) CONCAT2(NAME, _PixelShader)
    #define PS_INPUT(NAME) CONCAT2(NAME, _PixelInput)

    #constant GlobalUniformBuffer
        Float4x4 mView;
        Float4x4 mProjection;
        Float4 vCameraPosition;
        Float4 vTime;
        Float4 vAmbientColor;
        Float4 vAmbientIntensity;
#endregion
#region Material.DeclareRenderTargets
    #target ProxyTarget
        Color;
        Normal;
    #target GeometryTarget
        BaseColor;
        Normal;
        HdrColor;
    #target DepthTarget
    
    #define C_PASS_GEOMETRY 1
    #define C_PASS_DEPTH    2
    #define C_PASS_PROXY    3
#endregion
#region Material.Add
    /// Input constants:
    /// I_MATERIAL_NAME     string  Material unique name
    /// I_MATERIAL          int     Material ID to pass into includes
    /// I_HAS_PROXY         0/1     Set to add proxy generation pass
    ///
    /// Included blocks:
    ///
    /// "main.Material"     Included one time
    ///
    /// "main.Pass"         Included for each pass
    /// I_PROGRAM           string  Pass program name
    /// I_TARGET            string  Destination target
    /// I_PASS              int     Current pass ID
    /// + C_PASS_GEOMETRY   1       Geometry pass, target is @a GeometryTarget
    /// + C_PASS_DEPTH      2       Depth pass, target is @a DepthTarget
    /// + C_PASS_PROXY      3       Proxy generation pass, target is @a ProxyTarget
    /// 
    
    #include "main.Material"
    
    // Add proxy pass
    #if I_HAS_PROXY != 0
        #redefine I_PROGRAM CONCAT2(I_MATERIAL_NAME, Proxy)
        #redefine I_TARGET  ProxyTarget
        #redefine I_PASS    C_PASS_PROXY
        #include "main.Pass"
    #endif
    
    // Add geometry pass
    #redefine I_PROGRAM CONCAT2(I_MATERIAL_NAME, Geometry)
    #redefine I_TARGET  GeometryTarget
    #redefine I_PASS    C_PASS_GEOMETRY
    #include "main.Pass"
    
    // Add depth pass
    #redefine I_PROGRAM CONCAT2(I_MATERIAL_NAME, Depth)
    #redefine I_TARGET  DepthTarget
    #redefine I_PASS    C_PASS_DEPTH
    #include "main.Pass"
#endregion

#region Material.PBR
    #define C_MIN_REFLECTIVITY 0.01
#endregion