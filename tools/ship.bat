@echo off
md "temp"
copy /Y "..\build\vc15\bin\RelWithDebInfo\FlexEngine.exe" "temp"
copy /Y "..\build\vc15\bin\RelWithDebInfo\*.dll" "temp"
copy /Y "..\root.txt" "temp"
xcopy /Y /S /I "..\asset" "temp\asset"
xcopy /Y /S /I "..\cache" "temp\cache"
7z a -r temp "./temp/*"
rd /s /q "temp"
move "temp.7z" "../ship_debug.7z"

