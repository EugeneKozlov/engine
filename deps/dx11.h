#ifndef _MSC_VER
#define __in
#define __out
#define __inout
#define __in_bcount(x)
#define __out_bcount(x)
#define __in_ecount(x)
#define __out_ecount(x)
#define __in_ecount_opt(x)
#define __out_ecount_opt(x)
#define __in_bcount_opt(x)
#define __out_bcount_opt(x)
#define __in_opt
#define __inout_opt
#define __out_opt
#define __out_ecount_part_opt(x,y)
#define __deref_out
#define __deref_out_opt
#define __RPC__deref_out
#   pragma GCC diagnostic push
#   pragma GCC diagnostic ignored "-Wsign-compare"
#endif

#ifndef NOMINMAX
#	define NOMINMAX
#endif

#include <Windows.h>
#include <d3d11.h>
#include <d3dcompiler.h>

#ifndef _MSC_VER
#   pragma GCC diagnostic pop
#endif
