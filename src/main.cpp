#include "pch.h"

#include "ls/common.h"
#include "ls/asset/Asset.h"
#include "ls/asset/AssetPackage.h"
#include "ls/asset/AssetManager.h"
#include "ls/asset/all.h"
#include "ls/scene/type/FlatScene.h"
#include "ls/render/BatchRenderer.h"

#include "ls/scene/Entity.h"
#include "ls/scene/EntityManager.h"
#include "ls/scene/component/all.h"


#include "ls/core/Worker.h"
#include "ls/io/StaticStream.h"
#include "ls/io/StreamInterface.h"
#include "ls/io/Codec.h"

#include "ls/app/ApplicationInterface.h"
#include "ls/app/ApplicationSceneInterface.h"
#include "ls/app/GlfwApplication.h"
#include "ls/io/FileSystemInterface.h"
#include "ls/gapi/dx11/Dx11Renderer.h"
#include "ls/render/SceneView.h"
#include "ls/render/Plotter.h"
#include "ls/render/Mesh.h"
#include "ls/render/Texture2D.h"
#include "ls/render/model_vertex.h"
#include "ls/render/DeferredShading.h"
#include "ls/utility/Console.h"
#include "ls/import/font_import.h"
#include "ls/asset/AssetManagerOld.h"

#include "ls/geom/NodeAnimation.h"
#include "ls/geom/NodeHierarchy.h"
#include "ls/ik/BipedSkeleton.h"
#include "ls/ik/BipedWalkAnimation.h"
#include "ls/world/StreamingWorld.h"
#include "ls/world/WorldChunkManager.h"

#include "ls/engine/app/Config.h"
#include "ls/core/Timer.h"
#include "ls/misc/FrameRateMeter.h"
//#include "ls/scene/ObjectsFactory.h"

//#include "ls/engine/network/message.h"
//#include "ls/engine/network/reflection.h"
#include "ls/network/message.h"
#include "ls/network/reflection.h"

//#include "ls/simulation/PhysicalProcessor.h"
//#include "ls/simulation/Simulation.h"

//#include "ls/engine/network/Instance.h"
//#include "ls/engine/client/Client.h"
//#include "ls/engine/server/Server.h"
//#include "ls/simulation/Simulation.h"
//#include "ls/simulation/ActorSimulation.h"

#include "ls/core/UniformRandom.h"

#include "ls/lua/LuaApi.h"
// #include "ls/scene/type/FlatScene.h"
#include "ls/scene/graphical/GraphicalScene.h"
#include "ls/scene/physical/PhysicalScene.h"

#include "ls/core/GlobalVariableManager.h"
#include <windows.h>

#include "ls/gapi/usl/usl.h"
bool useOld = false;
using namespace ls;

struct ApplicationScene
    : ApplicationSceneInterface
    , WorldObserverInterface
{
    shared_ptr<AssetManager> m_assetManager;
//     shared_ptr<BatchRenderer> m_batchRenderer;
    shared_ptr<FlatScene> m_scene;

    Timer rateMeterTimer;
    bool wasResized = false;
    FrameRateMeter rateMeter;

    Camera cam;
    Renderer* renderer;
    FileSystemInterface* root;
    unique_ptr<Plotter> plotter;
    unique_ptr<Console> console;
    shared_ptr<Worker> worker;
    shared_ptr<LuaApi> lua;

    unique_ptr<AssetManagerOld> dbAssets;
    //unique_ptr<ObjectsFactory> dbObjectsFactory;
    vector<shared_ptr<Font>> fonts;
    unique_ptr<DeferredShading> rrDeferredShading;
    unique_ptr<SceneView> rrSceneView;

    double3 lookPos;

    uint botTimer = 0;
    uint botMode = 0;
    uint botLimit = 1;
    bool drawServer = false;

    uint mode = 1;
    uint seed = 0;
    float fovy = pi / 3;
    bool flyCamera = true;

//     unique_ptr<FlatScene> ssScene;
//     unique_ptr<WorldGeneratorManager> wg;
private:
    //void startServer(ushort port, const char* ip)
    //{
    //    try
    //    {
    //        //instance = make_unique<Instance>(*dbObjectsFactory, dbTerrain,
    //        //                                 ip, port, Web::Root, 0, "Server");
    //        instanceInit();
    //    }
    //    catch (...)
    //    {
    //        instance.reset();
    //    }
    //}
    //void connectToServer(ushort port, const char* ip, bool super)
    //{
    //    try
    //    {
    //        //instance = make_unique<Instance>(*dbObjectsFactory, dbTerrain,
    //        //                                 ip, port, super ? Web::Super : Web::Basic,
    //        //                                 4000, "Eugene");
    //        instanceInit();
    //    }
    //    catch (...)
    //    {
    //        instance.reset();
    //    }
    //}
    //void instanceInit()
    //{
    //    instance->client().onRaycast = bind(&ApplicationScene::onRaycast, this, _1, _2);
    //}
public:
    shared_ptr<EffectAsset> m_renderDsEffect;
    shared_ptr<EffectAsset> m_plainEffect;
    ApplicationScene(ApplicationInterface* application)
        : ApplicationSceneInterface(application)
        , rateMeterTimer(500, false)
    {
        renderer = application->renderer();
        root = &*application->root();
        worker = make_shared<Worker>(1);

        // Config
        //config().add(root->openStream("asset/config/config.xml")->text());
        //config().load();
        const string configText = root->openStream("asset/config/config.ini")->text();
        GlobalVariableManager::instance().loadIni(configText);

        config().load(configText);
        renderer->initialize();

        // Assets
        dbAssets = make_unique<AssetManagerOld>(root->openSystem("asset"),
                                             root->openSystem("cache"),
                                             *worker);
//         dbAssets->get<Material>("birch_bark");
//         dbAssets->get<Material>("birch_leaf_large");
//         dbAssets->get<GraphicalModel>("birch_forest");

        m_assetManager = make_shared<AssetManager>(root->openSystem("asset"),
                                                   root->openSystem("cache"),
                                                   renderer, worker);
        m_assetManager->loadAsset(AssetType::EffectLibrary, "architect/common/effect_library/default");
        m_assetManager->loadAsset(AssetType::EffectLibrary, "architect/common/effect_library/geometry");
        m_assetManager->loadAsset(AssetType::EffectLibrary, "architect/common/effect_library/generate");
        m_assetManager->loadAsset(AssetType::EffectLibrary, "architect/common/effect_library/lighting");
        m_assetManager->loadAsset(AssetType::EffectLibrary, "architect/common/effect_library/material");
        m_assetManager->loadAsset(AssetType::EffectLibrary, "architect/common/effect_library/math");
        try
        {
            m_plainEffect = Asset::cast<EffectAsset>(m_assetManager->loadAsset(AssetType::Effect, "architect/common/effect/plain_effect"));
            m_renderDsEffect = Asset::cast<EffectAsset>(m_assetManager->loadAsset(AssetType::Effect, "architect/common/effect/deferred_shading_effect"));
        }
        catch (const std::exception& ex)
        {
            logError(ex.what());
        }


        // Lua
        lua = make_shared<LuaApi>("Lua");
        lua->registerModule(*m_assetManager);
        lua->pushFolder(root->openSystem("asset/script"));
//         wg = make_unique<WorldGeneratorManager>(lua, config().world(), *m_assetManager,
//                                                 root->openSystem("world"),
//                                                 root->openSystem("cache"));
        lua->import("startup.lua");

        // Batch renderer
        // @{
//         m_batchRenderer = make_shared<BatchRenderer>(*renderer);
        // @}

        // Scene
        // @{
        SceneInfo sceneInfo;
        sceneInfo.worldStreamingConfig = config().worldStreaming();
        sceneInfo.worldName = "architect/worlds/test";
        m_scene = make_unique<FlatScene>(m_assetManager, renderer, *worker, sceneInfo);
//         m_scene->initialize();

//         // Add actor
//         EntityManager& entityManager = m_scene->entityManager();
//         Entity mainActor;
//         TransformComponent& transformComponent = mainActor.addComponent<TransformComponent>();
//         transformComponent.pose.position(double3(0.0f, 10.0f, 0.0f));
//         CameraComponent& cameraComponent = mainActor.addComponent<CameraComponent>();
//         entityManager.addEntity(mainActor);
// 
//         // Launch
//         m_scene->launch();
        // @}

//         ssScene = make_unique<FlatScene>(m_assetManager, renderer, *worker, sceneInfo);
        m_scene->createGraphicScene(*renderer, config().graphic());
        m_scene->createPhysicalScene();

        GraphicalScene* graphicalScene = m_scene->graphicalScene();
        graphicalScene->mainCamera(cam);

        // Deferred shading
        rrDeferredShading = make_unique<DeferredShading>(*m_assetManager,
                                                         config().graphic(),
                                                         m_renderDsEffect->gapiEffect());

        /*dbAssets->get<NodeAnimation>("test_box");
        dbAssets->get<Skeleton>("biped");
        dbAssets->get<BipedWalkAnimation>("biped/walk");
        dbAssets->get<BipedWalkAnimation>("biped/run");
        dbAssets->get<BipedWalkAnimation>("biped/idle");*/

        /*dbTerrain = make_shared<StreamingWorld>(*worker,
        root->openSystem("cache/world/architect.test"), "architect.test",
        config().world(), config().worldStreaming(), true, false);
        {
        BlockProfiler<milliseconds> meter("Terrain preparing: ",
        LogMessageLevel::Notice);
        dbTerrain->autosave(true);
        dbTerrain->moveTo(int2(0, 0));
        //rrModel->importCatalog(dbTerrain->world().cat, *dbAssets);
        }*/

        //terrain->saveAll();
        /*terrain->moveTo(int2(7, 8));
        Sleep(100);
        terrain->flush();
        terrain->moveTo(int2(8, 8));
        Sleep(100);
        terrain->flush();*/

        cam.setView(double3(0, 1.75, 0), float3(1, 0, 1), 0);
//         cam.setView(double3(-150, 15, -150), float3(1, 0, 1), 0);

        // Font
        auto fontTexture = dbAssets->get<Texture2D>("font_main");
        fontTexture->create(*renderer);
        Stream fontSource = root->openStream("asset/font/mono.xml");
        fonts = importFont(fontSource->text(), fontTexture->texture());

        plotter = make_unique<Plotter>(*renderer, m_plainEffect->gapiEffect());
        console = make_unique<Console>(&*plotter, false);
        console->onChat = bind(&ApplicationScene::onChat, this, _1);
        console->onCommand = bind(&ApplicationScene::onCommand, this, _1);

        console->setFont(fonts[2]);
        console->show();
        console->ignoreSymbol('`');
        console->ignoreSymbol('~');
        console->ignoreSymbol(1025);
        console->ignoreSymbol(1105);

        /*Gid texArray = renderer->createTexture2D(2, 1, 16, 16,
        Format::UbyteNorm4,
        ResourceUsage::Static,
        ResourceBinding::Readable,
        nullptr);
        Gid destArray = renderer->createTexture2D(2, 1, 16, 16,
        Format::UbyteNorm4,
        ResourceUsage::Static,
        ResourceBinding::Readable,
        nullptr);
        Gid destTex = renderer->createTexture2D(1, 1, 16, 16,
        Format::UbyteNorm4,
        ResourceUsage::Static,
        ResourceBinding::Readable,
        nullptr);
        uint temp = 0xffccddee;
        renderer->writeTexture2D(texArray, 1, 0, 3, 3, 1, 1, &temp);
        renderer->copyTexture2D(destArray, texArray, 0, 1, 0, 0, 1, 1, 3, 3, 1, 1);
        renderer->copyTexture2D(destTex, texArray, 0, 1, 0, 0, 0, 0, 0, 0, 16, 16);
        renderer->readTexture2D(destArray, 0, 0, 1, 1, 1, 1, &temp);
        renderer->readTexture2D(destTex, 0, 0, 3, 3, 1, 1, &temp);*/

        /*Gid texDummy = renderer->loadTexture2D(*root->openStream("asset/texture/dummy.dds"),
        ResourceBinding::Readable | ResourceBinding::ShaderResource);
        ubyte dummyValue[4][4][4];
        renderer->readTexture2D(texDummy, 0, 0, 4, 4, dummyValue);*/

        // Test
        /*Gid texDepth = renderer->createTexture2D(1, 1,
        Format::Depth24Stencil8,
        ResourceUsage::Static,
        ResourceBinding::DepthStencil | ResourceBinding::Readable,
        nullptr);
        float2 defColor[4] = {float2(0.0f), float2(1.0f)};
        Gid texColor = renderer->createTexture2D(1, 1,
        Format::Float2,
        ResourceUsage::Static,
        ResourceBinding::RenderTarget | ResourceBinding::Readable,
        defColor);
        Gid renderTarget = renderer->createRenderTarget(1, texDepth);
        renderer->attachColorTexture2D(renderTarget, 0, texColor);

        renderer->bindRenderTarget(renderTarget);

        glDrawBuffer(GL_COLOR_ATTACHMENT0);
        glClearStencil(255);
        glClearDepth(1.0f);
        glClearColor(1.0f, 0.5f, 0.25f, 0.75f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        renderer->bindRenderTarget(DefaultRenderTarget);*/

        // Terrain
        /*EffectHandle terrainEffect = dbEffect->effect("render.terrain");

        {
        BlockProfiler<milliseconds> meter("Terrain renderer creation: ",
        LogMessageLevel::Notice);
        rrTerrain = make_unique<TerrainRenderer>(*renderer,
        config().world(), config().worldStreaming(),
        terrainEffect,
        false);
        rrTerrain->registerPolicy(BasePassPolicy, "Geometry");
        rrTerrain->registerPolicy(BasePassWiredPolicy, "Wired");
        dbTerrain->addObserver(*rrTerrain);
        }*/

        // Editor
        //EffectHandle terrainEditorEffect = dbEffect->effect("editor.terrain");
        {
            //edTerrain = make_unique<TerrainEditor>(*rrTerrain, terrainEditorEffect);
        }

        {
            BlockProfiler<milliseconds> meter("Terrain flushing: ",
                                              LogMessageLevel::Notice);
            /*worker->wait();
            ssScene->streamingWorld().flush();
            worker->wait();
            ssScene->streamingWorld().flush();*/
        }

        // Fin
        //rrTerrain->setWire(true);
        //rrTerrain->setWireColor(float3(1, 1, 1));

        app->lockCursor(false);
        app->showCursor(true);
    }
    virtual ~ApplicationScene()
    {
        worker->wait();

//         ssScene.reset();
        //dbTerrain->removeObserver(*rrTerrain);
        dbAssets.reset();

        worker.reset();
        //instance.reset();

//         wg.reset();
        lua.reset();

        m_plainEffect.reset();
        m_renderDsEffect.reset();

        rrDeferredShading.reset();
    }
    virtual void idle(float deltaSeconds, uint deltaMsec, const float2& mouseDelta) override
    {
        app->showCursor(true);
        if (wasResized)
            doResize(app->width(), app->height());

        // Depth
        float depth = 1.0f;// (float) rrDeferredShading->readDepth(app->width() / 2, app->height() / 2);
        lookPos = convert<double3>(float3(0, 0, depth) * inverse(cam.viewProjMatrix())) + cam.worldCenter();

        // Mouse
        if (app->isLocked())
        {
            float3 mov;
            mov.z += app->isPressed((KeyCode) 'W') ? 1 : 0;
            mov.z -= app->isPressed((KeyCode) 'S') ? 1 : 0;
            mov.x += app->isPressed((KeyCode) 'D') ? 1 : 0;
            mov.x -= app->isPressed((KeyCode) 'A') ? 1 : 0;
            mov.y += app->isPressed(Key::Space) ? 1 : 0;
            mov.y -= app->isPressed(Key::Ctrl) ? 1 : 0;
            mov = cam.local2global(mov);
            if (!flyCamera)
                mov = normalize(swiz<X, O, Z>(mov));

            float3 rot;
            rot.x += app->isPressed(Key::Up) ? 1 : 0;
            rot.x -= app->isPressed(Key::Down) ? 1 : 0;
            rot.y += app->isPressed(Key::Left) ? 2 : 0;
            rot.y -= app->isPressed(Key::Right) ? 2 : 0;
            bool acc = app->isPressed(Key::Shift);

            float3 mouseRot = float3(-mouseDelta.y, -mouseDelta.x, 0);
            float3 mouseSensitivity = float3(1.2f, 1.5f, 0) * 0.001f;
            float3 keyboardSensitivity = float3(1.0f, 1.0f, 1.0f) * (acc ? 50.0f : 7.0f);
            cam.update(mov * deltaSeconds * keyboardSensitivity,
                       mouseRot * mouseSensitivity + rot * 0.01f);
        }

        m_scene->syncUpdate(deltaSeconds, deltaMsec);
        m_scene->beginAsyncUpdate();

        // Update console
        console->process();

        // Terrain
        double2 flatCamera = swiz<X, Z>(cam.position());
        /*int2 chunk = convert<int2>(floor(flatCamera / (float) dbTerrain->chunkSize));
        if (instance)
        instance->move(chunk);
        dbTerrain->moveTo(chunk);
        dbTerrain->flush();
        cam.setCenter(dbTerrain->worldCenterPosition());*/

        // Sim
        /*if (!!instance)
        {
        //instance->move(dbTerrain->currentChunk());
        if (!instance->update(deltaMsec))
        instance.reset();
        }

        ObjectIterator actor = (!!instance && instance->client().isSimulated())
        ? instance->client().sim().actors().thisActor() : ObjectIterator();
        if (!!actor)
        {
        cam.move(actor->position(), float3(0.0f));
        ActorMovement::Type movx, movy, movz;
        float3 look;
        if (botMode == 1)
        {
        if (botTimer < 1000)
        ;
        else if (botTimer < 2000)
        movx = ActorMovement::Left;
        else if (botTimer < 3000)
        ;
        else if (botTimer < 4000)
        movx = ActorMovement::Forward;
        else if (botTimer < 5000)
        ;
        else if (botTimer < 6000)
        movx = ActorMovement::Right;
        else if (botTimer < 7000)
        ;
        else
        movx = ActorMovement::Backward;
        look = float3(0.0f, 0.0f, 1.0f);
        }
        else if (botMode == 2)
        {
        if (botTimer < 4000)
        movx = ActorMovement::Left;
        else if (botTimer < 8000)
        movx = ActorMovement::Forward;
        else if (botTimer < 12000)
        movx = ActorMovement::Right;
        else
        movx = ActorMovement::Backward;
        look = float3(0.0f, 0.0f, 1.0f);
        }
        else if (!console->isVisible())
        {
        bool w = app->isPressed((KeyCode) 'W');
        bool s = app->isPressed((KeyCode) 'S');
        bool a = app->isPressed((KeyCode) 'A');
        bool d = app->isPressed((KeyCode) 'D');
        bool c = app->isPressed((KeyCode) Key::Ctrl);
        bool j = app->isPressed((KeyCode) ' ');
        movz = w ? ActorMovement::Forward : (s ? ActorMovement::Backward : ActorMovement::No);
        movx = a ? ActorMovement::Left : (d ? ActorMovement::Right : ActorMovement::No);
        movy = c ? ActorMovement::Down : (j ? ActorMovement::Up : ActorMovement::No);
        look = cam.look();
        }
        instance->client().sim().actors().moveThisActor(ActorMovement(movx | movy | movz, look));
        }*/

        // Advance
        rateMeterTimer.advance(deltaMsec);

        botTimer += deltaMsec;
        botTimer %= botLimit;

        if (wasResized)
            return;
        RendererStatistic stats = renderer->getStats();
        renderer->resetStats();

        renderer->bindRenderTarget(nullptr);

//         m_scene->render(*m_batchRenderer);
        rrDeferredShading->drawGeometryBuffer(*m_scene->graphicalScene());
        rrDeferredShading->drawLights(*m_scene->graphicalScene());
        rrDeferredShading->drawFinalImage();

        ColoredVertex axisVertices[] = {
            ColoredVertex(float3(0, 0, 0), 0xff0000ff),
            ColoredVertex(float3(1, 0, 0), 0xff0000ff),
            ColoredVertex(float3(0, 0, 0), 0xff00ff00),
            ColoredVertex(float3(0, 1, 0), 0xff00ff00),
            ColoredVertex(float3(0, 0, 0), 0xffff0000),
            ColoredVertex(float3(0, 0, 1), 0xffff0000),
        };

        //rrDeferredShading->beginGeometry(float4(0.5, 0.5, 1.0, 1.0));
        //rrDeferredShading->beginMaterial(DeferredShading::OneSided);

        plotter->setCamera(cam);

        // Terrain
        //if (!!rrTerrain)
        {
            //rrTerrain->setDistances({ 600.0f, 300.0f, 150.f, 70.f, 30.0f, 0.0f, 0.0f });
            //rrTerrain->render(dbTerrain->world().tree, cam);
            //glDisable(GL_CULL_FACE);
            //rrModel->render(dbTerrain->world().tree, cam);
            //glEnable(GL_CULL_FACE);
        }

        // Objects
        plotter->begin(Plotter::DrawLines, nullptr, false, false);
        /*auto objectPlotter = [this](ObjectInstance& object, const float4x4& matrix,
        bool server, uint color, float scale)
        {
        if (object.isGlobal())
        plotter->plotWireSphere(matrix, color, 0.5f * scale);
        else
        plotter->plotWireBox(matrix, color, float3(0.5f, 0.5f, 0.5f) * scale);

        if (!server && (object.debugFlags() & 1))
        {
        plotter->plotWireSphere(matrix * matrix::translate(float3(0.0f, 0.5f, 0.0f)),
        0xff0000ff, 0.1f);
        }
        };*/
        /*if (instance && instance->client().isSimulated())
        instance->client().sim().objectsManager().debugPlot(bind(objectPlotter, _1, _2, false, 0xff00ff00, 1.0f),
        cam.worldCenter(), cam.position(), 4);
        if (instance && instance->isServer() && drawServer)
        instance->server().objects().debugPlot(bind(objectPlotter, _1, _2, true, 0xffff0000, 1.1f),
        cam.worldCenter(), cam.position(), 4);*/

        // Axis
        plotter->plotLine(axisVertices[0], axisVertices[1]);
        plotter->plotLine(axisVertices[2], axisVertices[3]);
        plotter->plotLine(axisVertices[4], axisVertices[5]);
        plotter->end();



        //rrDeferredShading->beginLighting(cam.viewMatrix());
        //rrDeferredShading->drawAmbient();
        //rrDeferredShading->drawDirectional(normalize(float3(0.5, 1.0, 0.5)));
        //rrDeferredShading->drawDirectional(normalize(float3(0.0, 1.0, 0.0)));
        //rrDeferredShading->finalize();


        // Info
        //renderer->bindDepthStencil(NoDepth);

        auto chunkPlotter = [this](const char* text, const int2& chunk)
        {
            /*WorldChunk* ptr = dbTerrain->world().findChunk(chunk);
            if (ptr)
            {
            float height = ptr->bounds[1];
            float3 p(chunk.x*32.0f + 16, 0, chunk.y*32.0f + 16);
            plotter->plotText3D(*fonts[2], text, p, rgb2hex(float3(1.0f, 1.0f, 0.0f)));
            }*/
        };
        plotter->begin(Plotter::DrawText2D, fonts[2]->texture(), true, false);
        //if (!!instance && instance->client().isSimulated())
        //    instance->client().sim().debugDrawChunks(chunkPlotter, dbTerrain->currentChunk(), 3);
        plotter->end();

        static DiscreteTime nowTime = 0;
        static ulong lastSecond = 0;
        ulong nowSecond = duration_cast<seconds>(system_clock::now().time_since_epoch()).count();
        //if (instance && nowSecond != lastSecond)
        //    nowTime = instance->client().time();
        lastSecond = nowSecond;
//         const SceneStats sceneStats = m_scene->statistics();
        const string renderDuration = rateMeter.durationText();
//         const string updateDuration = format("{*} ms ({*} .. {*})", Lowp(),
//                                              sceneStats.averageTime, sceneStats.minTime, sceneStats.maxTime);
        string text = format("{*}\nRender: {*}\nUpdate: {*}\nTris: {*} DIPs:{*}\nLook from {*} to {*}\n",
                             rateMeter.fpsText(), renderDuration, "" /*updateDuration*/, 
                             stats.numTriangles, stats.numDrawCalls, Lowp(), cam.position(), lookPos);// instance ? nowTime / 1000.0f : 0.0f,
                                                                                                      //instance ? instance->client().lag() : 0,
                                                                                                      //instance ? instance->inputSpeedTcp() : 0.0, instance ? instance->inputSpeedUdp() : 0.0,
                                                                                                      //instance ? instance->outputSpeedTcp() : 0.0, instance ? instance->outputSpeedUdp() : 0.0);
        plotter->begin(Plotter::DrawText2D, fonts[1]->texture(), true, false);
        plotter->plotText(*fonts[1], text.c_str(), int2(0, 0), 0xffffffff);
        plotter->end();

        if (0)
        {
            auto tex = dbAssets->get<Texture2D>("A.birchLeaf_large.diffuse");
            tex->create(*renderer);
            plotter->setCamera(plotter->defaultCamera());
            plotter->begin(Plotter::DrawTexturedTris, tex->texture(), false, true);
            plotter->plotQuad(float2(0.0, 0.0), float2(512.0f, 512.0f));
            plotter->end();
        }
        console->render();
        m_scene->endAsyncUpdate();

//         m_assetManager->loadPackage("architect/birch_tree").generateAssets();
        rateMeter.step();
        if (rateMeterTimer.alarm())
            rateMeter.flush();
    }
    virtual void resize(uint width, uint height) override
    {
        wasResized = true;
    }
    void doResize(uint width, uint height)
    {
        logNotice("Resize window to (", width, ", ", height, ")");
        wasResized = false;

        if (!!m_scene)
        {
            m_scene->graphicalScene()->updateViewport((float) width, (float) height);
        }

        plotter->resize(width, height);
        console->resize(width, height);
        rrDeferredShading->resize(width, height);
    }
    virtual void keyDown(KeyCode key) override
    {
        console->control(key);
        switch (key)
        {
        case Key::Escape:
            app->stop();
            break;
        case Key::Tilde:
            console->swap();
            app->lockCursor(!console->isVisible());
            app->showCursor(console->isVisible());
            break;
        case Key::Left:
            ++seed;
            break;
        case Key::Right:
            --seed;
            break;
        case Key::Tab:
            //mode = !mode;
            useOld = !useOld;
            break;
        case Key::F1:
            flyCamera = !flyCamera;
            if (!flyCamera)
                cam.move(cam.position() * double3(1, 0, 1) + double3(0, 1.75, 0));
            break;
        default:;
        }

        if (console->isVisible())
            return;
        switch (key)
        {
        case '0':
            Sleep(500);
            break;
        default:;
        }
    }
    virtual void keyUp(KeyCode key) override
    {

    }
    virtual void enterSymbol(uint symbol) override
    {
        console->type(symbol);
    }
    virtual void mouseButtonDown(const double2& cursor, MouseButton button)
    {
        switch (button)
        {
            //case MouseButton::Left:
            //case MouseButton::Right:
            //    if (!!edTerrain)
            //    {
            //        edTerrain->chop();
            //    }
            //    break;
        case MouseButton::Left:
        {
            //if (instance)
            //    instance->client().invokeRaycast(cam.look(), 0, 0);
            break;
        }
        default: break;
        }
    }
    virtual void mouseButtonUp(const double2& cursor, MouseButton button)
    {
        switch (button)
        {
        case MouseButton::Left:
        case MouseButton::Right:
        default: break;
        }
    }
    void onCommand(const char* text)
    {
        stringstream stm(text);
        if (text[0] == '.')
        {
            string property, value;
            stm >> property >> value;
            property = "config" + property;
            if (!config().set(property, value))
            {
                logWarning("Config property [", property, "] is not found");
            }
            else
            {
                logNotice("Config property [", property, "] is changed to [", value, "]");
            }
        }
        else
        {/*
         string cmd;
         stm >> cmd;
         if (cmd == "ss")
         startServer(10010, "127.0.0.1");
         else if (cmd == "cs")
         connectToServer(10010, "127.0.0.1", true);
         else if (cmd == "bot")
         {
         uint num;
         stm >> num;
         botMode = num;
         botTimer = 0;
         switch (num)
         {
         case 1: botLimit = 8000; break;
         case 2: botLimit = 16000; break;
         default: botLimit = 1; break;
         }
         }*/
        }
    }
    void onChat(const char* text)
    {
        console->writeChat("Admin", text);
    }
    /*    void onRaycast(ObjectInstance& caster, const RaycastInfo& ray)
    {
    string name = format("Player {*}", caster.id());
    console->writeChat(name.c_str(), "BANG!");
    }
    */
};

using namespace ls;
inline void test(bool condition)
{
    debug_assert(condition);
}
void onTerminate()
{
    logError("Stack trace: ", Endl(), stackTrace());

}

#include "ls/io/Codec.h"
#define CATCH_CONFIG_RUNNER
#include <catch/catch.hpp>
int main(int argc, char** argv)
{
#if LS_DETECT_MEMORY_LEAKS != 0
    auto flags = _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG);
    flags |= _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF;
    _CrtSetDbgFlag(flags);
    //_CrtSetBreakAlloc(12701);
#endif

    // Run UTs
    const int utResult = Catch::Session().run(0, argv);
    if (utResult != 0)
    {
        logWarning("UT has failed");
    }

    std::set_terminate(onTerminate);
    std::set_unexpected(onTerminate);
    int code;
    {
        GlfwApplication app{};
//         app.createWindow("Title", 512, 512, false, Gapi::Any);
//         app.createWindow("Title", 640, 480, false, Gapi::Any);
//         app.createWindow("Title", 800, 600, false, Gapi::Any);
        app.createWindow("Title", 1360, 760, false, Gapi::Any);
//         app.createWindow("Title", 1366, 768, true, Gapi::Any);
        app.install(new ApplicationScene(&app));
        code = app.run();
        logNotice("Shutdown with code [", code, "]");
    }
    return code;
}

