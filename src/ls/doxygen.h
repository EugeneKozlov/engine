/// @defgroup LsConfig Ls.Config
/// @brief Framework configuration
///
/// @defgroup LsCommon Ls.Common
/// @brief Common classes, functions and templates
///
/// @defgroup LsCommonExceptions Ls.Common.Exceptions
/// @brief Exceptions
///
/// @defgroup LsCommonAlgorithm Ls.Common.Algorithm
/// @brief Must-be-standard algorithms
///
/// @defgroup LsMath Ls.Math
/// @brief Mathematics
///
///
/// @defgroup LsApp Ls.App
/// @brief Application management classes
///
/// @defgroup LsAsset Ls.Asset
/// @brief Asset storage and management
///
/// @defgroup LsEngine Ls.Engine
/// @brief Framework usage and module combiner
///
/// @defgroup LsGapi Ls.Gapi
/// @brief Graphical API
///
/// @defgroup LsGen Ls.Gen
/// @brief Generators and factories
///
/// @defgroup LsGeom Ls.Geom
/// @brief Node hierarchy, node animation, 3d-math and other geometry stuff.
///
/// @defgroup LsIk Ls.Ik
/// @brief Inverse kinematics implementation
///
/// @defgroup LsImport Ls.Import
/// @brief Importers from 3rd-party formats
///
/// @defgroup LsIo Ls.Io
/// @brief I/O systems and encoding
///
/// @defgroup LsMisc Ls.Misc
/// @brief Miscellaneous stuff
///
/// @defgroup LsNetwork Ls.Network
/// @brief Networking
///
/// @defgroup LsRender Ls.Render
/// @brief Renderer classes (no GAPI-specific)
///
/// @defgroup LsScene Ls.Scene
/// @brief Scene objects and etc
///
/// @defgroup LsUtility Ls.Utility
/// @brief Utility classes like console
///
/// @defgroup LsWorld Ls.World
/// @brief World geometry and static objects management classes
///
/// @defgroup LsXml Ls.Xml
/// @brief Pugi XML helpers

/// @namespace ls
/// @brief Main engine namespace
namespace ls
{
    /// @namespace ls::gapi
    /// @brief GAPI implementation
    namespace gapi
    {

    }
}