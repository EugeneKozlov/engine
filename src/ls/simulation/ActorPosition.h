/** @file ls/simulation/ActorPosition.h
 *  @brief Actor correction position
 */

#pragma once
#include "ls/common.h"
#include "ls/network/common.h"

namespace ls
{
    /** @addtogroup LsSimulation
     *  @{
     */
    /** Actor correction position
     */
    struct ActorPosition
    {
    public:
        /// Ctor
        ActorPosition() = default;
        /// Ctor
        ActorPosition(ClientId client, DiscreteTime time, double3 const& position)
            : client(client)
            , time(time)
            , position(position)
        {
        }
        /// Client ID
        ClientId client = NetInvalidClient;
        /// Time
        DiscreteTime time = 0;
        /// Position
        double3 position;
    };
    /// @}
}
