#include "pch.h"
#include "ls/simulation/Simulation.h"
#include "ls/simulation/PhysicalProcessor.h"
#include "ls/simulation/ActorSimulation.h"
#include "ls/simulation/RaycastInfo.h"
#include "ls/world/WorldChunkManager.h"
#include "ls/world/StreamingWorld.h"
#include "ls/scene/ObjectInstance.h"
#include "ls/scene/ObjectsManager.h"
#include "ls/scene/ObjectsFactory.h"
#include "ls/geom/math.h"

using namespace ls;
struct Simulation::Client
{
    /// No ass
    Client& operator =(Client const& another) = delete;
    /// Ctor
    Client(ClientId id, SimulationInfo const& info)
        : info(info)
        , id(id)
    {
    }
    /// Set ping
    bool setPing(DeltaTime ping)
    {
        lastPing = ping;

        DeltaTime minLag = info.minLag;
        DeltaTime lagError = info.lagError;
        DeltaTime extraLag = info.extraLag;
        DeltaTime newLag = max<DeltaTime>(minLag, ping / 2 + lagError + extraLag);
        sint error = newLag - lag;
        if (abs(error) < (sint) lagError)
            return false;

        if (id == NetRootClient)
            lag = 0;
        else
            lag = newLag;
        return true;
    }
    /// Config
    SimulationInfo const& info;
    /// This ID
    ClientId id = NetInvalidClient;
    /// Last ping
    DeltaTime lastPing = 0;
    /// Lag
    DeltaTime lag = 0;
};
Simulation::Simulation(SimulationInfo const& info,
                       ObjectsFactory& factory,
                       StreamingWorld& worldData,
                       DiscreteTime startTime)
    : Nameable("Simulation")
    , thisClient(info.thisClient)
    , info(info)
    , tracingInfo(info.tracingDuration,
                  info.framePeriod, info.actionPeriod, info.actionTick,
                  info.controlLag, info.correctionDelay,
                  info.trustActors, info.traceActors)

    , m_chunkManager(new WorldChunkManager(thisClient,
                                           info.maxSimDistance, info.simDistancePad,
                                           info.outerDistancePad, info.traceChunks))
    , m_worldData(worldData)
    , m_factory(factory)

    , m_localTime(startTime)
    , m_serverTime(startTime)
    , m_timeCompensator(info.numTimeStamps, 0)

    , m_inputLag(info.minLag)

    , m_thisActionTimer(info.actionPeriod, m_localTime)
    , m_frameTimer(info.framePeriod, m_localTime)
    , m_actorTimer(info.actionPeriod, m_localTime)

    , m_storage(make_unique<ObjectsManager>(worldData.chunkSize, worldData.regionWidth, true))
    , m_actors(make_unique<ActorSimulation>(thisClient, *m_chunkManager, tracingInfo))
    //, m_physics(make_unique<PhysicalProcessor>(float3(0, -10, 0), *m_storage, m_worldData))
{
    m_chunkManager->onEnter = bind(&Simulation::enterSimulation, this, _1);
    m_chunkManager->onLeave = bind(&Simulation::leaveSimulation, this, _1);

    m_worldData.addObserver(*m_chunkManager);
}
Simulation::~Simulation()
{
    m_chunkManager->removeClient(m_chunkManager->thisClient());
    m_worldData.removeObserver(*m_chunkManager);
    m_chunkManager.reset();
}
Simulation::Client* Simulation::findClient(ClientId id)
{
    return findOrDefault(m_clients, id);
}


// Callbacks
void Simulation::enterSimulation(int2 const& chunk)
{
    m_storage->allocateChunk(chunk);
    //m_physics->enableChunk(chunk);
}
void Simulation::leaveSimulation(int2 const& chunk)
{
    //m_physics->disableChunk(chunk);
    m_storage->destroyChunk(chunk);
}


// Main
void Simulation::setTime(DiscreteTime time)
{
    m_serverTime = time;
    m_timeCompensator.add((sint) (m_serverTime - m_localTime));
}
void Simulation::forceTime(DiscreteTime time)
{
    m_serverTime = time;
    resetTimers(time);
}
void Simulation::setPing(ClientId client, DeltaTime ping)
{
    Client* clientPtr = findClient(client);
    if (!clientPtr)
        return;

    // Compute
    clientPtr->setPing(ping);

    // Init all
    if (client == thisClient)
        m_inputLag = clientPtr->lag;
    m_actors->setClientLag(client, clientPtr->lag);

    // Log
    traceTime("[", client, "] Latence"
              ": Lag = ", clientPtr->lag,
              "; Ping = ", ping);
}
void Simulation::resetTimers(DiscreteTime time)
{
    operateTimers([time](IncrementalTimer & timer)
    {
        timer.set(time);
    });
}
void Simulation::advance(DeltaTime deltaTime)
{
    updateTime(deltaTime);
}
void Simulation::flush()
{
    if (!m_thisActionTimer.alarmAll())
        return;

    if (m_actors->thisActor())
    {
        DiscreteTime actionTime = m_thisActionTimer.tick();
        ActorMovement movement = m_actors->applyThisActor(actionTime);
        onAct(actionTime, movement);
    }
}
void Simulation::simulate()
{
    updateSimualtion();

    m_actors->advance(time());
}
void Simulation::updateTime(DeltaTime deltaTime)
{
    // Fix
    sint maxDelta = (sint) info.timeMismatch;
    sint currentDelta = m_timeCompensator.median();
    bool enoughStamps = m_timeCompensator.size() >= m_timeCompensator.limit() / 2;
    if (enoughStamps && abs(currentDelta) > maxDelta)
    {
        DiscreteTime newTime = m_localTime + currentDelta;
        logInfo("Reset time [", m_localTime, "->", newTime, "] ", currentDelta);

        resetTimers(newTime);

        m_timeCompensator.balance();
    }

    // Advance
    m_serverTime += deltaTime;
    operateTimers([deltaTime](IncrementalTimer & timer)
    {
        timer.advance(deltaTime);
    });
    m_localTime = m_frameTimer.time();
}
void Simulation::updateSimualtion()
{
    bool updateActorsCorrection = false;
    bool needStop;
    do
    {
        needStop = true;

        if (m_frameTimer.alarm())
        {
            needStop = false;

            updateActorsCorrection = true;

            // Simulate physics
            /*m_physics->simulate(m_frameTimer.period() * 0.001f, m_frameTimer.tick());
            for (auto& frame : m_physics->flush())
                processSimulatedFrame(frame);*/
            onFlushFrames(m_frameTimer.tick());
        }

    } while (!needStop);

    // Actors correction
    if (updateActorsCorrection)
    {
        DiscreteTime currentTime = m_frameTimer.tick();
        if (!info.trustActors)
        {
            m_actorsBuffer.clear();
            m_actors->flush(currentTime, m_actorsBuffer);
            for (ActorPosition& frame : m_actorsBuffer)
                processActorCorrection(frame);
            onFlushCorrection();
        }
        m_actors->correct(currentTime);
    }
}


// Inject
void Simulation::processSimulatedFrame(ObjectState const& frame)
{
    ObjectInstance& object = m_storage->getObject(frame.id());

    /*object.rebind(frame);
    // HACK
    if (!m_chunkManager->isSimulated(object.chunk()))
    {
        object.rebind(object.frame());
        return;
    }*/
    object.update(frame);

    int2 chunk = object.chunk();
    if (m_chunkManager->isSimulated(chunk))
    {
        // Do something

        // Flush frame
        onSimulateFrame(frame, chunk);
    }
}
void Simulation::processActorCorrection(ActorPosition const& frame)
{
    int2 chunk = chunkOf(frame.position);

    if (m_chunkManager->isOwned(chunk))
    {
        onCorrectActor(frame);
    }
}


// Chunk management
void Simulation::moveClient(ClientId client, int2 const& chunk, uint distance)
{
    m_chunkManager->moveClient(client, chunk, distance);

    // Add
    m_clients.emplace(client, Client(client, info));
}
void Simulation::removeClient(ClientId client)
{
    m_chunkManager->removeClient(client);

    // Remove
    m_clients.erase(client);
}


// Actors
void Simulation::attachActor(ClientId client, ObjectId objectId)
{
    ObjectIterator iterObject = m_storage->findObject(objectId);
    ObjectInstance& object = *iterObject;

    m_actors->attachActor(client, iterObject);
    m_actors->placeActor(client, object.position(), time());
}
void Simulation::receiveCinematic(ClientId client,
                                  DiscreteTime time, ActorMovement const& movement)
{
    m_actors->moveActor(client, movement, time);
}
void Simulation::correctCinematic(ClientId invoker,
                                  ClientId client,
                                  DiscreteTime time, double3 const& position)
{
    int2 chunk = chunkOf(position);
    bool ownedByInvoker = m_chunkManager->owner(chunk) == invoker;
    if (ownedByInvoker)
        m_actors->placeActor(client, position, time);
}


// Raycast
RayhitInfo Simulation::raycast(ClientId observer, ubyte source, ubyte type, DiscreteTime time,
                               ObjectId casterObjectId,
                               double3 const& rayStart, double3 const& rayEnd) const
{
    using namespace math3;
    /// HACK HACK HACK
    vector<pair<float, ObjectId>> hits;
    segment raySegment = segment(static_cast<float3>(rayStart),
                                 static_cast<float3>(rayEnd));
    auto groundHit = intersectPlaneSegment(plane(float3(0.0f, 1.0f, 0.0f), 0.0f), raySegment);
    if (groundHit)
        hits.push_back(make_pair(*groundHit, InvalidObjectId));
    auto actorProcessor = [&](ClientId clientId, ObjectId objectId,
        double3 const& actorPosition, float3 const& look)
    {
        sphere actor = sphere(static_cast<float3>(actorPosition), 0.5f);
        auto k = math3::intersectSphereSegment(actor, raySegment);
        if (!k)
            return;
        if (casterObjectId == objectId)
            return;
        hits.push_back(make_pair(*k, objectId));
    };
    m_actors->process(observer, time, actorProcessor);

    if (hits.empty())
        return none;
    sort(hits.begin(), hits.end());

    pair<float, ObjectId>& hit = hits.front();
    double3 hitPosition = static_cast<double3>(raySegment.put(hit.first));
    int2 hitChunk = chunkOf(hitPosition);
    bool sure = m_chunkManager->isSimulated(chunkOf(rayStart))
        && m_chunkManager->isSimulated(hitChunk);

    if (hit.second == InvalidObjectId)
    {
        return RayhitInfo(hitChunk, hitPosition, sure);
    }
    else
    {
        return RayhitInfo(hit.second, 0, hitChunk, hitPosition, sure);
    }
}
RayhitInfo Simulation::raycastByInvoker(RaycastInfo const& ray) const
{
    double rayLength = 100;
    /// HACK
    ObjectIterator actorObject = m_actors->actorObject(ray.invoker);
    double3 actorPosition = m_actors->selfPosition(ray.invoker, ray.time);
    double3 rayBegin = actorPosition;
    double3 rayEnd = rayBegin + static_cast<double3>(ray.direction) * rayLength;

    return raycast(ray.invoker, ray.source, ray.type, ray.time,
                   actorObject ? actorObject->id() : ObjectIterator(),
                   rayBegin, rayEnd);
}


// Objects
void Simulation::receiveServerDynamic(ObjectState const& frame, int2 const& chunk)
{
    ObjectId id = frame.id();

    // Find or create
    ObjectIterator objectPtr = m_storage->findObject(id);
    if (!objectPtr)
    {
        if (!frame.isComplete())
        {
            logWarning("Object [", id, "] of type [", frame.type(), "] "
                           "is unknown and incomplete");
            return;
        }
        objectPtr = m_storage->addObject(id);
    }

    // Allocate
    ObjectInstance& object = *objectPtr;
    bool isPrepared = m_factory.prepare(object, frame);

    if (!isPrepared)
    {
        m_storage->removeObject(id);
        // Log
        logWarning("Object [", id, "] of type [", frame.type(), "] preparing failed");
        return;
    }

    // Clear position
    object.inject(frame);
    object.update(frame);
}
void Simulation::receiveSupportDynamic(ClientId client,
                                       ObjectState const& frame, int2 const& chunk)
{
    ObjectId id = frame.id();

    // Find
    ObjectIterator objectPtr = m_storage->findObject(id);
    bool isOwned = false;
    if (!objectPtr)
    {
        if (!frame.isComplete())
        {
            logWarning("Object [", id, "] is fat and unknown");
            return;
        }
        objectPtr = m_storage->addObject(id);
    }
    else
    {
        isOwned = m_chunkManager->isOwned(objectPtr->chunk());
    }

    // Ignore if owned
    ObjectInstance& object = *objectPtr;
    if (isOwned && client >= thisClient)
    {
        return;
    }

    bool isPrepared = m_factory.prepare(object, frame);

    if (!isPrepared)
    {
        m_storage->removeObject(id);
        // Log
        logWarning("Object [", id, "] of type [", frame.type(), "] preparing failed");
        return;
    }

    // Update
    object.inject(frame);
    object.update(frame);
}
void Simulation::clearChunk(int2 const& chunk)
{
    m_storage->clearChunk(chunk);
    logDebug("Chunk ", chunk, " is cleared");
}
void Simulation::prepareChunk(int2 const& chunk)
{
    m_chunkManager->prepare(chunk);
    logDebug("Chunk ", chunk, " is simulated");
}
void Simulation::debugDrawChunks(function<void(const char* text, int2 const& chunk) > draw,
                                 int2 const& center, uint distance)
{
    m_chunkManager->debugDraw(draw, center, distance);
}
