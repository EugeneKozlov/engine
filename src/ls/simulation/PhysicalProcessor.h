/** @file ls/simulation/PhysicalProcessor.h
 *  @brief Valuable physical calculations.
 */
#pragma once
#include "ls/common.h"
#include "ls/forward.h"
#include "ls/physics/common.h"
#include "ls/scene/ObjectsManager.h"
#include "ls/scene/ObjectState.h"
#include "ls/world/WorldConfig.h"
#include "ls/world/StreamingWorld.h"


namespace ls
{
    /** @addtogroup LsSimulation
     *  @{
     */
    /** Valuable physical calculations.
     *  This class process all computations influences on gameplay.
     *  It controls enabling and disabling scene objects.
     */
    class PhysicalProcessor
    : public Noncopyable
    , public Nameable<PhysicalProcessor>
    , public WorldObserverInterface
    , public ObjectsManagerObserverInterface
    {
    public:
        /** Ctor.
         *  @param gravity
         *    Scene gravity
         *  @param objects
         *    Objects storage to observe
         */
        PhysicalProcessor(float3 const& gravity,
                          ObjectsManager& objects,
                          StreamingWorld& world);
        /// Dtor
        ~PhysicalProcessor();
        void enableChunk(int2 const& chunk);
        void disableChunk(int2 const& chunk);
        /** Update simulation.
         *  @param timeDelta
         *    Elapsed time (sec)
         *  @param realTime
         *    Real server time
         */
        void simulate(float timeDelta, DiscreteTime realTime);
        /** Flush all actions.
         *  This call will reset action list.
         *  @return Extracted frames
         */
        vector<ObjectState> const& flush();
        /** Perform ray cast.
         *  @param rayStart,rayDir
         *    Ray info
         *  @param [out] object,hitType
         *    Hit info
         *  @param [out] hitPosition
         *    Hit position
         *  @return @c true if hit occurs and hit info is filled
         */
        bool raycast(float3 const& rayStart, float3 const& rayDir,
                     ObjectIterator& object, ubyte& hitType, float3& hitPosition);
    private:
        function<void(ObjectInstance& object) > onStillAwakeCallback;
        void onStillAwake(ObjectInstance& object);

        //virtual void onPreloadRegionDetail(WorldRegion& region) override;
        //virtual void onAsyncLoadRegionDetail(WorldRegion& region) override;
        virtual void onPostloadRegionDetail(WorldRegion& region) override;
        virtual void onUnloadRegionDetail(WorldRegion& region) override;

        virtual void onMoveToChunk(int2 const& chunk) override;
        virtual void onRecenter(int2 const& oldCenter, int2 const& newCenter,
                                double3 const& newCenterPosition) override;
    private:
        virtual void onAddObject(ObjectInstance& object) override;
        virtual void onRemoveObject(ObjectInstance& object) override;
        virtual void onRebindObject(ObjectIterator object,
                                    int2 const& from, int2 const& to) override;
    private:
        /// Scene info
        struct SceneInfo
        {
        public:
            /// Ctor
            SceneInfo() = default;
            /// Cast from @c void
            static SceneInfo& cast(void* ptr)
            {
                return *reinterpret_cast<SceneInfo*>(ptr);
            }
        public:
            /// Center (for world re-centering)
            float3 center;
        };
        /// Physical chunk
        class Chunk;
        /// Get object actor iterator
        ObjectIterator getObject(btCollisionObject& actor);
        /// Log info about corrupted object
        void logCorruptedPhysicalObject(ObjectInstance& object);
    private:
        /// World
        StreamingWorld& m_world;
        /// Config
        WorldConfig const& m_worldConfig;
        /// Config
        WorldStreamingConfig const& m_worldStreamingConfig;
        /// Objects storage
        ObjectsManager& m_objectsManager;
        /// Scene attached info
        SceneInfo m_sceneInfo;
        /// Physical scene
        btSoftRigidDynamicsWorld* m_scene;
        /// Physical area of geometry creation
        uint m_staticRadius;

        /// Current server time
        DiscreteTime m_currentTime = 0;
        /// Active area center
        int2 m_activeAreaCenter;
        /// Awake objects
        ObjectsList m_awakeObjects;
        /// Extracted frames buffer
        vector<ObjectState> m_framesPool;

        /// Created chunks
        map<int2, Chunk> m_createdChunks;
        /// Chunks must be simulated, but not loaded yet
        set<int2> m_chunksToSimulate;
    };
    /// @}
}
