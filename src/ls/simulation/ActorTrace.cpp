#include "pch.h"
#include "ls/simulation/ActorTrace.h"
#include "ls/scene/ObjectInstance.h"

using namespace ls;
ActorTrace::ActorTrace(ObjectIterator actor,
                       ActorTracingInfo const& info,
                       MovementProcessor const& processor,
                       MovementValidator const& validator)
    : Nameable("Actor", (uint) actor->id(), info.logTrace)
    , actorId(actor->id())
    , p(info)

    , onProcessRelative(processor)
    , onValidateMovement(validator)

    , m_actor(actor)
{
    m_movements.emplace(0, ActorMovement());
}
ActorTrace::ActorTrace(ActorTrace&& another)
    : Nameable(another)
    , actorId(another.actorId)
    , p(another.p)

    , onProcessRelative(std::move(another.onProcessRelative))
    , onValidateMovement(std::move(another.onValidateMovement))

    , m_actor(another.m_actor)
    , m_actorLag(another.m_actorLag)
    , m_simulatedFlag(another.m_simulatedFlag)
    , m_lastMovement(another.m_lastMovement)
    , m_lastMovementTime(another.m_lastMovementTime)
    , m_numDuplicates(another.m_numDuplicates)

    , m_mismatchValue(another.m_mismatchValue)
    , m_mismatchFixStage(another.m_mismatchFixStage)

    , m_real(std::move(another.m_real))
    , m_movements(std::move(another.m_movements))
    , m_predicted(std::move(another.m_predicted))
    , m_smooth(std::move(another.m_smooth))
{
}
ActorTrace::~ActorTrace()
{
}


// Progress
DiscreteTime ActorTrace::advance(DiscreteTime time, DeltaTime inputLag, bool isThis)
{
    clear(time);

    // Ignore non-started
    auto iterLastFrame = m_predicted.end();
    if (iterLastFrame == m_predicted.begin())
        return time;

    // Fix time
    DiscreteTime fixedTime = time + p.controlLag;
    if (!isThis)
        fixedTime -= m_actorLag + inputLag;

    auto lastFrame = *--iterLastFrame;
    double3 lastPosition = lastFrame.second.first;
    DiscreteTime lastTime = lastFrame.first;

    // Ignore future
    if (fixedTime < lastTime)
        return fixedTime;

    // Update movement
    uint requiredAdvance = fixedTime / p.actionPeriod + 1 - lastTime / p.actionPeriod;
    while (requiredAdvance > 0)
    {
        auto iterMovement = --m_movements.upper_bound(lastTime);
        ActorMovement& movement = iterMovement->second;
        double3 const& requiredPosition = *movement.position;
        DiscreteTime movementTime = iterMovement->first;
        DeltaTime delay = lastTime - movementTime;

        // Duplicate movement
        bool duplicated = m_lastMovementTime == movementTime;

        // Try to move
        m_simulatedFlag = true;
        double3 newPosition;
        bool immovable = !onProcessRelative(actorId, lastPosition, movement, p.actionPeriod,
                                            newPosition);

        // Cannot move, should rely on client
        if (immovable && !isThis)
        {
            // Disable simulation
            m_simulatedFlag = false;

            // Load position from movement
            newPosition = requiredPosition;
        }

        // Can move, _was_ duplicated
        if (!immovable && !isThis && !duplicated)
        {
            bool valid = onValidateMovement(actorId, lastPosition, newPosition,
                                            m_lastMovement, movement,
                                            m_numDuplicates, p.actionPeriod);
            // We can rely on his position
            if (valid)
            {
                newPosition = requiredPosition;
            }
        }

        // Advance
        --requiredAdvance;
        lastPosition = newPosition;
        lastTime += p.actionPeriod;

        m_lastMovementTime = movementTime;
        m_lastMovement = movement;

        // Update number of duplicates
        if (duplicated)
            ++m_numDuplicates;
        else
            m_numDuplicates = 0;

        // Add
        addPosition(lastTime, lastPosition, duplicated && !m_simulatedFlag);

        // Log
        logTrace("Moved "
                     ": SampleTime = ", lastTime,
                     "; Predicted = ", lastPosition,
                     "; RealTime = ", time,
                     "; FixedTime = ", fixedTime,
                     "; Lag = ", delay,
                     "; NumDups = ", m_numDuplicates);
    }
    return fixedTime;
}
void ActorTrace::addPosition(DiscreteTime time, double3 position, bool duplicated)
{
    // Add predicted
    auto iterLastPredicted = m_predicted.emplace(time,
                                                 make_pair(position, duplicated)).first;
    m_predicted.erase(++iterLastPredicted, m_predicted.end());

    // Fix smooth
    if (m_mismatchFixStage > 0)
    {
        double undoFactor = m_mismatchFixStage / (double) p.numCorrectionFrames;
        --m_mismatchFixStage;
        position -= undoFactor * m_mismatchValue;
    }

    // Add smooth
    auto iterLastSmooth = m_smooth.emplace(time, position).first;
    m_smooth.erase(++iterLastSmooth, m_smooth.end());
}
void ActorTrace::setActorLag(DeltaTime lag)
{
    m_actorLag = lag;
}
void ActorTrace::clear(DiscreteTime time)
{
    auto clearTime = time - p.tracingTime;
    clearMap(m_real, clearTime, false);
    clearMap(m_movements, clearTime, true);
    clearMap(m_predicted, clearTime, false);
    clearMap(m_smooth, clearTime, false);
}


// Input
void ActorTrace::place(DiscreteTime time, double3 const& position,
                       bool resetPredicted)
{
    DiscreteTime roundTime = time / p.actionPeriod * p.actionPeriod;
    m_real.emplace(roundTime, position);
    if (resetPredicted)
        addPosition(roundTime, position, false);

    // Log
    const char* tag = resetPredicted ? "and started " : "";
    logTrace("Placed ", tag,
                 ": Time = ", roundTime,
                 "; Position = ", position);
}
void ActorTrace::move(DiscreteTime time, ActorMovement const& movement)
{
    sint lag = *movement.delay * p.actionTick;
    DiscreteTime fixedTime = max(0, (sint) (time - lag));
    m_movements.emplace(fixedTime, movement);
    //m_averageLag.add(lag);

    logTrace("Movement is registered ",
                 ": Time = ", fixedTime,
                 "; Type = ", *movement.type,
                 "; Lag = ", lag);
}
void ActorTrace::correct(DiscreteTime time, float minDelta)
{
    // No real data to compare
    auto iterRealLeft = m_real.upper_bound(time);
    if (iterRealLeft == m_real.begin())
        return;
    --iterRealLeft;
    if (iterRealLeft == m_real.begin())
        return;
    auto iterRealRight = iterRealLeft--;

    // Has two real positions
    double3 realLeft = iterRealLeft->second;
    double3 realRight = iterRealRight->second;

    // Fix time
    DiscreteTime timeLeft = iterRealLeft->first;
    DiscreteTime timeRight = iterRealRight->first;

    // Test reliability
    auto iterPredictedLeft = m_predicted.find(timeLeft);
    auto iterPredictedRight = m_predicted.find(timeRight);

    bool predictedLeftValid = iterPredictedLeft != m_predicted.end();
    bool predictedRightValid = iterPredictedRight != m_predicted.end();

    bool relyOnLeft = predictedLeftValid ? !iterPredictedLeft->second.second : false;
    bool relyOnRight = predictedLeftValid ? !iterPredictedRight->second.second : false;

    // Cannot test
    if (!relyOnRight)
        return;

    // Find min delta
    static double const almostInfinite = (double) MaxUint;
    double3 deltaRight = realRight - iterPredictedRight->second.first;
    double3 deltaLeft = almostInfinite;
    if (relyOnLeft)
        deltaLeft = realLeft - iterPredictedLeft->second.first;

    double3 delta = deltaRight;
    auto iter = iterPredictedRight;
    if (length2(deltaLeft) < length2(deltaRight))
    {
        delta = deltaLeft;
        iter = iterPredictedLeft;
    }

    // Ignore small delta
    if (length(delta) < minDelta)
        return;
    uint numCorrected = 0;

    for (; iter != m_predicted.end(); ++iter)
    {
        iter->second.first += delta;
        ++numCorrected;
    }

    // Smooth
    m_mismatchFixStage = p.numCorrectionFrames - 1;
    m_mismatchValue = delta;

    // Log
    logTrace("Mismatch ",
                 ": Value = ", delta,
                 "; TestTime = ", time,
                 "; Delay = ", p.correctionDelay,
                 "; NumCorrected = ", numCorrected);
}


// Gets
double3 ActorTrace::predictedPosition(DiscreteTime time) const noexcept
{
    typedef decltype(m_predicted.begin()->second) ValueType;
    ValueType left, right;
    float k = lerpMap(m_predicted, time, ValueType(),
                      left, right);
    return lerp(left.first, right.first, k);
}
double3 ActorTrace::realPosition(DiscreteTime time) const noexcept
{
    return lerpPosition(m_real, time);
}
double3 ActorTrace::smoothPosition(DiscreteTime time) const noexcept
{
    return lerpPosition(m_smooth, time);
}
float3 ActorTrace::lookDirection(DiscreteTime time) const noexcept
{
    ActorMovement left, right;
    float k = lerpMap(m_movements, time, ActorMovement(),
                      left, right);
    return lerp(*left.look, *right.look, k);
}
double3 ActorTrace::lerpPosition(const map<DiscreteTime, double3>& data,
                                 DiscreteTime time,
                                 double3 const& defaultPosition) noexcept
{
    double3 left, right;
    float k = lerpMap(data, time, defaultPosition,
                      left, right);
    return lerp(left, right, k);
}

