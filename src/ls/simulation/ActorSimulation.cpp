#include "pch.h"
#include "ls/simulation/ActorSimulation.h"
#include "ls/simulation/ActorTrace.h"
#include "ls/world/WorldChunkManager.h"

using namespace ls;
ActorMovement const ActorSimulation::DefaultMovement;


// Main
ActorSimulation::ActorSimulation(ClientId thisClient,
                                 WorldChunkManager& chunkManager,
                                 ActorTracingInfo const& info, bool logEvents)
    : Nameable("ActorSimulation")

    , logEvents(logEvents)
    , info(info)
    , thisClient(thisClient)

    , m_chunkManager(chunkManager)

    , m_thisMovement()
{
    m_thisMovement.delay = -(sbyte) (info.controlLag / info.actionTick);
    m_movementProcessor = [this](ObjectId actorId,
        double3 const& oldPosition,
        ActorMovement const& movement,
        DeltaTime timeDelta,
        double3 & newPosition)
    {
        ObjectInstance& object = *getOrFail(m_actorObjects, actorId);
        return moveObject(object, oldPosition, movement, timeDelta * 0.001f, newPosition);
    };
    m_movementValidator = [this](ObjectId actorId,
        double3 const& oldPosition,
        double3 const& newPosition,
        ActorMovement const& duplicatedMovement,
        ActorMovement const& receivedMovement,
        uint numDups,
        DeltaTime timeDelta)
    {
        ObjectInstance& object = *getOrFail(m_actorObjects, actorId);
        return validateMovement(object, oldPosition, newPosition,
                                duplicatedMovement, receivedMovement,
                                numDups, timeDelta);
    };
}
ActorSimulation::~ActorSimulation()
{
}
void ActorSimulation::setClientLag(ClientId client, DeltaTime lag)
{
    ActorTrace* trace = findOrDefault(m_traces, client);
    if (!trace)
        return;

    trace->setActorLag(lag);

    if (client == thisClient)
        m_thisLag = lag;
}


// Global management
void ActorSimulation::advance(DiscreteTime time)
{
    for (auto& iterTrace : m_traces)
    {
        bool isThis = iterTrace.first == thisClient;
        ActorTrace& trace = iterTrace.second;
        ObjectInstance& object = *getOrFail(m_actorObjects, trace.actorId);

        // Simulate
        DiscreteTime fixedTime = trace.advance(time, m_thisLag, isThis);

        // Update
        double3 newPosition = trace.smoothPosition(min(fixedTime, time));
        object.position(newPosition);
    }
}
void ActorSimulation::flush(DiscreteTime time, vector<ActorPosition>& frames)
{
    for (auto& iterTrace : m_traces)
    {
        ClientId clientId = iterTrace.first;
        ActorTrace& trace = iterTrace.second;
        bool isThis = clientId == thisClient;

        // Update
        trace.advance(time, m_thisLag, isThis);

        // Get flush position
        DiscreteTime flushTime = time + info.controlLag;
        if (!isThis)
            flushTime -= m_thisLag + trace.lag();
        flushTime = flushTime / info.actionPeriod * info.actionPeriod;
        double3 flushPosition = trace.predictedPosition(flushTime);

        // Put
        frames.push_back(ActorPosition(clientId, flushTime, flushPosition));
    }
}
void ActorSimulation::correct(DiscreteTime time)
{
    for (auto& iterTrace : m_traces)
    {
        ActorTrace& trace = iterTrace.second;

        // Fix
        DiscreteTime correctionTime = time - info.correctionDelay;
        trace.correct(correctionTime, 0.6f);
    }
}


// Actors
void ActorSimulation::attachActor(ClientId client, ObjectIterator object)
{
    debug_assert(!!object);

    ObjectId objectId = object->id();

    if (m_actors.count(client) != 0)
    {
        logWarning("Client [", client, "] already "
                       "has attached object [", m_actors[client]->id(), "]");
        return;
    }

    m_actorObjects.emplace(objectId, object);
    m_actors.emplace(client, object);
    m_traces.emplace(client,
                     ActorTrace(object, info,
                                m_movementProcessor, m_movementValidator));

    if (client == thisClient)
        m_thisActor = object;

    logInfo("Client [", client, "] attaches object [", objectId, "]");
}
void ActorSimulation::placeActor(ClientId client,
                                 double3 const& position, DiscreteTime time)
{
    // Not found
    ActorTrace* trace = findOrDefault(m_traces, client);
    if (!trace)
    {
        traceEvent("(!) Client [", client, "] is not found");
        return;
    }

    // Place
    bool forseReset = trace->isEmpty();
    trace->place(time, position, forseReset);
}
void ActorSimulation::moveActor(ClientId client,
                                ActorMovement const& movement, DiscreteTime time)
{
    // Not found
    ActorTrace* trace = findOrDefault(m_traces, client);
    if (!trace)
    {
        traceEvent("(!) Client [", client, "] is not found");
        return;
    }

    // Ignore this
    DiscreteTime messageTime = time - *movement.delay * info.actionTick;
    if (client == thisClient)
    {
        trace->logTrace("Ignored "
                            ": Time = ", messageTime,
                            "; Movement = ", *movement.type);
        return;
    }

    // Move
    trace->move(time, movement);
    if (info.trustActors)
        trace->place(messageTime + info.actionPeriod, *movement.position, false);
}


// This actor
void ActorSimulation::moveThisActor(ActorMovement const& movement)
{
    ActorTrace* trace = findOrDefault(m_traces, thisClient);
    if (!trace)
    {
        logWarning("This client [", thisClient, "] is not found");
        return;
    }

    m_thisMovement.look = movement.look;
    m_thisMovement.type = movement.type;
    m_thisMovement.flags = movement.flags;
}
ActorMovement const& ActorSimulation::applyThisActor(DiscreteTime time)
{
    ActorTrace* trace = findOrDefault(m_traces, thisClient);
    if (!trace)
    {
        logWarning("This client [", thisClient, "] is not found");
        return DefaultMovement;
    }

    trace->move(time, m_thisMovement);
    trace->advance(time, 0, true);
    *m_thisMovement.position = trace->lastPredictedPosition();
    return m_thisMovement;
}


// Gets
void ActorSimulation::imagineActor(DeltaTime observerLag,
                                   bool isSelf, DiscreteTime time,
                                   ActorTrace const& trace,
                                   double3* position, float3* look) const noexcept
{
    DiscreteTime fixedTime = time + info.controlLag;
    if (!isSelf)
        fixedTime -= trace.lag() + observerLag;
    fixedTime = min(fixedTime, time);

    if (position)
        *position = trace.predictedPosition(fixedTime);
    if (look)
        *look = trace.lookDirection(fixedTime);
}


// Physical
bool ActorSimulation::moveObject(ObjectInstance& object,
                                 double3 const& oldPosition,
                                 ActorMovement const& movement,
                                 float deltaTime, double3& newPosition)
{
    newPosition = oldPosition;
    bool simulated = m_chunkManager.isSimulated(object.chunk());
    if (!simulated)
        return false;

    ActorMovement::Type mov = (ActorMovement::Type) *movement.type;
    float3 look = *movement.look;

    /// HACK
    float const vel = 5.0f;
    if (object.id() != m_thisActor.index() && thisClient == 0)
        ; // Mov = ActorMovement::No;

    // Axises
    float3 zaxis = look;
    float3 yaxis = float3(0.0f, 1.0f, 0.0f);
    float3 xaxis = cross(yaxis, zaxis);
    yaxis = cross(zaxis, xaxis);

    // Velocities
    float zvel = mov.is(ActorMovement::Forward) ? 1.0f : (mov.is(ActorMovement::Backward) ? -1.0f : 0.0f);
    float xvel = mov.is(ActorMovement::Right) ? 1.0f : (mov.is(ActorMovement::Left) ? -1.0f : 0.0f);
    float yvel = mov.is(ActorMovement::Up) ? 1.0f : (mov.is(ActorMovement::Down) ? -1.0f : 0.0f);

    // Move
    float3 velocity = normalize(zaxis * zvel + yaxis * yvel + xaxis * xvel) * vel;
    float3 delta = velocity * deltaTime;

    newPosition += static_cast<double3>(delta);
    return true;
}
bool ActorSimulation::validateMovement(ObjectInstance& /*object*/,
                                       double3 const& oldPosition,
                                       double3 const& /*newPosition*/,
                                       ActorMovement const& duplicatedMovement,
                                       ActorMovement const& receivedMovement,
                                       uint numDups, DeltaTime timeDelta)
{
    // No static fix
    if (numDups == 0)
        return false;

    double3 requiredPosition = *receivedMovement.position;
    double movementDistance = distance(requiredPosition, oldPosition);

    /// HACK
    float const vel = 10.0f;
    double maxMovementDistance = vel * (double) timeDelta * 1.01;

    if (movementDistance > maxMovementDistance)
        return false;

    return true;
}
