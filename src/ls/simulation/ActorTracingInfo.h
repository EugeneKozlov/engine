/** @file ls/simulation/ActorTracingInfo.h
 *  @brief Actor tracing info
 */
#pragma once
#include "ls/common.h"

namespace ls
{
    /** @addtogroup LsSimulation
     *  @{
     */
    /** Actor tracing info
     */
    struct ActorTracingInfo
    {
        /// Ctor
        ActorTracingInfo(DeltaTime tracingTime, DeltaTime framePeriod,
                         DeltaTime actionPeriod, DeltaTime actionTick,
                         DeltaTime controlLag, DeltaTime correctionDelay,
                         bool trustActors, bool logTrace)
            : tracingTime(tracingTime)
            , framePeriod(framePeriod)
            , actionPeriod(actionPeriod)
            , actionTick(actionTick)
            , controlLag(controlLag)

            , correctionDelay(correctionDelay)

            , numCorrectionFrames(framePeriod / actionPeriod)
            , trustActors(trustActors)
            , logTrace(logTrace)
        {
        }
        /// Tracing time duration
        DeltaTime tracingTime = 0;
        /// Frame period
        DeltaTime framePeriod = 0;
        /// Character action time period
        DeltaTime actionPeriod = 0;
        /// Each tick time delta
        DeltaTime actionTick = 0;
        /// Client control lag
        DeltaTime controlLag = 0;

        /// Time to fix position mismatch
        DeltaTime correctionDelay = 0;

        /// Number of action periods used to smooth correction
        uint numCorrectionFrames = 0;

        /// Set to trust in actor positions
        bool trustActors = false;
        /// Set to log any trace sample
        bool logTrace = false;
    };
    /// @}
}
