/** @file ls/simulation/ActorTrace.h
 *  @brief Actor path tracer
 */
#pragma once
#include "ls/common.h"
#include "ls/core/Nameable.h"
#include "ls/misc/Mediator.h"
#include "ls/simulation/ActorMovement.h"
#include "ls/simulation/ActorTracingInfo.h"
#include "ls/scene/common.h"
#include "ls/network/common.h"

namespace ls
{
    /** @addtogroup LsSimulation
     *  @{
     */
    /** Actor trace.
     *
     *  <b>Real:</b>
     *  - Positions are stored
     *  - Update on @a place
     *  - Used for position correction
     *  - If empty, assume all predicted positions are valid (no correction)
     *
     *  <b>Movement:</b>
     *  - Relative movements are stored
     *  - Update on @a move
     *  - Used for further computation
     *  - Relative movement contains absolute position
     *    which can be used if simulation is not available
     *
     *  <b>Predicted:</b>
     *  - Computed position
     *  - Predicted positions are basing on actor movement and may be invalid
     *  - Position from movement may be used if simulation is not possible
     *  - Duplicate positions from movement are unreliable and never corrected
     *  - Will be fixed as soon as possible if mismatch is detected
     *  - You should @a correct positions on each frame update after @a advance call
     *  - Correction time is rounded down to nearest real frame
     *
     *  <b>Smooth:</b>
     *  - Contains smoothly interpolated and corrected positions
     *  - For rendering only
     *
     *  @see ActorSimulation
     */
    class ActorTrace
    : public Noncopyable
    , public Nameable<ActorTrace>
    {
    public:
        /// Latency prediction stamps
        static uint const LatenceStamps = 3;
        /// Movement process callback
        using MovementProcessor = function<bool(
            ObjectId actorId,
            double3 const& oldPosition,
            ActorMovement const& movement,
            DeltaTime timeDelta,
            double3& newPosition)>;
        /// Movement validate callback
        using MovementValidator = function<bool(
            ObjectId actorId,
            double3 const& oldPosition,
            double3 const& newPosition,
            ActorMovement const& duplicatedMovement,
            ActorMovement const& receivedMovement,
            uint numDups,
            DeltaTime timeDelta)>;
    public:
        /// Ctor
        ActorTrace(ObjectIterator actor,
                   ActorTracingInfo const& info,
                   MovementProcessor const& processor,
                   MovementValidator const& validator);
        /// Move
        ActorTrace(ActorTrace&& another);
        /// Dtor
        ~ActorTrace();
        /// Set @a time and this client @a inputLag; update actor
        DiscreteTime advance(DiscreteTime time, DeltaTime inputLag, bool isThis);
        /// Set actor lag (remote client lag)
        void setActorLag(DeltaTime lag);
        /// Clear out-of-time
        void clear(DiscreteTime time);

        /// Add reliable position at @a time
        void place(DiscreteTime time, double3 const& position,
                   bool resetPredicted);
        /// Add relative @a movement at @a time
        void move(DiscreteTime time, ActorMovement const& movement);
        /// Correct actor using real and predicted positions at @a time
        void correct(DiscreteTime time, float minDelta);

        /** @name Gets
         *  @{
         */
        /// Get remote client lag
        DeltaTime lag() const noexcept
        {
            return m_actorLag;
        }
        /// Test if empty
        bool isEmpty() const noexcept
        {
            return m_predicted.empty();
        }
        /// Test if simulated
        bool isSimulated() const noexcept
        {
            return m_simulatedFlag;
        }
        /// Get actor
        ObjectIterator actor() const noexcept
        {
            return m_actor;
        }
        /// Get last time
        DiscreteTime uptime() const noexcept
        {
            return (--m_movements.end())->first;
        }
        /// Get predicted position at @a time
        double3 predictedPosition(DiscreteTime time) const noexcept;
        /// Get real position at @a time
        double3 realPosition(DiscreteTime time) const noexcept;
        /// Get smooth position at @a time
        double3 smoothPosition(DiscreteTime time) const noexcept;
        /// Get look direction
        float3 lookDirection(DiscreteTime time) const noexcept;
        /// Get last predicted position
        double3 lastPredictedPosition() const noexcept
        {
            debug_assert(!m_predicted.empty());
            return (--m_predicted.end())->second.first;
        }
        /// @}
    private:
        /// Clear map
        template <class Map>
        static void clearMap(Map& map, DiscreteTime time, bool safeFirst)
        {
            uint minElems = safeFirst ? 2 : 1;
            if (map.size() <= minElems)
                return;

            auto iterLast = map.lower_bound(time);
            auto iterBegin = map.begin();
            if (safeFirst)
                ++iterBegin;
            if (iterLast == map.end())
                --iterLast;
            map.erase(iterBegin, iterLast);
        }
        /// Get map
        template <class Map>
        static float lerpMap(Map const& map, DiscreteTime time,
                             typename const Map::mapped_type& defaultValue,
                             typename Map::mapped_type& left,
                             typename Map::mapped_type& right)
        {
            if (map.empty())
            {
                left = right = defaultValue;
                return 0;
            }

            // Find upper
            auto iterLeft = map.upper_bound(time);

            // Left miss
            if (iterLeft == map.begin())
            {
                left = right = (map.begin())->second;
                return 0;
            }

            // Right miss
            if (iterLeft == map.end())
            {
                left = right = (--map.end())->second;
                return 0;
            }

            // Interpolate
            auto iterRight = iterLeft--;
            float factor = min<float>(float(time - iterLeft->first)
                                      / (iterRight->first - iterLeft->first), 1.0f);

            left = iterLeft->second;
            right = iterRight->second;
            return factor;
        }
        /// Get interpolated position
        static double3 lerpPosition(const map<DiscreteTime, double3>& data,
                                    DiscreteTime time,
                                    double3 const& defaultPosition = double3(0.0f)) noexcept;
        /// Add and fix absolute position
        void addPosition(DiscreteTime time, double3 position, bool duplicated);
    public:
        /// Traced actor ID
        ObjectId const actorId;
        /// Tracing parameters
        ActorTracingInfo const& p;
    private:
        /// Relative frame processor
        MovementProcessor onProcessRelative;
        /// Client movement validator
        MovementValidator onValidateMovement;

        /// Actor
        ObjectIterator m_actor;
        /// Tracing actor lag
        DeltaTime m_actorLag = 0;
        /// Is trace simulated?
        bool m_simulatedFlag = false;
        /// Last used movement
        ActorMovement m_lastMovement;
        /// Last used movement time
        DiscreteTime m_lastMovementTime;
        /// Number of last movement duplicates
        uint m_numDuplicates = 0;

        /// Position mismatch to be corrected
        double3 m_mismatchValue;
        /// Mismatch correction stage
        uint m_mismatchFixStage;

        /// Real positions
        map<DiscreteTime, double3> m_real;
        /// Relative movements
        map<DiscreteTime, ActorMovement> m_movements;
        /// Predicted absolute frames and reliability flag
        map<DiscreteTime, pair<double3, bool>> m_predicted;
        /// Smooth positions
        map<DiscreteTime, double3> m_smooth;
    };
    /// @}
}
