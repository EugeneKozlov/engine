/** @file ls/simulation/Simulation.h
 *  @brief Main simulation class
 */
#pragma once
#include "ls/common.h"
#include "ls/forward.h"
#include "ls/core/Timer.h"
#include "ls/core/Nameable.h"
#include "ls/misc/Mediator.h"
#include "ls/simulation/RayhitInfo.h"
#include "ls/simulation/SimulationInfo.h"
#include "ls/simulation/ActorTracingInfo.h"
#include "ls/simulation/ActorPosition.h"

namespace ls
{
    class StreamingWorld;

    /** @addtogroup LsSimulation
     *  @{
     */
    /** World simulation.
     *
     *  Right order of update:
     *  @code{.cpp}
     *  sim.advance(deltaTime);
     *  sim.flush();
     *  // Server.update(deltaTime);
     *  // Sim.forceTime(server.time());
     *  sim.simulate();
     *  @endcode
     */
    class Simulation
    : public Noncopyable
    , public Nameable<Simulation>
    {
    public:
        /// Send this client action
        function<void(DiscreteTime time, ActorMovement const& movement) > onAct;
        /// Send actors correction info
        function<void(ActorPosition const& frame) > onCorrectActor;
        /// Flush actors correction data
        function<void() > onFlushCorrection;

        /// Add simulated frame
        function<void(ObjectState const& frame, int2 const& chunk) > onSimulateFrame;
        /// Flush all simulated frames
        function<void(DiscreteTime time) > onFlushFrames;
    public:
        /** Ctor.
         *  @param info
         *    Simulation info (constants)
         *  @param factory
         *    Objects factory
         *  @param worldData
         *    World source data
         *  @param startTime
         *    Initial time
         */
        Simulation(SimulationInfo const& info,
                   ObjectsFactory& factory,
                   StreamingWorld& worldData,
                   DiscreteTime startTime);
        /// Dtor
        ~Simulation();
        /// Set sim time by rough server @a time
        void setTime(DiscreteTime time);
        /// Set sim time by precise server @a time
        void forceTime(DiscreteTime time);
        /** Set client ping
         *  @param client
         *    Client ID
         *  @param ping
         *    Client-server ping
         */
        void setPing(ClientId client, DeltaTime ping);
        /// Increase time
        void advance(DeltaTime deltaTime);
        /// Update output (this client actions)
        void flush();
        /// Simulate the world
        void simulate();

        /// Move @a client to @a chunk with inner @a distance
        void moveClient(ClientId client, int2 const& chunk, uint distance);
        /// Remove @a client
        void removeClient(ClientId client);

        /// Attach character @a objectId to @a client
        void attachActor(ClientId client, ObjectId objectId);
        /// Receive @a client character @a movement related to @a time
        void receiveCinematic(ClientId client,
                              DiscreteTime time, ActorMovement const& movement);
        /// Correct @a client character: at the @a time it was at @a position
        void correctCinematic(ClientId invoker, ClientId client,
                              DiscreteTime time, double3 const& position);

        /// Ray cast
        RayhitInfo raycast(ClientId observer, ubyte source, ubyte type,
                           DiscreteTime time,
                           ObjectId casterObjectId,
                           double3 const& rayStart, double3 const& rayEnd) const;
        /// Ray cast by invoker
        RayhitInfo raycastByInvoker(RaycastInfo const& ray) const;

        /// Receive object @a frame from server (message is related to @a chunk)
        void receiveServerDynamic(ObjectState const& frame, int2 const& chunk);
        /// Receive object @a frame from support (message is related to @a chunk)
        void receiveSupportDynamic(ClientId client,
                                   ObjectState const& frame, int2 const& chunk);
        /// Clear all objects bound to @a chunk
        void clearChunk(int2 const& chunk);
        /// Marks chunk as ready-to-simulation
        void prepareChunk(int2 const& chunk);

        /** @name Gets
         *  @{
         */
        /// Get time
        DiscreteTime time() const
        {
            return m_localTime;
        }
        /// Get lag
        DeltaTime lag() const noexcept
        {
            return m_inputLag;
        }
        /// Get chunk manager
        WorldChunkManager const& chunkManager() const noexcept
        {
            return *m_chunkManager;
        }
        /// Get objects manager
        ObjectsManager& objectsManager() noexcept
        {
            return *m_storage;
        }
        /// Get objects factory
        ObjectsFactory& objectsFactory() noexcept
        {
            return m_factory;
        }
        /// Actor simulation
        ActorSimulation& actors() noexcept
        {
            return *m_actors;
        }
        /// Get chunk by position
        int2 chunkOf(float3 const& position) const noexcept
        {
            return static_cast<int2>(upcast(swiz<X, Z>(position), (float) info.chunkWidth));
        }
        /// Get chunk by position
        int2 chunkOf(double3 const& position) const noexcept
        {
            return static_cast<int2>(upcast(swiz<X, Z>(position), (float) info.chunkWidth));
        }
        /// @}

        /// Debug draw chunks
        void debugDrawChunks(function<void(const char* text, int2 const& chunk) > draw,
                             int2 const& center, uint distance);
    private:
        void enterSimulation(int2 const& chunk);
        void leaveSimulation(int2 const& chunk);
    private:
        template <class ... Args>
        inline void traceTime(Args const& ... args) const
        {
            if (info.traceTime)
                this->logTrace(args...);
        }
    private:
        /// Client info
        struct Client;
        /// Find client
        Client* findClient(ClientId id);

        /// Set timers
        void resetTimers(DiscreteTime time);
        /// Update time
        void updateTime(DeltaTime deltaTime);
        /// Update simulation
        void updateSimualtion();

        /// Process simulated frame
        void processSimulatedFrame(ObjectState const& frame);
        /// Process simulated actor correction
        void processActorCorrection(ActorPosition const& frame);
    public:
        /// This client ID
        ClientId const thisClient;
        /// Simulation info
        SimulationInfo const info;
        /// Actor tracing info
        ActorTracingInfo const tracingInfo;
    private:
        /// Chunks manager
        unique_ptr<WorldChunkManager> m_chunkManager;
        /// World storage
        StreamingWorld& m_worldData;
        /// Objects factory
        ObjectsFactory& m_factory;

        /// All clients
        map<ClientId, Client> m_clients;

        /// Local time
        DiscreteTime m_localTime;
        /// Server time
        DiscreteTime m_serverTime;
        /// Server-local time deltas
        Mediator<sint> m_timeCompensator;

        /// Server-client lag (this)
        DeltaTime m_inputLag = 0;

        /// This client action timer
        IncrementalTimer m_thisActionTimer;
        /// Frame timer
        IncrementalTimer m_frameTimer;
        /// Actor applying timer
        IncrementalTimer m_actorTimer;
        /// Operate all timers
        template <class Operator>
        inline void operateTimers(Operator const& op)
        {
            op(m_thisActionTimer);
            op(m_frameTimer);
            op(m_actorTimer);
        }

        /// Objects storage
        unique_ptr<ObjectsManager> m_storage;

        /// Cinematic simulation
        unique_ptr<ActorSimulation> m_actors;
        /// Physics simulation
        //unique_ptr<PhysicalProcessor> m_physics;

        /// Actor frames buffer
        vector<ActorPosition> m_actorsBuffer;
    };
    /// @}
}
