/** @file ls/simulation/RaycastInfo.h
 *  @brief Ray cast info
 */
#pragma once
#include "ls/common.h"
#include "ls/forward.h"

namespace ls
{
    /** @addtogroup LsSimulation
     *  @{
     */
    /** Ray cast info
     */
    struct RaycastInfo
    {
        /// Client invokes ray cast
        ClientId invoker;
        /// Ray cast time
        DiscreteTime time;

        /// Caster object ID
        ObjectId object;
        /// Ray cast source
        ubyte source;
        /// Ray cast type
        ubyte type;

        /// Ray cast direction
        float3 direction;
    };
    /// Raycast key
    struct RaycastKey
        : HashableConcept
        , PrintableConcept
    {
        /// Ctor
        RaycastKey() = default;
        /// Ctor by ray
        RaycastKey(RaycastInfo const& ray)
            : invoker(ray.invoker)
            , time(ray.time)
        {
        }
        /// Ctor by info
        RaycastKey(ClientId invoker, DiscreteTime time)
            : invoker(invoker)
            , time(time)
        {
        }
        /// Hashable
        uint hash() const noexcept
        {
            return time << 16 | (uint) invoker;
        }
        /// Printable
        void toString(String& str) const
        {
            str.print(invoker, "(", time, ")");
        }
        /// Comparable
        bool operator == (RaycastKey const& another) const
        {
            return invoker == another.invoker && time == another.time;
        }
    public:
        /// Caster
        ClientId invoker;
        /// Cast time
        DiscreteTime time;
    };
    /// @}
}
