/** @file ls/simulation/SimulationInfo.h
 *  @brief Simulation info
 */
#pragma once
#include "ls/common.h"
#include "ls/forward.h"

namespace ls
{
    /** @addtogroup LsSimulation
     *  @{
     */
    /** Simulation info
     */
    struct SimulationInfo
    {
        /// This client ID
        ClientId thisClient;

        /// Frame period
        DeltaTime framePeriod;
        /// Action period
        DeltaTime actionPeriod;
        /// Action tick
        DeltaTime actionTick;
        /// Client control lag
        DeltaTime controlLag;

        /// Time mismatch
        DeltaTime timeMismatch;
        /// Chunk width
        uint chunkWidth;
        /// Max sim distance
        uint maxSimDistance;
        /// Sim distance pad
        uint simDistancePad;
        /// Outer distance pad (i.e. 1)
        uint outerDistancePad;

        /// Number of time stamps for time approximation
        uint numTimeStamps;
        /// Actor and object tracing duration
        DeltaTime tracingDuration;
        /// Correction delay
        DeltaTime correctionDelay;

        /// Minimal accepted lag
        DeltaTime minLag;
        /// Extra lag to amortise ping instability
        DeltaTime extraLag;
        /// Max accepted lag error
        DeltaTime lagError;

        /// Set to trust in actor positions
        bool trustActors;
        /// Set to trust in raycast results
        bool trustRaycasts;

        /// Set to trace actors
        bool traceActors;
        /// Set to trace chunks
        bool traceChunks;
        /// Set to trace time
        bool traceTime;
    };
    /// @}
}
