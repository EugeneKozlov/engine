/** @file ls/simulation/ActorMovement.h
 *  @brief Actor encoded movement
 */

#pragma once
#include "ls/common.h"
#include "ls/io/Encoded.h"

namespace ls
{
    /** @addtogroup LsSimulation
     *  @{
     */
    /** Controlled actor movement action
     */
    class ActorMovement
    : public OperableConcept
    {
    public:
        /// Direction type
        using DirectionEncoding = ushort;
        /// Directions
        enum Direction : DirectionEncoding
        {
            /// No
            No = 0,
            /// X-axis positive
            AxisXPositive = 1 << 0,
            /// X-axis negative
            AxisXNegative = 1 << 1,
            /// Y-axis positive
            AxisYPositive = 1 << 2,
            /// Y-axis negative
            AxisYNegative = 1 << 3,
            /// Z-axis positive
            AxisZPositive = 1 << 4,
            /// Z-axis negative
            AxisZNegative = 1 << 5,
            /// Forward
            Forward = AxisZPositive,
            /// Backward
            Backward = AxisZNegative,
            /// Up
            Up = AxisYPositive,
            /// Down
            Down = AxisYNegative,
            /// Left
            Left = AxisXNegative,
            /// Right
            Right = AxisXPositive
        };
        /// Type
        using Type = FlagSet<Direction, DirectionEncoding>;
    public:
        /// Position
        Encoded<double3, Encoding::VectorFixed3> position;
        /// Movement type
        Encoded<DirectionEncoding, Encoding::Short> type = 0;
        /// Movement flags
        Encoded<ubyte, Encoding::Byte> flags = 0;
        /// Delay
        Encoded<sbyte, Encoding::Byte> delay = 0;
        /// Look axis
        Encoded<float3, Encoding::DirectionSphereLength> look;
    public:
        /// Default
        ActorMovement() = default;
        /// Ctor
        ActorMovement(Type type, float3 const& look, ubyte flags = 0)
            : position()
            , type(type)
            , flags(flags)
            , delay()
            , look(look)
        {
        }
        /// Operate
        template<class Function>
        inline void operate(Function& foo)
        {
            foo(type);
            foo(flags);
            foo(delay);
            foo(look);
            foo(position);
        }
    };
    /// @}
}