/** @file ls/simulation/ActorSimulation.h
 *  @brief Actor simulation
 */
#pragma once
#include "ls/common.h"
#include "ls/core/Nameable.h"
#include "ls/core/Timer.h"
#include "ls/network/common.h"
#include "ls/simulation/ActorTrace.h"
#include "ls/simulation/ActorMovement.h"
#include "ls/simulation/ActorPosition.h"
#include "ls/simulation/ActorTracingInfo.h"
#include "ls/scene/ObjectInstance.h"

namespace ls
{
    /** @addtogroup LsSimulation
     *  @{
     */
    /** Cinematic simulation.
     *
     *  <b>Server-side processing:</b>
     *  - Each movement message from client will be sent during next server tick
     *  - Movement time is unmodified
     *
     *  <b>Client-side processing:</b>
     *  - This actor is always moved in real-time
     *  - Each other actor has maximum accepted @b Lag for incoming movements
     *    + @b Lag = ActorLag + InputLag - ControlLag
     *    + @b ActorLag is lag of actor (latency of foreign client)
     *    + @b InputLag is this client latency
     *    + @b ControlLag is artifical lag of actor control (suppresses network lag)
     *  - @b FixedTime = CurrentTime - Lag
     *  - Next position is emitted when @b FixedTime reaches next tick
     *  - Next position is computed basing on received movement
     *  - Previous movement is used if current movement is not received yet
     *  - Next position may be received from movement if the following conditions are met:
     *    + One or more messages are lost (last movement was re-used a couple of times)
     *    + Mismatch of required and predicted positions is not very large,
     *      e.g. less than movement during tick
     *  - Far actors are placed using position from movement data
     *  - This positions are unreliable and may be fixed by corrector
     *
     *  <b>Actors correction:</b>
     *  - Correction should be performed right after flushing
     *  - Each super-client sends positions of actors in his owning at @b FlushTime
     *    + @b FlushTime = [Time + ControlLag - ThisActor ? 0 : (ActorLag + InputLag)]
     *    + @b FlushTime is rounded down
     *  - Each client compares actor positions at @b TestTime
     *    + @b TestTime = Time - CorrectionDelay
     *    + @b CorrectionDelay is configurable value
     *    + @b CorrectionDelay >= MAX{ActorLag + InputLag - ControlLag} + MAX{SuperClientLag}
     *    + @b TestTime is rounded down to nearest received real position
     *  - Comparison is performed for two real stamps
     *    + 'right' is stamp at @b TestTime
     *    + 'left' is stamp right before 'right'
     *  - The predicted positions are compared with real ones at 'left' and 'right' time
     *    + Positions from duplicated movements are unreliable and never corrected
     *    + If predicted position at 'right' time is unreliable, no correction is performed
     *    + If predicted position at 'left' time is unreliable, only 'right' comparison is performed
     *    + Smallest mismatch is @b Delta
     *  - If @b Delta is less than some minimal delta, no correction is performed
     *  - Otherwise actor predicted positions are corrected by @b Delta
     *    starting from 'left' or 'right' time accordingly
     *
     *  <b>Confiding mode:</b>
     *  - Correction data are never sent
     *  - Correction is performed basing on positions from movement
     *
     *  @see ActorTrace
     */
    class ActorSimulation
    : public Noncopyable
    , public Nameable<ActorSimulation>
    {
    public:
        /** Ctor
         *  @param thisClient
         *    This client ID
         *  @param chunkManager
         *    Chunk manager
         *  @param info
         *    Tracing parameters
         *  @param logEvents
         *    Set to log any action event
         */
        ActorSimulation(ClientId thisClient,
                        WorldChunkManager& chunkManager,
                        ActorTracingInfo const& info,
                        bool logEvents = true);
        /// Dtor
        ~ActorSimulation();
        /// Set any actor client-server lag
        void setClientLag(ClientId client, DeltaTime lag);
        /// Set time (time must not decrease)
        void advance(DiscreteTime time);
        /** Flush all movements.
         *  @param time
         *    Flush time
         *  @param [out] frames
         *    Extracted frames
         */
        void flush(DiscreteTime time, vector<ActorPosition>& frames);
        /** Correct all actors
         *  @param time
         *    Current time
         */
        void correct(DiscreteTime time);
        /** Process all actors, retrieving poses at specified moment of time
         *  in point of view specified client
         *  @param observer
         *    Observer
         *  @param time
         *    Processing time
         *  @param callback
         *    @code callback(clientId, objectId, position, look) @endcode
         *  @return @c true on success
         */
        template <class Callback>
        bool process(ClientId observer, DiscreteTime time, Callback callback)
        {
            // Find trace
            ActorTrace* observerTrace = findOrDefault(m_traces, observer);
            if (!observerTrace)
                return false;
            DeltaTime observerLag = observerTrace->lag();

            // Process
            for (auto& iterTrace : m_traces)
            {
                ClientId clientId = iterTrace.first;
                ActorTrace& trace = iterTrace.second;
                bool self = observer == clientId;

                double3 position;
                float3 look;
                imagineActor(observerLag, self, time, trace,
                             &position, &look);
                callback(clientId, trace.actorId, position, look);
            }
            return true;
        }

        /** Attach actor to client.
         *  @param client
         *    Client ID
         *  @param object
         *    Controlled object
         */
        void attachActor(ClientId client, ObjectIterator object);
        /** Place actor.
         *  @param client
         *    Client to move
         *  @param position
         *    Target position
         *  @param time
         *    Sample time
         */
        void placeActor(ClientId client, double3 const& position, DiscreteTime time);
        /** Move actor.
         *  @param client
         *    Client to move
         *  @param movement
         *    Client movement
         *  @param time
         *    Sample time
         */
        void moveActor(ClientId client,
                       ActorMovement const& movement, DiscreteTime time);

        /** Move this actor.
         *  @param movement
         *    Compiled movement
         */
        void moveThisActor(ActorMovement const& movement);
        /** Apply this actor movement
         *  @param time
         *    Current time
         *  @return This movement @b after applying
         */
        ActorMovement const& applyThisActor(DiscreteTime time);

        /** @name Gets
         *  @{
         */
        /// Get this actor
        ObjectIterator thisActor() const noexcept
        {
            return m_thisActor;
        }
        /// Get actor object
        ObjectIterator actorObject(ClientId clientId) const noexcept
        {
            const ObjectIterator* object = findOrDefault(m_actors, clientId);
            if (!object)
                return ObjectIterator();
            else
                return *object;
        }
        /// Get self actor position at @a time from its point of view
        double3 selfPosition(ClientId clientId, DiscreteTime time) const noexcept
        {
            const ActorTrace* trace = findOrDefault(m_traces, clientId);
            debug_assert(trace);

            double3 position;
            imagineActor(0, true, time, *trace, &position, nullptr);
            return position;
        }
        /// @}
    private:
        /// This movement
        static ActorMovement const DefaultMovement;
        /// Log actions
        template <class ... Args>
        inline void traceEvent(Args const& ... args) const
        {
            if (logEvents)
                logTrace(args...);
        }
        /// Get actor pose at @a time from the observer point of view
        void imagineActor(DeltaTime observerLag,
                          bool isSelf, DiscreteTime time,
                          ActorTrace const& trace,
                          double3* position, float3* look) const noexcept;
        /// Move single actor
        bool moveObject(ObjectInstance& object,
                        double3 const& oldPosition,
                        ActorMovement const& movement,
                        float deltaTime,
                        double3& newPosition);
        /// Validate actor
        bool validateMovement(ObjectInstance& object,
                              double3 const& oldPosition,
                              double3 const& newPosition,
                              ActorMovement const& duplicatedMovement,
                              ActorMovement const& receivedMovement,
                              uint numDups,
                              DeltaTime timeDelta);
    public:
        /// Set to log any action event
        bool const logEvents;
        /// Tracing parameters
        ActorTracingInfo const info;
        /// This client ID
        ClientId const thisClient;
    private:
        /// Chunk manager
        WorldChunkManager& m_chunkManager;
        /// Process callback
        ActorTrace::MovementProcessor m_movementProcessor;
        /// Validate callback
        ActorTrace::MovementValidator m_movementValidator;

        /// All controlled actors
        map<ClientId, ObjectIterator> m_actors;
        /// Actor traces
        map<ClientId, ActorTrace> m_traces;
        /// Objects by IDs map
        map<ObjectId, ObjectIterator> m_actorObjects;

        /// This actor movement
        ActorMovement m_thisMovement;
        /// This actor
        ObjectIterator m_thisActor;

        /// This client server-client lag
        DeltaTime m_thisLag = 0;
    };
    /// @}
}

