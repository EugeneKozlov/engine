/** @file ls/simulation/RayhitInfo.h
 *  @brief Ray hit info
 */
#pragma once
#include "ls/common.h"
#include "ls/forward.h"

namespace ls
{
    /** @addtogroup LsSimulation
     *  @{
     */
    /** Ray cast info
     *
     *  <b>Miss:</b>
     *  - @a wasHit is @a false
     *  - other fields are undefined
     *
     *  <b>Dull:</b>
     *  - @a wasHit is @a true
     *  - Both @a objectId and @a hitType are invalid
     *  - @a chunk is hit chunk
     *  - @a position is hit position
     *  - @a isSure is valid
     *
     *  <b>Hit:</b>
     *  - @a wasHit is @a true
     *  - Both @a objectId and @a hitType are valid
     *  - @a chunk is hit chunk
     *  - @a position is hit position
     *  - @a isSure is valid
     */
    struct RayhitInfo
    {
        /// Ctor default
        RayhitInfo() = default;
        /// Empty ctor
        RayhitInfo(boost::none_t) noexcept
        {
        }
        /// Hit object
        RayhitInfo(ObjectId hitObject, ubyte hitType,
                   int2 const& hitChunk, double3 const& hitPosition, bool sure) noexcept
        {
            wasHit = true;
            objectId = hitObject;
            type = hitType;
            chunk = hitChunk;
            position = hitPosition;
            isSure = sure;
        }
        /// Hit chunk
        RayhitInfo(int2 const& hitChunk, double3 const& hitPosition, bool sure) noexcept
        {
            wasHit = true;
            chunk = hitChunk;
            position = hitPosition;
            isSure = sure;
        }
        /// Cast to bool
        operator bool() const noexcept
        {
            return wasHit;
        }
        /// Test if miss
        bool isMiss() const noexcept
        {
            return !wasHit;
        }
        /// Test if dull
        bool isDull() const noexcept
        {
            return wasHit && objectId == InvalidObjectId;
        }
        /// Test if hit
        bool isHit() const noexcept
        {
            return wasHit && objectId != InvalidObjectId;
        }
    public:
        /// Was hit?
        bool wasHit = false;

        /// Hit object ID
        ObjectId objectId = InvalidObjectId;
        /// Hit type
        ubyte type = 0;
        /// Hit chunk
        int2 chunk;
        /// Hit position
        double3 position;

        /// Is hit sure?
        bool isSure = false;
    };
    /// @}
}
