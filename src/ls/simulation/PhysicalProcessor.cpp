#include "pch.h"
#include "ls/simulation/PhysicalProcessor.h"
#include "ls/scene/PhysicalAspect.h"

using namespace ls;

// Chunk
class PhysicalProcessor::Chunk
: public Noncopyable
{
public:
    /// Shape type
    using Shape = btHeightfieldTerrainShape;
    /// Ctor
    Chunk(PhysicalProcessor& self,
          ObjectsManager& objects,
          function<void(ObjectInstance& object) > stillAwakeCallback,
          ObjectsList& awakeObjects,
          btSoftRigidDynamicsWorld& scene,
          WorldChunk* chunk)
        : m_self(self)
        , m_objectsManager(objects)
        , m_stillAwakeCallback(stillAwakeCallback)
        , m_scene(scene)
        , m_chunk(*chunk)
        , m_ground()
        , m_awakeObjects(awakeObjects)
        , m_staticFlag(false)
        , m_dynamicFlag(false)
    {
        sint width = (sint) m_chunk.width;
        Shape* groundShape = new Shape(width + 1, width + 1,
                                       m_chunk.heightMap.data(),
                                       1,
                                       m_chunk.min(),
                                       m_chunk.max(),
                                       1,
                                       PHY_FLOAT,
                                       false);
        groundShape->setLocalScaling(btVector3(1.0f, 1.0f, 1.0f));
        int2 center = m_chunk.index * width + int2(width / 2);

        btTransform pose(btQuaternion::getIdentity(),
                         btVector3((float) center.x,
                                   (m_chunk.min() + m_chunk.max()) / 2,
                                   (float) center.y));
        btRigidBodyCI groundInfo(0,
                                 new btDefaultMotionState(btTransform::getIdentity()),
                                 groundShape);
        groundInfo.m_friction = 1.0f;
        m_ground = make_unique<btRigidBody>(groundInfo);
    }
    /// Re-center
    void recenter(double3 const& center)
    {
        sint width = (sint) m_chunk.width;
        int2 chunkCenter = m_chunk.index * width + int2(width / 2);
        double3 chunkPosition = double3((double) chunkCenter.x,
                                        (m_chunk.min() + m_chunk.max()) / 2,
                                        (double) chunkCenter.y);
        chunkPosition -= center;

        btTransform transform(btQuaternion::getIdentity(),
                              btVector3((float) chunkPosition.x,
                                        (float) chunkPosition.y,
                                        (float) chunkPosition.z));
        m_ground->getMotionState()->setWorldTransform(transform);
    }
    /// Move
    Chunk(Chunk && another)
        : m_self(another.m_self)
        , m_objectsManager(another.m_objectsManager)
        , m_stillAwakeCallback(another.m_stillAwakeCallback)
        , m_scene(another.m_scene)
        , m_chunk(another.m_chunk)
        , m_ground(move(another.m_ground))
        , m_awakeObjects(another.m_awakeObjects)
        , m_objects(move(another.m_objects))
        , m_staticFlag(another.m_staticFlag)
        , m_dynamicFlag(another.m_dynamicFlag)
    {
        another.m_ground.reset();
    }
    /// Dtor
    ~Chunk()
    {
        if (!!m_ground)
        {
            delete m_ground->getMotionState();
            delete m_ground->getCollisionShape();
        }
    }

    /** @name Add/remove objects to/from scene
     *  @{
     */
    /// Add statics to scene
    bool addStatics()
    {
        if (m_staticFlag)
            return false;
        if (!!m_ground)
        {
            m_scene.addRigidBody(&*m_ground);
        }

        m_staticFlag = true;
        return true;

    }
    /// Remove statics from scene
    bool removeStatics()
    {
        if (!m_staticFlag)
            return false;
        if (!!m_ground)
        {
            m_scene.removeRigidBody(&*m_ground);
        }

        m_staticFlag = false;
        return true;
    }
    /// Add dynamics to scene
    bool addDynamics(ObjectsList& chunkObjects)
    {
        // Already
        if (m_dynamicFlag)
            return false;

        // Add
        for (ObjectIterator iterObject : chunkObjects)
        {
            // Find physics
            AspectInstance* aspect = iterObject->aspect(AspectCategory::Physical);
            if (!aspect)
                continue;

            // Check physics
            PhysicalAspect* physicalAspect = dynamic_cast<PhysicalAspect*>(aspect);
            if (!physicalAspect)
            {
                m_self.logCorruptedPhysicalObject(*iterObject);
                continue;
            }

            // Construct
            m_objects.emplace(iterObject, physicalAspect);
        }

        // Finalize
        m_dynamicFlag = true;
        return true;
    }
    /// Add single dynamic to scene (object can be attached to another chunk)
    void addSingleDynamic(ObjectIterator theObject)
    {
        /*if (auto aspect = theObject->get<Aspect::Physical>())
        {
            aspect->setAwakeNotifier(m_stillAwakeCallback);
            aspect->addToScene(m_scene);
        }*/
        m_awakeObjects.insert(theObject);
    }
    /// Remove dynamics from scene
    bool removeDynamics()
    {
        // Prepare
        if (!m_dynamicFlag)
            return false;
        // Add
        for (auto& iterObject : m_objects)
            removeSingleDynamic(iterObject.first);
        m_dynamicFlag = false;
        return true;

    }
    /// Remove single dynamic to scene (object can be attached to another chunk)
    void removeSingleDynamic(ObjectIterator theObject)
    {
        //if (auto aspect = theObject->get<Aspect::Physical>())
        //    aspect->removeFromScene();
        m_awakeObjects.erase(theObject);
    }
    /// @}

    /** @name Gets
     *  @{
     */
    /// Get chunk index
    int2 const& index() const
    {
        return m_chunk.index;

    }
    /// Check if chunk is static
    inline bool isStatic() const
    {
        return m_staticFlag;
    }
    /// Check if chunk is dynamic
    inline bool isDynamic() const
    {
        return m_dynamicFlag;
    }
    /// @}
private:
    PhysicalProcessor& m_self;
    ObjectsManager& m_objectsManager;
    function<void(ObjectInstance& object) > m_stillAwakeCallback;
    btSoftRigidDynamicsWorld& m_scene;
    WorldChunk& m_chunk;

    unique_ptr<btRigidBody> m_ground;

    ObjectsList& m_awakeObjects;
    map<ObjectIterator, PhysicalAspect*> m_objects;

    bool m_staticFlag;
    bool m_dynamicFlag;
};


// Physical
PhysicalProcessor::PhysicalProcessor(float3 const& gravity,
                          ObjectsManager& objects,
                          StreamingWorld& world)
: Nameable<PhysicalProcessor>("PhysicalProcessor")
, onStillAwakeCallback(bind(&PhysicalProcessor::onStillAwake, this, _1))
, m_world(world)
, m_worldConfig(world.worldConfig)
, m_worldStreamingConfig(world.streamingConfig)
, m_objectsManager(objects)
, m_scene(nullptr)
, m_staticRadius(*m_worldStreamingConfig.staticPhysicsRadius)
, m_activeAreaCenter(0)
{
    // Increase statics
    /*uint maxStaticRadius = *m_worldStreamingConfig.packLoadingDist * *m_worldConfig.packWidth;
    if(m_staticRadius > maxStaticRadius)
    {
        logWarning("Support physics #", debugIndex(), ": "
            "Too large static active area [", m_staticRadius, "] is required. "
            "Max width is [", maxStaticRadius, "].");
        m_staticRadius = maxStaticRadius;
    }*/

    // Scene
    auto collisionConfig = new btDefaultCollisionConfiguration();
    auto dispatcher = new btCollisionDispatcher(collisionConfig);
    auto broadphase = new btDbvtBroadphase();
    auto constraintSolver = new btSequentialImpulseConstraintSolver();
    m_scene = new btSoftRigidDynamicsWorld(dispatcher,
                                           broadphase,
                                           constraintSolver,
                                           collisionConfig);
    m_scene->setWorldUserInfo(&m_sceneInfo);

    m_scene->setGravity(convert(gravity));

    // Observer
    m_objectsManager.addObserver(*this);
    m_world.addObserver(*this);
}

PhysicalProcessor::~PhysicalProcessor()
{
    // Observer
    m_world.removeObserver(*this);
    m_objectsManager.removeObserver(*this);

    // Clear all
    btCollisionObjectArray& objects = m_scene->getCollisionObjectArray();
    for(int i = 0; i < objects.size(); ++i)
    {
        btCollisionObject* object = objects.at(i);
        m_scene->removeCollisionObject(object);
        delete object->getCollisionShape();
        delete object;
    }


    // Destroy
    auto constraintSolver = m_scene->getConstraintSolver();
    auto broadphase = m_scene->getBroadphase();
    auto dispatcher = m_scene->getDispatcher();
    auto collisionConfig = dynamic_cast<btCollisionDispatcher*>(dispatcher)
        ->getCollisionConfiguration();

    delete m_scene;
    delete constraintSolver;
    delete broadphase;
    delete dispatcher;
    delete collisionConfig;
}

void PhysicalProcessor::simulate(float timeDelta, DiscreteTime realTime)
{
    m_currentTime = realTime;
    m_scene->stepSimulation(timeDelta, 5, 1.0f/60);
}

vector<ObjectState> const& PhysicalProcessor::flush()
{
    m_framesPool.clear();
    for(ObjectIterator object : m_awakeObjects)
    {
        debug_assert(!!object,
                     "try to flush invalid object [", object.index(), "]");
        /*auto aspect = object->get<Aspect::Physical>();
        if(!aspect)
            continue;
        // Slow down
        if(auto rigid = dynamic_cast<Aspect::PhysicalRigid*>(aspect))
        {
            auto& actor = rigid->getActor();
            auto angularVelocity = actor.getAngularVelocity();
            auto maxAngularVelocity = pi / 0.05f;
            float rate = angularVelocity.length() / maxAngularVelocity;
            if(rate > 1.0f)
                actor.setAngularVelocity(angularVelocity / rate);
        }
        // Extract
        ObjectState state(object->proto(), object->id());
        state.resetTime(m_currentTime);
        if(object->extract(state))
        {
            //object->act();
            m_framesPool.push_back(state);
        }*/
    }
    m_awakeObjects.clear();
    return m_framesPool;
}

bool PhysicalProcessor::raycast(float3 const& theRayStart, float3 const& theRayDir,
                                ObjectIterator& theObject,
                                ubyte& theHitType, float3& theHitPosition)
{
    /*float len = (theHitType + 1)*10.0f;
    float t = math::intersectPlaneLine(plane(0, 0, 1, 0), theRayStart, theRayDir * len);
    if(t < 0 || t > 1)
        return false;
    theHitPosition = math::lerp(theRayStart, theRayDir * len, t);
    theObject = ObjectIterator();
    theHitType = 0;
    return true;*/
    return false;
}

ObjectIterator PhysicalProcessor::getObject(btCollisionObject& actor)
{
    //if(!actor.getUserPointer())
        return ObjectIterator();
    //else
    //  return m_objectsManager.findObject((uint)actor.getUserPointer() - 1);
}


void PhysicalProcessor::onPostloadRegionDetail(WorldRegion& region)
{
    for (WorldChunk& chunk : region.chunks())
    {
        // Create
        int2 index = chunk.index;
        Chunk& impl = m_createdChunks.emplace(index, Chunk(*this,
                                                           m_objectsManager,
                                                           onStillAwakeCallback,
                                                           m_awakeObjects,
                                                           *m_scene,
                                                           &chunk))
            .first->second;
        impl.recenter(m_world.worldCenterPosition());

        // Add
        impl.addStatics();
        if (m_chunksToSimulate.erase(index) == 1)
        {
            //bool success = impl.addDynamics();
        }
    }
}

void PhysicalProcessor::onUnloadRegionDetail(WorldRegion& region)
{
    for (WorldChunk& chunk : region.chunks())
    {
        // Find
        int2 index = chunk.index;
        Chunk* impl = findOrDefault(m_createdChunks, index);
        if(!impl)
        {
            bool fail = true;
            return;
        }

        // Disable dynamics and write about need
        if (impl->removeDynamics())
        {
            m_chunksToSimulate.emplace(index);
        }
        // Disable static
        impl->removeStatics();
        m_createdChunks.erase(index);

    }
}

void PhysicalProcessor::enableChunk(int2 const& chunk)
{
    Chunk* impl = findOrDefault(m_createdChunks, chunk);
    if(impl)
    {
        //bool success = impl->addDynamics();
    }
    else
    {
        m_chunksToSimulate.emplace(chunk);
    }
}

void PhysicalProcessor::disableChunk(int2 const& chunk)
{
    // Remove to-enable chunk
    if(m_chunksToSimulate.erase(chunk))
        return;

    // Or remove enabled
    Chunk* impl = findOrDefault(m_createdChunks, chunk);
    if(!impl)
    {
        logWarning("Physical processor #", debugIndex(),
            ": Try to disable invalid chunk ", chunk);
        return;
    }

    // Remove objects
    bool fDynamicsRemoved = impl->removeDynamics();
    if(!fDynamicsRemoved)
    {
        logWarning("Physical processor #", debugIndex(),
            ": Try to disable non-enabled chunk ", chunk);
        return;
    }
}

void PhysicalProcessor::onAddObject(ObjectInstance& object)
{
    Chunk* impl = findOrDefault(m_createdChunks, object.chunk());
    // Ignore call if out of area
    if(!impl)
        return;
    // Ignore non-dynamics
    if(!impl->isDynamic())
        return;
    // Add to chunk
    impl->addSingleDynamic(object.self());
}

void PhysicalProcessor::onRemoveObject(ObjectInstance& object)
{
    Chunk* impl = findOrDefault(m_createdChunks, object.chunk());
    // Clear awake
    bool wasAwake = m_awakeObjects.erase(object.self()) > 0;
    // Ignore call if out of area
    if(!impl)
    {
        if(wasAwake)
        {
            logWarning("Physical processor #", debugIndex(),
                ": Awake object [", object.id(), ":", object.type(), "] is bound to unknown chunk ", object.chunk());
        }
        return;
    }
    bool fDynamic = impl->isDynamic();
    // Invalid state
    /*if(auto physical = object->get<Aspect::Physical>())
    {
        if(physical->isSimulated() != fDynamic)
        {
            logWarning("Physical processor #", debugIndex(),
                ": Active object [", object->id(), ":", object->type(), "] is bound to inactive chunk ", chunk);
        }
        // No dynamics
        if(!fDynamic)
        {
            if(wasAwake)
            {
                logWarning("Physical processor #", debugIndex(),
                    ": Awake object [", object->id(), ":", object->type(), "] is bound to non-dynamic chunk ", chunk);
            }
            return;
        }
        // Remove from chunk
        iChunk->second.removeSingleDynamic(object);
    }*/
}

void PhysicalProcessor::onRebindObject(ObjectIterator object,
    int2 const& from, int2 const& to)
{
    // Chunks
    auto fromChunk = findOrDefault(m_createdChunks, from);
    auto toChunk = findOrDefault(m_createdChunks, to);
    // Flags
    bool fromDyanmics = fromChunk && fromChunk->isDynamic();
    bool toDynamics = toChunk && toChunk->isDynamic();
    // Enable object
    if(!fromDyanmics && toDynamics)
        toChunk->addSingleDynamic(object);
    // Disable object
    if(fromDyanmics && !toDynamics)
        fromChunk->removeSingleDynamic(object);
}

void PhysicalProcessor::onMoveToChunk(int2 const& index)
{
    m_activeAreaCenter = index;
    // Enable loaded chunks
    /*for(auto iLoadedChunk = m_loadedChunks.begin(); iLoadedChunk != m_loadedChunks.end(); iLoadedChunk)
        if(isChunkStatic(iLoadedChunk->first))
        {
            auto iCurrentLoadedChunk = iLoadedChunk++;
            createLoadedChunk(iCurrentLoadedChunk->first);
        }
        else
            ++iLoadedChunk;
    // Disable or reenable created chunks
    for(auto& iCreatedChunk : m_createdChunks)
    {
        bool fStatic = isChunkStatic(iCreatedChunk.first);
        // Add static
        if(fStatic)
            iCreatedChunk.second.addStatics();
        else
            iCreatedChunk.second.removeStatics();
    }*/
}

void PhysicalProcessor::onRecenter(int2 const& /*oldCenter*/, int2 const& /*newCenter*/,
                                   double3 const& newCenterPosition)
{
    for (auto& iterChunk : m_createdChunks)
    {
        Chunk& chunk = iterChunk.second;
        chunk.recenter(newCenterPosition);
    }
}

void PhysicalProcessor::onStillAwake(ObjectInstance& object)
{
    m_awakeObjects.insert(object.self());
}


// Log
void PhysicalProcessor::logCorruptedPhysicalObject(ObjectInstance& object)
{
    this->logWarning("Object [", object.id(), "] "
        "of type [", object.type(), "] has corrupted physical state "
        "and should be destroyed");
    object.destroyLater();
}
