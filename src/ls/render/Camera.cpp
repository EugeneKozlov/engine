#include "pch.h"
#include "ls/render/Camera.h"
using namespace ls;
float3 const Camera::Up = float3(0.0f, 1.0f, 0.0f);


// Projection
Projection::Projection(float left, float right, float bottom, float top,
                       float znear, float zfar)
    : m_zNear(znear)
    , m_zFar(zfar)
    , m_fovY(0)
    , m_aspect((right - left) / (top - bottom))
    , m_height(top - bottom)
    , m_center(float2(left + right, top + bottom) / 2)
    , m_matrix(matrix::orthoLeft(left, right, bottom, top, znear, zfar))
{
}
Projection::Projection(float aspect, float fovy, float znear, float zfar)
    : m_zNear(znear)
    , m_zFar(zfar)
    , m_fovY(fovy)
    , m_aspect(aspect)
    , m_height(2.0f)
    , m_center(float2(0.0f))
    , m_matrix(matrix::perspectiveLeft(fovy, aspect, znear, zfar))
{
}


// Camera main
Camera::Camera()
{
}
Camera::~Camera()
{
}
void Camera::setProjection(const Projection& projection)
{
    m_proj = projection;
    updateFrustumAndEtc();
}
void Camera::cameraRectangle(float distance, double3 points[]) const
{
    float4 area = m_proj.screenRectangle(distance);
    float3 z = m_look * distance;
    points[0] = m_worldPosition + static_cast<double3>(z + m_side * area.x + m_up * area.y);
    points[1] = m_worldPosition + static_cast<double3>(z + m_side * area.z + m_up * area.y);
    points[2] = m_worldPosition + static_cast<double3>(z + m_side * area.x + m_up * area.w);
    points[3] = m_worldPosition + static_cast<double3>(z + m_side * area.z + m_up * area.w);
}
void Camera::frustumShape(double3 points[], float zNear, float zFar) const
{
    cameraRectangle(zNear, points);
    cameraRectangle(zFar, points + 4);
}
void Camera::localFrustumShape(float3 points[], float zNear, float zFar) const
{
    double3 ps[8];
    frustumShape(ps, zNear, zFar);
    for (uint idx = 0; idx < 8; ++idx)
        points[idx] = float3(ps[idx] - worldCenter());
}
float3 Camera::local2global(const float3& move)
{
    return side() * move.x + up() * move.y + look() * move.z;
}


// Camera impl
void Camera::updateLocalPosition()
{
    m_position = static_cast<float3>(m_worldPosition - m_worldCenter);
}
void Camera::updateFrustumAndEtc()
{
    double3 points[8];
    frustumShape(points);
    m_frustum = Frustum(points);
    m_viewProj = m_view * m_proj.matrix();
}
void Camera::updateViewByPositionRotation()
{
    float4x4 rotMatrix = matrix::rotationYXZ(m_rotation.x, m_rotation.y, m_rotation.z);
    m_view = matrix::translate(-m_position) * rotMatrix;

    m_side = normalize(float3(m_view(0, 0), m_view(1, 0), m_view(2, 0)));
    m_up = normalize(float3(m_view(0, 1), m_view(1, 1), m_view(2, 1)));
    m_look = normalize(float3(m_view(0, 2), m_view(1, 2), m_view(2, 2)));
    m_world = inverse(m_view);
    //m_world = matrix::translate(m_position) * ~rotMatrix;
}
void Camera::updatePositionRotationByView()
{
    m_side = normalize(float3(m_view(0, 0), m_view(1, 0), m_view(2, 0)));
    m_up = normalize(float3(m_view(0, 1), m_view(1, 1), m_view(2, 1)));
    m_look = normalize(float3(m_view(0, 2), m_view(1, 2), m_view(2, 2)));
    m_world = inverse(m_view);

    m_rotation.x = asinf(m_look.y);
    m_rotation.y = -atan2f(m_look.x, m_look.z);
    float3 tempUp = normalize(cross(m_look, normalize(cross(Up, m_look))));
    float3 tempSide = normalize(cross(tempUp, m_look));
    float alpha = acos(clamp(dot(m_side, tempUp), -1.0f, 1.0f));
    float beta = acos(clamp(dot(m_side, tempSide), -1.0f, 1.0f));
    m_rotation.z = (alpha <= pi / 2) ? beta : -beta;
}


//  Camera use
void Camera::setCenter(const double3& center)
{
    m_worldCenter = center;
    updateLocalPosition();
    updateViewByPositionRotation();
    updateFrustumAndEtc();
}
void Camera::setView(const double3& position, const float3& direction, float roll)
{
    m_worldPosition = position;
    updateLocalPosition();

    float3 dir = normalize(direction);
    m_rotation.x = asinf(dir.y);
    m_rotation.y = -atan2f(dir.x, dir.z);
    m_rotation.z = roll;

    updateViewByPositionRotation();
    updateFrustumAndEtc();
}
void Camera::setView(const double3& position, const float3& rotation)
{
    m_worldPosition = position;
    updateLocalPosition();

    m_rotation = rotation;

    updateViewByPositionRotation();
    updateFrustumAndEtc();
}
void Camera::setViewLookAtPoint(const double3& position, const double3& target, float roll)
{
    setView(position, static_cast<float3>(target - position), roll);
}
void Camera::updateRotation(const float3& delta)
{
    m_rotation += delta;
    float eps = 0.001f;
    if (m_rotation.x > pi * 0.5f - eps)
        m_rotation.x = pi * 0.5f - eps;
    if (m_rotation.x < -pi * 0.5f + eps)
        m_rotation.x = -pi * 0.5f + eps;
    setView(m_worldPosition, m_rotation);
}
void Camera::move(const double3& position, const float3& deltaRot)
{
    m_worldPosition = position;
    updateLocalPosition();
    updateRotation(deltaRot);
}
void Camera::update(const float3& deltaPos, const float3& deltaRot)
{
    move(m_worldPosition + static_cast<double3>(deltaPos), deltaRot);
}
