/// @file ls/render/DrawingPolicy.h
/// @brief Drawing Policy
#pragma once

#include "ls/common.h"

namespace ls
{
    /// @addtogroup LsRender
    /// @{

    /// @brief Drawing policy
    enum DrawingPolicy : uint
    {
        /// @brief Draw G-buffer
        GeometryPolicy,
        /// @brief Draw G-buffer and wire 
        WiredGeometryPolicy,
        /// @brief Draw Cascaded Shadow Maps
        CascadedShadowPolicy,
        /// @brief Custom policy
        CustomPolicy
    };
    /// @}
}
