#include "pch.h"
#include "ls/render/DeferredShading.h"

#include "ls/asset/AssetManager.h"
#include "ls/asset/MaterialCache.h"
#include "ls/gapi/Renderer.h"
#include "ls/render/SceneView.h"
#include "ls/scene/RenderSystem.h"
#include "ls/scene/graphical/GraphicalScene.h"

using namespace ls;

// Deferred shading
DeferredShading::DeferredShading(AssetManager& assetManager, const GraphicConfig& graphicConfig,
                                 EffectHandle effect)
    : graphicConfig(graphicConfig)

    , m_assetManager(assetManager)
    , m_renderer(*m_assetManager.renderer())
    , m_materialCache(*m_assetManager.materialCache())
    , m_batchRenderer(m_renderer, 4, 1024*16)

    , m_effect(effect)
{
    initEffects();
    initStates();
    initPolicyState(GeometryPolicy, MaterialAsset::ShaderMode::Geometry);
    initPolicyState(WiredGeometryPolicy, MaterialAsset::ShaderMode::Geometry);
    initPolicyState(CascadedShadowPolicy, MaterialAsset::ShaderMode::Depth);

    m_shadowMap = m_renderer
        .createTexture2D(1024, 1024, false, Format::D32_FLOAT, ResourceUsage::Static,
                         ResourceBinding::DepthStencil | ResourceBinding::ShaderResource);
    m_renderer.setTexture2DSampler(m_shadowMap, Filter::Linear, Addressing::Clamp, true, Comparison::Less);
    m_shadowMapRT = m_renderer.createRenderTarget(0, m_shadowMap);

    // Get programs
    /*p.output= renderer.handleProgram(m_effect, "Output");
    p.color = renderer.handleProgram(m_effect, "Color");
    p.ambient = renderer.handleProgram(m_effect, "Ambient");
    p.directional[Ambient] = nullptr;
    p.directional[OneSided] = renderer.handleProgram(m_effect, "DirectionalOneSided");
    p.directional[TwoSided] = renderer.handleProgram(m_effect, "DirectionalTwoSided");

    // Get handles
    h.mapColor = renderer.handleResource(m_effect, "ColorMap");
    h.mapNormal = renderer.handleResource(m_effect, "NormalMap");

    h.vSize = renderer.handleUniform(m_effect, "vSize");
    h.vDirection = renderer.handleUniform(m_effect, "vDirection");

    // Create DS
    StencilInfo dsDrawGeomrtryInfo;
    dsDrawGeomrtryInfo.writeMask = 0xff;
    dsDrawGeomrtryInfo.front.stencilFun = Comparison::Always;
    dsDrawGeomrtryInfo.front.pass = StencilOperation::Replace;
    dsDrawGeomrtryInfo.back.stencilFun = Comparison::Always;
    dsDrawGeomrtryInfo.back.pass = StencilOperation::Replace;
    dsDrawGeometry = renderer.createDepthStencil(true, true,
                                                 Comparison::LessEqual,
                                                 &dsDrawGeomrtryInfo);

    StencilInfo dsDrawLightingInfo;
    dsDrawLightingInfo.readMask = 0xff;
    dsDrawLightingInfo.front.stencilFun = Comparison::Equal;
    dsDrawLightingInfo.back.stencilFun = Comparison::Equal;
    dsDrawLighting = renderer.createDepthStencil(false, false,
                                                 Comparison::Always,
                                                 &dsDrawLightingInfo);

    dsDrawLightingInfo.front.stencilFun = Comparison::NotEqual;
    dsDrawLightingInfo.back.stencilFun = Comparison::NotEqual;
    dsDrawLightingInv = renderer.createDepthStencil(false, false,
                                                    Comparison::Always,
                                                    &dsDrawLightingInfo);*/
}
void DeferredShading::initPolicyState(DrawingPolicy policy, const MaterialAsset::ShaderMode mode)
{
    // Update revision
    ++m_revision;

    // Init state
    StateDesc& state = m_policyStates[policy];
    switch (policy)
    {
    case GeometryPolicy:
    case WiredGeometryPolicy:
    case CascadedShadowPolicy:
        // Depth-stencil
        state.hasDepthTest = true;
        state.hasDepthWrite = true;
        state.hasStencil = false;
        state.stencil.readMask = 0;
        state.stencil.writeMask = 0xf0;
        state.stencil.front.stencilFun = Comparison::Always;
        state.stencil.back.stencilFun = Comparison::Always;
        state.depthFunction = Comparison::LessEqual;

        // Blending
        state.hasIndependentBlending = false;
        state.blending[0] = NoBlendingTag();
        state.blending[1] = NoBlendingTag();

        // Rasterizer
        state.rasterizer.isWireframe = false;
        break;
    default:
        break;
    }
    switch (policy)
    {
    case CascadedShadowPolicy:
        state.rasterizer.depthBias = 512;
        state.rasterizer.depthSlopeScaledBias = 5.0f;
        state.rasterizer.depthBiasClamp = 1024.0f;
        break;
    default:
        break;
    }
    switch (policy)
    {
    case GeometryPolicy:
    case WiredGeometryPolicy:
        state.rasterizer.culling = Culling::Back;
        break;
    case CascadedShadowPolicy:
        state.rasterizer.culling = Culling::Back;
        break;
    case CustomPolicy:
    default:
        break;
    }

    // Create context
    m_contexts[policy] = m_materialCache.registerContext(state, mode);
}
void DeferredShading::initEffects()
{
    LS_THROW(m_effect, ArgumentException);

    m_handleShadowMap = m_renderer.handleResource(m_effect, "ShadowMap");

    m_handleConstant = m_renderer.createUniformBuffer(sizeof(Constant));
    m_handlePerLight = m_renderer.createUniformBuffer(sizeof(PerLight));
    m_handleSkyboxParam = m_renderer.createUniformBuffer(sizeof(SkyboxParam));
    m_renderer.attachUniformBuffer(m_effect, m_handleConstant, "Constant");
    m_renderer.attachUniformBuffer(m_effect, m_handlePerLight, "PerLight");
    m_renderer.attachUniformBuffer(m_effect, m_handleSkyboxParam, "SkyboxParam");

    LS_THROW(!!m_handleShadowMap && !!m_handleConstant && !!m_handleSkyboxParam && !!m_handlePerLight,
             ArgumentException);
}
void DeferredShading::initStates()
{
    // Final
    StateDesc state;
    state.primitive = PrimitiveType::TriangleStrip;
    state.rasterizer.culling = Culling::None;

    state.program = m_renderer.handleProgram(m_effect, "Final");
    m_drawFinal = m_renderer.createState(state);

    //state.program = m_renderer.handleProgram(m_effect, "RestoreDepth");

    state.blending[0] = BlendingDesc(AdditiveBlendingTag());
    state.program = m_renderer.handleProgram(m_effect, "Directional");
    m_drawDirectional = m_renderer.createState(state);

    state.hasIndependentBlending = false;
    state.blending[0] = NoBlendingTag();
    state.primitive = PrimitiveType::Triangle;
    state.rasterizer.culling = Culling::Front;
    state.program = m_renderer.handleProgram(m_effect, "SkyboxProgram");
    state.depthFunction = Comparison::LessEqual;
    m_drawSkybox = m_renderer.createState(state);
}
DeferredShading::~DeferredShading()
{
    for (MaterialCacheContext* context : m_contexts)
    {
        m_materialCache.unregisterContext(context);
    }

    drop();
    m_renderer.destroyBuffer(m_handleConstant);
    m_renderer.destroyBuffer(m_handlePerLight);
    m_renderer.destroyBuffer(m_handleSkyboxParam);
    m_renderer.destroyRenderTarget(m_shadowMapRT);
    m_renderer.destroyTexture2D(m_shadowMap);
    m_renderer.destroyState(m_drawFinal);
    m_renderer.destroyState(m_drawDirectional);
    m_renderer.destroyState(m_drawSkybox);
}
void DeferredShading::drop()
{
    if (!m_gbufferRT)
        return;

    m_renderer.destroyRenderTarget(m_lightRT);
    m_renderer.destroyRenderTarget(m_gbufferRT);
    m_renderer.destroyRenderTarget(m_zprepassRT);
    m_renderer.destroyTexture2D(m_gbufferMaps[Map0_ColorReflectivity]);
    m_renderer.destroyTexture2D(m_gbufferMaps[Map1_NormalSmoothness]);
    m_renderer.destroyTexture2D(m_gbufferMaps[Map2_HdrColor]);
    m_renderer.destroyTexture2D(m_depthStencil);
    m_gbufferMaps.fill(nullptr);
    m_depthStencil = nullptr;
    m_gbufferRT = nullptr;

    m_renderer.destroyRenderTarget(m_linearDepthRT);
    m_renderer.destroyTexture2D(m_linearDepth);
    m_linearDepth = nullptr;
    m_linearDepthRT = nullptr;

    m_renderer.destroyRenderTarget(m_sourceTarget);
    m_renderer.destroyTexture2D(m_sourceBuffer);
    m_renderer.destroyRenderTarget(m_destTarget);
    m_renderer.destroyTexture2D(m_destBuffer);

    m_sourceBuffer = nullptr;
    m_sourceTarget = nullptr;
    m_destBuffer = nullptr;
    m_destTarget = nullptr;

    m_width = m_height = 0;
}
void DeferredShading::resize(uint width, uint height)
{
    drop();
    m_width = width;
    m_height = height;

    // Update uniform buffer
    m_constant.vSize = float2(static_cast<float>(width), static_cast<float>(height));
    m_renderer.writeBuffer(m_handleConstant, m_constant);

    // Create maps
    m_gbufferMaps[Map0_ColorReflectivity] = m_renderer
        .createTexture2D(width, height, false, Format::R8G8B8A8_UNORM, ResourceUsage::Static,
                         ResourceBinding::RenderTarget | ResourceBinding::ShaderResource);
    m_gbufferMaps[Map1_NormalSmoothness] = m_renderer
        .createTexture2D(width, height, false, Format::R10G10B10A2_UNORM, ResourceUsage::Static,
                         ResourceBinding::RenderTarget | ResourceBinding::ShaderResource);
    m_gbufferMaps[Map2_HdrColor] = m_renderer
        .createTexture2D(width, height, false, Format::R11G11B10_FLOAT, ResourceUsage::Static,
                         ResourceBinding::RenderTarget | ResourceBinding::ShaderResource);
    m_depthStencil = m_renderer
        .createTexture2D(width, height, false, Format::D24_UNORM_S8_UINT, ResourceUsage::Static,
                         ResourceBinding::DepthStencil | ResourceBinding::ShaderResource);

    // Create z-pre-pass
    m_zprepassRT = m_renderer.createRenderTarget(0, m_depthStencil);

    // Create g-buffer
    m_gbufferRT = m_renderer.createRenderTarget(3, m_depthStencil);
    m_renderer.attachColorTexture2D(m_gbufferRT, Map0_ColorReflectivity, m_gbufferMaps[Map0_ColorReflectivity]);
    m_renderer.attachColorTexture2D(m_gbufferRT, Map1_NormalSmoothness, m_gbufferMaps[Map1_NormalSmoothness]);
    m_renderer.attachColorTexture2D(m_gbufferRT, Map2_HdrColor, m_gbufferMaps[Map2_HdrColor]);

    // Create light RT
    m_lightRT = m_renderer.createRenderTarget(1, nullptr);
    m_renderer.attachColorTexture2D(m_lightRT, 0, m_gbufferMaps[Map2_HdrColor]);

    // Create linear depth
    m_linearDepth = m_renderer
        .createTexture2D(width, height, false, Format::R32_FLOAT, ResourceUsage::Static,
                         ResourceBinding::RenderTarget | ResourceBinding::ShaderResource);
    m_linearDepthRT = m_renderer.createRenderTarget(1, nullptr);
    m_renderer.attachColorTexture2D(m_linearDepthRT, 0, m_linearDepth);

    // Create fullscreen maps
    m_sourceBuffer = m_renderer
        .createTexture2D(width, height, false, Format::R8G8B8A8_UNORM, ResourceUsage::Static,
                         ResourceBinding::RenderTarget | ResourceBinding::ShaderResource);
    m_sourceTarget = m_renderer.createRenderTarget(m_sourceBuffer, nullptr);

    m_destBuffer = m_renderer
        .createTexture2D(width, height, false, Format::R8G8B8A8_UNORM, ResourceUsage::Static,
                         ResourceBinding::RenderTarget | ResourceBinding::ShaderResource);
    m_destTarget = m_renderer.createRenderTarget(m_destBuffer, nullptr);
}


// Render
void DeferredShading::drawScene(const SceneView& sceneView,
                                GraphicalScene& graphicalScene)
{
    // Render geometry
    for (auto& renderSystem : graphicalScene.renderSystems())
    {
        renderSystem->render(m_batchRenderer, &sceneView);
    }
}
void DeferredShading::drawGeometryBuffer(GraphicalScene& graphicalScene)
{
    // Z-pre-pass
    m_renderer.clearDepthStencil(m_zprepassRT, 1.0f, 0);

    // Clear GBuffer
    float3 backgroundColor = float3(0.529f, 0.808f, 0.980f);
//     float3 backgroundColor = float3(0.0f, 0.749f, 1.0f);
    m_renderer.clearColor(m_gbufferRT, Map0_ColorReflectivity, float4(backgroundColor, 0.0f));
    m_renderer.clearColor(m_gbufferRT, Map1_NormalSmoothness, float4(0.0f));
    m_renderer.clearColor(m_gbufferRT, Map2_HdrColor, float4(backgroundColor, 0.0f));
//     m_renderer.clearColor(m_lightRT, 0, float4(backgroundColor, 0.0f));

    // Scene view
    SceneView sceneView;
    sceneView.isBasePass = true;
    sceneView.camera = graphicalScene.mainCamera();
    sceneView.eyePosition = sceneView.camera.localPosition();
    sceneView.frustum = sceneView.camera.frustum();

    // Draw scene
    sceneView.policy = GeometryPolicy;
    sceneView.shaderMode = MaterialAsset::ShaderMode::Geometry;
    sceneView.passState = m_policyStates[GeometryPolicy];
    sceneView.materialCacheContext = m_contexts[GeometryPolicy];
    ResourceBucket maps;
    maps.addResource(0, nullptr, ShaderType::Pixel);
    maps.addResource(1, nullptr, ShaderType::Pixel);
    maps.addResource(2, nullptr, ShaderType::Pixel);
    maps.addResource(3, nullptr, ShaderType::Pixel);
    m_renderer.bindResources(maps);
    m_renderer.bindRenderTarget(m_gbufferRT);
    drawScene(sceneView, graphicalScene);

    // Draw skybox
    float4x4 viewRotationMatrix = sceneView.camera.viewMatrix();
    viewRotationMatrix.row(3, float4(0.0f, 0.0f, 0.0f, 1.0f));
    m_skyboxParam.mViewProj = transpose(viewRotationMatrix * sceneView.camera.projMatrix());
    m_skyboxParam.vSunPosition = -graphicalScene.cascadedShadowMapHelper().direction();
    m_skyboxParam.fTurbidity = 2.0f;
    m_renderer.bindState(m_drawSkybox);
    m_renderer.writeBuffer(m_handleSkyboxParam, m_skyboxParam);
    m_renderer.draw(0, 36);

    //// restore depth
    //m_renderer.bindRenderTarget(m_linearDepthRT);
    //m_renderer.bindTexture2D(m_handleRawDepth, m_depthStencil, ShaderType::Pixel);
}
void DeferredShading::drawLights(GraphicalScene& graphicalScene)
{
    Camera& mainCamera = graphicalScene.mainCamera();
    CascadedShadowMap& csm = graphicalScene.cascadedShadowMapHelper();
    //csm.setCascades({ 24.0f, 96.0f }, 0.1f, 1024);
    csm.setCascades({ 16.0f, 48.0f, 160.0f }, 0.1f, 1024);
    //csm.setCascades({ 12.0f, 32.0f, 80.0f, 180.0f }, 0.1f, 1024);
//     csm.update(mainCamera, float3(0.01f, -1.0f, 0.0f), 100.0f);
    csm.update(mainCamera, float3(0.5f, -1.0f, 0.0f), 100.0f);

    for (uint idx = 0; idx < csm.numCascades(); ++idx)
    {
        // Draw shadow map
        SceneView shadowView;
        shadowView.policy = CascadedShadowPolicy;
        shadowView.shaderMode = MaterialAsset::ShaderMode::Depth;
        shadowView.materialCacheContext = m_contexts[CascadedShadowPolicy];
        shadowView.passState = m_policyStates[CascadedShadowPolicy];
        shadowView.camera = csm.cascadeCamera(idx);
        shadowView.eyePosition = graphicalScene.mainCamera().localPosition()
            - csm.direction() * 1000;
        shadowView.frustum = shadowView.camera.frustum();
        m_renderer.bindRenderTarget(m_shadowMapRT);
        m_renderer.clearDepthStencil(m_shadowMapRT, 1.0f, 0);
        drawScene(shadowView, graphicalScene);

        // Draw cascade
        PerLight perLight;
        perLight.mViewToLight = inverse(mainCamera.viewMatrix()) * csm.lightMatrix(idx);
        perLight.vColor = float4(1.0f);
        perLight.vDirection = normalize(float4(-csm.direction(), 0.0f)) * mainCamera.viewMatrix();
        perLight.vCameraInfo = float4(mainCamera.projection().screenSize(1.0f) / 2.0f,
                                      mainCamera.projection().zNear(),
                                      mainCamera.projection().zFar());
        perLight.vNearFar = float4(csm.nearDistances(idx), csm.farDistances(idx));
        m_renderer.writeBuffer(m_handlePerLight, perLight);
        m_renderer.bindState(m_drawDirectional);
        m_renderer.bindRenderTarget(m_lightRT);
        ResourceBucket maps;
        maps.addResource(0, m_depthStencil, ShaderType::Pixel);
        maps.addResource(1, m_gbufferMaps[Map0_ColorReflectivity], ShaderType::Pixel);
        maps.addResource(2, m_gbufferMaps[Map1_NormalSmoothness], ShaderType::Pixel);
        maps.addResource(3, nullptr, ShaderType::Pixel);
        maps.addResource(4, m_shadowMap, ShaderType::Pixel);
        m_renderer.bindResources(maps);
        m_renderer.draw(0, 4);
        m_renderer.bindTexture2D(m_handleShadowMap, nullptr, ShaderType::Pixel);
    }

    // Draw ambient
//     m_renderer.bindState(m_drawAmbient);
//     m_renderer.bindTexture2D(m_handleDepthMap, m_depthStencil, ShaderType::Pixel);
//     m_renderer.bindTexture2D(m_handleColorMap, m_gbufferMaps[Map0_ColorReflectivity], ShaderType::Pixel);
//     m_renderer.bindTexture2D(m_handleNormalMap, m_gbufferMaps[Map1_Normal], ShaderType::Pixel);
//     m_renderer.draw(0, 4);

//     swap(m_destBuffer, m_sourceBuffer);
//     swap(m_destTarget, m_sourceTarget);
}
void DeferredShading::drawFinalImage()
{
    m_renderer.bindRenderTarget(nullptr);
    m_renderer.bindState(m_drawFinal);
    ResourceBucket maps;
    maps.addResource(3, m_gbufferMaps[Map2_HdrColor], ShaderType::Pixel);
    m_renderer.bindResources(maps);
    m_renderer.draw(0, 4);
}
/*void DeferredShading::beginGeometry(const float4& background)
{
    debug_assert(m_framebuffer);

    m_renderer.bindRenderTarget(m_framebuffer);
    m_renderer.bindBlendState(NoBlending);
    m_renderer.bindDepthStencil(dsDrawGeometry, Ambient);

    m_renderer.clearColor(0, background);
    m_renderer.clearColor(1, float4(0.0f));
    m_renderer.clearDepthStencil(1.0f, 0);
}
void DeferredShading::beginMaterial(Material material)
{
    m_renderer.bindDepthStencil(dsDrawGeometry, material);
}
void DeferredShading::beginLighting(const float4x4& viewMatrix)
{
    m_viewMatrix = viewMatrix;

    m_source.texture = m_colorMap;
    m_source.renderTarget = m_secondTarget;
    m_dest.texture = m_finalColor;
    m_dest.renderTarget = m_finalTarget;

    m_renderer.bindBlendState(AdditiveBlending);
    m_renderer.bindSource(nullptr);
    m_renderer.bindRenderTarget(m_dest.renderTarget);
    m_renderer.clearColor(0, float4(0.0f));

    m_renderer.bindTexture2D(m_effect, h.mapColor, m_colorMap);
    m_renderer.bindTexture2D(m_effect, h.mapNormal, m_normalMap);
}
void DeferredShading::finalize()
{
    swap(m_source, m_dest);

    m_renderer.bindDepthStencil(NoDepth);
    m_renderer.bindBlendState(NoBlending);
    m_renderer.bindRenderTarget(DefaultRenderTarget);
    m_renderer.bindSource(nullptr);

    m_renderer.bindTexture2D(m_effect, h.mapColor, m_source.texture);
    m_renderer.bindProgram(m_effect, p.output);
    m_renderer.draw(PrimitiveType::TriangleStrip, 0, 4);
}


// Draw
void DeferredShading::drawAmbient()
{
    m_renderer.bindDepthStencil(dsDrawLighting, 0);
    m_renderer.bindProgram(m_effect, p.color);
    m_renderer.draw(PrimitiveType::TriangleStrip, 0, 4);

    m_renderer.bindDepthStencil(dsDrawLightingInv, 0);
    m_renderer.bindProgram(m_effect, p.ambient);
    m_renderer.draw(PrimitiveType::TriangleStrip, 0, 4);
}
void DeferredShading::drawDirectional(const float3& direction)
{
    float4 dir = normalize(float4(direction, 0) * m_viewMatrix);
    m_renderer.setUniform(m_effect, h.vDirection, float3(dir));

    for (ubyte material = 0; material < NumMaterials; ++material)
    {
        Gid program = p.directional[material];
        if (program == nullptr)
            continue;

        m_renderer.bindDepthStencil(dsDrawLighting, material);
        m_renderer.bindProgram(m_effect, program);
        m_renderer.draw(PrimitiveType::TriangleStrip, 0, 4);
    }
}


// Gets
double DeferredShading::readDepth(uint x, uint y)
{
    m_renderer.bindRenderTarget(m_framebuffer);
    return m_renderer.readDepth(x, y);
}
*/