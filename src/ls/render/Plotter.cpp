#include "pch.h"
#include "ls/render/Plotter.h"
#include "ls/gapi/Renderer.h"
#include "ls/core/utf.h"

using namespace ls;
Plotter::Plotter(Renderer& renderer, EffectHandle effect)
    : renderer(renderer)
    , m_plainEffect(effect)

    , m_cachedColoredVertex(ColorVertexCahceSize)
    , m_cachedTexturedVertex(TexturedVertexCahceSize)
{
    // Handle effect
    m_bufferPerBatch = renderer.createUniformBuffer(sizeof(PerBatch));
    renderer.attachUniformBuffer(effect, m_bufferPerBatch, "PerBatch");
    m_handleTexture = renderer.handleResource(m_plainEffect, "Diffuse");
    debug_assert(m_bufferPerBatch);
    debug_assert(m_handleTexture);

    m_programColored = renderer.handleProgram(m_plainEffect, "Colored");
    m_programTextured = renderer.handleProgram(m_plainEffect, "Textured");
    m_programLighted = renderer.handleProgram(m_plainEffect, "Lighted");
    debug_assert(m_programColored);
    debug_assert(m_programTextured);
    debug_assert(m_programLighted);

    // Layouts
    m_layoutColored = renderer.createLayoutSingle(m_programColored,
                                                  sizeof(ColoredVertex),
                                                  ColoredVertex::Layout, ColoredVertex::LayoutSize);
    m_layoutTextured = renderer.createLayoutSingle(m_programTextured,
                                                  sizeof(TexturedVertex),
                                                  TexturedVertex::Layout, TexturedVertex::LayoutSize);
    m_layoutLighted = renderer.createLayoutSingle(m_programLighted,
                                                  sizeof(LightedVertex),
                                                  LightedVertex::Layout, LightedVertex::LayoutSize);
    debug_assert(m_layoutColored);
    debug_assert(m_layoutTextured);
    debug_assert(m_layoutLighted);

    // States
    generateStates(false, false);
    generateStates(false, true);
    generateStates(true, false);
    generateStates(true, true);

    // Buffers
    m_vertexBuffer = renderer.createBuffer(VertexCacheSize,
                                           BufferType::Vertex,
                                           ResourceUsage::Dynamic,
                                           nullptr);
    m_indexBuffer = renderer.createBuffer(IndexCacheLimit * sizeof(ushort),
                                          BufferType::Index,
                                          ResourceUsage::Dynamic,
                                          nullptr);
    debug_assert(m_vertexBuffer);
    debug_assert(m_indexBuffer);

    m_defaultCamera.setView(double3(0, 0, 0), float3(0, 0, 0));
    m_cachedColoredVertex.clear();
    m_cachedTexturedVertex.clear();
    m_cachedIndex.clear();
}
void Plotter::generateStates(bool depthTest, bool alphaBlending)
{
    StateDesc stateDesc;
    stateDesc.blending[0] = alphaBlending ? BlendingDesc(AlphaBlendingTag()) : BlendingDesc();
    stateDesc.hasDepthTest = depthTest;

    stateDesc.program = m_programColored;
    stateDesc.layout = m_layoutColored;
    stateDesc.primitive = PrimitiveType::Line;
    m_states[DrawLines][depthTest][alphaBlending] = renderer.createState(stateDesc);

    stateDesc.primitive = PrimitiveType::Triangle;
    m_states[DrawColoredTris][depthTest][alphaBlending] = renderer.createState(stateDesc);

    stateDesc.program = m_programTextured;
    stateDesc.layout = m_layoutTextured;
    m_states[DrawText2D][depthTest][alphaBlending] = renderer.createState(stateDesc);
    m_states[DrawTexturedTris][depthTest][alphaBlending] = renderer.createState(stateDesc);

    debug_assert(m_states[DrawLines][depthTest][alphaBlending]);
    debug_assert(m_states[DrawColoredTris][depthTest][alphaBlending]);
    debug_assert(m_states[DrawText2D][depthTest][alphaBlending]);
    debug_assert(m_states[DrawTexturedTris][depthTest][alphaBlending]);
}
Plotter::~Plotter()
{
    renderer.destroyBuffer(m_bufferPerBatch);
    renderer.destroyBuffer(m_vertexBuffer);
    renderer.destroyBuffer(m_indexBuffer);
    m_states.foreach(
        [this](StateHandle state)
    {
        if (!!state)
            renderer.destroyState(state);
    });
    renderer.destroyLayout(m_layoutColored);
    renderer.destroyLayout(m_layoutTextured);
    renderer.destroyLayout(m_layoutLighted);
}
void Plotter::setCamera(const Camera& camera)
{
    m_camera = &camera;
}
void Plotter::resetCamera()
{
    m_camera = &m_defaultCamera;
}
void Plotter::resize(uint width, uint heigth)
{
    m_windowSize = int2((sint) width, (sint) heigth);
    m_defaultCamera.setView(double3(), float3());
    m_defaultCamera.setProjection(Projection(0, static_cast<float>(width),
                                             static_cast<float>(heigth), 0,
                                             0, 1));
}
void Plotter::begin(DrawMode drawMode, Texture2DHandle texture, bool depthTest, bool alphaBlending)
{
    debug_assert(m_drawMode == DrawNothing);
    m_drawMode = drawMode;
    m_drawState = m_states[drawMode][depthTest][alphaBlending];
    m_texture = texture;
    switch (drawMode)
    {
    case Plotter::DrawLines: 
    case Plotter::DrawColoredTris:
        m_cachedData = CachedColoredVertex;
        break;
    case Plotter::DrawText2D:
    case Plotter::DrawTexturedTris:
        m_cachedData = CachedTexturedVertex;
        break;
    default:
        debug_assert(0);
    }
}
void Plotter::end()
{
    debug_assert(m_drawMode != DrawNothing);
    flush();
    m_drawMode = DrawNothing;
    m_drawState = nullptr;
}

void Plotter::flush()
{
    if (m_cachedIndex.empty())
        return;
    const Camera& renderCamera = m_drawMode == DrawText2D ? m_defaultCamera : *m_camera;

    // Matrix
    m_perBatch.mModelViewProj = renderCamera.viewProjMatrix();
    m_perBatch.mModelView = renderCamera.viewMatrix();
    renderer.writeBuffer(m_bufferPerBatch, m_perBatch);

    // Effect and texture
    renderer.bindState(m_drawState);
    renderer.bindGeometry(m_vertexBuffer, m_indexBuffer, IndexFormat::Ushort);
    renderer.bindTexture2D(m_handleTexture, m_texture, ShaderType::Pixel);

    // Indices
    uint numIndices = (uint) m_cachedIndex.size();

    // Vertex buffer
    ushort* indexDest = nullptr;
    ColoredVertex* coloredDest = nullptr;
    TexturedVertex* texturedDest = nullptr;

    // Update buffers
    switch (m_cachedData)
    {
    case CachedColoredVertex:
        coloredDest = static_cast<ColoredVertex*>(renderer.mapBuffer(m_vertexBuffer));
        copy(m_cachedColoredVertex.begin(), m_cachedColoredVertex.end(), coloredDest);
        renderer.unmapBuffer(m_vertexBuffer);

        indexDest = static_cast<ushort*>(renderer.mapBuffer(m_indexBuffer));
        copy(m_cachedIndex.begin(), m_cachedIndex.end(), indexDest);
        renderer.unmapBuffer(m_indexBuffer);

        break;

    case CachedTexturedVertex:
        texturedDest = static_cast<TexturedVertex*>(renderer.mapBuffer(m_vertexBuffer));
        copy(m_cachedTexturedVertex.begin(), m_cachedTexturedVertex.end(), texturedDest);
        renderer.unmapBuffer(m_vertexBuffer);

        indexDest = static_cast<ushort*>(renderer.mapBuffer(m_indexBuffer));
        copy(m_cachedIndex.begin(), m_cachedIndex.end(), indexDest);
        renderer.unmapBuffer(m_indexBuffer);

        break;
    }

    // Draw
    renderer.drawIndexed(0, numIndices, 0);

    // Clear
    m_cachedIndex.clear();
    m_cachedColoredVertex.clear();
    m_cachedTexturedVertex.clear();
}
template <class T>
void Plotter::putVertices(vector<T>& vertexCache, uint vertexCacheLimit,
                          const T* vertices, uint numVertices, uint stride,
                          uint* colorReplacer, const float4x4* transform)
{
    debug_assert(vertices);
    debug_assert(numVertices % stride == 0);

    const T* p = vertices;
    uint remaningVertices = numVertices;
    uint remaningIndices = numVertices;
    uint freeVertices = vertexCacheLimit - vertexCache.size();
    uint freeIndices = IndexCacheLimit - m_cachedIndex.size();
    while (remaningVertices)
    {
        // Put
        uint toPut = std::min(std::min(freeVertices, freeIndices), remaningVertices) / stride * stride;
        uint baseVertex = (uint) vertexCache.size();
        vertexCache.insert(vertexCache.end(), p, p + toPut);
        // Replace
        if (colorReplacer)
            for (uint i = 0; i < toPut; i++)
                vertexCache[i + baseVertex].color = *colorReplacer;
        // Transform
        if (transform)
        {
            auto mat = *transform;
            for (uint i = 0; i < toPut; i++)
                vertexCache[i + baseVertex].pos = vertexCache[i + baseVertex].pos * mat;
        }
        // Indices
        for (uint i = 0; i < toPut; i++)
            m_cachedIndex.push_back(ushort(i + baseVertex));
        // Pointers move
        p += toPut;
        remaningVertices -= toPut;
        remaningIndices -= toPut;
        freeVertices -= toPut;
        freeIndices -= toPut;
        // Flush and continue
        if (freeVertices < remaningVertices || freeIndices < remaningIndices)
        {
            flush();
            freeVertices = vertexCacheLimit;
            freeIndices = IndexCacheLimit;
        }
    }
}
template <class T>
void Plotter::putVertices(vector<T>& vertexCache, uint vertexCacheLimit,
                          const T* vertices, uint numVertices,
                          const ushort* indices, uint numIndices,
                          uint stride, uint* colorReplacer,
                          const float4x4* transform)
{
    debug_assert(vertices);
    debug_assert(indices);
    debug_assert(numIndices % stride == 0);
    debug_assert(numVertices < vertexCacheLimit);

    const ushort* p = indices;
    uint remaningIndices = numIndices;
    uint freeVertices = vertexCacheLimit - vertexCache.size();
    uint freeIndices = IndexCacheLimit - m_cachedIndex.size();
    while (remaningIndices)
    {
        // Put
        uint toPut = (freeVertices < numVertices) ? 0
            : (std::min(freeIndices, remaningIndices) / stride * stride);
        // If I can put all vertices and some indices
        if (toPut)
        {
            uint baseVertex = (uint) vertexCache.size();
            vertexCache.insert(vertexCache.end(), vertices, vertices + numVertices);
            // Replace color
            if (colorReplacer)
            {
                for (uint i = 0; i < numVertices; i++)
                    vertexCache[i + baseVertex].color = *colorReplacer;
            }
            // Transform
            if (transform)
            {
                auto mat = *transform;
                for (uint i = 0; i < numVertices; i++)
                    vertexCache[i + baseVertex].pos = vertexCache[i + baseVertex].pos * mat;
            }
            for (uint i = 0; i < toPut; i++)
                m_cachedIndex.push_back(ushort(p[i] + baseVertex));
            // Progress
            p += toPut;
            remaningIndices -= toPut;
            freeIndices -= toPut;
            freeVertices -= numVertices;
        }
        // Not enough place
        if (freeIndices < remaningIndices || (!toPut && remaningIndices))
        {
            flush();
            freeVertices = vertexCacheLimit;
            freeIndices = IndexCacheLimit;
        }
    }
}
void Plotter::putLines(const ColoredVertex* vertices, uint numVertices,
                       const ushort* indices, uint numLines,
                       uint* color, const float4x4* transform)
{
    debug_assert(m_drawMode == DrawLines);
    if (indices)
    {
        putVertices(m_cachedColoredVertex, ColorVertexCahceSize,
                    vertices, numVertices,
                    indices, numLines * 2,
                    2, color, transform);
    }
    else
    {
        debug_assert(!numVertices || numLines * 2 == numVertices);

        putVertices(m_cachedColoredVertex, ColorVertexCahceSize,
                    vertices, numLines * 2,
                    2, color, transform);
    }
}
void Plotter::putTris(const ColoredVertex* vertices, uint numVertices,
                      const ushort* indices, uint numTris,
                      uint* color, const float4x4* transform)
{
    debug_assert(m_drawMode == DrawColoredTris);
    if (indices)
    {
        putVertices(m_cachedColoredVertex, ColorVertexCahceSize, vertices, numVertices,
                    indices, numTris * 3, 3, color, transform);
    }
    else
    {
        debug_assert(!numVertices || numTris * 3 == numVertices);

        putVertices(m_cachedColoredVertex, ColorVertexCahceSize,
                    vertices, numTris * 3,
                    3, color, transform);
    }
}
void Plotter::putTris(const TexturedVertex* vertices, uint numVertices,
                      const ushort* indices, uint numTris,
                      const float4x4* transform)
{
    debug_assert(m_drawMode == DrawTexturedTris);
    if (indices)
    {
        putVertices(m_cachedTexturedVertex, TexturedVertexCahceSize, vertices, numVertices,
                    indices, numTris * 3, 3, nullptr, transform);
    }
    else
    {
        debug_assert(!numVertices || numTris * 3 == numVertices);

        putVertices(m_cachedTexturedVertex, TexturedVertexCahceSize,
                    vertices, numTris * 3,
                    3, nullptr, transform);
    }
}
void Plotter::putSymbol(const Symbol& symbol, const int2& pos, uint color)
{
    float2 p0 = float2(static_cast<float>(pos.x), static_cast<float>(pos.y))
        + float2(static_cast<float>(symbol.xoffset), static_cast<float>(symbol.yoffset));
    float2 p1 = p0 + symbol.size;
    float2 t0 = symbol.t0;
    float2 t1 = symbol.t1;
    ushort baseVertex = (ushort) m_cachedTexturedVertex.size();
    m_cachedTexturedVertex.push_back(TexturedVertex(float2(p0.x, p0.y), float2(t0.x, t0.y), color));
    m_cachedTexturedVertex.push_back(TexturedVertex(float2(p1.x, p0.y), float2(t1.x, t0.y), color));
    m_cachedTexturedVertex.push_back(TexturedVertex(float2(p0.x, p1.y), float2(t0.x, t1.y), color));
    m_cachedTexturedVertex.push_back(TexturedVertex(float2(p1.x, p1.y), float2(t1.x, t1.y), color));
    m_cachedIndex.push_back(baseVertex + 0);
    m_cachedIndex.push_back(baseVertex + 1);
    m_cachedIndex.push_back(baseVertex + 2);
    m_cachedIndex.push_back(baseVertex + 1);
    m_cachedIndex.push_back(baseVertex + 3);
    m_cachedIndex.push_back(baseVertex + 2);
}
template <class Painter>
int2 Plotter::putText(Font& font, const char* text, const int2& pos,
                      Painter picker)
{
    debug_assert(text);
    debug_assert(font.texture() == m_texture);
    debug_assert(m_drawMode == DrawText2D);

    const char* p = text;
    int2 cursor = pos;

    uint n = 0;
    while (*p)
    {
        // Reset if needed
        if (TexturedVertexCahceSize - m_cachedTexturedVertex.size() < 4 ||
            IndexCacheLimit - m_cachedIndex.size() < 6)
        {
            flush();
        }
        // Get symbol
        auto symbol = UTF8toUTF32(p);
        uint symbolId = symbol.first;
        p += symbol.second;
        // Write symbol
        const ls::Symbol& symbolInfo = font.symbol(symbolId);
        switch (symbolId)
        {
        case 13:
            break;
        case 10:
            cursor.y += font.height();
            cursor.x = pos.x;
            break;
        case 8:
            putSymbol(font.symbol('_'), cursor, picker(n));
            break;
        case 9:
            cursor.x += font.tabWidth();
            break;
        default:
            putSymbol(symbolInfo, cursor, picker(n));
        case 32:
            cursor.x += symbolInfo.origWidth;
        }
        n++;
    }
    return cursor;
}
template <class Painter>
int2 Plotter::putText(Font& font, const uint* text, const int2& pos,
                      Painter picker)
{
    debug_assert(text);

    debug_assert(m_drawMode == DrawText2D);

    const uint* p = text;
    int2 cursor = pos;

    uint n = 0;
    while (*p)
    {
        // Reset if needed
        if (TexturedVertexCahceSize - m_cachedTexturedVertex.size() < 4 ||
            IndexCacheLimit - m_cachedIndex.size() < 6)
        {
            flush();
        }
        // Get symbol
        uint symbolId = *p;
        ++p;
        // Write symbol
        const ls::Symbol& symbolInfo = font.symbol(symbolId);
        switch (symbolId)
        {
        case 13:
            break;
        case 10:
            cursor.y += font.height();
            cursor.x = pos.x;
            break;
        case 8:
            putSymbol(font.symbol('_'), cursor, picker(n));
            break;
        case 9:
            cursor.x += font.tabWidth();
            break;
        default:
            putSymbol(symbolInfo, cursor, picker(n));
        case 32:
            cursor.x += symbolInfo.origWidth;
        }
        n++;
    }
    return cursor;
}
uint Plotter::pickColorSingle(uint /*symbol*/, uint color)
{
    return color;
}
uint Plotter::pickColorMultiply(uint symbol, const uint* colors)
{
    return colors[symbol];
}
uint Plotter::pickColorPalette(uint symbol, const char* colors, const uint* palette)
{
    return palette[(ubyte) colors[symbol]];
}
void Plotter::plotLines(const ColoredVertex* vertices, uint numVertices,
                        const ushort* indices, uint numLines, uint color,
                        const float4x4* pose)
{
    putLines(vertices, numVertices, indices, numLines, &color, pose);
}
void Plotter::plotLines(const ColoredVertex* vertices, uint numVertices,
                        const ushort* indices, uint numLines,
                        const float4x4* pose)
{
    putLines(vertices, numVertices, indices, numLines, 0, pose);
}
void Plotter::plotLines(const ColoredVertex* vertices, uint color, uint numLines, const float4x4* pose)
{
    putLines(vertices, 0, 0, numLines, &color, pose);
}
void Plotter::plotLines(const ColoredVertex* vertices, uint numLines,
                        const float4x4* pose)
{
    putLines(vertices, 0, 0, numLines, 0, pose);
}
void Plotter::plotLine(const ColoredVertex& first, const ColoredVertex& second,
                       const float4x4* pose)
{
    ls::ColoredVertex v[2] = {first, second};
    putLines(v, 0, 0, 1, 0, pose);
}
void Plotter::plotTri(const ColoredVertex& v1st,
                      const ColoredVertex& v2nd, const ColoredVertex& v3rd,
                      const float4x4* pose)
{
    ls::ColoredVertex v[3] = {v1st, v2nd, v3rd};
    putTris(v, 0, 0, 1, 0, pose);
}
void Plotter::plotTri(const ColoredVertex* vertices, uint numTriangles,
                      const float4x4* pose)
{
    putTris(vertices, 0, 0, numTriangles, 0, pose);
}
void Plotter::plotTri(const ColoredVertex* vertices, uint color, uint numTriangles,
                      const float4x4* pose)
{
    putTris(vertices, 0, 0, numTriangles, &color, pose);
}
void Plotter::plotTri(const ColoredVertex* vertices, uint numVertices,
                      const ushort* indices, uint numTriangles,
                      const float4x4* pose)
{
    putTris(vertices, numVertices, indices, numTriangles, 0, pose);
}
void Plotter::plotTri(const ColoredVertex* vertices, uint numVertices, uint color,
                      const ushort* indices, uint numTriangles,
                      const float4x4* pose)
{
    putTris(vertices, numVertices, indices, numTriangles, &color, pose);
}
void Plotter::plotTri(const TexturedVertex* vertices, uint numVertices,
                      const ushort* indices, uint numTriangles,
                      const float4x4* pose)
{
    putTris(vertices, numVertices, indices, numTriangles, pose);
}
void Plotter::plotQuad(const float2& v1st, const float2& v2nd, uint color,
                       const float4x4* pose)
{
    ColoredVertex v[4] = {
        ColoredVertex(float2(v1st.x, v1st.y), color),
        ColoredVertex(float2(v2nd.x, v1st.y), color),
        ColoredVertex(float2(v1st.x, v2nd.y), color),
        ColoredVertex(float2(v2nd.x, v2nd.y), color)
    };
    ushort i[6] = {0, 1, 2, 1, 3, 2};
    putTris(v, 4, i, 2, 0, pose);
}
void Plotter::plotQuad(const float2& v1st, const float2& v2nd, const float4x4* pose)
{
    TexturedVertex v[4] = {
        TexturedVertex(float2(v1st.x, v1st.y), float2(0, 0)),
        TexturedVertex(float2(v2nd.x, v1st.y), float2(1, 0)),
        TexturedVertex(float2(v1st.x, v2nd.y), float2(0, 1)),
        TexturedVertex(float2(v2nd.x, v2nd.y), float2(1, 1))
    };
    ushort i[6] = { 0, 1, 2, 1, 3, 2 };
    plotTri(v, 4, i, 2, pose);
}
void Plotter::plotText3D(Font& font, const char* text, const float3& pos, uint color)
{
    float3 size(static_cast<float>(m_windowSize.x), static_cast<float>(m_windowSize.y), 1.0f);
    float4x4 remapMatrix = m_camera->viewProjMatrix()
        * matrix::scale(float4(0.5f, -0.5f, 1.0f, 1)) * matrix::translate(float3(0.5f, 0.5f, 0.0f));

    float3 flatPos = pos * remapMatrix * size;
    if (flatPos.z >= 0 && flatPos.z <= 1)
        plotText(font, text, int2(static_cast<sint>(flatPos.x), static_cast<sint>(flatPos.y)), color);
}
int2 Plotter::plotText(Font& font, const char* text, const int2 pos, uint color)
{
    return putText(font, text, pos, bind(&pickColorSingle, _1, color));
}
int2 Plotter::plotText(Font& font, const char* text, const int2 pos,
                       const uint* colorMap)
{
    return putText(font, text, pos, bind(&pickColorMultiply, _1, colorMap));
}
int2 Plotter::plotText(Font& font, const char* text, const int2 pos,
                       const char* colorMap, uint* palette)
{
    return putText(font, text, pos, bind(&pickColorPalette, _1, colorMap, palette));
}
void Plotter::plotTextWide3D(Font& font, const uint* text, const float3& pos,
                             uint color)
{
    float3 size(static_cast<float>(m_windowSize.x), static_cast<float>(m_windowSize.y), 1.0f);
    float4x4 remapMatrix = matrix::translate(float3(0.5f, 0.5f, 0.0f)) *
        matrix::scale(float4(0.5f, -0.5f, 1.0f, 1)) * m_camera->viewProjMatrix();
    float3 flatPos = remapMatrix * pos * size;
    if (flatPos.z >= 0 && flatPos.z <= 1)
        plotTextWide(font, text, int2(static_cast<sint>(flatPos.x), static_cast<sint>(flatPos.y)), color);
}
int2 Plotter::plotTextWide(Font& font, const uint* text, const int2 pos, uint color)
{
    return putText(font, text, pos, bind(&pickColorSingle, _1, color));
}
int2 Plotter::plotTextWide(Font& font, const uint* text, const int2 pos,
                           const uint* colorMap)
{
    return putText(font, text, pos, bind(&pickColorMultiply, _1, colorMap));
}
int2 Plotter::plotTextWide(Font& font, const uint* text, const int2 pos,
                           const char* colorMap, uint* palette)
{
    return putText(font, text, pos, bind(&pickColorPalette, _1, colorMap, palette));
}
void Plotter::plotWireBox(const float4x4& worldPose, uint color,
                          const float3& halfSize)
{
    static ColoredVertex v[8];
    v[0] = ColoredVertex(float3(-halfSize.x, -halfSize.y, -halfSize.z));
    v[1] = ColoredVertex(float3(halfSize.x, -halfSize.y, -halfSize.z));
    v[2] = ColoredVertex(float3(-halfSize.x, -halfSize.y, halfSize.z));
    v[3] = ColoredVertex(float3(halfSize.x, -halfSize.y, halfSize.z));
    v[4] = ColoredVertex(float3(-halfSize.x, halfSize.y, -halfSize.z));
    v[5] = ColoredVertex(float3(halfSize.x, halfSize.y, -halfSize.z));
    v[6] = ColoredVertex(float3(-halfSize.x, halfSize.y, halfSize.z));
    v[7] = ColoredVertex(float3(halfSize.x, halfSize.y, halfSize.z));
    applyTransform(worldPose, color, v, 8);
    ushort i[24] = {0, 1, 1, 3, 3, 2, 2, 0, 4, 5, 5, 7, 7, 6, 6, 4, 0, 4, 1, 5, 2, 6, 3, 7};
    plotLines(v, 8, i, 12);
}
void Plotter::plotWireSphere(const float4x4& worldPose, uint color, float radius)
{
    static ColoredVertex vertices[SphereSegLat * SphereSegLong];
    generateSphereVertices(vertices, SphereSegLat, SphereSegLong, radius);
    static const uint IndicesCount = (SphereSegLat * SphereSegLong) * 2 + SphereSegLong * (SphereSegLat - 2) * 2;
    static ushort indices[IndicesCount];
    generateSphereWireIndices(indices, SphereSegLat, SphereSegLong, false);
    applyTransform(worldPose, color, vertices, SphereSegLat * SphereSegLong);
    plotLines(vertices, SphereSegLat * SphereSegLong, indices, IndicesCount / 2);
}
void Plotter::plotWireCapsuleZ(const float4x4& worldPose, uint color, float radius, float length)
{
    static ColoredVertex vertices[SphereSegLat * SphereSegLong];
    generateSphereVertices(vertices, SphereSegLat, SphereSegLong, radius);
    for (uint i = 0; i < SphereSegLat * SphereSegLong; i++)
        vertices[i].pos.z += ((i < SphereSegLat * SphereSegLong / 2) ? -1 : 1) * length / 2;
    static const uint IndicesCount = (SphereSegLat * SphereSegLong) * 2 + SphereSegLong * (SphereSegLat - 1) * 2;
    static ushort indices[IndicesCount];
    generateSphereWireIndices(indices, SphereSegLat, SphereSegLong, true);
    applyTransform(worldPose, color, vertices, SphereSegLat * SphereSegLong);
    plotLines(vertices, SphereSegLat * SphereSegLong, indices, IndicesCount / 2);
}
void Plotter::plotWireCapsuleX(const float4x4& worldPose, uint color,
                               float radius, float length)
{
    plotWireCapsuleZ(worldPose * static_cast<float4x4>(matrix::rotationY(pi / 2)),
                     color, radius, length);
}
void Plotter::plotWireCapsuleY(const float4x4& worldPose, uint color,
                               float radius, float length)
{
    plotWireCapsuleZ(worldPose * static_cast<float4x4>(matrix::rotationX(pi / 2)),
                     color, radius, length);
}
void Plotter::generateSphereVertices(ColoredVertex* p, ushort n, ushort m, float r)
{
    debug_assert(p);
    int rn = int(n) / 2 - 1;
    for (int i = -rn; i <= rn; i++)
    {
        for (int t = 0; t <= int(i == 0); t++)
        {
            float beta = float(i) / int(rn + 1) * pi / 2;
            float rad = cos(beta), h = sin(beta);
            for (uint j = 0; j < m; j++)
            {
                float alpha = float(j) / m * 2 * pi;
                float x = cos(alpha) * rad, y = sin(alpha) * rad;
                *p++ = ColoredVertex(float3(x, y, h) * r);
            }
        }
    }
}
void Plotter::generateSphereWireIndices(ushort* p, ushort n, ushort m, bool capsule)
{
    debug_assert(p);
    // Lat rings
    ushort v = 0;
    for (ushort i = 0; i < n; i++)
        for (ushort j = 0; j < m; j++)
        {
            *p++ = v;
            *p++ = (ushort(v + 1) < (i + 1) * m) ? (v + 1) : i*m;
            v++;
        }
    // Long rings
    for (ushort j = 0; j < m; j++)
        for (ushort i = 0; i < n - 1; i++)
        {
            if ((i == n / 2 - 1) && !capsule)
                continue;
            *p++ = j + m * i;
            *p++ = j + m * (i + 1);
        }
}
void Plotter::applyTransform(const float4x4& worldPose, uint color,
                             ColoredVertex* vertices, uint numVertices)
{
    debug_assert(vertices);
    for (uint i = 0; i < numVertices; i++)
    {
        vertices[i].pos = vertices[i].pos * worldPose;
        vertices[i].color = color;
    }
}
