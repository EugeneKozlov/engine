#include "pch.h"
#include "ls/render/EffectInterface.h"
#include "ls/render/Camera.h"
#include "ls/render/SceneView.h"
#include "ls/gapi/Renderer.h"
#include "ls/asset/AssetManagerOld.h"

using namespace ls;


// Shader
SurfaceShader::SurfaceShader(EffectInterface& effect,
                             uint shaderIndex, const string& shaderName)
    : m_effect(effect)
    , m_shaderIndex(shaderIndex)
    , m_shaderName(shaderName)
{
}
SurfaceShader::SurfaceShader(SurfaceShader&& another) noexcept
    : m_effect(another.m_effect)
    , m_shaderIndex(move(another.m_shaderIndex))
    , m_shaderName(move(another.m_shaderName))
    , m_cachedStates(move(another.m_cachedStates))
{
}
SurfaceShader::~SurfaceShader()
{
    for (CachedState& cached : m_cachedStates)
    {
        if (!cached.pipelineState)
            continue;
        m_effect.renderer.destroyState(cached.pipelineState);
    }
}
void SurfaceShader::addPolicy(DrawingPolicy policy, ProgramHandle program, LayoutHandle layout)
{
    CachedState& cachedState = m_cachedStates[policy];
    cachedState.isInitialized = true;
    cachedState.program = program;
    cachedState.layout = layout;
}
StateHandle SurfaceShader::pipelineState(const SceneView& sceneView)
{
    // Skip non-initialized
    CachedState& cachedState = m_cachedStates[sceneView.policy];
    if (!cachedState.isInitialized)
        return nullptr;

    // Update cached state
    if (!cachedState.pipelineState)
    {
        // Destroy previous
        if (!!cachedState.pipelineState)
            m_effect.renderer.destroyState(cachedState.pipelineState);

        // Init new
        StateDesc desc = sceneView.passState;
        desc.program = cachedState.program;
        desc.layout = cachedState.layout;
        m_effect.initializeShaderState(m_shaderIndex, sceneView.policy, desc);
        cachedState.pipelineState = m_effect.renderer.createState(desc);
    }

    // Return
    return cachedState.pipelineState;
}


// Main
EffectInterface::EffectInterface(Renderer& renderer,
                                 Asset::Ptr effectAsset,
                                 const string& effectName,
                                 initializer_list<VertexFormat> vertexFormat,
                                 initializer_list<uint> streamStrides)
    : renderer(renderer)

    , m_effectAsset(Asset::cast<EffectAsset>(effectAsset))

    , m_vertexFormat(vertexFormat)
    , m_streamStrides(streamStrides)
{
    LS_THROW(!!m_effectAsset, GapiEffectException,
             "Unknown effect [", m_effectName, "]");
}
EffectInterface::~EffectInterface()
{
    if (!!m_layout)
        renderer.destroyLayout(m_layout);
}
SurfaceShaderIteratorRange EffectInterface::shaders()
{
    return make_pair(m_shaders.begin(), m_shaders.end());
}


// Initialize
void EffectInterface::addShader(uint shaderIndex, const string& shaderName)
{
    debug_assert(!m_layout, "initialize materials first!");
    m_shaders.emplace_back(*this, shaderIndex, shaderName);
}
void EffectInterface::assignProgram(DrawingPolicy policy, uint shaderIndex, const string& programName)
{
    debug_assert(shaderIndex < m_shaders.size());

    // Find
    SurfaceShader& shader = m_shaders[shaderIndex];

    // Load program
    ProgramHandle program = renderer.handleProgram(m_effectAsset->gapiEffect(), programName.c_str());
    if (!m_layout)
    {
        m_layout = renderer.createLayout(program,
                                           m_streamStrides, m_vertexFormat);
        LS_THROW(!!m_layout, GapiEffectException,
                 "Cannot initialize layout for shader #", shaderIndex, " "
                 "of effect [", m_effectName, "]; "
                 "program [", programName, "] is used");
    }

    // Init
    shader.addPolicy(policy, program, m_layout);
}
BufferHandle EffectInterface::getUniformBuffer(const uint size, const string& bufferName)
{
    BufferHandle handle = renderer.createUniformBuffer(size);
    renderer.attachUniformBuffer(m_effectAsset->gapiEffect(), handle, bufferName.c_str());
    return handle;
}
