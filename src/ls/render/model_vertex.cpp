#include "pch.h"
#include "ls/render/model_vertex.h"

using namespace ls;

// Vertexes
VertexFormat const VertexTangent::Layout[] = {
    VertexFormat(Format::R32G32B32_FLOAT, "Position", 0, offsetof(VertexTangent, pos), false),
    VertexFormat(Format::R32G32_FLOAT, "Uv", 0, offsetof(VertexTangent, uv), false),
    VertexFormat(Format::R32G32B32_FLOAT, "Normal", 0, offsetof(VertexTangent, normal), false),
    VertexFormat(Format::R32G32B32_FLOAT, "Tangent", 0, offsetof(VertexTangent, tangent), false),
    VertexFormat(Format::R32G32B32_FLOAT, "Binormal", 0, offsetof(VertexTangent, binormal), false)
};
VertexFormat const VertexTangentWind::Layout[] = {
    VertexFormat(Format::R32G32B32_FLOAT, "Position", 0, offsetof(VertexTangentWind, pos), false),
    VertexFormat(Format::R32G32_FLOAT, "Uv", 0, offsetof(VertexTangentWind, uv), false),
    VertexFormat(Format::R32G32B32_FLOAT, "Normal", 0, offsetof(VertexTangentWind, normal), false),
    VertexFormat(Format::R32G32B32_FLOAT, "Tangent", 0, offsetof(VertexTangentWind, tangent), false),
    VertexFormat(Format::R32G32B32_FLOAT, "Binormal", 0, offsetof(VertexTangentWind, binormal), false),
    VertexFormat(Format::R32G32B32A32_FLOAT, "Wind", 0, offsetof(VertexTangentWind, wind), false)
};