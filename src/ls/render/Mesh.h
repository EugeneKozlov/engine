/// @file ls/render/Mesh.h
/// @brief 3D Mesh
#pragma once

#include "ls/common.h"
#include "ls/core/Nameable.h"
#include "ls/gapi/common.h"
#include "ls/scene/graphical/limits.h"

namespace ls
{
    class Renderer;
    class StreamInterface;

    /// @addtogroup LsRender
    /// @{

    /// @brief Generate AABB
    template <class Vertex>
    AABB computeAabb(Vertex vertices[], uint numVertices)
    {
        debug_assert(numVertices > 0);
        AABB box = static_cast<double3>(vertices[0].pos);
        for (uint i = 1; i < numVertices; ++i)
            box += static_cast<double3>(vertices[i].pos);
        return box;
    }
    /// @brief Generate triangle normals
    template <class Vertex, class Index>
    void computeNormalSpace(Vertex vertices[], uint numVertices,
                            const Index indices[], uint numTriangles)
    {
        // Compute
        for (uint i = 0; i < numTriangles; ++i)
        {
            const Index a1 = indices[3 * i + 0];
            const Index a2 = indices[3 * i + 1];
            const Index a3 = indices[3 * i + 2];

            const float3 pos1 = vertices[a1].pos;
            const float3 pos2 = vertices[a2].pos;
            const float3 pos3 = vertices[a3].pos;
            const float3 normal = normalize(cross(pos2 - pos1, pos3 - pos1));

            vertices[a1].normal += normal;
            vertices[a2].normal += normal;
            vertices[a3].normal += normal;
        }
    }
    /// @brief Generate triangle TBN
    template <class Vertex>
    void computeTangentBinormal(const Vertex& v0, const Vertex& v1, const Vertex& v2, 
                                float3& tangent, float3& binormal)
    {
        float3 vector1 = v1.pos - v0.pos;
        float3 vector2 = v2.pos - v0.pos;

        float2 uv1 = v1.uv - v0.uv;
        float2 uv2 = v2.uv - v0.uv;

        float cp = uv1.x * uv2.y - uv2.x * uv1.y;
        if (almostEqual(cp, 0.0f))
        {
            tangent = float3(0.0f);
            binormal = float3(0.0f);
            return;
        }
        float den = 1.0f / cp;

        tangent.x = (uv2.y * vector1.x - uv1.y * vector2.x) * den;
        tangent.y = (uv2.y * vector1.y - uv1.y * vector2.y) * den;
        tangent.z = (uv2.y * vector1.z - uv1.y * vector2.z) * den;

        binormal.x = (uv1.x * vector2.x - uv2.x * vector1.x) * den;
        binormal.y = (uv1.x * vector2.y - uv2.x * vector1.y) * den;
        binormal.z = (uv1.x * vector2.z - uv2.x * vector1.z) * den;
    }
    /// @brief Generate TBN space
    template <class Vertex, class Index>
    void computeTangentSpace(Vertex vertices[], uint numVertices,
                             const Index indices[], uint numTriangles)
    {
        // Compute
        for (uint i = 0; i < numTriangles; ++i)
        {
            Index a1 = indices[3 * i + 0];
            Index a2 = indices[3 * i + 1];
            Index a3 = indices[3 * i + 2];
            float3 tangent, binormal;
            computeTangentBinormal(vertices[a1], vertices[a2], vertices[a3],
                                   tangent, binormal);

            vertices[a1].tangent += tangent;
            vertices[a2].tangent += tangent;
            vertices[a3].tangent += tangent;
            vertices[a1].binormal += binormal;
            vertices[a2].binormal += binormal;
            vertices[a3].binormal += binormal;
        }
        // Normalize
        for (uint i = 0; i < numVertices; ++i)
        {
            vertices[i].tangent = normalize(vertices[i].tangent);
            vertices[i].binormal = normalize(vertices[i].binormal);
        }
    }
    /// @brief Max Mesh subsets
    static const uint MaxMeshSubsets = MaxModelSubsets;
    /// @brief 3D Mesh Subset Description
    struct MeshSubset
    {
        /// @brief Triangle offset
        uint offset;
        /// @brief Number of triangles
        uint numTriangles;
        /// @brief Texture ID
        uint textureId;
    };
    /// @brief 3D Mesh
    class Mesh
        : public Named
        , public DisposableConcept
    {
    public:
        /// @brief Ctor
        Mesh();
        /// @brief Dtor
        ~Mesh();

        /// @brief Create GAPI buffers
        bool create(Renderer& renderer);
        /// @brief Destroy GAPI buffers
        void dispose();
        /// @brief Drop cached in RAM geometry data
        void drop();
        /// @brief Draw subset
        void draw(uint subsetIndex);
        /// @brief Draw subset instanced
        void drawInstanced(uint subsetIndex, uint startInstance, uint numInstances);

        /// @brief Serialize
        void serialize(StreamInterface& dest) const;
        /// @brief Deserialize
        void deserialize(StreamInterface& src);

        /// @brief Set model AABB
        void setAabb(const AABB& aabb)
        {
            m_aabb = aabb;
        }
        /// @brief Add model subset
        void addSubset(uint textureId, uint numTriangles);
        /// @brief Set vertex data
        template <class Vertex>
        void setVertexData(const Vertex* vertices, uint numVertices)
        {
            const ubyte* data = reinterpret_cast<const ubyte*>(vertices);
            m_vertexStride = sizeof(Vertex);
            m_numVertices = numVertices;
            m_vertexData.assign(data, data + numVertices * m_vertexStride);
        }
        /// @brief Set index data
        template <class Index>
        void setIndexData(const Index* indices, uint numIndices)
        {
            const ubyte* data = reinterpret_cast<const ubyte*>(indices);
            m_indexStride = sizeof(Index);
            m_numIndices = numIndices;
            m_indexData.assign(data, data + numIndices * m_indexStride);

            switch (m_indexStride)
            {
            case 1: m_indexType = IndexFormat::Ubyte; break;
            case 2: m_indexType = IndexFormat::Ushort; break;
            case 4: m_indexType = IndexFormat::Uint; break;
            default: m_indexType = IndexFormat::Unknown; break;
            }
        }

        /// @brief Get model AABB
        const AABB& aabb() const
        {
            return m_aabb;
        }
        /// @brief Get number of subsets
        uint numSubsets() const noexcept
        {
            return m_numSubsets;
        }
        /// @brief Get subset
        const MeshSubset& subset(uint index) const noexcept
        {
            debug_assert(index < m_numSubsets);
            return m_subsets[index];
        }
        /// @brief Get vertex data
        const ubyte* vertexData() const noexcept
        {
            return m_vertexData.data();
        }
        /// @brief Get vertices
        template <class Vertex>
        Vertex* vertices() noexcept
        {
            return reinterpret_cast<Vertex*>(m_vertexData.data());
        }
        /// @brief Get index data
        const ubyte* indexData() const noexcept
        {
            return m_indexData.data();
        }
        /// @brief Get indices
        template <class Index>
        Index* indices() noexcept
        {
            return reinterpret_cast<Index*>(m_indexData.data());
        }
        /// @brief Get number of vertices
        uint numVertices() const noexcept
        {
            return m_numVertices;
        }
        /// @brief Get vertex stride
        uint vertexStride() const noexcept
        {
            return m_vertexStride;
        }
        /// @brief Get number of indices
        uint numIndices() const noexcept
        {
            return m_numIndices;
        }
        /// @brief Get index type
        IndexFormat indexType() const noexcept
        {
            return m_indexType;
        }
        /// @brief Get index stride
        uint indexStride() const noexcept
        {
            return m_indexStride;
        }
        /// @brief Get geometry
        const GeometryBucket& geometry() const noexcept
        {
            return m_geometry;
        }
    private:
        struct IoHeader
        {
            uint numVertices;
            uint numIndices;
            uint vertexStride;
            uint indexStride;
            uint indexType;
            uint numSubsets;
            array<MeshSubset, MaxMeshSubsets> subsets;
            AABB aabb;
        };

        // Data
        vector<ubyte> m_vertexData;
        uint m_numVertices = 0;
        uint m_vertexStride = 0;

        vector<ubyte> m_indexData;
        uint m_numIndices = 0;
        uint m_indexStride = 0;
        IndexFormat m_indexType = IndexFormat::Unknown;

        // Subsets
        array<MeshSubset, MaxMeshSubsets> m_subsets;
        uint m_numSubsets = 0;

        // AABB
        AABB m_aabb;

        // GAPI
        Renderer* m_renderer = nullptr;
        GeometryBucket m_geometry;
    };
    /// @}
}

