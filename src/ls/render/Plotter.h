/// @file ls/render/Plotter.h
/// @brief Primitive geometry plotter
#pragma once

#include "ls/common.h"
#include "ls/gapi/common.h"
#include "ls/gapi/desc/vertices.h"
#include "ls/render/Camera.h"
#include "ls/render/Font.h"

namespace ls
{
    class Renderer;

    /// @addtogroup LsRender
    /// @{
        
    /// @brief Primitive geometry plotter
    class Plotter
        : Noncopyable
    {
    public:
        /// @brief Cached data
        enum CachedData
        {
            /// @brief Cached unknown
            CachedUnknown,
            /// @brief Cached ColoredVertex
            CachedColoredVertex,
            /// @brief Cached TexturedVertex
            CachedTexturedVertex
        };
        /// @brief Draw mode
        enum DrawMode
        {
            /// @brief Draw nothing
            DrawNothing,
            /// @brief Draw lines
            DrawLines,
            /// @brief Draw text
            DrawText2D,
            /// @brief Draw colored triangles
            DrawColoredTris,
            /// @brief Draw textured triangles
            DrawTexturedTris,
            /// @brief COUNTER
            NumDrawModes
        };
    public:
        /// @brief Ctor
        Plotter(Renderer& renderer, EffectHandle plainEffect);
        /// @brief Dtor
        ~Plotter();
        /// @brief Get default flat camera
        const Camera& defaultCamera() const
        {
            return m_defaultCamera;
        }
        /// @brief Set camera
        ///
        /// This camera will be used for
        /// - Rendering geometry
        /// - Placing 2.5D text on screen
        /// - Rendering 3D text
        ///
        /// This camera will @b not be used for
        /// - Rendering 2D and 2.5D text
        ///
        /// @note
        ///   Camera is stored by pointer without copying
        /// @param camera
        ///   New camera
        void setCamera(const Camera& camera);
        /// @brief Reset camera to default
        void resetCamera();
        /// @brief Begin rendering
        void begin(DrawMode drawMode, Texture2DHandle texture, bool depthTest, bool alphaBlending);
        /// @brief End rendering
        void end();
        /// @brief Resize window
        /// @param width,height
        ///   Window width and height
        void resize(uint width, uint height);

        /// @name Draw lines
        /// @{

        /// @brief Plot single line
        void plotLine(const ColoredVertex& first, const ColoredVertex& second, const float4x4* pose = nullptr);
        /// @brief Plot multiply lines
        void plotLines(const ColoredVertex* vertices, uint numLines, const float4x4* pose = nullptr);
        /// @brief Plot colored lines
        void plotLines(const ColoredVertex* vertices, uint color, uint numLines, const float4x4* pose = nullptr);
        /// @brief Plot indexed lines
        void plotLines(const ColoredVertex* vertices, uint numVertices,
                       const ushort* indices, uint numLines, const float4x4* pose = nullptr);
        /// @brief Plot indexed and colored lines
        void plotLines(const ColoredVertex* vertices, uint numVertices,
                       const ushort* indices, uint numLines, uint color, const float4x4* pose = nullptr);
        /// @}

        /// @name Draw triangles
        /// @{

        /// @brief Plot single triangle
        void plotTri(const ColoredVertex& v1st,
                     const ColoredVertex& v2nd, const ColoredVertex& v3rd, const float4x4* pose = nullptr);
        /// @brief Plot multiply triangles
        void plotTri(const ColoredVertex* vertices, uint numTriangles, const float4x4* pose = nullptr);
        /// @brief Plot multiply colored triangles
        void plotTri(const ColoredVertex* vertices, uint color, uint numTriangles, const float4x4* pose = nullptr);
        /// @brief Plot multiply indexed triangles
        void plotTri(const ColoredVertex* vertices, uint numVertices,
                     const ushort* indices, uint numTriangles, const float4x4* pose = nullptr);
        /// @brief Plot multiply indexed and colored triangles
        void plotTri(const ColoredVertex* vertices, uint numVertices, uint color,
                     const ushort* indices, uint numTriangles, const float4x4* pose = nullptr);
        /// @brief Plot multiply indexed textured triangles
        void plotTri(const TexturedVertex* vertices, uint numVertices,
                     const ushort* indices, uint numTriangles, 
                     const float4x4* pose = nullptr);
        /// @brief Plot flat quad (@a v1st and @a v2nd is min and max vertices)
        void plotQuad(const float2& v1st, const float2& v2nd, uint color, const float4x4* pose = nullptr);
        /// @brief Plot flat textured quad (@a v1st and @a v2nd is min and max vertices)
        void plotQuad(const float2& v1st, const float2& v2nd, const float4x4* pose = nullptr);
        /// @}

        /// @name Draw wire figures
        /// @{

        /// @brief Plot wire-frame box
        void plotWireBox(const float4x4& worldPose, uint color, const float3& halfSize);
        /// @brief Plot wire-frame sphere
        void plotWireSphere(const float4x4& worldPose, uint color, float radius);
        /// @brief Plot wire-frame x-axis capsule
        void plotWireCapsuleZ(const float4x4& worldPose, uint color, float radius, float length);
        /// @brief Plot wire-frame y-axis capsule
        void plotWireCapsuleX(const float4x4& worldPose, uint color, float radius, float length);
        /// @brief Plot wire-frame z-axis capsule
        void plotWireCapsuleY(const float4x4& worldPose, uint color, float radius, float length);
        /// @}

        /// @name Draw text
        /// @param colorMap
        ///   Contains per-symbol color or color index in palette started from 32 (space)
        /// @param palette
        ///   Text color palette
        /// @{

        /// @brief Plot text with single color
        int2 plotText(Font& font, const char* text, const int2 pos, uint color);
        /// @brief Plot text with pre-symbol color
        int2 plotText(Font& font, const char* text, const int2 pos, const uint* colorMap);
        /// @brief Plot text with pre-symbol color index
        int2 plotText(Font& font, const char* text, const int2 pos,
                      const char* colorMap, uint* palette);
        /// @brief Plot 2D text projected from 3D space (single color)
        void plotText3D(Font& font, const char* text, const float3& pos, uint color);
        /// @brief Plot text with single color
        int2 plotTextWide(Font& font, const uint* text, const int2 pos, uint color);
        /// @brief Plot text with pre-symbol color
        int2 plotTextWide(Font& font, const uint* text, const int2 pos, const uint* colorMap);
        /// @brief Plot text with pre-symbol color index
        int2 plotTextWide(Font& font, const uint* text, const int2 pos,
                      const char*colorMap, uint*palette);
        /// @brief Plot 2D text projected from 3D space (single color)
        void plotTextWide3D(Font& font, const uint* text, const float3& pos, uint color);
        /// @}
    public:
        /// @brief Generate n-rings by m-parts sphere vertices from - to + with duplicate equator
        static void generateSphereVertices(ColoredVertex*p, ushort n, ushort m, float r);
        /// @brief Generate indices for previous vertices, enable @a capsule to connect equator with itself
        static void generateSphereWireIndices(ushort*p, ushort n, ushort m, bool capsule);
        /// @brief Transform each vertex and apply the color
        static void applyTransform(const float4x4& worldPose, uint color,
                                   ColoredVertex* vertices, uint numVertices);
    private:
        /// @brief Vertex cache size
        static const uint VertexCacheSize = 1024 * 512;
        /// @brief ColoredVertex cache size
        uint ColorVertexCahceSize = VertexCacheSize / sizeof(ColoredVertex);
        /// @brief TexturedVertex cache size
        uint TexturedVertexCahceSize = VertexCacheSize / sizeof(TexturedVertex);
        /// @brief Index cache size
        static const uint IndexCacheLimit = 1024 * 128;
        /// @brief Sphere parameter #1
        static const uint SphereSegLong = 8;
        /// @brief Sphere parameter #2
        static const uint SphereSegLat = 8;
    private:
        /// @brief Generate states
        void generateStates(bool depthTest, bool alphaBlending);
        /// @brief Flush geometry if plot mode, texture or camera differ from current
        void flushIf(CachedData newMode, Texture2DHandle newTexture,
                     const Camera& camera);
        /// @brief Flush rendering cache
        void flush();

        /// @brief Put vertices to cache
        template <class T>
        void putVertices(vector<T>& vertexCache, uint vertexCacheLimit,
                         const T* vertices, uint numVertices, uint stride,
                         uint* colorReplacer, const float4x4* transform);
        /// @brief Put vertices and indices to cache
        template <class T>
        void putVertices(vector<T>& vertexCache, uint vertexCacheLimit,
                         const T* vertices, uint numVertices,
                         const ushort* indices, uint numIndices,
                         uint stride, uint* colorReplacer,
                         const float4x4* transform);
        /// @brief Put symbol to cache
        void putSymbol(const Symbol& symbol, const int2& pos, uint color);

        /// @brief Plot UTF8 text
        template <class Painter>
        int2 putText(Font& font, const char* text, const int2& pos,
                     Painter picker);
        /// @brief Plot UTF32 text
        template <class Painter>
        int2 putText(Font& font, const uint* text, const int2& pos,
                     Painter picker);
        /// @brief Plot lines
        void putLines(const ColoredVertex* vertices, uint numVertices,
                      const ushort* indices, uint numLines, uint* color,
                      const float4x4* transform);
        /// @brief Plot triangles
        void putTris(const ColoredVertex* vertices, uint numVertices,
                     const ushort* indices, uint numTris, uint* color,
                     const float4x4* transform);
        /// @brief Plot triangles
        void putTris(const TexturedVertex* vertices, uint numVertices,
                     const ushort* indices, uint numTris, 
                     const float4x4* transform);

        /// @brief Get color by symbol (single)
        static uint pickColorSingle(uint symbol, uint color);
        /// @brief Get color by symbol (array)
        static uint pickColorMultiply(uint symbol, const uint* colors);
        /// @brief Get color by symbol (palette)
        static uint pickColorPalette(uint symbol, const char* colors,
                                     const uint* palette);
    public:
        /// @brief Renderer
        Renderer& renderer;
    private:
        EffectHandle m_plainEffect;

        struct PerBatch
        {
            float4x4 mModelViewProj;
            float4x4 mModelView;
            float4 vLightDirection;
            float4 vLightColor;
        };
        PerBatch m_perBatch;
        BufferHandle m_bufferPerBatch;
        EffectResourceHandle m_handleTexture;

        ProgramHandle m_programColored;
        ProgramHandle m_programTextured;
        ProgramHandle m_programLighted;

        LayoutHandle m_layoutColored;
        LayoutHandle m_layoutTextured;
        LayoutHandle m_layoutLighted;

        MultiArray<StateHandle, NumDrawModes, 2, 2> m_states;

        BufferHandle m_vertexBuffer;
        BufferHandle m_indexBuffer;

        Texture2DHandle m_texture;

        vector<ColoredVertex> m_cachedColoredVertex;
        vector<TexturedVertex> m_cachedTexturedVertex;
        vector<ushort> m_cachedIndex;

        int2 m_windowSize;
        Camera m_defaultCamera;
        const Camera* m_camera = &m_defaultCamera;

        CachedData m_cachedData = CachedUnknown;
        DrawMode m_drawMode = DrawNothing;
        StateHandle m_drawState;
    };
    /// @}
}
