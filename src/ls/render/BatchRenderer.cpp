#include "pch.h"
#include "ls/render/BatchRenderer.h"

#include "ls/gapi/Renderer.h"

using namespace ls;
BatchRenderer::BatchRenderer(Renderer& renderer,
                             const uint numBuffers, const uint bufferSize)
    : m_renderer(renderer)
    , m_numBuffers(numBuffers)
    , m_bufferSize(bufferSize)
{
    debug_assert(numBuffers >= 0);
    debug_assert(bufferSize >= 1024);
    for (uint idx = 0; idx < numBuffers; ++idx)
    {
        BufferHandle buffer = m_renderer
            .createBuffer(m_bufferSize, BufferType::Vertex, ResourceUsage::Dynamic, nullptr);
        m_bufferPool.push_back(buffer);
    }
}
BatchRenderer::~BatchRenderer()
{
    for (BufferHandle buffer : m_bufferPool)
    {
        m_renderer.destroyBuffer(buffer);
    }
}


// Setup
BufferHandle BatchRenderer::currentBuffer()
{
    return m_bufferPool.front();
}
void BatchRenderer::setGeometryAndSlot(const GeometryBucket& geometry,
                                       const uint poolBufferSlot)
{
    debug_assert(!m_geometryUnused, "Redundant ", __FUNCTION__, " call");
    m_geometry = geometry;
    m_geometryPoolBufferSlot = poolBufferSlot;
    m_geometryDirty = true;
    m_geometryUnused = true;
}
void BatchRenderer::setGeometry(const GeometryBucket& geometry)
{
    setGeometryAndSlot(geometry);
}
void BatchRenderer::setGeometryAndPool(const GeometryBucket& geometry)
{
    debug_assert(geometry.numVertexBuffers < MaxStreams);
    setGeometryAndSlot(geometry, geometry.numVertexBuffers);
}
void BatchRenderer::setGeometryAndPool(const GeometryBucket& geometry, const uint poolBufferSlot)
{
    debug_assert(poolBufferSlot < MaxStreams);
    setGeometryAndSlot(geometry, poolBufferSlot);
}
void BatchRenderer::setResources(const ResourceBucket& resources)
{
    debug_assert(!m_resourcesDirty, "Redundant ", __FUNCTION__, " call");
    m_resourcesDirty = true;
    m_renderer.bindResources(resources);
}
void BatchRenderer::setPipelineState(StateHandle state, const ubyte stencilRef, const float4& blendColor)
{
    debug_assert(!m_stateDirty, "Redundant ", __FUNCTION__, " call");
    m_stateDirty = true;
    m_renderer.bindState(state, stencilRef, blendColor);
}
void BatchRenderer::commitBuffer(BufferHandle buffer, const void* data, const uint size)
{
    m_renderer.writeBuffer(buffer, 0, size, data);
}
uint BatchRenderer::maxAllocSize(const uint stride)
{
    return m_bufferSize / stride;
}
Blob BatchRenderer::allocatePool(const uint count, const uint stride)
{
    debug_assert(!m_bufferMapped);
    debug_assert(count <= maxAllocSize(stride));
    m_bufferMapped = true;
    void* data = m_renderer.mapBuffer(currentBuffer());
    return Blob(count * stride, data);
}
void BatchRenderer::commitAll()
{
    // Commit geometry
    if (m_geometryDirty)
    {
        m_geometryUnused = false;

        // Add buffer from pool if needed.
        // Such geometry is always dirty.
        if (m_geometryPoolBufferSlot != MaxStreams)
        {
            m_geometry.vertexBuffers[m_geometryPoolBufferSlot] = currentBuffer();
            m_geometry.numVertexBuffers = std::max(m_geometry.numVertexBuffers,
                                                   m_geometryPoolBufferSlot + 1);
        }
        else
        {
            m_geometryDirty = false;
        }

        // Bind buffers
        m_renderer.bindGeometry(m_geometry);
    }

    // Commit buffer
    if (m_bufferMapped)
    {
        // Unmap buffer
        m_bufferMapped = false;
        m_renderer.unmapBuffer(currentBuffer());

        // Roll pool
        m_bufferPool.push_back(currentBuffer());
        m_bufferPool.pop_front();
    }

    // 'Commit' resources and state
    m_resourcesDirty = false;
    m_stateDirty = false;
}
void BatchRenderer::drawBatch(const uint startVertex, const uint numVertices)
{
    commitAll();
    m_renderer.draw(startVertex, numVertices);
}
void BatchRenderer::drawBatch(const uint startIndex, const uint numIndices,
                              const uint baseVertex)
{
    commitAll();
    m_renderer.drawIndexed(startIndex, numIndices, baseVertex);
}
void BatchRenderer::drawBatchInstanced(const uint startVertex, const uint numVertices,
                                       const uint startInstance, const uint numInstances)
{
    commitAll();
    m_renderer.drawInstanced(startVertex, numVertices, startInstance, numInstances);
}
void BatchRenderer::drawBatchInstanced(const uint startIndex, const uint numIndices,
                                       const uint baseVertex,
                                       const uint startInstance, const uint numInstances)
{
    commitAll();
    m_renderer.drawIndexedInstanced(startIndex, numIndices, baseVertex, startInstance, numInstances);
}
