/// @file ls/render/BatchRenderer.h
/// @brief Batch Renderer
#pragma once

#include "ls/common.h"
#include "ls/core/Blob.h"
#include "ls/gapi/common.h"

namespace ls
{
    class Renderer;

    /// @addtogroup LsRender
    /// @{

    /// @brief Batch Renderer
    class BatchRenderer
        : Noncopyable
    {
    public:
        /// @brief Ctor
        /// @param renderer
        ///   Underlying renderer
        /// @param numBuffers
        ///   Number of internal buffers, greaten than 0
        /// @param bufferSize
        ///   Size of each underlying buffer, not less than 1kb
        BatchRenderer(Renderer& renderer, 
                      const uint numBuffers, const uint bufferSize);
        /// @brief Dtor
        ~BatchRenderer();

        void setGeometry(const GeometryBucket& geometry);
        void setGeometryAndPool(const GeometryBucket& geometry);
        void setGeometryAndPool(const GeometryBucket& geometry, const uint poolBufferSlot);
        void setResources(const ResourceBucket& resources);
        void setPipelineState(StateHandle state, 
                              const ubyte stencilRef = 0, const float4& blendColor = float4(0.0f));
        void commitBuffer(BufferHandle buffer, const void* data, const uint size);
        template <class T>
        void commitBuffer(BufferHandle buffer, const T& data)
        {
            commitBuffer(buffer, &data, sizeof(T));
        }
        uint maxAllocSize(const uint stride);
        template <class T>
        uint maxAllocSize()
        {
            return maxAllocSize(sizeof(T));
        }
        Blob allocatePool(const uint count, const uint stride);
        template <class T>
        ArrayWrapper<T> allocatePool(const uint count)
        {
            Blob data = allocatePool(count, sizeof(T));
            return ArrayWrapper<T>(reinterpret_cast<T*>(data.data()), 
                                   data.size() / sizeof(T));
        }
        void drawBatch(const uint startVertex, const uint numVertices);
        void drawBatch(const uint startIndex, const uint numIndices,
                       const uint baseVertex);
        void drawBatchInstanced(const uint startVertex, const uint numVertices,
                                const uint startInstance, const uint numInstances);
        void drawBatchInstanced(const uint startIndex, const uint numIndices,
                                const uint baseVertex,
                                const uint startInstance, const uint numInstances);
    private:
        /// @brief Set geometry and optionally pool buffer to temporary cache
        void setGeometryAndSlot(const GeometryBucket& geometry,
                                const uint poolBufferSlot = MaxStreams);
        /// @brief Get current buffer from pool
        BufferHandle currentBuffer();
        /// @brief Commit anything to GAPI before draw call
        void commitAll();
    private:
        Renderer& m_renderer;
        const uint m_numBuffers;
        const uint m_bufferSize;

        deque<BufferHandle> m_bufferPool; ///< Buffer pool
        bool m_bufferMapped = false; ///< Is buffer mapped right now?

        GeometryBucket m_geometry; ///< Geometry to be set
        uint m_geometryPoolBufferSlot = MaxStreams;
        bool m_geometryUnused = false;
        bool m_geometryDirty = false;
        bool m_stateDirty = false;
        bool m_resourcesDirty = false;
    };

    /// @}
}