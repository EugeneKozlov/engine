#include "pch.h"
#include "ls/render/Font.h"

using namespace ls;
Font::Font(const string& name, uint size, const vector<Symbol>& symbols,
           Texture2DHandle textureId)
    : m_name(name)
    , m_pointHeigth(size)
    , m_texture(textureId)
    , m_textureSize(static_cast<float>(textureId->width), static_cast<float>(textureId->height))
{
    for (auto& source : symbols)
    {
        Symbol symbol = source;
        symbol.size = float2(static_cast<float>(symbol.width), static_cast<float>(symbol.height));
        symbol.t0 = float2(static_cast<float>(symbol.x), static_cast<float>(symbol.y)) / m_textureSize;
        symbol.t1 = symbol.t0 + symbol.size / m_textureSize;
        m_pixelHeight = max(m_pixelHeight, symbol.origHeight);

        if (symbol.id >= BaseSize)
            m_symbols.emplace(symbol.id, symbol);
        else
            m_baseSymbols[symbol.id] = symbol;

    }
    m_tabWidth = symbol(' ').origWidth * 4;
}
Font::~Font()
{
}
Symbol const& Font::symbol(uint symbol) const
{
    if (symbol < BaseSize)
        return m_baseSymbols[symbol];
    else
    {
        auto iterSymbol = m_symbols.find(symbol);
        if (iterSymbol != m_symbols.end())
            return iterSymbol->second;
        else
            return m_baseSymbols[0];
    }
}


// Computers
uint Font::length(const uint* string)
{
    uint len = 0;
    while (uint ch = *string++)
        len += symbol(ch).origWidth;
    return len;
}
uint Font::length(const uint* string, uint length)
{
    uint len = 0;
    for (uint i = 0; (i < length) && *string; ++i, ++string)
        len += symbol(*string).origWidth;
    return len;
}
uint Font::symbolsByLength(const uint* string, uint limit, bool breakAtNewline)
{
    uint len = 0, num = 0;
    while (*string && (*string != 10 || !breakAtNewline))
    {
        uint width = symbol(*string++).origWidth;
        if (len + width > limit)
            break;
        len += width;
        num++;
    }
    return num;
}
uint Font::symbolsByLengthBackward(const uint* string, uint length, uint limit)
{
    sint idx = (sint) length;
    uint len = 0, num = 0;
    while (--idx >= 0)
    {
        uint width = symbol(string[idx]).origWidth;
        if (len + width > limit)
            break;
        len += width;
        ++num;
    }
    return num;
}

