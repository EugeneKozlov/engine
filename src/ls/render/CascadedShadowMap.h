/// @file ls/render/CascadedShadowMap.h
/// @brief Cascaded Shadow Map
#pragma once

#include "ls/common.h"
#include "ls/render/Camera.h"

namespace ls
{
    /// @addtogroup LsRender
    /// @{

    /// @brief Cascaded Shadow Map
    class CascadedShadowMap
        : Noncopyable
    {
    public:
        /// @brief Minimal near plane distance
        const float minNearZ = 0.001f;
        /// @brief Cascade scale factor
        const float cascadeScale = 1.05f;
        /// @brief Cascade desc
        struct Cascade
        {
            /// @brief Near distances
            float2 nears;
            /// @brief Far distances
            float2 fars;
            /// @brief Area size
            float size = 0.0f;

            /// @brief Camera
            Camera camera;
            /// @brief Main camera frustum
            Frustum mainFrustum;
        };
    public:
        /// @brief Reset
        void reset();
        /// @brief Set cascades and shadow map texture size
        void setCascades(const vector<float>& maxDistances, float smoothFactor,
                         uint textureSize);

        /// @brief Update cascades
        void update(const Camera& camera, const float3& lightDirection,
                    float maxShadowLength);

        /// @brief Get cascade camera
        const Camera& cascadeCamera(uint cascade) const
        {
            return m_cascades[cascade].camera;
        }
        float2 nearDistances(uint cascade) const
        {
            return m_cascades[cascade].nears;
        }
        float2 farDistances(uint cascade) const
        {
            return m_cascades[cascade].fars;
        }
        const Frustum& mainFrustumSegment(uint cascade) const
        {
            return m_cascades[cascade].mainFrustum;
        }
        /// @brief Get cascade light matrix
        float4x4 lightMatrix(uint cascade) const;

        /// @brief Get number of cascades
        uint numCascades() const
        {
            return m_cascades.size();
        }
        /// @brief Get direction
        const float3& direction() const
        {
            return m_direction;
        }
    private:
        vector<Cascade> m_cascades;
        uint m_textureSize = 1;
        float3 m_direction = float3(0, -1, 0);
    };
    /// @}
}
