/// @file ls/render/Camera.h
/// @brief 3D camera
///
#pragma once
#include "ls/common.h"

namespace ls
{
    /// @addtogroup LsRender
    /// @{
    
    /// @brief Projection (ortho or perspective)
    class Projection
    {
    public:
        /// @brief Make invalid projection
        Projection() = default;
        /// @brief Make ortho projection
        Projection(float left, float right, float bottom, float top,
                   float znear, float zfar);
        /// @brief Make perspective projection by aspect
        Projection(float aspect, float fov, float znear, float zfar);
        /// @brief Make perspective projection by screen size
        template <class T>
        Projection(T width, T height, float fov, float znear, float zfar)
            : Projection(static_cast<float>(width) / static_cast<float>(height), fov, znear, zfar)
        {
        }
    public:
        /// @brief Get near clip plane distance
        float zNear() const
        {
            return m_zNear;
        }
        /// @brief Get far clip plane distance
        float zFar() const
        {
            return m_zFar;
        }
        /// @brief Get y-axis view angle
        float fovY() const
        {
            return m_fovY;
        }
        /// @brief Get width-to-height rate
        float aspect() const
        {
            return m_aspect;
        }
        /// @brief Get projection center
        const float2& center() const
        {
            return m_center;
        }
        /// @brief Get projection height
        float height() const
        {
            return m_height;
        }
        /// @brief Get projection matrix
        const float4x4& matrix() const
        {
            return m_matrix;
        }
        /// @brief Get screen rectangle - rectangle which cover all screen if it is placed
        /// perpendicularly look vector with @a z distance to eye.
        /// It is flat rectangle (x1, y1, x2, y2), where @a x1, @a x2 based on right vector
        /// and @a y1, @a y2 - on up vector.
        /// @param z
        ///   Distance
        /// @return Screen rectangle
        float4 screenRectangle(float z) const
        {
            float f = fovY() ? (z * tan(0.5f * fovY())) : 1.0f;
            float x1 = center().x - aspect() * height() * f / 2;
            float x2 = center().x + aspect() * height() * f / 2;
            float y1 = center().y - height() * f / 2;
            float y2 = center().y + height() * f / 2;
            return float4(x1, y1, x2, y2);
        }
        /// @brief Get size of screen rectangle.
        float2 screenSize(float z) const
        {
            float4 a = screenRectangle(z);
            return float2(a.z - a.x, a.w - a.y);
        }
        /// @}
    private:
        /// @brief Near clip plane distance
        float m_zNear = 0;
        /// @brief Far clip plane distance
        float m_zFar = 0;
        /// @brief Y-axis view angle (perspective-specific, 0 for ortho)
        float m_fovY = 0;
        /// @brief Width-to-height rate
        float m_aspect = 0;
        /// @brief Projection height (ortho-specific, 2 for perspective)
        float m_height = 0;
        /// @brief Projection center (ortho-specific, (0,0) for perspective)
        float2 m_center;
        /// @brief Projection matrix
        float4x4 m_matrix;
    };
    /// @brief 3D camera.
    /// @note dot(#m_look, @b UP) != 0
    class Camera
    {
    public:
        /// @brief Default up vector
        static const float3 Up; // = float3(0.0f, 1.0f, 0.0f);
    public:
        /// @brief Ctor
        Camera();
        /// @brief Dtor
        ~Camera();
    public:
        /// @brief Set projection
        void setProjection(const Projection& projection);
        /// @brief Get camera rectangle.
        /// Points are returned in such order:
        /// @code
        /// 2 -- 3  ^
        /// | Z* |  |Y
        /// 0 -- 1  |
        /// ---X-->
        /// @endcode
        /// @param distance
        ///   Distance
        /// @param [out] points
        ///   Points
        void cameraRectangle(float distance, double3 points[]) const;
        /// @brief Get camera frustum shape
        /// @see cameraRectangle
        /// @param [out] points
        ///   Points
        /// @param zNear,zFar
        ///   First and second rectangle distances
        void frustumShape(double3 points[], float zNear, float zFar) const;
        /// @brief Get camera local frustum shape
        void localFrustumShape(float3 points[], float zNear, float zFar) const;
        /// @brief Get camera frustum shape with default @a zNear and @a zFar distances
        void frustumShape(double3 points[]) const
        {
            frustumShape(points, m_proj.zNear(), m_proj.zFar());
        }
        /// @brief Convert movement in local right-up-look space to world space
        float3 local2global(const float3& move);
    public:
        /// @brief Set world center
        void setCenter(const double3& center);
        /// @brief Set by eye @a position, look @a direction and @a roll rotation.
        void setView(const double3& position, const float3& direction, float roll);
        /// @brief Set by eye @a position and eiler @a rotation.
        void setView(const double3& position, const float3& rotation);
        /// @brief Set by eye @a position, look @a target point and @a roll rotation.
        void setViewLookAtPoint(const double3& position, const double3& target, float roll);
        /// @brief Update rotation by @a delta
        void updateRotation(const float3& delta);
        /// @brief Update rotation and reset @a position
        void move(const double3& position, const float3& deltaRot = float3());
        /// @brief Update position and rotation
        void update(const float3& deltaPos, const float3& deltaRot);

        /// @brief Get world center
        const double3& worldCenter() const noexcept
        {
            return m_worldCenter;
        }
        /// @brief Get view matrix
        const float4x4& viewMatrix() const noexcept
        {
            return m_view;
        }
        /// @brief Get projection matrix
        const float4x4& projMatrix() const noexcept
        {
            return m_proj.matrix();
        }
        /// @brief Get view-projection matrix
        const float4x4& viewProjMatrix() const noexcept
        {
            return m_viewProj;
        }
        /// @brief Get world matrix
        const float4x4& worldMatrix() const noexcept
        {
            return m_world;
        }
        /// @brief Get camera frustum
        const Frustum& frustum() const noexcept
        {
            return m_frustum;
        }
        /// @brief Get camera projection
        const Projection& projection() const noexcept
        {
            return m_proj;
        }
        /// @brief Get position
        const double3& position() const noexcept
        {
            return m_worldPosition;
        }
        /// @brief Get local position
        const float3 localPosition() const noexcept
        {
            return static_cast<float3>(position() - worldCenter());
        }
        /// @brief Get eiler rotation
        const float3& rotation() const noexcept
        {
            return m_rotation;
        }
        /// @brief Get look vector.
        const float3& look() const noexcept
        {
            return m_look;
        }
        /// @brief Get up vector.
        const float3& up() const noexcept
        {
            return m_up;
        }
        /// @brief Get side vector.
        const float3& side() const noexcept
        {
            return m_side;
        }
    private:
        void updateLocalPosition();
        void updateViewByPositionRotation();
        void updatePositionRotationByView();
        void updateFrustumAndEtc();
    protected:
        /// @brief World center
        double3 m_worldCenter;
        /// @brief Real position
        double3 m_worldPosition;

        /// @brief Look position
        float3 m_position;
        /// @brief Look Euler rotation (from (0,0,1) vector)
        float3 m_rotation;
        /// @brief View matrix
        float4x4 m_view;

        /// @brief Side vector
        float3 m_side;
        /// @brief Up vector
        float3 m_up;
        /// @brief Look vector
        float3 m_look;
        /// @brief World matrix
        float4x4 m_world;
        /// @brief Camera frustum
        Frustum m_frustum;
        /// @brief Projection
        Projection m_proj;
        /// @brief View-projection matrix
        float4x4 m_viewProj;
    };
    /// @}
}
