/// @file ls/render/Model.h
/// @brief 3D Model
#pragma once

#include "ls/common.h"
#include "ls/gapi/common.h"

namespace ls
{
    /// @addtogroup LsRender
    /// @{

    /// @brief Common model vertex type
    struct VertexTangent
    {
        /// @brief Layout
        static const VertexFormat Layout[];
        /// @brief Layout size
        static const uint LayoutSize = 5;

        /// @brief Deafult ctor
        VertexTangent() = default;
        /// @brief Tiny ctor
        VertexTangent(const float3& position, const float2& uv)
            : pos(position)
            , uv(uv)
        {
            debug_assert(isFinite(position));
        }
        /// @brief Normal ctor
        VertexTangent(const float3& position, const float2& uv, const float3& normal)
            : pos(position)
            , uv(uv)
            , normal(normal)
        {
            debug_assert(isFinite(position));
        }
        /// @brief Full ctor
        VertexTangent(const float3& position, const float2& uv,
                      const float3& tangent, const float3& binormal, const float3& normal)
            : pos(position)
            , uv(uv)
            , normal(normal)
            , tangent(tangent)
            , binormal(binormal)
        {
        }

        /// @brief Position
        float3 pos;
        /// @brief Texture coord
        float2 uv;

        /// @brief Normal
        float3 normal;
        /// @brief Tangent
        float3 tangent;
        /// @brief Binormal
        float3 binormal;
    };
    static_assert(std::is_standard_layout<VertexTangent>::value, "bad vertex type");
    /// @brief Tree vertex type
    struct VertexTangentWind
    {
        /// @brief Layout
        static const VertexFormat Layout[];
        /// @brief Layout size
        static const uint LayoutSize = 5;

        /// @brief Deafult ctor
        VertexTangentWind() = default;

        /// @brief Position
        float3 pos;
        /// @brief Texture coord
        float2 uv;

        /// @brief Normal
        float3 normal;
        /// @brief Tangent
        float3 tangent;
        /// @brief Binormal
        float3 binormal;
        /// @brief Wind factor
        float4 wind;
    };
    static_assert(std::is_standard_layout<VertexTangentWind>::value, "bad vertex type");
    /// @}
}

