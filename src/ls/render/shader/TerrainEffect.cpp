#include "pch.h"
#include "ls/render/shader/TerrainEffect.h"
#include "ls/render/SceneView.h"

using namespace ls;
initializer_list<VertexFormat> const TerrainEffect::VertexLayout =
{
    VertexFormat(Format::R32G32B32A32_FLOAT, "Position", 0, 0, false),
    VertexFormat(Format::R32G32_FLOAT, "LocalTex", 0, 16, false),
    VertexFormat(Format::R32G32_INT, "Flag", 0, 24, false),
};
initializer_list<uint> const TerrainEffect::VertexStrides =
{
    VertexStride,
};
const char* TerrainEffect::EffectName = "material.terrain";

// Ctor
TerrainEffect::TerrainEffect(Renderer& renderer, Asset::Ptr effectAsset)
    : EffectInterface(renderer, effectAsset, EffectName, VertexLayout, VertexStrides)
{
    // Programs
    addShader(TerrainDetailBasePass, "TerrainDetailBasePass");
    addShader(TerrainDetailSecondPass, "TerrainDetailSecondPass");
    addShader(TerrainCachedColor, "TerrainCachedColor");
    addShader(TerrainDetailBasePassTess, "TerrainDetailBasePassTess");
    addShader(TerrainDetailSecondPassTess, "TerrainDetailSecondPassTess");
    addShader(TerrainCachedColorTess, "TerrainCachedColorTess");

    assignProgram(GeometryPolicy, TerrainDetailBasePass, "DetailBasePass");
    assignProgram(GeometryPolicy, TerrainDetailSecondPass, "DetailSecondPass");
    assignProgram(GeometryPolicy, TerrainCachedColor, "CachedColor");
    assignProgram(WiredGeometryPolicy, TerrainDetailBasePass, "DetailBasePassWired");
    assignProgram(WiredGeometryPolicy, TerrainDetailSecondPass, "DetailSecondPassWired");
    assignProgram(WiredGeometryPolicy, TerrainCachedColor, "CachedColorWired");

    assignProgram(GeometryPolicy, TerrainDetailBasePassTess, "DetailBasePassTess");
    assignProgram(GeometryPolicy, TerrainDetailSecondPassTess, "DetailSecondPassTess");
    assignProgram(GeometryPolicy, TerrainCachedColorTess, "CachedColorTess");
    assignProgram(WiredGeometryPolicy, TerrainDetailBasePassTess, "DetailBasePassWiredTess");
    assignProgram(WiredGeometryPolicy, TerrainDetailSecondPassTess, "DetailSecondPassWiredTess");
    assignProgram(WiredGeometryPolicy, TerrainCachedColorTess, "CachedColorWiredTess");

    // Buffers
}
TerrainEffect::~TerrainEffect()
{
}
void TerrainEffect::initializeShaderState(uint shaderIndex, DrawingPolicy policy, StateDesc& desc) const
{
    const Trait& trait = shaderTrait(shaderIndex);

    // Primitive
    desc.primitive = trait.isTessellated ? PrimitiveType::Patch4 : PrimitiveType::Triangle;

    // Culling
    switch (policy)
    {
    case GeometryPolicy:
    case WiredGeometryPolicy:
        desc.rasterizer.culling = Culling::Back;
        break;
    case CascadedShadowPolicy:
        desc.rasterizer.culling = Culling::Front;
        break;
    case CustomPolicy:
        break;
    default:
        break;
    }

    // Blending
    // #HACK: check policy
    if (trait.isAdditive)
    {
        desc.hasIndependentBlending = true;
        desc.blending[0] = AlphaBlendingTag();
        desc.blending[0].sourceAlpha = BlendFactor::Zero;
        desc.blending[0].destAlpha = BlendFactor::One;
        desc.blending[1] = AlphaBlendingTag();
        desc.blending[1].sourceAlpha = BlendFactor::Zero;
        desc.blending[1].destAlpha = BlendFactor::One;
        desc.blending[2] = AlphaBlendingTag();
        desc.blending[2].sourceAlpha = BlendFactor::Zero;
        desc.blending[2].destAlpha = BlendFactor::One;
        desc.blending[3] = AlphaBlendingTag();
        desc.blending[3].sourceAlpha = BlendFactor::Zero;
        desc.blending[3].destAlpha = BlendFactor::One;
    }
}
