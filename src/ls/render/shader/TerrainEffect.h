/// @file ls/render/effect/TerrainEffect.h
/// @brief Terrain Effect
#pragma once

#include "ls/common.h"
#include "ls/render/EffectInterface.h"
#include "ls/gapi/Renderer.h"

namespace ls
{
    /// @addtogroup LsRender
    /// @{

    /// @brief Terrain Effect
    class TerrainEffect
        : public EffectInterface
    {
    public:
        /// @brief Shaders
        enum Shader
        {
            /// @brief Terrain Detailed (base pass)
            TerrainDetailBasePass,
            /// @brief Terrain Detailed (second pass)
            TerrainDetailSecondPass,
            /// @brief Terrain Cached Color
            TerrainCachedColor,
            /// @brief Terrain Detailed (base pass, tessellated)
            TerrainDetailBasePassTess,
            /// @brief Terrain Detailed (second pass, tessellated)
            TerrainDetailSecondPassTess,
            /// @brief Terrain Cached Color (tessellated)
            TerrainCachedColorTess,
            /// @brief Num shaders
            NumShaders
        };
        /// @brief Traits
        struct Trait
        {
            /// @brief Self
            Shader thisType;
            /// @brief Is tessellated?
            bool isTessellated;
            /// @brief Is detailed?
            bool isDetailed;
            /// @brief Is additive pass?
            bool isAdditive;
        };
        LS_BEGIN_ENUM_TRAITS(NumShaders, shaderTrait)
        {
            LS_ADD_ENUM_TRAIT(TerrainDetailBasePass, false, true, false),
            LS_ADD_ENUM_TRAIT(TerrainDetailSecondPass, false, true, true),
            LS_ADD_ENUM_TRAIT(TerrainCachedColor, false, false, false),
            LS_ADD_ENUM_TRAIT(TerrainDetailBasePassTess, true, true, false),
            LS_ADD_ENUM_TRAIT(TerrainDetailSecondPassTess, true, true, true),
            LS_ADD_ENUM_TRAIT(TerrainCachedColorTess, true, false, false)
        }
        LS_END_ENUM_TRAITS(NumShaders);

        /// @brief Vertex format
        static const initializer_list<VertexFormat> VertexLayout;
        /// @brief Streams strides
        static const initializer_list<uint> VertexStrides;
        /// @brief Vertex stride
        static const uint VertexStride = 32;

        /// @brief Effect name
        const static char* EffectName;
    public:
        /// @brief Ctor
        TerrainEffect(Renderer& renderer, Asset::Ptr effectAsset);
        /// @brief Dtor
        virtual ~TerrainEffect();
    public:
        virtual void initializeShaderState(uint shaderIndex, DrawingPolicy policy, StateDesc& desc) const override;
    };
    /// @}
}

