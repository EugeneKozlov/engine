#include "pch.h"
#include "ls/render/CascadedShadowMap.h"
using namespace ls;

// Init
void CascadedShadowMap::reset()
{
    m_cascades.clear();
    m_textureSize = 1;
    m_direction = float3(0, -1, 0);
}
void CascadedShadowMap::setCascades(const vector<float>& maxDistances, float smoothFactor,
                                    uint textureSize)
{
    m_textureSize = textureSize;
    uint num = maxDistances.size();
    debug_assert(num > 0);
    m_cascades.resize(num);

    m_cascades[0].nears = float2(-1.0f, 0.0f);
    for (uint idx = 0; idx < num; ++idx)
    {
        Cascade& cascade = m_cascades[idx];

        // Update near
        if (idx > 0)
            cascade.nears = m_cascades[idx - 1].fars;

        // Update far
        cascade.fars[1] = maxDistances[idx];
        cascade.fars[0] = lerp(cascade.nears[1], cascade.fars[1], 1.0f - smoothFactor);
    }
}


// Update
void CascadedShadowMap::update(const Camera& camera, const float3& lightDirection,
                               float maxShadowLength)
{
    if (m_cascades.empty())
        return;

    // Light projection axises
    m_direction = lightDirection;
    double3 forward = static_cast<double3>(normalize(lightDirection));
    double3 right = normalize(cross(double3(0, 1, 0), forward));
    double3 up = normalize(cross(forward, right));

    double3x3 w2l;
    w2l.row(0, right);
    w2l.row(1, up);
    w2l.row(2, forward);
    double3x3 l2w = transpose(w2l);

    // Update cascades
    for (Cascade& cascade : m_cascades)
    {
        // WS-frustum
        double3 frustumPoints[8];
        camera.frustumShape(frustumPoints, cascade.nears[0], cascade.fars[1]);

        // LS-frustum
        double3 shadowSpaceFrustum[8];
        for (uint idx = 0; idx < 8; ++idx)
            shadowSpaceFrustum[idx] = w2l * frustumPoints[idx];
        AABB shadowBox(shadowSpaceFrustum, 8);

        // Update size
        double3 size3 = shadowBox.max - shadowBox.min;
        float newSize = max<float>(static_cast<float>(size3.x), static_cast<float>(size3.y));
        if (cascade.size < newSize)
            cascade.size = newSize * cascadeScale;
        float halfSize = cascade.size / 2;
        float texelSize = cascade.size / m_textureSize;

        // Update position
        double3 center = (shadowBox.max + shadowBox.min) / 2;
        center.z = shadowBox.min.z;
        center.x -= fmod(center.x, texelSize);
        center.y -= fmod(center.y, texelSize);

        // Update camera
        cascade.camera.setProjection(Projection(-halfSize, halfSize, -halfSize, halfSize,
                                                -maxShadowLength, 
                                                static_cast<float>(shadowBox.max.z - shadowBox.min.z)));
        cascade.camera.setView(l2w * center, lightDirection, 0.0f);
    }
}


// Gets
float4x4 CascadedShadowMap::lightMatrix(uint cascade) const
{
    float4x4 bias = {
        0.5f, 0.0f, 0.0f, 0.0f,
        0.0f, -0.5f, 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f, 0.0f,
        0.5f, 0.5f, 0.0f, 1.0f 
    };
    const Camera& camera = m_cascades[cascade].camera;
    return camera.viewProjMatrix() * bias;
}
