#include "pch.h"
#include "ls/gapi/RenderTargetPool.h"
#include "ls/gapi/Renderer.h"

using namespace ls;
// Pool
RenderTargetPool::RenderTargetPool()
{
}
RenderTargetPool::RenderTargetPool(Renderer& renderer)
    : m_renderer(&renderer)
{
}
void RenderTargetPool::init(Renderer& renderer)
{
    m_renderer = &renderer;
}
RenderTargetPool::~RenderTargetPool()
{
    release();
}
RenderTargetHandle RenderTargetPool::get(Format format, uint width, uint height)
{
    RenderTargetKey key;
    key.format = format;
    key.width = width;
    key.height = height;

    // Get
    auto cached = m_unusedTargets.find(key);
    if (cached != m_unusedTargets.end())
        return cached->second;

    // Allocate new
    Texture2DHandle texture;
    RenderTargetHandle renderTarget;
    if (isShadowFormat(format))
    {
        texture = m_renderer->createTexture2D(width, height, format,
                                              ResourceUsage::Dynamic,
                                              ResourceBinding::ShaderResource | ResourceBinding::RenderTarget,
                                              nullptr);
        renderTarget = m_renderer->createRenderTarget(1, nullptr);
        m_renderer->attachColorTexture2D(renderTarget, 0, texture);
    }
    else
    {
        texture = m_renderer->createTexture2D(width, height, format,
                                              ResourceUsage::Dynamic,
                                              ResourceBinding::ShaderResource | ResourceBinding::DepthStencil,
                                              nullptr);
        renderTarget = m_renderer->createRenderTarget(0, texture);
    }
    
    // Add and return
    m_allTargets.emplace(renderTarget, key);
    return renderTarget;
}
void RenderTargetPool::free(RenderTargetHandle renderTarget)
{
    // Find
    auto cached = m_allTargets.find(renderTarget);
    debug_assert(cached != m_allTargets.end());

    // Store
    m_unusedTargets.emplace(cached->second, renderTarget);
}
void RenderTargetPool::release()
{
    // Drop all
    debug_assert(m_unusedTargets.size() == m_allTargets.size());

    for (auto& iterTarget : m_allTargets)
    {
        RenderTargetHandle renderTarget = iterTarget.first;
        if (!!renderTarget->colorTextures[0])
            m_renderer->destroyTexture2D(renderTarget->colorTextures[0]);
        if (!!renderTarget->depthTexture)
            m_renderer->destroyTexture2D(renderTarget->depthTexture);
        m_renderer->destroyRenderTarget(renderTarget);
    }

    m_allTargets.clear();
    m_unusedTargets.clear();
}

