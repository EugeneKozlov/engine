/// @file ls/render/SceneView.h
/// @brief Scene View
#pragma once

#include "ls/common.h"
#include "ls/asset/EffectAsset.h"
#include "ls/asset/MaterialAsset.h"
#include "ls/gapi/common.h"
#include "ls/render/Camera.h"
#include "ls/render/DrawingPolicy.h"

namespace ls
{
    class StackAllocator;
    struct MaterialCacheContext;

    /// @addtogroup LsRender
    /// @{

    /// @brief Scene View and draw info
    class SceneView
    {
    public:
        /// @brief Update LODs on base pass
        bool isBasePass = false;

        MaterialCacheContext* materialCacheContext = nullptr; ///< Material cache context
        MaterialAsset::ShaderMode shaderMode; ///< Shader mode

        // #TODO Remove'em
        DrawingPolicy policy;
        StateDesc passState;

        Camera camera; ///< Main Camera
        Frustum frustum; ///< Frustum
        float3 eyePosition; ///< Eye position
        double rendererTime; ///< Renderer time
    };
    /// @}
}

