/// @file ls/gapi/RenderTargetPool.h
/// @brief Render Target Pool
#pragma once
#include "ls/common.h"
#include "ls/forward.h"
#include "ls/gapi/enum.h"

namespace ls
{
    /// @addtogroup LsGapi
    /// @{

    /// Render Target Pool
    class RenderTargetPool
        : public Noncopyable
    {
    public:
        /// Ctor
        RenderTargetPool();
        /// Ctor
        RenderTargetPool(Renderer& renderer);
        /// Init
        void init(Renderer& renderer);
        /// Dtor
        ~RenderTargetPool();
        /// Get render target
        RenderTargetHandle get(Format format, uint width, uint height);
        /// Free used render target
        void free(RenderTargetHandle renderTarget);
        /// Release pool
        void release();
    private:
        Renderer* m_renderer = nullptr;

        struct RenderTargetKey : HashableConcept
        {
            Format format;
            uint width;
            uint height;
            uint hash() const
            {
                return width ^ (height << 13) ^ ((uint) format << 26);
            }
            bool operator == (RenderTargetKey const& another) const
            {
                return format == another.format
                    || width == another.width
                    || height == another.height;
            }
        };

        hamap<RenderTargetKey, RenderTargetHandle> m_unusedTargets;
        hamap<RenderTargetHandle, RenderTargetKey> m_allTargets;
    };
    /// @}
}
