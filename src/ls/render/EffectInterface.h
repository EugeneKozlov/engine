/// @file ls/render/EffectInterface.h
/// @brief Effect Interface
#pragma once

#include "ls/common.h"
#include "ls/asset/EffectAsset.h"
#include "ls/core/AutoExpandingVector.h"
#include "ls/core/EnumTrait.h"
#include "ls/gapi/common.h"
#include "ls/scene/graphical/limits.h"
#include "ls/render/DrawingPolicy.h"

namespace ls
{
    class Renderer;
    class BufferPool;
    class EffectInterface;
    class SceneView;

    /// @addtogroup LsRender
    /// @{

    /// @brief Surface Shader
    class SurfaceShader
        : Noncopyable
    {
    public:
        /// @brief Ctor
        SurfaceShader(EffectInterface& effect, uint shaderIndex, const string& shaderName);
        /// @brief Move
        SurfaceShader(SurfaceShader&& another) noexcept;
        /// @brief Dtor
        ~SurfaceShader();
        /// @brief Add policy
        void addPolicy(DrawingPolicy policy, ProgramHandle program, LayoutHandle layout);

        /// @brief Get cached pipeline state
        StateHandle pipelineState(const SceneView& sceneView);

        /// @brief Get name
        const string& name() const
        {
            return m_shaderName;
        }
    private:
        EffectInterface& m_effect;
        const uint m_shaderIndex;
        const string m_shaderName;

        struct CachedState
        {
            bool isInitialized = false;

            ProgramHandle program;
            LayoutHandle layout;

            StateHandle pipelineState;
        };
        AutoExpandingVector<CachedState> m_cachedStates;
    };
    /// @brief Surface Shader Iterator
    using SurfaceShaderIterator = vector<SurfaceShader>::iterator;
    /// @brief Surface Shader Iterator Range
    using SurfaceShaderIteratorRange = pair<SurfaceShaderIterator, SurfaceShaderIterator>;

    /// @brief Effect Interface
    class EffectInterface
        : Noncopyable
    {
    public:
        /// @brief Renderer
        Renderer& renderer;
    protected:
        /// @brief Ctor
        EffectInterface(Renderer& renderer,
                        Asset::Ptr effectAsset,
                        const string& effectName,
                        initializer_list<VertexFormat> vertexFormat,
                        initializer_list<uint> streamStrides);
        /// @brief Add shader
        void addShader(uint shaderIndex, const string& shaderName);
        /// @brief Assign program
        void assignProgram(DrawingPolicy policy, uint shaderIndex, const string& programName);
        /// @brief Get uniform buffer (return valid or throw)
        BufferHandle getUniformBuffer(const uint size, const string& bufferName);
    public:
        /// @brief Dtor
        virtual ~EffectInterface();
        /// @brief Initialize shader state
        virtual void initializeShaderState(uint shaderIndex, DrawingPolicy policy, StateDesc& desc) const = 0;

        /// @brief Get layout
        LayoutHandle layout()
        {
            return m_layout;
        }
        /// @brief Get effect
        EffectHandle effect()
        {
            return m_effectAsset->gapiEffect();
        }
        /// @brief Get shader
        SurfaceShader& shader(uint shaderIndex)
        {
            debug_assert(shaderIndex < m_shaders.size());
            return m_shaders[shaderIndex];
        }
        /// @brief Get shaders
        SurfaceShaderIteratorRange shaders();
        /// @brief Get name
        const string& name() const
        {
            return m_effectName;
        }
    private:
        string m_effectName;
        shared_ptr<EffectAsset> m_effectAsset;

        initializer_list<VertexFormat> m_vertexFormat;
        initializer_list<uint> m_streamStrides;

        vector<SurfaceShader> m_shaders;

        LayoutHandle m_layout;
    };
    /// @}
}

