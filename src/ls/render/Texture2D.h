/// @file ls/render/Texture2D.h
/// @brief 2D Texture
#pragma once

#include "ls/common.h"
#include "ls/core/Nameable.h"
#include "ls/gapi/common.h"

namespace ls
{
    class StreamInterface;
    class Renderer;

    /// @addtogroup LsRender
    /// @{

    /// @brief 2D Texture
    class Texture2D
        : public Named
    {
    public:
        /// @brief Ctor
        Texture2D();
        /// @brief Dtor
        ~Texture2D();

        /// @brief Create GAPI buffers
        bool create(Renderer& renderer);
        /// @brief Set wrapping
        void wrap(bool wrapped);
        /// @brief Destroy GAPI buffers
        void destroy();
        /// @brief Drop cached in RAM geometry data
        void drop();

        /// @brief Serialize
        void serialize(StreamInterface& src, Compression compression) const;
        /// @brief Deserialize
        void deserialize(StreamInterface& src);

        /// @brief Get storage
        TextureStorage& storage() noexcept
        {
            return m_storage;
        }
        /// @brief Get storage
        const TextureStorage& storage() const noexcept
        {
            return m_storage;
        }
        /// @brief Get texture
        Texture2DHandle texture() const noexcept
        {
            return m_texture;
        }
    private:
        TextureStorage m_storage;

        // GAPI
        Renderer* m_renderer = nullptr;
        Texture2DHandle m_texture = nullptr;
    };
    /// @}
}

