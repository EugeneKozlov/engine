#include "pch.h"
#include "ls/render/Mesh.h"
#include "ls/gapi/Renderer.h"
#include "ls/io/StreamInterface.h"

using namespace ls;


// Sets
Mesh::Mesh()
{
    zeroMemory(m_subsets);
}
Mesh::~Mesh()
{
    dispose();
}
void Mesh::addSubset(uint textureId, uint numTriangles)
{
    debug_assert(m_numSubsets < MaxMeshSubsets);

    MeshSubset subset;
    subset.offset = m_numSubsets == 0
        ? 0 
        : (m_subsets[m_numSubsets - 1].offset + m_subsets[m_numSubsets - 1].numTriangles);
    subset.numTriangles = numTriangles;
    subset.textureId = textureId;

    m_subsets[m_numSubsets++] = subset;
}


// Serialize
void Mesh::serialize(StreamInterface& dest) const
{
    // Header
    IoHeader header;
    header.numVertices = m_numVertices;
    header.numIndices = m_numIndices;
    header.vertexStride = m_vertexStride;
    header.indexStride = m_indexStride;
    header.indexType = (uint) m_indexType;
    header.numSubsets = m_numSubsets;
    header.subsets = m_subsets;
    header.aabb = m_aabb;

    // Data
    dest.write(&header, sizeof(header));
    uint tmp1 = m_vertexData.size(), tmp2 = m_indexData.size();
    dest.write(&tmp1, 4);
    if (!m_vertexData.empty())
        dest.write(m_vertexData.data(), m_vertexData.size());
    dest.write(&tmp2, 4);
    if (!m_indexData.empty())
        dest.write(m_indexData.data(), m_indexData.size());
}
void Mesh::deserialize(StreamInterface& src)
{
    // Header
    IoHeader header;
    src.read(&header, sizeof(header));

    m_numVertices = header.numVertices;
    m_numIndices = header.numIndices;
    m_vertexStride = header.vertexStride;
    m_indexStride = header.indexStride;
    m_indexType = (IndexFormat) header.indexType;
    m_numSubsets = header.numSubsets;
    m_subsets = header.subsets;
    m_aabb = header.aabb;

    // Data
    m_vertexData.resize(m_numVertices * m_vertexStride);
    src.read(&header.numIndices, 4);
    if (m_numVertices > 0)
        src.read(m_vertexData.data(), m_vertexData.size());
    src.read(&header.numIndices, 4);
    m_indexData.resize(m_numIndices * m_indexStride);
    if (m_numIndices > 0)
        src.read(m_indexData.data(), m_indexData.size());
}


// GAPI
bool Mesh::create(Renderer& renderer)
{
    if (m_renderer == &renderer)
        return true;
    debug_assert(!m_renderer);

    Disposer<Mesh> guard(*this);
    m_renderer = &renderer;

    if (m_vertexData.empty() || m_indexData.empty())
        return false;

    // Init vb
    m_geometry.numVertexBuffers = 1;
    m_geometry.vertexBuffers[0] = renderer
        .createBuffer(m_vertexData.size(), BufferType::Vertex, ResourceUsage::Constant,
                      m_vertexData.data());
    if (!m_geometry.vertexBuffers[0])
        return false;

    // Init ib
    m_geometry.indexBuffer = renderer
        .createBuffer(m_indexData.size(), BufferType::Index, ResourceUsage::Constant,
                      m_indexData.data());
    if (!m_geometry.indexBuffer)
        return false;

    m_geometry.indexFormat = m_indexType;
    return true;
}
void Mesh::dispose()
{
    if (!m_renderer)
        return;
    for (BufferHandle& buffer : m_geometry.vertexBuffers)
    {
        if (!!buffer)
            m_renderer->destroyBuffer(buffer);
        buffer = nullptr;
    }
    m_renderer->destroyBuffer(m_geometry.indexBuffer);
    m_geometry.indexBuffer = nullptr;
    m_renderer = nullptr;
}
void Mesh::drop()
{
    m_vertexData.clear();
    m_indexData.clear();
}
void Mesh::draw(uint subsetIndex)
{
    const MeshSubset& part = subset(subsetIndex);
    m_renderer->drawIndexed(part.offset * 3, part.numTriangles * 3, 0);
}
void Mesh::drawInstanced(uint subsetIndex, uint startInstance, uint numInstances)
{
    const MeshSubset& part = subset(subsetIndex);
    m_renderer->drawIndexedInstanced(part.offset * 3, part.numTriangles * 3, 0,
                                     startInstance, numInstances);
}
