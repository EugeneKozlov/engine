/// @file ls/render/GraphicConfig.h
/// @brief Graphic Config
#pragma once

#include "ls/common.h"
#include "ls/app/ConfigManager.h"

namespace ls
{
    /// @addtogroup LsRender
    /// @{

    /// @brief Graphic Config
    class GraphicConfig
        : public ConfigPrototype
    {
    public:
        /// @brief Compute all
        void compute()
        {
            LS_THROW(*nearDistance < *farDistance,
                     LogicException,
                     "Near/far mismatch");
        }
        /// @brief Operate
        template <class Operator>
        void operate(Operator op)
        {
            op(batchAllocSize);

            op(nearDistance);
            op(farDistance);
            op(fovyAngle);
        }
    public:
        /// @brief Batch allocator bucket size
        ConfigProperty<uint> batchAllocSize
            = { "config.graphic.batch_alloc_size" };

        /// @brief Near distance
        ConfigProperty<float> nearDistance
            = { "config.graphic.near_distance", "rrNear" };
        /// @brief Far distance
        ConfigProperty<float> farDistance
            = { "config.graphic.far_distance", "rrFar" };
        /// @brief Field of view Y-angle
        ConfigProperty<float> fovyAngle
            = { "config.graphic.fovy_angle", "rrFovY" };
    };
    /// @}
}