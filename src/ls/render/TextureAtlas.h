/** @file ls/render/TextureAtlas.h
 *  @brief Texture Atlas
 */
#pragma once
#include "ls/common.h"

namespace ls
{
    /** @addtogroup LsRender
     *  @{
     */
    /// @brief Texture Atlas image
    struct TextureAtlasImage
    {
        /// @brief Ctor
        TextureAtlasImage() = default;
        /// @brief Ctor
        TextureAtlasImage(uint x, uint y, uint width, uint height)
            : x(x)
            , y(y)
            , width(width)
            , height(height)
        {
        }
    public:
        /// @brief X
        uint x = 0;
        /// @brief Y
        uint y = 0;
        /// @brief Width
        uint width = 0;
        /// @brief Height 
        uint height = 0;

        /// @brief Atlas index
        uint atlasIndex = 0;
        /// @brief Image index
        uint imageIndex = 0;
    };
    /// @brief Texture Atlas node
    struct TextureAtlasNode
    {
        MultiArray<TextureAtlasNode*, 2> child = nullptr;
        TextureAtlasImage rect;
        bool isUsed = false;
    public:
        /// @brief Ctor
        TextureAtlasNode() = default;
        /// @brief Ctor
        TextureAtlasNode(uint width, uint height)
        {
            rect.width = width;
            rect.height = height;
        };
        /// @brief Dtor
        ~TextureAtlasNode()
        {
            delete child[0];
            delete child[1];
        }
        /// @brief Insert image
        TextureAtlasNode* insert(uint width, uint height);
    };
    /// @brief Texture Atlas
    class TextureAtlas
    {
    public:
        /// @brief Ctor
        TextureAtlas(uint width, uint height);
        /// @brief Dtor
        ~TextureAtlas();

        /// @brief Add image
        uint addImage(uint width, uint height);
        /// @brief Get image
        const TextureAtlasImage& getImage(uint index) const;

        /// @brief Get number of images
        uint numImages() const
        {
            return m_images.size();
        }
        /// @brief Get atlas width
        uint width() const
        {
            return m_width;
        }
        /// @brief Get atlas height
        uint height() const
        {
            return m_height;
        }
        /// @brief Get number of atlases
        uint numAtlases() const
        {
            return m_atlases.size();
        }
    private:
        uint storeImage(TextureAtlasImage& image, uint atlasIndex);
    private:
        vector<TextureAtlasNode> m_atlases;
        vector<TextureAtlasImage> m_images;

        uint m_width;
        uint m_height;
    };
    /// @}
}