/// @file ls/render/DeferredShading.h
/// @brief Deferred shading implementation
#pragma once

#include "ls/common.h"
#include "ls/asset/MaterialAsset.h"
#include "ls/render/BatchRenderer.h"
#include "ls/render/DrawingPolicy.h"
#include "ls/render/GraphicConfig.h"
#include "ls/scene/RenderSystem.h"

namespace ls
{
    class AssetManager;
    class GraphicalScene;
    class MaterialCache;
    class Renderer;
    class SceneView;
    struct MaterialCacheContext;

    /// @addtogroup LsRender
    /// @{

    /// @brief Deferred Shading renderer
    ///
    /// G-Buffer layout:
    /// Map | Bit | Format      | Red         | Green       | Blue        | Alpha
    /// ----|-----|-------------|-------------|-------------|-------------|--------------
    /// 0   | 32  | R8G8B8A8    | BaseColor.R | BaseColor.G | BaseColor.B | Reflectivity
    /// 1   | 32  | R10G10B10A2 | NormalSm.X  | NormalSm.Y  | NormalSm.Z  | Unused
    /// 2   | 32  | R11G11B10 f | HdrColor.X  | HdrColor.Y  | HdrColor.Z  | -
    class DeferredShading
        : Noncopyable
    {
    public:
        /// @brief G-Buffer map
        enum GBufferMap
        {
            /// @brief Map 0
            Map0_ColorReflectivity = 0,
            /// @brief Map 1
            Map1_NormalSmoothness = 1,
            /// @brief Map 2
            Map2_HdrColor = 2,
            /// @brief Count
            NumGBufferMaps
        };
    public:
        /// @brief Ctor
        DeferredShading(AssetManager& assetManager, const GraphicConfig& graphicConfig,
                        EffectHandle effect);
        /// @brief Dtor
        ~DeferredShading();

        /// @brief Drop maps
        void drop();
        /// @brief Resize
        void resize(uint width, uint height);

        /// @brief Draw geometry buffer
        void drawGeometryBuffer(GraphicalScene& graphicalScene);
        /// @brief Draw lights
        void drawLights(GraphicalScene& graphicalScene);
        /// @brief Draw final image
        void drawFinalImage();
    private:
        void initPolicyState(DrawingPolicy policy, const MaterialAsset::ShaderMode mode);
        void initEffects();
        void initStates();

        void drawScene(const SceneView& sceneView, GraphicalScene& graphicalScene);
    public:
        /// @brief Renderer config
        const GraphicConfig& graphicConfig;
    private:
        struct Constant
        {
            float2 vSize;
        } m_constant;
        struct PerLight
        {
            float4x4 mViewToLight;
            float4 vColor;
            float4 vDirection;
            float4 vCameraInfo;
            float4 vNearFar;
        };
        struct SkyboxParam
        {
            float4x4 mViewProj;
            float3 vSunPosition;
            float fTurbidity;
        } m_skyboxParam;

        // Main
        AssetManager& m_assetManager;
        Renderer& m_renderer;
        MaterialCache& m_materialCache;
        BatchRenderer m_batchRenderer;
        EffectHandle m_effect;
        array<StateDesc, CustomPolicy> m_policyStates;
        array<MaterialCacheContext*, CustomPolicy> m_contexts;
        uint m_revision = 0;

        uint m_width = 0;
        uint m_height = 0;

        // Maps
        RenderTargetHandle m_zprepassRT;
        RenderTargetHandle m_gbufferRT;
        array<Texture2DHandle, NumGBufferMaps> m_gbufferMaps;
        Texture2DHandle m_depthStencil;
        RenderTargetHandle m_lightRT;

        RenderTargetHandle m_linearDepthRT;
        Texture2DHandle m_linearDepth;

        // Shadow maps
        Texture2DHandle m_shadowMap;
        RenderTargetHandle m_shadowMapRT;

        // Fullscreen textures
        Texture2DHandle m_sourceBuffer;
        RenderTargetHandle m_sourceTarget;
        Texture2DHandle m_destBuffer;
        RenderTargetHandle m_destTarget;

        // Effects
        EffectResourceHandle m_handleShadowMap;
        BufferHandle m_handleConstant;
        BufferHandle m_handlePerLight;
        BufferHandle m_handleSkyboxParam;

        // Draw states
        StateHandle m_drawSkybox;
        StateHandle m_drawDirectional;
        StateHandle m_drawFinal;
    };
    /// @}
}

