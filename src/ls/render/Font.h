/// @file ls/render/Font.h
/// @brief Font from file
#pragma once

#include "ls/common.h"
#include "ls/gapi/common.h"

namespace ls
{
    /// @addtogroup LsRender
    /// @{

    /// @brief Font symbol description.
    struct Symbol
    {
    public:
        /// @brief Empty
        Symbol() = default;
        /// @brief Ctor
        /// @param id
        ///   Symbol ID
        /// @param x,y,width,height
        ///   Position in texture
        /// @param xoffset,yoffset,origWidth,origHeight
        ///   Texture offset and real size
        Symbol(uint id, uint x, uint y, uint width, uint height,
               uint xoffset, uint yoffset, uint origWidth, uint origHeight)
            : id(id)
            , x(x)
            , y(y)
            , width(width)
            , height(height)
            , xoffset(xoffset)
            , yoffset(yoffset)
            , origWidth(origWidth)
            , origHeight(origHeight)
        {
        }
    public:
        /// @brief Symbol ID
        uint id = 0;
        /// @brief Position - x
        uint x = 0;
        /// @brief Position - y
        uint y = 0;
        /// @brief Size - x
        uint width = 0;
        /// @brief Size - y
        uint height = 0;
        /// @brief Offset - x
        uint xoffset = 0;
        /// @brief Offset - y
        uint yoffset = 0;
        /// @brief Real size - x
        uint origWidth = 0;
        /// @brief Real size - y
        uint origHeight = 0;
        /// @brief Relative size in texture
        float2 size;
        /// @brief Texture coordinate of left-top
        float2 t0;
        /// @brief Texture coordinate of right-bottom
        float2 t1;
    };
    /// @brief Font
    class Font
        : Noncopyable
    {
    public:
        /// @brief Base table size
        static const uint BaseSize = 128;
        /// @brief Invalid
        Font() = default;
        /// @brief Ctor
        /// @param name
        ///   Font name
        /// @param size
        ///   Font size in points
        /// @param symbols
        ///   Array of symbols
        /// @param textureId
        ///   Symbols texture
        Font(const string& name, uint size,
             const vector<Symbol>& symbols, Texture2DHandle textureId);
        /// @brief Dtor
        ~Font();

        /// @brief Get symbol description
        const Symbol& symbol(uint symbol) const;
        /// @brief Get symbol height
        uint height() const noexcept
        {
            return m_pixelHeight;
        }
        /// @brief Get tab width in points (4 spaces)
        uint tabWidth() const noexcept
        {
            return m_tabWidth;
        }
        /// @brief Get font texture
        Texture2DHandle texture() const noexcept
        {
            return m_texture;
        }

        /// @brief Get string length (in points)
        uint length(const uint* string);
        /// @brief Get string length (in points)
        uint length(const uint* string, uint length);
        /// @brief Get number of symbols which can be placed in line with @a max_length points width.
        uint symbolsByLength(const uint* string, uint limit,
                             bool breakAtNewline = true);
        /// @brief Get number of symbols can be placed in line with @a limit points width
        uint symbolsByLengthBackward(const uint* string, uint length, uint limit);
    protected:
        /// @brief Name
        string m_name;

        /// @brief Height in points
        uint m_pointHeigth = 0;
        /// @brief Height in pixels
        uint m_pixelHeight = 0;
        /// @brief Tab width in points
        uint m_tabWidth = 0;

        /// @brief Font texture. May be shared between several fonts.
        Texture2DHandle m_texture;
        /// @brief Texture size
        float2 m_textureSize;

        /// @brief Base array
        array<Symbol, BaseSize> m_baseSymbols;
        /// @brief Symbols array
        map<uint, Symbol> m_symbols;
    };
    /// @}
}
