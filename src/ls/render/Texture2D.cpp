#include "pch.h"
#include "ls/render/Texture2d.h"
#include "ls/io/StreamInterface.h"
#include "ls/gapi/detail/TextureLoader.h"
#include "ls/gapi/Renderer.h"
#include <squish/squish.h>

using namespace ls;


// Sets
Texture2D::Texture2D()
{
}
Texture2D::~Texture2D()
{
    destroy();
}


// Serialize
void Texture2D::serialize(StreamInterface& dest, Compression compression) const
{
    debug_assert(!m_storage.empty());
    if (compression == Compression::No)
    {
        gapi::saveDDS(dest, m_storage);
    }
    else
    {
        debug_assert(m_storage.format() == gli::FORMAT_RGBA8_UNORM_PACK8);
        int flags = gapi::squishFlags(compression);
        gli::texture2DArray compressed(gapi::compressedFormat(compression),
                                       m_storage.dimensions(),
                                       m_storage.layers(), m_storage.levels() - 2);
        for (uint slice = 0; slice < m_storage.layers(); ++slice)
        {
            for (uint mipLevel = 0; mipLevel < compressed.levels(); ++mipLevel)
            {
                uint width = m_storage.dimensions().x >> mipLevel;
                uint height = m_storage.dimensions().y >> mipLevel;
                gli::image argb = m_storage[slice][mipLevel];
                gli::image dxt = compressed[slice][mipLevel];
                squish::CompressImage(static_cast<const ubyte*>(argb.data()), width, height,
                                      dxt.data(), flags);
            }
        }
        gapi::saveDDS(dest, compressed);
    }
}
void Texture2D::deserialize(StreamInterface& src)
{
    gli::texture storage = gapi::loadDDS(src);
    reconstruct(m_storage, storage);
}


// GAPI
bool Texture2D::create(Renderer& renderer)
{
    if (m_renderer == &renderer)
        return true;
    debug_assert(!m_renderer);
    m_renderer = &renderer;

    if (m_storage.empty())
        return false;

    m_texture = renderer.loadTexture2D(m_storage);

    return true;
}
void Texture2D::wrap(bool wrapped)
{
    debug_assert(m_renderer);
    m_renderer->setTexture2DSampler(m_texture, Filter::Linear, 
                                    wrapped ? Addressing::Wrap : Addressing::Clamp);
}
void Texture2D::destroy()
{
    if (!m_renderer)
        return;
    m_renderer->destroyTexture2D(m_texture);
    m_renderer = nullptr;
}
void Texture2D::drop()
{
    m_storage.clear();
}
