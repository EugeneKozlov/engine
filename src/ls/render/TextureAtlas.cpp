#include "pch.h"
#include "ls/render/TextureAtlas.h"

using namespace ls;


// Node
TextureAtlasNode* TextureAtlasNode::insert(uint width, uint height)
{
    if (child[0] || child[1])
    {
        TextureAtlasNode* newNode = child[0]->insert(width, height);
        if (newNode) return newNode;
        return child[1]->insert(width, height);
    }
    else
    {
        // Already has image
        if (isUsed)
            return nullptr;

        // Too small
        if (width > rect.width || height > rect.height)
            return nullptr;

        // Perfectly 
        if (width == rect.width && height == rect.height)
        {
            isUsed = true;
            return this;
        }

        // Split
        child[0] = new TextureAtlasNode();
        child[1] = new TextureAtlasNode();

        uint dw = rect.width - width;
        uint dh = rect.height - height;

        if (dw > dh)
        {
            child[0]->rect = TextureAtlasImage(rect.x, rect.y, width, rect.height);
            child[1]->rect = TextureAtlasImage(rect.x + width, rect.y, dw, rect.height);
        }
        else
        {
            child[0]->rect = TextureAtlasImage(rect.x, rect.y, rect.width, height);
            child[1]->rect = TextureAtlasImage(rect.x, rect.y + height, rect.width, dh);
        };

        return child[0]->insert(width, height);
    };
}


// Atlas
TextureAtlas::TextureAtlas(uint width, uint height)
    : m_width(width)
    , m_height(height)
{
}
TextureAtlas::~TextureAtlas()
{

}

uint TextureAtlas::addImage(uint width, uint height)
{
    debug_assert(width <= m_width && height <= m_height);
    for (uint atlasIndex = 0; atlasIndex < m_atlases.size(); ++atlasIndex)
    {
        TextureAtlasNode* node = m_atlases[atlasIndex].insert(width, height);
        if (node)
            return storeImage(node->rect, atlasIndex);
    }
    TextureAtlasNode* node = m_atlases.back().insert(width, height);
    m_atlases.emplace_back(m_width, m_height);
    return storeImage(node->rect, m_atlases.size() - 1);
}
uint TextureAtlas::storeImage(TextureAtlasImage& image, uint atlasIndex)
{
    uint index = m_images.size();
    image.imageIndex = index;
    image.atlasIndex = atlasIndex;
    m_images.push_back(image);
    return index;
}
TextureAtlasImage const& TextureAtlas::getImage(uint index) const
{
    return m_images[index];
}
