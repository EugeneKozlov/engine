/** @file ls/network/reflection.h
 *  @brief System network messages reflection
 */
#pragma once
#include "ls/common.h"
#include "ls/io/Encoded.h"
#include "ls/network/message.h"
#include "ls/network/NetworkReflection.h"

namespace ls
{
    /** @addtogroup LsNetwork
     *  @{
     */
    /** #MessageCode::Welcome reflection
     */
    class WelcomeMessage
    : public MessageReflection<MessageCode::Welcome>
    {
    public:
        /// @brief Header
        struct Header
        : public OperableConcept
        {
            /// @brief Version
            Encoded<uint, Encoding::Int> version = 0;
            /// @brief Operate
            template <class Function>
            inline void operate(Function& foo)
            {
                foo(version);
            }
        } h; ///< Message header
    };
    /** #MessageCode::Auth reflection
     */
    class AuthMessage
    : public MessageReflection<MessageCode::Auth>
    {
    public:
        /// @brief Buffer size
        static const ubyte BufferSize = 128;
        /// @brief Buffer type
        using Buffer = array<ubyte, BufferSize>;
        /// @brief Flags enum
        enum EnumFlags
        {
            /// @brief IPv4
            FlagIPv4 = 1 << 1,
            /// @brief IPv6
            FlagIPv6 = 1 << 2,
        };
    public:
        /// @brief Header
        struct Header
        : public OperableConcept
        {
            /// @brief Flags
            Encoded<ubyte, Encoding::Byte> flags = 0;
            /// @brief IP
            Encoded<array<ubyte, 16>, Encoding::Byte> serverIp;

            /// @brief TCP server port
            Encoded<ushort, Encoding::Short> supernodeTcp = 0;
            /// @brief UDP server port
            Encoded<ushort, Encoding::Short> supernodeUdp = 0;
            /// @brief Buffer
            Encoded<Buffer, Encoding::Byte> buffer;
            /// @brief Operate
            template <class Function>
            inline void operate(Function& foo)
            {
                foo(flags);
                foo(serverIp);
                foo(supernodeTcp);
                foo(supernodeUdp);
                foo(buffer);
            }
        } h; ///< Message header
    public:
        /** @name Main
         *  @{
         */
        /// @brief Empty ctor.
        AuthMessage() = default;
        /** Request ctor.
         *  @param ip
         *    Server IP
         *  @param supernodeTcp
         *    Client TCP super-node port
         *  @param supernodeUdp
         *    Client TCP super-node port
         *  @param buffer
         *    Buffer to send
         */
        AuthMessage(const string& ip,
                    ushort supernodeTcp, ushort supernodeUdp,
                    const ubyte* buffer)
            : MessageReflection(true)
        {
            setIp(ip);
            h.supernodeTcp = supernodeTcp;
            h.supernodeUdp = supernodeUdp;
            if (buffer)
                std::copy_n(buffer, BufferSize, h.buffer->begin());
            else
                h.buffer->fill(0);
        }
        /// @}

        /** @name Gets
         *  @{
         */
        /// @brief Get IP
        string ip() const
        {
            return itoaIP(h.serverIp->data(), (*h.flags & FlagIPv4) ? 4 : 16);
        }
        /// @brief Test if specified is super-node
        bool isSuper() const
        {
            return *h.supernodeTcp != 0 && *h.supernodeUdp != 0;
        }
        /// @}
    private:
        void setIp(const string& ip)
        {
            switch (atoiIP(ip.c_str(), h.serverIp->data()))
            {
            case 4:
                *h.flags |= FlagIPv4;
                break;
            case 16:
                *h.flags |= FlagIPv6;
                break;
            case 0:
                debug_assert(0);
            }
        }
    };
    /** #MessageCode::Register reflection
     */
    class RegisterMessage
    : public MessageReflection<MessageCode::Register>
    {
    public:
        /// @brief Buffer size
        static const ubyte BufferSize = 128;
        /// @brief Buffer type
        typedef array<ubyte, BufferSize> Buffer;
        /// @brief Flags enum
        enum EnumFlags
        {
            /// @brief Set if message is related to receiver
            FlagSelf = 1 << 0,
            /// @brief IPv4
            FlagIPv4 = 1 << 1,
            /// @brief IPv6
            FlagIPv6 = 1 << 2,
        };
    public:
        /// @brief Header
        struct Header
        : public OperableConcept
        {
            /// @brief Local ID
            Encoded<ClientId, Encoding::Short> clientId = NetInvalidClient;
            /// @brief Flags
            Encoded<ubyte, Encoding::Byte> flags = 0;
            /// @brief Secret identification code
            Encoded<uint, Encoding::Int> authCode = 0;
            /// @brief IP
            Encoded<array<ubyte, 16>, Encoding::Byte> supernodeIp;

            /// @brief TCP server port
            Encoded<ushort, Encoding::Short> supernodeTcp = 0;
            /// @brief UDP server port
            Encoded<ushort, Encoding::Short> supernodeUdp = 0;
            /// @brief Buffer
            Encoded<Buffer, Encoding::Byte> buffer;
            /// @brief Operate
            template<class Function>
            inline void operate(Function& foo)
            {
                foo(clientId);
                foo(flags);
                foo(authCode);
                foo(supernodeIp);

                foo(supernodeTcp);
                foo(supernodeUdp);
                foo(buffer);
            }
        } h; ///< Message header
    public:
        /** @name Main
         *  @{
         */
        /// @brief Empty ctor.
        RegisterMessage() = default;
        /** Ctor.
         *  @param client
         *    This client ID
         *  @param authCode
         *    Identification code
         *  @param ip
         *    Super-node IP
         *  @param supernodeTcp
         *    Client TCP super-node port
         *  @param supernodeUdp
         *    Client TCP super-node port
         */
        RegisterMessage(ClientId client,
                        uint authCode,
                        const string& ip,
                        ushort supernodeTcp, ushort supernodeUdp)
            : MessageReflection(true)
        {
            h.clientId = client;
            h.authCode = authCode;
            h.supernodeTcp = supernodeTcp;
            h.supernodeUdp = supernodeUdp;
            setIp(ip);
        }
        /** Copy and clear secret code.
         *  @param secretCode
         *    Specifies if secret code should be written
         *  @param isSelf
         *    Should self flag be set?
         *  @return Copy
         */
        RegisterMessage copy(bool secretCode, bool isSelf)
        {
            RegisterMessage copy(*this);
            if (!secretCode)
                *copy.h.authCode = 0;

            if (isSelf)
                *copy.h.flags |= FlagSelf;
            else
                *copy.h.flags &= ~FlagSelf;

            return copy;
        }
        /// @}

        /** @name Gets
         *  @{
         */
        /// @brief Get IP
        string ip() const
        {
            return itoaIP(h.supernodeIp->data(), (*h.flags & FlagIPv4) ? 4 : 16);
        }
        /// @brief Test if specified is super-node
        bool isSuper() const
        {
            return *h.supernodeTcp != 0;
        }
        /// @brief Test if #FlagSelf is enabled
        bool isSelf() const
        {
            return !!(*h.flags & FlagSelf);
        }
        /// @}
    private:
        void setIp(const string& ip)
        {
            switch (atoiIP(ip.c_str(), h.supernodeIp->data()))
            {
            case 4:
                *h.flags |= FlagIPv4;
                break;
            case 16:
                *h.flags |= FlagIPv6;
                break;
            case 0:
                debug_assert(0);
            }
        }
    };
    /** #MessageCode::Deregister reflection
     */
    class DeregisterMessage
    : public MessageReflection<MessageCode::Deregister>
    {
    public:
        /// @brief Header
        struct Header
        : public OperableConcept
        {
            /// @brief Local ID
            Encoded<ClientId, Encoding::Short> clientId = NetInvalidClient;
            /// @brief Flags
            Encoded<ubyte, Encoding::Byte> flags = 0;
            /// @brief Operate
            template<class Function>
            inline void operate(Function& foo)
            {
                foo(clientId);
                foo(flags);
            }
        } h; ///< Message header
    public:
        /** @name Main
         *  @{
         */
        /// @brief Empty ctor.
        DeregisterMessage() = default;
        /** Request ctor.
         *  @param clientId
         *    Node ID
         *  @param flags
         *    Flags
         */
        DeregisterMessage(ClientId clientId, ubyte flags)
        {
            h.clientId = clientId;
            h.flags = flags;
        }
        /// @}
    };
    /** #MessageCode::Establish reflection
     */
    class EstablishMessage
    : public MessageReflection<MessageCode::Establish>
    {
    public:
        /// @brief Header
        struct Header
        : public OperableConcept
        {
            /// @brief Local ID
            Encoded<ClientId, Encoding::Short> localId = NetInvalidClient;
            /// @brief Flags
            Encoded<ClientId, Encoding::Short> remoteId = NetInvalidClient;
            /// @brief Client secret code
            Encoded<uint, Encoding::Int> secretCode = 0;
            /// @brief Operate
            template<class Function>
            inline void operate(Function& foo)
            {
                foo(localId);
                foo(remoteId);
                foo(secretCode);
            }
        } h; ///< Message header
    public:
        /** @name Main
         *  @{
         */
        /// @brief Empty
        EstablishMessage() = default;
        /** Create any connection.
         *  @param secretCode
         *    Client secret code
         *  @param thisClient
         *    This client id.
         *  @param remoteClient
         *    Remote client id.
         */
        EstablishMessage(uint secretCode,
                         ClientId thisClient, ClientId remoteClient)
        {
            h.secretCode = secretCode;
            h.localId = thisClient;
            h.remoteId = remoteClient;
        }
    };
    /// @}
}