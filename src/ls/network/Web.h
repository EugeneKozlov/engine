/** @file ls/network/Web.h
 *  @brief Network nodes web
 */
#pragma once
#include "ls/common.h"
#include "ls/core/Nameable.h"
#include "ls/network/NetworkMessage.h"

namespace ls
{
    class TcpSocket;
    class TcpServer;
    class UdpSocket;

    class RegisterMessage;
    class DeregisterMessage;
    class EstablishMessage;

    /** @addtogroup LsNetwork
     *  @{
     */
    /** Network nodes web.
     */
    class Web
    : Noncopyable
    , public Nameable<Web>
    {
    public:
        /// @brief Default number of ping samples
        static const uint DefaultNumPingSamples = 5;
        /// @brief Web node type
        enum Type
        {
            /// @brief Root node
            Root,
            /// @brief Super-node (support)
            Super,
            /// @brief Basic node
            Basic,
            /// @brief Invalid node
            Invalid
        };
        /// @brief Test super-node server (optional)
        function<bool(const ubyte* buffer, const string& ip, ushort port) > onTestSuper;
        /// @brief Add client
        function<void(ClientId id) > onAddNode;
        /// @brief Remove client
        function<void(ClientId id) > onRemoveNode;
        /// @brief Receive TCP message
        function<void(ClientId id, const NetworkMessage& message) > onReceiveTcp;
        /// @brief Receive UDP message
        function<void(ClientId id, const NetworkMessage& message) > onReceiveUdp;
    public:
        /** @name Main
         *  @{
         */
        /** Create web.
         *  @note You should specify IP even for root node
         *  @note IPv4 or IPv6 will be chosen depending on @a ip format
         *  @param timeout
         *    Connection timeout
         *  @param ip,port
         *    Root node port and IP
         *  @param nodeType
         *    Node type
         *  @param numSuperNodes
         *    Max number of super nodes
         *  @param numPingSamples
         *    Number of ping median computer samples
         */
        Web(DeltaTime timeout, const string& ip, ushort port, Type nodeType,
            uint numSuperNodes = 128, uint numPingSamples = DefaultNumPingSamples);
        /// @brief Dtor
        ~Web();
        /** Update web, receive loopback.
         *  If error occurs, you should destroy the web.
         *  @return @c true on success
         */
        bool update();
        /// @brief Flush sockets outputs
        void flush();
        /// @brief Re-establish UDP connections
        void reestablishUdp();
        /// @brief Kick @a node
        bool kick(ClientId node);
        /// @brief Ping all accepted clients
        void pingAll();
        /** Destroy this node.
         *  @return @c false
         */
        bool destroy();
        /// @}


        /** @name IO
         *  @return @c true on success
         *  @{
         */
        /// @brief Send TCP @a message to @a node (NetBroadcastClient is allowed)
        bool sendTcp(ClientId node, const NetworkMessage& message);
        /// @brief Send UDP @a message to @a node (NetBroadcastClient is allowed)
        bool sendUdp(ClientId node, const NetworkMessage& message);
        /// @}

        /** @name Gets
         *  @{
         */
        /// @brief Get ID
        ClientId id() const
        {
            return m_id;
        }
        /// @brief Get type
        Type type() const
        {
            return m_type;
        }
        /// @brief Test is node is root
        bool isRoot() const
        {
            return m_type == Root;
        }
        /// @brief Test if this node is super-node
        bool isSuper() const
        {
            return m_type != Basic;
        }
        /// @brief Test if specified @a node is super-node (@a node may be invalid)
        bool isSuper(ClientId node) const;
        /// @brief Test if this node is valid
        bool isValid() const
        {
            return m_valid;
        }
        /// @brief Test if valid super @a node is connected
        bool isConnected(ClientId node) const;
        /// @brief Get valid @a node ping (root/super only)
        DeltaTime ping(ClientId node) const;
        /// @brief Get debug valid @a node name
        const char* debugNodeName(ClientId node) const;
        /// @brief Debug log all nodes
        void debugLogNodes() const;
        /// @}

        /** @name Traffic
         *  @return Number of bytes
         *  @{
         */
         /// @brief Get number of received by TCP bytes
        ulong inputTcp() const noexcept
        {
            return m_inputTcp;
        }
        /// @brief Get number of sent by TCP bytes.
        ulong outputTcp() const noexcept
        {
            return m_outputTcp;
        }
        /// @brief Get number of received by UDP bytes
        ulong inputUdp() const noexcept
        {
            return m_inputUdp;
        }
        /// @brief Get number of sent by UDP bytes.
        ulong outputUdp() const noexcept
        {
            return m_outputUdp;
        }
        /// @brief Get number of received UDP messages
        uint inputMessages() const noexcept
        {
            return m_inputCount;
        }
        /// @brief Get number of sent by UDP bytes.
        uint outputMessages() const noexcept
        {
            return m_outputCount;
        }
        /// @brief Reset byte count
        void resetTraffic() noexcept
        {
            m_inputTcp = 0;
            m_inputUdp = 0;
            m_inputCount = 0;
            m_outputTcp = 0;
            m_outputUdp = 0;
            m_outputCount = 0;
        }
        /// @}
    private:
        /// @brief Node
        struct Node;
        /// @brief 'Allocate' ID
        ClientId allocateId(bool super);
        /// @brief 'Free' ID
        void freeId(ClientId id);

        /// @brief Add client
        bool onAddClient(TcpSocket& socket, const string& ip, ushort port);
        /// @brief Remove client
        void onRemoveClient(TcpSocket& socket);

        /// @brief Register node (Root)
        bool serverRegisterNode(TcpSocket& socket, const string& ip, ushort port);
        /// @brief Deregister node (Root)
        void serverDeregisterNode(Node& node);
        /// @brief Process accepted connection (Super)
        bool superAcceptNode(TcpSocket& socket, const string& ip, ushort port);
        /// @brief Process removed connection (Super)
        void superRemoveNode(Node& node);

        /// @brief Register node (Super or Basic)
        bool processRegister(const RegisterMessage& reg);
        /// @brief Deregister node (Super or Basic)
        bool processDeregister(const DeregisterMessage& dereg);
        /// @brief Process UDP establish
        bool processEstablish(const EstablishMessage& est, const UdpSocketEndpoint& ep);
        /// @brief Process any UDP message
        bool processUdp(const NetworkMessage& message, Node& node);
        /// @brief Process ping
        bool processPing(const NetworkMessage& ping);
        /// @brief Process ping response
        bool processPingResponce(const NetworkMessage& pingRe, Node& node);
        /// @brief Process custom UDP
        bool processOtherUdp(const NetworkMessage& message, Node& node);

        /// @brief Update this Support
        bool updateSupport();
        /// @brief Update root
        bool updateRoot();
        /// @brief Update super-nodes
        bool updateNode(Node& node);
        /// @brief Update UDP
        bool updateUdp();
        /// @brief Update self messages
        bool updateSelf();

        /// @brief Send TCP message
        bool sendTcpMessage(Node& node, const NetworkMessage& message);
        /// @brief Send UDP message
        bool sendUdpMessage(Node& node, const NetworkMessage& message);

        /// @brief Find node
        Node* findNode(ClientId id);
        /// @brief Find const node
        const Node* findNode(ClientId id) const;
        /// @brief Find node ID by UDP endpoint
        ClientId findIdByUdp(const UdpSocketEndpoint& ep);
        /// @brief Get node
        Node& getNode(ClientId id);
        /// @brief Get const node
        const Node& getNode(ClientId id) const;
    public:
        /// @brief Number of ping median computer samples
        uint numPingSamples;
        /// @brief Max number of super-nodes
        uint numSuperNodes;
    private:
        /// @brief Validity
        bool m_valid = true;
        /// @brief Set when root is registered
        bool m_rootReg = false;
        /// @brief This node type
        Type m_type = Invalid;
        /// @brief Timeout
        DeltaTime m_timeout = 0;
        /// @brief This ID
        ClientId m_id = NetInvalidClient;
        /// @brief This auth code
        uint m_authCode = 0;

        /// @brief Root node address
        NetworkAddress m_rootAddress;
        /// @brief Root node port
        ushort m_rootPort = 0;

        /// @brief This server (optional)
        unique_ptr<TcpServer> m_server;
        /// @brief Udp socket
        unique_ptr<UdpSocket> m_udp;

        /// @brief Nodes
        map<ClientId, Node> m_nodes;
        /// @brief Socket-to-ID map
        map<TcpSocket*, ClientId> m_tcpMap;
        /// @brief Endpoint-to-ID map
        map<UdpSocketEndpoint, ClientId> m_udpMap;
        /// @brief Super endpoint-to-ID map
        map<UdpSocketEndpoint, ClientId> m_superUdpMap;
        /// @brief Free IDs map (server only)
        array<bool, NetNumClients> m_usedIds;

        /// @brief Self TCP messages
        deque<NetworkMessage> m_tcpMessages;
        /// @brief Self UDP messages
        deque<NetworkMessage> m_udpMessages;

        ulong m_inputTcp = 0;
        ulong m_inputUdp = 0;
        uint m_inputCount = 0;
        ulong m_outputTcp = 0;
        ulong m_outputUdp = 0;
        uint m_outputCount = 0;
    };
    /// @}
}
