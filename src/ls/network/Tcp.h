/** @file ls/network/Tcp.h
 **  @brief TCP socket, server and client.
 **/
#pragma once
#include "ls/common.h"
#include "ls/network/NetworkMessage.h"

namespace ls
{
    /** @addtogroup LsNetwork
     *  @{
     */
    /** TCP socket.
     *  This class is used to send/receive messages.
     *  You must guarantee that @a socket is valid.
     *  First message received by socket must be eNetMessage::Welcome.
     */
    class TcpSocket
    : Noncopyable
    {
    public:
        /// @brief Set to log messages
        static bool LogMessages;
        /// @brief Socket type
        using Socket = boost::asio::ip::tcp::socket;
        /// @brief Shared socket
        using SharedSocket = shared_ptr<Socket>;
        /// @brief Endpoint
        using Endpoint = boost::asio::ip::tcp::endpoint;
        /// @brief Protocol type
        using Protocol = boost::asio::ip::tcp;
        /// @brief v4 protocol
        static const Protocol Protocol4;
        /// @brief v6 protocol
        static const Protocol Protocol6;
        /// @brief Get protocol
        static Protocol protocol(bool v6)
        {
            return !v6 ? Protocol4 : Protocol6;
        }
    public:
        /// @brief Ctor
        TcpSocket();
        /// @brief Dtor
        ~TcpSocket();

        /** Reset all input/output.
         *  @param socket
         *    New TCP socket.
         */
        void reset(Socket& socket);
        /** Send simple message.
         *  @param messageCode
         *    Message code
         *  @param dataByte
         *    Message custom data byte.
         */
        void send(ubyte messageCode, ubyte dataByte);
        /** Send @a message.
         */
        void send(const NetworkMessage& message);
        /** Put @a message at the end of receiving stack.
         */
        void put(const NetworkMessage& message);

        /// @brief Get number of received messages
        uint received() const noexcept;
        /// @brief Get first message from stack
        NetworkMessage* receive() noexcept;
        /// @brief Remove first message from stack
        void pop() noexcept;

        /// @brief Flush output
        void flush();
        /** Update IO.
         *  Will process eNetMessage::Welcome.
         *  @return @c true on success, @c false otherwise.
         */
        bool update();

        /** @name Gets
         *  @{
         */
        /// @brief Check if no data is transmitted now
        bool empty() const noexcept;
        /// @brief Get local address
        Endpoint local() const noexcept;
        /// @brief Get remote address
        Endpoint remote() const noexcept;
        /// @}

        /** @name Debug
         *  @{
         */
        /// @brief Write to log debug message info
        void debugMessage(const NetworkMessage& message, const char* text);
        /// @}
    private:
        /// @brief Reset error
        bool handleError(boost::system::error_code error);
        /// @brief Read header handle
        void handleReadHeader(boost::system::error_code error);
        /// @brief Read message handle
        void handleReadAll(boost::system::error_code error);
        /// @brief Write message handler
        void handleWrite(boost::system::error_code error);
        /// @brief Reset IO
        void reset();
        /// @brief Update output
        void updateOutput();

        /// @brief Input pool
        deque<NetworkMessage> m_inputMsgPool;
        /// @brief Output portion
        struct Portion
        {
            /// @brief Ctor
            Portion() : dataSize(0)
            {
            }
            /// @brief Reset
            void reset()
            {
                dataSize = 0;
            }
            /// @brief Add data
            uint add(const NetworkMessage& message, uint offset);
            /// @brief Check if empty
            inline bool isEmpty() const
            {
                return dataSize == 0;
            }
            /// @brief Check if full
            inline bool isFull() const
            {
                return dataSize == NetworkTcpPortionSize;
            }
            /// @brief Size
            uint dataSize;
            /// @brief Buffer
            array<ubyte, NetworkTcpPortionSize> data;
        };
        /// @brief Output pool
        deque<Portion> m_outputMsgPool;
    private:
        Error m_error = Error::OK;
        Socket* m_socket = nullptr;
        boost::system::error_code m_internalCode;
        bool m_welcomeFlag = false;

        Portion m_sendingPortion;

        uint m_readingMessageSize = 0;
        NetworkMessage m_readingMessage;
    };
    /** Network server.
     *  You should set callback to receive clients disconnection.
     *  Clients disconnected during server destruction will not call callback.
     */
    class TcpServer
    : Noncopyable
    {
    public:
        /// @brief Number of stored pings
        static const ubyte StoredPings = 9;
        /// @brief Number of averaged pings
        static const ubyte AveragedPings = 3;
        /// @brief Connections acceptor
        typedef boost::asio::ip::tcp::acceptor Acceptor;
        /// @brief Add client callback
        function<bool(TcpSocket& socket, const string& ip, ushort port) > onAddClient;
        /// @brief Remove client callback
        function<void(TcpSocket& socket) > onRemoveClient;
    public:
        /** Ctor.
         *  @throw ListeningFailException
         *  @param port
         *    Port to listen, 0 - one of free ports
         *  @param protocol
         *    Protocol to use
         *  @param numMessages
         *    Number of incoming messages should be received before
         *    #onAddClient callback will be called
         */
        TcpServer(ushort port, TcpSocket::Protocol protocol, uint numMessages = 0);
        /// @brief Dtor
        ~TcpServer();

        /// @brief Flush output
        void flush();
        /// @brief Update connections. Never throws.
        void update();
        /// @brief Cancel connection (will be performed during #update)
        bool kick(TcpSocket& socket);

        /** @name Gets
         *  @{
         */
        /// @brief Get server port
        ushort port() const noexcept;
        /// @}
    private:
        /// @brief Handle client
        void handleClient(TcpSocket::SharedSocket socket, boost::system::error_code error);
        /// @brief Internal client struct
        struct Client
        {
            /// @brief Socket
            TcpSocket::SharedSocket socket;
            /// @brief Client
            shared_ptr<TcpSocket> client;
            /// @brief Address
            string address = "127.0.0.1";
            /// @brief Port
            ushort port = 0;
            /// @brief Compare
            bool operator <(const Client& another) const noexcept
            {
                return client.get() < another.client.get();
            }
        };
    private:
        TcpSocket::Protocol m_protocol;
        boost::asio::io_service m_service;
        Acceptor m_acceptor;
        uint m_numMessages;

        list<Client> m_newClients;
        list<Client> m_clients;
        set<TcpSocket*> m_kickedClients;
    };
    /** Network client.
     **/
    class TcpClient
    : Noncopyable
    {
    public:
        /// @brief Socket type
        typedef TcpSocket::Socket Socket;
        /// @brief State
        enum State
        {
            /// @brief Client is disconnected
            Disconnected,
            /// @brief Client is connecting now
            Connecting,
            /// @brief Connection is failed (e.g because of timeout)
            Failed,
            /// @brief Client is connected
            Connected
        };
    public:
        /** Ctor.
         *  @param address
         *    Host address
         *  @param port
         *    Host port
         */
        TcpClient(const string& address, ushort port);
        /// @brief Dtor
        ~TcpClient();
        /** Client update.
         *  Update client if connected, otherwise call will be ignored.
         *  Will process MessageCode::Welcome
         *  @return @c true on success, @c false if socket is disconnected or
         *          error occur. You should reconnect on error.
         */
        bool update();
        /** Flush socket output
         */
        void flush();

        /** Try to establish connection, non-blocking.
         *  @param timeout
         *    Connecting/disconnecting timeout
         */
        void connect(DeltaTime timeout = 0);
        /** Wait for connection, blocking.
         *  @return @c true if #Connected, @c false otherwise
         */
        bool wait();

        /** @name Gets
         *  @{
         */
        /// @brief Check if error occurs
        bool isFailed() const;
        /// @brief Check if client is connected
        bool isConnected() const;
        /// @brief Get socket
        TcpSocket& socket();
        /// @brief Get local port
        ushort localPort() const;
        /// @brief Get server port
        ushort remotePort() const;
        /// @brief Get address
        string remoteIP() const;
        /// @brief Get end point
        const TcpSocket::Endpoint& endpoint() const;
        /// @}
    private:
        State m_state;
        boost::asio::io_service m_service;
        boost::asio::deadline_timer m_timer;
        Socket m_pureSocket;
        TcpSocket m_socket;

        TcpSocket::Endpoint m_endpoint;
        DeltaTime m_timeout;
    };
    /// @}
}
