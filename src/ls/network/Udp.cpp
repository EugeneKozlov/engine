#include "pch.h"
#include "ls/network/Udp.h"

using namespace ls;

const UdpSocket::Protocol UdpSocket::Protocol4 = boost::asio::ip::udp::v4();
const UdpSocket::Protocol UdpSocket::Protocol6 = boost::asio::ip::udp::v6();
UdpSocket::UdpSocket(Protocol protocol)
    : m_protocol(protocol)
    , m_service()
    , m_socket(m_service)
//     , m_outputPool(MessagesPoolLimit)
{
    boost::system::error_code error;
    m_socket.open(m_protocol, error);

    // Add buffer
    m_inputMessages.emplace_back();
    m_inputEndpoints.emplace_back();
    // Fire
    auto buffer = boost::asio::buffer(m_inputMessages.back().data(), NetworkMessage::MaxSize);
    auto receiver = bind(&UdpSocket::receiver, this, _1, _2);
    m_socket.async_receive_from(buffer, m_inputEndpoints.back(), 0, receiver);
}
UdpSocket::UdpSocket(ushort port, Protocol protocol)
    : UdpSocket(protocol)
{
    assign(port);
}
UdpSocket::~UdpSocket()
{
}
void UdpSocket::assign(ushort port)
{
    // Rebind
    boost::system::error_code error;
    m_socket.bind(Endpoint(m_protocol, port), error);
    error || m_socket.set_option(boost::asio::ip::udp::socket::reuse_address(false), error);
    error || m_socket.set_option(boost::asio::socket_base::broadcast(false), error);
    LS_THROW(!error,
             ListeningFailException,
             "UDP socket listening failed - ", error.message());
}
void UdpSocket::sendTo(const NetworkMessage& message, const Endpoint& target)
{
//     // Wait for pool releasing
//     boost::system::error_code error;
//     while (m_outputPool.size() >= MessagesPoolLimit)
//         if (!m_service.run_one(error))
//             return;
// 
//     // Add to pool
//     uint messageId = m_outputPool.insert(message);
//     auto messageIter = m_outputPool.iterator(messageId);
// 
//     // Sending request
//     auto sender = [this, messageId](const boost::system::error_code& /*error*/, size_t /*sent*/)
//     {
//         m_outputPool.remove(messageId);
//     };
//     auto buffer = boost::asio::buffer(messageIter->constData(), messageIter->length());
//     m_socket.async_send_to(buffer, target, 0, sender);
}
bool UdpSocket::sendBroadcast(const NetworkMessage& message, ushort port)
{
    if (m_protocol != Protocol4)
        return false;
    using namespace boost::asio;

    boost::system::error_code error;
    m_socket.set_option(ip::udp::socket::reuse_address(true), error);
    m_socket.set_option(socket_base::broadcast(true), error);

    Endpoint endpoint = Endpoint(ip::address_v4::broadcast(), port);
    auto buffer = boost::asio::buffer(message.constData(), message.length());

    uint sent = (uint) m_socket.send_to(buffer, endpoint, 0, error);
    bool success = !error && sent == message.length();

    m_socket.set_option(ip::udp::socket::reuse_address(false), error);
    m_socket.set_option(socket_base::broadcast(false), error);
    return success;
}
bool UdpSocket::receiveFrom(NetworkMessage& message, UdpSocket::Endpoint& target)
{
    boost::system::error_code error;
    m_service.poll(error);
    while (m_inputMessages.size() > 1)
    {
        message = m_inputMessages.front();
        target = m_inputEndpoints.front();
        m_inputMessages.pop_front();
        m_inputEndpoints.pop_front();
        if (message.length() != 0)
            return true;
    }
    return false;
}
void UdpSocket::receiver(const boost::system::error_code& error, size_t received)
{
    auto& thisMessage = m_inputMessages.back();
    if (error || thisMessage.length() != received)
        thisMessage.length(0);
    thisMessage.resize();
    // Add next
    m_inputMessages.emplace_back();
    m_inputEndpoints.emplace_back();
    // Fire
    auto buffer = boost::asio::buffer(m_inputMessages.back().data(), NetworkMessage::MaxSize);
    auto receiver = bind(&UdpSocket::receiver, this, _1, _2);
    m_socket.async_receive_from(buffer, m_inputEndpoints.back(), 0, receiver);
}
ushort UdpSocket::port() const noexcept
{
    boost::system::error_code error;
    return m_socket.local_endpoint(error).port();
}

