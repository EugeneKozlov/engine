/** @file ls/network/common.h
 *  @brief Common network declarations
 */

#pragma once
#include "ls/common.h"

namespace ls
{
    /** @addtogroup LsNetwork
     *  @{
     */
    /// @brief Network version
    static const uint NetworkVersion = 2;
    /// @brief Default port
    static const ushort NetworkDefaultPort = 21001;
    /// @brief Network address type
    using NetworkAddress = boost::asio::ip::address;
    /// @brief UDP socket endpoint
    using UdpSocketEndpoint = boost::asio::ip::udp::endpoint;
    /// @brief Max TCP/UDP message size
    static const uint NetworkMessageMaxSize = 508;
    /// @brief TCP portion size
    static const uint NetworkTcpPortionSize = 508;

    /// @brief Network message ID type
    using MessageId = ubyte;
    /// @brief Invalid message ID
    static const MessageId InvalidMessageId = 0;
    /// @brief Network client unique ID
    using ClientId = ushort;
    /// @brief Reserved client IDs
    enum ReservedClientIds : ClientId
    {
        /// @brief Server client ID
        NetRootClient = 0,
        /// @brief Max client ID +1
        NetNumClients = 255,
        /// @brief Broadcast client ID
        NetBroadcastClient = NetNumClients,
        /// @brief Invalid client ID
        NetInvalidClient = ClientId(-1)
    };
    /** Convert IP from string to bytes
     *  @param string
     *    String IP
     *  @param [out] bytes
     *    Bytes array IP, not less than 16 bytes
     *  @return Number of written bytes
     */
    uint atoiIP(const char* string, ubyte bytes[]);
    /** Convert IP from string to bytes
     *  @param [out] bytes
     *    Bytes array IP
     *  @param size
     *    Bytes to read (4 or 16)
     *  @return String IP
     */
    string itoaIP(const ubyte bytes[], uint size);
    /// @}
}