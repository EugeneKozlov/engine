/** @file ls/network/NetworkMessage.h
 *  @brief Network byte stream
 */
#pragma once
#include "ls/common.h"
#include "ls/network/common.h"
#include "ls/io/StaticStream.h"

namespace ls
{
    /** @addtogroup LsNetwork
     *  @{
     */
    /** Network message stream.
     *  @tparam Size Max stream size
     */
    template <uint Size>
    class NetworkStream
    : public StaticStream<Size>
    {
    public:
        /// @brief Min network message size
        static const uint MinSize = 4;
        static_assert(Size >= MinSize, "too small message");
        static_assert(sizeof (MessageId) == 1, "bad message ID size");
        /// @brief Max message size
        static const uint MaxSize = Size;
    public:
        /// @brief Ctor
        NetworkStream() noexcept
        : StaticStream<Size>(MinSize)
        {
        }
        /// @brief Resize message after data updating
        inline void resize() noexcept
        {
            this->m_writePos = length();
        }

        /** @name Gets/sets
         *  @{
         */
        /// @brief Get message length
        inline uint length() const noexcept
        {
            return (uint) ntoh(*reinterpret_cast<const ushort*>(this->constData()));
        }
        /// @brief Set message @a length
        inline void length(uint length) noexcept
        {
            *reinterpret_cast<ushort*>(this->data()) = hton((ushort) length);
        }
        /// @brief Get message id
        inline MessageId id() const noexcept
        {
            return this->constData()[2];
        }
        /// @brief Set message @a id
        inline void id(MessageId id) noexcept
        {
            this->data()[2] = id;
        }
        /// @brief Get message tag
        inline ubyte tag() const noexcept
        {
            return this->constData()[3];
        }
        /// @brief Set message @a tag
        inline void tag(ubyte tag) noexcept
        {
            this->data()[3] = tag;
        }
        /// @}
    };
    /// @brief Network message stream
    using NetworkMessage = NetworkStream<NetworkMessageMaxSize>;
    /// @}
}