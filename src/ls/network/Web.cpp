#include "pch.h"
#include "ls/network/Web.h"
#include "ls/network/Tcp.h"
#include "ls/network/Udp.h"
#include "ls/network/NetworkMessage.h"
#include "ls/network/reflection.h"
#include "ls/misc/Mediator.h"

using namespace ls;
struct Web::Node
{
public:
    /// @brief Default
    Node() = default;
    /// @brief Init from server
    Node(const string& ip, ushort port, TcpSocket* socket, uint numPingSamples)
        : superAddress(ip)
        , acceptedAddress(ip)
        , acceptedPort(port)
        , acceptedConnection(socket)
        , averagePing(numPingSamples, 0)
    {
    }
    /// @brief Init from client
    Node(const string& ip, ushort portTcp, ushort portUdp, uint numPingSamples)
        : superAddress(ip)
        , superTcp(portTcp)
        , superUdp(portUdp)
        , superUdpEndpoint(NetworkAddress::from_string(superAddress), superUdp)
        , averagePing(numPingSamples, 0)
    {
    }
    /// @brief Test if super
    bool isSuper() const
    {
        return superTcp != 0 && superUdp != 0;
    }
    /// @brief Test if connected
    bool isConnected() const
    {
        return acceptedConnection || (remoteServer && remoteServer->isConnected());
    }
    /// @brief Debug log
    void logme() const
    {
        const char* name = !isSuper() ? "Node" : "Supernode";
        if (acceptedConnection)
        {
            ls::logDebug(name, " [", id, "] at "
                        "[", superAddress, "*", superTcp, "*", superUdp, "]"
                        " has incoming TCP connection at [", acceptedAddress, "*", acceptedPort, "]");
        }
        else if (remoteServer)
        {
            ls::logDebug(name, " [", id, "] at "
                        "[", superAddress, "*", superTcp, "*", superUdp, "]"
                        " remote server is connected");
        }
        else
        {
            ls::logDebug(name, " [", id, "] at "
                        "[", superAddress, "*", superTcp, "*", superUdp, "]");
        }
    }
    /// @brief Debug name
    const char* name() const
    {
        if (id == NetRootClient)
            return "Root";
        else if (isSuper())
            return "Supernode";
        else
            return "Node";
    }
public:
    ClientId id = NetInvalidClient;
    string rootIp = "N/A";

    string superAddress = "N/A";
    ushort superTcp = 0;
    ushort superUdp = 0;
    UdpSocketEndpoint superUdpEndpoint;

    shared_ptr<TcpClient> remoteServer;
    uint numFailed = 0;

    string acceptedAddress = "N/A";
    ushort acceptedPort = 0;
    TcpSocket* acceptedConnection = nullptr;
    UdpSocketEndpoint acceptedUdp;

    uint authCode = 0;
    struct Ping
    {
        ubyte code;
        DiscreteTime time;
    };
    deque<Ping> pingRequests;
    DeltaTime lastPing = 0;
    Mediator<DeltaTime> averagePing;
};


// Main
Web::Web(DeltaTime timeout,
         const string& ip, ushort port,
         Type nodeType,
         uint numSuperNodes, uint numPingSamples)
    : Nameable("Web")
    , numPingSamples(numPingSamples)
    , numSuperNodes(numSuperNodes)
    , m_type(nodeType)
    , m_timeout(timeout)
    , m_rootAddress(boost::asio::ip::address::from_string(ip))
    , m_rootPort(port)
{
    debug_assert(nodeType != Invalid);

    m_usedIds.fill(false);
    bool useIp6 = m_rootAddress.is_v6();

    // Start UDP
    m_udp = make_unique<UdpSocket>(isRoot() ? port : 0, UdpSocket::protocol(useIp6));

    // Start TCP
    try
    {
        switch (nodeType)
        {
        case Root:
            // Start
            m_server = make_unique<TcpServer>(m_rootPort, TcpSocket::protocol(useIp6), 1);
            // Log
            logInfo("Root server is started at "
                        "[", ip, "*", m_server->port(), "*", m_udp->port(), "]");
            break;
        case Super:
            // Start
            m_server = make_unique<TcpServer>(0, TcpSocket::protocol(useIp6), 1);
            // Log
            logInfo("Supernode server is started at "
                        "[", ip, "*", m_server->port(), "*", m_udp->port(), "]");
            break;
        default:
            logInfo("UDP socket is bound to "
                        "[", m_udp->port(), "]");
            break;
        }
    }
    catch (const ListeningFailException&)
    {
        logInfo("Port [", m_server->port(), "] listening failed");
        LS_THROW(m_type != Root,
                 ListeningFailException,
                 "TCP socket listening failed");
        // Reset
        m_type = Basic;
        m_server.reset();
    }
    // Init
    if (m_server)
    {
        m_server->onAddClient = bind(&Web::onAddClient, this, _1, _2, _3);
        m_server->onRemoveClient = bind(&Web::onRemoveClient, this, _1);
    }

    Node rootNode(ip, port, port, numPingSamples);
    rootNode.id = NetRootClient;
    m_usedIds[NetRootClient] = true;

    // Start server
    if (m_type == Root)
    {
        m_id = NetRootClient;
        rootNode.authCode = random();
        rootNode.rootIp = ip;
    }
    else
    {
        // Connect to server
        auto& server = rootNode.remoteServer;
        server = make_shared<TcpClient>(ip, port);
        server->connect(m_timeout);
        if (!server->wait())
        {
            logInfo("Cannot connect to server at [", ip, "*", port, "]");
            LS_THROW(0, ConnectionLostException,
                     "Tried to connect to [", ip, "*", port, "]");
        }

        ushort superTcp = isSuper() ? m_server->port() : 0;
        ushort superUdp = isSuper() ? m_udp->port() : 0;
        AuthMessage request{ip, superTcp, superUdp, nullptr};
        server->socket().send(send(request));
    }

    m_nodes.emplace(NetRootClient, std::move(rootNode));
}
Web::~Web()
{
    destroy();
}
ClientId Web::allocateId(bool super)
{
    ClientId id = super ? 0 : (ClientId) numSuperNodes;
    while (m_usedIds[id] && id < NetNumClients)
        ++id;
    if (id == NetNumClients)
    {
        return NetInvalidClient;
    }
    else
    {
        m_usedIds[id] = true;
        return id;
    }
}
void Web::freeId(ClientId id)
{
    m_usedIds[id] = false;
}
void Web::debugLogNodes() const
{
    logDebug("[", m_id, "] List - ", m_nodes.size(), " nodes");
    for (auto& iterNode : m_nodes)
        iterNode.second.logme();
}


// Register
bool Web::onAddClient(TcpSocket& socket, const string& ip, ushort port)
{
    if (isRoot())
        return serverRegisterNode(socket, ip, port);
    else
        return superAcceptNode(socket, ip, port);
}
bool Web::serverRegisterNode(TcpSocket& socket, const string& ip, ushort port)
{
    // Check message
    NetworkMessage& firstMessage = *socket.receive();
    if (firstMessage.id() != MessageCode::Auth)
    {
        logInfo("Connection from [", ip, "*", port, "] is denied : "
                    "first message is #", firstMessage.id());
        return false;
    }

    Node newNode(ip, port, &socket, numPingSamples);

    // Test super
    AuthMessage request = receive<AuthMessage>(firstMessage);
    socket.pop();
    ushort superTcp = *request.h.supernodeTcp;
    ushort superUdp = *request.h.supernodeUdp;
    auto buffer = request.h.buffer->data();
    bool canSuper = request.isSuper();
    bool allowSuper = canSuper;
    if (allowSuper && onTestSuper && !onTestSuper(buffer, ip, superTcp))
        allowSuper = false;

    // Get ID
    ClientId nodeId = allocateId(allowSuper);
    if (nodeId >= numSuperNodes)
        allowSuper = false;

    if (nodeId == NetInvalidClient)
    {
        logInfo("Connection from [", ip, "*", port, "] is denied : "
                    "ID map overflow, ", m_nodes.size(), " nodes are already exist");
        return false;
    }
    newNode.id = nodeId;
    newNode.rootIp = request.ip();

    // Init super
    if (allowSuper)
    {
        newNode.superAddress = ip;
        newNode.superTcp = superTcp;
        newNode.superUdp = superUdp;
        newNode.superUdpEndpoint = UdpSocketEndpoint(NetworkAddress::from_string(ip), superUdp);
        logInfo("Connection from [", ip, "*", port, "] is accepted : "
                    "ID is ", nodeId, ", "
                    "super ports are [", superTcp, "*", superUdp, "]");
    }
    else
    {
        superTcp = superUdp = 0;
        logInfo("Connection from [", ip, "*", port, "] is accepted : "
                    "ID is ", nodeId);
    }

    newNode.authCode = random();
    RegisterMessage report(nodeId, newNode.authCode, ip, superTcp, superUdp);

    socket.send(send(report.copy(true, true)));
    const string& rootIp = m_nodes[NetRootClient].superAddress;
    for (auto& iterNode : m_nodes)
    {
        Node& node = iterNode.second;

        // Inform newbie
        string realIp = node.superAddress == rootIp ? newNode.rootIp : node.superAddress;
        RegisterMessage oldClient{node.id, allowSuper ? node.authCode : 0,
            realIp, node.superTcp, node.superUdp};
        socket.send(send(oldClient));

        // Inform oldbie
        if (node.id == NetRootClient)
            continue;
        TcpSocket& client = *node.acceptedConnection;
        client.send(send(report.copy(node.isSuper(), false)));
    }

    m_superUdpMap[newNode.superUdpEndpoint] = nodeId;
    m_tcpMap[&socket] = nodeId;
    m_nodes.emplace(nodeId, std::move(newNode));
    if (onAddNode)
        onAddNode(nodeId);
    return true;
}
bool Web::superAcceptNode(TcpSocket& socket, const string& ip, ushort port)
{
    // Check message
    NetworkMessage& firstMessage = *socket.receive();
    if (firstMessage.id() != MessageCode::Establish)
    {
        logInfo("Connection from [", ip, "*", port, "] is denied : "
                    "first message is #", firstMessage.id());
        return false;
    }

    EstablishMessage request = receive<EstablishMessage>(firstMessage);
    socket.pop();
    ClientId nodeId = *request.h.localId;
    ClientId thisId = *request.h.remoteId;
    uint authCode = *request.h.secretCode;

    if (thisId != m_id)
    {
        logInfo("Connection from [", ip, "*", port, "] is denied : "
                    "invalid this node ID [", thisId, "]");
        return false;
    }

    auto iterNode = m_nodes.find(nodeId);
    if (iterNode == m_nodes.end())
    {
        logInfo("Connection from [", ip, "*", port, "] is denied : "
                    "unknown node [", nodeId, "]");
        return false;
    }

    if (iterNode->second.authCode != authCode)
    {
        logInfo("Connection from [", ip, "*", port, "] is denied : "
                    "invalid auth code for node [", nodeId, "]");
        return false;
    }

    Node& node = iterNode->second;

    if (node.acceptedConnection)
    {
        m_tcpMap.erase(node.acceptedConnection);
        m_server->kick(*node.acceptedConnection);
        logInfo("Connection for node [", nodeId, "] already exist : "
                    "closing");
    }

    node.acceptedAddress = ip;
    node.acceptedPort = port;
    node.acceptedConnection = &socket;
    m_tcpMap[&socket] = nodeId;

    logInfo("Connection from [", ip, "*", port, "] is accepted : "
                "node is [", nodeId, "]");

    return true;
}
void Web::onRemoveClient(TcpSocket& socket)
{
    auto iterNodeId = m_tcpMap.find(&socket);
    if (iterNodeId == m_tcpMap.end())
    {
        logInfo("Try to remove unknown connection");
        return;
    }

    auto nodeId = iterNodeId->second;
    auto iterNode = m_nodes.find(nodeId);
    if (iterNode == m_nodes.end())
    {
        logWarning("Try to remove invalid connection : "
                       "ID is [", nodeId, "]");
        return;
    }

    if (isRoot())
        serverDeregisterNode(iterNode->second);
    else
        superRemoveNode(iterNode->second);
}
void Web::serverDeregisterNode(Node& node)
{
    ClientId nodeId = node.id;
    if (onRemoveNode)
        onRemoveNode(nodeId);
    m_tcpMap.erase(node.acceptedConnection);
    m_udpMap.erase(node.acceptedUdp);
    m_superUdpMap.erase(node.superUdpEndpoint);
    m_nodes.erase(nodeId);

    // Inform
    for (auto& iterNode : m_nodes)
    {
        if (iterNode.first == NetRootClient)
            continue;

        TcpSocket& socket = *iterNode.second.acceptedConnection;
        socket.send(send(DeregisterMessage{nodeId, 0}));
    }

    logInfo("Node [", nodeId, "] is removed");
}
void Web::superRemoveNode(Node& node)
{
    logInfo("TCP connection with node [", node.id, "] is lost");

    m_tcpMap.erase(node.acceptedConnection);
    node.acceptedAddress = "N/A";
    node.acceptedPort = 0;
    node.acceptedConnection = nullptr;
}


// Update
bool Web::update()
{
    if (!isValid())
        return false;

    if (isRoot() && !m_rootReg && onAddNode)
    {
        onAddNode(NetRootClient);
        m_rootReg = true;
    }

    if (!updateSupport())
        return destroy();

    if (!updateRoot())
        return destroy();

    for (auto& iterNode : m_nodes)
        if (!updateNode(iterNode.second))
            return destroy();

    if (!updateUdp())
        return destroy();

    if (!updateSelf())
        return destroy();

    return true;
}
bool Web::updateSupport()
{
    if (!m_server)
        return true;

    m_server->update();
    return true;
}
bool Web::updateRoot()
{
    if (isRoot())
        return true;

    Node& root = m_nodes[NetRootClient];
    TcpSocket& socket = root.remoteServer->socket();
    if (!root.remoteServer->update())
    {
        logInfo("Root connection is lost");
        return destroy();
    }
    while (NetworkMessage* message = socket.receive())
    {
        m_inputTcp += message->length();
        switch (message->id())
        {
        case MessageCode::Auth:
            logWarning("Invalid [Auth] message from root");
            return destroy();
            break;
        case MessageCode::Register:
            if (!processRegister(receive<RegisterMessage>(*message)))
                return destroy();
            break;
        case MessageCode::Deregister:
            if (!processDeregister(receive<DeregisterMessage>(*message)))
                return destroy();
            break;
        default:
            if (onReceiveTcp)
                onReceiveTcp(NetRootClient, *message);
            break;
        }
        socket.pop();
    }
    return true;
}
bool Web::processRegister(const RegisterMessage& reg)
{
    ClientId nodeId = *reg.h.clientId;
    if (reg.isSelf())
    {
        if (m_id != NetInvalidClient)
        {
            logWarning("Invalid [Register] message from root : "
                           "duplicate self");
            return false;
        }
        m_id = nodeId;

        // Support accepted
        if (m_server && reg.isSuper())
        {
            logInfo("This node is registered as [", nodeId, "] : "
                        "support is accepted");
        }
            // Support denied
        else if (m_server && !reg.isSuper())
        {
            m_type = Basic;
            m_server.reset();
            logInfo("This node is registered as [", nodeId, "] : "
                        "support is denied");
        }
            // No support
        else
        {
            logInfo("This node is registered as [", nodeId, "]");
        }

        Node& node = m_nodes.emplace(nodeId,
                                     Node(reg.ip(),
                                          *reg.h.supernodeTcp,
                                          *reg.h.supernodeUdp,
                                          numPingSamples))
            .first->second;
        m_authCode = node.authCode = *reg.h.authCode;
        node.id = nodeId;
    }
    else
    {
        Node& newNode = (nodeId == NetRootClient)
            ? m_nodes[NetRootClient]
            : m_nodes.emplace(nodeId,
                              Node(reg.ip(),
                                   *reg.h.supernodeTcp, *reg.h.supernodeUdp,
                                   numPingSamples))
            .first->second;
        newNode.authCode = *reg.h.authCode;
        newNode.id = nodeId;

        // Connect to super
        const char* tag = "";
        if (newNode.isSuper() && nodeId != NetRootClient && nodeId < m_id)
        {
            newNode.remoteServer = make_shared<TcpClient>(newNode.superAddress, newNode.superTcp);
            tag = ", connecting to support server...";
        }

        // Set UDP endpoint
        m_superUdpMap[newNode.superUdpEndpoint] = nodeId;

        logInfo("Node is registered as [", nodeId, "]", tag);
    }

    if (onAddNode)
        onAddNode(nodeId);
    return true;
}
bool Web::processDeregister(const DeregisterMessage& dereg)
{
    ClientId nodeId = *dereg.h.clientId;
    auto iterNode = m_nodes.find(nodeId);
    if (iterNode == m_nodes.end())
    {
        logWarning("Try to deregister unknown node [", nodeId, "]");
        return false;
    }

    if (onRemoveNode)
        onRemoveNode(nodeId);

    Node& node = iterNode->second;
    m_tcpMap.erase(node.acceptedConnection);
    m_udpMap.erase(node.acceptedUdp);
    m_superUdpMap.erase(node.superUdpEndpoint);
    m_nodes.erase(iterNode);
    return true;
}
bool Web::updateNode(Node& node)
{
    // Update as server
    if (node.acceptedConnection && onReceiveTcp)
    {
        TcpSocket& socket = *node.acceptedConnection;
        while (NetworkMessage* message = socket.receive())
        {
            onReceiveTcp(node.id, *message);
            m_inputTcp += message->length();
            socket.pop();
        }
    }

    // Update as client
    if (node.id == NetRootClient)
        return true;
    if (!node.remoteServer)
        return true;

    if (!node.remoteServer->update())
    {
        if (node.numFailed != 0)
        {
            logInfo("Cannot connect to supernode [", node.id, "] "
                        "at [", node.superAddress, "*", node.superTcp, "] : "
                        "reconnecting...");
        }

        ++node.numFailed;
        node.remoteServer->connect(m_timeout);
        node.remoteServer->socket().send(send(EstablishMessage(m_authCode,
                                                               m_id,
                                                               node.id)));
    }
    else if (node.remoteServer->isConnected())
    {
        // Reset
        node.numFailed = 1;

        // Receive
        TcpSocket& socket = node.remoteServer->socket();

        if (onReceiveTcp)
        {
            while (NetworkMessage* message = socket.receive())
            {
                m_inputTcp += message->length();
                onReceiveTcp(node.id, *message);
                socket.pop();
            }
        }
    }

    return true;
}


// Super and UDP stuff
bool Web::updateUdp()
{
    Node* node = nullptr;
    NetworkMessage message;
    UdpSocketEndpoint endpoint;
    while (m_udp->receiveFrom(message, endpoint))
    {
        ++m_inputCount;
        m_inputUdp += message.length();
        switch (message.id())
        {
        case MessageCode::Establish:
            if (!processEstablish(receive<EstablishMessage>(message), endpoint))
                return false;
            break;
        default:
            node = findNode(findIdByUdp(endpoint));
            if (node)
                if (!processUdp(message, *node))
                    return false;
            break;
        }
    }
    return true;
}
bool Web::processEstablish(const EstablishMessage& est, const UdpSocketEndpoint& ep)
{
    string ip = ep.address().to_string();
    ushort port = ep.port();
    ClientId nodeId = *est.h.localId;
    ClientId thisId = *est.h.remoteId;
    uint authCode = *est.h.secretCode;

    if (thisId != m_id)
    {
        logInfo("UDP connection from [", ip, "*", port, "] is denied : "
                    "invalid this node ID [", thisId, "]");
        return false;
    }

    auto iterNode = m_nodes.find(nodeId);
    if (iterNode == m_nodes.end())
    {
        logInfo("UDP connection from [", ip, "*", port, "] is denied : "
                    "unknown node [", nodeId, "]");
        return false;
    }

    Node& node = iterNode->second;
    if (node.authCode != authCode)
    {
        logInfo("UDP connection from [", ip, "*", port, "] is denied : "
                    "invalid auth code for node [", nodeId, "]");
        return false;
    }

    m_udpMap.erase(node.acceptedUdp);
    node.acceptedUdp = ep;
    m_udpMap[ep] = nodeId;

    logInfo("UDP connection for node [", nodeId, "] "
                "is established at [", ip, "*", port, "]");
    return true;
}
bool Web::processUdp(const NetworkMessage& message, Node& node)
{
    switch (message.id())
    {
    case MessageCode::Ping:
        if (!processPing(message))
            return false;
        break;
    case MessageCode::PingRe:
        if (!processPingResponce(message, node))
            return false;
        break;
    default:
        if (!processOtherUdp(message, node))
            return false;
        break;
    }
    return true;
}
bool Web::processPing(const NetworkMessage& ping)
{
    NetworkMessage pingRe = ping;
    pingRe.id(MessageCode::PingRe);
    sendUdpMessage(m_nodes[NetRootClient], pingRe);
    return true;
}
bool Web::processPingResponce(const NetworkMessage& pingRe, Node& node)
{
    ubyte code = pingRe.tag();
    auto& pings = node.pingRequests;

    // Remove missed
    while (!pings.empty() && pings.front().code != code)
        pings.pop_front();

    // Fail
    if (pings.empty())
    {
        logTrace("Node [", node.id, "] sent unknown ping response");
        return true;
    }

    // Calc ping
    DiscreteTime prevTime = pings.front().time;
    node.lastPing = max<DeltaTime>(1, relativeTime() - prevTime);
    node.averagePing.add(node.lastPing);
    pings.pop_front();
    return true;
}
bool Web::processOtherUdp(const NetworkMessage& message, Node& node)
{
    if (onReceiveUdp)
        onReceiveUdp(node.id, message);
    return true;
}
bool Web::updateSelf()
{
    if (m_id == NetInvalidClient)
        return true;

    // TCP
    if (onReceiveTcp)
    {
        while (!m_tcpMessages.empty())
        {
            onReceiveTcp(m_id, m_tcpMessages.front());
            m_tcpMessages.pop_front();
        }
    }
    else
    {
        m_tcpMessages.clear();
    }

    // UDP
    Node& thisNode = getNode(m_id);
    while (!m_udpMessages.empty())
    {
        processUdp(m_udpMessages.front(), thisNode);
        m_udpMessages.pop_front();
    }

    return true;
}


// Main
void Web::flush()
{
    if (m_server)
    {
        m_server->flush();
    }
    for (auto& iterNode : m_nodes)
    {
        auto server = iterNode.second.remoteServer;
        if (server)
            server->flush();
    }
}
void Web::reestablishUdp()
{
    if (isSuper())
        return;

    for (auto& iterNode : m_nodes)
    {
        Node& node = iterNode.second;
        if (!node.isSuper())
            continue;
        NetworkMessage message = send(EstablishMessage(m_authCode, m_id, node.id));
        m_udp->sendTo(message, node.superUdpEndpoint);
    }
}
void Web::pingAll()
{
    if (!isRoot())
        return;

    NetworkMessage message;
    message.id(MessageCode::Ping);
    message.length(4);

    for (auto& iNode : m_nodes)
    {
        Node& node = iNode.second;

        ubyte pingCode = (ubyte) random<ubyte>(1, 255);
        message.tag(pingCode);
        sendUdpMessage(node, message);

        Node::Ping ping = {pingCode, relativeTime()};
        node.pingRequests.push_back(ping);
    }
}
bool Web::kick(ClientId node)
{
    debug_assert(isRoot());

    Node& ref = getNode(node);
    return m_server->kick(*ref.acceptedConnection);
}
bool Web::destroy()
{
    if (!m_valid)
        return false;

    m_valid = false;
    m_server.reset();
    m_udp.reset();
    m_nodes.clear();

    logInfo("Destroyed");
    return false;
};


// Nodes
Web::Node* Web::findNode(ClientId id)
{
    return findOrDefault(m_nodes, id);
}
const Web::Node* Web::findNode(ClientId id) const
{
    return findOrDefault(m_nodes, id);
}
ClientId Web::findIdByUdp(const UdpSocketEndpoint& ep)
{
    auto iterClient = m_udpMap.find(ep);
    auto iterServer = m_superUdpMap.find(ep);
    if (iterServer != m_superUdpMap.end())
        return iterServer->second;
    else if (iterClient != m_udpMap.end())
        return iterClient->second;
    else
        return NetInvalidClient;
}
const Web::Node& Web::getNode(ClientId id) const
{
    return getOrFail(m_nodes, id);
}
Web::Node& Web::getNode(ClientId id)
{
    return getOrFail(m_nodes, id);
}


// Messages
bool Web::sendTcp(ClientId node, const NetworkMessage& message)
{
    if (node == NetBroadcastClient)
    {
        for (auto& iterNode : m_nodes)
            sendTcpMessage(iterNode.second, message);
        return true;
    }
    else
    {
        Node* nodePtr = findNode(node);
        if (!nodePtr)
            return false;

        sendTcpMessage(*nodePtr, message);
        return true;
    }
}
bool Web::sendTcpMessage(Node& node, const NetworkMessage& message)
{
    TcpSocket* clientSocket = node.acceptedConnection;
    TcpSocket* serverSocket = node.remoteServer ? &node.remoteServer->socket() : nullptr;

    if (node.id < m_id && serverSocket)
    {
        m_outputTcp += message.length();
        serverSocket->send(message);
    }
    else if (node.id > m_id && clientSocket)
    {
        m_outputTcp += message.length();
        clientSocket->send(message);
    }
    else if (node.id == m_id)
    {
        m_tcpMessages.push_back(message);
    }
    else
    {
        return false;
    }

    return true;
}
bool Web::sendUdp(ClientId node, const NetworkMessage& message)
{
    if (node == NetBroadcastClient)
    {
        for (auto& iterNode : m_nodes)
            sendUdpMessage(iterNode.second, message);
        return true;
    }
    else
    {
        Node* nodePtr = findNode(node);
        if (!nodePtr)
            return false;

        sendUdpMessage(*nodePtr, message);
        return true;
    }
}
bool Web::sendUdpMessage(Node& node, const NetworkMessage& message)
{
    UdpSocketEndpoint& serverEndpoint = node.superUdpEndpoint;
    UdpSocketEndpoint& clientEndpoint = node.acceptedUdp;

    if (node.id == m_id)
    {
        m_udpMessages.push_back(message);
    }
    else if (node.isSuper())
    {
        m_udp->sendTo(message, serverEndpoint);
        ++m_outputCount;
        m_outputUdp += message.length();
    }
    else if (!node.isSuper() && clientEndpoint != UdpSocketEndpoint())
    {
        m_udp->sendTo(message, clientEndpoint);
        ++m_outputCount;
        m_outputUdp += message.length();
    }
    else
    {
        return false;
    }

    return true;
}
bool Web::isSuper(ClientId node) const
{
    const Node* nodePtr = findNode(node);
    if (nodePtr)
        return nodePtr->isSuper();
    else
        return false;
}
bool Web::isConnected(ClientId node) const
{
    return getNode(node).isConnected();
}
DeltaTime Web::ping(ClientId node) const
{
    return getNode(node).averagePing.median();
}
const char* Web::debugNodeName(ClientId node) const
{
    return getNode(node).name();
}
