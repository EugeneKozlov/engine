/** @file ls/network/message.h
 *  @brief System network message IDs
 */
#pragma once
#include "ls/common.h"

namespace ls
{
    /** @addtogroup LsNetwork
     *  @{
     */
    /** Network messages.
     *
     *  General bytes of any message is:
     *  | Bytes | Type      | Description               |
     *  |:------|:----------|:--------------------------|
     *  | 2     | u16       | Message size              |
     *  | 1     | u8        | Message ID                |
     *  | 1     | u8        | Custom byte               |
     */
    namespace MessageCode
    {
        /// @brief Internal
        enum EnumSystem : MessageId
        {
            /** Welcome message.
             *
             *  Should be first message received (or sent) by anyone.
             *
             *  @b Header:
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 4     | u16u8u8   |                           |
             *  | 64    | u8[64]    | Welcome string            |
             *  | 4     | u32       | Network version           |
             */
            Welcome = 1,
            /** Ping message.
             *
             *  @b Header:
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 4     | u16u8u8   | Secret byte               |
             */
            Ping,
            /** Ping response.
             *
             *  You should reply the secret byte from received ping message.
             *
             *  @b Header:
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 4     | u16u8u8   | Secret byte from #Ping    |
             */
            PingRe,
            /** Authenticate client.
             *
             *  @b Header:
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 4     | u16u8u8   |                           |
             *  | 1     | u8        | Flags                     |
             *  | 16    | u8[16]    | Server IP                 |
             *  | 2     | u16       | Super-node TCP port       |
             *  | 2     | u16       | Super-node UDP port       |
             *  | 128   | u8[128]   | Buffer info               |
             */
            Auth,
            /** Register client.
             *
             *  @b Header:
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 4     | u16u8u8   |                           |
             *  | 2     | ClientId  | Local client ID           |
             *  | 1     | u8        | Flags                     |
             *  | 4     | u32       | Auth code                 |
             *  | 16    | u8[16]    | IP                        |
             *  | 2     | u16       | Super-node port           |
             *  | 128   | u8[128]   | Buffer info               |
             */
            Register,
            /** Deregister client.
             *
             *  @b Header:
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 4     | u16u8u8   |                           |
             *  | 2     | ClientId  | Local client ID           |
             *  | 1     | u8        | Flags                     |
             */
            Deregister,
            /** Establish connection.
             *
             *  - Send this message one time to establish TCP connection
             *
             *  - Send this message one time per several seconds to keep UDP connection.
             *
             *  @b Header:
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 4     | u16u8u8   |                           |
             *  | 2     | ClientId  | Local client ID           |
             *  | 2     | ClientId  | Remote client ID          |
             *  | 4     | u32       | Auth code                 |
             */
            Establish,

            /// @brief Counter
            NumCommonMessage,
            /// @brief Max id
            SystemLimit = 255
        };
    };
    /// @}
}