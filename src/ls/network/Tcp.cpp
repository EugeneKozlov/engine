#include "pch.h"
#include "ls/network/Tcp.h"
#include "ls/network/reflection.h"
#include <boost/asio/read.hpp>
#include <boost/asio/write.hpp>

using namespace ls;

const TcpSocket::Protocol TcpSocket::Protocol4 = boost::asio::ip::tcp::v4();
const TcpSocket::Protocol TcpSocket::Protocol6 = boost::asio::ip::tcp::v6();
bool TcpSocket::LogMessages = false;
// Socket
uint TcpSocket::Portion::add(const NetworkMessage& message, uint offset)
{
    uint remaning = message.length() - offset;
    uint freeSpace = NetworkTcpPortionSize - dataSize;
    uint written = std::min(remaning, freeSpace);
    const ubyte* begin = message.constData() + offset;
    std::copy(begin, begin + written, data.begin() + dataSize);
    dataSize += written;
    return written;
}
TcpSocket::TcpSocket()
{
    reset();

    NetworkMessage welcome = ls::send(WelcomeMessage{});
    send(welcome);
}
TcpSocket::~TcpSocket()
{
}
bool TcpSocket::handleError(boost::system::error_code error)
{
    using namespace boost::system;
    if (!error)
        return true;
    switch (error.value())
    {
    case boost::asio::error::operation_aborted:
        return false;
    case errc::stream_timeout:
    case errc::connection_aborted:
    case errc::connection_refused:
    case errc::connection_reset:
        m_error = Error::ConnectionLost;
        break;
    default:
        m_error = Error::NetworkFail;
    }
    return false;
}
void TcpSocket::reset(Socket& socket)
{
    using namespace boost::asio;
    reset();
    m_socket = &socket;
    //m_socket->set_option(boost::asio::ip::tcp::no_delay(true));
    async_read(*m_socket, buffer(m_readingMessage.data(), 2),
               bind(&TcpSocket::handleReadHeader, this, _1));
}
void TcpSocket::handleReadHeader(boost::system::error_code error)
{
    string m = error.message();
    // Break on error
    if (!handleError(error))
        return;
    // Read header
    using namespace boost::asio;
    m_readingMessageSize = m_readingMessage.length();
    if (m_readingMessageSize < 4 || m_readingMessageSize > NetworkMessageMaxSize)
    {
        m_error = Error::Io;
        return;
    }
    // Fire reading body
    async_read(*m_socket, buffer(m_readingMessage.data() + 2, m_readingMessageSize - 2),
               bind(&TcpSocket::handleReadAll, this, _1));
}
void TcpSocket::handleReadAll(boost::system::error_code error)
{
    // Break on error
    if (!handleError(error))
        return;
    // Read body
    using namespace boost::asio;
    m_readingMessage.resize();
    debugMessage(m_readingMessage, "received");
    m_inputMsgPool.push_back(m_readingMessage);
    m_readingMessageSize = 0;
    // Fire reading header
    async_read(*m_socket, buffer(m_readingMessage.data(), 2),
               bind(&TcpSocket::handleReadHeader, this, _1));
}
void TcpSocket::handleWrite(boost::system::error_code error)
{
    // Break on error
    if (!handleError(error))
        return;
    // Push next
    using namespace boost::asio;
    m_outputMsgPool.pop_front();
    if (!m_outputMsgPool.empty())
    {
        auto& firstPortion = m_outputMsgPool.front();
        async_write(*m_socket, buffer(firstPortion.data.data(), firstPortion.dataSize),
                    bind(&TcpSocket::handleWrite, this, _1));
    }
}
void TcpSocket::reset()
{
    m_inputMsgPool.clear();
    m_readingMessage = NetworkMessage{};
}
void TcpSocket::flush()
{
    using namespace boost::asio;
    // Fire
    if (!m_sendingPortion.isEmpty())
    {
        bool wasEmpty = m_outputMsgPool.empty();
        m_outputMsgPool.push_back(m_sendingPortion);
        m_sendingPortion.reset();
        if (wasEmpty)
        {
            auto& firstPortion = m_outputMsgPool.front();
            async_write(*m_socket,
                        buffer(firstPortion.data.data(), firstPortion.dataSize),
                        bind(&TcpSocket::handleWrite, this, _1));
        }
    }
}
bool TcpSocket::update()
{
    if (!m_socket)
        return true;
    if (m_error != Error::OK)
        return false;
    using namespace boost::asio;
    using namespace boost::system;
    // Check welcome
    if (!m_welcomeFlag)
    {
        if (NetworkMessage * message = receive())
        {
            if (message->id() != MessageCode::Welcome)
            {
                m_error = Error::Io;
                return false;
            }
            m_welcomeFlag = true;
            pop();
        }
    }
    return true;
}
void TcpSocket::send(ubyte messageCode, ubyte dataByte)
{
    NetworkMessage message;
    message.length(4);
    message.id(messageCode);
    message.tag(dataByte);
    send(message);
}
void TcpSocket::send(const NetworkMessage& message)
{
    using namespace boost::asio;
    // Put msg into portion
    bool wasEmpty = m_outputMsgPool.empty();
    uint offset = 0;
    uint length = message.length();
    while (offset < length)
    {
        offset += m_sendingPortion.add(message, offset);
        if (m_sendingPortion.isFull())
        {
            m_outputMsgPool.push_back(m_sendingPortion);
            m_sendingPortion.reset();
        }
    }
    // Fire
    if (wasEmpty && !m_outputMsgPool.empty())
    {
        auto& firstPortion = m_outputMsgPool.front();
        async_write(*m_socket,
                    buffer(firstPortion.data.data(), firstPortion.dataSize),
                    bind(&TcpSocket::handleWrite, this, _1));
    }

}
void TcpSocket::put(const NetworkMessage& message)
{
    debugMessage(m_inputMsgPool.front(), "'received'");
    m_inputMsgPool.push_back(message);
}
uint TcpSocket::received() const noexcept
{
    return m_inputMsgPool.size();
}
NetworkMessage* TcpSocket::receive() noexcept
{
    return m_inputMsgPool.empty() ? nullptr : &m_inputMsgPool.front();
}
void TcpSocket::pop() noexcept
{
    debug_assert(!m_inputMsgPool.empty());

    debugMessage(m_inputMsgPool.front(), "removed");
    m_inputMsgPool.pop_front();
}
bool TcpSocket::empty() const noexcept
{
    return m_sendingPortion.isEmpty() && !m_readingMessageSize;
}
TcpSocket::Endpoint TcpSocket::local() const noexcept
{
    boost::system::error_code error;
    return m_socket ? m_socket->local_endpoint(error) : Endpoint();
}
TcpSocket::Endpoint TcpSocket::remote() const noexcept
{
    boost::system::error_code error;
    return m_socket ? m_socket->remote_endpoint(error) : Endpoint();
}
void TcpSocket::debugMessage(const NetworkMessage& message, const char* text)
{
    return;
    uint length = message.length();
    ubyte id = message.id();
    ubyte custom = message.tag();
    uint data = ntoh(*reinterpret_cast<const uint*> (message.constData() + 4));
    if (length >= 8)
        logDebug("Message [", length, ":", id, ":", custom, ":", Hex(), data, "]: ", text);
    else
        logDebug("Message [", length, ":", id, ":", custom, "]: ", text);
}


// Server
void TcpServer::handleClient(TcpSocket::SharedSocket socket,
                             boost::system::error_code error)
{
    boost::system::error_code ignoredError;
    // Params
    string address = socket->remote_endpoint(ignoredError).address()
        .to_string(ignoredError);
    ushort port = socket->remote_endpoint(ignoredError).port();
    // Fail
    if (error)
        logInfo("TcpServer: Connection failed - ", error.message());
    else
    {
        Client newClient;
        newClient.address = address;
        newClient.port = port;
        newClient.socket = socket;
        newClient.client = make_shared<TcpSocket>();
        newClient.client->reset(*socket);

        m_newClients.push_back(newClient);
    }
    // New handle
    TcpSocket::SharedSocket newSocket(new TcpSocket::Socket(m_service));
    m_acceptor.async_accept(*newSocket, bind(&TcpServer::handleClient,
                                             this, newSocket, _1));
}
TcpServer::TcpServer(ushort port, TcpSocket::Protocol protocol, uint numMessages)
    : m_protocol(protocol)
    , m_service()
    , m_acceptor(m_service)
    , m_numMessages(numMessages)
{
    boost::system::error_code error;
    if (!error)
        m_acceptor.open(m_protocol, error);
    if (!error)
        m_acceptor.bind(TcpSocket::Endpoint(m_protocol, port), error);
    if (!error)
        m_acceptor.listen(64, error);
    LS_THROW(!error,
             ListeningFailException,
             error.message());
    // Fire accepting
    TcpSocket::SharedSocket newSocket(new TcpSocket::Socket(m_service));
    m_acceptor.async_accept(*newSocket, bind(&TcpServer::handleClient, this, newSocket, _1));
}
TcpServer::~TcpServer()
{
}
void TcpServer::update()
{
    // Handle connections
    boost::system::error_code error;
    m_service.poll(error);

    // Process all
    auto clientsRemover = [this](const Client& constClient)
    {
        Client& client = const_cast<Client&> (constClient);
        TcpSocket& socket = *client.client;
        TcpSocket * const& socketPointer = &socket;

        // Should be kicked
        auto iterKicked = m_kickedClients.find(socketPointer);
        bool needKick = iterKicked != m_kickedClients.end();
        if (needKick)
        {
            m_kickedClients.erase(iterKicked);
            onRemoveClient(socket);
            return true;
        }

        // Bad client
        if (!socket.update())
        {
            onRemoveClient(socket);
            return true;
        }
        return false;
    };
    m_clients.remove_if(clientsRemover);
    m_kickedClients.clear();

    // Process news
    auto newClientsRemover = [this](const Client& constClient)
    {
        auto& newClient = const_cast<Client&> (constClient);
        auto& socket = *newClient.client;
        // Bad client
        if (!socket.update())
            return true;
        // Not enough messages
        auto numMessages = socket.received();
        if (numMessages < m_numMessages)
            return false;
        // Can add
        bool canAdd = onAddClient(socket, newClient.address, newClient.port);
        if (canAdd)
            m_clients.push_back(newClient);
        return true;
    };
    m_newClients.remove_if(newClientsRemover);
}
bool TcpServer::kick(TcpSocket& socket)
{
    auto kicker = [&socket](Client & client)
    {
        return client.client.get() == &socket;
    };
    auto iterClient = std::find_if(m_clients.begin(), m_clients.end(), kicker);
    if (iterClient == m_clients.end())
        return false;
    // Kick
    m_kickedClients.insert(&socket);
    return true;
}
ushort TcpServer::port() const noexcept
{
    boost::system::error_code error;
    return m_acceptor.local_endpoint(error).port();
}
void TcpServer::flush()
{
    for (auto& iterClient : m_clients)
        iterClient.client->flush();
}


// Client
TcpClient::TcpClient(const string& address, ushort port)
    : m_state(Disconnected)
    , m_service()
    , m_timer(m_service)
    , m_pureSocket(m_service)
    , m_socket()
    , m_endpoint(NetworkAddress::from_string(address), port)
    , m_timeout(0)
{
}
TcpClient::~TcpClient()
{
    boost::system::error_code hError;
    m_pureSocket.close(hError);
}
void TcpClient::connect(DeltaTime timeout)
{
    // Return if already connected
    if (m_state == Connected || m_state == Connecting)
        return;
    // Clear socket
    m_timeout = timeout;
    m_state = Connecting;

    // Async request
    auto connecter = [this](const boost::system::error_code & theError)
    {
        m_state = !theError ? Connected : Failed;
        m_socket.reset(m_pureSocket);
    };
    m_pureSocket.async_connect(m_endpoint, connecter);

    // Set timer
    m_timer.expires_from_now(boost::posix_time::milliseconds(m_timeout));
    auto failer = [this](const boost::system::error_code&)
    {
        if (m_state == Connecting)
            m_state = Failed;
    };
    m_timer.async_wait(failer);
}
bool TcpClient::wait()
{
    switch (m_state)
    {
    case Connected:
        return true;
    case Disconnected:
    case Failed:
        return false;
    case Connecting:
        boost::system::error_code hError;
        m_service.run_one(hError);
        if (hError)
            m_state = Disconnected;
        return m_state == Connected;
    }
    // Wat teh fuc?!
    return false;
}
bool TcpClient::isFailed() const
{
    return m_state == Failed;
}
bool TcpClient::isConnected() const
{
    return m_state == Connected;
}
bool TcpClient::update()
{
    // Disconnected
    if (m_state == Disconnected)
        return false;
    // Poll messages
    boost::system::error_code error;
    m_service.poll(error);
    if (error)
    {
        m_state = Disconnected;
        return false;
    }
    // Cant connect
    if (m_state == Failed)
        return false;
    // Wait
    if (m_state == Connecting)
        return true;
    // Update
    bool success = m_socket.update();
    // Reset
    if (!success)
        m_state = Disconnected;
    return success;
}
void TcpClient::flush()
{
    if (m_state == Connected)
        m_socket.flush();
}
TcpSocket& TcpClient::socket()
{
    return m_socket;
}
ushort TcpClient::remotePort() const
{
    return m_endpoint.port();
}
ushort TcpClient::localPort() const
{
    boost::system::error_code hError;
    return m_pureSocket.local_endpoint(hError).port();
}
string TcpClient::remoteIP() const
{
    boost::system::error_code hError;
    return m_endpoint.address().to_string(hError);
}
const TcpSocket::Endpoint& TcpClient::endpoint() const
{
    return m_endpoint;
}
