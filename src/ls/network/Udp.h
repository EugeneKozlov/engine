/** @file ls/network/Udp.h
 *  @brief UDP socket.
 */
#pragma once
#include "ls/common.h"
#include "ls/core/Pool.h"
#include "ls/network/NetworkMessage.h"

namespace ls
{
    /** @addtogroup LsNetwork
     *  @{
     */
    /** UDP socket.
     */
    class UdpSocket
    : Noncopyable
    {
    public:
        /// @brief Socket type
        using Socket = boost::asio::ip::udp::socket;
        /// @brief Shared socket
        using SharedSocket = shared_ptr<Socket>;
        /// @brief Endpoint
        using Endpoint = boost::asio::ip::udp::endpoint;
        /// @brief Protocol type
        using Protocol = boost::asio::ip::udp;
        /// @brief v4 protocol
        static const Protocol Protocol4;
        /// @brief v6 protocol
        static const Protocol Protocol6;
        /// @brief Limit of sending messages
        static const uint MessagesPoolLimit = 64;
        /// @brief Get protocol
        static Protocol protocol(bool v6)
        {
            return !v6 ? Protocol4 : Protocol6;
        }
    public:
        /** @name Main
         *  @param protocol
         *    Protocol to use
         *  @param port
         *    Port to listen
         *  @{
         */
        /// @brief Ctor
        UdpSocket(Protocol protocol);
        /// @brief Binding ctor
        UdpSocket(ushort port, Protocol protocol);
        /// @brief Dtor
        ~UdpSocket();
        /// @brief Bind receiver to @a port
        void assign(ushort port);
        /// @}

        /** @name Input/output
         *  @{
         */
        /** Send message to address. Returns immediately.
         *  @param message
         *    Message to send
         *  @param target
         *    Target endpoint
         */
        void sendTo(const NetworkMessage& message, const Endpoint& target);
        /** Send broadcast message. Call blocks caller until message will be send.
         *  @param message
         *    Message to send
         *  @param port
         *    Target port
         *  @return @c true if message has been sent
         */
        bool sendBroadcast(const NetworkMessage& message, ushort port);
        /** Receive message from address.
         *  @param [out] message
         *    Received message, undefined on fail
         *  @param [out] target
         *    Message source, undefined on fail
         *  @return @c true on success, @c false on fail
         */
        bool receiveFrom(NetworkMessage& message, Endpoint& target);
        /// @}

        /** @name Gets
         *  @{
         */
        /// @brief Get port
        ushort port() const noexcept;
        /// @}
    private:
        /// @brief Message receiving handler
        void receiver(const boost::system::error_code& error, size_t received);
    private:
        /// @brief Protocol
        Protocol m_protocol;
        /// @brief Service
        boost::asio::io_service m_service;
        /// @brief Socket
        Socket m_socket;
        /// @brief Output message buffer
        // #TODO Restore it
//         PoolOld<NetworkMessage> m_outputPool;
        /// @brief Input message buffer
        deque<NetworkMessage> m_inputMessages;
        /// @brief Input endpoints
        deque<Endpoint> m_inputEndpoints;
    };
    /// @}
}
