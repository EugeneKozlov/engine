/** @file ls/network/NetworkReflection.h
 *  @brief Network message reflection
 */
#pragma once
#include "ls/common.h"
#include "ls/network/NetworkMessage.h"
#include "ls/io/Codec.h"

namespace ls
{
    /** @addtogroup LsNetwork
     *  @{
     */
    /** Network message reflection prototype.
     */
    class NetworkReflection
    {
    private:
        /// @brief Min sizes per ID
        static array<uint, 256> MessageSizes;
    protected:
        /// @brief Invalid reflection
        NetworkReflection() = default;
        /// @brief New reflection
        NetworkReflection(MessageId id, ubyte tag = 0)
            : id(id)
            , tag(tag)
        {
        }
    public:
        /// @brief Pre-serialize callback
        void preSerialize() const
        {
        }
        /// @brief Post-serialize callback
        void postSerialize() const
        {
        }
        /// @brief Pre-deserialize callback
        void preDeserialize(uint /*messageLength*/)
        {
        }
        /// @brief Post-deserialize callback
        void postDeserialize()
        {
        }
        /// @brief Is valid?
        inline bool isValid() const
        {
            return id != InvalidMessageId;
        }
    public:
        /// @brief Message ID
        MessageId id = InvalidMessageId;
        /// @brief Message tag
        ubyte tag = 0;
    };
    /** Tag specifies if header will be provided by derived.
     */
    struct MessageReflectionCustomHeader
    {
    };
    /** Network message reflection with specified type.
     *  @tparam MessageType Message type
     *  @tparam MaxTailSize Max size of dynamic tail
     */
    template <MessageId MessageType, uint MaxTailSize = 0 >
    class MessageReflection
    : public NetworkReflection
    {
    public:
        /// @brief Reflection type
        static constexpr MessageId Type = MessageType;
        /// @brief Max tail size
        static constexpr uint TailSize = MaxTailSize;
        /// @brief New reflection
        MessageReflection(ubyte tag = 0)
            : NetworkReflection(Type, tag)
        {
        }
        /// @brief Test parsed reflection
        void test() const
        {
            debug_assert(this->id == Type,
                         "message id mismatch, ", this->id, " instead of ", (uint) Type);
        }
        /// @brief Empty header
        EmptySerializable h;
        /// @brief Tail
        StaticStream<TailSize> tail;
    };

    /// @brief Serialize @a message to @a stream
    template <class Message, uint StreamSize>
    enable_if_t<is_base_of<NetworkReflection, Message>::value>
    serialize(const Message& message, NetworkStream<StreamSize>& stream)
    {
        message.test();
        message.preSerialize();

        uint length = NetworkMessage::MinSize + sizeOf(message.h) + sizeOf(message.tail);
        stream.length(length);
        stream.id(message.id);
        stream.tag(message.tag);

        encode(stream, message.h);
        encode(stream, message.tail);

        message.postSerialize();
    }
    /// @brief Deserialize @a message from @a stream
    template <class Message, uint StreamSize>
    enable_if_t<is_base_of<NetworkReflection, Message>::value, void>
    deserialize(Message& message, const NetworkStream<StreamSize>& stream)
    {
        uint streamLength = stream.length();

        message.id = stream.id();
        message.tag = stream.tag();
        message.test();
        message.preDeserialize(streamLength);

        uint headerLength = NetworkMessage::MinSize + sizeOf(message.h);
        uint tailLength = streamLength - headerLength;
        debug_assert(streamLength >= headerLength,
                     "too few message [", message.id, "] : ",
                     streamLength, " instead of ", headerLength);

        DynamicStream source(stream);

        decode(source, message.h);
        message.tail.resize(tailLength);
        decode(source, message.tail);

        message.postDeserialize();
    }
    /// @brief Send @a message
    template <class Message>
    enable_if_t<is_base_of<NetworkReflection, Message>::value, NetworkMessage>
    send(const Message& message)
    {
        NetworkMessage serialized;
        serialize(message, serialized);
        return serialized;
    }
    /// @brief Receive message from @a stream
    template <class Message>
    enable_if_t<is_base_of<NetworkReflection, Message>::value, Message>
    receive(const NetworkMessage& stream)
    {
        Message deserialized;
        deserialize(deserialized, stream);
        return deserialized;
    }
    /// @}
}