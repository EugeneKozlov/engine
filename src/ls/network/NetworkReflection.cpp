#include "pch.h"
#include "ls/network/NetworkReflection.h"

using namespace ls;

array<uint, 256> NetworkReflection::MessageSizes;
uint ls::atoiIP(const char* string, ubyte bytes[])
{
    debug_assert(string && bytes);

    auto address = boost::asio::ip::address::from_string(string);
    if (address.is_v4())
    {
        auto ip = address.to_v4().to_bytes();
        copy_n(ip.data(), 4, bytes);
        return 4;
    }
    else if (address.is_v6())
    {
        auto ip = address.to_v6().to_bytes();
        copy_n(ip.data(), 16, bytes);
        return 16;
    }
    else
    {
        return 0;
    }
}
string ls::itoaIP(const ubyte bytes[], uint size)
{
    debug_assert(bytes);

    using namespace boost::asio::ip;
    boost::system::error_code error;
    switch (size)
    {
    case 4:
    {
        address_v4::bytes_type ip;
        copy_n(bytes, 4, ip.data());
        return address_v4(ip).to_string(error);
    }
    case 16:
    {
        address_v6::bytes_type ip;
        copy_n(bytes, 16, ip.data());
        return address_v6(ip).to_string(error);
    }
    default:
        return "";
    }
}