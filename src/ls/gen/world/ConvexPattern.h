/// @file ls/gen/world/ConvexPattern.h
/// @brief Convex Pattern
#pragma once

#include "ls/common.h"
#include "ls/gen/world/PatternInterface.h"

namespace ls
{
    /// @addtogroup LsGen
    /// @{

    /// @brief Minimize
    template <class Container>
    typename Container::value_type minimize(const Container& container)
    {
        typename Container::value_type minItem{};
        bool first = true;
        for (auto const& item : container)
        {
            if (first)
                minItem = item;
            else
                minItem = min(minItem, item);
            first = false;
        }
        return minItem;
    }
    /// @brief Maximize
    template <class Container>
    typename Container::value_type maximize(const Container& container)
    {
        typename Container::value_type maxItem{};
        bool first = true;
        for (auto const& item : container)
        {
            if (first)
                maxItem = item;
            else
                maxItem = max(maxItem, item);
            first = false;
        }
        return maxItem;
    }
    /// @brief Mix product
    template <class Scalar>
    Scalar mix(const Vector<Scalar, 2>& lhs, const Vector<Scalar, 2>& rhs)
    {
        return lhs.x * rhs.y - lhs.y * rhs.x;
    }

    /// @brief Convex Pattern
    class ConvexPattern
        : public PatternInterface
    {
    public:
        /// @brief Ctor
        ConvexPattern(PatternPool& pool, float smoothDistance, const vector<double2>& shape);
        /// @brief Dtor
        virtual ~ConvexPattern();
        /// @brief Compute pattern
        virtual Pattern& compute(const int2& regionIndex, bool coverAll) const override;
    private:
        struct Vertex
        {
            double2 from;
            double2 to;
            double2 pos;
        };
        static double3 lineByPoints(const double2& a, const double2& b);
        static Vertex vertexByLines(const double2& p, const double3& a, const double3& b);
        float edgeFactor(const double3& edge, const double2& point) const;
        float vertexFactor(const double2& delta) const;
    private:
        float m_smoothDistance;
        vector<double2> m_shape;

        vector<double3> m_edges;
        vector<Vertex> m_vertices;
    };
    /// @}
}

