#include "pch.h"
#include "ls/gen/world/WorldGenerator.h"

#include "ls/asset/AssetManager.h"
#include "ls/asset/TextureAsset.h"
#include "ls/gapi/Renderer.h"
#include "ls/gen/world/ConvexPattern.h"
#include "ls/gen/world/GapiTerrainBrush.h"
#include "ls/gen/world/LayerBrush.h"
#include "ls/gen/world/ObjectGenerator.h"
#include "ls/io/FileSystemInterface.h"

#include "ls/scene/component/all.h"

using namespace ls;
// Buffer
struct WorldGenerator::RegionBuffer : Noncopyable
{
    RegionBuffer(Renderer& renderer, const WorldRegion::Config& worldConfig)
        : renderer(renderer)
        , buffer(renderer, worldConfig, nullptr, true)
    {
        renderHeightNormal = renderer.createRenderTarget(2, nullptr);
        renderer.attachColorTexture2D(renderHeightNormal, 0, buffer.textures[TerrainRegionBuffer::HeightMap]);
        renderer.attachColorTexture2D(renderHeightNormal, 1, buffer.textures[TerrainRegionBuffer::NormalMap]);
        renderer.setTexture2DSampler(buffer.textures[TerrainRegionBuffer::HeightMap],
                                     Filter::Linear, Addressing::Clamp);
        renderer.setTexture2DSampler(buffer.textures[TerrainRegionBuffer::NormalMap],
                                     Filter::Linear, Addressing::Clamp);
    }
    ~RegionBuffer()
    {
        renderer.destroyRenderTarget(renderHeightNormal);
    }

    Renderer& renderer;
    TerrainRegionBuffer buffer;
    RenderTargetHandle renderHeightNormal;
};


// Impl
struct WorldGenerator::Impl
{
    Impl(AssetManager& assetManager, shared_ptr<WorldAsset> world,
         const WorldRegion::Config& worldConfig)
        : m_assetManager(assetManager)
        , m_renderer(*assetManager.renderer())
        , m_worldConfig(worldConfig)
        , m_gapiBuffer0(m_renderer, worldConfig)
        , m_gapiBuffer1(m_renderer, worldConfig)
        , m_world(world)
        , m_worldLoader(true, m_world)
        , m_entityManager({})
        , m_patternPool(worldConfig.regionWidth)
    {
        // Setup world
        m_world->config = m_worldConfig;

        // Create pattern texture
        const uint patternSize = m_worldConfig.regionWidth + 1;
        m_patternTexture = m_renderer
            .createTexture2D(patternSize, patternSize, false, Format::R32_FLOAT,
                             ResourceUsage::Static, ResourceBinding::ShaderResource);
        m_renderer.setTexture2DSampler(m_patternTexture, Filter::Linear, Addressing::Clamp);
    }
    ~Impl()
    {
        m_renderer.destroyTexture2D(m_patternTexture);
    }
    void generateRegion(WorldRegion& region)
    {
        // Brushes
        const auto& brushes = m_brushes[region.index()];

        // Clear and reset color
        m_sourceBuffer->buffer.clearAll(m_defaultHeight);

        // Generate
        bool geometryOnGapi = true;
        for (auto brush : brushes)
        {
            if (brush->isGeometryOnGapi())
            {
                // MUST read and write geometry
                debug_assert(brush->isGeometryRead() && brush->isGeometryWritten());

                // Commit to GAPI
                if (!geometryOnGapi)
                {
                    m_sourceBuffer->buffer.commitMap(TerrainRegionBuffer::HeightMap);
                    m_sourceBuffer->buffer.commitMap(TerrainRegionBuffer::NormalMap);
                }

                // Prepare
                brush->setBuffers(m_sourceBuffer->buffer, m_destBuffer->renderHeightNormal,
                                  m_patternTexture);

                // Draw
                brush->generate(region);

                // Swap buffers
                swap(m_sourceBuffer, m_destBuffer);
            }
            else
            {
                // Extract from GAPI
                if (geometryOnGapi && brush->isGeometryRead())
                {
                    m_sourceBuffer->buffer.extractMap(TerrainRegionBuffer::HeightMap);
                    m_sourceBuffer->buffer.extractMap(TerrainRegionBuffer::NormalMap);
                    m_sourceBuffer->buffer.readMap(TerrainRegionBuffer::HeightMap, region);
                    m_sourceBuffer->buffer.readMap(TerrainRegionBuffer::NormalMap, region);
                }

                // Draw
                brush->generate(region);
            }

            // Geometry is stored there from now
            if (brush->isGeometryWritten())
            {
                geometryOnGapi = brush->isGeometryOnGapi();
            }
        }

        // Read output
        if (geometryOnGapi)
        {
            m_sourceBuffer->buffer.extractMap(TerrainRegionBuffer::HeightMap);
            m_sourceBuffer->buffer.extractMap(TerrainRegionBuffer::NormalMap);
            m_sourceBuffer->buffer.readMap(TerrainRegionBuffer::HeightMap, region);
            m_sourceBuffer->buffer.readMap(TerrainRegionBuffer::NormalMap, region);
        }

        // Bake colors
        for (WorldRegion::Chunk& chunk : region.chunksData())
        {
            chunk.updateColor(m_materialColors);
        }

        // Delete brushes
        m_brushes.erase(region.index());

        // Generate objects
        for (ObjectGenerator& gen : m_generators)
            gen.generate(region, m_entityManager);

        // Store objects
        Buffer staticObjects;
        m_entityManager.serializeStatic(staticObjects, region.index());
        m_entityManager.releaseStatic(region.index());
        region.chunksData().staticObjects(staticObjects);

        // Save region
        region.save();
    }
    void generateGlobals()
    {
        // Add terrain
        Entity heightFieldTerrainEntity;
        HeightfieldTerrainComponent& heightFieldTerrainComponent
            = heightFieldTerrainEntity.addComponent<HeightfieldTerrainComponent>();
        GrassComponent& grassComponent
            = heightFieldTerrainEntity.addComponent<GrassComponent>();
        grassComponent.assetName = "architect/nature/plants/grass/bluegrass/model";
        grassComponent.minDistance = 0.5f;
        m_entityManager.addEntity(heightFieldTerrainEntity);

        // Construct components
        for (ObjectGenerator& objectGenerator : m_generators)
        {
            objectGenerator.constructModelComponents(m_entityManager, m_assetManager);
        }
    }
    void generate()
    {
        // Generate globals
        generateGlobals();

        // Generate regions
        m_worldLoader.process(m_beginRegion, m_endRegion, bind(&Impl::generateRegion, this, _1));
    }
    template <class Hamap, class Object>
    Object& addToRegions(Hamap& dest, shared_ptr<Object> obj, PatternInterface& pattern)
    {
        m_tempRegionList.clear();
        pattern.listRegions(m_tempRegionList);
        for (const int2& regionIndex : m_tempRegionList)
            dest[regionIndex].push_back(obj);
        return *obj;
    }

public:
    /// @name Common
    /// @{
    AssetManager& m_assetManager;
    Renderer& m_renderer;
    const WorldRegion::Config m_worldConfig;
    /// @}

    /// @name Buffers to generate region geometry on GPU
    /// @{
    RegionBuffer m_gapiBuffer0;
    RegionBuffer m_gapiBuffer1;
    RegionBuffer* m_sourceBuffer = &m_gapiBuffer0;
    RegionBuffer* m_destBuffer = &m_gapiBuffer1;
    /// @}

    /// @name World to be generated
    /// @{
    shared_ptr<WorldAsset> m_world;
    IterableWorld m_worldLoader;
    EntityManagerBuildMode m_entityManager;
    /// @}
    /// @name Materials
    /// @{
    hamap<string, uint> m_materialNames;
    vector<float4> m_materialColors;
    /// @}

    /// @name Generating info
    /// @{
    float m_defaultHeight = 0.0f;
    vector<int2> m_tempRegionList;
    PatternPool m_patternPool;
    hamap<int2, vector<shared_ptr<PatternInterface>>> m_patterns;
    hamap<int2, vector<shared_ptr<BrushInterface>>> m_brushes;
    list<ObjectGenerator> m_generators;
    /// @}

    /// @name Size
    /// @{
    int2 m_beginRegion;
    int2 m_endRegion;
    /// @}

    /// @name Temporary texture
    /// @{
    Texture2DHandle m_patternTexture;
    /// @}
};


// Main
WorldGenerator::WorldGenerator(AssetManager& assetManager)
    : m_renderer(*assetManager.renderer())
    , m_assetManager(assetManager)
{
    // Noise texture
    vector<float4> noiseData(NoiseMapSize * NoiseMapSize, 0.0f);
    for (float4& texel : noiseData)
        texel = float4(random(0.0f, 1.0f), random(0.0f, 1.0f), random(0.0f, 1.0f), random(0.0f, 1.0f));
    m_noiseTexture = m_renderer
        .createTexture2D(NoiseMapSize, NoiseMapSize, false, Format::R32G32B32A32_FLOAT,
                         ResourceUsage::Constant, ResourceBinding::ShaderResource, 
                         asArray({ static_cast<const void*>(noiseData.data()) }));
    m_renderer.setTexture2DSampler(m_noiseTexture, Filter::Linear, Addressing::Wrap);


    // Compute point cloud
    // #HACK use seed
    m_poissonCloud = PoissonRandom(42).generate(m_poissonCloudStep, 30, 10000);

    // Load effects
    m_bakeEffect = Asset::cast<EffectAsset>(
        m_assetManager.loadAsset(AssetType::Effect, "architect/generator/world/tex_to_color_effect"));
}
WorldGenerator::~WorldGenerator()
{
    m_renderer.destroyTexture2D(m_noiseTexture);
}


// Setup
void WorldGenerator::begin(shared_ptr<WorldAsset> world,
                           const WorldRegion::Config& worldConfig)
{
    WorldRegion::Config worldConfigCopy = worldConfig;
    worldConfigCopy.compute();
    m_impl = make_unique<Impl>(m_assetManager, world, worldConfigCopy);
}
void WorldGenerator::setDefaultHeight(const float height)
{
    checkImpl();

    m_impl->m_defaultHeight = height;
}
void WorldGenerator::setSize(const uint width, const uint height)
{
    checkImpl();

    const uint regionWidth = m_impl->m_world->config.regionWidth;
    m_impl->m_beginRegion = int2(static_cast<sint>(floor(width / -2.0 / regionWidth)),
                                 static_cast<sint>(floor(height / -2.0 / regionWidth)));
    m_impl->m_endRegion = int2(static_cast<sint>(ceil(width / 2.0 / regionWidth)),
                               static_cast<sint>(ceil(height / 2.0 / regionWidth)));
}


// Paint
uint WorldGenerator::addMaterial(const string& materialName)
{
    checkImpl();

    return addMaterialColor(materialName, 0.0f);
}
uint WorldGenerator::addMaterialColor(const string& materialName, const float4& color)
{
    checkImpl();

    // Return old
    const uint* oldIdx = findOrDefault(m_impl->m_materialNames, materialName);
    if (oldIdx)
        return *oldIdx;

    // Make new
    vector<string>& materialsList = m_impl->m_world->materials;
    const uint newIdx = materialsList.size();
    materialsList.push_back(materialName);
    m_impl->m_materialNames.emplace(materialName, newIdx);
    m_impl->m_materialColors.push_back(color);
    return newIdx;
}
PatternInterface& WorldGenerator::addConvexPattern(const float smoothDistance, const vector<double2>& shape)
{
    checkImpl();

    // Make pattern
    auto pattern = make_shared<ConvexPattern>(m_impl->m_patternPool, smoothDistance, shape);

    // Store and return
    return m_impl->addToRegions(m_impl->m_patterns, pattern, *pattern);
}
GapiTerrainBrush& WorldGenerator::addGapiTerrainBrush(PatternInterface& pattern, 
                                                      const string& effectName, const string& programName)
{
    checkImpl();

    try
    {
        // Load effect
        shared_ptr<EffectAsset> effectAsset = Asset::cast<EffectAsset>(
            m_assetManager.loadAsset(AssetType::Effect, effectName));

        // Make brush
        auto brush = make_shared<GapiTerrainBrush>(m_renderer, pattern, 
                                                   effectAsset, programName);

        // Store and return
        return m_impl->addToRegions(m_impl->m_brushes, brush, pattern);
    }
    catch (const IoException& ex)
    {
        LS_THROW(0, ArgumentException, ex.what());
        throw 0;
    }
}
LayerBrush& WorldGenerator::addLayerBrush(PatternInterface& pattern, const int2& layer, const float2& value)
{
    checkImpl();

    // Make brush
    auto brush = make_shared<LayerBrush>(pattern, layer, value);

    // Store and return
    return m_impl->addToRegions(m_impl->m_brushes, brush, pattern);
}
ObjectGenerator& WorldGenerator::addObjectGenerator()
{
    checkImpl();

    m_impl->m_generators.emplace_back(m_poissonCloud, m_poissonCloudStep, m_impl->m_world->config.regionWidth);
    return m_impl->m_generators.back();
}


// Material colors
void WorldGenerator::computeMaterialColors(const vector<string>& materials, 
                                           vector<float4>& colors)
{
    // Load materials
    const uint numMaterials = materials.size();
    vector<shared_ptr<TextureAsset>> materialTextures;
    for (uint idx = 0; idx < numMaterials; ++idx)
    {
        shared_ptr<TextureAsset> texture = Asset::cast<TextureAsset>(
            m_assetManager.loadAsset(AssetType::Texture, materials[idx]));
        materialTextures.push_back(texture);
    }

    // Create texture
    Texture2DHandle texture = m_renderer
        .createTexture2D(1, 1, false, Format::R32G32B32A32_FLOAT,
                         ResourceUsage::Static, 
                         ResourceBinding::Readable | ResourceBinding::RenderTarget);
    RenderTargetHandle renderTarget = m_renderer.createRenderTarget(texture, nullptr);

    // Create state
    StateDesc stateDesc;
    stateDesc.primitive = PrimitiveType::TriangleStrip;
    stateDesc.program = m_renderer.handleProgram(m_bakeEffect->gapiEffect(), "Bake");
    debug_assert(stateDesc.program);
    StateHandle state = m_renderer.createState(stateDesc);

    // Make
    m_renderer.bindState(state);
    m_renderer.bindRenderTarget(renderTarget);
    colors.resize(numMaterials);
    for (uint idx = 0; idx < numMaterials; ++idx)
    {
        // We already have a color
        if (colors[idx].w > 0.0)
            continue;

        // Render texture to pixel
        TextureAsset& materialTexture = *materialTextures[idx];
        m_renderer.bindTexture(0, materialTexture.gapiResource(), ShaderType::Pixel);
        m_renderer.draw(0, 4);

        // Save color
        m_renderer.readTexture2D(texture, &colors[idx]);
    }

    // Destroy
    m_renderer.destroyRenderTarget(renderTarget);
    m_renderer.destroyTexture2D(texture);
    m_renderer.destroyState(state);
}


// Save
bool WorldGenerator::save()
{
    checkImpl();

    // Compute colors
    computeMaterialColors(m_impl->m_world->materials, m_impl->m_materialColors);

    // Generate all
    try
    {
        m_impl->generate();
    }
    catch (const Exception& ex)
    {
        logWarning("Cannot save world : ", ex.what());
        return false;
    }

    // Save entities
    m_impl->m_entityManager.serializeGlobalComponents(m_impl->m_world->globalComponents);
    m_impl->m_entityManager.serializeDynamics(m_impl->m_world->dynamicEntities);

    // Save main
    m_impl->m_world->save();

    // Log
    logInfo("World [", m_impl->m_world->name(), "] is saved");
    return true;
}


// Getters
Texture2DHandle WorldGenerator::noiseTexture()
{
    return m_noiseTexture;
}


// Export to Lua
void WorldGenerator::exportToLua(LuaState& state)
{
    function<WorldGenerator&()> luaFactory = [this]() -> WorldGenerator& { return *this; };
    LuaBinding(state)
        .beginModule("cg")
        /**/.beginClass<WorldRegion::Config>("WorldRegionConfig")
        /**//**/.addConstructor(LUA_ARGS())
        /**//**/.addVariable("chunkWidth", &WorldRegion::Config::chunkWidth)
        /**//**/.addVariable("regionWidth", &WorldRegion::Config::regionWidth)
        /**//**/.addVariable("regionHeightMap", &WorldRegion::Config::regionHeightMap)
        /**//**/.addVariable("regionNormalMap", &WorldRegion::Config::regionNormalMap)
        /**//**/.addVariable("regionColorMap", &WorldRegion::Config::regionColorMap)
        /**/.endClass()
        /**/.beginClass<Texture2DHandle>("Texture2DHandle")
        /**/.endClass()
        /**/.beginClass<WorldGenerator>("WorldGenerator")
        /**//**/.addFunction("begin", &WorldGenerator::luaBegin)
        /**//**/.addFunction("defaultHeight", &WorldGenerator::setDefaultHeight)
        /**//**/.addFunction("setSize", &WorldGenerator::setSize)
        /**//**/.addFunction("addMaterial", &WorldGenerator::addMaterial)
        /**//**/.addFunction("addMaterialColor", &WorldGenerator::addMaterialColor)
        /**//**/.addFunction("addConvexPattern", &WorldGenerator::addConvexPattern)
        /**//**/.addFunction("addGapiTerrainBrush", &WorldGenerator::addGapiTerrainBrush)
        /**//**/.addFunction("addLayerBrush", &WorldGenerator::addLayerBrush)
        /**//**/.addFunction("addObjectGenerator", &WorldGenerator::addObjectGenerator)
        /**//**/.addFunction("noiseTexture", &WorldGenerator::noiseTexture)
        /**//**/.addFunction("save", &WorldGenerator::save)
        /**/.endClass()
        /**/.beginClass<PatternInterface>("PatternInterface")
        /**/.endClass()
        /**/.beginClass<GapiTerrainBrush>("GapiTerrainBrush")
        /**//**/.addFunction("setTexture", &GapiTerrainBrush::setTexture)
        /**/.endClass()
        /**/.beginClass<LayerBrush>("LayerBrush")
        /**/.endClass()
        /**/.beginClass<ObjectGenerator>("ObjectGenerator")
        /**//**/.addFunction("addRectangle", &ObjectGenerator::addRectangle)
        /**//**/.addFunction("addCircle", &ObjectGenerator::addCircle)
        /**/.endClass()
        /**/.addFunction("world", luaFactory)
        .endModule();
}
void WorldGenerator::luaBegin(Asset::Ptr worldAsset, const WorldRegion::Config& worldConfig)
{
    shared_ptr<WorldAsset> world = Asset::cast<WorldAsset>(worldAsset);
    LS_THROW(world, ArgumentException, "Asset is not a world");
    begin(world, worldConfig);
}
