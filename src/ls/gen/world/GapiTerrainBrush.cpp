#include "pch.h"
#include "ls/gen/world/GapiTerrainBrush.h"

#include "ls/gapi/Renderer.h"
#include "ls/gen/world/PatternInterface.h"
#include "ls/scene/graphical/TerrainBuffer.h"
#include "ls/asset/WorldAsset.h"

using namespace ls;
GapiTerrainBrush::GapiTerrainBrush(Renderer& renderer,
                                   PatternInterface& pattern,
                                   shared_ptr<EffectAsset> effectAsset, const string& programName)
    : BrushInterface(pattern)
    , renderer(renderer)
    , m_effect(effectAsset)
    , m_programName(programName)
{
    // Load and check check program
    m_program = renderer.handleProgram(m_effect->gapiEffect(), programName.c_str());
    LS_THROW(!!m_program, ArgumentException, "Unknown program [", m_programName, "]");

    // Create uniform buffer
    m_constBuffer = renderer.createUniformBuffer(sizeof(Constants));
    renderer.attachUniformBuffer(m_program->effect, m_constBuffer, "Const");

    // Create state
    StateDesc desc;
    desc.primitive = PrimitiveType::TriangleStrip;
    desc.program = m_program;
    desc.rasterizer.culling = Culling::None;
    m_state = renderer.createState(desc);
}
GapiTerrainBrush::~GapiTerrainBrush()
{
    if (!!m_state)
        renderer.destroyState(m_state);
    if (!!m_constBuffer)
        renderer.destroyBuffer(m_constBuffer);
}


// Init
void GapiTerrainBrush::setTexture(uint slot, Texture2DHandle texture)
{
    debug_assert(slot < NumSourceMaps);
    m_textures[slot] = texture;
}


// Generate
void GapiTerrainBrush::setBuffers(TerrainRegionBuffer& sourceBuffer,
                                  RenderTargetHandle destBuffer,
                                  Texture2DHandle patternTexture)
{
    m_sourceBuffer = &sourceBuffer;
    m_destBuffer = destBuffer;
    m_patternTexture = patternTexture;
}
string GapiTerrainBrush::generate(WorldRegion& region)
{
    debug_assert(m_sourceBuffer && !!m_destBuffer);

    // Get pattern
    Pattern& pattern = m_pattern.compute(region.index(), true);
    uint patternSize = m_pattern.regionSize() + 1;
    renderer.writeTexture2D(m_patternTexture, patternSize, patternSize, pattern.data());

    // Update const
    float regionWidth = static_cast<float>(region.config().regionWidth);
    double2 regionBegin = static_cast<double2>(region.index()) * regionWidth;
    double2 regionEnd = static_cast<double2>(region.index() + 1) * regionWidth;

    float2 quadBegin = 0.0f;
    float2 quadEnd = 1.0f;

    float2 beginPosition = static_cast<float2>(regionBegin) - 0.5f;
    float2 endPosition = static_cast<float2>(regionEnd) + 0.5f;

    float2 beginTexture = -0.5f / regionWidth;
    float2 endTexture = 1.0f + 0.5f / regionWidth;

    Constants constants;
    constants.vQuadPosition = float4(quadBegin, quadEnd);
    constants.vTexturePosition = float4(beginTexture, endTexture);
    constants.vWorldPosition = float4(beginPosition, endPosition);
    for (uint slot = 0; slot < NumSourceMaps; ++slot)
    {
        if (!m_textures[slot])
            constants.vTexturesSizes[slot] = int2(0);
        else
            constants.vTexturesSizes[slot] = m_textures[slot]->size();
    }
    renderer.writeBuffer(m_constBuffer, constants);

    // Update resources
    ResourceBucket resources;
    resources.addResource(SlotPattern, m_patternTexture, ShaderType::Pixel);
    resources.addResource(SlotHeightMap, m_sourceBuffer->textures[TerrainRegionBuffer::HeightMap], ShaderType::Pixel);
    resources.addResource(SlotNormalMap, m_sourceBuffer->textures[TerrainRegionBuffer::NormalMap], ShaderType::Pixel);
    for (uint slot : enumerate(SlotSource0, SlotCOUNT))
    {
        resources.addResource(slot, m_textures[slot - SlotSource0], ShaderType::Pixel);
    }

    // Bind 
    renderer.bindState(m_state);
    renderer.bindResources(resources);
    renderer.bindRenderTarget(m_destBuffer);
    renderer.draw(0, 4);
    renderer.bindRenderTarget(nullptr);

    // Release pattern
    m_pattern.releasePattern(pattern);
    return "";
}