/// @file ls/gen/world/PatternInterface.h
/// @brief Pattern Interface
#pragma once

#include "ls/common.h"
#include "ls/core/FlatArray.h"

namespace ls
{
    /// @addtogroup LsGen
    /// @{

    /// @brief Pattern
    class Pattern
        : Noncopyable
    {
    public:
        /// @brief Reset
        void reset();
        /// @brief Resize
        void resize(const double2& begin, const double2& end);

        /// @brief Get data
        const float* data() const
        {
            return m_mask.data();
        }
        /// @brief Get element
        float& operator [] (const int2& index)
        {
            return m_mask[index];
        }
        /// @brief Get const element
        float operator [] (const int2& index) const
        {
            return m_mask[index];
        }
        /// @brief Make position
        double2 position(const int2& index) const
        {
            return (double2) (index + m_begin);
        }
        /// @brief Get size
        int2 size() const
        {
            return m_mask.size();
        }
        /// @brief Get width
        uint width() const
        {
            return m_mask.width();
        }
        /// @brief Get height
        uint height() const
        {
            return m_mask.height();
        }
        /// @brief Get begin
        int2 begin() const
        {
            return m_begin;
        }
        /// @brief Get end
        int2 end() const
        {
            return m_end;
        }
    private:
        /// @brief Mask
        FlatArray<float> m_mask;
        /// @brief Begin
        int2 m_begin;
        /// @brief End
        int2 m_end;
    };
    /// @brief Pattern Pool
    class PatternPool
        : Noncopyable
    {
    public:
        /// @brief Region size
        const uint regionSize;
    public:
        /// @brief Ctor
        PatternPool(uint regionSize);
        /// @brief Allocate
        Pattern& allocate();
        /// @brief Release
        void release(Pattern& pattern);
    private:
        list<Pattern> m_freePatterns;
        list<Pattern> m_usedPatterns;
    };
    /// @brief Pattern Interface
    class PatternInterface
        : Noncopyable
    {
    public:
        /// @brief Ctor
        PatternInterface(PatternPool& pool, const double2& begin, const double2& end);
        /// @brief Dtor
        virtual ~PatternInterface();
        /// @brief Compute pattern
        virtual Pattern& compute(const int2& regionIndex, bool coverAll) const = 0;
        /// @brief Release pattern
        void releasePattern(Pattern& pattern);

        /// @brief Get region size
        uint regionSize() const;
        /// @brief List regions affected by this brush
        void listRegions(vector<int2>& regions) const;
    protected:
        /// @brief Pool
        PatternPool& m_pool;
        /// @brief Begin
        const double2 m_begin;
        /// @brief End
        const double2 m_end;
    };
    /// @}
}

