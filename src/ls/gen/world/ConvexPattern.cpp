#include "pch.h"
#include "ls/gen/world/ConvexPattern.h"

using namespace ls;
ConvexPattern::ConvexPattern(PatternPool& pool, float smoothDistance, const vector<double2>& shape)
    : PatternInterface(pool, minimize(shape) - smoothDistance, maximize(shape) + smoothDistance)
    , m_smoothDistance(smoothDistance)
    , m_shape(shape)
{
    // Compute edges
    m_shape.push_back(m_shape.front());
    for (uint idx = 1; idx < m_shape.size(); ++idx)
    {
        m_edges.push_back(lineByPoints(m_shape[idx - 1], m_shape[idx]));
    }

    // Compute vertices
    m_edges.push_back(m_edges.front());
    for (uint idx = 1; idx < m_edges.size(); ++idx)
    {
        m_vertices.push_back(vertexByLines(m_shape[idx], m_edges[idx - 1], m_edges[idx]));
    }
    m_edges.pop_back();
}
ConvexPattern::~ConvexPattern()
{
}
Pattern& ConvexPattern::compute(const int2& regionIndex, bool coverAll) const
{
    Pattern& pattern = m_pool.allocate();

    // Get area
    double2 regionBegin = static_cast<double2>(regionIndex) * regionSize();
    double2 regionEnd = static_cast<double2>(regionIndex + 1) * regionSize();

    // Make smaller
    if (!coverAll)
    {
        regionBegin = max(regionBegin, m_begin);
        regionEnd = min(regionEnd, m_end);

        // Skip out-of-area
        if (regionBegin.x > regionEnd.x || regionBegin.y > regionEnd.y)
        {
            pattern.reset();
            return pattern;
        }
    }

    // Resize
    pattern.resize(regionBegin, regionEnd);

    // Fill distance map
    for (int2 idx : makeRange(int2(0), pattern.size()))
    {
        double2 pos = pattern.position(idx);
        float factor = 1.0f;
        for (double3 const& line : m_edges)
        {
            factor = min(factor, edgeFactor(line, pos));
            if (factor == 0.0f)
                break;
        }
        for (Vertex const& vertex : m_vertices)
        {
            double2 v = vertex.pos - pos;
            if (mix(v, vertex.to) < 0.0f || mix(vertex.from, v) < 0.0f)
                continue;
            factor = min(factor, vertexFactor(v));
        }
        pattern[idx] = factor;
    }

    return pattern;
}


// Math
double3 ConvexPattern::lineByPoints(const double2& a, const double2& b)
{
    double2 n = normalize(double2(a.y - b.y, b.x - a.x));
    return double3(n, -dot(double2(n), a));
}
ConvexPattern::Vertex ConvexPattern::vertexByLines(const double2& p, const double3& a, const double3& b)
{
    Vertex v;
    v.pos = p;
    v.from = static_cast<double2>(a);
    v.to = static_cast<double2>(b);
    return v;
}
float ConvexPattern::edgeFactor(const double3& edge, const double2& point) const
{
    double dist = -dot(double3(point, 1.0), edge);
    return 1.0f - clamp(static_cast<float>(-dist) / m_smoothDistance, 0.0f, 1.0f);
}
float ConvexPattern::vertexFactor(const double2& delta) const
{
    double dist = length(delta);
    return 1.0f - clamp(static_cast<float>(dist) / m_smoothDistance, 0.0f, 1.0f);
}


