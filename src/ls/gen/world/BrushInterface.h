/// @file ls/gen/world/BrushInterface.h
/// @brief Brush Interface
#pragma once

#include "ls/common.h"
#include "ls/gapi/common.h"

namespace ls
{
    class WorldRegion;
    class TerrainRegionBuffer;
    class PatternInterface;

    /// @addtogroup LsGen
    /// @{

    /// @brief Brush Interface
    class BrushInterface
        : Noncopyable
    {
    public:
        /// @brief Ctor
        BrushInterface(PatternInterface& pattern);
        /// @brief Dtor
        virtual ~BrushInterface();
        /// @brief Initialize GAPI
        virtual void setBuffers(TerrainRegionBuffer& sourceBuffer,
                                RenderTargetHandle destBuffer,
                                Texture2DHandle pattern);
        /// @brief Generate region
        virtual string generate(WorldRegion& region) = 0;

        /// @brief Are heights and normals read?
        virtual bool isGeometryRead() const
        {
            return false;
        }
        /// @brief Are heights and normals written?
        virtual bool isGeometryWritten() const
        {
            return false;
        }
        /// @brief Are heights and normals processed by GAPI?
        virtual bool isGeometryOnGapi() const
        {
            return false;
        }

        /// @brief Get pattern
        const PatternInterface& pattern() const
        {
            return m_pattern;
        }
    protected:
        /// @brief Pattern
        PatternInterface& m_pattern;
    };
    /// @}
}

