#include "pch.h"
#include "ls/gen/world/LayerBrush.h"
#include "ls/gen/world/PatternInterface.h"
#include "ls/asset/WorldAsset.h"

using namespace ls;
LayerBrush::LayerBrush(PatternInterface& pattern, const int2& layer, const float2& value)
    : BrushInterface(pattern)
    , m_layer(layer)
    , m_value(value)
{
}
LayerBrush::~LayerBrush()
{
}


// Generate
string LayerBrush::generate(WorldRegion& region)
{
    // Get pattern
    Pattern& pattern = m_pattern.compute(region.index(), false);

    // Get chunks
    int2 chunkBegin = static_cast<int2>(floor(static_cast<float2>(pattern.begin()) 
                                              / static_cast<float>(region.config().chunkWidth)));
    int2 chunkEnd = static_cast<int2>(floor(static_cast<float2>(pattern.end()) 
                                            / static_cast<float>(region.config().chunkWidth))) + 1;

    // Fill data
    vector<float3> dataBuffer(pattern.width() * pattern.height(), 0.0f);
    FlatArrayWrapper<float3> dataWrapper(pattern.width(), pattern.height(), dataBuffer.data());
    FlatArray<WorldRegion::Chunk::Blend> tempBuffer;
    for (int2 idx : makeRange(int2(0), pattern.size()))
    {
        float srcFactor = pattern[idx];
        dataWrapper[idx] = float3(m_value, srcFactor);
    }

    // Iterate over chunks
    for (int2 idx : makeRange(max(region.beginChunk(), chunkBegin),
                              min(region.endChunk(), chunkEnd)))
    {
        WorldRegion::Chunk* chunk = region.findChunkByIndex(idx);
        debug_assert(chunk);
        chunk->updateBlend(m_layer, pattern.begin(), pattern.end(), dataWrapper, tempBuffer);
    }

    // Release pattern
    m_pattern.releasePattern(pattern);
    return "";
}