#include "pch.h"
#include "ls/gen/world/ObjectGenerator.h"

#include "ls/asset/AssetManager.h"
#include "ls/gen/PoissonRandom.h"
#include "ls/gen/world/WorldGenerator.h"
#include "ls/scene/component/ModelComponent.h"
#include "ls/scene/component/RenderComponent.h"
#include "ls/scene/component/TransformComponent.h"

using namespace ls;


// Object
ObjectGenerator::ObjectGenerator(const PointCloud2DNorm& pointCloud, const float pointCloudStep,
                                 const uint regionWidth)
    : regionWidth(regionWidth)
    , m_pointCloud(pointCloud)
    , m_pointCloudStep(pointCloudStep)
{
}
ObjectGenerator::~ObjectGenerator()
{
}
PointCloud2D ObjectGenerator::samplePoints(const double2& begin, const double2& end,
                                           const float minDistanceNew, const float minDistanceExist)
{
    // Sample point cloud
    // #TODO change float2 to double2
    const float scale = minDistanceNew / m_pointCloudStep;
    PointCloud2D points = samplePointCloud(m_pointCloud, begin, end, scale);

    // Skip points too close to existing
    auto eraser = [this, minDistanceExist](const double2& point)
    {
        const int2 pointRegion = static_cast<int2>(floor(point / static_cast<float>(regionWidth)));
        int2 idx;
        for (idx.x = pointRegion.x - 1; idx.x <= pointRegion.x + 1; ++idx.x)
        {
            for (idx.y = pointRegion.y - 1; idx.y <= pointRegion.y + 1; ++idx.y)
            {
                // Skip empty region
                if (!m_objects.count(idx))
                    continue;

                // Enumerate objects
                for (const auto& item : m_objects[idx])
                {
                    const double2 anotherPos = static_cast<double2>(swiz<X, Z>(item.pose.position()));
                    if (square_distance(point, anotherPos) < minDistanceExist * minDistanceExist)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    };
    points.erase(std::remove_if(points.begin(), points.end(), eraser), points.end());

    return points;
}
void ObjectGenerator::addRectangle(const float x0, const float y0, const float x1, const float y1,
                                   const float minDistance, const float objectRadius, const string& modelName)
{
    // Reserve record model component
    m_modelComponents[modelName] = nullptr;

    // Sample points
    PointCloud2D points = samplePoints(double2(x0, y0), double2(x1, y1), minDistance, objectRadius);
    
    // Generate objects
    // #HACK use seed
    const uint seed = static_cast<uint>(x0 + y0 * 3) ^ static_cast<uint>(x1 * 3 + y1);
    UniformRandom uniformRandom(seed);
    for (const double2& point : points)
    {
        const int2 pointRegion = static_cast<int2>(floor(point / static_cast<float>(regionWidth)));
        ObjectDesc object;
        object.pose.position(double3(point.x, 0.0f, point.y));
        object.pose.rotation(quat(float3(0, 1, 0), uniformRandom.randomReal_01() * 2 * pi));
        object.pose.scaling(uniformRandom.randomReal(0.9f, 1.1f));
        object.modelName = modelName;
        m_objects[pointRegion].push_back(object);
    }
}
void ObjectGenerator::addCircle(const float x0, const float y0, const float r,
                                const float minDistance, const float objectRadius, const string& modelName)
{
    // Reserve record model component
    m_modelComponents[modelName] = nullptr;

    // Sample points
    const double2 center = double2(x0, y0);
    PointCloud2D points = samplePoints(center - r, center + r, minDistance, objectRadius);

    // Generate objects
    // #HACK use seed
    const uint seed = static_cast<uint>(x0 + y0 * 3) ^ static_cast<uint>(r);
    UniformRandom uniformRandom(seed);
    for (const double2& point : points)
    {
        // Skip if out-of-circle
        // #TODO Generalize
        if (distance(point, center) > r)
        {
            continue;
        }

        const int2 pointRegion = static_cast<int2>(floor(point / static_cast<float>(regionWidth)));
        ObjectDesc object;
        object.pose.position(double3(point.x, 0.0f, point.y));
        object.pose.rotation(quat(float3(0, 1, 0), uniformRandom.randomReal_01() * 2 * pi));
        object.pose.scaling(uniformRandom.randomReal(0.9f, 1.1f));
        object.modelName = modelName;
        m_objects[pointRegion].push_back(object);
    }
}
void ObjectGenerator::constructModelComponents(EntityManagerBuildMode& entityManager, AssetManager& assetManager)
{
    for (auto& item : m_modelComponents)
    {
        ModelComponent& model = entityManager.createSharedComponent<ModelComponent>();
        model.assetName = item.first;
        item.second = &model;
    }
}
void ObjectGenerator::generate(WorldRegion& region, EntityManagerBuildMode& entityManager)
{
    auto* objects = findOrDefault(m_objects, region.index());
    if (!objects)
        return;

    for (ObjectDesc& desc : *objects)
    {
        double3 position = desc.pose.position();
        position.y = region.getHeight(swiz<X, Z>(position));

        Entity entity;

        entity.addSharedComponent(*m_modelComponents[desc.modelName]);
        TransformComponent& transform = entity.addComponent<TransformComponent>();
        transform.pose = ObjectPose(position, desc.pose.rotationQuat(), desc.pose.scaling());
        RenderComponent& render = entity.addComponent<RenderComponent>();

        entityManager.addStaticEntity(entity, region.index());
    }
}
