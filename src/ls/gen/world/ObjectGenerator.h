/// @file ls/gen/world/ObjectGenerator.h
/// @brief Object Generator
#pragma once

#include "ls/common.h"
#include "ls/core/UniformRandom.h"
#include "ls/gen/PoissonRandom.h"
#include "ls/scene/core/ObjectPose.h"
#include "ls/asset/WorldAsset.h"

namespace ls
{
    class AssetManager;
    class EntityManagerBuildMode;
    class ModelComponent;

    /// @addtogroup LsGen
    /// @{

    /// @brief Object Generator
    class ObjectGenerator
        : Noncopyable
    {
    public:
        /// @brief Poisson max new points
        static const uint MaxNewPoints = 30;
    public:
        /// @brief Ctor
        ObjectGenerator(const PointCloud2DNorm& pointCloud, const float pointCloudStep,
                        const uint regionWidth);
        /// @brief Dtor
        ~ObjectGenerator();
        /// @brief Generate region
        void generate(WorldRegion& region, EntityManagerBuildMode& entityManager);
        /// @brief Model components
        void constructModelComponents(EntityManagerBuildMode& entityManager, AssetManager& assetManager);
        /// @brief Generate rectangle
        void addRectangle(const float x0, const float y0, const float x1, const float y1,
                          const float minDistance, const float objectRadius, const string& modelName);
        /// @brief Generate circle
        void addCircle(const float x0, const float y0, const float r,
                       const float minDistance, const float objectRadius, const string& modelName);
    public:
        const uint regionWidth;
    private:
        PointCloud2D samplePoints(const double2& begin, const double2& end,
                                  const float minDistanceNew, const float minDistanceExist);
    private:
        const PointCloud2DNorm& m_pointCloud; ///< Precomputed point cloud
        const float m_pointCloudStep; ///< Point cloud step

        /// @brief Object Description
        struct ObjectDesc
        {
            ObjectPose pose;
            string modelName;
        };
        hamap<int2, vector<ObjectDesc>> m_objects; ///< Objects
        hamap<string, ModelComponent*> m_modelComponents;
    };
    /// @}
}

