#include "pch.h"
#include "ls/gen/world/BrushInterface.h"

using namespace ls;
BrushInterface::BrushInterface(PatternInterface& pattern)
    : m_pattern(pattern)
{
}
BrushInterface::~BrushInterface()
{
}
void BrushInterface::setBuffers(TerrainRegionBuffer& /*sourceBuffer*/,
                                RenderTargetHandle /*destBuffer*/,
                                Texture2DHandle /*pattern*/)
{
    LS_THROW(isGeometryOnGapi(), NotImplementedException);
    LS_THROW(!isGeometryOnGapi(), LogicException);
}
