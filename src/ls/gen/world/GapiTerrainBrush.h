/// @file ls/gen/world/GapiTerrainBrush.h
/// @brief Gapi Terrain Brush
#pragma once

#include "ls/common.h"
#include "ls/asset/EffectAsset.h"
#include "ls/gapi/common.h"
#include "ls/gen/world/BrushInterface.h"

namespace ls
{
    class Renderer;
    class TerrainRegionBuffer;

    /// @addtogroup LsGen
    /// @{

    /// @brief Gapi Terrain Brush
    class GapiTerrainBrush
        : public BrushInterface
    {
    public:
        /// @brief Resource slots
        enum : uint
        {
            /// @brief Pattern
            SlotPattern,
            /// @brief Height map
            SlotHeightMap,
            /// @brief Normal map
            SlotNormalMap,
            /// @brief Source #0
            SlotSource0,
            /// @brief Source #1
            SlotSource1,
            /// @brief Num slots
            SlotCOUNT
        };
        /// @brief Number of source maps
        static const uint NumSourceMaps = (uint) SlotCOUNT - (uint) SlotSource0;
        /// @brief Constants
        struct Constants
        {
            /// @brief Quad position
            float4 vQuadPosition;
            /// @brief Texture position
            float4 vTexturePosition;
            /// @brief World position
            float4 vWorldPosition;
            /// @brief Textures sizes
            int2 vTexturesSizes[NumSourceMaps];
        };
    public:
        /// @brief Renderer
        Renderer& renderer;
    public:
        /// @brief Ctor
        GapiTerrainBrush(Renderer& renderer, PatternInterface& pattern, 
                         shared_ptr<EffectAsset> effectAsset, const string& programName);
        /// @brief Dtor
        ~GapiTerrainBrush();
        /// @brief Set texture
        void setTexture(uint slot, Texture2DHandle texture);
    public:
        virtual void setBuffers(TerrainRegionBuffer& sourceBuffer,
                                RenderTargetHandle destBuffer,
                                Texture2DHandle patternTexture) override;
        virtual string generate(WorldRegion& region) override;

        virtual bool isGeometryRead() const override
        {
            return true;
        }
        virtual bool isGeometryWritten() const override
        {
            return true;
        }
        virtual bool isGeometryOnGapi() const override
        {
            return true;
        }
    private:
        // Main
        shared_ptr<EffectAsset> m_effect;
        string m_programName;
        ProgramHandle m_program;
        BufferHandle m_constBuffer;
        StateHandle m_state;

        // Textures
        array<Texture2DHandle, NumSourceMaps> m_textures;

        // Current state
        TerrainRegionBuffer* m_sourceBuffer = nullptr;
        RenderTargetHandle m_destBuffer = nullptr;
        Texture2DHandle m_patternTexture = nullptr;
    };
    /// @}
}

