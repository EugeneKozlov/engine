/// @file ls/gen/world/WorldGenerator.h
/// @brief World Generator Instance
#pragma once

#include "ls/common.h"
#include "ls/asset/EffectAsset.h"
#include "ls/core/Nameable.h"
#include "ls/gen/PoissonRandom.h"
#include "ls/gen/world/PatternInterface.h"
#include "ls/lua/LuaApi.h"
#include "ls/scene/EntityManager.h"
#include "ls/scene/graphical/TerrainBuffer.h"
#include "ls/world/IterableWorld.h"
#include "ls/asset/WorldAsset.h"

namespace ls
{
    class AssetManager;

    class PatternInterface;
    class ConvexPattern;

    class BrushInterface;
    class GapiTerrainBrush;
    class LayerBrush;

    class ObjectGenerator;

    /// @addtogroup LsGen
    /// @{

    /// @brief World Generator Instance
    class WorldGenerator
        : Noncopyable
        , public BuiltinModule
    {
    public:
        /// @brief Noise Map Size
        static const uint NoiseMapSize = 1024;
    public:
        /// @brief Ctor
        WorldGenerator(AssetManager& assetManager);
        /// @brief Dtor
        ~WorldGenerator();

        /// @brief Begin generation
        /// @param world
        ///   World to be generated
        /// @param worldConfig
        ///   Configuration of generated world
        void begin(shared_ptr<WorldAsset> world,
                   const WorldRegion::Config& worldConfig);

        /// @brief Set size
        /// @throw LogicException if generator is not initialized
        void setSize(const uint width, const uint height);
        /// @brief Set default height
        /// @throw LogicException if generator is not initialized
        void setDefaultHeight(const float height);
        /// @brief Add material
        /// @throw LogicException if generator is not initialized
        uint addMaterial(const string& materialName);
        /// @brief Add material with specified color
        /// @throw LogicException if generator is not initialized
        uint addMaterialColor(const string& materialName, const float4& color);
        /// @brief Add convex pattern
        /// @throw LogicException if generator is not initialized
        PatternInterface& addConvexPattern(const float smoothDistance, const vector<double2>& shape);
        /// @brief Add GAPI terrain brush
        /// @throw LogicException if generator is not initialized
        GapiTerrainBrush& addGapiTerrainBrush(PatternInterface& pattern,
                                              const string& effectName, const string& programName);
        /// @brief Add layer brush
        /// @throw LogicException if generator is not initialized
        LayerBrush& addLayerBrush(PatternInterface& pattern, const int2& layer, const float2& value);
        /// @brief Add object generator
        /// @throw LogicException if generator is not initialized
        ObjectGenerator& addObjectGenerator();

        /// @brief End generation and save asset
        /// @throw LogicException if generator is not initialized
        bool save();

        /// @brief Get noise texture
        Texture2DHandle noiseTexture();
    private:
        /// @name Implement BuiltinModule
        /// @{
        virtual void exportToLua(LuaState& state) override;
        void luaBegin(Asset::Ptr worldAsset,
                      const WorldRegion::Config& worldConfig);
        /// @}

        void checkImpl() const
        {
            LS_THROW(m_impl, LogicException, "Generator is not initialized");
        }

        /// @brief Compute material colors by textures
        void computeMaterialColors(const vector<string>& materials, vector<float4>& colors);
    private:
        Renderer& m_renderer;
        AssetManager& m_assetManager;
        shared_ptr<EffectAsset> m_bakeEffect;

        struct RegionBuffer;
        struct Impl;
        unique_ptr<Impl> m_impl; ///< Contain all generation-specific members

        /// @name Precomputed Poisson cloud
        /// @{
        PointCloud2DNorm m_poissonCloud;
        float m_poissonCloudStep = 0.05f;
        /// @}

        /// @name Textures
        /// @{
        Texture2DHandle m_noiseTexture;
        /// @}
    };
    /// @}
}

