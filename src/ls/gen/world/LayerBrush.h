/// @file ls/gen/world/LayerBrush.h
/// @brief Layer Brush
#pragma once

#include "ls/common.h"
#include "ls/gen/world/BrushInterface.h"

namespace ls
{
    /// @addtogroup LsGen
    /// @{

    /// @brief Layer Brush
    class LayerBrush
        : public BrushInterface
    {
    public:
        /// @brief Ctor
        LayerBrush(PatternInterface& pattern, const int2& layer, const float2& value);
        /// @brief Dtor
        ~LayerBrush();
        /// @brief Set texture
        void setTexture(uint slot, Texture2DHandle texture);
    public:
        virtual string generate(WorldRegion& region) override;

        virtual bool isGeometryRead() const override
        {
            return false;
        }
        virtual bool isGeometryWritten() const override
        {
            return false;
        }
        virtual bool isGeometryOnGapi() const override
        {
            return false;
        }
    private:
        const int2 m_layer; 
        const float2 m_value;
    };
    /// @}
}

