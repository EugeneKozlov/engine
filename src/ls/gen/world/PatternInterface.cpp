#include "pch.h"
#include "ls/gen/world/PatternInterface.h"

using namespace ls;


// Pattern
void Pattern::reset()
{
    m_begin = int2(0);
    m_end = int2(-1);
    m_mask.reset(0, 0);
}
void Pattern::resize(const double2& begin, const double2& end)
{
    m_begin = static_cast<int2>(floor(begin));
    m_end = static_cast<int2>(ceil(end));
    const int2 size = m_end - m_begin + 1;
    m_mask.reset(static_cast<uint>(size.x), static_cast<uint>(size.y));
}


// Pool
PatternPool::PatternPool(uint regionSize)
    : regionSize(regionSize)
{

}
Pattern& PatternPool::allocate()
{
    if (m_freePatterns.empty())
        m_freePatterns.emplace_front();
    Pattern& pattern = m_freePatterns.front();
    m_usedPatterns.splice(m_usedPatterns.begin(), m_freePatterns, m_freePatterns.begin());
    return pattern;
}
void PatternPool::release(Pattern& pattern)
{
    for (auto iter = m_usedPatterns.begin(); iter != m_usedPatterns.end(); ++iter)
    {
        if (&*iter == &pattern)
        {
            m_freePatterns.splice(m_freePatterns.begin(), m_usedPatterns, iter);
            return;
        }
    }
    debug_assert(0, "pattern is not found");
}


// Interface
PatternInterface::PatternInterface(PatternPool& pool, const double2& begin, const double2& end)
    : m_pool(pool)
    , m_begin(begin)
    , m_end(end)
{
}
PatternInterface::~PatternInterface()
{
}
void PatternInterface::releasePattern(Pattern& pattern)
{
    m_pool.release(pattern);
}
uint PatternInterface::regionSize() const
{
    return m_pool.regionSize;
}
void PatternInterface::listRegions(vector<int2>& regions) const
{
    const int2 beginRegion = static_cast<int2>(floor(m_begin / static_cast<float>(regionSize())));
    const int2 endRegion = static_cast<int2>(floor(m_end / static_cast<float>(regionSize())));
    for (int2 const regionIndex : makeRange(beginRegion, endRegion + 1))
        regions.push_back(regionIndex);
}
