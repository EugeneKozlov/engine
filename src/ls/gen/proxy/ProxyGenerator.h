/// @file ls/gen/proxy/ProxyGenerator.h
/// @brief Proxy Generator
#pragma once

#include "ls/common.h"
#include "ls/gapi/common.h"
#include "ls/gen/texture/TextureGenerator.h"
#include "ls/lua/BuiltinModule.h"

namespace ls
{
    class MeshAsset;
    class ModelAsset;
    class Renderer;
    class StreamInterface;
    class TextureAsset;
    class TextureGenerator;
    struct MaterialCacheContext;

    /// @addtogroup LsGen
    /// @{

    /// @brief Proxy Generator
    class ProxyGenerator
        : Noncopyable
        , public BuiltinModule
    {
    public:
        /// @brief Proxy Model Type
        // #TODO Update docs
        enum class ModelType
        {
            /// @brief Unknown
            Unknown = 0,
            /// @brief Cylinder proxy.
            ///        It has 4 two-sided vertical slices and 
            ///        8 one-sided skew ones.
            Cylinder8d8s,
            /// @brief Counter
            COUNT
        };
        /// @brief Number of Proxy Model Types
        static const uint NumModelTypes = static_cast<uint>(ModelType::COUNT);
    public:
        /// @brief Ctor
        ProxyGenerator(AssetManager& assetManager, TextureGenerator& textureGenerator);
        /// @brief Dtor
        ~ProxyGenerator();

        /// @brief Resize output
        /// @note Output can be extended, only minimal size of output is specified
        /// @throw ArgumentException if dimensions are invalid
        void resizeOutput(const uint width, const uint height);
        /// @brief Set source model
        void setSourceModel(shared_ptr<ModelAsset> model);
        /// @brief Set generation mode
        void setModelType(const ModelType modelType);
        /// @brief Set color output
        void setColorOutput(const Compression compression,
                            shared_ptr<EffectAsset> mipmapEffect, const string& mipmapProgram,
                            const vector<float>& mipmapParameters,
                            shared_ptr<EffectAsset> healEffect, const string& healProgram,
                            const vector<float>& healParameters);
        /// @brief Set normal output
        void setNormalOutput(const Compression compression,
                             shared_ptr<EffectAsset> mipmapEffect, const string& mipmapProgram,
                             const vector<float>& mipmapParameters,
                             shared_ptr<EffectAsset> healEffect, const string& healProgram,
                             const vector<float>& healParameters);
        /// @brief Generate resource
        /// @param mesh
        ///   Output mesh
        /// @param textures
        ///   Output textures
        void generate(shared_ptr<MeshAsset> meshAsset,
                      shared_ptr<TextureAsset> colorTextureAsset, shared_ptr<TextureAsset> normalTextureAsset);
        /// @brief Drop cached data
        void drop();
    private:
        /// @name Implement BuiltinModule
        /// @{
        virtual void exportToLua(LuaState& state) override;
        void luaResizeOutput(const uint width, const uint height);
        void luaSetSourceModel(Asset::Ptr model);
        void luaSetModelType(const uint modelType);
        void luaSetColorOutput(const uint compression,
                               Asset::Ptr mipmapEffect, const string& mipmapProgram,
                               const vector<float>& mipmapParameters,
                               Asset::Ptr healEffect, const string& healProgram,
                               const vector<float>& healParameters);
        void luaSetNormalOutput(const uint compression,
                                Asset::Ptr mipmapEffect, const string& mipmapProgram,
                                const vector<float>& mipmapParameters,
                                Asset::Ptr healEffect, const string& healProgram,
                                const vector<float>& healParameters);
        void luaGenerate(Asset::Ptr meshAsset, Asset::Ptr colorTextureAsset, Asset::Ptr normalTextureAsset);
        /// @}

        void generateCylinder8d8s(shared_ptr<MeshAsset> destMesh,
                                  shared_ptr<TextureAsset> destColor, shared_ptr<TextureAsset> destNormal);
        void allocateOutput();
        void dropOutputCache();
    private:
        Renderer& m_renderer;
        MaterialCache& m_materialCache;
        TextureGenerator& m_textureGenerator;
        MaterialCacheContext* m_materialCacheContext;

        uint m_outputWidth = 0;
        uint m_outputHeight = 0;
        shared_ptr<ModelAsset> m_sourceModel;
        ModelType m_modelType = ModelType::Unknown;

        struct ShaderDesc
        {
            shared_ptr<EffectAsset> effect;
            string program;
            TextureGenerator::ParameterArray parameters;
        };
        struct OutputDesc
        {
            Compression compression;
            ShaderDesc mipmapShader;
            ShaderDesc healShader;
        };

        OutputDesc m_colorOutput;
        OutputDesc m_normalOutput;

        Texture2DHandle m_colorTexture;
        Texture2DHandle m_normalTexture;
        Texture2DHandle m_depthTexture;
        RenderTargetHandle m_renderTarget;
    };
    /// @}
}

