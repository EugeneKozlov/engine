#include "pch.h"
#include "ls/gen/proxy/ProxyGenerator.h"

#include "ls/asset/AssetManager.h"
#include "ls/asset/MaterialCache.h"
#include "ls/asset/MeshAsset.h"
#include "ls/asset/ModelAsset.h"
#include "ls/asset/TextureAsset.h"
#include "ls/asset/TreeAsset.h"
#include "ls/gapi/Renderer.h"
#include "ls/gen/utility/geometry.h"

using namespace ls;
// Main
ProxyGenerator::ProxyGenerator(AssetManager& assetManager, TextureGenerator& textureGenerator)
    : m_renderer(*assetManager.renderer())
    , m_materialCache(*assetManager.materialCache())
    , m_textureGenerator(textureGenerator)
{
    m_materialCacheContext = m_materialCache
        .registerContext(StateDesc(), MaterialAsset::ShaderMode::Proxy);
}
ProxyGenerator::~ProxyGenerator()
{
    drop();
    m_materialCache.unregisterContext(m_materialCacheContext);
}


// Setup
void ProxyGenerator::setSourceModel(shared_ptr<ModelAsset> model)
{
    // Load source model
    m_sourceModel = model;
    m_sourceModel->loadLevel(0);
}
void ProxyGenerator::setModelType(const ModelType modelType)
{
    m_modelType = modelType;
}
void ProxyGenerator::setColorOutput(const Compression compression,
                                    shared_ptr<EffectAsset> mipmapEffect, const string& mipmapProgram,
                                    const vector<float>& mipmapParameters,
                                    shared_ptr<EffectAsset> healEffect, const string& healProgram,
                                    const vector<float>& healParameters)
{
    LS_THROW(!mipmapProgram.empty(),
             ArgumentException, "Empty mipmap program name");
    LS_THROW(!healProgram.empty(),
             ArgumentException, "Empty heal program name");
    LS_THROW(mipmapParameters.size() < TextureGenerator::BufferSize * 4,
             ArgumentException, "Too many mipmap parameters");
    LS_THROW(healParameters.size() < TextureGenerator::BufferSize * 4,
             ArgumentException, "Too many heal parameters");

    m_colorOutput.compression = compression;
    m_colorOutput.mipmapShader.effect = mipmapEffect;
    m_colorOutput.mipmapShader.program = mipmapProgram;
    m_colorOutput.mipmapShader.parameters = TextureGenerator::compressParameters(mipmapParameters);
    m_colorOutput.healShader.effect = healEffect;
    m_colorOutput.healShader.program = healProgram;
    m_colorOutput.healShader.parameters = TextureGenerator::compressParameters(healParameters);
}
void ProxyGenerator::setNormalOutput(const Compression compression,
                                     shared_ptr<EffectAsset> mipmapEffect, const string& mipmapProgram,
                                     const vector<float>& mipmapParameters,
                                     shared_ptr<EffectAsset> healEffect, const string& healProgram,
                                     const vector<float>& healParameters)
{
    LS_THROW(!mipmapProgram.empty(),
             ArgumentException, "Empty mipmap program name");
    LS_THROW(!healProgram.empty(),
             ArgumentException, "Empty heal program name");
    LS_THROW(mipmapParameters.size() < TextureGenerator::BufferSize * 4,
             ArgumentException, "Too many mipmap parameters");
    LS_THROW(healParameters.size() < TextureGenerator::BufferSize * 4,
             ArgumentException, "Too many heal parameters");

    m_normalOutput.compression = compression;
    m_normalOutput.mipmapShader.effect = mipmapEffect;
    m_normalOutput.mipmapShader.program = mipmapProgram;
    m_normalOutput.mipmapShader.parameters = TextureGenerator::compressParameters(mipmapParameters);
    m_normalOutput.healShader.effect = healEffect;
    m_normalOutput.healShader.program = healProgram;
    m_normalOutput.healShader.parameters = TextureGenerator::compressParameters(healParameters);
}
void ProxyGenerator::resizeOutput(const uint width, const uint height)
{
    if (m_outputWidth != width || m_outputHeight != height)
    {
        dropOutputCache();
    }
    m_outputWidth = width;
    m_outputHeight = height;
}


// Generate
namespace
{
    /// @brief Describes one proxy slice to be rendered
    struct RenderSlice
    {
        float2 uvBegin; ///< Begin UV
        float2 uvEnd; ///< End UV
        uint2 texelBegin; ///< Begin texel
        uint2 texelEnd; ///< End texel
        float3 viewPosition; ///< View position
        float4x4 viewMatrix; ///< View matrix
        float4x4 projectionMatrix; ///< Projection matrix

        /// @brief Set texture region
        void setUv(const float2& begin, const float2& end, const uint width, const uint height)
        {
            uvBegin = begin;
            uvEnd = end;
            texelBegin = uint2(static_cast<uint>(round(uvBegin.x * width)),
                               static_cast<uint>(round(uvBegin.y * height)));
            texelEnd = uint2(static_cast<uint>(round(uvEnd.x * width)),
                             static_cast<uint>(round(uvEnd.y * height)));
        }
    };
    /// @brief Generation context
    template <class Vertex>
    struct Context
    {
        uint2 dimensions;
        vector<RenderSlice> slices;
        vector<Vertex> vertices;
        vector<ushort> indices;
    };
    /// @brief Set wind parameters
    template <class Vertex>
    void setVertexWind(Vertex& /*vertex*/, 
                       const float /*mainAdherence*/, const float /*edgeOscillation*/)
    {
        // Do nothing
    }
    /// @brief Set wind parameters for tree vertex
    template <>
    void setVertexWind<TreeAsset::Vertex>(TreeAsset::Vertex& vertex, 
                                          const float mainAdherence, const float edgeOscillation)
    {
        vertex.mainAdherence = mainAdherence;
        vertex.edgeOscillation = edgeOscillation;
        vertex.phase = 0.0f;
        vertex.branchAdherence = 0.0f;
    }
    /// @brief Fix main adherence
    template <class Vertex>
    void fixVertexMainAdherence(Vertex& /*vertex*/, const float /*trunkStrength*/)
    {
    }
    /// @brief Fix main adherence for tree vertex
    template <>
    void fixVertexMainAdherence(TreeAsset::Vertex& vertex, const float trunkStrength)
    {
        vertex.mainAdherence = TreeAsset::computeMainAdherence(vertex.mainAdherence, trunkStrength);
    }
    /// @brief Compute skew height
    float computeSkewHeight(const float width, const float height, const float skewAngle)
    {
        const float2 axis = float2(sin(skewAngle), cos(skewAngle));
        const float2 size = float2(width, height);
        return project(axis, size);
    }
    /// @brief Expand region to meet the ratio
    float2 expandRegionToScale(const float2 region, const float ratio)
    {
        float2 ret;
        ret.x = max(region.x, ratio * region.y);
        ret.y = ret.x / ratio;
        return ret;
    }
    /// @brief Generate slices for cylindrical proxy
    /// @pre width <= height
    template <class Vertex>
    void generateCylinderSlices(Context<Vertex>& ctx,
                                const uint numSlices, const uint numVertSegments,
                                const uint baseTextureSize,
                                const float width, const float height, const float skewAngle,
                                const float3& center,
                                const float edgeMagnitude, const float trunkStrength)
    {
        // Compute dimensions
        debug_assert(width <= height);
        const float skewHeight = computeSkewHeight(width, height, deg2rad(skewAngle));
        const float2 totalSize = float2(width * numSlices, height + skewHeight);
        const float aspectRatio = totalSize.x / totalSize.y;
        const uint textureScale = aspectRatio < sqrt(2.0f) ? 1 : 2;
        const uint2 dimensions = uint2(textureScale, 1) * baseTextureSize;
        const float2 fixedTotalSize = expandRegionToScale(totalSize, static_cast<float>(textureScale));

        // Store in context
        ctx.dimensions = dimensions;

        // Generate vertical slices
        for (uint row = 0; row < 2; ++row)
        {
            // Size of slice
            const float sliceHeight = row ? height : skewHeight;
            const float halfWidth = width / 2.0f;
            const float halfHeight = sliceHeight / 2.0f;
            const float halfDepth = length(float2(halfWidth, halfHeight));

            // UV base
            const float baseV = row ? skewHeight : 0.0f;

            // Rotation around X axis
            const float angleX = row ? 0.0f : pi / 4;

            for (uint idx = 0; idx < numSlices; ++idx)
            {
                RenderSlice slice;

                // Rotation angle and axises
                const float angleY = 2 * pi * idx / numSlices;
                const float3 axisX = float3(cos(angleY), 0.0f, sin(angleY));
                const float3 axisFlatZ = normalize(cross(axisX, float3(0.0f, 1.0f, 0.0f)));
                const float3 axisY = float3(sin(angleX) * axisFlatZ.x, cos(angleX), sin(angleX) * axisFlatZ.z);
                const float3 axisZ = normalize(cross(axisX, axisY));

                // Compute UVs
                const float2 absBegin = float2(idx * width, baseV);
                const float2 absEnd = float2((idx + 1) * width, baseV + sliceHeight);
                slice.setUv(absBegin / fixedTotalSize, absEnd / fixedTotalSize, 
                            dimensions.x, dimensions.y);

                // Compute camera
                slice.viewPosition = center;
                const float3x3 rotationY = matrix::fromBasis(axisX, -axisY, axisZ);
                slice.viewMatrix = inverse(static_cast<float4x4>(rotationY)
                                           * matrix::translate(center));
                slice.projectionMatrix = matrix::orthoLeft(-halfWidth, halfWidth,
                                                           halfHeight, -halfHeight,
                                                           -halfDepth, halfDepth);

                // Compute normal
                const float3 normal = float3(float4(0.0f, 0.0f, -1.0f, 0.0f) 
                                             * transpose(slice.viewMatrix));

                // Generate positions and texture coordinates
                array<Vertex, 4> verts;
                verts[0].pos = center - axisX * halfWidth - axisY * halfHeight;
                verts[0].uv = float2(slice.uvBegin.x, slice.uvEnd.y);
                verts[0].normal = normal;
                verts[1].pos = center + axisX * halfWidth - axisY * halfHeight;
                verts[1].uv = float2(slice.uvEnd.x, slice.uvEnd.y);
                verts[1].normal = normal;
                verts[2].pos = center - axisX * halfWidth + axisY * halfHeight;
                verts[2].uv = float2(slice.uvBegin.x, slice.uvBegin.y);
                verts[2].normal = normal;
                verts[3].pos = center + axisX * halfWidth + axisY * halfHeight;
                verts[3].uv = float2(slice.uvEnd.x, slice.uvBegin.y);
                verts[3].normal = normal;

                // Generate wind parameters
                setVertexWind(verts[0], 0.0f, 0.0f);
                setVertexWind(verts[1], 0.0f, 0.0f);
                setVertexWind(verts[2], 1.0f, edgeMagnitude);
                setVertexWind(verts[3], 1.0f, edgeMagnitude);

                // Generate quads
                appendQuadGrid(ctx.vertices, ctx.indices,
                               verts[0], verts[1], verts[2], verts[3],
                               1, numVertSegments);
                ctx.slices.push_back(slice);
            }
        }

        // Fix main adherence
        for (Vertex& vertex : ctx.vertices)
        {
            fixVertexMainAdherence(vertex, trunkStrength);
        }
    }
    /// @brief Render slice
    void renderSlice(RenderSlice& slice,
                     ModelAsset& model, Renderer& renderer,
                     MaterialCacheContext* materialCacheContext,
                     RenderTargetHandle renderTarget,
                     BufferHandle instanceBuffer,
                     BufferHandle globalUniformBuffer, BufferHandle modelUniformBuffer)
    {
        MeshAsset& mesh = *model.mesh(0);

        // Update buffers
        MaterialAsset::SimpleInstanceBuffer instanceBufferData;
        instanceBufferData.modelMatrix = float4x4();
        instanceBufferData.windDirection = ubyte3(0, 0, 0);
        instanceBufferData.windMain = static_cast<half>(0.0f);
        instanceBufferData.windPulseFrequency = static_cast<half>(0.0f);
        instanceBufferData.windPulseMagnitude = static_cast<half>(0.0f);
        instanceBufferData.windTurbulence = static_cast<half>(0.0f);
        instanceBufferData.fade1 = static_cast<half>(1.0f);
        instanceBufferData.fade2 = static_cast<half>(1.0f);
        renderer.writeBuffer(instanceBuffer, instanceBufferData);

        MaterialAsset::GlobalUniformBuffer globalUniformBufferData;
        globalUniformBufferData.mView = slice.viewMatrix;
        globalUniformBufferData.mProjection = slice.projectionMatrix;
        globalUniformBufferData.vCameraPosition = float4(slice.viewPosition, 1.0f);
        globalUniformBufferData.vTime = float4(0.0f);
        globalUniformBufferData.vAmbientColor = 1.0f;
        globalUniformBufferData.vAmbientFadeIntensity = 1.0f;
        globalUniformBufferData.vAmbientFadeDistance = 1.0f;
        renderer.writeBuffer(globalUniformBuffer, globalUniformBufferData);

        MaterialAsset::ModelUniformBuffer modelUniformBufferData;
        modelUniformBufferData.vParam = mesh.uniformBuffer();
        renderer.writeBuffer(modelUniformBuffer, modelUniformBufferData);

        // Bind render target
        const uint2 size = slice.texelEnd - slice.texelBegin;
        renderer.bindRenderTargetRegion(renderTarget,
                                        slice.texelBegin.x, slice.texelBegin.y,
                                        size.x, size.y);

        // Bind geometry
        GeometryBucket geometry = mesh.gapiGeometry();
        geometry.addVertexBuffer(instanceBuffer);

        // Render subsets
        const uint numSubsets = mesh.numSubsets();
        for (uint subsetIndex = 0; subsetIndex < numSubsets; ++subsetIndex)
        {
            const MeshAsset::Subset& subset = mesh.subset(subsetIndex);
            MaterialAsset* material = model.material(0, subset.materialIndex);

            material->attachUniformBuffer(MaterialAsset::UniformBuffer::Global,
                                          globalUniformBuffer,
                                          MaterialAsset::GlobalUniformBuffer::name());
            material->attachUniformBuffer(MaterialAsset::UniformBuffer::Model,
                                          modelUniformBuffer,
                                          MaterialAsset::ModelUniformBuffer::name());

            StateDesc desc;
            StateHandle state = material->gapiState(materialCacheContext);
            renderer.bindState(state);
            renderer.bindGeometry(geometry);
            renderer.bindResources(material->gapiResources(MaterialAsset::ShaderMode::Proxy));
            renderer.drawIndexedInstanced(subset.offset, subset.numIndices, 0, 0, 1);

            material->detachUniformBuffer(MaterialAsset::UniformBuffer::Global,
                                          MaterialAsset::GlobalUniformBuffer::name());
            material->detachUniformBuffer(MaterialAsset::UniformBuffer::Model,
                                          MaterialAsset::ModelUniformBuffer::name());
        }
    }
}

void ProxyGenerator::generate(shared_ptr<MeshAsset> meshAsset,
                              shared_ptr<TextureAsset> colorTextureAsset, shared_ptr<TextureAsset> normalTextureAsset)
{
    shared_ptr<MeshAsset> mesh = meshAsset;
    shared_ptr<TextureAsset> colorTexture = colorTextureAsset;
    shared_ptr<TextureAsset> normalTexture = normalTextureAsset;
    switch (m_modelType)
    {
    case ModelType::Cylinder8d8s:
        generateCylinder8d8s(mesh, colorTexture, normalTexture);
        break;
    default:
        LS_THROW(0, ArgumentException, "Invalid proxy type");
        break;
    }

    // Finalize
    if (!m_textureGenerator.isCompressionDisabled())
    {
        colorTexture->compress(m_colorOutput.compression);
        normalTexture->compress(m_normalOutput.compression);
    }
}
void ProxyGenerator::generateCylinder8d8s(shared_ptr<MeshAsset> destMesh,
                                          shared_ptr<TextureAsset> destColor, shared_ptr<TextureAsset> destNormal)
{
    // #TODO Not only trees

    // DO NOT CHANGE
    static const uint NumVertSlices = 8;
    static const uint NumVertParts = 3;
    static const uint NumHorSlices = 4;

    // Get source
    MeshAsset* sourceMesh = m_sourceModel->mesh(0);
    LS_THROW(sourceMesh, ArgumentException, "Bad source model");
    AABB aabb = sourceMesh->aabb();

    // Get tree parameters
    const TreeAsset::ModelParameters params = sourceMesh->parameters<TreeAsset::ModelParameters>();

    // Destination arrays
    Context<TreeAsset::Vertex> ctx;

    // Compute size
    const float3 size = static_cast<float3>(aabb.extents());
    const float width = 2 * params.modelRadius;
    const float height = size.y;
    const float3 center = static_cast<float3>(aabb.center());

    // Generate slices and geometry
    generateCylinderSlices(ctx, NumVertSlices, NumVertParts, m_outputWidth,
                           width, std::max(width, height), 45.0f, center, 
                           1.0f, params.trunkStrength);
    resizeOutput(ctx.dimensions.x, ctx.dimensions.y);

    // Prepare buffers
    BufferHandle instanceBuffer = m_renderer
        .createBuffer(sizeof(MaterialAsset::SimpleInstanceBuffer), 
                      BufferType::Vertex, ResourceUsage::Static, nullptr);
    BufferHandle globalUniformBuffer = m_renderer
        .createUniformBuffer(sizeof(MaterialAsset::GlobalUniformBuffer));
    BufferHandle modelUniformBuffer = m_renderer
        .createUniformBuffer(sizeof(MaterialAsset::ModelUniformBuffer));

    // Render texture
    allocateOutput();
    m_renderer.bindRenderTarget(m_renderTarget);
    m_renderer.clearDepthStencil(m_renderTarget, 1.0f, 0);
    m_renderer.clearColor(m_renderTarget, 0, float4(0.0f));
    m_renderer.clearColor(m_renderTarget, 1, float4(0.0f, 0.0f, 0.0f, 1.0f));
    for (RenderSlice& slice : ctx.slices)
    {
        renderSlice(slice,
                    *m_sourceModel, m_renderer, 
                    m_materialCacheContext, m_renderTarget,
                    instanceBuffer, globalUniformBuffer, modelUniformBuffer);
    }

    // Heal color
    m_textureGenerator.processTexture(m_colorTexture, TextureGenerator::TextureType::Regular,
                                      m_colorOutput.healShader.effect, m_colorOutput.healShader.program,
                                      "Size", "Parameters", m_colorOutput.healShader.parameters);

    // Heal normal
    m_textureGenerator.processTexture(m_normalTexture, TextureGenerator::TextureType::Regular,
                                      m_normalOutput.healShader.effect, m_normalOutput.healShader.program,
                                      "Size", "Parameters", m_normalOutput.healShader.parameters);

    // Clean up buffers
    m_renderer.destroyBuffer(globalUniformBuffer);
    m_renderer.destroyBuffer(modelUniformBuffer);
    m_renderer.destroyBuffer(instanceBuffer);

    // Generate mipmaps
    m_textureGenerator.generateMimpaps(m_colorTexture, TextureGenerator::TextureType::Regular,
                                       m_colorOutput.mipmapShader.effect, 
                                       m_colorOutput.mipmapShader.program, 
                                       m_colorOutput.mipmapShader.parameters);
    m_textureGenerator.generateMimpaps(m_normalTexture, TextureGenerator::TextureType::Regular,
                                       m_normalOutput.mipmapShader.effect,
                                       m_normalOutput.mipmapShader.program,
                                       m_normalOutput.mipmapShader.parameters);

    // Save textures
    m_renderer.unloadTexture2D(destColor->storage(), m_colorTexture);
    m_renderer.unloadTexture2D(destNormal->storage(), m_normalTexture);

    // Generate TBN
    computeTangentSpace(ctx.vertices.data(), ctx.vertices.size(), ctx.indices.data(), ctx.indices.size() / 3);

    // Store mesh
    destMesh->setVertexData(ctx.vertices.data(), ctx.vertices.size());
    destMesh->setIndexData(ctx.indices.data(), ctx.indices.size());
    destMesh->addSubset(0, ctx.indices.size());
    destMesh->setAabb(computeAabb(ctx.vertices.data(), ctx.vertices.size()));
    destMesh->setUniformBuffer(params.proxy);
}


// Cache helpers
void ProxyGenerator::allocateOutput()
{
    const auto flags = ResourceBinding::RenderTarget | ResourceBinding::ShaderResource | ResourceBinding::Readable;
    if (!m_colorTexture)
    {
        m_colorTexture = m_renderer
            .createTexture2D(m_outputWidth, m_outputHeight, true, 
                             Format::R8G8B8A8_UNORM, 
                             ResourceUsage::Static, flags);
    }
    if (!m_normalTexture)
    {
        m_normalTexture = m_renderer
            .createTexture2D(m_outputWidth, m_outputHeight, true,
                             Format::R8G8B8A8_UNORM,
                             ResourceUsage::Static, flags);
    }
    if (!m_depthTexture)
    {
        m_depthTexture = m_renderer
            .createTexture2D(m_outputWidth, m_outputHeight, false,
                             Format::D24_UNORM_S8_UINT,
                             ResourceUsage::Static,
                             ResourceBinding::DepthStencil);
    }
    if (!m_renderTarget)
    {
        m_renderTarget = m_renderer.createRenderTarget(2, m_depthTexture);
        m_renderer.attachColorTexture2D(m_renderTarget, 0, m_colorTexture);
        m_renderer.attachColorTexture2D(m_renderTarget, 1, m_normalTexture);
    }
}
void ProxyGenerator::drop()
{
    dropOutputCache();
}
void ProxyGenerator::dropOutputCache()
{
    if (!!m_renderTarget)
    {
        m_renderer.destroyRenderTarget(m_renderTarget);
        m_renderTarget = nullptr;
    }
    if (!!m_colorTexture)
    {
        m_renderer.destroyTexture2D(m_colorTexture);
        m_colorTexture = nullptr;
    }
    if (!!m_normalTexture)
    {
        m_renderer.destroyTexture2D(m_normalTexture);
        m_normalTexture = nullptr;
    }
    if (!!m_depthTexture)
    {
        m_renderer.destroyTexture2D(m_depthTexture);
        m_depthTexture = nullptr;
    }
}


// Lua
void ProxyGenerator::exportToLua(LuaState& state)
{
    function<ProxyGenerator&()> luaFactory = [this]() -> ProxyGenerator& { return *this; };
    LuaBinding(state)
        .beginModule("cg")
        /**/.beginModule("ModelType")
        /**//**/.addConstant("Cylinder8d8s", static_cast<uint>(ModelType::Cylinder8d8s))
        /**/.endModule()
        /**/.beginClass<ProxyGenerator>("ProxyGenerator")
        /**//**/.addFunction("resizeOutput", &ProxyGenerator::luaResizeOutput)
        /**//**/.addFunction("setSourceModel", &ProxyGenerator::luaSetSourceModel)
        /**//**/.addFunction("setModelType", &ProxyGenerator::luaSetModelType)
        /**//**/.addFunction("setColorOutput", &ProxyGenerator::luaSetColorOutput)
        /**//**/.addFunction("setNormalOutput", &ProxyGenerator::luaSetNormalOutput)
        /**//**/.addFunction("generate", &ProxyGenerator::luaGenerate)
        /**/.endClass()
        /**/.addFunction("proxy", luaFactory)
        .endModule();
}
void ProxyGenerator::luaResizeOutput(const uint width, const uint height)
{
    resizeOutput(width, height);
}
void ProxyGenerator::luaSetSourceModel(Asset::Ptr model)
{
    LS_THROW(model->type() == AssetType::Model,
             ArgumentException, "Invalid asset");
    setSourceModel(Asset::cast<ModelAsset>(model));
}
void ProxyGenerator::luaSetModelType(const uint modelType)
{
    LS_THROW(modelType > 0 && modelType < NumModelTypes,
             ArgumentException, "Bad model type");
    setModelType(static_cast<ModelType>(modelType));
}
void ProxyGenerator::luaSetColorOutput(const uint compression,
                                       Asset::Ptr mipmapEffect, const string& mipmapProgram,
                                       const vector<float>& mipmapParameters,
                                       Asset::Ptr healEffect, const string& healProgram,
                                       const vector<float>& healParameters)
{
    LS_THROW(compression < static_cast<uint>(Compression::COUNT),
             ArgumentException, "Bad compression format");
    LS_THROW(mipmapEffect->type() == AssetType::Effect,
             ArgumentException, "Mipmap effect must be an effect");
    LS_THROW(healEffect->type() == AssetType::Effect,
             ArgumentException, "Heal effect must be an effect");
    setColorOutput(static_cast<Compression>(compression),
                   Asset::cast<EffectAsset>(mipmapEffect), mipmapProgram, mipmapParameters,
                   Asset::cast<EffectAsset>(healEffect), healProgram,
                   healParameters);
}
void ProxyGenerator::luaSetNormalOutput(const uint compression,
                                        Asset::Ptr mipmapEffect, const string& mipmapProgram,
                                        const vector<float>& mipmapParameters,
                                        Asset::Ptr healEffect, const string& healProgram,
                                        const vector<float>& healParameters)
{
    LS_THROW(compression < static_cast<uint>(Compression::COUNT),
             ArgumentException, "Bad compression format");
    LS_THROW(mipmapEffect->type() == AssetType::Effect,
             ArgumentException, "Mipmap effect must be an effect");
    LS_THROW(healEffect->type() == AssetType::Effect,
             ArgumentException, "Heal effect must be an effect");
    setNormalOutput(static_cast<Compression>(compression),
                    Asset::cast<EffectAsset>(mipmapEffect), mipmapProgram,
                    mipmapParameters,
                    Asset::cast<EffectAsset>(healEffect), healProgram,
                    healParameters);
}
void ProxyGenerator::luaGenerate(Asset::Ptr meshAsset,
                                 Asset::Ptr colorTextureAsset, Asset::Ptr normalTextureAsset)
{
    LS_THROW(meshAsset->type() == AssetType::Mesh,
             ArgumentException, "Mesh must be a mesh");
    LS_THROW(colorTextureAsset->type() == AssetType::Texture,
             ArgumentException, "Mesh must be a texture");
    LS_THROW(normalTextureAsset->type() == AssetType::Texture,
             ArgumentException, "Mesh must be a texture");
    generate(Asset::cast<MeshAsset>(meshAsset),
             Asset::cast<TextureAsset>(colorTextureAsset),
             Asset::cast<TextureAsset>(normalTextureAsset));
}
