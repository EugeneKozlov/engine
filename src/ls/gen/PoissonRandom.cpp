#include "pch.h"
#include "ls/gen/PoissonRandom.h"

using namespace ls;
// Point Cloud
PointCloud2D ls::samplePointCloud(const PointCloud2DNorm& cloud,
                                  const double2& begin, const double2& end,
                                  const float scale)
{
    PointCloud2D dest;
    const double2 from = floor(begin / scale);
    const double2 to = ceil(end / scale);
    for (double nx = from.x; nx <= to.x; ++nx)
    {
        for (double ny = from.y; ny <= to.y; ++ny)
        {
            const double2 tileBegin = double2(nx, ny);
            const double2 tileEnd = double2(nx + 1, ny + 1);
            const double2 clipBegin = max(begin / scale, min(end / scale, tileBegin));
            const double2 clipEnd = max(begin / scale, min(end / scale, tileEnd));
            for (const float2& sourcePoint : cloud)
            {
                const double2 point = lerp(tileBegin, tileEnd, static_cast<double2>(sourcePoint));
                if (!insideInc(point, clipBegin, clipEnd))
                    continue;

                dest.push_back(point * scale);
            }
        }
    }
    return dest;
}


// Ctor
PoissonRandom::PoissonRandom(const uint seed)
    : m_generator(seed)
    , m_distr(0.0, 1.0)
{
}
PoissonRandom::~PoissonRandom()
{
}


// Generator
struct PoissonRandom::Cell
    : public float2
{
    bool isValid = false;
};
float PoissonRandom::randomFloat()
{
    return static_cast<float>(m_distr(m_generator));
}
int2 PoissonRandom::imageToGrid(const float2& p, const float cellSize)
{
    return int2(sint(p.x / cellSize), sint(p.y / cellSize));

}
struct PoissonRandom::Grid
{
    Grid(const int w, const int h, const float cellSize)
        : m_width(w)
        , m_height(h)
        , m_cellSize(cellSize)
    {
        m_grid.resize(m_height);

        for (auto i = m_grid.begin(); i != m_grid.end(); i++)
        {
            i->resize(m_width);
        }
    }
    void insert(const float2& p)
    {
        int2 g = imageToGrid(p, m_cellSize);
        Cell& c = m_grid[g.x][g.y];

        c.x = p.x;
        c.y = p.y;
        c.isValid = true;
    }
    bool isInNeighbourhood(const float2& point, const float minDist, const float cellSize)
    {
        int2 g = imageToGrid(point, cellSize);

        // Number of adjucent cells to look for neighbour points
        const int d = 5;

        // Scan the neighbourhood of the point in the grid
        for (int i = g.x - d; i < g.x + d; i++)
        {
            for (int j = g.y - d; j < g.y + d; j++)
            {
                // Wrap cells
                int wi = i;
                int wj = j;
                while (wi < 0) wi += m_width;
                while (wj < 0) wj += m_height;
                wi %= m_width;
                wj %= m_height;

                // Test wrapped distances
                Cell p = m_grid[wi][wj];
                const float dist = min(min(distance(p, point),
                                           distance(p + float2(1, 0), point)),
                                       min(distance(p + float2(0, 1), point),
                                           distance(p + float2(1, 1), point)));

                if (p.isValid && dist < minDist)
                {
                    return true;
                }
            }
        }
        return false;
    }

private:
    int m_width;
    int m_height;
    float m_cellSize;

    vector<vector<Cell>> m_grid;
};
float2 PoissonRandom::popRandom(PointCloud2DNorm& points)
{
    std::uniform_int_distribution<> dis(0, points.size() - 1);
    const int idx = dis(m_generator);
    const float2 p = points[idx];
    points.erase(points.begin() + idx);
    return p;
}
float2 PoissonRandom::generateRandomPointAround(const float2& p, const float minDist)
{
    // Start with non-uniform distribution
    const float r1 = randomFloat();
    const float r2 = randomFloat();

    // Radius should be between MinDist and 2 * MinDist
    const float radius = minDist * (r1 + 1.0f);

    // Random angle
    const float angle = 2 * 3.141592653589f * r2;

    // The new point is generated around the point (x, y)
    const float x = p.x + radius * cos(angle);
    const float y = p.y + radius * sin(angle);

    return float2(x, y);
}


// Output
PointCloud2DNorm PoissonRandom::generate(const float minDist, const uint newPointsCount, const uint numPoints)
{
    PointCloud2DNorm samplePoints;
    PointCloud2DNorm processList;

    // Create the grid
    const float cellSize = minDist / sqrt(2.0f);

    const int gridW = static_cast<int>(ceil(1.0f / cellSize));
    const int gridH = static_cast<int>(ceil(1.0f / cellSize));

    Grid grid(gridW, gridH, cellSize);

    float2 firstPoint;
    firstPoint.x = randomFloat();
    firstPoint.y = randomFloat();

    // Update containers
    processList.push_back(firstPoint);
    samplePoints.push_back(firstPoint);
    grid.insert(firstPoint);

    // Generate new points for each point in the queue
    while (!processList.empty() && samplePoints.size() < numPoints)
    {
        const float2 point = popRandom(processList);

        for (uint i = 0; i < newPointsCount; i++)
        {
            const float2 newPoint = generateRandomPointAround(point, minDist);

            // Test
            const bool fits = newPoint.x >= 0 && newPoint.y >= 0 && newPoint.x <= 1 && newPoint.y <= 1;

            if (fits && !grid.isInNeighbourhood(newPoint, minDist, cellSize))
            {
                processList.push_back(newPoint);
                samplePoints.push_back(newPoint);
                grid.insert(newPoint);
                continue;
            }
        }
    }
    return samplePoints;
}
