/// @file ls/gen/texture/TextureGenerator.h
/// @brief Texture Generator
#pragma once

#include "ls/common.h"
#include "ls/asset/EffectAsset.h"
#include "ls/asset/TextureAsset.h"
#include "ls/gapi/common.h"
#include "ls/lua/BuiltinModule.h"

namespace ls
{
    class AssetManager;
    class Renderer;
    class StreamInterface;

    /// @addtogroup LsGen
    /// @{

    /// @brief Texture Generator
    class TextureGenerator
        : Noncopyable
        , public BuiltinModule
    {
    public:
        /// @brief Texture type
        enum class TextureType
        {
            /// @brief Regular R8G8B8A8 texture
            Regular,
            /// @brief Depth texture (for internal use only!)
            Depth,
            /// @brief Counter
            COUNT
        };
        /// @brief Number of texture types
        static const uint NumTextureTypes = static_cast<uint>(TextureType::COUNT);
        /// @brief Parameter buffer max size
        static const uint BufferSize = 32;
        /// @brief Parameter Array
        using ParameterArray = array<float4, BufferSize>;
        /// @brief Compress vector to parameter array
        static array<float4, BufferSize> compressParameters(const vector<float>& src);
        /// @brief Parameter Buffer Name
        static const string ParameterBufferName;
        /// @brief Size Buffer Name
        static const string SizeBufferName;
    public:
        /// @brief Ctor
        TextureGenerator(Renderer& renderer, const bool disableCompression);
        /// @brief Dtor
        ~TextureGenerator();

        /// @name Setup Generator
        /// @note Invalidated after @a save
        /// @{

        /// @brief Add output resource
        /// @throw ArgumentException if texture type and compression are incompatible
        /// @throw ArgumentException if too many outputs
        /// @throw LogicException if called between begin/save calls
        /// @note @a generate call resets output resources
        void addOutputResource(const TextureType type, const Compression compression);
        /// @brief Add output resource
        /// @throw ArgumentException if texture type and compression are incompatible
        /// @throw ArgumentException if too many outputs
        /// @throw ArgumentException if program name is empty
        /// @throw ArgumentException if too many parameters
        /// @throw LogicException if called between begin/save calls
        /// @note @a generate call resets output resources
        void addOutputMipmapResource(const TextureType type, const Compression compression,
                                     shared_ptr<EffectAsset> mipmapEffect, const string& mipmapProgram,
                                     const vector<float>& mipmapParameters);
        /// @brief Add post-process event
        /// @note @a generate call resets post-process requests
        /// @throw ArgumentException if output index is too large
        /// @throw ArgumentException if program name is empty
        /// @throw ArgumentException if too many parameters
        /// @throw LogicException if called between begin/save calls
        void addPostProcess(const uint outputIndex,
                            shared_ptr<EffectAsset> effect, const string& program,
                            const vector<float>& parameters);
        /// @}

        /// @name Setup Pass
        /// @{

        /// @brief Begin texture generation
        /// @throw ArgumentException if dimensions are invalid
        /// @throw LogicException if wrong order of begin/save calls
        void begin(const uint width, const uint height);
        /// @brief Add input resource
        /// @note Invalidated after @a generate call
        void addInputResource(const string& name, shared_ptr<TextureAsset> asset);
        /// @brief Set main generator info
        /// @throw ArgumentException if program name is empty
        /// @throw ArgumentException if too many parameters
        /// @note Does @b not invalidated after @a generate call
        void setGenerator(shared_ptr<EffectAsset> mainEffect, const string& mainProgram,
                          const vector<float>& mainParameters);
        /// @brief Generate resource
        /// @throw ArgumentException if cannot find main program
        /// @throw ArgumentException if cannot find mipmap program
        /// @throw ArgumentException if cannot find post-process program
        /// @throw ArgumentException if cannot find input resource
        /// @throw ArgumentException if no output is bound
        void generate(const uint numInstances);
        /// @brief Save generated resources
        /// @throw ArgumentException if null assets
        /// @throw LogicException if wrong order of begin/save calls
        void save(const vector<shared_ptr<TextureAsset>>& assets);
        /// @brief Drop cached data
        void drop();
        /// @}

        /// @name Utility
        /// @{

        /// @brief Allocate texture
        Texture2DHandle allocateTexture(const uint width, const uint height,
                                        const TextureType type,
                                        const bool hasMipmaps);
        /// @brief Deallocate texture
        void deallocateTexture(Texture2DHandle texture);
        /// @brief Generate mipmaps for texture
        void generateMimpaps(Texture2DHandle texture, 
                             const TextureType type,
                             shared_ptr<EffectAsset> mipmapEffectAsset, 
                             const string mipmapProgramName,
                             const ParameterArray& parameters);
        /// @brief Heal normal map in RGBA8 format
        void processTexture(Texture2DHandle texture,
                            const TextureType type,
                            shared_ptr<EffectAsset> effectAsset,
                            const string programName,
                            const string& sizeBufferName = "",
                            const string& parameterBufferName = "",
                            const ParameterArray& parameters = {});
        /// @}

        /// @brief Is texture compression disabled?
        bool isCompressionDisabled() const;
    private:
        /// @name Implement BuiltinModule
        /// @{
        virtual void exportToLua(LuaState& state) override;

        void luaAddOutputResource(const uint type, const uint compression);
        void luaAddOutputMipmapResource(const uint type, const uint compression,
                                        Asset::Ptr mipmapEffect, const string& mipmapProgram,
                                        const vector<float>& mipmapParameters);
        void luaAddPostProcess(const uint outputIndex,
                               Asset::Ptr effect, const string& program,
                               const vector<float>& parameters);

        void luaBegin(const uint width, const uint height);
        void luaAddInputResource(const string& name, Asset::Ptr asset);
        void luaSetGenerator(Asset::Ptr mainEffect, const string& mainProgram,
                             const vector<float>& mainParameters);
        void luaGenerate(const uint numInstances);
        void luaSave(const vector<Asset::Ptr>& assets);
        /// @}

    private:
        Renderer& m_renderer;
        const bool m_disableCompression;

        /// @name Output info
        /// @note Invalidated after @a save
        /// @{
        uint m_outputWidth = 0;
        uint m_outputHeight = 0;
        uint m_numOutputs = 0;
        vector<TextureType> m_outputTypes;
        vector<Compression> m_outputCompression;
        vector<ubyte> m_outputMipmaps;
        vector<shared_ptr<EffectAsset>> m_outputMipmapEffects;
        vector<string> m_outputMipmapPrograms;
        vector<ParameterArray> m_outputMipmapParameters;

        struct PostProcessDesc
        {
            uint outputIndex = 0;
            shared_ptr<EffectAsset> effect;
            string program;
            ParameterArray parameters;
        };
        vector<PostProcessDesc> m_outputPostProcessRequests;
        /// @}

        /// @name Generation info
        /// @note May vary for different passes
        /// @{
        shared_ptr<EffectAsset> m_mainEffect = nullptr;
        string m_mainProgram = "";
        ParameterArray m_mainParameters;
        /// @}

        /// @name Input resources
        /// @note Invalidated after each pass
        /// @{
        hamap<string, shared_ptr<TextureAsset>> m_inputResources;
        /// @}

        /// @name Generator internals
        /// @{
        RenderTargetHandle m_renderTarget;
        /// @}

        /// @name Internal Texture Cache
        /// @{
        struct TextureKey
        {
            TextureKey() = default;
            TextureKey(const uint width, const uint height, 
                       const TextureType type, const bool hasMipmaps);
            bool operator < (const TextureKey& another) const;
            uint width = 0;
            uint height = 0;
            TextureType type = TextureType::Regular;
            bool hasMipmaps = false;
        };
        mumap<TextureKey, Texture2DHandle> m_cachedTextures;
        hamap<Texture2DHandle, TextureKey> m_usedTextures;
        /// @}
    };
    /// @}
}

