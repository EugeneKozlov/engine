#include "pch.h"
#include "ls/gen/texture/TextureGenerator.h"

#include "ls/asset/EffectAsset.h"
#include "ls/asset/TextureAsset.h"
#include "ls/gapi/Renderer.h"
#include "ls/io/StreamInterface.h"


using namespace ls;
// Helpers
namespace
{
    /// @brief Generate mipmaps
    void generateMipmaps(Renderer& renderer,
                         uint width, uint height,
                         BufferHandle cbSize,
                         Texture2DHandle destTexture,
                         Texture2DHandle buffer1, Texture2DHandle buffer2)
    {
        // Compute number of mipmaps
        const uint numMips = max(log2u(width), log2u(height));

        // Init render targets
        RenderTargetHandle renderTarget1 = renderer.createRenderTarget(buffer1, nullptr);
        RenderTargetHandle renderTarget2 = renderer.createRenderTarget(buffer2, nullptr);

        // Prepare constants
        uint mipWidth = width / 2;
        uint mipHeight = height / 2;

        float4 size = float4(static_cast<float>(mipWidth), static_cast<float>(mipHeight), 0, 0) * 2.0f;
        renderer.writeBuffer(cbSize, size);

        // Drop bound resources
        renderer.bindResources(ResourceBucket());

        // Draw first
        renderer.bindRenderTargetRegion(renderTarget1, 0, 0, mipWidth, mipHeight);
        renderer.bindTexture(0, destTexture, ShaderType::Pixel);
        renderer.draw(0, 4);

        renderer.copyTexture2D(destTexture, buffer1, 0, 0,
                               1, 0, 0, 0, 0, 0, mipWidth, mipHeight);

        // Draw others
        for (uint mipLevel = 2; mipLevel <= numMips; ++mipLevel)
        {
            if (mipWidth > 1)
                mipWidth >>= 1;
            if (mipHeight > 1)
                mipHeight >>= 1;

            // Update constant
            size = float4(static_cast<float>(mipWidth), static_cast<float>(mipHeight), 0, 0) * 2.0f;
            renderer.writeBuffer(cbSize, size);

            // Set previous
            renderer.bindTexture(0, nullptr, ShaderType::Pixel);
            renderer.bindRenderTargetRegion(renderTarget2, 0, 0, mipWidth, mipHeight);
            renderer.bindTexture(0, buffer1, ShaderType::Pixel);

            // Draw and save
            renderer.draw(0, 4);
            renderer.copyTexture2D(destTexture, buffer2, 0, 0,
                                   mipLevel, 0, 0, 0, 0, 0, mipWidth, mipHeight);

            // Swap
            swap(renderTarget1, renderTarget2);
            swap(buffer1, buffer2);
        }

        // Clean up
        renderer.destroyRenderTarget(renderTarget1);
        renderer.destroyRenderTarget(renderTarget2);
    }
    Format internalTextureFormat(const TextureGenerator::TextureType type)
    {
        switch (type)
        {
        case TextureGenerator::TextureType::Regular:
            return Format::R8G8B8A8_UNORM;
        case TextureGenerator::TextureType::Depth:
            return Format::D32_FLOAT;
        default:
            return Format::Unknown;
        }
    }
}
TextureGenerator::TextureKey::TextureKey(const uint width, const uint height,
                                         const TextureType type,
                                         const bool hasMipmaps)
    : width(width)
    , height(height)
    , type(type)
{

}
bool TextureGenerator::TextureKey::operator < (const TextureKey& another) const
{
    if (width != another.width)
        return width < another.width;
    if (height != another.height)
        return height < another.height;
    if (type != another.type)
        return type < another.type;
    return hasMipmaps < another.hasMipmaps;
}


// Main
const string TextureGenerator::ParameterBufferName = "Parameters";
const string TextureGenerator::SizeBufferName = "Size";
TextureGenerator::TextureGenerator(Renderer& renderer, const bool disableCompression)
    : m_renderer(renderer)
    , m_disableCompression(disableCompression)
{
}
TextureGenerator::~TextureGenerator()
{
    drop();
}
TextureGenerator::ParameterArray TextureGenerator::compressParameters(const vector<float>& src)
{
    ParameterArray buffer;
    buffer.fill(float4(0.0f));
    std::copy_n(src.begin(), src.size(), &buffer.front()[0]);
    return buffer;
}


// Setup Generator
void TextureGenerator::addOutputResource(const TextureType type,
                                         const Compression compression)
{
    // Check order
    LS_THROW(!m_renderTarget, LogicException, "begin is already called");

    // Check
    LS_THROW(compression == Compression::No || type == TextureType::Regular,
             ArgumentException, "Only regular texture can be compressed");
    LS_THROW(m_numOutputs < 8,
             ArgumentException, "Too many outputs");

    // Set
    m_outputTypes.push_back(type);
    m_outputCompression.push_back(compression);
    m_outputMipmaps.push_back(false);
    m_outputMipmapEffects.push_back(nullptr);
    m_outputMipmapPrograms.push_back("");
    m_outputMipmapParameters.emplace_back();

    ++m_numOutputs;
}
void TextureGenerator::addOutputMipmapResource(const TextureType type,
                                               const Compression compression,
                                               shared_ptr<EffectAsset> mipmapEffect,
                                               const string& mipmapProgram,
                                               const vector<float>& mipmapParameters)
{
    // Check order
    LS_THROW(!m_renderTarget, LogicException, "begin is already called");

    // Check
    LS_THROW(compression == Compression::No || type == TextureType::Regular,
             ArgumentException, "Only regular texture can be compressed");
    LS_THROW(m_numOutputs < 8,
             ArgumentException, "Too many outputs");
    LS_THROW(!mipmapProgram.empty(),
             ArgumentException, "Empty program name");
    LS_THROW(mipmapParameters.size() < BufferSize * 4,
             ArgumentException, "Too many parameters");

    // Set
    m_outputTypes.push_back(type);
    m_outputCompression.push_back(compression);
    m_outputMipmaps.push_back(true);
    m_outputMipmapEffects.push_back(mipmapEffect);
    m_outputMipmapPrograms.push_back(mipmapProgram);
    m_outputMipmapParameters.push_back(compressParameters(mipmapParameters));

    ++m_numOutputs;
}
void TextureGenerator::addPostProcess(const uint outputIndex,
                                      shared_ptr<EffectAsset> effect,
                                      const string& program, const vector<float>& parameters)
{
    // Check order
    LS_THROW(!m_renderTarget, LogicException, "begin is already called");

    // Check param
    LS_THROW(outputIndex < m_numOutputs,
             ArgumentException, "Bad output index");
    LS_THROW(!program.empty(),
             ArgumentException, "Empty program name");
    LS_THROW(parameters.size() < BufferSize * 4,
             ArgumentException, "Too many parameters");

    PostProcessDesc desc;
    desc.outputIndex = outputIndex;
    desc.effect = effect;
    desc.program = program;
    desc.parameters = compressParameters(parameters);
    m_outputPostProcessRequests.push_back(desc);
}


// Cache
Texture2DHandle TextureGenerator::allocateTexture(const uint width, const uint height,
                                                  const TextureType type,
                                                  const bool hasMipmaps)
{
    TextureKey key;
    key.width = width;
    key.height = height;
    key.type = type;
    key.hasMipmaps = hasMipmaps;

    // Find texture
    auto iterTexture = m_cachedTextures.find(key);
    Texture2DHandle texture;
    if (iterTexture != m_cachedTextures.end())
    {
        texture = iterTexture->second;
        m_cachedTextures.erase(iterTexture);
    }

    // Make flags
    auto flags = ResourceBinding::ShaderResource | ResourceBinding::Readable;
    flags |= (type != TextureGenerator::TextureType::Depth)
        ? ResourceBinding::RenderTarget
        : ResourceBinding::DepthStencil;

    // Create texture
    if (texture == nullptr)
    {
        texture = m_renderer
            .createTexture2D(width, height, hasMipmaps, internalTextureFormat(type),
                             ResourceUsage::Static, flags);
    }

    // Store
    m_usedTextures.emplace(texture, key);
    return texture;
}
void TextureGenerator::deallocateTexture(Texture2DHandle texture)
{
    const TextureKey* key = findOrDefault(m_usedTextures, texture);
    debug_assert(key);
    m_cachedTextures.emplace(*key, texture);
    m_usedTextures.erase(texture);
}


// Setup Pass
void TextureGenerator::addInputResource(const string& name,
                                        shared_ptr<TextureAsset> asset)
{
    asset->refreshGapi();
    m_inputResources.emplace(name, asset);
}
void TextureGenerator::setGenerator(shared_ptr<EffectAsset> mainEffect,
                                    const string& mainProgram,
                                    const vector<float>& mainParameters)
{
    // Check
    LS_THROW(!mainProgram.empty(),
             ArgumentException, "Empty program name");
    LS_THROW(mainParameters.size() < BufferSize * 4,
             ArgumentException, "Too many parameters");
    LS_THROW(m_numOutputs > 0,
             ArgumentException, "Too few outputs");

    // Set
    m_mainEffect = mainEffect;
    m_mainProgram = mainProgram;
    m_mainParameters = compressParameters(mainParameters);
}


// Generate
void TextureGenerator::begin(const uint width, const uint height)
{
    // Check order
    LS_THROW(!m_renderTarget, LogicException, "begin is already called");

    // Setup size
    LS_THROW(width > 0 && height > 0,
             ArgumentException, "Too small output texture");
    m_outputWidth = width;
    m_outputHeight = height;

    // Init render targets
    Texture2DHandle depthTexture = allocateTexture(m_outputWidth, m_outputHeight,
                                                   TextureGenerator::TextureType::Depth, false);
    m_renderTarget = m_renderer
        .createRenderTarget(m_numOutputs, depthTexture);
    m_renderer.clearDepthStencil(m_renderTarget, 1.0f, 0);
    for (uint outputIndex = 0; outputIndex < m_numOutputs; ++outputIndex)
    {
        Texture2DHandle outputTexture =
            allocateTexture(m_outputWidth, m_outputHeight,
                            m_outputTypes[outputIndex], !!m_outputMipmaps[outputIndex]);
        m_renderer.attachColorTexture2D(m_renderTarget, outputIndex, outputTexture);
        m_renderer.clearColor(m_renderTarget, outputIndex, 0.0f);
    }
    m_renderer.bindRenderTarget(m_renderTarget);
}
void TextureGenerator::generate(const uint numInstances)
{
    // Find effect
    EffectHandle mainEffect = m_mainEffect->gapiEffect();
    ProgramHandle mainProgram = m_renderer
        .handleProgram(mainEffect, m_mainProgram.c_str());
    LS_THROW(!!mainProgram,
             ArgumentException, "Cannot find main program [", m_mainProgram, "]");

    // Commit constants
    BufferHandle cbMainParams = m_renderer.createUniformBuffer(sizeof(m_mainParameters));
    m_renderer.attachUniformBuffer(mainEffect, cbMainParams, ParameterBufferName.c_str());
    m_renderer.writeBuffer(cbMainParams, m_mainParameters);

    const float2 outputSize = float2(static_cast<float>(m_outputWidth), 
                                     static_cast<float>(m_outputHeight));
    BufferHandle cbMainSize = m_renderer.createUniformBuffer(sizeof(float4));
    m_renderer.attachUniformBuffer(mainEffect, cbMainSize, SizeBufferName.c_str());
    m_renderer.writeBuffer(cbMainSize, float4(outputSize, 0.0f, 0.0f));

    // Commit resources
    StateHandle mainStateHandle = m_renderer.createState(mainProgram);
    m_renderer.bindState(mainStateHandle);
    for (auto& iterResource : m_inputResources)
    {
        // Init resource
        const string& resourceName = iterResource.first;
        TextureAsset& resource = *iterResource.second;
        Texture2DHandle texture = resource.gapiResource();

        // Set resource
        EffectResourceHandle resourceHandle = m_renderer
            .handleResource(mainEffect, resourceName.c_str());
        LS_THROW(!!resourceHandle,
                 ArgumentException, "Unknown input resource [", resourceName, "]");
        m_renderer.bindTexture2D(resourceHandle, texture, ShaderType::Pixel);
    }

    // Render
    m_renderer.drawInstanced(0, 4, 0, numInstances);

    // Clean up states and buffers
    m_renderer.destroyBuffer(cbMainSize);
    m_renderer.destroyBuffer(cbMainParams);
    m_renderer.destroyState(mainStateHandle);

    // Clean up input resources
    m_inputResources.clear();
}
void TextureGenerator::save(const vector<shared_ptr<TextureAsset>>& assets)
{
    // Check order
    LS_THROW(m_renderTarget, LogicException, "begin was not called");

    // Check input
    const size_t nulls = std::count(assets.begin(), assets.end(), nullptr);
    LS_THROW(nulls == 0, ArgumentException, "Asset must not be null");

    // Post-process
    for (PostProcessDesc& desc : m_outputPostProcessRequests)
    {
        const uint idx = desc.outputIndex;
        processTexture(m_renderTarget->colorTextures[idx],
                       m_outputTypes[idx], desc.effect, desc.program,
                       SizeBufferName, ParameterBufferName, desc.parameters);
    }

    // Generate mipmaps
    for (uint outputIndex = 0; outputIndex < m_numOutputs; ++outputIndex)
    {
        if (!m_outputMipmaps[outputIndex])
            continue;

        generateMimpaps(m_renderTarget->colorTextures[outputIndex], m_outputTypes[outputIndex],
                        m_outputMipmapEffects[outputIndex], m_outputMipmapPrograms[outputIndex],
                        m_outputMipmapParameters[outputIndex]);
    }

    // Save textures
    const uint numAssets = min(assets.size(), m_numOutputs);
    for (uint outputIndex = 0; outputIndex < numAssets; ++outputIndex)
    {
        // Read texture from GAPI
        Texture2DHandle textureHandle = m_renderTarget->colorTextures[outputIndex];
        m_renderer.unloadTexture2D(assets[outputIndex]->storage(), textureHandle);

        // Compress texture
        if (!m_disableCompression)
        {
            const Compression compression = m_outputCompression[outputIndex];
            assets[outputIndex]->compress(m_outputCompression[outputIndex]);
        }

        // Clean up textures
        deallocateTexture(textureHandle);
    }

    // Clean up depth and RT
    deallocateTexture(m_renderTarget->depthTexture);
    m_renderer.destroyRenderTarget(m_renderTarget);
    m_renderTarget = nullptr;

    // Invalidate outputs
    m_numOutputs = 0;
    m_outputTypes.clear();
    m_outputCompression.clear();
    m_outputMipmaps.clear();
    m_outputMipmapEffects.clear();
    m_outputMipmapPrograms.clear();
    m_outputMipmapParameters.clear();
    m_outputPostProcessRequests.clear();
}


// Utility
void TextureGenerator::generateMimpaps(Texture2DHandle texture,
                                       const TextureType type,
                                       shared_ptr<EffectAsset> mipmapEffectAsset,
                                       const string mipmapProgramName,
                                       const ParameterArray& parameters)
{
    // Find mipmap effect
    EffectHandle mipmapEffect = mipmapEffectAsset->gapiEffect();
    ProgramHandle mipmapProgram = m_renderer
        .handleProgram(mipmapEffect, mipmapProgramName.c_str());
    LS_THROW(!!mipmapProgram,
             ArgumentException, "Cannot find mipmap program [", mipmapProgramName, "]");

    // Commit constants
    BufferHandle cbMipmapParams = m_renderer.createUniformBuffer(sizeof(float4) * BufferSize);
    m_renderer.attachUniformBuffer(mipmapEffect, cbMipmapParams, ParameterBufferName.c_str());
    m_renderer.writeBuffer(cbMipmapParams, parameters);
    BufferHandle cbMipmapSize = m_renderer.createUniformBuffer(sizeof(float4));
    m_renderer.attachUniformBuffer(mipmapEffect, cbMipmapSize, SizeBufferName.c_str());

    // Allocate buffers
    Texture2DHandle buffer1
        = allocateTexture(texture->width / 2, texture->height / 2, type, false);
    Texture2DHandle buffer2
        = allocateTexture(texture->width / 2, texture->height / 2, type, false);

    // Init state
    StateHandle mipmapStateHandle = m_renderer.createState(mipmapProgram);
    m_renderer.bindState(mipmapStateHandle);

    // Generate mipmaps
    generateMipmaps(m_renderer, texture->width, texture->height,
                    cbMipmapSize, texture, buffer1, buffer2);

    // Clean up
    deallocateTexture(buffer1);
    deallocateTexture(buffer2);
    m_renderer.destroyBuffer(cbMipmapParams);
    m_renderer.destroyBuffer(cbMipmapSize);
    m_renderer.destroyState(mipmapStateHandle);
}
void TextureGenerator::processTexture(Texture2DHandle texture, const TextureType type,
                                      shared_ptr<EffectAsset> effectAsset, const string programName,
                                      const string& sizeBufferName, const string& parameterBufferName,
                                      const ParameterArray& parameters)
{
    // Find effect
    EffectHandle effect = effectAsset->gapiEffect();
    ProgramHandle program = m_renderer
        .handleProgram(effect, programName.c_str());
    LS_THROW(!!program,
             ArgumentException, "Cannot find program [", programName, "]");

    // Create size buffer
    BufferHandle sizeBuffer = nullptr;
    if (!sizeBufferName.empty())
    {
        sizeBuffer = m_renderer.createUniformBuffer(sizeof(float4));
        m_renderer.writeBuffer(sizeBuffer,
                               float4(static_cast<float>(texture->width),
                                      static_cast<float>(texture->height),
                                      0.0f, 0.0f));
        m_renderer.attachUniformBuffer(effect, sizeBuffer, sizeBufferName.c_str());
    }

    // Create size buffer
    BufferHandle parameterBuffer = nullptr;
    if (!parameterBufferName.empty())
    {
        parameterBuffer = m_renderer.createUniformBuffer(sizeof(float4) * BufferSize);
        m_renderer.writeBuffer(parameterBuffer, parameters);
        m_renderer.attachUniformBuffer(effect, parameterBuffer, parameterBufferName.c_str());
    }

    // Create state
    StateHandle state = m_renderer.createState(program);
    m_renderer.bindState(state);

    // Allocate buffer
    Texture2DHandle source = allocateTexture(texture->width, texture->height, type, false);
    m_renderer.copyTexture2D(source, texture);

    // Init render target
    RenderTargetHandle renderTarget = m_renderer.createRenderTarget(texture, nullptr);
    m_renderer.bindRenderTarget(renderTarget);
    m_renderer.clearColor(renderTarget, 0, float4(0.0f));

    // Render
    m_renderer.bindTexture(0, source, ShaderType::Pixel);
    m_renderer.draw(0, 4);

    // Clean up
    deallocateTexture(source);
    if (!!sizeBuffer)
    {
        m_renderer.destroyBuffer(sizeBuffer);
    }
    if (!!parameterBuffer)
    {
        m_renderer.destroyBuffer(parameterBuffer);
    }
    m_renderer.destroyRenderTarget(renderTarget);
    m_renderer.destroyState(state);
}
void TextureGenerator::drop()
{
    for (auto& iterTexture : m_cachedTextures)
    {
        m_renderer.destroyTexture2D(iterTexture.second);
    }
    m_cachedTextures.clear();
    for (auto& iterTexture : m_usedTextures)
    {
        m_renderer.destroyTexture2D(iterTexture.first);
    }
    m_usedTextures.clear();
}


// Lua
void TextureGenerator::exportToLua(LuaState& state)
{
    function<TextureGenerator&()> luaFactory = [this]() -> TextureGenerator& { return *this; };
    LuaBinding(state)
        .beginModule("cg")
        /**/.beginModule("TextureType")
        /**//**/.addConstant("Regular", static_cast<uint>(TextureType::Regular))
        /**/.endModule()
        /**/.beginModule("Compression")
        /**//**/.addConstant("No", static_cast<uint>(Compression::No))
        /**//**/.addConstant("BC1", static_cast<uint>(Compression::Block1))
        /**//**/.addConstant("BC2", static_cast<uint>(Compression::Block2))
        /**//**/.addConstant("BC3", static_cast<uint>(Compression::Block3))
        /**/.endModule()
        /**/.beginClass<TextureGenerator>("TextureGenerator")
        /**//**/.addFunction("setGenerator", &TextureGenerator::luaSetGenerator)
        /**//**/.addFunction("addInputResource", &TextureGenerator::luaAddInputResource)
        /**//**/.addFunction("addOutputResource", &TextureGenerator::luaAddOutputResource)
        /**//**/.addFunction("addOutputMipmapResource", &TextureGenerator::luaAddOutputMipmapResource)
        /**//**/.addFunction("addPostProcess", &TextureGenerator::luaAddPostProcess)
        /**//**/.addFunction("begin", &TextureGenerator::luaBegin)
        /**//**/.addFunction("generate", &TextureGenerator::luaGenerate)
        /**//**/.addFunction("save", &TextureGenerator::luaSave)
        /**/.endClass()
        /**/.addFunction("texture", luaFactory)
        .endModule();
}
void TextureGenerator::luaAddOutputResource(const uint type,
                                            const uint compression)
{
    LS_THROW(type < NumTextureTypes,
             ArgumentException, "Bad texture type");
    LS_THROW(compression < static_cast<uint>(Compression::COUNT),
             ArgumentException, "Bad compression format");

    addOutputResource(static_cast<TextureType>(type),
                      static_cast<Compression>(compression));
}
void TextureGenerator::luaAddOutputMipmapResource(const uint type,
                                                  const uint compression,
                                                  Asset::Ptr mipmapEffect,
                                                  const string& mipmapProgram,
                                                  const vector<float>& mipmapParameters)
{
    LS_THROW(type < NumTextureTypes,
             ArgumentException, "Bad texture type");
    LS_THROW(compression < static_cast<uint>(Compression::COUNT),
             ArgumentException, "Bad compression format");
    LS_THROW(mipmapEffect->type() == AssetType::Effect,
             ArgumentException, "Mipmap effect must be an effect");
    addOutputMipmapResource(static_cast<TextureType>(type),
                            static_cast<Compression>(compression),
                            Asset::cast<EffectAsset>(mipmapEffect),
                            mipmapProgram, mipmapParameters);
}
void TextureGenerator::luaAddPostProcess(const uint outputIndex,
                                         Asset::Ptr effect, const string& program,
                                         const vector<float>& parameters)
{
    LS_THROW(effect->type() == AssetType::Effect,
             ArgumentException, "Effect must be an effect");
    addPostProcess(outputIndex, Asset::cast<EffectAsset>(effect),
                   program, parameters);
}
void TextureGenerator::luaBegin(const uint width, const uint height)
{
    begin(width, height);
}
void TextureGenerator::luaAddInputResource(const string& name, Asset::Ptr asset)
{
    LS_THROW(asset->type() == AssetType::Texture,
             ArgumentException, "Invalid texture");
    addInputResource(name, Asset::cast<TextureAsset>(asset));
}
void TextureGenerator::luaSetGenerator(Asset::Ptr mainEffect, const string& mainProgram, const vector<float>& mainParameters)
{
    LS_THROW(mainEffect->type() == AssetType::Effect,
             ArgumentException, "Invalid effect");
    setGenerator(Asset::cast<EffectAsset>(mainEffect),
                 mainProgram, mainParameters);
}
void TextureGenerator::luaGenerate(const uint numInstances)
{
    generate(numInstances);
}
void TextureGenerator::luaSave(const vector<Asset::Ptr>& assets)
{
    vector<shared_ptr<TextureAsset>> textureAssets;
    Asset::convert(assets, textureAssets);
    save(textureAssets);
}


// Gets
bool TextureGenerator::isCompressionDisabled() const
{
    return m_disableCompression;
}