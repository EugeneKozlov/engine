#include "pch.h"
#include "ls/gen/tree/TreeGenerator.h"

#include "ls/asset/MeshAsset.h"

using namespace ls;
TreeGenerator::TreeGenerator()
{
}
TreeGenerator::~TreeGenerator()
{
}


// Main
void TreeGenerator::setPrototype(shared_ptr<TreeAsset> prototype)
{
    m_prototype = prototype;
}
void TreeGenerator::addLod(const uint numLengthSegments, const uint numRadialSegments)
{
    m_lodInfos.emplace_back();
    TreeAsset::LodInfo& lodInfo = m_lodInfos.back();

    lodInfo.numLengthSegments = numLengthSegments;
    lodInfo.numRadialSegments = numRadialSegments;
}
void TreeGenerator::generate(vector<shared_ptr<MeshAsset>>& assets)
{
    LS_THROW(m_prototype, ArgumentException, "Null prototype");

    // Generate
    m_prototype->generateMeshes(m_lodInfos, assets);
    m_lodInfos.clear();
}


// Lua
void TreeGenerator::exportToLua(LuaState& state)
{
    function<TreeGenerator&()> luaFactory = [this]() -> TreeGenerator& { return *this; };
    LuaBinding(state)
        .beginModule("cg")
        /**/.beginClass<TreeGenerator>("TreeGenerator")
        /**//**/.addFunction("setPrototype", &TreeGenerator::luaSetPrototype)
        /**//**/.addFunction("addLod", &TreeGenerator::luaAddLod)
        /**//**/.addFunction("generate", &TreeGenerator::luaGenerate)
        /**/.endClass()
        /**/.addFunction("tree", luaFactory)
        .endModule();
}
void TreeGenerator::luaSetPrototype(Asset::Ptr prototype)
{
    LS_THROW(prototype->type() == AssetType::Tree,
             ArgumentException, "Invalid asset");
    setPrototype(Asset::cast<TreeAsset>(prototype));
}
void TreeGenerator::luaAddLod(const uint numLengthSegments, const uint numRadialSegments)
{
    addLod(numLengthSegments, numRadialSegments);
}
void TreeGenerator::luaGenerate(const vector<Asset::Ptr>& assets)
{
    vector<shared_ptr<MeshAsset>> meshAssets;
    Asset::convert(assets, meshAssets);

    generate(meshAssets);
}
