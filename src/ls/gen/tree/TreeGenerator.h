/// @file ls/gen/tree/TreeGenerator.h
/// @brief Tree Generator
#pragma once

#include "ls/common.h"
#include "ls/asset/TreeAsset.h"
#include "ls/lua/BuiltinModule.h"

namespace ls
{
    class UniformRandom;
    class MeshAsset;

    /// @addtogroup LsGen
    /// @{

    /// @brief Tree Generator
    class TreeGenerator
        : public BuiltinModule
    {
    public:
        /// @brief Ctor
        TreeGenerator();
        /// @brief Dtor
        ~TreeGenerator();

        /// @brief Set prototype
        void setPrototype(shared_ptr<TreeAsset> prototype);
        /// @brief Add LOD to generate
        void addLod(const uint numLengthSegments, const uint numRadialSegments);
        /// @brief Generate geometry
        /// @throw ArgumentException if asset is not a mesh
        /// @throw ArgumentException if prototype is null
        void generate(vector<shared_ptr<MeshAsset>>& assets);
    public:
        /// @name Implement BuiltinModule
        /// @{
        void luaSetPrototype(Asset::Ptr prototype);
        void luaAddLod(const uint numLengthSegments, const uint numRadialSegments);
        void luaGenerate(const vector<Asset::Ptr>& assets);
        virtual void exportToLua(LuaState& state) override;
        /// @}
    private:
        vector<TreeAsset::LodInfo> m_lodInfos;
        shared_ptr<TreeAsset> m_prototype;
    };
    /// @}
}

