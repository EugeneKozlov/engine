/// @file ls/gen/PoissonRandom.h
/// @brief Poisson random generator
#pragma once

#include "ls/common.h"
#include <random>

namespace ls
{
    /// @addtogroup LsGen
    /// @{

    /// @brief Point Cloud 2D
    using PointCloud2D = vector<double2>;
    /// @brief Point Cloud 2D (normalized)
    using PointCloud2DNorm = vector<float2>;
    /// @brief Sample Point Cloud
    /// @pre @a cloud should be normalized to [0, 1] range
    PointCloud2D samplePointCloud(const PointCloud2DNorm& cloud,
                                  const double2& begin, const double2& end,
                                  const float scale);
    /// @brief Poisson random generator
    class PoissonRandom
    {
    public:
        /// @brief Ctor
        PoissonRandom(const uint seed);
        /// @brief Dtor
        ~PoissonRandom();
        /// @brief Generate
        PointCloud2DNorm generate(const float minDist, const uint newPointsCount, const uint numPoints);
    private:
        struct Cell;
        struct Grid;
        float randomFloat();
        static int2 imageToGrid(const float2& p, const float cellSize);
        float2 popRandom(PointCloud2DNorm& points);
        float2 generateRandomPointAround(const float2& p, const float minDist);
    private:
        std::mt19937 m_generator;
        std::uniform_real_distribution<> m_distr;
    };
    /// @}
}

