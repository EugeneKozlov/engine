/// @file ls/gen/utility/geometry.h
/// @brief Geometry generation utilities
#pragma once

#include "ls/common.h"

namespace ls
{
    /// @addtogroup LsGen
    /// @{

    /// @brief Generate AABB
    template <class Vertex>
    AABB computeAabb(Vertex vertices[], const uint numVertices)
    {
        debug_assert(numVertices > 0);
        AABB box = static_cast<double3>(vertices[0].pos);
        for (uint i = 1; i < numVertices; ++i)
            box += static_cast<double3>(vertices[i].pos);
        return box;
    }
    /// @brief Generate triangle normals
    template <class Vertex, class Index>
    void computeNormalSpace(Vertex vertices[], const uint numVertices,
                            const Index indices[], const uint numTriangles)
    {
        // Compute
        for (uint i = 0; i < numTriangles; ++i)
        {
            const Index a1 = indices[3 * i + 0];
            const Index a2 = indices[3 * i + 1];
            const Index a3 = indices[3 * i + 2];

            const float3 pos1 = vertices[a1].pos;
            const float3 pos2 = vertices[a2].pos;
            const float3 pos3 = vertices[a3].pos;
            const float3 normal = normalize(cross(pos2 - pos1, pos3 - pos1));

            vertices[a1].normal += normal;
            vertices[a2].normal += normal;
            vertices[a3].normal += normal;
        }
    }
    /// @brief Normalize normal space
    template <class Vertex>
    void normalizeNormalSpace(Vertex vertices[], const uint numVertices)
    {
        for (uint idx = 0; idx < numVertices; ++idx)
        {
            vertices[idx].normal = normalize(vertices[idx].normal);
        }
    }
    /// @brief Generate triangle TBN
    template <class Vertex>
    void computeTangentBinormal(const Vertex& v0, const Vertex& v1, const Vertex& v2, 
                                float3& tangent, float3& binormal)
    {
        float3 vector1 = v1.pos - v0.pos;
        float3 vector2 = v2.pos - v0.pos;

        float2 uv1 = v1.uv - v0.uv;
        float2 uv2 = v2.uv - v0.uv;

        float cp = uv1.x * uv2.y - uv2.x * uv1.y;
        if (almostEqual(cp, 0.0f))
        {
            tangent = float3(0.0f);
            binormal = float3(0.0f);
            return;
        }
        float den = 1.0f / cp;

        tangent.x = (uv2.y * vector1.x - uv1.y * vector2.x) * den;
        tangent.y = (uv2.y * vector1.y - uv1.y * vector2.y) * den;
        tangent.z = (uv2.y * vector1.z - uv1.y * vector2.z) * den;

        binormal.x = (uv1.x * vector2.x - uv2.x * vector1.x) * den;
        binormal.y = (uv1.x * vector2.y - uv2.x * vector1.y) * den;
        binormal.z = (uv1.x * vector2.z - uv2.x * vector1.z) * den;
    }
    /// @brief Generate TBN space
    template <class Vertex, class Index>
    void computeTangentSpace(Vertex vertices[], const uint numVertices,
                             const Index indices[], const uint numTriangles)
    {
        // Compute
        for (uint i = 0; i < numTriangles; ++i)
        {
            Index a1 = indices[3 * i + 0];
            Index a2 = indices[3 * i + 1];
            Index a3 = indices[3 * i + 2];
            float3 tangent, binormal;
            computeTangentBinormal(vertices[a1], vertices[a2], vertices[a3],
                                   tangent, binormal);

            vertices[a1].tangent += tangent;
            vertices[a2].tangent += tangent;
            vertices[a3].tangent += tangent;
            vertices[a1].binormal += binormal;
            vertices[a2].binormal += binormal;
            vertices[a3].binormal += binormal;
        }
        // Normalize
        for (uint i = 0; i < numVertices; ++i)
        {
            vertices[i].tangent = normalize(vertices[i].tangent);
            vertices[i].binormal = normalize(vertices[i].binormal);
        }
    }
    /// @brief Append quad indices
    ///
    /// Orientation:
    /// @code
    ///     z
    /// y   /_____ 
    /// |  /2   3/
    /// | /     /
    /// |/_____/__x
    ///  0   1
    ///  
    /// ^ is normal
    /// v is flipped
    /// @endcode
    template <class Index>
    void appendQuadIndices(vector<Index>& indices, const Index base,
                           const Index v0, const Index v1, const Index v2, const Index v3,
                           const bool flipped = false)
    {
        if (!flipped)
        {
            indices.push_back(static_cast<Index>(base + v0));
            indices.push_back(static_cast<Index>(base + v2));
            indices.push_back(static_cast<Index>(base + v3));
            indices.push_back(static_cast<Index>(base + v0));
            indices.push_back(static_cast<Index>(base + v3));
            indices.push_back(static_cast<Index>(base + v1));
        }
        else
        {
            indices.push_back(static_cast<Index>(base + v0));
            indices.push_back(static_cast<Index>(base + v3));
            indices.push_back(static_cast<Index>(base + v2));
            indices.push_back(static_cast<Index>(base + v0));
            indices.push_back(static_cast<Index>(base + v1));
            indices.push_back(static_cast<Index>(base + v3));
        }
    }
    /// @brief Append quad to vertex stream.
    /// @see appendQuadIndices
    template <class Vertex, class Index, class Position, class Uv>
    void appendQuad(vector<Vertex>& vertices, vector<Index>& indices,
                    const Vertex& v0, const Vertex& v1, const Vertex& v2, const Vertex& v3,
                    bool flipped = false)
    {
        const uint base = vertices.size();

        // Append vertices
        vertices.push_back(v0);
        vertices.push_back(v1);
        vertices.push_back(v2);
        vertices.push_back(v3);

        // Append indices
        appendQuadIndices<Index>(indices, base, 0, 1, 2, 3, flipped);
    }
    /// @brief Quad interpolation.
    /// @note X is from v0 to v1 and from v2 to v3
    /// @note Y is from v0 to v2 and from v1 to v3
    template <class Vector, class Factor>
    Vector quadLerp(const Vector& v0, const Vector& v1,
                    const Vector& v2, const Vector& v3, const Factor& factor)
    {
        return Vector(Vector(v0, v1, factor.x), Vector(v2, v3, factor.x), factor.y);
    }
    /// @brief Append quad grid to vertex stream.
    /// @see appendQuad
    template <class Vertex, class Index>
    void appendQuadGrid(vector<Vertex>& vertices, vector<Index>& indices,
                        const Vertex& v0, const Vertex& v1,
                        const Vertex& v2, const Vertex& v3,
                        const uint numX, const uint numZ,
                        bool flipped = false)
    {
        const uint base = vertices.size();

        // Generate vertices
        for (float j = 0; j <= numZ; ++j)
        {
            for (float i = 0; i <= numX; ++i)
            {
                const float2 factor = float2(i / numX, j / numZ);
                const Vertex v = quadLerp(v0, v1, v2, v3, factor);
                vertices.push_back(v);
            }
        }

        // Generate indices
        for (uint j = 0; j < numZ; ++j)
        {
            for (uint i = 0; i < numX; ++i)
            {
                appendQuadIndices<Index>(indices, base,
                                         j * (numX + 1) + i,
                                         j * (numX + 1) + i + 1,
                                         (j + 1) * (numX + 1) + i,
                                         (j + 1) * (numX + 1) + i + 1,
                                         flipped);
            }
        }
    }
    /// @}
}

