/// @file ls/xml/common.h
/// @brief Common XML helpers
#pragma once

#include "ls/common.h"
#include "ls/io/StreamInterface.h"
#include "ls/xml/parse.h"
#include <xml/pugixml.hpp>

namespace ls
{
    /// @addtogroup LsXml
    /// @{

    /// @brief XML node
    using XmlNode = pugi::xml_node;
    /// @brief XML attribute
    using XmlAttribute = pugi::xml_attribute;
    /// @brief XML document
    using XmlDocument = pugi::xml_document;
    /// @brief XML-to-stream writer
    class XmlWriter
        : public pugi::xml_writer
    {
    public:
        /// @brief Ctor
        XmlWriter(StreamInterface* stream)
            : stream(stream)
        {
        }
        /// @brief Write
        virtual void write(const void* data, size_t size) override
        {
            stream->write(data, size);
        }
    private:
        /// @brief Stream
        StreamInterface* stream;
    };
    /// @brief Read XML from @a stream
    inline unique_ptr<pugi::xml_document> readXml(StreamInterface& stream)
    {
        auto document = make_unique<pugi::xml_document>();
        auto result = document->load(stream.text().c_str());
        LS_THROW(result.status == pugi::xml_parse_status::status_ok,
                 IoException,
                 "XML parse error : ", result.description());
        return document;
    }
    /// @brief Get full @a node name
    inline string fullName(XmlNode node)
    {
        string name = node.name();
        node = node.parent();
        while (!node.parent().empty())
        {
            name = node.name() + string(".") + name;
            node = node.parent();
        }
        return name;
    }
    /// @brief Load node or throw
    inline XmlNode loadNode(XmlNode parent, const char* name)
    {
        auto node = parent.child(name);
        LS_THROW(!node.empty(),
                 IoException,
                 "<", fullName(parent), ".", name, "> node is required");
        return node;
    };
    /// @brief Load attribute or throw
    inline XmlAttribute loadAttrib(XmlNode parent,
                                          const char* name)
    {
        auto attrib = parent.attribute(name);
        LS_THROW(!attrib.empty(),
                 IoException,
                 "<", fullName(parent), ":", name, "> attribute is required");
        return attrib;
    };
    /// @brief Load attribute of child node or throw
    inline XmlAttribute loadNodeAttrib(XmlNode parent,
                                              const char* node,
                                              const char* attrib)
    {
        auto child = loadNode(parent, node);
        return loadAttrib(child, attrib);
    }
    /// @brief Load node-value pairs to map
    inline map<string, string> loadMap(XmlNode parent)
    {
        map<string, string> map;
        for (auto& key : parent)
        {
            string mapKey = key.name();
            string mapValue = key.attribute("value").as_string();
            bool success = map.emplace(mapKey, mapValue).second;
            LS_THROW(success,
                     IoException,
                     "Duplicate key of [", mapKey, " -> ", mapValue, "]");
        }
        return map;
    }
    /// @brief Load value-node pairs to map
    inline map<string, string> loadInverseMap(XmlNode parent)
    {
        map<string, string> map;
        for (auto& key : parent)
        {
            string mapKey = key.name();
            string mapValue = key.attribute("value").as_string();
            bool success = map.emplace(mapValue, mapKey).second;
            LS_THROW(success,
                     IoException,
                     "Duplicate key of [", mapKey, " <- ", mapValue, "]");
        }
        return map;
    }
    /// @}
}
