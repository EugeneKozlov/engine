/** @file ls/xml/common.h
 *  @brief Common XML helpers
 */
#pragma once
#include "ls/common.h"

namespace ls
{
    /** @addtogroup LsXml
     *  @{
     */
    /// @brief Read vector from string
    template <class T>
    inline vector<T> readVector(const char* value)
    {
        vector<T> ret;
        const char* begin = value;

        try
        {
            while (*begin)
            {
                while (*begin && isspace(*begin))
                    ++begin;
                const char* end = begin;
                while (*end && !isspace(*end))
                    ++end;
                ret.emplace_back(fromString<T>(string(begin, end)));
                begin = end;
            }
        }
        catch (const FormatException&)
        {
            LS_THROW(0,
                     IoException,
                     "Cannot cast string [", value, "] "
                     "to vector<", typeid (T).name(), ">");
        }

        return ret;
    }
    /// @brief Parse vector as something
    template <class Object, class Element>
    inline Object parseVector(const vector<Element>& value)
    {
        static_assert(is_same<Object, Element>::value,
                      "You need another template specialization");
        LS_THROW(!value.empty(),
                 IoException,
                 "Vector is empty");
        return value.front();
    }
    /// @brief Parse string as something
    template <class Object, class Element>
    inline Object parseString(const char* value)
    {
        return parseVector<Object, Element>(readVector<Element>(value));
    }
    /// @brief Parse vector as matrix
    template <>
    inline float4x4 parseVector<float4x4, float>(const vector<float>& value)
    {
        LS_THROW(value.size() >= 16,
                 IoException,
                 "Too few data vector : "
                 "NumElements = ", value.size());
        float4x4 ret;
        for (ubyte i = 0; i < 16; ++i)
            ret[i] = value[i];
        return ret;
    }
    /// @brief Parse vector as vectorN
    template <class T, uint N>
    inline Vector<T, N> parseVectorN(const vector<T>& value)
    {
        LS_THROW(value.size() >= N,
                 IoException,
                 "Too few data vector : "
                 "NumElements = ", value.size());
        Vector<T, N> ret;
        for (ubyte i = 0; i < N; ++i)
            ret[i] = value[i];
        return ret;
    }
    /// @brief Parse vector as float2
    template <>
    inline float2 parseVector<float2, float>(const vector<float>& value)
    {
        return parseVectorN<float, 2>(value);
    }
    /// @brief Parse vector as float3
    template <>
    inline float3 parseVector<float3, float>(const vector<float>& value)
    {
        return parseVectorN<float, 3>(value);
    }
    /// @brief Parse vector as float4
    template <>
    inline float4 parseVector<float4, float>(const vector<float>& value)
    {
        return parseVectorN<float, 4>(value);
    }
    /// @brief Parse vector as quat
    template <>
    inline quat parseVector<quat, float>(const vector<float>& value)
    {
        return quat(parseVectorN<float, 4>(value));
    }
    /// @brief Parse vector as double2
    template <>
    inline double2 parseVector<double2, double>(const vector<double>& value)
    {
        return parseVectorN<double, 2>(value);
    }
    /// @brief Parse vector as double3
    template <>
    inline double3 parseVector<double3, double>(const vector<double>& value)
    {
        return parseVectorN<double, 3>(value);
    }
    /// @brief Parse vector as double4
    template <>
    inline double4 parseVector<double4, double>(const vector<double>& value)
    {
        return parseVectorN<double, 4>(value);
    }
    /// @brief Read matrix from vector
    inline float4x4 readMatrix(const vector<float>& value)
    {
        return parseVector<float4x4, float>(value);
    }
    /// @brief Read matrix from string
    inline float4x4 readMatrix(const char* value)
    {
        return readMatrix(readVector<float>(value));
    }

    /// @brief Write vector to string
    template <class T>
    inline string writeVector(const vector<T>& value)
    {
        string ret;
        try
        {
            for (auto& item : value)
                ret += toString(item) + " ";
            if (!ret.empty())
                ret.erase(ret.end() - 1, ret.end());
        }
        catch (const FormatException&)
        {
            LS_THROW(0,
                     IoException,
                     "Cannot cast vector<", typeid (T).name(), "> to string");
        }

        return ret;
    }
    /// @brief Write matrix to string
    inline string writeMatrix(const float4x4& value)
    {
        return writeVector(vector<float>(value.value(), value.value() + 16));
    }
    /// @}
}
