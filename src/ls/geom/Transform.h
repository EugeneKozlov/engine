/// @file ls/geom/Transform.h
/// @brief Transform of the object in 3D space
#pragma once

#include "ls/common.h"

namespace ls
{
    /// @addtogroup LsGeom
    /// @{
    
    /// @brief Transform of the object in 3D space
    class Transform
    {
    public:
        /// @brief Empty
        Transform() noexcept
            : m_position(0.0f)
            , m_scaling(1.0f)
            , m_rotation()
            , m_matrix()
        {
        }
        /// @brief By @a matrix
        Transform(const float4x4& matrix) noexcept
            : m_position()
            , m_scaling()
            , m_rotation()
            , m_matrix(matrix)
        {
            fromMatrix();
        }
        /// @brief By @a position, @a rotation and @a scaling
        Transform(const float3& position,
             const quat& rotation = quat(),
             const float3& scaling = 1.0f) noexcept
            : m_position(position)
            , m_scaling(scaling)
            , m_rotation(rotation)
            , m_matrix()
        {
            toMatrix();
        }
        /// @brief By two poses
        Transform(const Transform& first, const Transform& second, float factor) noexcept
            : m_position(lerp(first.position(), second.position(), factor))
            , m_scaling(lerp(first.scaling(), second.scaling(), factor))
            , m_rotation(slerp(first.rotation(), second.rotation(), factor))
        {
            toMatrix();
        }

        /// @brief Get position
        const float3& position() const noexcept
        {
            return m_position;
        }
        /// @brief Get rotation
        const quat& rotation() const noexcept
        {
            return m_rotation;
        }
        /// @brief Get scaling
        const float3& scaling() const noexcept
        {
            return m_scaling;
        }
        /// @brief Get matrix
        const float4x4& matrix() const noexcept
        {
            return m_matrix;
        }
        /// @brief Test if finite
        bool isFinite() const noexcept
        {
            return ls::isFinite(m_position)
                && ls::isFinite(m_scaling)
                && ls::isFinite(m_rotation);
        }

        /// @brief Set @a position
        void position(const float3& position) noexcept
        {
            m_position = position;
            toMatrix();
        }
        /// @brief Set @a rotation
        void rotation(const quat& rotation) noexcept
        {
            m_rotation = rotation;
            toMatrix();
        }
        /// @brief Set @a scaling
        void scaling(const float3& scaling) noexcept
        {
            m_scaling = scaling;
            toMatrix();
        }

        /// @brief Multiply this by transorm
        Transform& operator *= (const Transform& pose) noexcept
        {
            m_matrix *= pose.m_matrix;
            fromMatrix();
            return *this;
        }
        /// @brief Multiply by transorm
        Transform operator * (const Transform& pose) const noexcept
        {
            Transform tmp = *this;
            tmp *= pose;
            return tmp;
        }
        /// @brief Add position to this
        Transform& operator += (const float3& position) noexcept
        {
            m_matrix.row(3, m_matrix.row<3>(3) + position);
            m_position += position;
            return *this;
        }
        /// @brief Add position
        Transform operator + (const float3& position) const noexcept
        {
            Transform tmp = *this;
            tmp += position;
            return tmp;
        }
        /// @brief Multiply this by rotation
        Transform& operator *= (const quat& rotation) noexcept
        {
            m_matrix *= matrix::rotationQuaternion4(rotation);
            fromMatrix();
            return *this;
        }
        /// @brief Multiply by rotation
        Transform operator * (const quat& rotation) const noexcept
        {
            Transform tmp = *this;
            tmp *= rotation;
            return tmp;
        }

        /// @brief Is equal?
        friend bool operator == (const Transform& lhs, const Transform& rhs) noexcept
        {
            return lhs.position() == rhs.position()
                && lhs.rotation() == rhs.rotation()
                && lhs.scaling() == rhs.scaling();
        }
        /// @brief Is not equal?
        inline friend bool operator != (const Transform& lhs, const Transform& rhs) noexcept
        {
            return !(lhs == rhs);
        }
    protected:
        /// @brief Internal conversion from transform to matrix
        void toMatrix() noexcept
        {
            m_matrix = matrix::compose(m_position, m_rotation, m_scaling);
        }
        /// @brief Internal conversion from matrix to transform
        void fromMatrix() noexcept
        {
            matrix::decompose(m_matrix, m_position, m_rotation, m_scaling);
        }
    protected:
        /// @brief Position
        float3 m_position;
        /// @brief Scaling
        float3 m_scaling;
        /// @brief Rotation
        quat m_rotation;
        /// @brief Matrix
        float4x4 m_matrix;
    };
    /// @}
}
