#include "pch.h"
#include "ls/geom/AnimationRectifier.h"
#include "ls/geom/NodeAnimation.h"
#include "ls/geom/GeometryNode.h"
#include "ls/geom/NodeHierarchy.h"
#include "ls/geom/math.h"
#include "ls/render/Plotter.h"

using namespace ls;
AnimationRectifier::AnimationRectifier(NodeHierarchy& hierarchy,
                                       const NodeAnimation& animation,
                                       uint rootNode, double begin, double end)
    : m_hierarchy(hierarchy)
    , m_rootNode(rootNode)
    , m_restoredAnimation(animation.cut(begin, end))
{

}
void AnimationRectifier::rectify(double rectifySegmentDuration,
                                 const float3& direction)
{
    // Trace
    vector<Transform> poseTrack;
    m_restoredAnimation->traceGlobalPoses(m_rootNode, poseTrack);
    vector<float3> positionTrack;
    for (auto& pose : poseTrack)
        positionTrack.push_back(pose.position());

    // Get way
    vector<float3> pelvisWaySegments;
    uint numPoints = (uint) positionTrack.size();
    uint numPointsPerSegment = (uint) (rectifySegmentDuration / m_restoredAnimation->step());
    uint numSegments = max<uint>(1, numPoints / numPointsPerSegment);
    for (uint iterSegment = 0; iterSegment < numSegments; ++iterSegment)
    {
        auto firstPoint = positionTrack.data() + iterSegment * numPointsPerSegment;
        uint tailSize = (iterSegment + 1 == numSegments) ? numPoints % numPointsPerSegment : 0;

        auto seg = math3::computeSegmentByPoints(firstPoint, min<uint>(numPoints, numPointsPerSegment + tailSize));

        pelvisWaySegments.push_back(seg.a);
        pelvisWaySegments.push_back(seg.b);
    }
    uint numSegments2 = (uint) pelvisWaySegments.size();

    // Weld way
    vector<float3> pelvisWay;
    pelvisWay.push_back(pelvisWaySegments.front());
    for (uint i = 1; i < numSegments2 - 1; i += 2)
    {
        pelvisWay.push_back((pelvisWaySegments[i] + pelvisWaySegments[i + 1]) / 2);
    }
    pelvisWay.push_back(pelvisWaySegments.back());

    // Flatten height
    auto averageHeight = std::accumulate(pelvisWay.begin(), pelvisWay.end(), float3(0.0f)).y / (float) pelvisWay.size();
    for (auto& point : pelvisWay)
        point.y = averageHeight;

    // Restore direction
    auto zAxis = direction;
    //auto yAxis = float3{0, 1, 0};
    //auto xAxis = cross(yAxis, zAxis);
    auto basePosition = float3{0, averageHeight, 0};
    auto baseZ = 0.0f;
    for (uint iterSegment = 0; iterSegment < pelvisWay.size() - 1; ++iterSegment)
    {
        auto fromPoint = pelvisWay[iterSegment];
        auto toPoint = pelvisWay[iterSegment + 1];
        auto moveDir = toPoint - fromPoint;
        auto moveLength = length(moveDir);

        float4x4 restoreMatrix
            = matrix::translate(-fromPoint)
            * matrix::rotationQuaternion4(quat(moveDir / moveLength, direction))
            * matrix::translate(basePosition);
        baseZ += moveLength;
        basePosition += zAxis * moveLength;

        uint startIndex = iterSegment * numPointsPerSegment;
        uint tailSize = (iterSegment + 1 == numSegments)
            ? numPoints % numPointsPerSegment
            : 0;
        for (uint iterPoint = startIndex;
            iterPoint < min(numPoints, startIndex + numPointsPerSegment + tailSize);
            ++iterPoint)
        {
            auto& pose = poseTrack[iterPoint];
            pose = pose * restoreMatrix;
        }
    }

    // Upload
    m_restoredAnimation->updateNode(m_rootNode, poseTrack);
    m_restoredAnimation->finalize(m_hierarchy);

    // Update traces
    m_linearMovement.clear();
    m_realMovement.clear();
    for (uint iterPoint = 0; iterPoint < numPoints; ++iterPoint)
    {
        m_realMovement.push_back(poseTrack[iterPoint].position());
        m_linearMovement.push_back(baseZ * iterPoint / (numPoints - 1) * zAxis);
    }
}
double AnimationRectifier::averageSpeed() const
{
    return distance(m_linearMovement.front(), m_linearMovement.back())
        / m_restoredAnimation->duration();
}
void AnimationRectifier::center()
{
    m_centeredAnimation = make_shared<NodeAnimation>(*m_restoredAnimation);

    // Unmove
    vector<Transform> track;
    m_centeredAnimation->traceLocalPoses(m_rootNode, track);
    uint numKeys = m_centeredAnimation->numKeys();
    for (uint iterFrame = 0; iterFrame < numKeys; ++iterFrame)
    {
        track[iterFrame] += -m_linearMovement[iterFrame];
    }

    // Update
    m_centeredAnimation->updateNode(m_rootNode, track);
    m_centeredAnimation->finalize(m_hierarchy);
}
void AnimationRectifier::periodizeSoft(uint traceFrom, uint weldTo,
                                       uint weldFrom, uint traceTo)
{
    m_periodicalAnimation = make_shared<NodeAnimation>(
        m_centeredAnimation->numNodes());

    // Borders
    uint numKeys = m_centeredAnimation->numKeys();
    uint numNodes = m_centeredAnimation->numNodes();
    vector<Transform> track;
    for (uint iterNode = 0; iterNode < numNodes; ++iterNode)
    {
        // Trace
        track.clear();
        m_centeredAnimation->traceLocalPoses(iterNode, track);

        // Weld
        uint numWeld = min(traceTo - weldFrom, weldTo - traceFrom);
        for (uint iterWeld = 0; iterWeld < numWeld; ++iterWeld)
        {
            float lerpFactor = static_cast<float>(iterWeld) / numWeld;
            track[traceFrom + iterWeld] = Transform(track[weldFrom + iterWeld],
                                               track[traceFrom + iterWeld],
                                               lerpFactor);
        }

        // Write
        uint numTraced = weldFrom - traceFrom + 1;
        for (uint iterFrame = 0; iterFrame < numTraced; ++iterFrame)
        {
            auto time = static_cast<double>(iterFrame) / (numTraced - 1);
            m_periodicalAnimation->addKey(time, iterNode,
                                          track[traceFrom + iterFrame], none);
        }
    }

    m_periodicalAnimation->finalize(m_hierarchy);
}
void AnimationRectifier::periodizeRaw(double traceFrom, double traceTo,
                                      double startLerp)
{
    // Borders
    uint numNodes = m_centeredAnimation->numNodes();
    m_periodicalAnimation = m_centeredAnimation->cut(traceFrom, traceTo, true);

    // Last frame
    auto& lastFrames = (--m_periodicalAnimation->end())->second;

    for (auto& key : *m_periodicalAnimation)
    {
        auto time = key.first;
        auto& frames = key.second;
        if (time >= startLerp)
            break;
        float factor = static_cast<float>(time / startLerp);

        // Lerp
        for (uint nodeIndex = 0; nodeIndex < numNodes; ++nodeIndex)
        {
            frames[nodeIndex].local = lerp(lastFrames[nodeIndex].local.matrix(),
                                           frames[nodeIndex].local.matrix(),
                                           factor);
        }

        // Update global
        m_hierarchy.update(frames);
    }
}
