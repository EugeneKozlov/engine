/** @file ls/geom/AnimationRectifier.h
 *  @brief Animation rectifier and 'periodizer'
 */
#pragma once
#include "ls/common.h"
#include "ls/geom/Transform.h"

namespace ls
{
    class NodeHierarchy;
    class NodeAnimation;

    /** @addtogroup LsGeom
     *  @{
     */
    /** Animation rectifier and 'periodizer'
     */
    class AnimationRectifier
        : Noncopyable
    {
    public:
        /// @brief Ctor
        AnimationRectifier(NodeHierarchy& hierarchy,
                           const NodeAnimation& animation,
                           uint rootNode, double begin, double end);
        /// @brief Rectify
        void rectify(double rectifySegmentDuration, const float3& direction);
        /// @brief Center
        void center();
        /// @brief Periodize and soft glue
        void periodizeSoft(uint traceFrom, uint weldTo, uint weldFrom, uint traceTo);
        /// @brief Periodize and raw glue
        void periodizeRaw(double traceFrom, double traceTo, double startLerp);

        /** @name Gets
         *  @{
         */
        /// @brief Get average speed
        double averageSpeed() const;
        /// @brief Get restored animation
        shared_ptr<NodeAnimation> restoredAnimation() const noexcept
        {
            return m_restoredAnimation;
        }
        /// @brief Linear pelvis movement
        const vector<float3>& linearMovement() const noexcept
        {
            return m_linearMovement;
        }
        /// @brief Real pevlis movement
        const vector<float3>& realMovement() const noexcept
        {
            return m_realMovement;
        }
        /// @brief Get centered animation
        shared_ptr<NodeAnimation> centeredAnimation() const noexcept
        {
            return m_centeredAnimation;
        }
        /// @brief Get periodical animation
        shared_ptr<NodeAnimation> periodicalAnimation() const noexcept
        {
            return m_periodicalAnimation;
        }
        /// @}
    private:
        /// @brief Default forward vector
        const float3 DefaultForward = float3{ 0, 0, 1 };
    private:
        /// @brief Hierarchy
        NodeHierarchy& m_hierarchy;
        /// @brief Root node index
        uint m_rootNode;

        /// @brief Restored animation
        shared_ptr<NodeAnimation> m_restoredAnimation;
        /// @brief Linear pelvis movement
        vector<float3> m_linearMovement;
        /// @brief Real pevlis movement
        vector<float3> m_realMovement;

        /// @brief Centered animation
        shared_ptr<NodeAnimation> m_centeredAnimation;

        /// @brief Periodical animation
        shared_ptr<NodeAnimation> m_periodicalAnimation;
    };
    /// @}
}

