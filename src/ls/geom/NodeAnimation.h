/** @file ls/geom/NodeAnimation.h
 *  @brief Node animation
 */
#pragma once
#include "ls/common.h"
#include "ls/core/Nameable.h"
#include "ls/geom/Transform.h"
#include "ls/geom/GeometryNode.h"

namespace ls
{
    class NodeHierarchy;

    /** @addtogroup LsGeom
     *  @{
     */
    /** Node animation.
     *  Get/lerp functions will rewrite only animated nodes.
     *
     *  - Initialized/uninitialized nodes
     *
     *    You may skip one or more nodes @b at @a all,
     *    You must make sure that @b every non-skipped node
     *    is fully initialized, i.e. each timestamp has local matrix.
     *
     *  - Global/local animation
     *
     *    You can @a finalize animation to make it global
     *    At other way, you can load global poses for each node.
     *    You must make sure that you set global node
     *    for each node for each time key.
     */
    class NodeAnimation
        : public Named
    {
    public:
        /// @brief Key map type
        using KeyMap = map<double, DecomposedNodeArray>;
        /** Ctor.
         *  @param numNodes
         *    Number of animation channels
         */
        NodeAnimation(uint numNodes);
        /// @brief Dtor
        ~NodeAnimation();
        /** Set @a node @a pose at specified @a time.
         *  @param time
         *    Animation key time
         *  @param node
         *    Node to set
         *  @param local
         *    Local pose to set
         *  @param global
         *    Global pose to set
         */
        void addKey(double time, uint node,
                    const Transform& local,
                    const optional<Transform>& global);
        /// @brief Clear @a node animation
        void clear(uint node);
        /// @brief Update global transforms
        void finalize(const NodeHierarchy& hierarchy);

        /// @brief Transfer animation between hierarchies
        shared_ptr<NodeAnimation> transfer(const NodeHierarchy& from,
                                           const NodeHierarchy& to) const;
        /// @brief Extract animation of specified nodes and save node indices
        shared_ptr<NodeAnimation> extractStable(const set<uint>& nodes) const;
        /// @brief Extract animation of specified nodes, nodes will be re-indexed
        shared_ptr<NodeAnimation> extractReindex(const vector<uint>& nodes) const;
        /// @brief Cut animation
        shared_ptr<NodeAnimation> cut(double begin, double end,
                                      bool rescale = false) const;
        /// @brief Update @a node local @a poses
        void updateNode(uint node, const vector<Transform>& poses);

        /** @name Gets
         *  @{
         */
        /// @brief Get number of nodes
        uint numNodes() const noexcept
        {
            return m_numNodes;
        }
        /// @brief Get number of keys
        uint numKeys() const noexcept
        {
            return (uint) m_keys.size();
        }
        /// @brief Test if @a node is animated
        bool isAnimated(uint node) const noexcept
        {
            debug_assert(node < m_numNodes, node);
            return !!m_animatedNodes[node];
        }
        /// @brief Test if all nodes are animated
        bool isFullyAnimated() const noexcept
        {
            if (!m_fullAnimated)
            {
                for (auto iter : m_animatedNodes)
                {
                    if (!iter)
                    {
                        *m_fullAnimated = false;
                        return false;
                    }
                }
                *m_fullAnimated = true;
            }
            return *m_fullAnimated;
        }
        /// @brief Test if global poses are valid
        bool isGlobal() const noexcept
        {
            return m_global;
        }
        /// @brief Get time delta between two frames
        double step() const noexcept
        {
            if (!m_step)
            {
                debug_assert(m_keys.size(), "too few keys");

                auto iterSecond = m_keys.begin();
                auto iterFirst = iterSecond++;
                m_step = iterSecond->first - iterFirst->first;
            }
            return *m_step;
        }
        /// @brief Get duration
        double duration() const noexcept
        {
            if (!m_duration)
            {
                debug_assert(m_keys.size(), "too few keys");

                auto iterFirst = m_keys.begin();
                auto iterLast = --m_keys.end();
                m_duration = iterLast->first - iterFirst->first;
            }
            return *m_duration;
        }
        /// @brief Get begin key iterator
        KeyMap::iterator begin() noexcept
        {
            return m_keys.begin();
        }
        /// @brief Get end key iterator
        KeyMap::iterator end() noexcept
        {
            return m_keys.end();
        }
        /// @brief Get begin const key iterator
        KeyMap::const_iterator begin() const noexcept
        {
            return m_keys.begin();
        }
        /// @brief Get end const key iterator
        KeyMap::const_iterator end() const noexcept
        {
            return m_keys.end();
        }
        /// @brief Get array of animated/non-animated node flags
        const vector<ubyte>& animationFlags() const
        {
            return m_animatedNodes;
        }
        /// @}

        /** @name Animation
         *  @{
         */
        /// @brief Get local @a node pose (raw) at specified @a time
        const Transform& getLocalPose(uint node, double time) const;
        /// @brief Get local @a node pose (interpolated) at specified @a time
        Transform lerpLocalPose(uint node, double time) const;
        /// @brief Get global @a node pose (raw) at specified @a time
        const Transform& getGlobalPose(uint node, double time) const;
        /// @brief Get global @a node pose (interpolated) at specified @a time
        Transform lerpGlobalPose(uint node, double time) const;
        /// @brief Copy raw poses at @a time to @a dest
        void getPoses(double time, ComposedNodeArray& dest) const;
        /// @brief Copy linear interpolated poses at @a time to @a dest
        void lerpPoses(double time, ComposedNodeArray& dest) const;
        /// @brief Copy mixed poses at @a time to @a dest
        void mixPoses(double time, ComposedNodeArray& dest) const;
        /// @brief Copy raw re-indexed poses at @a time to @a dest
        void getPoses(double time, ComposedNodeArray& dest, const vector<uint>& reindex) const;
        /// @brief Copy linear interpolated re-indexed poses at @a time to @a dest
        void lerpPoses(double time, ComposedNodeArray& dest, const vector<uint>& reindex) const;
        /// @brief Copy mixed re-indexed poses at @a time to @a dest
        void mixPoses(double time, ComposedNodeArray& dest, const vector<uint>& reindex) const;
        /// @brief Trace local poses of @a node to @a dest
        void traceLocalPoses(uint node, vector<Transform>& dest) const;
        /// @brief Trace global poses of @a node to @a dest
        void traceGlobalPoses(uint node, vector<Transform>& dest) const;
        /// @}
    private:
        /// @brief Get key frames
        void getKeys(double time,
                     const DecomposedNodeArray* &left,
                     const DecomposedNodeArray* &right,
                     float& factor) const;
        /// @brief Interpolate keys
        template <class Getter>
        void posesCombine(const Getter& get, double time,
                          ComposedNodeArray& dest,
                          const uint* reindex) const;
        /// @brief Transform getter
        struct TransformGetter
        {
            void operator ()(ComposedNodeArray& dest,
                const DecomposedNodeArray& left,
                const DecomposedNodeArray& right,
                uint destIndex, uint srcIndex, float factor) const;
        };
        /// @brief Transform interpolator
        struct TransformLerper
        {
            void operator ()(ComposedNodeArray& dest,
                const DecomposedNodeArray& left,
                const DecomposedNodeArray& right,
                uint destIndex, uint srcIndex, float factor) const;
        };
        /// @brief Transform mixer
        struct TransformMixer
        {
            void operator ()(ComposedNodeArray& dest,
                const DecomposedNodeArray& left,
                const DecomposedNodeArray& right,
                uint destIndex, uint srcIndex, float factor) const;
        };
    private:
        /// @brief Animation step
        mutable optional<double> m_step = none;
        /// @brief Animation duration
        mutable optional<double> m_duration = none;
        /// @brief Is fully animated?
        mutable optional<bool> m_fullAnimated = none;
        /// @brief Set if global poses are valid
        bool m_global = false;
        /// @brief Number of nodes
        uint m_numNodes;
        /// @brief Animated nodes
        vector<ubyte> m_animatedNodes;
        /// @brief Animation keys
        KeyMap m_keys;
    };
    /// @}
}