#include "pch.h"
#include "ls/geom/NodeHierarchy.h"
#include "ls/render/Plotter.h"

using namespace ls;
NodeHierarchy::NodeHierarchy()
    : m_root(GeometryNode::Invalid, "(root)", float4x4{}, nullptr)
{
}
NodeHierarchy::~NodeHierarchy()
{
}
NodeHierarchy::Node& NodeHierarchy::addNode(const char* name,
                                            const float4x4& localPose,
                                            Node* parent)
{
    m_startPose.clear();

    if (!parent)
        parent = &m_root;

    auto index = (sint) m_nodes.size();
    m_nodes.emplace_back(index, name, localPose, parent);
    auto node = &m_nodes.back();

    if (!parent->child)
        parent->child = node;
    else
    {
        auto child = parent->child;
        while (child->sibling)
            child = child->sibling;
        child->sibling = node;
    }

    return *node;
}
NodeHierarchy::Node& NodeHierarchy::addNode(const char* name,
                                            const float4x4& localPose,
                                            uint parentIndex)
{
    auto parent = (parentIndex != GeometryNode::Invalid) ? &getNode(parentIndex) : nullptr;
    return addNode(name, localPose, parent);
}
void NodeHierarchy::reset()
{
    for (auto& node : m_nodes)
    {
        node.local = node.startLocal;
        node.global = node.startGlobal;
    }
}
void NodeHierarchy::reset(const ComposedNodeArray& frames)
{
    for (auto& node : m_nodes)
    {
        node.local = frames[node.index].local;
        node.global = frames[node.index].global;
    }
}


// Gets
NodeHierarchy::Node& NodeHierarchy::getNode(uint index)
{
    return const_cast<Node&> (const_cast<const NodeHierarchy*> (this)->getNode(index));
}
const NodeHierarchy::Node& NodeHierarchy::getNode(uint index) const
{
    debug_assert(index < m_nodes.size(), index);

    return m_nodes[index];
}
NodeHierarchy::Node* NodeHierarchy::findNode(const char* name)
{
    return const_cast<Node*> (const_cast<const NodeHierarchy*> (this)->findNode(name));
}
const NodeHierarchy::Node* NodeHierarchy::findNode(const char* name) const
{
    auto iterNode = std::find_if(m_nodes.begin(), m_nodes.end(), [name](const Node& node)
    {
        return node.name == name;
    });
    if (iterNode == m_nodes.end())
        return nullptr;
    else
        return &*iterNode;
}
ComposedNodeArray const& NodeHierarchy::getStartPose() const
{
    // Generate
    if (m_startPose.empty())
    {
        for (auto& node : m_nodes)
            m_startPose.emplace_back(node.startLocal.matrix(), node.startGlobal.matrix());
    }

    return m_startPose;
}
ComposedNodeArray NodeHierarchy::getPoses() const
{
    ComposedNodeArray dest;
    for (auto& node : m_nodes)
        dest.emplace_back(node.local.matrix(), node.global.matrix());
    return dest;
}
uint NodeHierarchy::numNodes() const
{
    return (uint) m_nodes.size();
}


// Draw
float4x4 NodeHierarchy::Node::debugMatrix(const float4x4& thisGlobal,
                                          const float4x4& parentGlobal)
{
    return boxByLine(float3{parentGlobal.row(3)}, float3{thisGlobal.row(3)});
}
float4x4 NodeHierarchy::Node::boxByLine(const float3& from, const float3& to)
{
    static const float3 def{1, 0, 0};

    float3 dir = to - from;
    auto len = length(dir);
    float4x4 mat;
    mat.row(3, float3(0.5f, 0, 0));
    mat *= matrix::scale(float4(len, 1.0f, 1.0f, 1));
    mat *= matrix::rotationQuaternion4(quat{def, dir / len});
    mat *= matrix::translate(from);

    return mat;
}
void NodeHierarchy::debugDraw(Plotter& plotter,
                              const float3& tagColor,
                              const float3& bonesColor,
                              float rootSize, float boneSize)
{
    auto tagHexColor = rgb2hex(tagColor);
    auto bonesHexColor = rgb2hex(bonesColor);
    for (auto& node : m_nodes)
    {
        auto& thisGlobal = node.global.matrix();
        if (node.parent == &m_root)
            plotter.plotWireSphere(thisGlobal, tagHexColor, rootSize);
        else
        {
            auto& parentGlobal = node.parent->global.matrix();
            plotter.plotWireBox(node.debugMatrix(thisGlobal, parentGlobal),
                                bonesHexColor, float3(0.5f, boneSize, boneSize));
            if (!node.child)
                plotter.plotWireSphere(thisGlobal, tagHexColor, boneSize);
        }
    }
}

