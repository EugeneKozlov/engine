/** @file ls/geom/GeometryNode.h
 *  @brief Node description
 */
#pragma once
#include "ls/common.h"
#include "ls/geom/Transform.h"

namespace ls
{
    /** @addtogroup LsGeom
     *  @{
     */
    /** Geometry node
     */
    class GeometryNode
    : Noncopyable
    {
    public:
        /// @brief Invalid index
        static const uint Invalid = static_cast<uint>(-1);
        /// @brief Ctor
        GeometryNode(uint index,
                     const char* name,
                     const float4x4& local,
                     GeometryNode* parent)
            : index(index)
            , startLocal(local)
            , startGlobal(parent ? (startLocal * parent->startGlobal) : startLocal)
            , startInverseGlobal(inverse(startGlobal.matrix()))
            , name(name)
            , parent(parent)
            , local(startLocal)
            , global(startGlobal)
        {
        }
        /// @brief Update global pose
        void updateGlobal()
        {
            if (parent)
                global = local * parent->global;
            else
                global = local;
            if (sibling)
                sibling->updateGlobal();
            if (child)
                child->updateGlobal();
        }

        /** @name Search
         *  @{
         */
        /// @brief Test if root
        bool isRoot() const noexcept
        {
            return parent->index == -1;
        }
        /// @brief Find const node by @a name among sibling
        const GeometryNode* findSibling(const string& name) const noexcept
        {
            if (name == this->name)
                return this;
            else if (!sibling)
                return nullptr;
            else
                return sibling->findSibling(name);
        }
        /// @brief Find const node by @a name among children
        const GeometryNode* findChild(const string& name) const noexcept
        {
            if (!child)
                return nullptr;
            else
                return child->findSibling(name);
        }
        /// @brief Find const node by @a name
        const GeometryNode* find(const string& name) const noexcept
        {
            if (name == this->name)
                return this;
            else if (sibling)
            {
                auto node = sibling->find(name);
                if (node)
                    return node;
            }
            else if (child)
                return child->find(name);
            else
                return nullptr;
        }
        /// @brief Find parent by @a index
        const GeometryNode* findParent(sint index) const noexcept
        {
            if (this->index == index)
                return this;
            if (!parent)
                return nullptr;
            return parent->findParent(index);
        }
        /// @brief Find node by @a name among sibling
        GeometryNode* findSibling(const string& name) noexcept
        {
            return const_cast<GeometryNode*> (const_cast<const GeometryNode*>
                (this)->findSibling(name));
        }
        /// @brief Find node by @a name among children
        GeometryNode* findChild(const string& name) noexcept
        {
            return const_cast<GeometryNode*> (const_cast<const GeometryNode*>
                (this)->findChild(name));
        }
        /// @brief Find node by @a name
        GeometryNode* find(const string& name) noexcept
        {
            return const_cast<GeometryNode*> (const_cast<const GeometryNode*>
                (this)->find(name));
        }
        /// @brief Find parent by @a index
        GeometryNode* findParent(sint index) noexcept
        {
            return const_cast<GeometryNode*> (const_cast<const GeometryNode*>
                (this)->findParent(index));
        }
        /// @}
    public:
        /// @brief Node index
        const sint index;
        /// @brief Local pose
        const Transform startLocal;
        /// @brief Global pose
        const Transform startGlobal;
        /// @brief Inverse global
        const float4x4 startInverseGlobal;
        /// @brief Name
        string name;

        /// @brief Parent pointer
        GeometryNode * const parent;
        /// @brief Sibling pointer
        GeometryNode* sibling = nullptr;
        /// @brief Child pointer
        GeometryNode* child = nullptr;

        /// @brief Local pose
        Transform local;
        /// @brief Global pose
        Transform global;
    public:
        /// @brief Get debug box matrix by line
        static float4x4 boxByLine(const float3& from, const float3& to);
        /// @brief Get debug matrix
        static float4x4 debugMatrix(const float4x4& thisGlobal,
                                    const float4x4& parentGlobal);
    };
    /** Tiny composed node description
     */
    struct ComposedNode
    {
        /// @brief Empty ctor
        ComposedNode() = default;
        /// @brief Local only
        ComposedNode(const float4x4& local)
            : local(local)
        {
        }
        /// @brief Global and local
        ComposedNode(const float4x4& local, const float4x4& global)
            : local(local)
            , global(global)
        {
        }
        /// @brief Global position
        const float3& globalPosition() const noexcept
        {
            return global.row<3>(3);
        }
        /// @brief Global matrix
        const float4x4& globalMatrix() const noexcept
        {
            return global;
        }
        /// @brief Local matrix
        const float4x4& localMatrix() const noexcept
        {
            return local;
        }
        /// @brief Local pose
        float4x4 local;
        /// @brief Global pose
        float4x4 global;
    };
    /** Tiny decomposed node description
     */
    struct DecomposedNode
    {
        /// @brief Empty ctor
        DecomposedNode() = default;
        /// @brief Local only
        DecomposedNode(const Transform& local)
            : local(local)
        {
        }
        /// @brief Global and local
        DecomposedNode(const Transform& local, const Transform& global)
            : local(local)
            , global(global)
        {
        }
        /// @brief Global position
        const float3& globalPosition() const noexcept
        {
            return global.position();
        }
        /// @brief Global matrix
        const float4x4& globalMatrix() const noexcept
        {
            return global.matrix();
        }
        /// @brief Local matrix
        const float4x4& localMatrix() const noexcept
        {
            return local.matrix();
        }
        /// @brief Local pose
        Transform local;
        /// @brief Global pose
        Transform global;
    };
    /// @brief Packed tiny node array
    using ComposedNodeArray = vector<ComposedNode>;
    /// @brief Unpacked heavy node array
    using DecomposedNodeArray = vector<DecomposedNode>;
    /// @brief Match child node to specified position
    template <class Node>
    void matchNode(vector<Node>& nodesArray,
                   uint parentIndex, uint nodeIndex, uint childIndex,
                   const float3& newPosition)
    {
        Node& parent = nodesArray[parentIndex];
        Node& node = nodesArray[nodeIndex];
        Node& child = nodesArray[childIndex];

        float3 oldNode = node.globalPosition();
        float3 oldChild = child.globalPosition();

        quat rotation{normalize(oldChild - oldNode), normalize(newPosition - oldNode)};
        float4x4 transform
            = matrix::translate(-oldNode)
            * matrix::rotationQuaternion4(rotation)
            * matrix::translate(oldNode);

        node.local = node.globalMatrix() * transform * inverse(parent.globalMatrix());
    }
    /// @brief Lerp nodes
    inline void lerpNodes(ComposedNodeArray& left,
                          const ComposedNodeArray& right,
                          float factor)
    {
        uint numNodes = min<uint>(left.size(), right.size());
        for (uint nodeIndex = 0; nodeIndex < numNodes; ++nodeIndex)
        {
            left[nodeIndex].local = lerp(left[nodeIndex].local,
                                         right[nodeIndex].local,
                                         factor);
            left[nodeIndex].global = lerp(left[nodeIndex].global,
                                          right[nodeIndex].global,
                                          factor);
        }
    }
    /// @}
}