/** @file ls/geom/NodeHierarchy.h
 ** @brief Node hierarchy
 **/
#pragma once
#include "ls/common.h"
#include "ls/geom/Transform.h"
#include "ls/geom/GeometryNode.h"

namespace ls
{
    class Plotter;

    /** @addtogroup LsGeom
     *  @{
     */
    /** Node hierarchy.
     *  You always can set local poses from array,
     *  and you always can reset all nodes to initial state.
     */
    class NodeHierarchy
    : Noncopyable
    {
    public:
        /// @brief Node
        using Node = GeometryNode;
        /// @brief Node container
        using Container = deque<Node>;
    public:
        /// @brief Ctor.
        NodeHierarchy();
        /// @brief Dtor
        ~NodeHierarchy();
        /// @brief Attach node with @a name and @a pose to @a parent (optional)
        Node& addNode(const char* name, const float4x4& localPose, Node* parent);
        /// @brief Attach node with @a name and @a pose to @a parent by index
        Node& addNode(const char* name, const float4x4& localPose, uint parentIndex = GeometryNode::Invalid);
        /// @brief Reset hierarchy by start poses
        void reset();
        /// @brief Reset hierarchy by @a frames
        void reset(const ComposedNodeArray& frames);
        /// @brief Update global hierarchy transforms
        template <class Array>
        void update(Array& nodes) const
        {
            for (auto child = m_root.child; child; child = child->sibling)
                updateChilds(nodes, *child, float4x4{});
        }
        /// @brief Update global hierarchy transforms starting from node with @a index
        template <class Array>
        void update(Array& nodes, uint index) const
        {
            debug_assert(index < (uint) m_nodes.size(), index);

            sint parentIndex = m_nodes[index].parent->index;
            float4x4 parentGlobal =
                (parentIndex >= 0)
                ? nodes[parentIndex].globalMatrix()
                : float4x4();
            updateChilds(nodes, m_nodes[index], parentGlobal);
        }
        /** Rename nodes
         *  @param names
         *    Old-to-new names map
         *  @param unnamed
         *    Bones without mapping will be renamed to '@<unnamed@>_@<boneIndex@>'
         */
        template <class Map>
        void rename(const Map& names, const string& unnamed = "bone")
        {
            Map usage = names;
            for (auto& node : * this)
            {
                string oldName = node.name;
                auto found = findOrDefault(names, oldName);

                string newName = unnamed + toString(node.index);
                if (found)
                {
                    newName = *found;

                    LS_THROW(usage.count(oldName) == 1, IoException,
                             "Duplicate usage of name [", newName, "] with node [", oldName, "]");
                    usage.erase(oldName);
                }
                node.name = newName;
            }
            LS_THROW(usage.empty(), IoException,
                     "Unused name [", usage.begin()->second, ":", usage.begin()->first, "]");
        }

        /** @name Gets
         *  @{
         */
        /// @brief Test if node is root
        bool isRoot(uint index) const;
        /// @brief Get node by @a index
        Node& getNode(uint index);
        /// @brief Find node by @a name
        Node* findNode(const char* name);
        /// @brief Get const node by @a index
        const Node& getNode(uint index) const;
        /// @brief Find const node by @a name
        const Node* findNode(const char* name) const;
        /// @brief Get tiny array for start pose
        const ComposedNodeArray& getStartPose() const;
        /// @brief Get all poses
        ComposedNodeArray getPoses() const;
        /// @brief Get number of nodes
        uint numNodes() const;
        /// @brief Get begin iterator
        Container::iterator begin()
        {
            return m_nodes.begin();
        }
        /// @brief Get end iterator
        Container::iterator end()
        {
            return m_nodes.end();
        }
        /// @brief Get begin const iterator
        Container::const_iterator begin() const
        {
            return m_nodes.begin();
        }
        /// @brief Get end const iterator
        Container::const_iterator end() const
        {
            return m_nodes.end();
        }
        /// @}

        /// @brief Debug draw this
        void debugDraw(Plotter& plotter,
                       const float3& tagColor,
                       const float3& bonesColor,
                       float rootSize, float boneSize);
    private:
        /// @brief Recursive update
        template <class Array>
        void updateChilds(Array& nodes,
                          const Node& node,
                          const float4x4& parentTransform) const
        {
            debug_assert(nodes.size() >= m_nodes.size());

            auto& tiny = nodes[node.index];
            tiny.global = tiny.local * parentTransform;
            for (auto child = node.child; child; child = child->sibling)
                updateChilds(nodes, *child, tiny.globalMatrix());
        }
    private:
        /// @brief Pool
        Container m_nodes;
        /// @brief Root
        Node m_root;
        /// @brief Tiny nodes for this hierarchy
        mutable ComposedNodeArray m_startPose;
    };
    /// @}
}

