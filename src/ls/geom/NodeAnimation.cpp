#include "pch.h"
#include "ls/geom/NodeAnimation.h"
#include "ls/geom/NodeHierarchy.h"

using namespace ls;
NodeAnimation::NodeAnimation(uint numNodes)
    : m_numNodes(numNodes)
    , m_animatedNodes(numNodes, false)
{
}
NodeAnimation::~NodeAnimation()
{
}


// Keys
void NodeAnimation::addKey(double time, uint node,
                           const Transform& local,
                           const optional<Transform>& global)
{
    debug_assert(node < m_numNodes, node);

    m_step = none;
    m_duration = none;
    m_fullAnimated = none;

    auto iterKey = m_keys.emplace(time, vector<DecomposedNode>{m_numNodes});
    auto& key = iterKey.first->second;

    if (!!global)
    {
        m_global = true;
        key[node] = DecomposedNode{local, *global};
    }
    else
    {
        key[node] = DecomposedNode{local, float4x4
            {}};
    }
    m_animatedNodes[node] = true;
}
void NodeAnimation::clear(uint node)
{
    debug_assert(isAnimated(node), node);

    for (auto& iterKey : m_keys)
        iterKey.second[node] = DecomposedNode();
    m_animatedNodes[node] = false;
    m_global = false;
    m_fullAnimated = false;
}
void NodeAnimation::finalize(const NodeHierarchy& hierarchy)
{
    for (auto& iterKey : m_keys)
        hierarchy.update(iterKey.second);
    m_global = true;
}
void NodeAnimation::getKeys(double time,
                            const DecomposedNodeArray* &left,
                            const DecomposedNodeArray* &right,
                            float& factor) const
{
    auto iterLeft = m_keys.upper_bound(time);
    if (iterLeft == m_keys.begin())
    {
        left = &iterLeft->second;
        right = &iterLeft->second;
        factor = 0;
        return;
    }

    auto iterRight = iterLeft--;
    if (iterRight == m_keys.end())
    {
        left = &iterLeft->second;
        right = &iterLeft->second;
        factor = 1;
        return;
    }

    left = &iterLeft->second;
    right = &iterRight->second;
    factor = static_cast<float>((time - iterLeft->first) / (iterRight->first - iterLeft->first));
}
shared_ptr<NodeAnimation> NodeAnimation::transfer(const NodeHierarchy& from,
                                                  const NodeHierarchy& to) const
{
    auto newAnim = make_shared<NodeAnimation>(to.numNodes());

    DecomposedNodeArray source;
    for (auto& iterKey : m_keys)
    {
        auto time = iterKey.first;
        source.clear();

        // Update
        for (auto& local : iterKey.second)
            source.push_back(local);
        from.update(source);

        // Transfer
        for (uint iterNode = 0; iterNode < to.numNodes(); ++iterNode)
        {
            auto& node = to.getNode(iterNode);
            auto proto = from.findNode(node.name.c_str());
            if (!proto)
                return nullptr;
            bool isRoot = node.parent->index == -1;

            Transform localPose = isRoot ? source[proto->index].global : source[proto->index].local;
            newAnim->addKey(time, iterNode, localPose, none);
        }
    }
    newAnim->finalize(to);
    return newAnim;
}
shared_ptr<NodeAnimation> NodeAnimation::extractStable(const set<uint>& nodes) const
{
    auto newAnim = make_shared<NodeAnimation>(*this);
    for (uint nodeIndex = 0; nodeIndex < m_numNodes; ++nodeIndex)
        if (nodes.count(nodeIndex) == 0)
            newAnim->clear(nodeIndex);
    return newAnim;
}
shared_ptr<NodeAnimation> NodeAnimation::extractReindex(const vector<uint>& nodes) const
{
    auto newAnim = make_shared<NodeAnimation>(nodes.size());
    for (auto& key : m_keys)
    {
        auto time = key.first;
        auto& frames = key.second;
        uint index = 0;
        for (auto& node : nodes)
        {
            newAnim->addKey(time, index, frames[node].local, none);
            ++index;
        }
    }
    return newAnim;
}
shared_ptr<NodeAnimation> NodeAnimation::cut(double begin, double end,
                                             bool rescale) const
{
    // Copy
    auto newAnim = make_shared<NodeAnimation>(*this);

    // Cut
    auto iterFrom = m_keys.lower_bound(begin);
    auto iterTo = m_keys.upper_bound(end);

    auto iterLast = iterTo;
    double beginTime = iterFrom->first;
    double newDuration = (--iterLast)->first - iterFrom->first;

    newAnim->m_keys.clear();
    for (auto& key : make_pair(iterFrom, iterTo))
    {
        double time = key.first - beginTime;
        if (rescale)
            time /= newDuration;
        newAnim->m_keys.emplace(time, key.second);
    }

    // Empty
    if (newAnim->m_keys.empty())
        return nullptr;

    return newAnim;
}
void NodeAnimation::updateNode(uint node, const vector<Transform>& poses)
{
    debug_assert(m_keys.size() <= poses.size(), "too few keys");
    debug_assert(isAnimated(node), node);

    uint index = 0;
    for (auto& iterKey : m_keys)
        iterKey.second[node] = poses[index++];
}


// Gets impl
void NodeAnimation::TransformGetter::operator ()(ComposedNodeArray& dest,
    const DecomposedNodeArray& left, DecomposedNodeArray const& /*right*/,
    uint destIndex, uint srcIndex, float /*factor*/) const
{
    auto& l = left[srcIndex];
    dest[destIndex].local = l.local.matrix();
    dest[destIndex].global = l.global.matrix();
}
void NodeAnimation::TransformLerper::operator ()(ComposedNodeArray& dest,
    const DecomposedNodeArray& left, const DecomposedNodeArray& right,
    uint destIndex, uint srcIndex, float factor) const
{
    auto& l = left[srcIndex];
    auto& r = right[srcIndex];
    dest[destIndex].local = lerp(l.local.matrix(), r.local.matrix(), factor);
    dest[destIndex].global = lerp(l.global.matrix(), r.global.matrix(), factor);
}
void NodeAnimation::TransformMixer::operator ()(ComposedNodeArray& dest,
    const DecomposedNodeArray& left, const DecomposedNodeArray& right,
    uint destIndex, uint srcIndex, float factor) const
{
    auto& l = left[srcIndex];
    auto& r = right[srcIndex];
    dest[destIndex].local = Transform{l.local, r.local, factor}
    .matrix();
    dest[destIndex].global = Transform{l.global, r.global, factor}
    .matrix();
}
template<class Getter>
void NodeAnimation::posesCombine(const Getter& get, double time,
                                 ComposedNodeArray& dest,
                                 const uint* reindex) const
{
    if (dest.size() < m_numNodes)
        dest.resize(m_numNodes);

    const DecomposedNodeArray *left, *right;
    float factor;
    getKeys(time, left, right, factor);

    if (!reindex)
    {
        for (uint iterNode = 0; iterNode < m_numNodes; ++iterNode)
        {
            if (!isAnimated(iterNode))
                continue;
            get(dest, *left, *right, iterNode, iterNode, factor);
        }
    }
    else
    {
        for (uint iterNode = 0; iterNode < m_numNodes; ++iterNode)
        {
            if (!isAnimated(iterNode))
                continue;
            get(dest, *left, *right, reindex[iterNode], iterNode, factor);
        }
    }
}


// Gets
void NodeAnimation::getPoses(double time, ComposedNodeArray& dest) const
{
    posesCombine(TransformGetter{}, time, dest, nullptr);
}
void NodeAnimation::lerpPoses(double time, ComposedNodeArray& dest) const
{
    posesCombine(TransformLerper{}, time, dest, nullptr);
}
void NodeAnimation::mixPoses(double time, ComposedNodeArray& dest) const
{
    posesCombine(TransformMixer{}, time, dest, nullptr);
}


// Gets re-mapped
void NodeAnimation::getPoses(double time, ComposedNodeArray& dest,
                             const vector<uint>& reindex) const
{
    posesCombine(TransformGetter{}, time, dest, reindex.data());
}
void NodeAnimation::lerpPoses(double time, ComposedNodeArray& dest,
                              const vector<uint>& reindex) const
{
    posesCombine(TransformLerper{}, time, dest, reindex.data());
}
void NodeAnimation::mixPoses(double time, ComposedNodeArray& dest,
                             const vector<uint>& reindex) const
{
    posesCombine(TransformMixer{}, time, dest, reindex.data());
}


// Get single
Transform const& NodeAnimation::getLocalPose(uint node, double time) const
{
    debug_assert(isAnimated(node), node);

    const DecomposedNodeArray *left, *right;
    float factor;
    getKeys(time, left, right, factor);

    return left->at(node).local;
}
Transform NodeAnimation::lerpLocalPose(uint node, double time) const
{
    debug_assert(isAnimated(node), node);

    const DecomposedNodeArray *left, *right;
    float factor;
    getKeys(time, left, right, factor);

    return Transform{left->at(node).local, right->at(node).local, factor};
}
Transform const& NodeAnimation::getGlobalPose(uint node, double time) const
{
    debug_assert(isGlobal());
    debug_assert(isAnimated(node), node);

    const DecomposedNodeArray *left, *right;
    float factor;
    getKeys(time, left, right, factor);

    return left->at(node).global;
}
Transform NodeAnimation::lerpGlobalPose(uint node, double time) const
{
    debug_assert(isGlobal());
    debug_assert(isAnimated(node), node);

    const DecomposedNodeArray *left, *right;
    float factor;
    getKeys(time, left, right, factor);

    return Transform{left->at(node).global, right->at(node).global, factor};
}
void NodeAnimation::traceLocalPoses(uint node, vector<Transform>& dest) const
{
    debug_assert(isAnimated(node), node);

    for (auto& iterKey : m_keys)
    {
        auto& pose = iterKey.second[node].local;
        dest.push_back(pose);
    }
}
void NodeAnimation::traceGlobalPoses(uint node, vector<Transform>& dest) const
{
    debug_assert(isGlobal());
    debug_assert(isAnimated(node), node);

    for (auto& iterKey : m_keys)
    {
        auto& pose = iterKey.second[node].global;
        dest.push_back(pose);
    }
}
