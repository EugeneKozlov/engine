/** @file ls/geom/math.h
 *  @brief 3D geometry math
 */
#pragma once
#include "ls/common.h"

namespace ls
{
    /** 3D-mathematics
     *  @ingroup LsGeom
     */
    namespace math3
    {
        /// @brief Point type
        using point = float3;
        /// @brief Normal type
        using normal = float3;
        /// @brief Scalar type
        using scalar = float;
        /// @brief Natural number
        using natural = uint;
        /// @brief Scalar epsilon
        inline scalar epsilon()
        {
            return std::numeric_limits<scalar>::epsilon();
        }
        struct segment;
        struct line;
        struct ray;
        struct plane;
        /// @brief Segment
        struct segment
        {
            /// @brief Ctor
            segment() = default;
            /// @brief Ctor by value
            segment(const point& a, const point& b)
            : a(a)
            , b(b)
            {
            }
            /// @brief First point
            point a;
            /// @brief Second point
            point b;
            /// @brief Put point on segment
            point put(scalar k) const
            {
                return lerp(a, b, k);
            }
            /// @brief Get length
            scalar length() const
            {
                return distance(a, b);
            }
            /// @brief Get direction
            normal direction() const
            {
                return normalize(b - a);
            }
        };
        /// @brief Ray
        struct ray
        {
            /// @brief Ctor
            ray() = default;
            /// @brief Ctor by value
            ray(const point& p, const normal& d)
            : p(p)
            , d(d)
            {
            }
            /// @brief From points
            static ray fromPoints(const point& a, const point& b)
            {
                return ray(a, normalize(b - a));
            }
            /// @brief From segment
            static ray fromSegment(const segment& s)
            {
                return fromPoints(s.a, s.b);
            }
            /// @brief Origin
            point p;
            /// @brief Direction
            normal d;
            /// @brief Put point on ray
            point put(scalar k) const
            {
                return p + d*k;
            }
        };
        /// @brief Line
        struct line
        {
            /// @brief Ctor
            line() = default;
            /// @brief Ctor by value
            line(const point& p, const normal& d)
            : p(p)
            , d(d)
            {
            }
            /// @brief Origin
            point p;
            /// @brief Direction
            normal d;
            /// @brief Put point on line
            point put(scalar k) const
            {
                return p + d*k;
            }
            /// @brief From two points
            static line fromPoints(const point& a, const point& b)
            {
                return line(a, normalize(b - a));
            }
            /// @brief From segment
            static line fromSegment(const segment& s)
            {
                return fromPoints(s.a, s.b);
            }
        };
        /// @brief Plane
        struct plane
        {
            /// @brief Ctor
            plane() = default;
            /// @brief Ctor by value
            plane(const normal& n, scalar d)
            : n(n)
            , d(d)
            {
            }
            /// @brief Normal
            normal n;
            /// @brief Dist
            scalar d;
            /// @brief Get plane by @a normal and @a point
            static plane fromNormalPoint(const normal& normal, const point& point)
            {
                plane p;
                p.n = normalize(normal);
                p.d = -dot(p.n, point);
                return p;
            }
        };
        /// @brief Sphere
        struct sphere
        {
            /// @brief Ctor
            sphere() = default;
            /// @brief Ctor by value
            sphere(const point& p, scalar r)
            : p(p)
            , r(r)
            {
            }
            /// @brief Center
            point p;
            /// @brief Radius
            float r;
        };
        /// @brief Plane-vector distance
        inline scalar distancePointPoint(const point& first,
                                         const point& second)
        {
            return distance(first, second);
        }
        /// @brief Plane-point distance
        inline scalar distancePlanePoint(const plane& plane,
                                         const point& point)
        {
            return dot(float4(plane.n, plane.d), float4(point, 1));
        }
        /// @brief Vector-vector angle
        inline scalar angleVectorVector(const normal& first,
                                        const normal& second,
                                        const normal& dir)
        {
            normal axb = cross(first, second);
            scalar cosa = dot(first, second);
            scalar sign = dot(dir, axb);
            scalar angle = atan2f(length(axb), cosa);
            if (sign < 0)
                angle = -angle;
            return angle;
        }
        /// @brief Get plane-line intersection
        inline optional<scalar> intersectPlaneLine(const plane& plane, const line& line)
        {
            scalar d = distancePlanePoint(plane, line.p);
            scalar k = dot(plane.n, line.d);
            if (abs(d) < epsilon())
                return (scalar) 0;
            else if (abs(k) < epsilon())
                return none;
            else
                return -d / k;
        }
        /// @brief Get plane-ray intersection
        inline optional<scalar> intersectPlaneRay(const plane& plane,
                                                  const ray& ray)
        {
            auto k = intersectPlaneLine(plane, line{ray.p, ray.d});
            if (!k || *k < 0)
                return none;
            else
                return k;
        }
        /// @brief Get plane-segment intersection
        inline optional<scalar> intersectPlaneSegment(const plane& plane,
                                                      const segment& segment)
        {
            auto k = intersectPlaneLine(plane, line::fromPoints(segment.a, segment.b));
            scalar l = segment.length();
            if (!k || *k < 0 || *k > l)
                return none;
            else
                return *k / l;
        }
        /// @brief Get sphere-sphere intersection
        inline optional<sphere> intersectSphereSphere(const sphere& first,
                                                      const sphere& second)
        {
            scalar d = distancePointPoint(first.p, second.p);
            scalar r1 = first.r;
            scalar r2 = second.r;
            scalar a = d * d - r1 * r1 - r2*r2;
            scalar rr = 4 * r1 * r1 * r2 * r2 - a*a;
            scalar r = sqrt(max(0.0f, rr)) / (2 * d);
            scalar kk = r1 * r1 - r*r;
            scalar k = sqrt(max<scalar>(0.0f, kk)) / d;
            point p = lerp(first.p, second.p, k);
            return sphere{p, r};
        }
        /// @brief Get sphere-ray intersection
        inline optional<scalar> intersectSphereRay(const sphere& sphere,
                                                   const ray& ray)
        {
            normal rs = sphere.p - ray.p;
            scalar k = dot(rs, ray.d);
            if (k < 0)
                return none;
            point c = ray.p + ray.d * k;
            scalar m = distance(c, sphere.p);
            if (m > sphere.r)
                return none;
            scalar q = sqrt(max<scalar>(0, sphere.r * sphere.r - m * m));
            if (k - q < 0)
                k += q;
            else
                k -= q;
            return k;
        }
        /// @brief Get sphere-segment intersection
        inline optional<scalar> intersectSphereSegment(const sphere& sphere,
                                                       const segment& segment)
        {
            normal d = segment.b - segment.a;
            scalar l = length(d);
            ray r = ray(segment.a, d / l);
            auto k = intersectSphereRay(sphere, r);
            if (!k)
                return none;
            if (*k > l)
                return none;
            return *k / l;
        }
        /// @brief Project point on line
        inline scalar projectPointOnLine(const line& line,
                                         const point& point)
        {
            plane p = plane::fromNormalPoint(line.d, point);
            return *intersectPlaneLine(p, line);
        }
        /// @brief Project point on segment
        inline scalar projectPointOnSegment(const segment& segment,
                                            const point& point)
        {
            plane p = plane::fromNormalPoint(segment.direction(), point);
            return *intersectPlaneSegment(p, segment);
        }
        /// @brief Calculate 3rd triangle side
        inline scalar triangleSide(scalar first, scalar second, scalar cosa)
        {
            return sqrt(first * first + second * second - 2 * first * second * cosa);
        }
        /// @brief Get line by points
        inline line computeLineByPoints(const point* points, natural num)
        {
            point x, y, xx, xy;
            for (natural i = 0; i < num; i++)
            {
                scalar tx = static_cast<scalar>(i);
                point ty = points[i];
                x += tx;
                xx += tx*tx;
                y += ty;
                xy += ty*tx;
            };

            scalar nums = static_cast<scalar>(num);
            x /= nums;
            xx /= nums;
            y /= nums;
            xy /= nums;

            point d = xx - x*x;
            point a = (xy - x * y) / d;
            point b = (xx * y - x * xy) / d;
            scalar t = static_cast<scalar>(num) - 1;
            return line::fromPoints(b, t * a + b);
        }
        /// @brief Get segment by points
        inline segment computeSegmentByPoints(const point* points, natural num)
        {
            line line = computeLineByPoints(points, num);
            point begin = line.put(projectPointOnLine(line, points[0]));
            point end = line.put(projectPointOnLine(line, points[num - 1]));
            return segment(begin, end);
        }
    }
}