/** @file ls/import/model_import.h
 *  @brief Model and animation importer
 */
#pragma once
#include "ls/common.h"

namespace ls
{
    class StreamInterface;
    class NodeHierarchy;
    class NodeAnimation;

    /** @addtogroup LsImport
     *  @{
     */
    /** Model importer
     */
    class ModelImporterInterface
        : Noncopyable
    {
    public:
        /// @brief Dtor
        virtual ~ModelImporterInterface()
        {
        }
        /** Load hierarchy.
         *  @throw InvalidDataException
         *  @param rootNode
         *    Hierarchy root node
         *  @param global
         *    Set to load in global space; load in local space otherwise
         *  @return Hierarchy, null if not found
         */
        virtual shared_ptr<NodeHierarchy> loadHierarchy(const string& rootNode, bool global) = 0;
        /** Load animation
         *  @throw InvalidDataException
         *  @param hierarchy
         *    Animation will be applicable to specified hierarchy
         *  @param animName
         *    Animation name; empty to load first
         *  @param rootNode
         *    Animation root node; empty to load global animation
         *  @return Animation, null if not found
         */
        virtual shared_ptr<NodeAnimation> loadAnimation(const NodeHierarchy& hierarchy,
                                                        const string& animName,
                                                        const string& rootNode) = 0;
    };
    /** Import hierarchy from file
     */
    shared_ptr<ModelImporterInterface> importModel(StreamInterface& file);
    /// @}
}
