#include "pch.h"
#include "ls/import/font_import.h"
#include "ls/xml/common.h"

using namespace ls;
vector<shared_ptr<Font> > ls::importFont(const string& source, Texture2DHandle textureId)
{
    using namespace pugi;
    vector<shared_ptr<Font> > fonts;


    xml_document document;
    document.load(source.c_str());
    for (xml_node& nodeFontList : document.children("fontList"))
    {
        for (xml_node& nodeFont : nodeFontList.children("font"))
        {
            string fontName = loadAttrib(nodeFont, "name").as_string();
            string fontSize = loadAttrib(nodeFont, "size").as_string();
            fontSize.erase(fontSize.length() - 2);

            // Symbols
            vector<Symbol> symbols;
            for (xml_node& nodeSymbol : nodeFont.children("char"))
            {
                Symbol sym;
                sym.id = loadAttrib(nodeSymbol, "id").as_uint();
                sym.x = loadAttrib(nodeSymbol, "x").as_uint();
                sym.y = loadAttrib(nodeSymbol, "y").as_uint();
                sym.width = loadAttrib(nodeSymbol, "width").as_uint();
                sym.height = loadAttrib(nodeSymbol, "height").as_uint();
                sym.xoffset = loadAttrib(nodeSymbol, "Xoffset").as_uint();
                sym.yoffset = loadAttrib(nodeSymbol, "Yoffset").as_uint();
                sym.origWidth = loadAttrib(nodeSymbol, "OrigWidth").as_uint();
                sym.origHeight = loadAttrib(nodeSymbol, "OrigHeight").as_uint();
                symbols.push_back(sym);
            }

            // Add
            auto newFont = make_shared<Font>(fontName.c_str(),
                fromString<uint>(fontSize), symbols, textureId);
            fonts.push_back(newFont);
        }
    }


    return move(fonts);
}
