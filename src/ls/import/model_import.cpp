#include "pch.h"
#include "ls/import/model_import.h"
#include "ls/io/StreamInterface.h"
#include "ls/geom/NodeHierarchy.h"
#include "ls/geom/NodeAnimation.h"
#include "assimp/Importer.hpp"
#include "assimp/scene.h"
#include "assimp/postprocess.h"

using namespace ls;

namespace ls
{
    /// @brief Assimp model/anim/etc loader
    class AssimpModelImporter;
}
class ls::AssimpModelImporter
: public ModelImporterInterface
{
public:
    AssimpModelImporter(StreamInterface& file)
    {
        string source = file.text();

        uint flags
            = aiProcess_ValidateDataStructure
            | aiProcess_Triangulate
            | aiProcess_CalcTangentSpace
            | aiProcess_JoinIdenticalVertices
            | aiProcess_GenNormals
            | aiProcess_LimitBoneWeights

            | aiProcess_MakeLeftHanded
            | aiProcess_FlipUVs
            | aiProcess_FlipWindingOrder;

        m_scene = m_importer.ReadFileFromMemory(source.data(), source.size(), flags);
        LS_THROW(m_scene, IoException, m_importer.GetErrorString());

        m_hierarchy = loadHierarchy("", true);
    }
    virtual shared_ptr<NodeHierarchy> loadHierarchy(const string& rootNode, bool global) override;
    virtual shared_ptr<NodeAnimation> loadAnimation(const NodeHierarchy& hierarchy,
                                                    const string& animName, const string& rootNode) override;
private:
    static float4x4 convert(const aiMatrix4x4& mat)
    {
        return float4x4{mat.a1, mat.b1, mat.c1, mat.d1, mat.a2, mat.b2, mat.c2, mat.d2,
            mat.a3, mat.b3, mat.c3, mat.d3, mat.a4, mat.b4, mat.c4, mat.d4};
    }
    static float3 convert(const aiVector3D& vec)
    {
        return float3{vec.x, vec.y, vec.z};
    }
    static quat convert(const aiQuaternion& q)
    {
        return quat{q.x, q.y, q.z, q.w};
    }
    static float4x4 getGlobalTransform(const aiNode* node)
    {
        if (!node)
            return float4x4();
        else
            return convert(node->mTransformation) * getGlobalTransform(node->mParent);
    }
    void loadNode(NodeHierarchy& hier, aiNode* assNode, NodeHierarchy::Node* parent, const float4x4& transformLocal = float4x4{})
    {
        auto node = &hier.addNode(assNode->mName.C_Str(), convert(assNode->mTransformation) * transformLocal, parent);
        for (uint i = 0; i < assNode->mNumChildren; ++i)
        {
            auto assChild = assNode->mChildren[i];
            loadNode(hier, assChild, node);
        }
    }
    Transform lerpPose(map<double, Transform>& channel, double time) const
    {
        auto iterLeft = channel.upper_bound(time);
        if (iterLeft == channel.begin())
            return iterLeft->second;

        auto iterRight = iterLeft--;
        if (iterRight == channel.end())
            return iterLeft->second;

        auto& left = *iterLeft;
        auto& right = *iterRight;
        float k = static_cast<float>((time - left.first) / (right.first - left.first));

        return Transform{left.second, right.second, k};
    }
    shared_ptr<NodeAnimation> parseAnimation(const NodeHierarchy& hierarchy, const string& animName);
private:
    Assimp::Importer m_importer;
    const aiScene* m_scene = nullptr;
    shared_ptr<NodeHierarchy> m_hierarchy;
};
shared_ptr<NodeHierarchy>
AssimpModelImporter::loadHierarchy(const string& rootNode, bool global)
{
    // Find root
    auto assNode = rootNode.empty() ? m_scene->mRootNode : m_scene->mRootNode->FindNode(rootNode.c_str());
    if (!assNode)
        return nullptr;

    auto hierarchy = make_shared<NodeHierarchy>();

    auto rootTransform = global ? getGlobalTransform(assNode->mParent) : float4x4();
    loadNode(*hierarchy, assNode, nullptr, rootTransform);

    return hierarchy;
}
shared_ptr<NodeAnimation>
AssimpModelImporter::loadAnimation(const NodeHierarchy& hierarchy,
                                   const string& animName, const string& rootNode)
{
    auto animatedHierarchy = loadHierarchy(rootNode, true);
    auto animation = parseAnimation(*animatedHierarchy, animName);
    if (animation)
        return animation->transfer(*animatedHierarchy, hierarchy);
    else
        return nullptr;
}
shared_ptr<NodeAnimation>
AssimpModelImporter::parseAnimation(const NodeHierarchy& hierarchy,
                                    const string& animName)
{
    auto beginAnim = m_scene->mAnimations;
    auto endAnim = beginAnim + m_scene->mNumAnimations;
    auto ifName = [&animName](aiAnimation * assAnim)
    {
        return animName == assAnim->mName.C_Str();
    };
    // Find anim
    auto iterAnim = animName.empty() ? beginAnim : std::find_if(beginAnim, endAnim, ifName);
    if (iterAnim == endAnim)
        return nullptr;

    auto& assAnim = **iterAnim;
    auto numChannels = assAnim.mNumChannels;
    auto channels = assAnim.mChannels;
    auto numNodes = hierarchy.numNodes();
    auto animation = make_shared<NodeAnimation>(numNodes);

    // Load stored
    vector<map<double, Transform >> keys
    {
        numNodes
    };
    set<double> times;
    for (auto assChannel : make_pair(channels, channels + numChannels))
    {
        auto node = hierarchy.findNode(assChannel->mNodeName.C_Str());
        if (!node)
            continue;
        bool keyEqual = allequal(assChannel->mNumPositionKeys, assChannel->mNumRotationKeys, assChannel->mNumScalingKeys);
        LS_THROW(keyEqual, IoException, "Different number of keys");

        auto& map = keys[node->index];
        uint numKeys = assChannel->mNumPositionKeys;
        auto positions = assChannel->mPositionKeys;
        auto rotations = assChannel->mRotationKeys;
        auto scales = assChannel->mScalingKeys;

        for (uint iterKey = 0; iterKey < numKeys; ++iterKey)
        {
            bool timeEqual = allequal(positions[iterKey].mTime, rotations[iterKey].mTime, scales[iterKey].mTime);
            LS_THROW(timeEqual, IoException, "Different key times");
            auto time = positions[iterKey].mTime;
            auto pos = convert(positions[iterKey].mValue);
            auto rot = convert(rotations[iterKey].mValue);
            auto scale = convert(scales[iterKey].mValue);
            map[time] = Transform{pos, rot, scale};
            times.emplace(time);
        }
    }
    // Fill empty
    for (uint iterNode = 0; iterNode < numNodes; ++iterNode)
    {
        auto& channel = keys[iterNode];
        if (channel.empty())
            channel.emplace(0.0, hierarchy.getNode(iterNode).startLocal);
    }
    // Compile
    for (auto time : times)
    {
        for (uint iterNode = 0; iterNode < numNodes; ++iterNode)
            animation->addKey(time, iterNode, lerpPose(keys[iterNode], time), none);
    }
    animation->finalize(hierarchy);
    return animation;
}
shared_ptr<ModelImporterInterface> ls::importModel(StreamInterface& file)
{
    return make_shared<AssimpModelImporter>(file);
}


