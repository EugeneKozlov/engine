/** @file ls/import/font_import.h
 *  @brief Font importer
 */

#pragma once
#include "ls/common.h"
#include "ls/render/Font.h"

namespace ls
{
    /** @addtogroup LsImport
     *  @{
     */
    /** Import font from XML
     *  @param source
     *    Source XML text
     *  @param textureId
     *    Font texture ID
     *  @return Font
     */
    vector<shared_ptr<Font>> importFont(const string& source, Texture2DHandle textureId);
    /// @}
}