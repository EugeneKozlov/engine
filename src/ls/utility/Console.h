/** @file ls/utility/Console.h
 *  @brief Visual console
 */
#pragma once
#include "ls/common.h"
#include "ls/core/utf.h"
#include "ls/render/Plotter.h"
#include "ls/app/common.h"

namespace ls
{
    /** @addtogroup LsUtility
     *  @{
     */
    /** Visual console and command translator.
     *
     *  There are two modes:
     *  - Chat mode: Command messages should start with #CommandPrefix symbol,
     *    other messages are interpreted as text
     *  - Command mode: Text messages should start with #ChatPrefix symbol,
     *    other messages are interpreted as commands
     */
    class Console
    : Noncopyable
    , public LoggerInterface
    {
    public:
        /// @brief Chat callback
        function<void(const char* message)> onChat;
        /// @brief Command callback
        function<void(const char* command)> onCommand;

        /// @brief Limit for command cache
        static const uint MaxStoredCommands = 64;
        /// @brief Limit for stored lines
        static const uint MaxStoredLines = 64;
        /// @brief Internal
        enum class Color
        {
            /// @brief Background
            Back,
            /// @brief Command line welcome
            Welcome,
            /// @brief Command line text
            Input,
            /// @brief Tag symbol
            Tag,
            /// @brief Command name
            Command,
            /// @brief Command arguments
            Arguments,
            /// @brief Chat name
            Name,
            /// @brief Chat text
            Message,
            /// @brief Info message
            Info,
            /// @brief Important info
            Attention,
            /// @brief Counter
            COUNT
        };
        /// @brief Char prefix
        static const char ChatPrefix = '!';
        /// @brief Command prefix
        static const char CommandPrefix = '/';
    public:
        /** Ctor
         *  @param plotter
         *    Plotter, @c null to disable rendering
         *  @param chatMode
         *    @c true to use chat mode, @c else - in command mode
         */
        Console(Plotter* plotter, bool chatMode);
        /// @brief Dtor
        ~Console();
        /** Set defaults.
         *  - Relative height is 0.4
         *  - Borders is 5 pixels
         *  - Colors
         *    + <b>Background</b> is semi-transparent dark grey
         *    + <b>Welcome ">>"</b> is yellow
         *    + <b>Input line</b> is white
         *    + <b>Tag</b> is dark green
         *    + <b>Command</b> is dark blue
         *    + <b>Arguments</b> is blue
         *    + <b>Chat name</b> is green
         *    + <b>Chat message</b> is white
         *    + <b>Info</b> message is yellow
         *    + <b>Attention</b> message is red
         */
        void reset();
        /// @brief Resize window
        void resize(uint width, uint height);
        /// @brief Render console
        void render();
        /// @brief Clear console
        void clear();
        /// @brief Show console
        void show();
        /// @brief Hide console
        void hide();
        /// @brief Swap console view (visible <-> hidden)
        void swap();
        /** Register ignored symbol
         *  @param symbol
         */
        void ignoreSymbol(uint symbol);
        /// @brief Process commands
        void process();

        /** @name Typing
         *  @{
         */
        /// @brief Add text character
        void type(uint symbol);
        /// @brief Add control character
        void control(KeyCode control);
        /// @brief Enter text
        void chat(const char* command);
        /// @brief Enter command
        void enter(const char* command);
        /// @brief Write colored wide string
        void writeString(const uint* string, const uint* colors);
        /// @brief Write colored string
        void writeString(const char* string, const uint* colors);
        /// @brief Write command message
        void writeCommand(const char* tag, const char* name, const char* arguments);
        /// @brief Write chat message as @a name
        void writeChat(const char* name, const char* text);
        /// @brief Write info
        void writeInfo(const char* text, bool attention);
        /** Write any
         *  @param numElements
         *    Number of string
         *  @param ...
         *    Pairs of (const char*, uint) contain string and color
         */
        void write(uint numElements, ...);
        /// @}

        /** @name Setters
         *  @{
         */
        /// @brief Set relative height
        void setRelativeHeight(float relativeHeight);
        /// @brief Set text offsets
        void setBorders(const float2& borders);
        /// @brief Set text font
        void setFont(shared_ptr<Font> font);
        /// @brief Set color
        void setColor(Color target, uint color);
        /// @brief Set color
        void setColor(Color target, const float4& color);
        /// @}

        /** @name Gets
         *  @{
         */
        /// @brief Check visibility
        bool isVisible() const;
        /// @}
    private:
        /// @brief Put command from list on the top
        void topCommand();
        /// @brief Get current command
        WideString& currentCommand();
        /// @brief Prepare renderable for command line
        void prerenderCommand();
        /// @brief Get number of lines in string
        uint accumulateLine(uint line, uint maxWidth);
        /// @brief Prepare renderable for text
        void prerenderText();
        /// @brief Calculate number of lines
        void countLines();
        /// @brief Implementation
        virtual void write(const LogMessageLevel level,
                           const char* fullMessage,
                           const char* textOnly) override;
    private:
        set<uint> m_ignoredSymbols;

        Plotter* m_plotter;
        shared_ptr<Font> m_font;

        bool m_chatMode = false;
        float2 m_size = float2();
        float m_relativeHeight = 1.0f;
        uint m_linesCount = 0;
        float2 m_borders = float2();
        uint m_palette[(uint) Color::COUNT];
        bool m_enabled = false;

        uint m_cursor = 0;
        vector<WideString> m_commandList;
        uint m_currentCommand = 0;
        vector<WideString> m_linesText;
        vector<WideString> m_linesColors;

        vector<string> m_messages;

        uint m_linesToRender = 0;
        WideString m_renderCommand;
        WideString m_renderCommandColors;
        WideString m_renderText;
        WideString m_renderTextColors;
    };
    /// @}
}
