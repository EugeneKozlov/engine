#include "pch.h"
#include "ls/utility/Console.h"

using namespace ls;
Console::Console(Plotter* plotter, bool chatMode)
    : m_plotter(plotter)
    , m_chatMode(chatMode)
{
    verbosity = LogMessageLevel::Notice;
    std::fill(m_palette, m_palette + (uint) Color::COUNT, 0);
    reset();
    m_commandList.push_back(WideString{});
}
void Console::reset()
{
    m_relativeHeight = 0.4f;
    m_borders = float2(5.0f);
    setColor(Color::Back,
             float4(0.3f, 0.3f, 0.3f, 0.6f));
    setColor(Color::Welcome,
             float4(1.0f, 1.0f, 0.0f, 1.0f));
    setColor(Color::Input,
             float4(1.0f, 1.0f, 1.0f, 1.0f));
    setColor(Color::Tag,
             float4(1.0f, 0.0f, 1.0f, 1.0f));
    setColor(Color::Command,
             float4(0.6f, 0.6f, 1.0f, 1.0f));
    setColor(Color::Arguments,
             float4(0.75f, 0.75f, 1.0f, 1.0f));
    setColor(Color::Name,
             float4(0.0f, 1.0f, 0.0f, 1.0f));
    setColor(Color::Message,
             float4(1.0f, 1.0f, 1.0f, 1.0f));
    setColor(Color::Info,
             float4(1.0f, 1.0f, 0.3f, 1.0f));
    setColor(Color::Attention,
             float4(1.0f, 0.25f, 0.25f, 1.0f));
}
Console::~Console()
{
}
void Console::resize(uint width, uint height)
{
    m_size = float2(float(width), float(height));
    countLines();
    prerenderCommand();
    prerenderText();
}
void Console::show()
{
    m_enabled = true;
}
void Console::hide()
{
    m_enabled = false;
}
void Console::ignoreSymbol(uint symbol)
{
    m_ignoredSymbols.emplace(symbol);
}
void Console::process()
{
    for (string& str : m_messages)
    {
        if (str.empty())
            continue;
        bool chatMode = !m_chatMode ? (str[0] == ChatPrefix) : (str[0] != CommandPrefix);
        const char* text = str.c_str() + (m_chatMode != chatMode);

        if (!chatMode)
        {
            while (*text == ' ')
                ++text;
            if (onCommand)
                onCommand(text);
        }
        else
        {
            if (onChat)
                onChat(text);
        }
    }
    m_messages.clear();
}


// Render and control
void Console::topCommand()
{
    if (m_currentCommand + 1 != m_commandList.size() - 1)
    {
        m_commandList.back() = m_commandList[m_currentCommand];
        m_currentCommand = m_commandList.size() - 1;
    }
}
WideString& Console::currentCommand()
{
    return m_commandList[m_currentCommand];
}
void Console::prerenderCommand()
{
    if (!m_plotter)
        return;

    static const WideString welcome = stringToWide(">> ");
    uint space = uint(m_size.x - m_borders.x * 2) - m_font->length(welcome.c_str());
    uint symbols = m_font->symbolsByLengthBackward(currentCommand().c_str(),
                                                   m_cursor,
                                                   space)
        + currentCommand().size()
        - m_cursor;

    uint skip = (symbols < currentCommand().size())
        ? ((uint) currentCommand().size() - symbols)
        : 0;

    m_renderCommand = welcome;
    m_renderCommandColors.assign(welcome.length(),
                                 m_palette[(uint) Color::Welcome]);
    m_renderCommand.append(currentCommand().begin() + skip,
                           currentCommand().end());
    m_renderCommand.insert(m_cursor + welcome.length() - skip, 1, '\b');
    m_renderCommandColors.append(currentCommand().size() - skip + 1,
                                 m_palette[(uint) Color::Input]);
}
void Console::type(uint symbol)
{
    if (!m_enabled || m_ignoredSymbols.count(symbol) != 0)
        return;

    // Put symbol
    if (symbol >= ' ')
    {
        topCommand();
        currentCommand().insert(currentCommand().begin() + m_cursor++,
                                symbol);
        prerenderCommand();
    }
}
void Console::control(KeyCode symbol)
{
    if (!m_enabled)
        return;
    switch (symbol)
    {
    case Key::Left:
        if (m_cursor)
            m_cursor--;
        break;
    case Key::Right:
        if (m_cursor < currentCommand().size())
            m_cursor++;
        break;
    case Key::Up:
        if (m_currentCommand)
        {
            m_currentCommand--;
            m_cursor = currentCommand().size();
        }
        break;
    case Key::Down:
        if (m_currentCommand + 1 < m_commandList.size())
        {
            m_currentCommand++;
            m_cursor = currentCommand().size();
        }
        break;
    case Key::Backspace:
        if (m_cursor)
        {
            topCommand();
            currentCommand().erase(m_cursor - 1, 1);
            m_cursor--;
        }
        break;
    case Key::Delete:
        if (m_cursor < currentCommand().size())
        {
            topCommand();
            currentCommand().erase(m_cursor, 1);
        }
        break;
    case Key::Enter:
        if (currentCommand().size())
        {
            m_messages.push_back(wideToString(currentCommand()));
            if (m_currentCommand + 1 == m_commandList.size())
            {
                m_commandList.push_back(WideString{});
                if (m_commandList.size() > MaxStoredCommands)
                {
                    uint toErase = m_commandList.size() - MaxStoredCommands;
                    m_commandList.erase(m_commandList.begin(),
                                        m_commandList.begin() + toErase);
                }
            }
        }
        m_currentCommand = m_commandList.size() - 1;
        currentCommand() = WideString{};
        m_cursor = 0;
        break;
    default:;
    }
    prerenderCommand();
}
uint Console::accumulateLine(uint theLine, uint theMaxWidth)
{
    const uint* pt = m_linesText[theLine].c_str();
    const uint* pc = m_linesColors[theLine].c_str();
    uint n = 0;
    while (*pt)
    {
        uint m = m_font->symbolsByLength(pt, theMaxWidth);
        m_renderText.append(pt, pt + m);
        m_renderText += '\n';
        m_renderTextColors.append(pc, pc + m);
        m_renderTextColors.append(1, 0);
        n++;
        pt += m;
        pc += m;
        if (*pt == 10) pt++;
    }
    return n;
}
void Console::prerenderText()
{
    if (!m_plotter)
        return;

    uint numLines = (uint) m_linesText.size();
    uint toErase = (numLines > MaxStoredLines) ? (numLines - MaxStoredLines) : 0;
    m_linesText.erase(m_linesText.begin(), m_linesText.begin() + toErase);
    m_linesColors.erase(m_linesColors.begin(), m_linesColors.begin() + toErase);
    numLines -= toErase;

    uint maxWidth = (uint) (m_size.x - m_borders.x * 2);
    m_renderText.clear();
    m_renderTextColors.clear();
    uint realNumLines = 0;
    for (uint iLine = 0; iLine < numLines; iLine++)
        realNumLines += accumulateLine(iLine, maxWidth);

    m_linesToRender = std::min(realNumLines, m_linesCount - 1);
    toErase = realNumLines - m_linesToRender;
    for (uint i = 0; i < toErase; i++)
    {
        uint eraser = (uint) m_renderText.find('\n') + 1;
        m_renderText.erase(0, eraser);
        m_renderTextColors.erase(0, eraser);
    }
}
void Console::countLines()
{
    m_linesCount = uint(m_size.y * m_relativeHeight - m_borders.y * 2)
        / m_font->height();
}
void Console::setRelativeHeight(float relativeHeight)
{
    m_relativeHeight = relativeHeight;
    countLines();
}
void Console::setColor(Color target, uint color)
{
    m_palette[(uint) target] = color;
}
void Console::setColor(Color target, const float4& color)
{
    m_palette[(uint) target] = rgb2hex(color);
}
void Console::setBorders(const float2& borders)
{
    m_borders = borders;
    countLines();
}
void Console::setFont(shared_ptr<Font> font)
{
    debug_assert(!!font);

    m_font = font;
    prerenderCommand();
    prerenderText();
}
void Console::clear()
{
    m_linesText.clear();
    m_linesColors.clear();
    prerenderText();
}
void Console::swap()
{
    m_enabled = !m_enabled;
}
void Console::render()
{
    if (!m_enabled || !m_plotter)
        return;

    debug_assert(m_font);

    // Init
    m_plotter->resetCamera();

    // Quad
    m_plotter->begin(Plotter::DrawColoredTris, nullptr, false, true);
    float lineHeight = float(m_font->height());
    m_plotter->plotQuad(float2(0.0, 0.0),
                        float2(m_size.x, m_linesCount * lineHeight + m_borders.y * 2),
                        m_palette[(uint) Color::Back]);
    m_plotter->end();

    // Command line
    m_plotter->begin(Plotter::DrawText2D, m_font->texture(), false, false);
    int2 commandPos(int(m_borders.x),
                    int((m_linesCount - 1) * lineHeight + m_borders.y));
    m_plotter->plotTextWide(*m_font,
                            m_renderCommand.c_str(),
                            commandPos,
                            m_renderCommandColors.c_str());

    // Console
    int2 textPos(int(m_borders.x),
                 int(m_borders.y + (m_linesCount - m_linesToRender - 1) * lineHeight));
    m_plotter->plotTextWide(*m_font,
                            m_renderText.c_str(),
                            textPos,
                            m_renderTextColors.c_str());
    m_plotter->end();
}
bool Console::isVisible() const
{
    return m_enabled;
}


// Input
void Console::writeString(const uint* string, const uint* colors)
{
    m_linesText.push_back(string);
    m_linesColors.push_back(colors);
    prerenderText();
}
void Console::writeString(const char* string, const uint* colors)
{
    m_linesText.push_back(stringToWide(string));
    m_linesColors.push_back(colors);
    prerenderText();
}
void Console::write(uint numElements, ...)
{
    WideString string;
    WideString colors;
    // Process
    va_list args;
    va_start(args, numElements);
    for (uint elementIndex = 0; elementIndex < numElements; ++elementIndex)
    {
        uint color = va_arg(args, uint);
        const char* text = va_arg(args, const char*);
        WideString wideText = stringToWide(text);
        // Add
        string.append(wideText.begin(), wideText.end());
        colors.append(wideText.length(), color);
    }
    va_end(args);
    // Send
    writeString(string.c_str(), colors.c_str());
}
void Console::chat(const char* command)
{
    string text = m_chatMode ? string() : string(1, ChatPrefix);
    text += command;
    m_messages.push_back(text);
}
void Console::enter(const char* command)
{
    string text = m_chatMode ? string(1, CommandPrefix) : string();
    text += command;
    m_messages.push_back(text);
}
void Console::writeCommand(const char* tag, const char* name, const char* arguments)
{
    write(3,
          m_palette[(uint) Color::Tag], tag,
          m_palette[(uint) Color::Command], name,
          m_palette[(uint) Color::Arguments], arguments);
}
void Console::writeChat(const char* name, const char* text)
{
    write(3,
          m_palette[(uint) Color::Name], name,
          m_palette[(uint) Color::Name], ": ",
          m_palette[(uint) Color::Message], text);
}
void Console::writeInfo(const char* text, bool attention)
{
    write(1,
          m_palette[(uint) (attention ? Color::Attention : Color::Info)],
          text);
}
void Console::write(LogMessageLevel level,
                    const char* /*fullMessage*/,
                    const char* textOnly)
{
    bool warning = (uint) level >= (uint) LogMessageLevel::Warning;
    writeInfo(textOnly, warning);
    if (warning)
        m_enabled = true;
}
