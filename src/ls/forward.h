/** @file ls/forward.h
 *  @brief Forward declaration of @a Ls
 */
#pragma once
#include "ls/common.h"
#include "ls/gapi/common.h"
#include "ls/network/common.h"
#include "ls/scene/common.h"

namespace ls
{
    /** @addtogroup LsApp
     *  @{
     */
    class ApplicationInterface;
    class ApplicationSceneInterface;
    /// @}

    /** @addtogroup LsAsset
     *  @{
     */
    /// @}

    /** @addtogroup LsGapi
     *  @{
     */
    namespace gapi
    {
        class EffectParser;
    }
    class WindowInterface;
    /// @}

    /** @addtogroup LsGen
     *  @{
     */
    class ContentGenerator;
    class UniformRandom;
    class PoissonRandom;
    class TextureGenerator;
    class TreeGenerator;
    /// @}

    /** @addtogroup LsGeom
     *  @{
     */
    class Transform;
    struct ComposedNode;
    struct DecomposedNode;
    class GeometryNode;
    class NodeHierarchy;
    class NodeAnimation;
    /// Packed tiny node array
    using ComposedNodeArray = vector<ComposedNode>;
    /// Unpacked heavy node array
    using DecomposedNodeArray = vector<DecomposedNode>;

    class AnimationRectifier;
    /// @}

    /** @addtogroup LsIk
     *  @{
     */
    class Skeleton;
    class BipedSkeleton;

    struct AnimationEnvironment;
    class BipedWalkAnimation;

    class BipedWalkAnalyzer;
    /// @}

    /** @addtogroup LsNetwork
     *  @{
     */
    template <uint Size>
    class NetworkStream;
    /// Network message stream
    using NetworkMessage = NetworkStream<NetworkMessageMaxSize>;

    class TcpSocket;
    class TcpClient;
    class TcpServer;
    class UdpSocket;

    class WelcomeMessage;
    class AuthMessage;
    class RegisterMessage;
    class DeregisterMessage;
    class EstablishMessage;

    class Web;

    /// @}

    /** @addtogroup LsRender
     *  @{
     */
    class Plotter;
    /// @}

    /** @addtogroup LsScene
     *  @{
     */
    // Main
    class ObjectObserverInterface;
    class ObjectState;
    class AspectPrototype;
    class ObjectPrototype;
    class AspectInstance;
    class ObjectInstance;

    class ObjectsManager;
    class ObjectsFactory;

    // Aspects
    class PoseAspect;
    class PoseAspectPrototype;
    class MovableAspect;
    class MovableAspectPrototype;
    class InfoAspect;
    class InfoAspectPrototype;
    class PhysicalAspect;
    class PhysicalAspectPrototype;
    class RigidAspect;
    class RigidAspectPrototype;
    /// @}

    /** @addtogroup LsSimulation
     *  @{
     */
    struct RaycastInfo;
    struct RayhitInfo;

    struct ActorPosition;
    class ActorMovement;
    class ActorTrace;

    class ActorSimulation;
    class PhysicalProcessor;
    /// @}

    /** @addtogroup LsWorld
     *  @{
     */
    class WorldChunkObserver;
    class WorldChunkOwner;
    class WorldChunkManager;
    /// @}
}
