/// @file ls/common/debug.h
/// @brief Debug stack tracer
#pragma once

#include "ls/common/import.h"

namespace ls
{
	/// @brief Generate stack trace
	/// @return Stack trace string
	/// @ingroup LsCommon
    string stackTrace();
}