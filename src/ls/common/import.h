/// @file ls/common/import.h
/// @brief Import STL and Boost into main namespace
#pragma once

namespace ls
{
    using std::initializer_list;
    using std::move;
    using std::swap;

    using std::copy;
    using std::copy_n;
    using std::fill;
    using std::fill_n;
    using std::sort;
    using std::back_inserter;

    using std::min;
    using std::max;
    using std::floor;
    using std::ceil;
    using std::abs;

    using std::result_of;
    using std::enable_if;
    using std::enable_if_t;
    using std::is_enum;
    using std::is_base_of;
    using std::is_same;
    using std::is_floating_point;
    using std::is_integral;
    using std::is_signed;
    using std::is_unsigned;
    using std::is_arithmetic;
    using std::is_convertible;
    using std::underlying_type;
    using std::make_unsigned;
    using std::make_signed;
    using std::remove_reference;
    using std::reference_wrapper;
    using std::ref;
    using std::type_index;

    using std::numeric_limits;

    using std::pair;
    using std::make_pair;
    using std::tuple;
    using std::less;
    using std::equal_to;
    using std::hash;

    using std::shared_ptr;
    using std::weak_ptr;
    using std::make_shared;
    using std::allocate_shared;
    using std::unique_ptr;
    using std::make_unique;

    using std::string;

    using std::endl;
    using std::stringstream;

    using std::function;
    using std::bind;
    using std::placeholders::_1;
    using std::placeholders::_2;
    using std::placeholders::_3;
    using std::placeholders::_4;
    using std::placeholders::_5;
    using std::placeholders::_6;
    using std::placeholders::_7;
    using std::placeholders::_8;

    using boost::optional;
    using boost::none;
    using boost::any;
    using boost::any_cast;

    using std::packaged_task;
    using std::future;
    namespace this_thread = std::this_thread;
    using std::thread;
    using std::mutex;
    using std::recursive_mutex;
    using std::lock_guard;
    using std::unique_lock;
    using std::condition_variable;
    using std::atomic;

    using std::chrono::system_clock;
    using std::chrono::steady_clock;
    using std::chrono::high_resolution_clock;
    using std::chrono::duration_cast;
    using std::chrono::seconds;
    using std::chrono::milliseconds;
    using std::chrono::microseconds;
    using std::chrono::nanoseconds;

    using std::bitset;
    using std::array;
    using std::stack;
    using std::queue;
    using std::vector;
    using std::list;
    using std::deque;
    /// @}
}
