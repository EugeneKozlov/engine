#include "pch.h"
#include "ls/common/Logger.h"
#include "ls/common/String.h"
#include "ls/common/time.h"
#include "ls/common/Observable.h"

using namespace ls;

// Interface
LoggerInterface::LoggerInterface()
{
    LogManager::instance().addObserver(*this);
}
LoggerInterface::~LoggerInterface()
{
    LogManager::instance().removeObserver(*this);
}


// File logger
FileLogger::FileLogger(const string& fileName)
{
    verbosity = LogMessageLevel::Trace;

    auto time = currentTime();
    m_fileName = print(fileName, '_',
                       Dec(2), time.day, '.', time.month, '.',
                       Dec(4), time.year, '_',
                       Dec(2), time.hour, '-', time.min, '-', time.sec, ".log");
    // Open
    m_file = fopen(m_fileName.c_str(), "wt");
}
FileLogger::~FileLogger()
{
    if (m_file)
        fclose(m_file);
}
void FileLogger::write(LogMessageLevel const /*level*/,
                       const char* fullMessage,
                       const char* /*textOnly*/)
{
    if (!m_file)
        return;
    fprintf(m_file, fullMessage);
    fflush(m_file);
}


// Console logger
void ConsoleLogger::write(const LogMessageLevel level,
                          const char* /*fullMessage*/,
                          const char* textOnly)
{
    if (level != LogMessageLevel::Message)
        printf("\n%s", textOnly);
    else
        printf(textOnly);
}


// Debug output logger
#ifdef _MSC_VER
#    include <Windows.h>
#endif
void DebugOutputLogger::write(const LogMessageLevel level,
                              const char* /*fullMessage*/,
                              const char* textOnly)
{
#ifdef _MSC_VER
    if (level != LogMessageLevel::Message)
        OutputDebugStringA("* ");
    OutputDebugStringA(textOnly);
    if (level != LogMessageLevel::Message)
        OutputDebugStringA("\n");
#else
    (void) level;
    (void) textOnly;
#endif

}



// Manager
LogManager::LogManager()
{

}
LogManager::~LogManager()
{
    write(LogMessageLevel::Message, Endl());
}
uint LogManager::writeTags(LogMessageLevel type)
{
    m_buffer.clear();

    // Print nothing
    if (type == LogMessageLevel::Message)
        return 0;
    else
    {
        // Print tags
        auto time = currentTime();
        m_buffer.print(Endl(),
                       Dec(2), time.hour, ':', time.min, ':', time.sec, '~',
                       Dec(3), time.msec, ' ');
        m_buffer.print(asString(type));

        return m_buffer.length();
    }
}
void LogManager::flush(const LogMessageLevel type, const uint start, string* dest)
{
    // Copy
    const char* text = m_buffer.cstr() + start;
    if (dest)
        *dest = text;
    // Update observers
    for (LoggerInterface* observer : m_loggers)
        if ((uint) observer->verbosity <= (uint) type)
            observer->write(type, m_buffer.cstr(), text);
}