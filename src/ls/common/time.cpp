#include "pch.h"
#include "ls/common/time.h"
#include "ls/common/import.h"

using namespace ls;
DateTime ls::currentTime()
{
    static mutex thisMutex;

    const auto currentTime = system_clock::now();
    const auto encodedTime = system_clock::to_time_t(currentTime);

    lock_guard<mutex> locker{thisMutex};
    const tm decodedTime = *localtime(&encodedTime);
    const auto milliseconds = duration_cast<std::chrono::milliseconds>(currentTime.time_since_epoch()).count() % 1000;

    const DateTime ret = {
        (ushort) (1900 + decodedTime.tm_year),
        (ushort) (1 + decodedTime.tm_mon), (ushort) decodedTime.tm_mday,
        (ushort) decodedTime.tm_hour, (ushort) decodedTime.tm_min, (ushort) decodedTime.tm_sec,
        (ushort) milliseconds
    };
    return ret;
}
uint ls::relativeTime()
{
    const auto now = std::chrono::system_clock::now();
    const auto duration = duration_cast<milliseconds>(now.time_since_epoch());
    return (uint) (duration.count() % MaxUint);
}