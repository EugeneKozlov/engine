/// @file ls/common/concept.h
/// @brief Template concepts
#pragma once

#include "ls/common/import.h"
#include "ls/common/integer.h"

namespace ls
{
	/// @addtogroup LsCommon
	/// @{

    /// @brief Non-copyable concept
    using Noncopyable = boost::noncopyable;

    class String;
    /// @brief Printable concept
    struct PrintableConcept
    {
        /// @brief Print object to string
        void toString(String& str) const = delete;
    };
    /// @brief Test if printable
    template <class Object>
    struct IsPrintable : is_base_of<PrintableConcept, Object>
    {
    };
    /// @brief Check whether printable
    template <class Object>
    inline constexpr bool isPrintable() noexcept
    {
        return IsPrintable<Object>::value;
    }

    /// @brief Stream concept
    struct StreamConcept
    {
        /// @brief Write @a buffer with specified @a size
        void write(const void*buffer, uint size) = delete;
        /// @brief Read @a buffer with specified @a size
        void read(void*buffer, uint size) = delete;
    };
    /// @brief Test if stream
    template <class Object>
    struct IsStream : is_base_of<StreamConcept, Object>
    {
    };
    /// @brief Check whether stream
    template <class Object>
    inline constexpr bool isStream() noexcept
    {
        return IsStream<Object>::value;
    }

    /// @brief Serializable prototype
    struct SerializableConcept
    {
        /// @brief Serialize to @a stream
        template<class Stream>
        void serialize(Stream& stream) const = delete;
        /// @brief De-serialize from @a stream
        template<class Stream>
        void deserialize(Stream& stream) = delete;
        /// @brief Get size
        uint size() const = delete;
    };
    /// @brief Test if serializable
    template <class Object>
    struct IsSerializable : is_base_of<SerializableConcept, Object>
    {
    };
    /// @brief Check whether serializable
    template <class Object>
    inline constexpr bool isSerializable() noexcept
    {
        return IsSerializable<Object>::value;
    }
    /// @brief Empty serializable
    struct EmptySerializable : SerializableConcept
    {
        /// @brief Nop
        template<class Stream>
        inline void serialize(Stream& /*stream*/) const
        {
        }
        /// @brief Nop
        template<class Stream>
        inline void deserialize(Stream& /*stream*/)
        {
        }
        /// @brief Empty
        inline uint size() const
        {
            return 0;
        }
    };

    /// @brief Operable prototype
    struct OperableConcept
    {
        /// @brief Operate
        template <class Function>
        inline void operate(Function foo) = delete;
    };
    /// @brief Test if operable
    template <class Object>
    struct IsOperable : is_base_of<OperableConcept, Object>
    {
    };
    /// @brief Check whether operable
    template <class Object>
    inline constexpr bool isOperable() noexcept
    {
        return IsOperable<Object>::value;
    }
    /// @brief Operate mutable or immutable @a object by @a functor
    template<class Object, class Functor, class = enable_if_t<IsOperable<Object>::value>>
    inline void operate(const Object& object, Functor func)
    {
        const_cast<typename std::remove_const<Object>::type&> (object).operate(func);
    }

    /// @brief Disposable concept
    struct DisposableConcept
    {
        /// @brief Dispose
        void dispose() = delete;
    };
    /// @brief Auto disposer
    template <class Disposable, class = enable_if_t<std::is_base_of<DisposableConcept, Disposable>::value>>
    class Disposer
        : Noncopyable
    {
    public:
        /// @brief Ctor
        Disposer(Disposable& impl)
            : m_impl(impl)
        {
        }
        /// @brief Dtor
        ~Disposer()
        {
            if (m_active)
                m_impl.dispose();
        }
        /// @brief Deactivate
        void deactivate()
        {
            m_active = false;
        }
    private:
        Disposable& m_impl;
        bool m_active = false;
    };

    /// @brief Hashable concept
    struct HashableConcept
    {
        /// @brief Hash function
        uint hash() const noexcept = delete;
    };
    /// @brief Test if hashable
    template <class Object>
    struct IsHashable : std::is_base_of<HashableConcept, Object>
    {
    };
    /// @brief Check whether hashable
    template <class Object>
    inline constexpr bool isHashable() noexcept
    {
        return IsHashable<Object>::value;
    }

    /// @brief Hasher
    struct Hash
    {
        /// @brief Standard
        template <class Object>
        std::enable_if_t<IsHashable<Object>::value, std::size_t>
            operator()(const Object& object) const
        {
            return (std::size_t) object.hash();
        }
        /// @brief Concept
        template <class Object>
        std::enable_if_t<!IsHashable<Object>::value, std::size_t>
            operator()(const Object& object) const
        {
            return std::hash<Object>()(object);
        }
    };

    /// @brief Is Enum Class?
    template <class T>
    using IsEnumClass = std::integral_constant<bool, !std::is_convertible<T, int>::value && std::is_enum<T>::value>;

    /// @brief Enable if scalar
    template <class Scalar, class Type = void>
    using EnableIfScalar = enable_if_t<is_arithmetic<Scalar>::value, Type>;
    /// @brief Enable if float
    template <class Float, class Type = void>
    using EnableIfFloat = enable_if_t<is_floating_point<Float>::value, Type>;
    /// @brief Enable if integer
    template <class Integer, class Type = void>
    using EnableIfInteger = enable_if_t<is_integral<Integer>::value, Type>;
    /// @brief Enable if signed
    template <class Integer, class Type = void>
    using EnableIfSigned = enable_if_t<is_integral<Integer>::value && is_signed<Integer>::value, Type>;
    /// @brief Enable if unsigned
    template <class Integer, class Type = void>
    using EnableIfUnsigned = enable_if_t<is_integral<Integer>::value && is_unsigned<Integer>::value, Type>;
    /// @}
}
