/// @file ls/common/import.h
/// @brief Import STL and Boost into main namespace
#pragma once

#include "ls/common/import.h"
#include "ls/common/concept.h"
#include "ls/common/allocator.h"

namespace ls
{
    /// @addtogroup LsCommon
    /// @{

    /// @brief Std.Deque
    template <class Element, template<class> class Allocator = StdAllocator>
    using std_deque = std::deque<Element, Allocator<Element>>;
    /// @brief Std.List
    template <class Element, template<class> class Allocator = StdAllocator>
    using std_list = std::list<Element, Allocator<Element>>;
    /// @brief Std.Map
    template <class Key, class Value, template<class> class Allocator = StdAllocator>
    using std_map = std::map<Key, Value, less<Key>, Allocator<pair<const Key, Value>>>;
    /// @brief Std.MultiMap
    template <class Key, class Value, template<class> class Allocator = StdAllocator>
    using std_multimap = std::multimap<Key, Value, less<Key>, Allocator<pair<const Key, Value>>>;
    /// @brief Std.Set
    template <class Key, template<class> class Allocator = StdAllocator>
    using std_set = std::set<Key, less<Key>, Allocator<Key>>;
    /// @brief Std.MultiSet
    template <class Key, template<class> class Allocator = StdAllocator>
    using std_multiset = std::multiset<Key, less<Key>, Allocator<Key>>;
    /// @brief Std.HashMap
    template <class Key, class Value, template<class> class Allocator = StdAllocator>
    using std_hash_map = std::unordered_map<Key, Value, Hash, equal_to<Key>, Allocator<pair<const Key, Value>>>;
    /// @brief Std.HashMultiMap
    template <class Key, class Value, template<class> class Allocator = StdAllocator>
    using std_hash_multimap = std::unordered_multimap<Key, Value, Hash, equal_to<Key>, Allocator<pair<const Key, Value>>>;
    /// @brief Std.HashSet
    template <class Key, template<class> class Allocator = StdAllocator>
    using std_hash_set = std::unordered_set<Key, Hash, equal_to<Key>, Allocator<Key>>;
    /// @brief Std.HashMultiSet
    template <class Key, template<class> class Allocator = StdAllocator>
    using std_hash_multiset = std::unordered_multiset<Key, Hash, equal_to<Key>, Allocator<Key>>;

    /// @brief Fast.List
    template<class Type>
    using fast_list = std_list<Type, FastAllocator>;
    /// @brief Fast.Deque
    template<class Type>
    using fast_deque = std_deque<Type, FastAllocator>;
    /// @brief Fast.Map
    template<class Key, class Value>
    using fast_map = std_map<Key, Value, FastAllocator>;
    /// @brief Fast.MultiMap
    template<class Key, class Value>
    using fast_multimap = std_multimap<Key, Value, FastAllocator>;
    /// @brief Fast.Set
    template<class Key>
    using fast_set = std_set<Key, FastAllocator>;
    /// @brief Fast.MultiSet
    template<class Key>
    using fast_multiset = std_multiset<Key, FastAllocator>;
    /// @brief Fast.HashMap
    template<class Key, class Value>
    using fast_hash_map = std_hash_map<Key, Value, FastAllocator>;
    /// @brief Fast.HashMultiMap
    template<class Key, class Value>
    using fast_hash_multimap = std_hash_multimap<Key, Value, FastAllocator>;
    /// @brief Fast.HashSet
    template<class Key>
    using fast_hash_set = std_hash_set<Key, FastAllocator>;
    /// @brief Fast.HashMultiSet
    template<class Key>
    using fast_hash_multiset = std_hash_multiset<Key, FastAllocator>;

    /// @brief Map
    template<class Key, class Value>
    using map = fast_map<Key, Value>;
    /// @brief MultiMap
    template<class Key, class Value>
    using mumap = fast_multimap<Key, Value>;
    /// @brief Set
    template<class Key>
    using set = fast_set<Key>;
    /// @brief MultiSet
    template<class Key>
    using muset = fast_multiset<Key>;
    /// @brief HashMap
    template<class Key, class Value>
    using hamap = fast_hash_map<Key, Value>;
    /// @brief HashMultiMap
    template<class Key, class Value>
    using hamumap = fast_hash_multimap<Key, Value>;
    /// @brief HashSet
    template<class Key>
    using haset = fast_hash_set<Key>;
    /// @brief HashMultiSet
    template<class Key>
    using hamuset = fast_hash_multiset<Key>;
    /// @}
}
