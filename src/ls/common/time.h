/// @file ls/common/time.h
/// @brief Date-time getters
#pragma once

#include "ls/common/integer.h"

namespace ls
{
    /// @addtogroup LsCommon
    /// @{

    // #TODO Use std::chrono types
    /// @brief Integer time type
    using DiscreteTime = uint;
    /// @brief Time delta type
    using DeltaTime = uint;
    /// @brief Date-Time struct
    struct DateTime
    {
        /// @brief Year
        ushort year;
        /// @brief Month
        ushort month;
        /// @brief Day
        ushort day;
        /// @brief Hour
        ushort hour;
        /// @brief Minute
        ushort min;
        /// @brief Second
        ushort sec;
        /// @brief Millisec
        ushort msec;
    };
    /// @brief Get current time
    DateTime currentTime();
    /// @brief Get relative time
    uint relativeTime();
    /// @}
}