/// @file ls/time/BlockProfiler.h
/// @brief Duration measurer
#pragma once

#include "ls/common/import.h"
#include "ls/common/Logger.h"

namespace ls
{
    /// @addtogroup LsCommon
    /// @{

    /// @brief Get short name of duration
    template <class D>
    const char* durationSuffix();
    /// @brief Block Profiler
    template <class D>
    class BlockProfiler
        : Noncopyable
    {
    public:
        /// @brief Duration class
        using Duration = D;
    public:
        /// @brief Ctor
        /// @param message
        ///   Finalize message
        /// @param level
        ///   Log level value
        BlockProfiler(const string& message, const LogMessageLevel level, 
                      const bool isStarted = true)
            : m_level(level)
            , m_message(message)
        {
            if (isStarted)
                start();
        }
        /// @brief Start
        void start()
        {
            if (m_started)
                stop();

            m_started = true;
            m_start = high_resolution_clock::now();
        }
        /// @brief Stop
        void stop()
        {
            if (!m_started)
                return;

            m_started = false;
            m_end = high_resolution_clock::now();
            const ulong num = duration_cast<Duration>(m_end - m_start).count();
            log(m_level, m_message, num, durationSuffix<Duration>());
        }
        /// @brief Dtor
        ~BlockProfiler()
        {
            stop();
        }
    private:
        const LogMessageLevel m_level;
        const string m_message;

        bool m_started = false;

        high_resolution_clock::time_point m_start;
        high_resolution_clock::time_point m_end;
    };
    /// @}
}