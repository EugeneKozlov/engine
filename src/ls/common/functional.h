/// @file ls/common/functional.h
/// @brief Functors
#pragma once

namespace ls
{
	/// @addtogroup LsCommon
	/// @{

	/// @brief NOP functor
    class nop
    {
    public:
        /// @brief Nothing
        template<class... Args>
        void operator () (Args...)
        {
        }
    };
	/// @brief Return functor
    struct returner
    {
        /// @brief Operator
        template<class T>
        T& operator () (T& arg)
        {
            return arg;
        }
        /// @brief Const operator
        template<class T>
        const T& operator () (const T& arg)
        {
            return arg;
        }
    };
	/// @brief Get functor
    template<class T>
    struct getter
    {
        /// @brief Ctor
        getter(const T& value = T{}) : value(value)
        {
        }
        /// @brief Value to return;
        T value;
        /// @brief Operator
        template<class... Args>
        T operator () (Args...)
        {
            return value;
        }
    };
	/// @brief Reference functor
    struct reference
    {
        /// @brief Operator
        template<class T>
        T* operator () (T& arg)
        {
            return &arg;
        }
        /// @brief Const operator
        template<class T>
        const T* operator () (const T& arg)
        {
            return &arg;
        }
    };
	/// @brief Dereference functor
	/// @tparam T Dereferenced object type
    template<class T>
    struct dereference
    {
        /// @brief Result type
        typedef T result_type;
        /// @brief Operator
        template<class U>
        T operator () (U arg)
        {
            return *arg;
        }
    };
	/// @brief Delete functor
    struct deleter
    {
        /// @brief Result type
        typedef void result_type;
        /// @brief Operator
        template<class T>
        void operator () (T arg)
        {
            delete arg;
        }
    };
    /// @}
}
