/// @file ls/common/String.h
/// @brief Printable string and formatting functions
#pragma once

#include "ls/common/import.h"
#include "ls/common/integer.h"
#include "ls/common/concept.h"
#include "ls/common/container.h"

namespace ls
{
	/// @addtogroup LsCommon
	/// @{

    /// @brief Format tag prototype
    struct FormatTag
    {
    };
    /// @brief Check whether tag
    template<class Object>
    inline constexpr bool isTag()
    {
        return is_base_of<FormatTag, Object>::value;
    }
    /// @brief Decimal format tag
    struct Dec : FormatTag
    {
        /// @brief Ctor
        Dec(const ubyte length = 0, const bool zeros = true)
            : length(length)
            , zeros(zeros)
        {
        }
        /// @brief Min number length
        ubyte length;
        /// @brief Set to pretend by zeros
        bool zeros;
    };
    /// @brief Hexadecimal format tag
    struct Hex : FormatTag
    {
        /// @brief Ctor
        Hex(const ubyte length = 0, const bool zeros = true)
            : length(length)
            , zeros(zeros)
        {
        }
        /// @brief Min number length
        ubyte length;
        /// @brief Set to pretend by zeros
        bool zeros;
    };
    /// @a float format tag
    struct Precision : FormatTag
    {
        /// @brief Ctor
        Precision(const ubyte length = 0, const ubyte fract = 0, const bool exp = false)
            : length(length)
            , fract(fract)
            , exp(exp)
        {
        }
        /// @brief Min number length
        ubyte length;
        /// @brief Min fractional part length
        ubyte fract;
        /// @brief Use scientific format
        bool exp;
    };
    /// @brief Low-precision float
    struct Lowp : Precision
    {
        /// @brief Ctor
        Lowp() : Precision(3, 2)
        {
        }
    };
    /// @brief Medium-precision float
    struct Medp : Precision
    {
        /// @brief Ctor
        Medp() : Precision(6, 5)
        {
        }
    };
    /// @brief High-precision float
    struct Highp : Precision
    {
        /// @brief Ctor
        Highp() : Precision(8, 7)
        {
        }
    };
    /// @brief Default-precision float
    struct Fullp : Precision
    {
    };

    /// @brief End-line symbol(s)
    struct Endl
    {
    };
    /// @brief Tab symbol
    struct Tab
    {
    };
    /// @brief Formatted output string.
    /// Contains additional formatting info.
    ///
    /// @a format usage:
    /// + Each entry of '{{' is replaced by '{'
    /// + Each entry of '}}' is replaced by '}'
    /// + Each entry of '{*}' is replaced by appropriate object (ignoring tags)
    /// + Each extra placeholder '{*}' is replace by nothing
    /// + Each extra object is printed at the end of string consequentially
    /// + Tags are applied in specified order
    class String
    {
    public:
        /// @brief Empty ctor
        String()
        {
            reset();
        }
        /// @brief Target ctor
        String(string& target)
            : String()
        {
            m_ptr = &target;
        }
        /// @brief Reset printers
        void reset()
        {
            strcpy(m_intPrinter, "%d");
            strcpy(m_uintPrinter, "%u");
            strcpy(m_floatPrinter, "%f");
        }
        /// @brief Set integer formatting
        /// @param size
        ///   Minimal size of number
        /// @param insertZeros
        ///   Set to use zeros, spaces is used otherwise
        /// @param hex
        ///   Set to display number as @a hex, use @a dec otherwise
        void formatInt(const ubyte size, const bool insertZeros, const bool hex)
        {
            if (size == 0)
            {
                sprintf(m_intPrinter, "%%%c", hex ? 'X' : 'd');
                sprintf(m_uintPrinter, "%%%c", hex ? 'X' : 'u');
            }
            else if (insertZeros)
            {
                sprintf(m_intPrinter, "%%0%d%c", size, hex ? 'X' : 'd');
                sprintf(m_uintPrinter, "%%0%d%c", size, hex ? 'X' : 'u');
            }
            else
            {
                sprintf(m_intPrinter, "%%%d%c", size, hex ? 'X' : 'd');
                sprintf(m_uintPrinter, "%%%d%c", size, hex ? 'X' : 'u');
            }
        }
        /// @brief Set float formatting
        /// @param totalSize
        ///   Minimal size of number
        /// @param fractionalSize
        ///   Minimal size of fractional part
        /// @param exp
        ///   Set to use mantissa/exponent notation
        void formatFloat(const ubyte totalSize, const ubyte fractionalSize, const bool exp)
        {
            if (totalSize == 0)
            {
                sprintf(m_floatPrinter, "%%%c", exp ? 'e' : 'f');
            }
            else
            {
                sprintf(m_floatPrinter, "%%%d.%d%c", totalSize, fractionalSize,
                        exp ? 'e' : 'f');
            }
        }
        /// @brief Clear string
        void clear()
        {
            m_ptr->clear();
        }

        /// @brief Get string size
        uint length() const
        {
            return (uint) m_ptr->length();
        }
        /// @brief Get mutable string
        string& ref()
        {
            return *m_ptr;
        }
        /// @brief Get immutable string
        const string& cref() const
        {
            return *m_ptr;
        }
        /// @brief Get immutable C-string
        const char* cstr() const
        {
            return m_ptr->c_str();
        }

        /// @brief Get int printer
        const char* intPrinter() const
        {
            return m_intPrinter;
        }
        /// @brief Get uint printer
        const char* uintPrinter() const
        {
            return m_uintPrinter;
        }
        /// @brief Get float printer
        const char* floatPrinter() const
        {
            return m_floatPrinter;
        }

        /// @brief Print nothing
        inline void print()
        {
        }
        /// @brief Print Int8
        inline void print(const sbyte value)
        {
            print((sint) value);
        }
        /// @brief Print Uint8
        inline void print(const ubyte value)
        {
            print((uint) value);
        }
        /// @brief Print Int16
        inline void print(const sshort value)
        {
            print((sint) value);
        }
        /// @brief Print Uint16
        inline void print(const ushort value)
        {
            print((uint) value);
        }

        /// @brief Print Int32
        inline void print(const sint value)
        {
            char buf[32] = {0};
            sprintf(buf, intPrinter(), value);
            *m_ptr += buf;
        }
        /// @brief Print Uint32
        inline void print(const uint value)
        {
            char buf[32] = {0};
            sprintf(buf, uintPrinter(), value);
            *m_ptr += buf;
        }
        /// @brief Print Int64
        inline void print(const slong value)
        {
            char buf[32] = {0};
            std::stringstream stream;
            stream << value;
            stream >> buf;
            *m_ptr += buf;
        }
        /// @brief Print Uint64
        inline void print(const ulong value)
        {
            char buf[32] = {0};
            std::stringstream stream;
            stream << value;
            stream >> buf;
            *m_ptr += buf;
        }
        /// @brief Print float
        inline void print(const float value)
        {
            char buf[32] = {0};
            sprintf(buf, floatPrinter(), value);
            *m_ptr += buf;
        }
        /// @brief Print double
        inline void print(const double value)
        {
            char buf[32] = {0};
            sprintf(buf, floatPrinter(), value);
            *m_ptr += buf;
        }
        /// @brief Apply @a dec tag
        inline void print(const Dec tag)
        {
            formatInt(tag.length, tag.zeros, false);
        }
        /// @brief Apply @a hex tag
        inline void print(const Hex tag)
        {
            formatInt(tag.length, tag.zeros, true);
        }
        /// @brief Apply @a precision tag
        inline void print(const Precision tag)
        {
            formatFloat(tag.length, tag.fract, tag.exp);
        }
        /// @brief Print bool
        inline void print(const bool value)
        {
            if (value)
                *m_ptr += "true";
            else
                *m_ptr += "false";
        }
        /// @brief Print char
        inline void print(const char value)
        {
            *m_ptr += value;
        }
        /// @brief Print end-line
        inline void print(Endl)
        {
            *m_ptr += "\n";
        }
        /// @brief Print tab
        inline void print(Tab)
        {
            *m_ptr += "\n";
        }
        /// @brief Print C-String
        inline void print(const char* value)
        {
            *m_ptr += value;
        }
        /// @brief Print STL string
        inline void print(const string& value)
        {
            *m_ptr += value;
        }
        /// @brief Print any pointer
        template <class Object, class = enable_if_t<!is_same<Object, char>::value>>
        inline void print(const Object* value)
        {
            char buf[32] = {0};
            sprintf(buf, "%p", value);
            *m_ptr += buf;
        }
        /// @brief Print printable
        template <class Object, class = enable_if_t<isPrintable<Object>()>>
        inline void print(const Object& object)
        {
            object.toString(*this);
        }
        /// @brief Print enum
        template <class Enum, class = enable_if_t<is_enum<Enum>::value>>
        inline void print(Enum value)
        {
            *m_ptr += asString(value);
        }
        /// @brief Print multiple objects
        template <class First, class Second, class... Others>
        inline void print(const First& first, const Second& second,
                          const Others& ... others)
        {
            print(first);
            print(second, others...);
        }
        /// @}

        /// @brief Print format string
        bool format(const char* formatString)
        {
            const char* begin = formatString;
            const char* p = formatString;
            while (*p)
            {
                if (p[0] == '{')
                {
                    if (p[1] == '*' && p[2] == '}')
                    {
                        m_ptr->append(begin, p);
                        p += 3;
                        begin = p;
                    }
                    else if (p[1] == '{')
                    {
                        m_ptr->append(begin, p + 1);
                        p += 2;
                        begin = p;
                    }
                    else
                        return false;
                }
                else if (p[0] == '}')
                {
                    if (p[1] == '}')
                    {
                        m_ptr->append(begin, p + 1);
                        p += 2;
                        begin = p;
                    }
                    else
                        return false;
                }
                else
                    ++p;
            }
            // Print format
            if (begin != p)
                m_ptr->append(begin, p);
            return true;
        }
        /// @brief Print non-tag object at placeholder or tag immediatelly
        template <class Object, class... Objects>
        bool format(const char* formatString,
                    const Object& object, const
                    Objects& ... objects)
        {
            // Just apply
            if (isTag<Object>())
            {
                print(object);
                return format(formatString, objects...);
            }
            // Find matching placeholder and replace
            const char* begin = formatString;
            const char* p = formatString;
            while (*p)
            {
                if (p[0] == '{')
                {
                    if (p[1] == '*' && p[2] == '}')
                    {
                        m_ptr->append(begin, p);
                        p += 3;
                        print(object);
                        return format(p, objects...);
                    }
                    else if (p[1] == '{')
                    {
                        m_ptr->append(begin, p + 1);
                        p += 2;
                        begin = p;
                    }
                    else
                        return false;
                }
                else if (p[0] == '}')
                {
                    if (p[1] == '}')
                    {
                        m_ptr->append(begin, p + 1);
                        p += 2;
                        begin = p;
                    }
                    else
                        return false;
                }
                else
                    ++p;
            }
            // Print format
            if (begin != p)
                m_ptr->append(begin, p);
            // Print object
            print(object);
            // Print remaning
            return format(p, objects...);
        }
    private:
        /// @brief Local storage
        string m_localStorage;
        /// @brief Mutable string
        string* m_ptr = &m_localStorage;
        /// @brief int format string
        char m_intPrinter[8];
        /// @brief uint format string
        char m_uintPrinter[8];
        /// @brief float format string
        char m_floatPrinter[12];
    };
    /// @brief Print arguments to string.
    /// @param args
    ///   Arguments to print
    /// @return String
    template <class... Args>
    inline string print(const Args&... args)
    {
        String str;
        str.print(args...);
        return move(str.ref());
    }
    /// @brief Cast printable to string
    template <class Object, class = enable_if_t<isPrintable<Object>()>>
    inline string toString(const Object& object)
    {
        String buf;
        object.toString(buf);
        return move(buf.ref());
    }
    /// @brief Split string by tokens
    void split(const string& src, char delim, vector<string>& dest);
    /// @brief Split string by tokens
    vector<string> split(const string& src, char delim);
    /// @brief Remove trailing and ending spaces
    string chop(const string& str);
    /// @brief Filter non-empty strings from vector
    vector<string> filterNonEmpty(const vector<string>& src);
    /// @}
}