/// @file ls/common/FlatIndexRange.h
/// @brief Flat Index Iterator and Range
#pragma once

#include "ls/common/import.h"
#include "ls/math/Vector.h"

namespace ls
{
    /// @addtogroup LsCommonAlgorithm
    /// @{

    /// @brief Flat Index Iterator
    template <class Integer>
    struct FlatIndexIterator : public std::iterator<std::forward_iterator_tag, Vector<Integer, 2>>
    {
    public:
        /// @brief Value Type
        using Type = Vector<Integer, 2>;
    public:
        /// @brief Ctor
        FlatIndexIterator() = default;
        /// @brief Ctor
        FlatIndexIterator(const Type& value, const Type& begin, const Type& end)
            : m_value(value)
            , m_begin(begin)
            , m_end(end)
        {
            debug_assert(m_begin.x < m_end.x && m_begin.y < m_end.y);
        }
        /// @brief Increment
        FlatIndexIterator& operator ++ ()
        {
            debug_assert(m_value != m_end);

            // Next cell
            ++m_value.x;
            if (m_value.x == m_end.x)
            {
                // Next row
                m_value.x = m_begin.x;
                ++m_value.y;

                // Finalize
                if (m_value.y == m_end.y)
                    m_value = m_end;
            }
            return *this;
        }
        /// @brief Post-increment
        FlatIndexIterator operator ++ (int)
        {
            FlatIndexIterator tmp(*this);
            ++*this;
            return tmp;
        }
        Type operator * () const
        {
            return m_value;
        }
        /// @brief Access
        Type* operator -> () const
        {
            return &m_value;
        }

        /// @brief Compare equal
        bool operator == (const FlatIndexIterator& another) const
        {
            debug_assert(m_begin == another.m_begin && m_end == another.m_end);
            return m_value == another.m_value;
        }
        /// @brief Compare non-equal
        bool operator != (const FlatIndexIterator& another) const
        {
            return !(*this == another);
        }
    private:
        Type m_value;
        Type m_begin;
        Type m_end;
    };
    /// @brief Flat Index Range
    template <class Integer>
    using FlatIndexRange = pair<FlatIndexIterator<Integer>, FlatIndexIterator<Integer>>;
    /// @brief Make flat range
    template <class Integer, class = EnableIfInteger<Integer>>
    FlatIndexRange<Integer> makeRange(const Vector<Integer, 2>& begin, const Vector<Integer, 2>& end)
    {
        return FlatIndexRange<Integer>(FlatIndexIterator<Integer>(begin, begin, end),
                                       FlatIndexIterator<Integer>(end, begin, end));
    }
    /// @}
}
