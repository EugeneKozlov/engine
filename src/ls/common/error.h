/// @file ls/common/error.h
/// @brief Error codes
#pragma once

#include "ls/common/Exception.h"

namespace ls
{
	/// @addtogroup LsCommonExceptions
	/// @{

	/// @brief Error and exception codes.
    enum class Error : Exception::Code
    {
        /// @brief No error
        OK = 0,
        /// @brief Unknown error
        Unknown,
        /// @brief Assertion failed
        Assert,

        /// @brief Requested functional is not implemented yet
        NotImplemented,
        /// @brief Logic error
        Logic,
        /// @brief Overflow
        Overflow,
        /// @brief Invalid function argument
        Argument,
        /// @brief Not enough memory
        OutOfMemory,
        /// @brief Bad format string
        Format,

        /// @brief Input/output error
        Io,
        /// @brief Script error
        Script,
        /// @brief File system error
        FileSystem,
        /// @brief Application system fail
        ApplicationFail,
        /// @brief GAPI effect error
        GapiEffect,

        /// @brief Network fail
        NetworkFail,
        /// @brief Server listening fail
        ListeningFail,
        /// @brief Connection closed/lost
        ConnectionLost,

    };

    /// @brief Assert (internal use only)
    class AssertException : public SpecializedException<Error, Error::Assert>
    {
    };
    /// @brief Ditto
    class NotImplementedException : public SpecializedException<Error, Error::NotImplemented>
    {
    };
    /// @brief Ditto
    class LogicException : public SpecializedException<Error, Error::Logic>
    {
    };
    /// @brief Ditto
    class OverflowException : public SpecializedException<Error, Error::Overflow>
    {
    };
    /// @brief Ditto
    class ArgumentException : public SpecializedException<Error, Error::Argument>
    {
    };
    /// @brief Ditto
    class OutOfMemoryException : public SpecializedException<Error, Error::OutOfMemory>
    {
    };
    /// @brief Ditto
    class FormatException : public SpecializedException<Error, Error::Format>
    {
    };
    /// @brief Ditto
    class IoException : public SpecializedException<Error, Error::Io>
    {
    };
    /// @brief Ditto
    class ScriptException : public SpecializedException<Error, Error::Script>
    {
    };
    /// @brief Ditto
    class FileSystemException : public SpecializedException<Error, Error::FileSystem>
    {
    };
    /// @brief Ditto
    class ApplicationFailException : public SpecializedException<Error, Error::ApplicationFail>
    {
    };
    /// @brief Ditto
    class GapiEffectException : public SpecializedException<Error, Error::GapiEffect>
    {
    };

    /// @brief Ditto
    class NetworkFailException : public SpecializedException<Error, Error::NetworkFail>
    {
    };
    /// @brief Ditto
    class ListeningFailException : public SpecializedException<Error, Error::ListeningFail>
    {
    };
    /// @brief Ditto
    class ConnectionLostException : public SpecializedException<Error, Error::ConnectionLost>
    {
    };
    /// @}
}