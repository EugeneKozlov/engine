/// @file ls/common/ArrayWrapper.h
/// @brief Pure-C Array Wrapper
#pragma once

#include "ls/common/assert.h"
#include "ls/common/import.h"
#include "ls/common/integer.h"

namespace ls
{
    /// @addtogroup LsCommon
    /// @{

    /// @brief Array Wrapper
    template <class Object>
    class ArrayWrapper
    {
    public:
        /// @brief Ctor
        ArrayWrapper() = default;
        /// @brief Ctor
        ArrayWrapper(nullptr_t)
        {
        }
        /// @brief Ctor
        ArrayWrapper(Object data[], const uint size)
            : m_data(data)
            , m_size(size)
        {
            debug_assert(data);
        }
        /// @brief Ctor
        ArrayWrapper(vector<Object>& vec)
            : m_data(vec.data())
            , m_size(vec.size())
        {
        }
        /// @brief Is empty?
        bool isEmpty() const
        {
            return m_size == 0;
        }
        /// @brief Get size
        uint size() const
        {
            return m_size;
        }
        /// @brief Access
        Object& operator[] (const uint idx)
        {
            debug_assert(idx < m_size);
            return m_data[idx];
        }
        /// @brief Access
        const Object& operator[] (const uint idx) const
        {
            debug_assert(idx < m_size);
            return m_data[idx];
        }
    private:
        Object* m_data = nullptr;
        uint m_size = 0;
    };
    /// @brief Constant Array Wrapper
    template <class Object>
    class ConstArrayWrapper
    {
    public:
        /// @brief Ctor
        ConstArrayWrapper() = default;
        /// @brief Ctor
        ConstArrayWrapper(nullptr_t)
        {
        }
        /// @brief Ctor
        ConstArrayWrapper(const Object data[], const uint size)
            : m_data(data)
            , m_size(size)
        {
            debug_assert(data);
        }
        /// @brief Ctor
        ConstArrayWrapper(const vector<Object>& vec)
            : m_data(vec.data())
            , m_size(vec.size())
        {
        }
        /// @brief Is empty?
        bool isEmpty() const
        {
            return m_size == 0;
        }
        /// @brief Get size
        uint size() const
        {
            return m_size;
        }
        /// @brief Access
        const Object& operator[] (const uint idx) const
        {
            debug_assert(idx < m_size);
            return m_data[idx];
        }
    private:
        const Object* m_data = nullptr;
        uint m_size = 0;
    };
    /// @}
}
