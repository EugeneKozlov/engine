/// @file ls/common/Logger.h
/// @brief Logger class
#pragma once

#include "ls/common/config.h"
#include "ls/common/import.h"
#include "ls/common/container.h"
#include "ls/common/String.h"
#include "ls/common/Observable.h"

namespace ls
{
	/// @addtogroup LsCommon
	/// @{

	/// @brief Log Message verbosity Level
    enum class LogMessageLevel : uint
    {
        /// @brief Trace messages
        Trace,
        /// @brief All exceptions (reserved)
        Throw,
        /// @brief Debug information
        Debug,
        /// @brief General information
        Info,
        /// @brief High-level system information
        System,
        /// @brief Important information
        Notice,
        /// @brief Warning or possible error
        Warning,
        /// @brief Non-critical error
        Error,
        /// @brief Critical error (reserved)
        Crit,
        /// @brief Message without time and rank
        Message
    };
	/// @brief Logger interface
    class LoggerInterface
    {
    public:
        /// @brief Logger verbosity
        LogMessageLevel verbosity;
    public:
        /// @brief Ctor
        LoggerInterface();
        /// @brief Dtor
        virtual ~LoggerInterface();
		/// @brief Write message
		/// @param level
		///   Message level
		/// @param fullMessage
		///   Full message text with verbosity and time stamp
		/// @param textOnly
		///   Only message text
        virtual void write(const LogMessageLevel level,
                           const char* fullMessage,
                           const char* textOnly) = 0;
    };
	/// @brief File Logger
    class FileLogger
		: public LoggerInterface
    {
    public:
		/// @brief Ctor
		/// @param fileName
		///   File name
        FileLogger(const string& fileName);
        /// @brief Dtor
        virtual ~FileLogger();
    public:
        virtual void write(const LogMessageLevel level,
                           const char* fullMessage,
                           const char* textOnly) override;
    protected:
        /// @brief Log file name
        string m_fileName;
        /// @brief Log file
        FILE* m_file;
    };
	/// @brief Console Logger
    class ConsoleLogger
		: public LoggerInterface
    {
    public:
        /// @brief Ctor
        ConsoleLogger(const LogMessageLevel level)
        {
            verbosity = level;
        }
    public:
        virtual void write(const LogMessageLevel level,
                           const char* fullMessage,
                           const char* textOnly) override;
    };
	/// @brief Debug-Output logger
    class DebugOutputLogger
		: public LoggerInterface
    {
    public:
        /// @brief Ctor
        DebugOutputLogger(const LogMessageLevel level)
        {
            verbosity = level;
        }
    public:
        virtual void write(const LogMessageLevel level,
                           const char* fullMessage,
                           const char* textOnly) override;
    };
	/// @brief Log Manager
    class LogManager
    {
    public:
		/// @brief Ctor
        LogManager();
        /// @brief Dtor
        ~LogManager();
        /// @brief Get singleton instance
        static LogManager& instance()
        {
            static LogManager self;
            return self;
        }
        /// @brief Add @a observer
        void addObserver(LoggerInterface& logger)
        {
            lock_guard<mutex> guard(m_mutex);
            m_loggers.emplace_back(&logger);
        }
        /// @brief Remove @a observer
        void removeObserver(LoggerInterface& observer)
        {
            lock_guard<mutex> guard(m_mutex);
            auto iterLogger = std::find(m_loggers.begin(),
                                        m_loggers.end(),
                                        &observer);
            m_loggers.erase(iterLogger);
        }

        /// @brief Write string to log
        template <class... Args>
        inline void log(const LogMessageLevel type, const Args& ... args)
        {
            writeAndCopy(type, nullptr, args...);
        }
        /// @brief Write string to log and return this one
        template <class... Args>
        inline string write(const LogMessageLevel type, const Args& ... args)
        {
            string ret;
            writeAndCopy(type, &ret, args...);
            return move(ret);
        }

        /// @brief Log as @a LogMessageLevel::Throw and return exception string
        template <class... Args>
        inline string writeThrow(const Args& ... args)
        {
            return write(LogMessageLevel::Throw, args...);
        }
        /// @brief Log as @a LogMessageLevel::Crit and return assertion string
        template <class... Args>
        inline string writeCrit(const Args& ... args)
        {
            return write(LogMessageLevel::Crit, args...);
        }
    private:
        /// @brief Write time and tag
        uint writeTags(const LogMessageLevel type);
        /// @brief Flush buffer
        void flush(const LogMessageLevel type, const uint start, string* dest);
        /// @brief Write something to log and to string
        template <class... Args>
        inline void writeAndCopy(const LogMessageLevel type,
                                 string* dest,
                                 const Args& ... args)
        {
            lock_guard<mutex> guard{m_mutex};
            const uint textBegin = writeTags(type);
            m_buffer.reset();
            m_buffer.print(args...);
            flush(type, textBegin, dest);
        }
    private:
        /// @brief Loggers
        vector<LoggerInterface*> m_loggers;
        /// @brief Write buffer
        String m_buffer;
        /// @brief Write mutex
        mutex m_mutex;
    };
	/// @brief Cast @a LogMessageLevel to string.
	/// @param value
	///   Enum value
	/// @return String
    const inline char* asString(const LogMessageLevel value) noexcept
    {
        switch (value)
        {
        case LogMessageLevel::Trace:
            return "[TRACE]   ";
        case LogMessageLevel::Debug:
            return "[DEBUG]   ";
        case LogMessageLevel::Throw:
            return "[THROW]   ";
        case LogMessageLevel::Info:
            return "[INFO]    ";
        case LogMessageLevel::System:
            return "[SYSTEM]  ";
        case LogMessageLevel::Notice:
            return "[NOTICE]  ";
        case LogMessageLevel::Warning:
            return "[WARNING] ";
        case LogMessageLevel::Error:
            return "[ERROR]   ";
        case LogMessageLevel::Crit:
            return "[CRIT]    ";
        case LogMessageLevel::Message:
            return "[MESSAGE] ";
        default: return nullptr;
        }
    }
    /// @brief Log as @a level
    template <class... Args>
    inline void log(const LogMessageLevel level, const Args& ... args)
    {
        LogManager::instance().log(level, args...);
    }
    /// @brief Log as @a LogMessageLevel::Trace
    template <class... Args>
    inline void logTrace(const Args& ... args)
    {
        LogManager::instance().log(LogMessageLevel::Trace, args...);
    }
    /// @brief Log as @a LogMessageLevel::Debug
    template <class... Args>
    inline void logDebug(const Args& ... args)
    {
        LogManager::instance().log(LogMessageLevel::Debug, args...);
    }
    /// @brief Log as @a LogMessageLevel::Info
    template <class... Args>
    inline void logInfo(const Args& ... args)
    {
        LogManager::instance().log(LogMessageLevel::Info, args...);
    }
    /// @brief Log as @a LogMessageLevel::System
    template <class... Args>
    inline void logSystem(const Args& ... args)
    {
        LogManager::instance().log(LogMessageLevel::System, args...);
    }
    /// @brief Log as @a LogMessageLevel::Notice
    template <class... Args>
    inline void logNotice(const Args& ... args)
    {
        LogManager::instance().log(LogMessageLevel::Notice, args...);
    }
    /// @brief Log as @a LogMessageLevel::Warning
    template <class... Args>
    inline void logWarning(const Args& ... args)
    {
        LogManager::instance().log(LogMessageLevel::Warning, args...);
    }
    /// @brief Log as @a LogMessageLevel::Error
    template <class... Args>
    inline void logError(const Args& ... args)
    {
        LogManager::instance().log(LogMessageLevel::Error, args...);
    }
    /// @brief Log as @a LogMessageLevel::Message
    template <class... Args>
    inline void logMessage(const Args& ... args)
    {
        LogManager::instance().log(LogMessageLevel::Message, args...);
    }
    /// @}
}
