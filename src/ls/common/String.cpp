#include "pch.h"
#include "ls/common/String.h"

using namespace ls;
void ls::split(const string& src, char delim, vector<string>& dest)
{
    stringstream ss(src);
    string item;
    while (std::getline(ss, item, delim))
    {
        dest.push_back(item);
    }
}
vector<string> ls::split(const string& src, char delim)
{
    vector<string> dest;
    split(src, delim, dest);
    return dest;
}

string ls::chop(const string& str)
{
    const size_t from = str.find_first_not_of(' ');
    if (from == string::npos)
        return "";
    const size_t to = str.find_last_not_of(' ');
    if (to == string::npos)
        return str.substr(from);
    return str.substr(from, to - from + 1);
}
vector<string> ls::filterNonEmpty(const vector<string>& src)
{
    vector<string> dest;
    for (const string& str : src)
    {
        dest.push_back(chop(str));
        if (dest.back().empty())
            dest.pop_back();
    }
    return dest;
}