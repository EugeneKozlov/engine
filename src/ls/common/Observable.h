/// @file ls/common/Observable.h
/// @brief Observer and observable interfaces.
#pragma once

#include "ls/common/concept.h"
#include "ls/common/container.h"

namespace ls
{
	/// @addtogroup LsCommon
	/// @{

	/// @brief Observable object interface.
	/// @tparam T Observer interface
    template <class T>
    class ObservableInterface
		: Noncopyable
    {
    public:
        /// @brief Real observer type
        using Observer = T;
        /// @brief Ctor
        ObservableInterface()
        {
        }
        /// @brief Dtor
        virtual ~ObservableInterface()
        {
        }
        /// @brief Add @a observer
        void addObserver(Observer& observer);
        /// @brief Remove @a observer
        void removeObserver(Observer& observer);
        /// @brief Discard all observers
        void discardObservers();
        /// @brief Check whether @a observer is added
        bool hasObserver(Observer& observer) const
        {
            return m_observers.count(&observer) == 1;
        }
    protected:
        /// @brief Add @a observer impl
        virtual void doAddObserver(Observer& /*observer*/)
        {
        }
        /// @brief Remove @a observer impl
        virtual void doRemoveObserver(Observer& /*observer*/)
        {
        }
    protected:
        /// @brief Set of observers
        set<Observer*> m_observers;
    };
	/// @brief Observer holder.
	/// @brief Hold observer attached to observable.
	/// @tparam T Observer interface
    template<class T>
    class ObserverHolder
		: Noncopyable
    {
    public:
        /// @brief Real observer type
        using Observer = T;
        /// @brief Ctor
        ObserverHolder(ObservableInterface<Observer>& observable,
                       Observer& observer)
            : m_observable(observable)
            , m_observer(observer)
        {
            m_observable.addObserver(m_observer);
        }
        /// @brief Dtor
        ~ObserverHolder()
        {
            m_observable.removeObserver(m_observer);
        }
    private:
        /// @brief Observable
        ObservableInterface<Observer>& m_observable;
        /// @brief Observer
        Observer& m_observer;
    };
    /// @}
}
