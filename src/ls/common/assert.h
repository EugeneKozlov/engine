/// @file ls/common/assert.h
/// @brief Global configuration
#pragma once

#include "ls/common/String.h"
#include "ls/common/error.h"

namespace ls
{
	/// @brief Invoke assertion fail
	/// @throw AssertException
	/// @param exception
	///   Assert exception
	/// @ingroup LsCommon
    void invokeAssertion(AssertException&& exception);
}

/// @addtogroup LsCommon
/// @{

/// @def LS_DEBUG_ASSERT_IMPL
/// Check @a CONDITION and invoke assertion-fail if @a false.
/// This check may be omitted.
#define LS_DEBUG_ASSERT_IMPL(CONDITION, ...)    \
    (!!(CONDITION) ? true : (                   \
        ls::invokeAssertion(ls::Exception::construct<AssertException>( \
            std::move(ls::LogManager::instance().writeThrow(__VA_ARGS__)), \
            __FILE__,                           \
            __LINE__,                           \
            __FUNCTION__,                       \
            #CONDITION                          \
        )), false                               \
    ))

/// @def debug_assert
/// @see LS_DEBUG_ASSERT_IMPL
#define debug_assert(CONDITION, ...) LS_DEBUG_ASSERT_IMPL(CONDITION, __VA_ARGS__)
///@}

#if LS_DISABLE_ASSERT != 0
#    undef LS_DEBUG_ASSERT_IMPL
#    define LS_DEBUG_ASSERT_IMPL(CONDITION, ...)
#endif

#if LS_OVERRIDE_ASSERT != 0
#    undef assert
#    define assert(CONDITION, ...) LS_DEBUG_ASSERT(CONDITION, __VA_ARGS__)
#endif
