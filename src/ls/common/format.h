/// @file ls/common/format.h
/// @brief String formatter
#pragma once

#include "ls/common/String.h"
#include "ls/common/error.h"

namespace ls
{
	/// @addtogroup LsCommon
	/// @{

	/// @brief Make string by format
	/// @throw FormatException
	/// @param str
	///   Format string
	/// @param args
	///   Arguments to replace place-holders
	/// @return Formatted string
    template <class... Args>
    string format(const char* str, const Args&... args)
    {
        String buf;
        bool success = buf.format(str, args...);
        LS_THROW(success,
                 FormatException,
                 "Bad format string : ", str);
        return move(buf.ref());
    }
	/// @brief Cast C-string to object
	/// @throw FormatException
    template <class Object>
    Object fromString(const char* str)
    {
        stringstream stm;
        Object obj;
        stm << str;
        bool success = static_cast<bool>(stm >> obj);
        LS_THROW(success,
                 FormatException,
                 "Cannot parse string : ", str);
        return obj;
    }
	/// @brief Cast any string to object
	/// @throw FormatException
    template <class Object, class String>
    Object fromString(const String& str)
    {
        return fromString<Object>(str.c_str());
    }
	/// @brief Cast C-string to object
	/// @throw FormatException
    template <class Object, class Modifier>
    Object fromString(const char* str, Modifier modifier)
    {
        stringstream stm;
        Object obj;
        stm << str;
        bool success = static_cast<bool>(stm >> modifier >> obj);
        LS_THROW(success,
                 FormatException,
                 "Cannot parse string : ", str);
        return obj;
    }
	/// @brief Cast any string to object
	/// @throw FormatException
    template <class Object, class String, class Modifier>
    Object fromString(const String& str, Modifier modifier)
    {
        return fromString<Object>(str.c_str(), modifier);
    }
	/// @brief Cast object to string
    /// @throw FormatException
    template <class Object>
    string toString(const Object& object)
    {
        return format("{*}", object);
    }
    /// @brief Helper to use in 'operator >>' for enums
    template <class Stream, class Enum>
    Stream& defaultEnumReader(Stream& stm, Enum& value,
                              const map<string, Enum>& m, const Enum def)
    {
        string str;
        stm >> str;
        value = getOrDefault(m, str, def);
        return stm;
    }
    /// @brief Helper to use in 'operator >>' for enums
    /// @throw FormatException instead of return default
    template <class Stream, class Enum>
    Stream& defaultEnumReader(Stream& stm, Enum& value,
                              const map<string, Enum>& m)
    {
        string str;
        stm >> str;
        value = getOrThrow<FormatException>(m, str, "Unknown enum value [", str, "]");
        return stm;
    }
    /// @}
}

/// @def LS_E2S_CASE
/// Generate @b case for enum-to-string switch
#define LS_E2S_CASE(E, X) case E::X: return #X
