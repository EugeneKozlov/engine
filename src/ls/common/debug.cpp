#include "pch.h"
#include "ls/common/debug.h"
#include "ls/common/import.h"

using namespace ls;

#ifdef _MSC_VER
#    pragma warning(push, 0)
#    include <stackwalker/StackWalker.h>
#    pragma warning(pop)
string ls::stackTrace()
{
    class StackWalkerImpl : public StackWalker
    {
    public:
        StackWalkerImpl() : StackWalker(RetrieveLine | RetrieveSymbol)
        {
        }
        string buffer;
    protected:
        virtual void OnCallstackEntry(CallstackEntryType eType, CallstackEntry &entry)
        {
            CHAR buf[STACKWALK_MAX_NAMELEN];
            if ((eType != lastEntry) && (eType != firstEntry) && (entry.offset != 0))
            {
                if (entry.name[0] == 0)
                    strcpy(entry.name, "(unknown function)");
                if (entry.undName[0] != 0)
                    strcpy(entry.name, entry.undName);
                if (entry.undFullName[0] != 0)
                    strcpy(entry.name, entry.undFullName);
                if (entry.lineFileName[0] == 0)
                {
                    if (entry.moduleName[0] == 0)
                        strcpy(entry.moduleName, "(unknown module)");
                    snprintf(buf, STACKWALK_MAX_NAMELEN, "(%s): %s\n",
                             entry.moduleName, entry.name);
                }
                else
                {
                    snprintf(buf, STACKWALK_MAX_NAMELEN, "%s (%d): %s\n",
                             entry.lineFileName, entry.lineNumber, entry.name);
                }
                buffer += buf;
            }
        }
    };
    StackWalkerImpl sw;
    sw.ShowCallstack();
    return sw.buffer;
}

#else
#    ifdef _WIN32
#        include <windows.h>
#    else
#        error Unsupported system
#    endif
static char* addr2line(const char* programName, void* address)
{
    if (!address)
        return nullptr;

    char line[256 * 3] = {0};

    sprintf(line,
            "addr2line -a -f -p -e %.256s %p",
            programName, address);

#    ifdef _WIN32
    // Init pipe
    SECURITY_ATTRIBUTES saAttr;
    saAttr.nLength = sizeof (SECURITY_ATTRIBUTES);
    saAttr.bInheritHandle = TRUE;
    saAttr.lpSecurityDescriptor = NULL;
    HANDLE pipeRead, pipeWrite;
    BOOL pipeCreated = CreatePipe(&pipeRead, &pipeWrite, &saAttr, 1024);
    if (!pipeCreated)
        return nullptr;

    // Init process
    STARTUPINFO info;
    memset(&info, 0, sizeof (STARTUPINFO));
    info.cb = sizeof (STARTUPINFO);
    info.hStdError = pipeWrite;
    info.hStdOutput = pipeWrite;
    info.dwFlags |= STARTF_USESTDHANDLES;
    PROCESS_INFORMATION process;
    BOOL threadCreated = CreateProcess(NULL, line,
                                       NULL, NULL, TRUE, 0, NULL, NULL,
                                       &info, &process);
    if (!threadCreated)
        return nullptr;
    WaitForSingleObject(process.hProcess, INFINITE);
    CloseHandle(process.hProcess);
    CloseHandle(process.hThread);

    // Read pipe
    char* buffer = new char[513];
    DWORD bufferSize;
    if (!ReadFile(pipeRead, buffer, 512, &bufferSize, NULL))
        return nullptr;
    buffer[bufferSize] = 0;

    // Close pipe
    CloseHandle(pipeRead);
    CloseHandle(pipeWrite);
#    else
#        error Unsupported system
#    endif

    return buffer;
}
string ls::stackTrace()
{
    // Get program name
    char programName[MAX_PATH];
    GetModuleFileName(GetModuleHandle(NULL), programName, MAX_PATH);

    // Trace
    string buffer;
    bool stop = false;
#    define T(N) if(!stop) \
{ \
    void* address = __builtin_extract_return_addr(__builtin_return_address(N)); \
    char* text = addr2line(programName, address); \
    stop = !text; \
    if (text) \
    { \
        buffer += text; \
        delete[] text; \
    } \
}

    T(0);
    T(1);
    T(2);
    T(3);
    T(4);
    T(5);
    T(6);
    T(7);
    T(8);
    T(9);
    T(10);
    T(11);
    T(12);
    T(13);
    T(14);
    T(15);

    return buffer;
}

#endif
