/// @file ls/common/Exception.h
/// @brief Exception class
#pragma once

#include "ls/common/import.h"
#include "ls/common/integer.h"
#include "ls/common/Logger.h"
#include "ls/common/String.h"
#include "ls/common/concept.h"

namespace ls
{
	/// @addtogroup LsCommon
	/// @{

	/// @brief Exception to throw.
	/// @brief Stores error code and description, file, line and function threw exception.
    class Exception
		: public std::exception 
        , public PrintableConcept
    {
    public:
        /// @brief Enable if class is exception
        template <class Class>
        using EnableIfException = enable_if_t<is_base_of<Exception, Class>::value>;
        /// @brief Exception code
        using Code = sint;
		/// @brief Construct exception
		/// @param what
		///   Exception description
		/// @param file,line,function
		///   Error source
		/// @param expression
		///   Expression caused error
		/// @return Constructed exception
        template <class Class, class = EnableIfException<Class>>
        static Class construct(string&& what,
                               const char* file = nullptr, const int line = -1,
                               const char* function = nullptr,
                               const char* expression = nullptr) noexcept
        {
            Class ex;
            ex.m_text = move(what);
            ex.m_file = file ? file : "(unspecified)";
            ex.m_line = line;
            ex.m_function = function ? function : "(unspecified)";
            ex.m_expression = expression ? expression : "?";
            return move(ex);
        }
		/// @brief Ctor
        explicit Exception(Code code) noexcept
			: m_text()
			, m_code(code)
			, m_file()
			, m_line(-1)
			, m_function()
			, m_expression()
        {
        }
        /// @brief Copy ctor
        Exception(const Exception& another) = default;
        /// @brief Move ctor
        Exception(Exception&& another) noexcept
			: m_text(move(another.m_text))
			, m_code(move(another.m_code))
			, m_file(move(another.m_file))
			, m_line(move(another.m_line))
			, m_function(move(another.m_function))
			, m_expression(move(another.m_expression))
        {
        }
        /// @brief Print exception info
        void toString(String& str) const noexcept
        {
            str.print("FILE:\t\t", m_file, Endl(),
                      "LINE:\t\t", m_line, Endl(),
                      "FUNCTION:\t", m_function, Endl(),
                      "EXPRESSION:\t", m_expression, Endl(),
                      "COMMENT:\t", m_text);
        }

        /// @brief Error description
        const char* what() const noexcept
        {
            return m_text.c_str();
        }
        /// @brief Error code
        Code code() const noexcept
        {
            return m_code;
        }
        /// @brief File threw exception
        const char* file() const noexcept
        {
            return m_file.c_str();
        }
        /// @brief Line threw exception
        int line() const noexcept
        {
            return m_line;
        }
        /// @brief Function threw exception
        const char* function() const noexcept
        {
            return m_function.c_str();
        }
        /// @brief Expression threw exception
        const char* expression() const noexcept
        {
            return m_expression.c_str();
        }
    private:
        /// @brief Error description
        string m_text;
        /// @brief Error code
        Code m_code;
        /// @brief File threw exception
        string m_file;
        /// @brief Line threw exception
        int m_line;
        /// @brief Function threw exception
        string m_function;
        /// @brief Expression threw exception
        string m_expression;
    };
	/// @brief Exception with error code.
	/// @tparam ExceptionCode
	///   Exception code
    template <class Enum, Enum ExceptionCode>
    class SpecializedException
		: public Exception
    {
    public:
        /// @brief Ctor
        SpecializedException()
            : Exception((Exception::Code) ExceptionCode)
        {
        }
    };
	/// @brief Invoke exception.
    /// @throw ExceptionType
    /// @tparam
    ///   ExceptionType Expression type
    /// @param what
    ///   Description
    /// @param file,line,function
    ///   Place
    /// @param expression
    ///   Expression
    template<class ExceptionType>
    inline bool invokeException(string&& what,
                                const char* file, int line,
                                const char* function, const char* expression)
    {
        throw Exception::construct<ExceptionType>(move(what),
            file, line, function, expression);
    }
    /// @}
}

/// @def LS_THROW
/// @brief Check @a CONDITION and throw @a EXCEPTION if @a false
/// @ingroup LsCommon
#define LS_THROW(CONDITION, EXCEPTION, ...)     \
    (!!(CONDITION) ? true : (                   \
        ls::invokeException<EXCEPTION>(         \
            std::move(ls::LogManager::instance().writeThrow(__VA_ARGS__)), \
            __FILE__,                           \
            __LINE__,                           \
            __FUNCTION__,                       \
            #CONDITION                          \
        )                                       \
    ))


