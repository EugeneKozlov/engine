/// @file ls/common/algorithm.h
/// @brief Abstract algorithms
#pragma once

#include "ls/common/assert.h"
#include "ls/math/common.h"

namespace ls
{
    /// @addtogroup LsCommonAlgorithm
    /// @{

    /// @brief Reconstruct object
    template <class Object, class ... Args>
    void reconstruct(Object& object, Args && ... args)
    {
        object.~Object();
        new (&object) Object(args...);
    }
    /// @brief Cast initializer list to array
    template <class Elem>
    const Elem* asArray(initializer_list<Elem> arr)
    {
        return arr.begin();
    };
    /// @brief Check whether STL tree container has specified element
    template <class Container, class Key>
    auto contain(const Container& container, const Key& key)
        -> decltype(container.find(key), bool())
    {
        return container.find(key) != container.end();
    }
    /// @brief Check whether any container has specified element
    template <class Iterator, class Element>
    bool contain(Iterator begin, Iterator end, const Element& element)
    {
        return std::find(begin, end, element) != end;
    }
    /// @brief Equality test
    template <class T, class U>
    bool allequal(const T& t, const U& u)
    {
        return t == u;
    }
    /// @brief Equality test
    template <class T, class U, class ... Args>
    bool allequal(const T& t, const U& u, const Args&... args)
    {
        return (t == u) && allequal(u, args...);
    }
    /// @brief Find not null
    template <class T>
    T iif(const T& t, const T& u)
    {
        return !!t ? t : u;
    }
    /// @brief Find not null and dereference
    template <class T>
    T& deref(T* first, T* second)
    {
        debug_assert(first || second);
        return first ? *first : *second;
    }
    /// @brief Find not null and dereference
    template <class T>
    const T& deref(const T* first, const T* second)
    {
        debug_assert(first || second);
        return first ? *first : *second;
    }
    /// @brief Remove-if for map/set containers
    template <class Container, class Predicate>
    void erase_if(Container& container, const Predicate& predicate)
    {
        for (auto it = container.begin(); it != container.end();)
        {
            if (predicate(*it))
                it = container.erase(it);
            else
                ++it;
        }
    };
    /// @brief Get element by @a key from mutable @a container or return @a def
    template <class Container, class Key>
    typename Container::mapped_type*
    findOrDefault(Container& container, const Key& key,
                  typename Container::mapped_type* def = nullptr)
    {
        auto it = container.find(key);
        if (it != container.end())
            return &it->second;
        else
            return def;
    }
    /// @brief Get element by @a key from mutable @a container or fail
    template <class Container, class Key>
    typename Container::mapped_type&
    getOrFail(Container& container, const Key& key)
    {
        auto it = findOrDefault(container, key);
        debug_assert(!!it, key);
        return *it;
    }
    /// @brief Get element by @a key from const @a container or return @a def
    template <class Container, class Key>
    const typename Container::mapped_type*
    findOrDefault(const Container& container, const Key& key,
                  const typename Container::mapped_type* def = nullptr)
    {
        auto it = container.find(key);
        if (it != container.end())
            return &it->second;
        else
            return def;
    }
    /// @brief Get element by @a key from immutable @a container or return @a def
    template <class Container, class Key>
    typename Container::mapped_type
    getOrDefault(const Container& container, const Key& key,
                 const typename Container::mapped_type& def = typename Container::mapped_type())
    {
        auto it = findOrDefault(container, key);
        if (it)
            return *it;
        else
            return def;
    }
    /// @brief Get element by @a key from const @a container or fail
    template <class Container, class Key>
    const typename Container::mapped_type&
    getOrFail(const Container& container, const Key& key)
    {
        auto it = findOrDefault(container, key);
        debug_assert(!!it, key);
        return *it;
    }
    /// @brief Get element by @a key from const @a container or throw
    template <class Exception, class Container, class Key, class ... Args>
    const typename Container::mapped_type&
    getOrThrow(const Container& container, const Key& key, const Args& ... args)
    {
        auto it = findOrDefault(container, key);
        LS_THROW(!!it, Exception, args...);
        return *it;
    }

    /// @brief Iterate over tuple - impl
    template <int Index, class Callback, class... Ts>
    struct IterateTupleImpl
    {
        /// @brief Do
        void operator ()(tuple<Ts...>& t, Callback callback)
        {
            IterateTupleImpl<Index - 1, Callback, Ts...>()(t, callback);
            callback(std::get<Index>(t));
        }
        /// @brief Do
        void operator ()(const tuple<Ts...>& t, Callback callback)
        {
            IterateTupleImpl<Index - 1, Callback, Ts...>()(t, callback);
            callback(std::get<Index>(t));
        }
    };
    /// @brief Iterate over tuple - impl
    template <class Callback, class... Ts>
    struct IterateTupleImpl <0, Callback, Ts...>
    {
        /// @brief Do
        void operator ()(tuple<Ts...>& t, Callback callback)
        {
            callback(std::get<0>(t));
        }
        /// @brief Do
        void operator ()(const tuple<Ts...>& t, Callback callback)
        {
            callback(std::get<0>(t));
        }
    };
    /// @brief For-each tuple element
    template <class Callback, class... Ts>
    void for_each(tuple<Ts...>& t, Callback callback)
    {
        constexpr size_t size = std::tuple_size<tuple<Ts...>>::value;
        IterateTupleImpl<size - 1, Callback, Ts...> it;
        it(t, callback);
    }
    /// @brief For-each tuple element
    template <class Callback, class... Ts>
    void for_each(const tuple<Ts...>& t, Callback callback)
    {
        constexpr size_t size = std::tuple_size<tuple<Ts...>>::value;
        IterateTupleImpl<size - 1, Callback, Ts...> it;
        it(t, callback);
    }
    /// @brief Type wrapper to pass it into template operator()
    template <class T>
    struct TypeWrapper
    {
    };
    /// @brief Iterate over tuple type
    template <int Index, class Callback, class Tuple>
    struct IterateTupleTypeImpl
    {
        /// @brief Do
        bool operator ()(Callback& callback)
        {
            if (!IterateTupleTypeImpl<Index - 1, Callback, Tuple>()(callback))
                return false;
            return callback(TypeWrapper<std::tuple_element_t<Index, Tuple>>());
        }
    };
    /// @brief Iterate over tuple - impl
    template <class Callback, class Tuple>
    struct IterateTupleTypeImpl <0, Callback, Tuple>
    {
        /// @brief Do
        bool operator ()(Callback& callback)
        {
            return callback(TypeWrapper<std::tuple_element_t<0, Tuple>>());
        }
    };
    /// @brief Iterate over tuple - impl empty
    template <class Callback, class Tuple>
    struct IterateTupleTypeImpl <-1, Callback, Tuple>
    {
        /// @brief Do nothing
        bool operator ()(Callback& /*callback*/)
        {
            return true;
        }
    };
    /// @brief For-each tuple type
    template <class Tuple, class Callback>
    bool for_each_type(Callback& callback)
    {
        constexpr size_t size = std::tuple_size<Tuple>::value;
        IterateTupleTypeImpl<size - 1, Callback, Tuple> it;
        return it(callback);
    }

    /// @brief Get tuple element index by type - impl
    template <class T, size_t N, class... Args>
    struct TupleNumberByType
    {
        /// @brief Result
        static constexpr size_t value = N;
    };
    /// @brief Get tuple element index by type - impl
    template <class T, size_t N, class... Args>
    struct TupleNumberByType<T, N, T, Args...>
    {
        /// @brief Result
        static constexpr size_t value = N;
    };
    /// @brief Get tuple element index by type - impl
    template <class T, std::size_t N, class U, class... Args>
    struct TupleNumberByType<T, N, U, Args...>
    {
        /// @brief Result
        static constexpr size_t value =
            TupleNumberByType<T, N + 1, Args...>::value;
    };
    /// @brief Check whether tuple has type - impl
    template <class T, class... Args>
    struct TupleHasTypeImpl;
    /// @brief Check whether tuple has type - impl
    template <class T>
    struct TupleHasTypeImpl<T>
    {
        /// @brief Check result
        static const bool value = false;
    };
    /// @brief Check whether tuple has type - impl
    template <class T, class... Args>
    struct TupleHasTypeImpl<T, T, Args...>
    {
        /// @brief Check result
        static const bool value = true;
    };
    /// @brief Check whether tuple has type - impl
    template <class T, class U, class ... Args>
    struct TupleHasTypeImpl<T, U, Args...>
    {
        /// @brief Check result
        static const bool value = TupleHasTypeImpl<T, Args...>::value;
    };
    /// @brief Check whether tuple has type
    template <class Type, class Tuple>
    struct TupleHasType;
    /// @brief Check whether tuple has type
    template <class Type, class ... Args>
    struct TupleHasType<Type, std::tuple<Args...>>
    {
        static const bool value = TupleHasTypeImpl<Type, Args...>::value;
    };
    /// @brief Get tuple type index
    template <class Type, class Tuple>
    struct TupleTypeIndex;
    /// @brief Get tuple type index
    template <class Type, class ... Args>
    struct TupleTypeIndex<Type, std::tuple<Args...>>
    {
        static const uint value = TupleNumberByType<Type, 0, Args...>::value;
    };
    /// @brief Get tuple element by type
    template <class T, class... Args>
    auto& elementByType(std::tuple<Args...>& t)
    {
        return std::get<TupleNumberByType<T, 0, Args...>::value> (t);
    }
    /// @brief Get tuple const element by type
    template <class T, class... Args>
    const auto& elementByType(std::tuple<Args...> const& t)
    {
        return std::get<TupleNumberByType<T, 0, Args...>::value> (t);
    }
    /// @brief Lerp in container by @a position in R [0, N-1];
    template<class Value, class Container, class Scalar>
    Value interpolateArray(const Container& elements, const uint maxIndex, const Scalar position)
    {
        Scalar factor = clamp<Scalar>(position, 0, static_cast<Scalar>(maxIndex));
        const uint base = min(static_cast<uint>(factor), maxIndex - 1);
        factor -= base;
        return Value(elements[base], elements[base + 1], factor);
    }
    /// @brief Lerp in container by relative @a factor in R [0, 1].
    template<class Value, class Container, class Scalar>
    Value interpolateArrayFactor(const Container& elements,
                                 const uint maxIndex,
                                 const Scalar factor)
    {
        return interpolateArray<Value>(elements, maxIndex, factor * maxIndex);
    }
    /// @brief Container to range
    template <class Container>
    auto asRange(Container&& container)
    {
        return make_pair(std::begin(container), std::end(container));
    }
    /// @brief Fill data with zeros
    template <class T>
    void zeroMemory(T& object)
    {
        memset(&object, 0, sizeof(T));
    }
    /// @brief Is future ready?
    template <typename R>
    bool isReady(const std::future<R>& fut)
    {
        return fut.valid() 
            && (fut.wait_for(std::chrono::seconds(0)) == std::future_status::ready);
    }
    /// @}
}

/// @brief STL name-space
namespace std
{
    /// @brief Begin for pair
    template <class Iterator>
    Iterator begin(const pair<Iterator, Iterator>& p) noexcept
    {
        return p.first;
    }
    /// @brief End for pair
    template <class Iterator>
    Iterator end(const pair<Iterator, Iterator>& p) noexcept
    {
        return p.second;
    }
}
