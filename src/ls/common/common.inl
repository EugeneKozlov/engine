/// @file ls/common/common.inl
/// @brief Implement stuff
#pragma once

#include "ls/common/Observable.h"
#include "ls/common/assert.h"

namespace ls
{
    template <class T>
    void ObservableInterface<T>::addObserver(Observer& observer)
    {
        debug_assert(!hasObserver(observer));

        m_observers.emplace(&observer);
        doAddObserver(observer);
    }
    template <class T>
    void ObservableInterface<T>::removeObserver(Observer& observer)
    {
        debug_assert(hasObserver(observer));

        doRemoveObserver(observer);
        m_observers.erase(&observer);
    }
    template <class T>
    void ObservableInterface<T>::discardObservers()
    {
        for (Observer* observer : m_observers)
            doRemoveObserver(*observer);
        m_observers.clear();
    }
}