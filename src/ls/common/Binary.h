/// @file ls/io/Binary.h
/// @brief Binary stream read/write
#pragma once

#include "ls/common/concept.h"
#include "ls/common/container.h"
#include "ls/common/import.h"

namespace ls
{
    /// @addtogroup LsCommon
    /// @{

    static_assert(std::numeric_limits<float>::is_iec559, "Only IEC 559 floats are supported");
    static_assert(std::numeric_limits<double>::is_iec559, "Only IEC 559 floats are supported");
    /// @brief Binary read/write helper
    /// @note Integers and reals are written in native endianness
    template <class Object>
    struct Binary
    {
        static_assert(std::is_standard_layout<Object>::value, "Only standard layout types can be raw-copied");
        /// @brief Read binary data
        template <class Stream>
        static void read(Stream& stream, Object& object)
        {
            stream.read(&object, sizeof(object));
        }
        /// @brief Write binary data
        template <class Stream>
        static void write(Stream& stream, const Object& object)
        {
            stream.write(&object, sizeof(object));
        }
    };
    /// @brief string
    template <>
    struct Binary<string>
    {
        /// @brief Read binary string
        template <class Stream>
        static void read(Stream& stream, string& object)
        {
            uint len;
            Binary<uint>::read(stream, len);
            object.resize(len);
            if (!object.empty())
            {
                stream.read(&*object.begin(), len);
            }
        }
        /// @brief Write binary string
        template <class Stream>
        static void write(Stream& stream, const string& object)
        {
            Binary<uint>::write(stream, object.size());
            if (!object.empty())
            {
                stream.write(object.c_str(), object.size());
            }
        }
    };
    /// @brief vector
    template <class Object>
    struct Binary<vector<Object>>
    {
        /// @brief Read binary vector
        template <class Stream>
        static void read(Stream& stream, vector<Object>& object)
        {
            uint len;
            Binary<uint>::read(stream, len);
            object.resize(len);
            for (Object& item : object)
            {
                Binary<Object>::read(stream, item);
            }
        }
        /// @brief Write binary vector
        template <class Stream>
        static void write(Stream& stream, const vector<Object>& object)
        {
            Binary<uint>::write(stream, object.size());
            for (const Object& item : object)
            {
                Binary<Object>::write(stream, item);
            }
        }
    };
    /// @brief array
    template <class Object, uint Size>
    struct Binary<array<Object, Size>>
    {
        /// @brief Read binary vector
        template <class Stream>
        static void read(Stream& stream, array<Object, Size>& object)
        {
            for (Object& item : object)
            {
                Binary<Object>::read(stream, item);
            }
        }
        /// @brief Write binary vector
        template <class Stream>
        static void write(Stream& stream, const array<Object, Size>& object)
        {
            for (const Object& item : object)
            {
                Binary<Object>::write(stream, item);
            }
        }
    };
    /// @brief [unordered-][multi-]map
    template <class Key, class Value>
    struct Binary<hamap<Key, Value>>
    {
        /// @brief Read binary map
        template <class Stream>
        static void read(Stream& stream, hamap<Key, Value>& object)
        {
            // Read length
            uint len;
            Binary<uint>::read(stream, len);

            // Read key/value pairs
            object.clear();
            for (uint idx = 0; idx < len; idx++)
            {
                Key key;
                Value value;
                Binary<Key>::read(stream, key);
                Binary<Value>::read(stream, value);
                object.emplace(key, value);
            }
        }
        /// @brief Write binary map
        template <class Stream>
        static void write(Stream& stream, const hamap<Key, Value>& object)
        {
            // Write length
            Binary<uint>::write(stream, object.size());

            // Write key/value pairs
            for (const pair<Key, Value>& item : object)
            {
                Binary<Key>::write(stream, item.first);
                Binary<Value>::write(stream, item.second);
            }
        }
    };
    /// @brief Read binary object
    template <class Object, class Stream>
    void readBinary(Stream& stream, Object& object)
    {
        Binary<Object>::read(stream, object);
    }
    /// @brief Read binary object
    template <class Object, class Stream>
    Object readBinary(Stream& stream)
    {
        Object object;
        Binary<Object>::read(stream, object);
        return object;
    }
    /// @brief Write binary data
    template <class Object, class Stream>
    void writeBinary(Stream& stream, const Object& object)
    {
        Binary<Object>::write(stream, object);
    }
    /// @brief Archive Mode
    enum ArchiveMode : bool
    {
        /// @brief Read from archive
        ArchiveRead = false,
        /// @brief Write to archive
        ArchiveWrite = true
    };
    /// @brief Archive template
    template <class Stream>
    class Archive
    {
    public:
        /// @brief Ctor
        Archive(Stream& stream, const ArchiveMode mode)
            : m_stream(stream)
            , m_mode(mode)
        {
        }
        /// @brief Get stream
        Stream& stream()
        {
            return m_stream;
        }
        /// @brief Is it write archive?
        bool isWrite()
        {
            return m_mode == ArchiveWrite;
        }
        /// @brief Is it read archive?
        bool isRead()
        {
            return m_mode == ArchiveRead;
        }
    private:
        Stream& m_stream;
        const ArchiveMode m_mode;
    };
    /// @brief Serialize binary
    template <class Object, class Stream>
    void serializeBinary(Archive<Stream>& archive, Object& object)
    {
        if (archive.isWrite())
        {
            writeBinary(archive.stream(), object);
        } 
        else
        {
            readBinary(archive.stream(), object);
        }
    }
}

