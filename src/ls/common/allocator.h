/// @file ls/common/allocator.h
/// @brief Standard and fast allocators
#pragma once

#include "ls/common/config.h"

namespace ls
{
    /// @addtogroup LsCommon
    /// @{

    /// @brief Standard allocator type
    template<class Type>
    using StdAllocator = std::allocator<Type>;
    /// @brief Fast allocator type (list, set, map, deque, etc)
    template<class Type>
    using FastAllocator
#if LS_USE_FAST_ALLOCATOR == 0
    = std::allocator<Type>;
#else
    = boost::fast_pool_allocator<Type>;
#endif
    /// @brief Fast pool allocator type (vector)
    template<class Type>
    using FastPoolAllocator
#if LS_USE_FAST_ALLOCATOR == 0
    = std::allocator<Type>;
#else
    = boost::pool_allocator<Type>;
#endif
    /// @}
}
