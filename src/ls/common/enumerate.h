/// @file ls/common/enumerate.h
/// @brief Enum iterator
#pragma once

#include "ls/common/import.h"

namespace ls
{
    /// @addtogroup LsCommon
    /// @{

    /// @brief Enum Iterator
    template <class Enum>
    class EnumIterator : std::iterator<std::forward_iterator_tag, Enum>
    {
    public:
        /// @brief Empty Ctor
        EnumIterator() = default;
        /// @brief Ctor
        EnumIterator(const Enum value)
            : m_value(value)
        {
        }
        /// @brief Copy Ctor
        EnumIterator(const EnumIterator& another) = default;
        /// @brief Assign
        EnumIterator& operator = (const EnumIterator& another) = default;
        /// @brief Compare
        bool operator ==(const EnumIterator& rhs)
        {
            return m_value == rhs.m_value;
        }
        /// @brief Compare
        bool operator !=(const EnumIterator& rhs)
        {
            return m_value != rhs.m_value;
        }
        /// @brief Deref
        Enum& operator * ()
        {
            return m_value;
        }
        /// @brief Deref
        Enum operator * () const
        {
            return m_value;
        }
        /// @brief Pre-Increment
        EnumIterator& operator ++ ()
        {
            m_value = (Enum) ((uint) m_value + 1);
            return *this;
        }
        /// @brief Post-Increment
        EnumIterator operator ++ (int)
        {
            EnumIterator tmp(*this);
            ++*this;
            return tmp;
        }
    private:
        Enum m_value = static_cast<Enum>(-1);
    };
    /// @brief Enumerate constants between @a begin and @a end
    template <class Enum>
    pair<EnumIterator<Enum>, EnumIterator<Enum>> enumerate(const Enum begin, const Enum end)
    {
        return make_pair(EnumIterator<Enum>(begin), EnumIterator<Enum>(end));
    }
    /// @}
}
