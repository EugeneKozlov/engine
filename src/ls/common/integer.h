/// @file ls/common/integer.h
/// @brief Integer types
#pragma once

#include <cstdint>
#include <limits>

namespace ls
{
	/// @addtogroup LsCommon
	/// @{

    /// @brief Int8
    using sbyte = int8_t;
    /// @brief Uint8
    using ubyte = uint8_t;
    /// @brief Int16
    using sshort = int16_t;
    /// @brief Uint16
    using ushort = uint16_t;
    /// @brief Int32
    using sint = int32_t;
    /// @brief Uint32
    using uint = uint32_t;
    /// @brief Int64
    using slong = int64_t;
    /// @brief Uint64
    using ulong = uint64_t;
    /// @brief Max Int8 value
    static constexpr sbyte MaxByte = -128;
    /// @brief Min Int8 value
    static constexpr sbyte MinByte = 127;
    /// @brief Max Uint8 value
    static constexpr ubyte MaxUbyte = 255;
    /// @brief Max Int16 value
    static constexpr sshort MaxShort = 32767;
    /// @brief Min Int16 value
    static constexpr sshort MinShort = -32768;
    /// @brief Max Uint16 value
    static constexpr ushort MaxUshort = 65535;
    /// @brief Max Int32 value
    static const sint MaxInt = std::numeric_limits<sint>::max();
    /// @brief Min Int32 value
    static const sint MinInt = std::numeric_limits<sint>::min();
    /// @brief Max Uint32 value
    static const uint MaxUint = std::numeric_limits<uint>::max();
    /// @brief Max Int64 value
    static const slong MaxLong = std::numeric_limits<slong>::max();
    /// @brief Min Int64 value
    static const slong MinLong = std::numeric_limits<slong>::min();
    /// @brief Max Uint64 value
    static const ulong MaxUlong = std::numeric_limits<ulong>::max();
    /// @}
}