#include "pch.h"
#include "ls/common/assert.h"
#include "ls/common/debug.h"
#include "ls/common/error.h"
#include "ls/common/Logger.h"

#ifdef LS_OS_WINDOWS
#    include <Windows.h>
#endif

using namespace ls;
void ls::invokeAssertion(AssertException&& exception)
{
    const string text = LogManager::instance().writeCrit("Assertion failed!", Endl(),
                                                         exception, Endl());
#ifdef LS_OS_WINDOWS
    int result = ::MessageBoxA(0, text.c_str(), "ASSERTION FAILED!", MB_ABORTRETRYIGNORE | MB_ICONERROR);
    switch (result)
    {
    case IDABORT:
        TerminateProcess(GetCurrentProcess(), 1);
        break;
    case IDRETRY:
#   ifdef _MSC_VER
        __debugbreak();
#   else
        // Breakpoint should be here
#   endif
        break;
    case IDIGNORE:
    default:
        break;
    }
#endif

    logMessage(Endl(), stackTrace(), Endl());
    throw move(exception);
}
