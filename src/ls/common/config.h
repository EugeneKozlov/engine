/// @file ls/common/config.h
/// @brief Global configuration
#pragma once

/// @addtogroup LsConfig
/// @{

/// @def LS_OS_WINDOWS
/// Defined and true if Windows
#define LS_OS_WINDOWS 0
#if defined(_WIN32) || defined(_MSC_VER)
#undef LS_OS_WINDOWS
#define LS_OS_WINDOWS 1
#else
#error Fixme
#endif

/// @def LS_DEBUG
/// Is project compiled in debug mode?
#ifdef NDEBUG
#define LS_DEBUG 0
#else
#define LS_DEBUG 1
#endif

/// @def LS_DISABLE_ASSERT
/// Set to disable debug assertions
#define LS_DISABLE_ASSERT 0

/// @def LS_OVERRIDE_ASSERT
/// Set to override default @a assert macro
#define LS_OVERRIDE_ASSERT 0

/// @def LS_DEFAULT_LOG_NAME
/// Default log name
#define LS_DEFAULT_LOG_NAME "log"

/// @def LS_USE_FAST_ALLOCATOR
/// Set to use fast pool allocator
#define LS_USE_FAST_ALLOCATOR 0

/// @def LS_USE_BOOST_CONTAINER
/// Set to use boost containers instead of STL
#define LS_USE_BOOST_CONTAINER 0

/// @def LS_COMPILE_DX11
/// Set to compile DX11 GAPI
#define LS_COMPILE_DX11 1

/// @def LS_COMPILE_OPENGL
/// Set to compile OpenGL GAPI
#define LS_COMPILE_OPENGL 0

/// @def LS_OPENGL_FIX_MAPPING
/// Workaround OpenGL buffer mapping bug
#define LS_OPENGL_FIX_MAPPING 1

/// @def LS_OPENGL_FIX_INSTANCING
/// Workaround OpenGL instancing base instance bug
#define LS_OPENGL_FIX_INSTANCING 1

/// @def LS_DETECT_MEMORY_LEAKS
/// Set to detect memory leaks
#define LS_DETECT_MEMORY_LEAKS 0

/// @}