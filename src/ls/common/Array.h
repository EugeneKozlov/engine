/// @file ls/common/Array.h
/// @brief Multi-dimensional array
#pragma once

#include "ls/common/assert.h"
#include "ls/common/import.h"

namespace ls
{
    /// @addtogroup LsCommon
    /// @{

    /// @brief For-each flat array
    template <class Array, class Functor>
    enable_if_t<Array::Depth == 0> foreach(Array& array, Functor& foo)
    {
        for (auto& item : array)
            foo(item);
    }
    /// @brief For-each multi-array
    template <class Array, class Functor>
    enable_if_t<Array::Depth != 0> foreach(Array& array, Functor& foo)
    {
        for (auto& item : array)
            foreach(item, foo);
    }
    /// @brief Allows element conversion
    struct ConvertTag
    {
    };
    /// @brief 1D array
    /// @tparam T Element type
    /// @tparam N Array length
    /// @tparam D Array depth
    template <class T, uint N, uint D>
    class LinearArray
    {
    public:
        /// @brief This type
        using This = LinearArray<T, N, D>;
        /// @brief Element type
        using Element = T;
        /// @brief Array size
        static constexpr uint Size = N;
        /// @brief Array depth
        static constexpr uint Depth = D;
    public:
        /// @brief Empty ctor
        LinearArray() = default;
        /// @brief Copy ctor
        LinearArray(const LinearArray& another) = default;
        /// @brief Move
        LinearArray(LinearArray&& another)
            : m_data(move(another.m_data))
        {

        }
        /// @brief Dtor
        ~LinearArray() noexcept = default;
        /// @brief Assign
        LinearArray& operator = (const LinearArray& another) = default;
        /// @brief Fill ctor (init with @a value)
        LinearArray(const Element& value) noexcept
        {
            fill(value);
        }
        /// @brief Convert and fill ctor
        template <class Object>
        LinearArray(const Object& value, ConvertTag)
        {
            fill(value, ConvertTag());
        }
        /// @brief Assign ctor
        template <class Iteartor>
        LinearArray(Iteartor begin, Iteartor end)
        {
            for (uint i = 0; i < Size; i++)
            {
                debug_assert(begin != end, "index = ", i, "; size = ", Size);
                m_data[i] = *begin;
                ++begin;
            }
        }
        /// @brief Copy ctor
        LinearArray(initializer_list<Element> list)
            : LinearArray(list.begin(), list.end())
        {
        }

        /// @brief Fill array by @a value
        void fill(const Element& value) noexcept
        {
            for (auto& item : m_data)
                item = value;
        }
        /// @brief Fill array by any type @a value
        template <class Object>
        void fill(const Object& value, ConvertTag)
        {
            for (auto& item : m_data)
            {
                item.~Element();
                new (&item) Element(value);
            }
        }
        /// @brief For-each
        template <class Functor>
        void foreach(Functor foo)
        {
            ls::foreach(*this, foo);
        }
        /// @brief Const for-each
        template <class Functor>
        void foreach(Functor foo) const
        {
            ls::foreach(*this, foo);
        }

        /// @brief Get element by @a index
        Element& at(const uint index) noexcept
        {
            debug_assert(index < Size);

            return m_data[index];
        }
        /// @brief Get element by index array
        template<class... IndexTypes>
        auto& at(const uint index,
                 const uint nextIndex,
                 IndexTypes ... tail) noexcept
        {
            static_assert(Depth >= sizeof...(tail) + 1, "too many indices");

            return at(index).at(nextIndex, tail...);
        }
        /// @brief Get element by @a index
        const Element& at(const uint index) const noexcept
        {
            debug_assert(index < Size);

            return m_data[index];
        }
        /// @brief Get element by index array
        template<class... IndexTypes>
        const auto& at(const uint index,
                       const uint nextIndex,
                       IndexTypes ... tail) const noexcept
        {
            static_assert(Depth >= sizeof...(tail) + 1, "too many indices");

            return at(index).at(nextIndex, tail...);
        }
        /// @brief Get array begin iterator
        auto begin() noexcept
        {
            return m_data.begin();
        };
        /// @brief Get array end iterator
        auto end() noexcept
        {
            return m_data.end();
        };
        /// @brief Get array begin const iterator
        const auto begin() const noexcept
        {
            return m_data.begin();
        };
        /// @brief Get array end const iterator
        const auto end() const noexcept
        {
            return m_data.end();
        };
        /// @brief Get element
        Element& operator [](const uint index) noexcept
        {
            return at(index);
        }
        /// @brief Get const element
        const Element& operator [](const uint index) const noexcept
        {
            return at(index);
        }
        /// @brief Get data
        Element* data() noexcept
        {
            return m_data.data();
        }
        /// @brief Get const data
        const Element* data() const noexcept
        {
            return m_data.data();
        }
        /// @brief Get max size
        const uint size() const noexcept
        {
            return Size;
        }
    private:
        /// @brief Data
        std::array<Element, Size> m_data;
    };
    /// @brief Multi-D array impl
    /// @tparam T Element type
    /// @tparam I First dim
    /// @tparam J... Other dims
    template <class T, uint I, uint ... J>
    struct MultiArrayImpl
    {
        static_assert(I > 0, "zero-size array is not allowed");
        /// @brief Nested array type
        using Nested = typename MultiArrayImpl<T, J...>::type;
        /// @brief This array type
        using type = LinearArray<Nested, I, sizeof...(J)>;
    };
	/// @brief Multi-D array impl
	/// @tparam T Element type
	/// @tparam I First dim
	/// @tparam J... Other dims
    template <class T, uint I>
    struct MultiArrayImpl<T, I>
    {
        static_assert(I > 0, "zero-size array is not allowed");
        /// @brief This array type
        using type = LinearArray<T, I, 0>;
    };
	/// @brief Multi-D array
    template <class T, uint ... I>
    using MultiArray = typename MultiArrayImpl<T, I...>::type;
    /// @brief Copy string to array
    template <uint N>
    void string2array(MultiArray<ubyte, N>& array, const string& str)
    {
        const uint len = std::min(str.length(), N - 1);
        strncpy(reinterpret_cast<char*>(array.data()), str.c_str(), len);
        for (uint i = len; i < N; ++i)
            array[i] = 0;
    }
    /// @}
}
