#include "pch.h"
#include "ls/common/BlockProfiler.h"

namespace ls
{
    template<>
    const char* durationSuffix<std::chrono::hours>()
    {
        return "h";
    }
    template<>
    const char* durationSuffix<std::chrono::minutes>()
    {
        return "m";
    }
    template<>
    const char* durationSuffix<std::chrono::seconds>()
    {
        return "s";
    }
    template<>
    const char* durationSuffix<std::chrono::milliseconds>()
    {
        return "ms";
    }
    template<>
    const char* durationSuffix<std::chrono::microseconds>()
    {
        return "us";
    }
    template<>
    const char* durationSuffix<std::chrono::nanoseconds>()
    {
        return "ns";
    }
}
