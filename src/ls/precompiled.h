/// @file ls/precompiled.h
/// @brief Precompiled standard headers
#pragma once

#define _WIN32_WINNT 0x0501
#define BOOST_BIND_NO_PLACEHOLDERS
#define BOOST_THREAD_NO_LIB
#define BOOST_CHRONO_NO_LIB
#define BOOST_REGEX_NO_LIB
#ifndef _MSC_VER
#pragma GCC diagnostic ignored "-Wstrict-aliasing"
#endif

// C-lib
#include <cstdio>
#include <ctime>
#include <cmath>
// Containers
#include <string>
#include <vector>
#include <set>
#include <map>
#include <deque>
#include <queue>
#include <stack>
#include <list>
#include <unordered_set>
#include <unordered_map>
#include <bitset>
// Common
#include <algorithm>
#include <numeric>
// Patterns
#include <boost/any.hpp>
#include <boost/optional.hpp>
#include <boost/variant.hpp>
// Streams
#include <iostream>
#include <sstream>
// Time
#include <chrono>
// Functional
#include <functional>
// Memory
#include <memory>
#include <boost/pool/pool_alloc.hpp>
#include <boost/pool/object_pool.hpp>
// Templates
#include <type_traits>
#include <typeindex>
#include <utility>
// Networking
#define BOOST_USE_WINDOWS_H
#include <boost/asio/io_service.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <boost/asio/ip/address.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ip/udp.hpp>
// Threading
#include <atomic>
#include <thread>
#include <future>
#include <condition_variable>
#include <mutex>
// Xml
#include <boost/property_tree/ptree.hpp>

#include "ls/common/config.h"
#if LS_DETECT_MEMORY_LEAKS != 0
    #define _CRTDBG_MAP_ALLOC
    #include <stdlib.h>
    #include <crtdbg.h>
    //#include <vld.h>
#endif
