/// @file ls/common.h
/// @brief Common header
#pragma once

#include "ls/common/algorithm.h"
#include "ls/common/allocator.h"
#include "ls/common/concept.h"
#include "ls/common/config.h"
#include "ls/common/container.h"
#include "ls/common/enumerate.h"
#include "ls/common/functional.h"
#include "ls/common/import.h"
#include "ls/common/integer.h"

#include "ls/common/assert.h"
#include "ls/common/debug.h"
#include "ls/common/error.h"
#include "ls/common/format.h"
#include "ls/common/time.h"

#include "ls/common/Array.h"
#include "ls/common/ArrayWrapper.h"
#include "ls/common/Binary.h"
#include "ls/common/Exception.h"
#include "ls/common/FlagSet.h"
#include "ls/common/FlatIndexRange.h"
#include "ls/common/Observable.h"

#include "ls/common/BlockProfiler.h"

#include "ls/math/Frustum.h"
#include "ls/math/Matrix.h"
#include "ls/math/Plane.h"
#include "ls/math/Quaternion.h"
#include "ls/math/Region.h"
#include "ls/math/Vector.h"
#include "ls/math/color.h"
#include "ls/math/common.h"
#include "ls/math/endianness.h"
#include "ls/math/matrix_construct.h"
#include "ls/math/random.h"

#include "ls/math/alias.h"

#include "ls/common/common.inl"