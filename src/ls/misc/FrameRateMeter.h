/// @file ls/misc/FrameRateMeter.h
/// @brief Support class to measure total performance
#pragma once

#include "ls/common.h"

namespace ls
{
    /// @addtogroup LsMisc
    /// @{

    /// Support class to measure total performance
    class FrameRateMeter
    {
    public:
        /// @brief Clock type
        using Clock = high_resolution_clock;
        /// @brief Ctor
        FrameRateMeter()
        {
        }
        /// @brief Add frame
        void step()
        {
            Clock::time_point currentTime = Clock::now();
            if (m_first)
            {
                m_previousTime = currentTime;
                m_first = false;
                return;
            }

            auto duration = currentTime - m_previousTime;
            m_previousTime = currentTime;
            uint delta = (uint) duration_cast<microseconds>(duration).count();
            m_deltas.push_back(delta);
        }
        /// @brief Flush all and recompute info
        void flush()
        {
            uint numDeltas = m_deltas.size();
            if (m_deltas.empty())
                return;

            m_maxFrameDuration = static_cast<float>(m_deltas.front());
            m_minFrameDuration = static_cast<float>(m_deltas.front());
            m_averageFrameDuration = 0;
            for (uint delta : m_deltas)
            {
                m_minFrameDuration = min(m_minFrameDuration, static_cast<float>(delta));
                m_maxFrameDuration = max(m_maxFrameDuration, static_cast<float>(delta));
                m_averageFrameDuration += static_cast<float>(delta);
            }
            m_averageFrameDuration /= numDeltas * 1000;
            m_minFrameDuration /= 1000;
            m_maxFrameDuration /= 1000;

            m_minFps = 1000 / m_maxFrameDuration;
            m_averageFps = 1000 / m_averageFrameDuration;
            m_maxFps = 1000 / m_minFrameDuration;

            m_deltas.clear();
        }

        /// @name Gets
        /// @{

        /// @brief Get min frame duration
        float minFrameDuration() const noexcept
        {
            return m_minFrameDuration;
        }
        /// @brief Get max frame duration
        float maxFrameDuration() const noexcept
        {
            return m_maxFrameDuration;
        }
        /// @brief Get average frame duration
        float averageFrameDuration() const noexcept
        {
            return m_averageFrameDuration;
        }
        /// @brief Get min FPS
        float minFps() const noexcept
        {
            return m_minFps;
        }
        /// @brief Get max FPS
        float maxFps() const noexcept
        {
            return m_maxFps;
        }
        /// @brief Get average FPS
        float averageFps() const noexcept
        {
            return m_averageFps;
        }
        /// @brief Get standard frame duration time string
        string durationText() const
        {
            return format("{*} ms ({*} .. {*})", Lowp(),
                          averageFrameDuration(),
                          minFrameDuration(), maxFrameDuration());
        }
        /// @brief Get standard FPS string
        string fpsText() const
        {
            return format("{*} FPS ({*} .. {*})",
                          static_cast<uint>(averageFps()),
                          static_cast<uint>(minFps()), static_cast<uint>(maxFps()));
        }
        /// @}
    private:
        bool m_first = true;
        Clock::time_point m_previousTime;
        vector<uint> m_deltas;

        float m_minFrameDuration = 0;
        float m_averageFrameDuration = 0;
        float m_maxFrameDuration = 0;

        float m_minFps = 0;
        float m_averageFps = 0;
        float m_maxFps = 0;
    };
    /// @}
}
