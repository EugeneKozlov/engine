/** @file ls/misc/SlidingArray2D.h
 *  @brief Sliding flat array.
 */
#pragma once
#include "ls/common.h"

namespace ls
{
    /** Sliding 2D array.
     *  This is 'unbounded' array with indices from -inf to inf
     *  around specified movable center
     *  @tparam T Element type
     */
    template <class T>
    class SlidingArray2D
    {
    public:
        /// @brief Element type

        typedef T Element;
        /** @name Main
         *  @{
         */
        /// @brief Degenerate
        SlidingArray2D() noexcept
            : m_size(0)
            , m_data()
            , m_offset(0)
            , m_cross(0)
        {
        }
        /// @brief Copy.
        SlidingArray2D(const SlidingArray2D& another)
            : m_size(another.m_size)
            , m_data(another.m_data)
            , m_offset(0)
            , m_cross(0)
        {
        }
        /// @brief New array.
        SlidingArray2D(const int2& offset, uint width, uint height)
            : m_size((sint) width, (sint) height)
            , m_data(length())
            , m_offset(offset)
            , m_cross(0)
        {
            upcast(offset, m_size, &m_cross);
        }
        /// @brief New array with element-wise value.
        SlidingArray2D(const int2& offset, Element value, uint width, uint height)
            : m_size((sint) width, (sint) height)
            , m_data(length(), value)
            , m_offset(offset)
            , m_cross(0)
        {
            upcast(offset, m_size, &m_cross);
        }
        /// @brief Swap with another
        void swap(SlidingArray2D<Element>& rhs) noexcept
        {
            std::swap(m_data, rhs.m_data);
            std::swap(m_size, rhs.m_size);
            std::swap(m_offset, rhs.m_offset);
            std::swap(m_cross, rhs.m_cross);
        }
        /** Move to another position
         *  @param target New offset
         *  @param leaveCallback Will be called for each lost cell
         *    @code void leave_callback(T& cell, int2 global) @endcode
         *  @param enterCallback Will be called for each obtained cell
         *    @code void enter_callback(T& cell, int2 global) @endcode
         */
        template<class F, class G>
        void slip(const int2& target, F leaveCallback, G enterCallback) noexcept
        {
            if (m_offset == target)
                return;
            int2 i;
            for (i.x = 0; i.x < m_size.x; ++i.x)
            {
                for (i.y = 0; i.y < m_size.y; ++i.y)
                {
                    int2 global = i + m_offset;
                    int2 loc = local(global);
                    int2 relative = global - target;
                    if (!inside(relative, int2(0), m_size))
                    {
                        // Leave
                        leaveCallback(get(loc), global);
                        // Find entered element
                        if (relative.x < 0) relative.x += m_size.x;
                        else if (relative.x >= m_size.x) relative.x -= m_size.x;
                        if (relative.y < 0) relative.y += m_size.y;
                        else if (relative.y >= m_size.x) relative.y -= m_size.y;
                        // Enter
                        global = relative + target;
                        enterCallback(get(loc), global);
                    }
                }
            }
            upcast(m_cross + target - m_offset, m_size, &m_cross);
            m_offset = target;
        }
        /** Process all elements.
         *  @param processor Elements processor
         *    @code void processor(T& elem, int2 global) @endcode
         */
        template<class F>
        void process(F processor) noexcept
        {
            int2 begin, end;
            bounds(begin, end);
            int2 i;
            for (i.x = begin.x; i.x < end.x; ++i.x)
                for (i.y = begin.y; i.y < end.y; ++i.y)
                    processor(element(i), i);
        }
        /// @}

        /** @name Gets
         *  @param local Local element index
         *  @param global Global element index
         *  @{
         */
        /// @brief Get element by global
        inline Element& element(const int2& global) noexcept
        {
            return get(local(global));
        }
        /// @brief Get element by global
        inline const Element& element(const int2& global) const noexcept
        {
            return get(local(global));
        }
        /// @brief Access
        inline Element& operator [] (const int2& global) noexcept
        {
            return element(global);
        }
        /// @brief Access
        inline const Element& operator [] (const int2& global) const noexcept
        {
            return element(global);
        }
        /** Get bounds (min and max+1)
         *  @param [out] begin,end
         *    Bounds
         */
        inline void bounds(int2& begin, int2& end) const noexcept
        {
            begin = m_offset;
            end = m_offset + m_size;
        }
        /// @brief Check if element is inside area
        inline bool isInside(const int2& global) const noexcept
        {
            return inside(global, m_offset, m_size + m_offset);
        }
        /// @brief Get size
        inline const int2& size() const noexcept
        {
            return m_size;
        }
        /// @brief Get data length
        inline const uint length() const noexcept
        {
            return uint(m_size.x * m_size.y);
        }
        /// @brief Get offset
        inline const int2& offset() const noexcept
        {
            return m_offset;
        }
        /// @}
    private:
        /// @brief Local index from global, fail on out-of-area
        inline int2 local(const int2& global) const noexcept
        {
            debug_assert(isInside(global), global);
            int2 local;
            upcast(global - m_offset + m_cross, m_size, &local);
            return local;
        }
        /// @brief Get element by local
        Element& get(const int2& local) noexcept
        {
            debug_assert(inside(local, int2(0), m_size), local);
            return m_data[local.x + local.y * m_size.x];
        }
        /// @brief Get element by local
        const Element& get(const int2& local) const noexcept
        {
            debug_assert(inside(local, int2(0), m_size), local);
            return m_data[local.x + local.y * m_size.x];
        }
    private:
        /// @brief Array size
        int2 m_size;
        /// @brief Array
        vector<Element> m_data;
        /// @brief Offset
        int2 m_offset;
        /// @brief Cross offset
        int2 m_cross;
    };
}