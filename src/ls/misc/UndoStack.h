/** @file ls/misc/UndoStack.h
 *  @brief Undo/redo stack template
 */
#pragma once
#include "ls/common.h"

namespace ls
{
    /** @addtogroup LsMisc
     *  @{
     */
    /** Undo/redo stack description
     *  @tparam State State/Action class.
     *    Should be default-constructible and copy-able.
     */
    template <class State>
    class UndoStack
    {
    public:
        /// @brief Ctor
        UndoStack()
            : m_position(1)
        {
            m_states.emplace_back();
        }
        /** @name Actions
         *  @param foo
         *    Functor to perform action.
         *    @code void foo(S prev_state, S cur_state) @endcode
         *  @param factory
         *    Functor to create new state based on previous.
         *    @code S factory(S prev_state) @endcode
         *  @param saver
         *    Functor to finalzie state; should return number of states to pop.
         *    @code u32 saver(S state, deque<S> stack) @endcode
         *  @param activator
         *    Functor to activate selected state.
         *    State is last if it is nothing to redo anymore.
         *    State is first if it is first (during @a redo) or last (during @a undo) in queue.
         *    @code
         *    void activate(S state_to_activate, S prev_state,
         *      bool direction = true, bool last, bool first)
         *    @endcode
         *  @param checker
         *    Functor to check state if it is can be undone.
         *    @code bool can_undo(S state_to_activate) @endcode
         *  @param eraser
         *    Functor to clear state.
         *    @code void erase(S state) @endcode
         *  @param num
         *    Number of states to redo/undo.
         *  @return  @c true on success, @a false if operation cannot be performed
         *  @{
         */
        /// @brief Act
        template <class F>
        inline void act(F foo)
        {
            foo(m_states.back(), m_lastState);
        }
        /// @brief Store current action in stack
        template <class F, class G>
        inline void step(F factory, G saver)
        {
            State newState = factory(m_lastState);
            uint toPop = saver(m_lastState, m_states);
            debug_assert(toPop <= m_states.size());
            // Pop
            m_states.erase(m_states.begin(), m_states.begin() + toPop);
            // Save
            m_states.push_back(m_lastState);
            // Reset
            m_position = m_states.size();
            m_lastState = newState;
        }
        /// @brief Undo last state.
        template <class F, class G>
        inline bool undo(F activator, G checker)
        {
            if (!m_position || !checker(at(m_position)))
                return false;
            // Undo
            --m_position;
            activator(at(m_position), at(m_position + 1), true, m_position == 0, m_position + 1 == m_states.size());
            return true;
        }
        /// @brief Undo a number of actions
        template <class F, class G>
        inline bool undo(F activator, G checker, uint num)
        {
            for (uint i = 0; i < num; ++i)
                if (!undo(activator, checker))
                    return false;
            return true;
        }
        /// @brief Redo last undone action
        template <class F, class G>
        inline bool redo(F activator, G checker)
        {
            if (m_position == m_states.size() || !checker(at(m_position)))
                return false;
            // Undo
            ++m_position;
            activator(at(m_position), at(m_position - 1), false, m_position == m_states.size(), m_position == 1);
            return true;
        }
        /// @brief Redo a number of undone actions
        template <class F, class G>
        inline bool redo(F activator, G checker, uint num)
        {
            for (uint i = 0; i < num; ++i)
                if (!redo(activator, checker))
                    return false;
            return true;
        }
        /// @brief Remove all actions after selected
        template <class F, class G>
        inline void chop(F factory, G eraser)
        {
            auto iterEnd = m_states.begin() + m_position;
            if (iterEnd == m_states.end())
                return;
            // Erase
            eraser(m_lastState);
            std::for_each(++iterEnd, m_states.end(), eraser);
            // Copy
            State newState = factory(m_states[m_position]);
            m_lastState = newState;
            m_states.erase(m_states.begin() + m_position + 1, m_states.end());
            debug_assert(m_states.size() > 0);
            m_position = m_states.size();
        }
        /// @}

        /** @name Gets
         *  @param index
         *    Index
         *  @{
         */
        /// @brief Get mutable
        State& at(uint index)
        {
            if (index == m_states.size())
                return m_lastState;
            else
                return m_states[index];
        }
        /// @brief Get constant
        const State& at(uint index) const
        {
            if (index == m_states.size())
                return m_lastState;
            else
                return m_states[index];
        }
        /// @brief Get last mutable state
        State& last()
        {
            return m_lastState;
        }
        /// @brief Get last constant state
        const State& last() const
        {
            return m_lastState;
        }
        /// @brief Get current mutable state
        State& current()
        {
            return (m_position < m_states.size()) ? m_states[m_position] : m_lastState;
        }
        /// @brief Get current constant state
        const State& current() const
        {
            return (m_position < m_states.size()) ? m_states[m_position] : m_lastState;
        }
        /// @brief Check if current state is on the stack top
        bool isActual() const
        {
            return m_states.size() == m_position;
        }
        /// @}
    public:
        /// @brief Current state
        State m_lastState;
        /// @brief States pool
        deque<State> m_states;
        /// @brief Undo/redo position state
        uint m_position;
    };
    /// @}
}
