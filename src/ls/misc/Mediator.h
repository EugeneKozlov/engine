/** @file ls/misc/Mediator.h
 *  @brief Data median computer
 */
#pragma once
#include "ls/common.h"

namespace ls
{
    /** Data median computer.
     *  Value should be comparable and subtractible.
     *  @tparam Value Value type
     */
    template<class Value, class LessComparer = std::less<Value>>
    class Mediator
    {
    public:
        /// @brief Object type
        typedef Value ValueType;
        /// @brief Invalid
        Mediator() = default;
        /** Ctor.
         *  @param numElements
         *    Number of elements user for determine median
         *  @param numAverage
         *    Max number of average summed values
         *  @param startValue
         *    Initial value
         */
        Mediator(uint numElements, uint numAverage,
                 const ValueType& startValue = ValueType())
            : m_limit(numElements)
            , m_numAverage(numAverage)
            , m_median(startValue)
        {
        }
        /// @brief Add new @a object.
        void add(const ValueType& object) noexcept
        {
            m_nonSorted.push_back(object);
            while(m_nonSorted.size() > m_limit)
                m_nonSorted.pop_front();

            m_sorted = m_nonSorted;
            sort(m_sorted.begin(), m_sorted.end(), LessComparer());

            auto last = m_sorted.back();
            for (auto& iterValue : m_sorted)
                iterValue -= last;
            sort(m_sorted.begin(), m_sorted.end(), LessComparer());
            for (auto& iterValue : m_sorted)
                iterValue += last;

            uint center = m_sorted.size() / 2;
            uint average
                = (m_sorted.size() >= 3)
                ? min<uint>(m_numAverage, (m_sorted.size() - 1) / 2)
                : 0;

            m_median = ValueType();
            for (uint i = center - average; i <= center + average; ++i)
                m_median += m_sorted[i];
            m_median /= ValueType(average * 2 + 1);
        }
        /// @brief Balance array (set median to zero).
        void balance() noexcept
        {
            for (auto& iterValue : m_nonSorted)
                iterValue -= m_median;
            for (auto& iterValue : m_sorted)
                iterValue -= m_median;
            m_median = ValueType();
        }
        /// @brief Get median
        const ValueType& median() const noexcept
        {
            return m_median;
        }
        /// @brief Get number of contained elements
        uint size() const noexcept
        {
            return m_nonSorted.size();
        }
        /// @brief Get limit
        uint limit() const noexcept
        {
            return m_limit;
        }
    protected:
        /// @brief Max number of stored objects
        uint m_limit = 0;
        /// @brief Max number of average summed values
        uint m_numAverage = 0;
        /// @brief Non-sorted objects
        deque<ValueType> m_nonSorted;
        /// @brief Sorted objects
        deque<ValueType> m_sorted;
        /// @brief Median
        ValueType m_median = ValueType();
    };
}