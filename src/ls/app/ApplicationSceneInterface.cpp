#include "pch.h"
#include "ls/app/ApplicationSceneInterface.h"

using namespace ls;
ApplicationSceneInterface::ApplicationSceneInterface(ApplicationInterface* application)
    : app(application)
{
}
ApplicationSceneInterface::~ApplicationSceneInterface()
{
}
void ApplicationSceneInterface::enterSymbol(uint /*symbol*/)
{
}
void ApplicationSceneInterface::mouseMove(double2 const& /*cursor*/)
{
}
void ApplicationSceneInterface::mouseButtonDown(double2 const& /*cursor*/,
                                                MouseButton /*button*/)
{
}
void ApplicationSceneInterface::mouseButtonUp(double2 const& /*cursor*/,
                                              MouseButton /*button*/)
{
}
void ApplicationSceneInterface::mouseWheel(int /*delta*/)
{
}
