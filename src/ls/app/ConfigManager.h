/// @file ls/app/ConfigManager.h
/// @brief Application config manager
#pragma once

#include "ls/common.h"
#include "ls/core/INIReader.h"

namespace ls
{
    /// @addtogroup LsApp
    /// @{

    /// @brief Config Property
    template <class T>
    class ConfigProperty
    {
    public:
        /// @brief This type
        using Type = T;
        /// @brief Default property
        ConfigProperty() = default;
        /// @brief Construct by value
        ConfigProperty(const T& value)
            : m_name("")
            , m_value(value)
        {
        }
        /// @brief Load property by name
        ConfigProperty(const char* name, const char* alias = "")
            : m_name(name)
            , m_alias(alias)
        {
        }
        /// @brief Load property by name
        ConfigProperty(const string& name, const string& alias = "")
            : m_name(name)
            , m_alias(alias)
        {
        }
        /// @brief Copy another
        ConfigProperty(const ConfigProperty& another) = default;
        /// @brief Get name
        const string& name() const noexcept
        {
            return m_name;
        }
        /// @brief Get alias
        const string& alias() const noexcept
        {
            return m_alias;
        }
        /// @brief Get value
        const Type& operator *() const
        {
            return m_value;
        }
        /// @brief Set @a value
        void set(const Type& value)
        {
            m_value = value;
        }
    private:
        /// @brief Name
        string m_name;
        /// @brief Alias
        string m_alias;
        /// @brief Value
        Type m_value;
    };
    /// @brief Config Prototype
    struct ConfigPrototype
        : public OperableConcept
    {
        /// @brief Compute all variables
        void compute()
        {
        }
    };
    /// @brief Config Manager
    ///
    /// Provides access for each config.
    /// @warning Never call @a load simultaneously even in different threads
    /// @tparam Configs List of all configs
    template<class ... Configs>
    class ConfigManager
        : Noncopyable
    {
    public:
        /// @brief List of configs
        typedef std::tuple<Configs...> ConfigsList;
        /// @brief Setter operator
        template <class Config>
        struct SetterOperator
        {
            /// @brief Config
            Config* config = nullptr;
            /// @brief Property name
            const string* property = nullptr;
            /// @brief New value
            const string* value = nullptr;
            /// @brief Number of found properties
            uint found = 0;
            /// @brief Do cocaine!
            template <class Type>
            void operator ()(ConfigProperty<Type>& object)
            {
                if (object.name() == *property 
                    || object.alias() == *property)
                {
                    ++found;
                    object.set(fromString<Type>(*value));
                }
            }
        };
        /// @brief Property setter
        struct Setter
        {
            /// @brief Property name
            const string* property = nullptr;
            /// @brief New value
            const string* value = nullptr;
            /// @brief Number of found properties
            uint found = 0;
            /// @brief Do cocaine
            template <class Config>
            void operator ()(Config& config)
            {
                SetterOperator<Config> op;
                op.config = &config;
                op.property = property;
                op.value = value;
                config.operate(ref(op));
                if (op.found > 0)
                    config.compute();
                found += op.found;
            }
        };
        /// @brief Loader operator
        template <class Config>
        struct LoaderOperator
        {
            /// @brief Documents list
            INIReader* document = nullptr;
            /// @brief Do cocaine!
            template <class Type>
            void operator ()(ConfigProperty<Type>& object)
            {
                static const string invalid = "\6\6\6";

                // Find section
                const string& name = object.name();
                size_t split = name.find('|');
                string section = "default";
                string key = name;
                if (split != string::npos)
                {
                    section = name.substr(0, split);
                    key = name.substr(split + 1);
                }

                // Load
                string value = document->get(section, key, invalid);
                LS_THROW(value != invalid, 
                         ArgumentException,
                         "Node not found: ", section, "|", key);

                // Init
                object.set(fromString<Type>(value));
            }
        };
        /// @brief Property loader
        struct Loader
        {
            /// @brief Documents list
            INIReader* document = nullptr;
            /// @brief Do cocaine
            template <class Config>
            void operator ()(Config& config)
            {
                LoaderOperator<Config> op;
                op.document = document;
                config.operate(ref(op));
                config.compute();
            }
        };
    protected:
        /// @brief Ctor
        ConfigManager()
        {
        }
        /// @brief Dtor
        ~ConfigManager()
        {
        }
    public:
        /// @brief Load config
        void load(const string& text)
        {
            INIReader reader;
            reader.parse(text);
            Loader loader;
            loader.document = &reader;
            for_each(m_configs, ref(loader));
        }
        /// @brief Get config
        /// @tparam Config Config type
        /// @tparam Index Config type index
        template<class ConfigType, int Index>
        ConfigType& get()
        {
            return std::get<Index>(m_configs);
        }
        /// @brief Get const config
        /// @tparam Config Config type
        /// @tparam Index Config type index
        template<class ConfigType, int Index>
        const ConfigType& get() const
        {
            return std::get<Index>(m_configs);
        }
        /// @brief Set config variable
        /// @param property
        ///   Variable name
        /// @param value
        ///   New value
        /// @return @c true on success
        bool set(const string& property, const string& value)
        {
            Setter setter;
            setter.property = &property;
            setter.value = &value;
            for_each(m_configs, ref(setter));
            if (setter.found > 1)
            {
                logWarning("Config value [", property, "] "
                           "is changed to [", value, "] ", setter.found, "times");
            }
            return setter.found > 0;
        }
    private:
        vector<boost::property_tree::ptree> m_documents;

        ConfigsList m_configs;
    };
    /// @}
}
