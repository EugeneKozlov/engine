#include "pch.h"
#include "ls/app/GlfwApplication.h"

#if LS_COMPILE_DX11 != 0
#include "ls/gapi/dx11/Dx11Window.h"
#endif

#if LS_COMPILE_OPENGL != 0
#include "ls/gapi/opengl/OpenGLWindow.h"
#endif

using namespace ls;
GlfwApplication::GlfwApplication()
{
    glfwSetErrorCallback(onError);
    bool success = !!glfwInit();
    LS_THROW(success, ApplicationFailException, "Cannot init GLFW system");
}
GlfwApplication::~GlfwApplication()
{
    m_scene.reset();
    destroyWindow();
    glfwTerminate();
}
void GlfwApplication::createWindow(const string& title, uint width, uint height,
                                   bool borderless, Gapi gapi)
{
    debug_assert(!m_runnedFlag);
    destroyWindow();

    // Glfw
    glfwWindowHint(GLFW_DECORATED, !borderless);
    glfwWindowHint(GLFW_DEPTH_BITS, 24);
    GLFWmonitor* selectedMonitor = nullptr;// glfwGetPrimaryMonitor();
    GLFWwindow* newWindow = glfwCreateWindow(width, height, title.c_str(),
                                             selectedMonitor, nullptr);
    LS_THROW(newWindow, ApplicationFailException, "Cannot create GLFW window");
    glfwSetWindowUserPointer(newWindow, this);

    // Construct
    WindowInterface* abstractWindow = nullptr;
    switch (gapi)
    {
#if LS_COMPILE_DX11 != 0
    case Gapi::Direct3D11:
        abstractWindow = new gapi::Dx11Window(newWindow, width, height);
        break;
#endif
#if LS_COMPILE_OPENGL != 0
    case Gapi::OpenGL:
        glfwMakeContextCurrent(newWindow);
        abstractWindow = new gapi::OpenGLWindow(newWindow, width, height);
        break;
#endif
    case Gapi::Any:
#if LS_COMPILE_DX11 != 0
        abstractWindow = new gapi::Dx11Window(newWindow, width, height);
#elif LS_COMPILE_OPENGL != 0
        glfwMakeContextCurrent(newWindow);
        abstractWindow = new gapi::OpenGLWindow(newWindow, width, height);
#endif
        break;
    default:
        break;
    }

    // Test
    if (!abstractWindow)
        glfwDestroyWindow(newWindow);
    LS_THROW(abstractWindow,
             ApplicationFailException,
             "Cannot init window : Gapi = ", (int) gapi);

    // Init
    glfwSetKeyCallback(newWindow, onKey);
    glfwSetCharCallback(newWindow, onChar);
    glfwSetMouseButtonCallback(newWindow, onMouseClick);
    glfwSetCursorPosCallback(newWindow, onMouseMove);
    glfwSetWindowSizeCallback(newWindow, onResize);
    glfwSetCursorPos(newWindow, width / 2.0, height / 2.0);
    m_width = width;
    m_height = height;
    m_glfwWindow = newWindow;
    m_mousePosition = double2(width / 2.0, height / 2.0);
    m_window = shared_ptr<WindowInterface>(abstractWindow);
}
void GlfwApplication::destroyWindow()
{
    debug_assert(!m_runnedFlag);

    if (m_window)
        m_window.reset();
    if (m_glfwWindow)
        glfwDestroyWindow(m_glfwWindow);
    m_glfwWindow = nullptr;
}
int GlfwApplication::run()
{
    m_runnedFlag = true;

    Renderer& renderer = m_window->renderer();
    uint prevMsec = relativeTime();
    while (!glfwWindowShouldClose(m_glfwWindow))
    {
        glGetError();
        uint currentMsec = relativeTime();
        uint deltaMsec = currentMsec - prevMsec;
        prevMsec = currentMsec;

        glfwPollEvents();
        m_scene->idle(deltaMsec * 0.001f, deltaMsec,
                      static_cast<float2>(m_mouseOffset));
        m_mouseOffset = double2();

        m_window->swap();
    }

    m_runnedFlag = false;
    return 0;
}
void GlfwApplication::install(ApplicationSceneInterface* scene)
{
    debug_assert(scene);

    m_scene.reset(scene);

    // Resize
    int width, height;
    glfwGetWindowSize(m_glfwWindow, &width, &height);
    m_scene->resize((uint) width, (uint) height);
}
void GlfwApplication::showCursor(bool show)
{
    glfwSetInputMode(m_glfwWindow,
                     GLFW_CURSOR,
                     show ? GLFW_CURSOR_NORMAL : GLFW_CURSOR_HIDDEN);
}
void GlfwApplication::lockCursor(bool lock)
{
    if (m_isLocked == lock)
        return;

    if (lock)
    {
        m_mouseLockedPosition = m_mousePosition;
        m_isLocked = true;

        // Center
        m_mousePosition = double2(m_width / 2.0, m_height / 2.0);
        glfwSetCursorPos(m_glfwWindow, m_mousePosition.x, m_mousePosition.y);
    }
    else
    {
        m_isLocked = false;
        m_mousePosition = m_mouseLockedPosition;

        // Un-center
        glfwSetCursorPos(m_glfwWindow, m_mousePosition.x, m_mousePosition.y);
    }
}
void GlfwApplication::moveCursor(const double2& position)
{
    m_mouseSet = true;
    glfwSetCursorPos(m_glfwWindow, position.x, position.y);
}
void GlfwApplication::stop()
{
    glfwSetWindowShouldClose(m_glfwWindow, true);
}


// Gets
Renderer* GlfwApplication::renderer()
{
    if (!m_window)
        return nullptr;
    else
        return &m_window->renderer();
}
bool GlfwApplication::isLocked() const
{
    return m_isLocked;
}


// Main
GlfwApplication& GlfwApplication::uncast(GLFWwindow* window)
{
    void* pointer = glfwGetWindowUserPointer(window);
    debug_assert(pointer);
    return *static_cast<GlfwApplication*>(pointer);
}
void GlfwApplication::onError(int error, const char* description)
{
    logError("GLFW application error: [", error, "]");
    logError(description);
}


// Resize
void GlfwApplication::onResize(GLFWwindow* window, int width, int height)
{
    debug_assert(width >= 0 && height >= 0);

    uncast(window).resizeMe((uint) width, (uint) height);
}
void GlfwApplication::resizeMe(uint width, uint height)
{
    m_width = width;
    m_height = height;
    m_window->resize(width, height);
    m_scene->resize(width, height);
}


// Keyboard
KeyCode GlfwApplication::mapKeyCode(int key)
{
    switch (key)
    {
    case GLFW_KEY_SPACE: return Key::Space;

    case GLFW_KEY_ESCAPE: return Key::Escape;
    case GLFW_KEY_ENTER: return Key::Enter;
    case GLFW_KEY_TAB: return Key::Tab;
    case GLFW_KEY_BACKSPACE: return Key::Backspace;
    case GLFW_KEY_INSERT: return Key::Insert;
    case GLFW_KEY_DELETE: return Key::Delete;

    case GLFW_KEY_RIGHT: return Key::Right;
    case GLFW_KEY_LEFT: return Key::Left;
    case GLFW_KEY_DOWN: return Key::Down;
    case GLFW_KEY_UP: return Key::Up;

    case GLFW_KEY_PAGE_UP: return Key::PageUp;
    case GLFW_KEY_PAGE_DOWN: return Key::PageDown;
    case GLFW_KEY_HOME: return Key::Home;
    case GLFW_KEY_END: return Key::End;

    case GLFW_KEY_CAPS_LOCK: return Key::CapsLock;
    case GLFW_KEY_SCROLL_LOCK: return Key::ScrollLock;
    case GLFW_KEY_NUM_LOCK: return Key::NumLock;

    case GLFW_KEY_PRINT_SCREEN: return Key::PrintScreen;
    case GLFW_KEY_PAUSE: return Key::Pause;

    case GLFW_KEY_F1: return Key::F1;
    case GLFW_KEY_F2: return Key::F2;
    case GLFW_KEY_F3: return Key::F3;
    case GLFW_KEY_F4: return Key::F4;
    case GLFW_KEY_F5: return Key::F5;
    case GLFW_KEY_F6: return Key::F6;
    case GLFW_KEY_F7: return Key::F7;
    case GLFW_KEY_F8: return Key::F8;
    case GLFW_KEY_F9: return Key::F9;
    case GLFW_KEY_F10: return Key::F10;
    case GLFW_KEY_F11: return Key::F11;
    case GLFW_KEY_F12: return Key::F12;

    case GLFW_KEY_KP_0: return Key::Keypad0;
    case GLFW_KEY_KP_1: return Key::Keypad1;
    case GLFW_KEY_KP_2: return Key::Keypad2;
    case GLFW_KEY_KP_3: return Key::Keypad3;
    case GLFW_KEY_KP_4: return Key::Keypad4;
    case GLFW_KEY_KP_5: return Key::Keypad5;
    case GLFW_KEY_KP_6: return Key::Keypad6;
    case GLFW_KEY_KP_7: return Key::Keypad7;
    case GLFW_KEY_KP_8: return Key::Keypad8;
    case GLFW_KEY_KP_9: return Key::Keypad9;

    case GLFW_KEY_KP_DECIMAL: return Key::KeypadDecimal;
    case GLFW_KEY_KP_DIVIDE: return Key::KeypadDivide;
    case GLFW_KEY_KP_MULTIPLY: return Key::KeypadMultiply;
    case GLFW_KEY_KP_SUBTRACT: return Key::KeypadSubstract;
    case GLFW_KEY_KP_ADD: return Key::KeypadAdd;
    case GLFW_KEY_KP_ENTER: return Key::KeypadEnter;
    case GLFW_KEY_KP_EQUAL: return Key::KeypadEqual;

    case GLFW_KEY_LEFT_SHIFT: return Key::Shift;
    case GLFW_KEY_RIGHT_SHIFT: return Key::ShiftRight;
    case GLFW_KEY_LEFT_CONTROL: return Key::Ctrl;
    case GLFW_KEY_RIGHT_CONTROL: return Key::CtrlRight;
    case GLFW_KEY_LEFT_ALT: return Key::Alt;
    case GLFW_KEY_RIGHT_ALT: return Key::AltRight;
    case GLFW_KEY_LEFT_SUPER: return Key::Super;
    case GLFW_KEY_RIGHT_SUPER: return Key::SuperRight;

    case GLFW_KEY_GRAVE_ACCENT: return Key::Tilde;

    default:
        if (key < 128)
            return (KeyCode) key;
        else
            return Key::Unknown;
    }
}
void GlfwApplication::onKey(GLFWwindow* window, int key,
                            int scancode, int action, int mods)
{
    GlfwApplication& self = uncast(window);
    KeyCode code = mapKeyCode(key);
    if (code == Key::Unknown)
        return;
    if (action == GLFW_PRESS || action == GLFW_REPEAT)
    {
        if (self.m_scene)
            self.m_scene->keyDown(code);
        self.m_keyMap[code] = true;
    }
    else
    {
        if (self.m_scene)
            self.m_scene->keyUp(code);
        self.m_keyMap[code] = false;
    }
}
void GlfwApplication::onChar(GLFWwindow* window, uint symbol)
{
    GlfwApplication& self = uncast(window);
    self.m_scene->enterSymbol(symbol);
}
void GlfwApplication::onMouseMove(GLFWwindow* window, double xpos, double ypos)
{
    uncast(window).mouseMoveMe(xpos, ypos);
}
void GlfwApplication::mouseMoveMe(double xpos, double ypos)
{
    // Update offset
    double2 center = double2(m_width / 2.0, m_height / 2.0);
    m_mouseOffset += double2(xpos, ypos) - m_mousePosition;
    m_mousePosition = double2(xpos, ypos);

    // Reset center
    if (m_isLocked)
    {
        m_mousePosition = center;
        glfwSetCursorPos(m_glfwWindow, center.x, center.y);
    }
    else
    {
        if (m_scene)
            m_scene->mouseMove(m_mousePosition);
    }
}
MouseButton GlfwApplication::mapMouseButton(int button)
{
    switch (button)
    {
    case GLFW_MOUSE_BUTTON_LEFT:
        return MouseButton::Left;
    case GLFW_MOUSE_BUTTON_RIGHT:
        return MouseButton::Right;
    case GLFW_MOUSE_BUTTON_MIDDLE:
        return MouseButton::Middle;
    case GLFW_MOUSE_BUTTON_4:
        return MouseButton::Button1;
    case GLFW_MOUSE_BUTTON_5:
        return MouseButton::Button2;
    default:
        return MouseButton::Unknown;
    }
    return MouseButton::Unknown;
}
void GlfwApplication::onMouseClick(GLFWwindow* window, int button,
                                   int action, int mods)
{
    MouseButton code = mapMouseButton(button);
    switch (action)
    {
    case GLFW_PRESS:
        uncast(window).mouseDownMe(code);
        break;
    case GLFW_RELEASE:
        uncast(window).mouseUpMe(code);
        break;
    default:
        break;
    }
}
void GlfwApplication::mouseDownMe(MouseButton button)
{
    m_mouseMap[(uint) button] = true;
    m_scene->mouseButtonDown(m_mousePosition, button);
}
void GlfwApplication::mouseUpMe(MouseButton button)
{
    m_mouseMap[(uint) button] = false;
    m_scene->mouseButtonUp(m_mousePosition, button);
}
