/// @file ls/app/ApplicationInterface.h
/// @brief Application interface
#pragma once

#include "ls/common.h"
#include "ls/app/common.h"
#include "ls/gapi/WindowInterface.h"

namespace ls
{
    class ApplicationSceneInterface;

    /// @addtogroup LsApp
    /// @{

    /// @brief Abstract application interface.
    /// @throw ApplicationFailException
    class ApplicationInterface
        : Noncopyable
    {
    protected:
        /// @brief Ctor
        ApplicationInterface();
    public:
        /// @brief Create window.
        /// @brief Destroy previous window.
        /// @param title
        ///   Window title
        /// @param width,height
        ///   Window size
        /// @param borderless
        ///   Set to create border-less window
        /// @param gapi
        ///   Used GAPI
        /// @param hasLogging
        ///   Set to enable full renderer logging
        virtual void createWindow(const string& title, uint width, uint height,
                                  bool borderless, Gapi gapi) = 0;
        /// @brief Destroy window.
        /// @brief Do nothing if no window is created.
        virtual void destroyWindow() = 0;
        /// @brief Run.
        /// @brief Call this function only once
        virtual int run() = 0;

        /// @brief Install scene
        virtual void install(ApplicationSceneInterface* scene) = 0;
        /// @brief Stop application
        virtual void stop() = 0;
        /// @brief Set cursor position
        virtual void moveCursor(const double2& position) = 0;
        /// @brief Hide/show cursor
        virtual void showCursor(bool show) = 0;
        /// @brief Lock/unlock cursor
        virtual void lockCursor(bool lock) = 0;
        /// @brief Get renderer
        virtual Renderer* renderer() = 0;
        /// @brief Get root file system
        inline FileSystem root()
        {
            return m_root;
        }
        /// @brief Test if cursor is locked
        virtual bool isLocked() const = 0;
        /// @brief Test if key is pressed
        bool isPressed(KeyCode key) const noexcept
        {
            return m_keyMap[key];
        }
        /// @brief Test if mouse button is pressed
        bool isPressed(MouseButton button) const noexcept
        {
            return m_mouseMap[(uint) button];
        }
        /// @brief Get window width
        uint width() const noexcept
        {
            return m_width;
        }
        /// @brief Get window height
        uint height() const noexcept
        {
            return m_height;
        }
    protected:
        /// @brief Root folder
        FileSystem m_root;
        /// @brief Loggers
        list<unique_ptr<LoggerInterface> > m_loggers;
        /// @brief Current scene
        shared_ptr<ApplicationSceneInterface> m_scene;
        /// @brief Key map
        MultiArray<bool, Key::COUNT> m_keyMap;
        /// @brief Mouse map
        MultiArray<bool, (uint) MouseButton::COUNT> m_mouseMap;

        /// @brief Width
        uint m_width = 0;
        /// @brief Height
        uint m_height = 0;
    };
    /// @}
}
