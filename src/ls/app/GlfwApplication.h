/// @file ls/app/GlfwApplication.h
/// @brief GLFW Application
#pragma once

#include "ls/common.h"
#include "ls/app/ApplicationInterface.h"
#include "ls/app/ApplicationSceneInterface.h"
#include <gl/glew.h>
#include <glfw/glfw3.h>
#include <glfw/glfw3native.h>

namespace ls
{
    /// @addtogroup LsApp
    /// @{

    /// @brief GLFW Application Impl
    class GlfwApplication
        : public ApplicationInterface
    {
    public:
        /// @brief Ctor
        GlfwApplication();
        /// @brief Dtor
        ~GlfwApplication();
    public:
        virtual void createWindow(const string& title, uint width, uint height,
                                  bool borderless, Gapi gapi) override;
        virtual void destroyWindow();
        virtual int run() override;
    public:
        virtual void install(ApplicationSceneInterface* scene) override;
        virtual void showCursor(bool show) override;
        virtual void lockCursor(bool lock) override;
        virtual void moveCursor(const double2& position) override;
        virtual void stop() override;
        virtual Renderer* renderer() override;
        virtual bool isLocked() const;
    private:
        static KeyCode mapKeyCode(int key);
        static MouseButton mapMouseButton(int button);
        static GlfwApplication& uncast(GLFWwindow* window);
        static void onError(int error, const char* description);
        static void onResize(GLFWwindow* window, int width, int height);
        static void onKey(GLFWwindow* window, int key, int scancode, int action, int mods);
        static void onChar(GLFWwindow* window, uint symbol);
        static void onMouseMove(GLFWwindow* window, double xpos, double ypos);
        static void onMouseClick(GLFWwindow* window, int button, int action, int mods);
    private:
        void resizeMe(uint width, uint height);
        void mouseMoveMe(double xpos, double ypos);
        void mouseDownMe(MouseButton button);
        void mouseUpMe(MouseButton button);
    private:
        shared_ptr<WindowInterface> m_window;
        GLFWwindow* m_glfwWindow = nullptr;

        bool m_runnedFlag = false;
        bool m_mouseSet = false;

        double2 m_mousePosition;
        double2 m_mouseOffset;

        double2 m_mouseLockedPosition;
        bool m_isLocked = false;
    };
    /// @}
}

