#include "pch.h"
#include "ls/app/ApplicationInterface.h"
#include "ls/io/FileSystemInterface.h"

using namespace ls;
ApplicationInterface::ApplicationInterface()
    : m_keyMap(false)
    , m_mouseMap(false)
{
    const uint maxDepth = 5;
    string rootMarker = "root.txt";
    string rootFolder = "";

    FILE* rootTag = fopen(rootMarker.c_str(), "rt");
    uint depth = 0;
    for (; depth < maxDepth && !rootTag; ++depth)
    {
        rootMarker = "../" + rootMarker;
        rootFolder = "../" + rootFolder;
        rootTag = fopen(rootMarker.c_str(), "rt");
    }
    if (rootTag)
    {
        fclose(rootTag);
    }
    else
    {
        printf("Cannot locate root folder!\n Press any key to quit");
        getchar();
        LS_THROW(0, ApplicationFailException, "Cannot locate root folder");
    }

    m_root = FileSystemInterface::open(rootFolder + ".");
    string logName = rootFolder + LS_DEFAULT_LOG_NAME;

    m_loggers.push_back(make_unique<FileLogger>(logName));
    m_loggers.push_back(make_unique<ConsoleLogger>(LogMessageLevel::Debug));
    m_loggers.push_back(make_unique<DebugOutputLogger>(LogMessageLevel::Debug));
}

