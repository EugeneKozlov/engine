/// @file ls/app/ApplicationSceneInterface.h
/// @brief Application scene interface
#pragma once

#include "ls/common.h"
#include "ls/app/common.h"

namespace ls
{
    class ApplicationInterface;

    /// @addtogroup LsApp
    /// @{

    /// @brief Application Scene Interface
    class ApplicationSceneInterface
    {
    public:
        /// @brief Ctor
        /// @param application
        ///   Application which must transmit to scene environment info.
        ApplicationSceneInterface(ApplicationInterface* application);
        /// @brief Dtor
        virtual ~ApplicationSceneInterface();
        /// @brief Main loop update
        /// @param deltaSeconds
        ///   Delta time in seconds
        /// @param deltaMsec
        ///   Delta time in milliseconds
        /// @param mouseDelta
        ///   Delta mouse cursor in pixels
        virtual void idle(float deltaSeconds, uint deltaMsec, const float2& mouseDelta) = 0;
        /// @brief Resize screen
        virtual void resize(uint width, uint height) = 0;
        /// @brief <b>Key down</b> message
        /// @param key
        ///   Key code
        virtual void keyDown(KeyCode key) = 0;
        /// @brief <b>Key up</b> message
        /// @param key
        ///   Key code
        virtual void keyUp(KeyCode key) = 0;
        /// @brief <b>Text symbol print</b> message.
        /// @param symbol
        ///   Symbol code
        virtual void enterSymbol(uint symbol);
        /// @brief <b>Mouse move</b> message
        /// @param cursor
        ///   Cursor position
        virtual void mouseMove(const double2& cursor);
        /// @brief <b>Mouse down</b> message
        /// @param cursor
        ///   Cursor position
        /// @param button
        ///   Mouse button code
        virtual void mouseButtonDown(const double2& cursor,
                                     MouseButton button);
        /// @brief <b>Mouse up</b> message
        /// @param cursor
        ///   Cursor position
        /// @param button
        ///   Mouse button code
        virtual void mouseButtonUp(const double2& cursor,
                                   MouseButton button);
        /// @brief <b>Mouse wheel scroll</b> message
        /// @param delta
        ///   Wheel delta
        virtual void mouseWheel(int delta);
    protected:
        /// @brief Application
        ApplicationInterface* app;
    };
    /// @}

}