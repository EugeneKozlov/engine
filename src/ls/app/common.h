/// @file ls/app/common.h
/// @brief Keyboard keys and mouse button codes
#pragma once

namespace ls
{
    /// @addtogroup LsApp
    /// @{

    /// @brief Keyboard Key
    struct Key
    {
        /// @brief Enum
        enum Enum : uint
        {
            /// @brief Unknown key. Mustn't used.
            Unknown = static_cast<uint>(-1),
            /// @brief Space
            Space = 32,
            /// @brief Tilde or 'Yo'
            Tilde = static_cast<unsigned char>('~'),
            /// @brief Special code separated printed from special keys. Mustn't used.
            Special = 256,
            /// @brief Escape
            Escape,

            F1, /**<F1 */ F2, /**<F2 */F3, /**<F3 */ F4, /**<F4 */ F5, /**<F5 */ F6, /**<F6 */
            F7, /**<F7 */ F8, /**<F8 */F9, /**<F9 */ F10, /**<F10*/ F11, /**<F11*/ F12, /**<F12*/
            F13, /**<F13*/ F14, /**<F14*/F15, /**<F15*/ F16, /**<F16*/ F17, /**<F17*/ F18, /**<F18*/
            F19, /**<F19*/ F20, /**<F20*/F21, /**<F21*/ F22, /**<F22*/ F23, /**<F23*/ F24, /**<F24*/

            /// @brief Up arrow
            Up,
            /// @brief Down arrow
            Down,
            /// @brief Left arrow
            Left,
            /// @brief Right arrow
            Right,

            /// @brief Tab
            Tab,
            /// @brief Enter
            Enter,
            /// @brief Backspace
            Backspace,
            /// @brief Insert
            Insert,
            /// @brief Delete
            Delete,

            /**Left shift   */ Shift, ShiftRight, /**<Right shift */
            /**Left ctrl    */ Ctrl, CtrlRight, /**<Right ctrl  */
            /**Left alt     */ Alt, AltRight, /**<Right alt */
            /**Left win     */ Super, SuperRight, /**<Right win */
            /**Page up      */ PageUp, PageDown, /**<Page down  */
            /**Home         */ Home, End, /**<End           */

            /// @brief Caps Lock
            CapsLock,
            /// @brief Scroll Lock
            ScrollLock,
            /// @brief Num Lock
            NumLock,
            /// @brief Print Screen
            PrintScreen,
            /// @brief Pause
            Pause,
            /// @brief Menu
            Menu,

            Keypad0, /**<NumPad 0 */ Keypad1, /**<NumPad 1 */ Keypad2, /**<NumPad 2 */ Keypad3, /**<NumPad 3 */
            Keypad4, /**<NumPad 4 */ Keypad5, /**<NumPad 5 */ Keypad6, /**<NumPad 6 */
            Keypad7, /**<NumPad 7 */ Keypad8, /**<NumPad 8 */ Keypad9, /**<NumPad 9 */
            KeypadDivide, /**<NumPad /   */ KeypadMultiply, /**<NumPad * */
            KeypadSubstract, /**<NumPad -    */ KeypadAdd, /**<NumPad + */
            KeypadDecimal, /**<NumPad .  */ KeypadEqual, /**<NumPad = */
            KeypadEnter, /**<NumPad Enter */ KeypadNumLock, /**<Num Lock */
            /// @brief Greaten than any keyboard key code
            COUNT
        }; ///< Key value
    };
    /// @brief Key code
    using KeyCode = Key::Enum;
    /// @brief Mouse Button Code
    enum class MouseButton
    {
        /// @brief Left
        Left = 0,
        /// @brief Middle
        Middle,
        /// @brief Right
        Right,
        /// @brief First special
        Button1,
        /// @brief Second special
        Button2,
        /// @brief Unknown
        Unknown,
        /// @brief Greaten than any mouse button code
        COUNT
    };
    /// @}
}