/// @file ls/io/StreamInterface.h
/// @brief Abstract stream interface and functions
#pragma once

#include "ls/common.h"
#include "ls/io/common.h"

namespace ls
{
    /// @addtogroup LsIo
    /// @{
    
    /// @brief Stream access mode
    enum class StreamAccess : uint
    {
        /// @brief Read data
        Read = 1 << 0,
        /// @brief Write data
        Write = 1 << 2,
        /// @brief New data stream (if associated with file system, it will be created if not exist)
        New = 1 << 3,
        /// @b Read and @b Write combination
        ReadWrite = StreamAccess::Read | StreamAccess::Write,
        /// @b Read, @b Write and @b New combination
        ReadWriteNew = StreamAccess::Read | StreamAccess::Write | StreamAccess::New,
        /// @b Write and @b New combination
        WriteNew = StreamAccess::Write | StreamAccess::New,
    };
    /// @brief Stream access mode flags
    using StreamAccessFlag = FlagSet<StreamAccess>;
    /// @brief Abstract data read/write interface.
    /// Use for all read/write operations.
    /// @brief Throws exception if internal error occur and fails assert if function call caused access conflict.
    /// For example, EndOfFile - writing exception, but write to read-only stream - assertion fail.
    class StreamInterface
        : Noncopyable
        , public StreamConcept
    {
    public:
        /// @brief Open new stream in memory.
        /// @return New memory stream
        static Stream open();
        /// @brief Open new stream in memory and fill it with data.
        /// @param data
        ///   Stream data
        /// @return New memory stream
        static Stream open(vector<ubyte> && data);
    public:
        /// @brief Dtor
        virtual ~StreamInterface()
        {
        }

        /// @name IO
        /// @throw IoException
        /// @{
        
        /// @brief Write @a buffer with specified @a size
        virtual void write(const void* buffer, const uint size) = 0;
        /// @brief Read @a buffer with specified @a size
        virtual void read(void* buffer, const uint size) = 0;
        /// @brief Write @a buffer with specified @a size at absolute @a offset
        virtual void writeTo(const void* buffer, const uint size, const ulong offset) = 0;
        /// @brief Read @a buffer with specified @a size at absolute @a offset
        virtual void readFrom(void* buffer, const uint size, const ulong offset) const = 0;
        /// @brief Set pointer at @a offset
        virtual bool seek(const ulong offset) = 0;
        /// @}

        /// @brief Get current cursor position
        virtual ulong tell() const = 0;
        /// @brief Get full stream size
        virtual ulong size() const = 0;
        /// @brief Check if no data to read remaining
        virtual bool eof() const = 0;
        /// @brief Get stream access mode.
        StreamAccessFlag access() const
        {
            return m_accessMode;
        }

        /// @brief Get full stream size as uint
        uint sizeUint() const
        {
            // Get length
            ulong full = size();
            uint len = static_cast<uint>(full);

            // Too long
            if (full - len != 0)
                return 0;
            
            return len;
        }
        /// @brief Get stream data as text string
        string text() const
        {
            // Get length
            uint len = sizeUint();
            if (!len)
                return "";

            // Read
            string buf(len + 1, char(0));
            readFrom(&*buf.begin(), len, 0);
            return buf;
        }
        /// @brief Get stream data as byte array
        vector<ubyte> data() const
        {
            // Get length
            uint len = sizeUint();
            if (!len)
                return vector<ubyte>();

            // Read
            vector<ubyte> buf(len, 0);
            readFrom(buf.data(), len, 0);
            return buf;
        }
    protected:
        /// @brief Ctor prototype
        StreamInterface(const StreamAccessFlag access)
            : m_accessMode(access)
        {
        }
        /// @brief Stream access
        StreamAccessFlag m_accessMode;
    };
    /// @}
}
