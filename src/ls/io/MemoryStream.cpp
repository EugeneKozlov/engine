#include "pch.h"
#include "ls/io/MemoryStream.h"

using namespace ls;
MemoryStream::MemoryStream(StreamAccessFlag access)
    : StreamInterface(access)
    , m_data()
{
}
MemoryStream::MemoryStream(vector<ubyte> && data, const StreamAccessFlag access)
    : StreamInterface(access)
    , m_data(std::move(data))
{

}
MemoryStream::~MemoryStream()
{
}
void MemoryStream::writeTo(const void* buffer, const uint size, const ulong off)
{
    debug_assert(m_accessMode.is(StreamAccess::Write));
    LS_THROW(off == ulong(uint(off)),
             IoException,
             "too many data to write");

    const ubyte* src = static_cast<const ubyte*>(buffer);
    if (off + size > m_data.size())
        m_data.insert(m_data.end(), uint(off) + size - m_data.size(), 0);
    copy(src, src + size, m_data.begin() + uint(off));
}
void MemoryStream::write(const void* buffer, const uint size)
{
    writeTo(buffer, size, m_offset);
    m_offset += size;
}
void MemoryStream::readFrom(void* buffer, const uint size, const ulong off) const
{
    debug_assert(m_accessMode.is(StreamAccess::Read));
    LS_THROW(off == ulong(uint(off)),
             IoException,
             "too many data to read");
    LS_THROW(m_data.size() - off >= size,
             IoException,
             "not enough data");

    const ubyte* src = m_data.data() + off;
    ubyte* dest = static_cast<ubyte*>(buffer);
    copy(src, src + size, dest);
}
void MemoryStream::read(void* buffer, const uint size)
{
    readFrom(buffer, size, m_offset);
    m_offset += size;
}
ulong MemoryStream::tell() const
{
    return m_offset;
}
bool MemoryStream::seek(ulong offset)
{
    if (offset >= m_data.size())
        return false;
    m_offset = offset;
    return true;
}
ulong MemoryStream::size() const
{
    return m_data.size();
}
bool MemoryStream::eof() const
{
    return m_data.size() >= m_offset;
}
Stream StreamInterface::open()
{
    return Stream(new MemoryStream(StreamAccess::ReadWrite));
}
Stream StreamInterface::open(vector<ubyte> && data)
{
    return Stream(new MemoryStream(std::move(data), StreamAccess::ReadWrite));
}
