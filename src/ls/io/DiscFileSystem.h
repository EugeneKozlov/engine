/// @file ls/io/DiscFileSystem.h
/// @brief Disc file system implementation
#pragma once

#include "ls/common.h"
#include "ls/io/FileSystemInterface.h"
#include "ls/io/FileStream.h"

namespace ls
{
    /// @addtogroup LsIo
    /// @{

    /// @brief Hard drive folder file system
    class DiscFileSystem
        : public FileSystemInterface
    {
    public:
        /// @brief Path type
        using Path = boost::filesystem::path;
        /// @brief Ctor
        DiscFileSystem(const Path path, const StreamAccessFlag access);
        virtual string decorateName(const string& name) override;
        virtual Stream openStream(const string& name,
                                  const StreamAccessFlag access) override;
        virtual FileSystem openSystem(const string& name,
                                      const string& password,
                                      const StreamAccessFlag access) override;
        virtual bool deleteItem(const string& name) override;
        virtual bool isRegularFile(const string& fileName) override;
        virtual std::time_t lastWriteTime(const string& fileName) override;
        virtual void enumerateItems(vector<string>& files) override;
        virtual bool iterateItems(function<bool(StreamInterface&)> func) override;
    protected:
        /// @brief Make full name
        Path makeFull(const Path& shortName);
    protected:
        /// @brief Path
        Path m_path;
    };
    /// @}
}
