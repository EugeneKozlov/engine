/// @file ls/io/Buffer.h
/// @brief Buffer
#pragma once

#include "ls/common.h"

namespace ls
{
    class StreamInterface;

    /// @addtogroup LsIo
    /// @{

    /// @brief Buffer
    /// 
    /// Supported types:
    /// - PODs without pointers
    /// - string
    class Buffer
        : public StreamConcept
    {
    public:
        /// @brief Default ctor
        Buffer() = default;
        /// @brief Copy from vector
        Buffer(const vector<ubyte>& data);
        /// @brief Move from vector
        Buffer(vector<ubyte> && data);

        /// @brief Pull from stream
        /// @throw IoException if @a StreamInterface::read throws
        static Buffer pull(StreamInterface& src, const uint size = 0);
        /// @brief Push to stream
        /// @throw IoException if @a StreamInterface::write throws
        void push(StreamInterface& dest);

        /// @brief Drop all data and release memory
        void release();

        /// @brief Read from stream
        /// @throw IoException if @a StreamInterface::write throws
        template <class Stream>
        static Buffer readFromStream(Stream& src)
        {
            return Buffer(src);
        }
        /// @brief Write to stream
        /// @throw IoException if @a StreamInterface::write throws
        template <class Stream>
        void writeToStream(Stream& dest) const
        {
            dest.write(&m_writtenBytes, sizeof(m_writtenBytes));
            if (m_writtenBytes > 0)
            {
                dest.write(m_data.data(), m_writtenBytes);
            }
        }

        /// @brief Read data
        /// @throw IoException if not enough data
        void read(void* buffer, const uint size);
        /// @brief Write data
        void write(const void* buffer, const uint size);
        /// @brief Read text data
        void readText(string& buffer);
        /// @brief Write text data
        void writeText(const string& buffer);
        /// @brief Seek
        /// @throw IoException if @a pos is invalid
        void seek(const uint pos);
        /// @brief Available to read
        uint avalilable() const;
        /// @brief Total size
        uint size();
        /// @brief Data pointer
        const ubyte* data() const;
    private:
        /// @brief Pull ctor
        /// @throw IoException if @a StreamInterface::read throws
        Buffer(StreamInterface& src, const uint size);
        /// @brief Read ctor
        /// @throw IoException if @a StreamInterface::read throws
        template <class Stream>
        Buffer(Stream& src)
        {
            src.read(&m_writtenBytes, sizeof(m_writtenBytes));
            m_data.resize(m_writtenBytes);
            if (m_writtenBytes > 0)
            {
                src.read(m_data.data(), m_writtenBytes);
            }
        }
    private:
        vector<ubyte> m_data;

        uint m_readBytes = 0;
        uint m_writtenBytes = 0;
    };
    /// @brief Binary IO
    template <>
    struct Binary<Buffer>
    {
        /// @brief Read binary string
        template <class Stream>
        static void read(Stream& stream, Buffer& object)
        {
            object = Buffer::readFromStream(stream);
        }
        /// @brief Write binary string
        template <class Stream>
        static void write(Stream& stream, const Buffer& object)
        {
            object.writeToStream(stream);
        }
    };

    /// @}
}