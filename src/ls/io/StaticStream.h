/// @file ls/io/StaticStream.h
/// @brief Static data stream
#pragma once

#include "ls/common.h"
#include "ls/io/DynamicStream.h"

namespace ls
{
    /// @addtogroup LsIo
    /// @{
    
    /// @brief Static data stream.
    /// Stream base can be specified.
    /// @brief Couple of data immutable by read/write ops will be stored at array's begin.
    /// @tparam S Size of stream
    template <uint S>
    class StaticStream
        : public DynamicStream
    {
    public:
        /// @brief Stream size
        static const uint Size = S;
        /// @brief Ctor.
        /// @param base
        ///   Stream IO @a base offset
        StaticStream(const uint base = 0)
            : DynamicStream(m_data.data(), Size, base)
        {
        }
        /// @brief Copy ctor
        StaticStream(const StaticStream& another)
            : DynamicStream(m_data.data(), Size, another.base())
            , m_data(another.m_data)
        {
            m_readPos = another.m_readPos;
            m_writePos = another.m_writePos;
        }
        /// @brief Assignment
        StaticStream& operator = (const StaticStream& another)
        {
            m_base = another.m_base;
            m_readPos = another.m_readPos;
            m_writePos = another.m_writePos;
            m_size = another.m_size;
            m_data = another.m_data;
            return *this;
        }
    protected:
        /// @brief Data
        array<ubyte, Size> m_data;
    };
    /// @}
}
