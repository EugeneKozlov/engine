/// @file ls/io/DynamicStream.h
/// @brief Dynamic data stream
#pragma once

#include "ls/common.h"
#include "ls/core/Blob.h"

namespace ls
{
    template <uint>
    class StaticStream;

    /// @addtogroup LsIo
    /// @{
    
    /// @brief Data stream.
    /// Couple of data immutable by read/write ops can be stored at array's begin.
    class DynamicStream
        : public SerializableConcept
        , public StreamConcept
    {
    public:
        /// @brief Read-write stream. Write position is @a base.
        /// @param data
        ///   Data buffer
        /// @param size
        ///   Size of data
        /// @param base
        ///   Stream IO @a base offset
        DynamicStream(void* data, const uint size, const uint base = 0)
            : m_size(size)
            , m_writeData(static_cast<ubyte*>(data))
            , m_readData(static_cast<const ubyte*>(data))
            , m_base(base)
            , m_writePos(base)
            , m_readPos(base)
        {
        }
        /// @brief Read-only stream. Write position is @a size.
        /// @param data
        ///   Data buffer
        /// @param size
        ///   Size of data
        /// @param base
        ///   Stream IO @a base offset
        DynamicStream(const void* data, const uint size, const uint base = 0)
            : m_size(size)
            , m_writeData(nullptr)
            , m_readData(static_cast<const ubyte*>(data))
            , m_base(base)
            , m_writePos(size)
            , m_readPos(base)
        {
        }
        /// @brief Copy ctor
        DynamicStream(const DynamicStream& another) = default;
        /// @brief Ctor from static mutable @a stream
        template<uint Size>
        DynamicStream(StaticStream<Size>& stream);
        /// @brief Ctor from static const @a stream
        template<uint Size>
        DynamicStream(const StaticStream<Size>& stream);
        /// @brief Ctor by blob
        DynamicStream(const Blob& source)
            : DynamicStream(static_cast<const void*>(source.data()), source.size())
        {
        }
        /// @brief Resize stream (moves write-pointer)
        void resize(uint size)
        {
            debug_assert(size <= m_size,
                         "newSize = ", size, "; maxSize = ", m_size);
            m_writePos = size;
        }

        /// @brief Get data pointer
        ubyte* data() noexcept
        {
            return m_writeData;
        }
        /// @brief Get data const pointer
        const ubyte* constData() const noexcept
        {
            return m_readData;
        }
        /// @brief Check if @a number of bytes can be written
        bool canWrite(const uint number) const noexcept
        {
            return m_writeData && m_writePos + number <= m_size;
        }
        /// @brief Check if @a number of bytes can be read
        bool canRead(const uint number) const noexcept
        {
            return m_readData && m_readPos + number <= m_writePos;
        }
        /// @brief Get number of written bytes
        uint numWritten() const noexcept
        {
            return m_writePos;
        }
        /// @brief Get number of read bytes
        uint numRead() const noexcept
        {
            return m_readPos;
        }
        /// @brief Get max number of bytes can be written
        uint freeSize() const noexcept
        {
            return m_size - m_writePos;
        }
        /// @brief Get max number of bytes can be read
        uint remaning() const noexcept
        {
            return m_writePos - m_readPos;
        }
        /// @brief Get base
        uint base() const noexcept
        {
            return m_base;
        }

        /// @brief Write some @a data
        void write(const void* data, const uint size)
        {
            debug_assert(canWrite(size),
                         "need = ", size, "; pos = ", m_writePos, "; size = ", m_size);
            std::memcpy(m_writeData + m_writePos, data, size);
            m_writePos += size;
        }
        /// @brief Read some @a data
        void read(void* data, const uint size)
        {
            debug_assert(canRead(size),
                         "need = ", size, "; pos = ", m_readPos, "; size = ", m_writePos);
            std::memcpy(data, m_readData + m_readPos, size);
            m_readPos += size;
        }
        /// @brief Reset input
        void resetOutput()
        {
            m_writePos = m_base;
        }
        /// @brief Reset output
        void resetInput()
        {
            m_readPos = m_base;
        }
        /// @brief Reset IO
        void reset()
        {
            resetInput();
            resetOutput();
        }

        /// @brief Serialize to @a stream
        template<class Stream>
        void serialize(Stream& stream) const
        {
            stream.write(constData(), numWritten());
        }
        /// @brief Deserialize from @a stream, this stream must be resized before
        template<class Stream>
        void deserialize(Stream& stream)
        {
            stream.read(data(), m_writePos);
        }
        /// @brief Get size
        uint size() const noexcept
        {
            return numWritten();
        }
    protected:
        /// @brief Data size
        uint m_size = 0;
        /// @brief Write data pointer
        ubyte* m_writeData = nullptr;
        /// @brief Read data pointer
        const ubyte* m_readData = nullptr;
        /// @brief Zero offset
        uint m_base = 0;
        /// @brief Write position
        uint m_writePos = 0;
        /// @brief Read position
        uint m_readPos = 0;
    };
    /// @}
}

#include "ls/io/StaticStream.h"

namespace ls
{
    template<uint Size>
    DynamicStream::DynamicStream(StaticStream<Size>& stream)
        : DynamicStream(stream.data(), Size, stream.base())
    {
        m_readPos = stream.m_readPos;
        m_writePos = stream.m_writePos;
    }
    template<uint Size>
    DynamicStream::DynamicStream(const StaticStream<Size>& stream)
        : DynamicStream(stream.constData(), Size, stream.base())
    {
        m_readPos = stream.m_readPos;
        m_writePos = stream.m_writePos;
    }
}
