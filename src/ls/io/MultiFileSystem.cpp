#include "pch.h"
#include "ls/io/MultiFileSystem.h"

using namespace ls;

MultiFileSystem::MultiFileSystem() : FileSystemInterface(StreamAccess::ReadWriteNew)
{

}
MultiFileSystem::MultiFileSystem(initializer_list<FileSystem> systems) 
    : FileSystemInterface(StreamAccess::ReadWriteNew)
    , m_systems(systems.begin(), systems.end())
{
}
void MultiFileSystem::pushBack(FileSystem fileSystem)
{
    m_systems.push_back(fileSystem);
}
void MultiFileSystem::popBack()
{
    m_systems.pop_back();
}
void MultiFileSystem::pushFront(FileSystem fileSystem)
{
    m_systems.push_front(fileSystem);
}
void MultiFileSystem::popFront()
{
    m_systems.pop_front();
}
FileSystem MultiFileSystem::openMultisystem(const string& name, const string& password, const StreamAccessFlag access)
{
    shared_ptr<MultiFileSystem> newSystem = make_shared<MultiFileSystem>();
    for (FileSystem& fileSystem : m_systems)
    {
        FileSystem fs = fileSystem->openSystem(name, password, access);
        if (fs)
        {
            newSystem->pushBack(fs);
        }
    }
    return newSystem;
}
string MultiFileSystem::decorateName(const string& name)
{
    if (m_systems.empty())
        return "";
    return m_systems.front()->decorateName(name);
}


// Manage
Stream MultiFileSystem::openStream(const string& name, const StreamAccessFlag access)
{
    for (FileSystem& fileSystem : m_systems)
    {
        Stream stm = fileSystem->openStream(name, access);
        if (stm)
            return stm;
    }
    return nullptr;
}
FileSystem MultiFileSystem::openSystem(const string& name, const string& password, const StreamAccessFlag access)
{
    for (FileSystem& fileSystem : m_systems)
    {
        FileSystem fs = fileSystem->openSystem(name, password, access);
        if (fs)
            return fs;
    }
    return nullptr;
}
bool MultiFileSystem::deleteItem(const string& name)
{
    bool success = false;
    for (FileSystem& fileSystem : m_systems)
    {
        success |= fileSystem->deleteItem(name);
    }
    return true;
}


// Gets
bool MultiFileSystem::isRegularFile(const string& fileName)
{
    for (FileSystem& fileSystem : m_systems)
    {
        if (fileSystem->isRegularFile(fileName))
            return true;
    }
    return false;
}
std::time_t MultiFileSystem::lastWriteTime(const string& fileName)
{
    static const std::time_t InvalidTime = static_cast<std::time_t>(0);
    for (FileSystem& fileSystem : m_systems)
    {
        std::time_t time = fileSystem->lastWriteTime(fileName);
        if (time != InvalidTime)
            return time;
    }
    return InvalidTime;
}
void MultiFileSystem::enumerateItems(vector<string>& files)
{
    for (FileSystem& fileSystem : m_systems)
    {
        fileSystem->enumerateItems(files);
    }
}
bool MultiFileSystem::iterateItems(function<bool(StreamInterface&)> func)
{
    for (FileSystem& fileSystem : m_systems)
    {
        if (!fileSystem->iterateItems(func))
            return false;
    }
    return true;
}
