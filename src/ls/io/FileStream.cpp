#include "pch.h"
#include "ls/io/FileStream.h"

using namespace ls;
std::ios_base::openmode FileStream::flags(const StreamAccessFlag access)
{
    std::ios_base::openmode flag = std::ios_base::binary;
    if (access.is(StreamAccess::Read))
        flag |= std::ios_base::in;
    if (access.is(StreamAccess::Write))
        flag |= std::ios_base::out;
    if (access.is(StreamAccess::New))
        flag |= std::ios_base::trunc;
    return flag;
}
FileStream::FileStream(const boost::filesystem::path& path, const StreamAccessFlag access)
    : StreamInterface(access)
    , m_path(path)
{
    // Create directory if needed
    if (access.is(StreamAccess::Write))
    {
        boost::filesystem::path dir = path.parent_path();
        if (!boost::filesystem::exists(dir))
        {
            boost::filesystem::create_directories(dir);
        }
    }

    // Open
    m_stream.open(m_path, flags(m_accessMode));
    LS_THROW(!!m_stream,
             FileSystemException,
             "Cannot open file [", m_path.string(), "]");
}
FileStream::~FileStream()
{
}
void FileStream::write(const void* buffer, const uint size)
{
    LS_THROW(m_accessMode.is(StreamAccess::Write),
             IoException,
             "Stream is not writable");
    m_stream.write(reinterpret_cast<const char*>(buffer), size);
    LS_THROW(!!m_stream,
             IoException,
             "Writing error");
}
void FileStream::writeTo(const void* buffer, const uint size, const ulong offset)
{
    LS_THROW(m_accessMode.is(StreamAccess::Write),
             IoException,
             "Stream is not writable");
    ulong pos = tell();
    LS_THROW(seek(offset),
             IoException,
             "Seeking error");
    write(buffer, size);
    LS_THROW(seek(pos),
             IoException,
             "Seeking error");
}
void FileStream::read(void* buffer, const uint size)
{
    LS_THROW(m_accessMode.is(StreamAccess::Read),
             IoException,
             "Stream is not readable");
    m_stream.read(reinterpret_cast<char*>(buffer), size);
    LS_THROW(!!m_stream,
             IoException,
             "Reading error");
}
void FileStream::readFrom(void* buffer, const uint size, const ulong offset) const
{
    LS_THROW(m_accessMode.is(StreamAccess::Read),
             IoException,
             "Stream is not readable");
    ulong pos = tell();
    LS_THROW(cseek(offset),
             IoException,
             "Seeking error");
    m_stream.read(reinterpret_cast<char*>(buffer), size);
    LS_THROW(cseek(pos),
             IoException,
             "Seeking error");
    LS_THROW(!!m_stream,
             IoException,
             "Reading error");
}
ulong FileStream::tell() const
{
    return boost::iostreams::seek(m_stream, 0, std::ios_base::cur);
}
bool FileStream::seek(const ulong offset)
{
    return cseek(offset);
}
bool FileStream::cseek(const ulong offset) const
{
    boost::iostreams::seek(m_stream, boost::iostreams::position_to_offset(offset), std::ios_base::beg);
    return !!m_stream;
}
ulong FileStream::size() const
{
    boost::system::error_code errorHandle;
    return boost::filesystem::file_size(m_path, errorHandle);
}
bool FileStream::eof() const
{
    return m_stream.eof();
}