/// @file ls/io/Encoded.h
/// @brief Encoded value description
#pragma once

#include "ls/common.h"
#include "ls/io/Encoding.h"

namespace ls
{
    /// @addtogroup LsIo
    /// @{
    
    /// @brief Encoded value
    ///
    /// @tparam T Encoded value base type
    /// @tparam E Value encoding type
    template<class T, Encoding E>
    class Encoded
    {
    public:
        /// @brief Value type
        using Type = T;
        /// @brief Encoding tag
        static const ls::Encoding Encoding = E;
    public:
        /// @brief Default ctor
        Encoded() = default;
        /// @brief Init by value
        Encoded(const Type& value)
            : value(value)
        {
        }
        /// @brief Get value
        Type& get()
        {
            return value;
        }
        /// @brief Get const value
        const Type& get() const
        {
            return value;
        }
        /// @brief Get value
        Type* operator ->()
        {
            return &value;
        }
        /// @brief Get const value
        const Type* operator ->() const
        {
            return &value;
        }
        /// @brief Get value
        Type& operator *()
        {
            return value;
        }
        /// @brief Get const value
        const Type& operator *() const
        {
            return value;
        }
    private:
        /// @brief Value
        Type value;
    };
    /// @}
}