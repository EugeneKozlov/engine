#include "pch.h"
#include "ls/io/Encoding.h"
#include "ls/core/PropertyArray.h"

using namespace ls;
/// Encoding Property
struct EncodingProperty
{
    /// @brief Ctor
    EncodingProperty(const Encoding key, const string& name, const EncodingCategory category)
        : key(key)
        , name(name)
        , category(category)
    {
    }
    Encoding key; ///< Key
    string name; ///< Name
    EncodingCategory category; ///< Category
};
/// Encoding Properties
static PropertyArray<EncodingProperty, static_cast<uint>(Encoding::COUNT)> EncodingProperties = 
{
    EncodingProperty(Encoding::Empty, "Empty", EncodingCategory::Unknown),

    EncodingProperty(Encoding::Byte, "Byte", EncodingCategory::Integer),
    EncodingProperty(Encoding::Short, "Short", EncodingCategory::Integer),
    EncodingProperty(Encoding::Int, "Int", EncodingCategory::Integer),

    EncodingProperty(Encoding::Half, "Half", EncodingCategory::Real),
    EncodingProperty(Encoding::Float, "Float", EncodingCategory::Real),
    EncodingProperty(Encoding::Fixed3, "Fixed3", EncodingCategory::Real),
    EncodingProperty(Encoding::Fixed4, "Fixed4", EncodingCategory::Real),
    EncodingProperty(Encoding::Double, "Double", EncodingCategory::Real),

    EncodingProperty(Encoding::VectorByte, "VectorByte", EncodingCategory::IntegerVector),
    EncodingProperty(Encoding::VectorShort, "VectorShort", EncodingCategory::IntegerVector),
    EncodingProperty(Encoding::VectorInt, "VectorInt", EncodingCategory::IntegerVector),

    EncodingProperty(Encoding::VectorHalf, "VectorHalf", EncodingCategory::RealVector),
    EncodingProperty(Encoding::VectorFloat, "VectorFloat", EncodingCategory::RealVector),
    EncodingProperty(Encoding::VectorFixed3, "VectorFixed3", EncodingCategory::RealVector),
    EncodingProperty(Encoding::VectorFixed4, "VectorFixed4", EncodingCategory::RealVector),
    EncodingProperty(Encoding::VectorDouble, "VectorDouble", EncodingCategory::RealVector),

    EncodingProperty(Encoding::DirectionFloat3, "DirectionFloat3", EncodingCategory::Direction3),
    EncodingProperty(Encoding::DirectionSphere, "DirectionSphere", EncodingCategory::Direction3),
    EncodingProperty(Encoding::DirectionDoubleSphere, "DirectionDoubleSphere", EncodingCategory::Direction3),
    EncodingProperty(Encoding::DirectionSphereLength, "DirectionSphereLength", EncodingCategory::Direction3),

    EncodingProperty(Encoding::RotationPaq, "RotationPaq", EncodingCategory::Rotation3),
    EncodingProperty(Encoding::RotationAxisAngle, "RotationAxisAngle", EncodingCategory::Rotation3),
};
Encoding ls::encodingByName(const char* name)
{
    static map<string, Encoding> remap;
    if (remap.empty())
    {
        for (auto& prop : EncodingProperties)
            remap.emplace(prop->name, prop->key);
    }
    const Encoding* encoding = findOrDefault(remap, name);
    if (encoding)
        return *encoding;
    else
        return Encoding::Unknown;
}
EncodingCategory ls::encodingCategory(const Encoding encoding)
{
    return EncodingProperties[encoding].category;
}
const char* ls::asString(const Encoding encoding)
{
    return EncodingProperties[encoding].name.c_str();
}
