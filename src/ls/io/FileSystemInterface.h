/// @file ls/io/FileSystemInterface.h
/// @brief File system interface
#pragma once

#include "ls/common.h"
#include "ls/io/StreamInterface.h"

namespace ls
{
    /// @addtogroup LsIo
    /// @{
    
    /// @brief File System Interface
    /// 
    /// Source of data streams and other file systems.
    class FileSystemInterface
        : Noncopyable
    {
    public:
        /// @brief Open disc file system
        static FileSystem open(const string& folder);
    protected:
        /// @brief Ctor
        FileSystemInterface(const StreamAccessFlag access)
            : m_accessMode(access)
        {
        }
    public:
        /// @brief Dtor
        virtual ~FileSystemInterface()
        {
        }
        /// @brief Decorate stream name
        virtual string decorateName(const string& name) = 0;
        /// @brief Open new file stream (use @a StreamAccess::New to clear file)
        virtual Stream openStream(const string& name,
                                  const StreamAccessFlag access = StreamAccess::Read) = 0;
        /// @brief Open new file system with @a name and @a password (archives only).
        virtual FileSystem openSystem(const string& name, const string& password = "",
                                      const StreamAccessFlag access = StreamAccess::ReadWriteNew) = 0;
        /// @brief Delete item
        virtual bool deleteItem(const string& name) = 0;
        /// @brief Does @b file exist?
        virtual bool isRegularFile(const string& fileName) = 0;
        /// @brief Get last file write time
        /// @return 0 if failed - emptiness is initial
        virtual std::time_t lastWriteTime(const string& fileName) = 0;
        /// @brief Get all files list
        virtual void enumerateItems(vector<string>& files) = 0;
        /// @brief Iterate over all files
        virtual bool iterateItems(function<bool(StreamInterface&)> func) = 0;
    protected:
        /// @brief Access mode
        StreamAccessFlag m_accessMode;
    };
    /// @}
}
