/// @file ls/io/MemoryStream.h
/// @brief Memory stream impl
#pragma once

#include "ls/common.h"
#include "ls/io/StreamInterface.h"

namespace ls
{
    /// @addtogroup LsIo
    /// @{

    /// @brief Memory stream
    class MemoryStream
        : public virtual StreamInterface
    {
    public:
        /// @brief Ctor
        MemoryStream(const StreamAccessFlag access);
        MemoryStream(vector<ubyte> && data, const StreamAccessFlag access);
        virtual ~MemoryStream();
        virtual void write(const void* buffer, const uint size) override;
        virtual void read(void* buffer, const uint size) override;
        virtual void writeTo(const void* buffer, const uint size, const ulong offset) override;
        virtual void readFrom(void* buffer, const uint size, const ulong offset) const override;

        virtual bool seek(const ulong offset) override;
        virtual ulong tell() const override;

        virtual ulong size() const override;
        virtual bool eof() const override;
    protected:
        /// @brief Buffer
        vector<ubyte> m_data;
        /// @brief Offset
        ulong m_offset = 0;
    };
    /// @}
}
