/// @file ls/io/FileSystemInterface.h
/// @brief File system interface
#pragma once

#include "ls/common.h"
#include "ls/io/FileSystemInterface.h"

namespace ls
{
    /// @addtogroup LsIo
    /// @{
    
    /// @brief Multi-system
    class MultiFileSystem
        : public FileSystemInterface
    {
    public:
        /// @brief Empty
        MultiFileSystem();
        /// @brief Ctor
        MultiFileSystem(initializer_list<FileSystem> systems);
        /// @brief Push file system
        void pushBack(FileSystem fileSystem);
        /// @brief Pop file system
        void popBack();
        /// @brief Push file system
        void pushFront(FileSystem fileSystem);
        /// @brief Pop file system
        void popFront();
        /// @brief Open multi file system
        FileSystem openMultisystem(const string& name, const string& password,
                                   const StreamAccessFlag access);
    public:
        virtual string decorateName(const string& name) override;
        virtual Stream openStream(const string& name, const StreamAccessFlag access) override;
        virtual FileSystem openSystem(const string& name, const string& password,
                                  const StreamAccessFlag access) override;
        virtual bool deleteItem(const string& name) override;
        virtual bool isRegularFile(const string& fileName) override;
        virtual std::time_t lastWriteTime(const string& fileName) override;
        virtual void enumerateItems(vector<string>& files) override;
        virtual bool iterateItems(function<bool(StreamInterface&)> func) override;
    private:
        deque<FileSystem> m_systems;
    };
    /// @brief Multi-system back folder holder
    class BackFolderHolder
        : Noncopyable
    {
    public:
        BackFolderHolder(MultiFileSystem& base, FileSystem added)
            : m_base(base)
        {
            m_base.pushBack(added);
        }
        ~BackFolderHolder()
        {
            m_base.popBack();
        }
    private:
        MultiFileSystem& m_base;
    };
    /// @brief Multi-system front folder holder
    class FrontFolderHolder
        : Noncopyable
    {
    public:
        FrontFolderHolder(MultiFileSystem& base, FileSystem added)
            : m_base(base)
        {
            m_base.pushFront(added);
        }
        ~FrontFolderHolder()
        {
            m_base.popFront();
        }
    private:
        MultiFileSystem& m_base;
    };
    /// @}
}
