#include "pch.h"
#include "ls/io/Buffer.h"
#include "ls/io/StreamInterface.h"

using namespace ls;

// Pull/push
Buffer::Buffer(StreamInterface& src, const uint size)
{
    m_writtenBytes = size > 0 ? size : static_cast<uint>(src.size());

    m_data.resize(m_writtenBytes);
    src.read(m_data.data(), m_data.size());
}
Buffer Buffer::pull(StreamInterface& src, const uint size)
{
    return Buffer(src, size);
}
void Buffer::push(StreamInterface& dest)
{
    dest.write(m_data.data(), m_data.size());
}


// Read/write
void Buffer::release()
{
    m_data.clear();
    m_data.shrink_to_fit();
    m_readBytes = 0;
    m_writtenBytes = 0;
}

// Ctor
Buffer::Buffer(const vector<ubyte>& data)
    : m_data(data)
    , m_writtenBytes(m_data.size())
{

}
Buffer::Buffer(vector<ubyte> && data)
    : m_data(std::move(data))
    , m_writtenBytes(m_data.size())
{

}


// I/O
void Buffer::read(void* buffer, const uint size)
{
    // Check
    LS_THROW(size <= avalilable(),
             IoException,
             "Can't read ", size, " bytes");

    // Read
    if (size > 0)
    {
        std::copy_n(&m_data[m_readBytes], size, static_cast<ubyte*>(buffer));
        m_readBytes += size;
    }
}
void Buffer::write(const void* buffer, const uint size)
{
    // Write
    if (size > 0)
    {
        m_data.insert(m_data.end(), size, 0);
        std::copy_n(static_cast<const ubyte*>(buffer), size, &m_data[m_writtenBytes]);
        m_writtenBytes += size;
    }
}
void Buffer::readText(string& buffer)
{
    const ubyte* begin = &m_data[m_readBytes];
    buffer.append(begin, begin + m_writtenBytes - m_readBytes);
}
void Buffer::writeText(const string& buffer)
{
    write(buffer.data(), buffer.size());
}
void Buffer::seek(const uint pos)
{
    // Check
    LS_THROW(pos <= m_writtenBytes,
             IoException,
             "Can't seek to ", pos, "byte");

    m_readBytes = pos;
}
uint Buffer::avalilable() const
{
    return m_writtenBytes - m_readBytes;
}
uint Buffer::size()
{
    return m_writtenBytes;
}
const ubyte* Buffer::data() const
{
    return m_data.data();
}
