/// @file ls/io/FileStream.h
/// @brief Hard drive file stream impl
#pragma once

#include "ls/common.h"
#include "ls/io/common.h"
#include "ls/io/StreamInterface.h"

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/iostreams/seek.hpp>

namespace ls
{
    /// @addtogroup LsIo
    /// @{
        
    /// @brief Hard drive file stream impl
    class FileStream
        : public StreamInterface
    {
    public:
        /// @brief Ctor
        FileStream(const boost::filesystem::path& path, const StreamAccessFlag access);
        virtual ~FileStream();
        virtual void write(const void* buffer, const uint size) override;
        virtual void read(void* buffer, const uint size) override;
        virtual void writeTo(const void* buffer, const uint size, const ulong offset) override;
        virtual void readFrom(void* buffer, const uint size, const ulong offset) const override;

        virtual bool seek(const ulong offset) override;
        virtual ulong tell() const override;

        virtual ulong size() const override;
        virtual bool eof() const override;
    protected:
        /// @brief Const seek
        bool cseek(const ulong offset) const;
        /// @brief Get internal flags by StreamAccess flags
        static std::ios_base::openmode flags(const StreamAccessFlag access);
    protected:
        /// @brief Path
        boost::filesystem::path m_path;
        /// @brief File
        mutable boost::filesystem::fstream m_stream;
    };
    /// @}
}
