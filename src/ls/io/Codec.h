/// @file ls/io/Codec.h
/// @brief Codec
#pragma once

#include "ls/common.h"
#include "ls/io/Encoding.h"
#include "ls/io/Encoded.h"

namespace ls
{
    /// @addtogroup LsIo
    /// @{
    
    /// @brief Enable if meets serialize-able concept
    template <class Object, class Type = void>
    using EnableIfSerializable = enable_if_t<IsSerializable<Object>::value, Type>;
    /// @brief Enable if meets operable concept
    template <class Object, class Type = void>
    using EnableIfOperable = enable_if_t<IsOperable<Object>::value, Type>;
    /// @brief Codec prototype
    template <class Object, Encoding Tag, class Enable = void>
    struct Codec;
    /// @brief Empty Codec
    template <class Object>
    struct EmptyCodec
    {
        /// @brief Encode 
        template <class Stream>
        static void encode(Stream& /*stream*/, Object const& /*object*/)
        {
        }
        /// @brief Decode
        template <class Stream>
        static void decode(Stream& /*stream*/, Object& /*object*/)
        {
        }
        /// @brief Get size
        static constexpr uint size()
        {
            return 0;
        }
    };
    template <class T>
    struct Codec<T, Encoding::Empty> : EmptyCodec<T>{};
    /// @brief Array Codec
    template <class T, size_t N, Encoding Tag>
    struct Codec<array<T, N>, Tag, enable_if_t<sizeof(T) != 1>>
    {
        /// @brief Encode 
        template <class Stream>
        static void encode(Stream& stream, const array<T, N>& object)
        {
            for (uint idx = 0; idx < N; ++idx)
                Codec<T, Tag>::encode(stream, object[idx]);
        }
        /// @brief Decode
        template <class Stream>
        static void decode(Stream& stream, array<T, N>& object)
        {
            for (uint idx = 0; idx < N; ++idx)
                Codec<T, Tag>::decode(stream, object[idx]);
        }
        /// @brief Get size
        static constexpr uint size()
        {
            return Codec<T, Tag>::size() * N;
        }
    };
    template <class T, size_t N>
    struct Codec<array<T, N>, Encoding::Byte, enable_if_t<sizeof(T) == 1>>
    {
        /// @brief Encode 
        template <class Stream>
        static void encode(Stream& stream, const array<T, N>& object)
        {
            stream.write(object.data(), N);
        }
        /// @brief Decode
        template <class Stream>
        static void decode(Stream& stream, array<T, N>& object)
        {
            stream.read(object.data(), N);
        }
        /// @brief Get size
        static constexpr uint size()
        {
            return N;
        }
    };
    /// @brief Unsigned/Signed Integer Type
    template <class Base, bool Signed>
    struct UnSignedInteger;
    /// @brief Signed
    template <class Base>
    struct UnSignedInteger<Base, true>
    {
        /// @brief Type
        using type = typename make_signed<Base>::type;
    };
    /// @brief Unsigned
    template <class Base>
    struct UnSignedInteger<Base, false>
    {
        /// @brief Type
        using type = typename make_unsigned<Base>::type;
    };
    /// @brief Corresponding Signed Integer Type
    template <class Base, class Sign>
    using CorrespondingSignedInteger = typename UnSignedInteger<Base, is_signed<Sign>::value>::type;
    /// @brief Integer Codec
    template <class Integer, class Packed>
    struct IntegerCodec
    {
        /// @brief Encode 
        template <class Stream>
        static void encode(Stream& stream, const Integer& object)
        {
            using Unsigned = typename make_unsigned<Integer>::type;
            Packed value = hton(static_cast<Packed>(static_cast<Unsigned>(object)));
            stream.write(&value, sizeof(Packed));
        }
        /// @brief Decode
        template <class Stream>
        static void decode(Stream& stream, Integer& object)
        {
            using Signed = CorrespondingSignedInteger<Packed, Integer>;
            Packed value;
            stream.read(&value, sizeof(Packed));
            object = static_cast<Integer>(static_cast<Signed>(ntoh(value)));
        }
        /// @brief Get size
        static constexpr uint size()
        {
            return sizeof(Packed);
        }
    };
    template <class T>
    struct Codec<T, Encoding::Byte, EnableIfInteger<T>> : IntegerCodec<T, ubyte>{};
    template <class T>
    struct Codec<T, Encoding::Short, EnableIfInteger<T>> : IntegerCodec<T, ushort>{};
    template <class T>
    struct Codec<T, Encoding::Int, EnableIfInteger<T>> : IntegerCodec<T, uint>{};
    /// @brief Real Codec
    template <class Real, class Packed>
    struct RealCodec
    {
        /// @brief Encode 
        template <class Stream>
        static void encode(Stream& stream, const Real& object)
        {
            Packed value = static_cast<Packed>(object);
            stream.write(&value, sizeof(Packed));
        }
        /// @brief Decode
        template <class Stream>
        static void decode(Stream& stream, Real& object)
        {
            Packed value;
            stream.read(&value, sizeof(Packed));
            object = static_cast<Real>(value);
        }
        /// @brief Get size
        static constexpr uint size()
        {
            return sizeof(Packed);
        }
    };
    template <class T>
    struct Codec<T, Encoding::Half, EnableIfReal<T>> : RealCodec<T, half>{};
    template <class T>
    struct Codec<T, Encoding::Float, EnableIfReal<T>> : RealCodec<T, float>{};
    template <class T>
    struct Codec<T, Encoding::Fixed3, EnableIfReal<T>> : RealCodec<T, int3fixed>{};
    template <class T>
    struct Codec<T, Encoding::Fixed4, EnableIfReal<T>> : RealCodec<T, int4fixed>{};
    template <class T>
    struct Codec<T, Encoding::Double, EnableIfReal<T>> : RealCodec<T, double>{};
    /// @brief Integer Vector Codec
    template <uint N, class Integer, class Packed>
    struct IntegerVectorCodec
    {
        /// @brief Encode 
        template <class Stream>
        static void encode(Stream& stream, const Vector<Integer, N>& object)
        {
            for (uint idx = 0; idx < N; ++idx)
                IntegerCodec<Integer, Packed>::encode(stream, object[idx]);
        }
        /// @brief Decode
        template <class Stream>
        static void decode(Stream& stream, Vector<Integer, N>& object)
        {
            for (uint idx = 0; idx < N; ++idx)
                IntegerCodec<Integer, Packed>::decode(stream, object[idx]);
        }
        /// @brief Get size
        static constexpr uint size()
        {
            return IntegerCodec<Integer, Packed>::size() * N;
        }
    };
    template <class T, uint N>
    struct Codec<Vector<T, N>, Encoding::VectorByte, EnableIfInteger<T>> : IntegerVectorCodec<N, T, ubyte>{};
    template <class T, uint N>
    struct Codec<Vector<T, N>, Encoding::VectorShort, EnableIfInteger<T>> : IntegerVectorCodec<N, T, ushort>{};
    template <class T, uint N>
    struct Codec<Vector<T, N>, Encoding::VectorInt, EnableIfInteger<T>> : IntegerVectorCodec<N, T, uint>{};
    /// @brief Real Vector Codec
    template <uint N, class Real, class Packed>
    struct RealVectorCodec
    {
        /// @brief Encode 
        template <class Stream>
        static void encode(Stream& stream, const Vector<Real, N>& object)
        {
            for (uint idx = 0; idx < N; ++idx)
                RealCodec<Real, Packed>::encode(stream, object[idx]);
        }
        /// @brief Decode
        template <class Stream>
        static void decode(Stream& stream, Vector<Real, N>& object)
        {
            for (uint idx = 0; idx < N; ++idx)
                RealCodec<Real, Packed>::decode(stream, object[idx]);
        }
        /// @brief Get size
        static constexpr uint size()
        {
            return RealCodec<Real, Packed>::size() * N;
        }
    };
    template <class T, uint N>
    struct Codec<Vector<T, N>, Encoding::VectorHalf, EnableIfReal<T>> : RealVectorCodec<N, T, half>{};
    template <class T, uint N>
    struct Codec<Vector<T, N>, Encoding::VectorFloat, EnableIfReal<T>> : RealVectorCodec<N, T, float>{};
    template <class T, uint N>
    struct Codec<Vector<T, N>, Encoding::VectorFixed3, EnableIfReal<T>> : RealVectorCodec<N, T, int3fixed>{};
    template <class T, uint N>
    struct Codec<Vector<T, N>, Encoding::VectorFixed4, EnableIfReal<T>> : RealVectorCodec<N, T, int4fixed>{};
    template <class T, uint N>
    struct Codec<Vector<T, N>, Encoding::VectorDouble, EnableIfReal<T>> : RealVectorCodec<N, T, double>{};
    /// @brief Sphere Direction Codec
    template <class Integer, Encoding LengthEncoding>
    struct SphereDirectionCodec
    {
        /// @brief Encode 
        template <class Stream>
        static void encode(Stream& stream, const float3& object)
        {
            const float length = object.length();
            const bool isZero = length <= numeric_limits<float>::epsilon();
            float3 normalized = isZero ? float3(1.0f, 0.0f, 0.0f) : (object / length);
            float2 spherical = normal2sphere(normalized);
            Integer sx = real2unorm<Integer>(spherical.x);
            Integer sy = real2unorm<Integer>(spherical.y);

            IntegerCodec<Integer, Integer>::encode(stream, sx);
            IntegerCodec<Integer, Integer>::encode(stream, sy);
            Codec<float, LengthEncoding>::encode(stream, length);
        }
        /// @brief Decode
        template <class Stream>
        static void decode(Stream& stream, float3& object)
        {
            Integer sx, sy;
            float length = 1.0f;
            IntegerCodec<Integer, Integer>::decode(stream, sx);
            IntegerCodec<Integer, Integer>::decode(stream, sy);
            Codec<float, LengthEncoding>::decode(stream, length);

            object = sphere2normal(float2(unorm2real<float>(sx),
                                          unorm2real<float>(sy))) * length;
        }
        /// @brief Get size
        static constexpr uint size()
        {
            return IntegerCodec<Integer, Integer>::size() * 2
                + Codec<float, LengthEncoding>::size();
        }
    };
    template <>
    struct Codec<float3, Encoding::DirectionSphere> : SphereDirectionCodec<ushort, Encoding::Empty>{};
    template <>
    struct Codec<float3, Encoding::DirectionDoubleSphere> : SphereDirectionCodec<uint, Encoding::Empty>{};
    template <>
    struct Codec<float3, Encoding::DirectionSphereLength> : SphereDirectionCodec<ushort, Encoding::Half>{};
    template <>
    struct Codec<float3, Encoding::DirectionFloat3> : Codec<float3, Encoding::VectorFloat>{};
    /// @brief Quaternion Rotation Coded
    template <class Integer>
    struct QuaternionRotationCodec
    {
        /// @brief Encode 
        template <class Stream>
        static void encode(Stream& stream, const quat& object)
        {
            const float4 normalized = float4(normalize(object).value()) * 0.5f + float4(0.5f);
            const Vector<Integer, 4> encoded(real2unorm<Integer>(normalized.x),
                                             real2unorm<Integer>(normalized.y),
                                             real2unorm<Integer>(normalized.z),
                                             real2unorm<Integer>(normalized.w));
            IntegerVectorCodec<4, Integer, Integer>::encode(stream, encoded);
        }
        /// @brief Decode
        template <class Stream>
        static void decode(Stream& stream, quat& object)
        {
            Vector<Integer, 4> encoded;
            IntegerVectorCodec<4, Integer, Integer>::decode(stream, encoded);
            const float4 temp(unorm2real<float>(encoded[0]),
                              unorm2real<float>(encoded[1]),
                              unorm2real<float>(encoded[2]),
                              unorm2real<float>(encoded[3]));
            object = quat(temp * 2.0f - float4(1.0f));
        }
        /// @brief Get size
        static constexpr uint size()
        {
            return IntegerVectorCodec<4, Integer, Integer>::size();
        }
    };
    template <>
    struct Codec<quat, Encoding::RotationPaq> : QuaternionRotationCodec<ubyte>{};
    /// @brief Axis Angle Rotation Coded
    template <class Integer>
    struct AxisAngleRotationCodec
    {
        /// @brief Encode 
        template <class Stream>
        static void encode(Stream& stream, const quat& object)
        {
            const float4 axisAngle = object.asAxisAngle();
            const float2 spherical = normal2sphere(float3(axisAngle));
            IntegerCodec<Integer, Integer>::encode(stream, real2unorm<ushort>(spherical.x));
            IntegerCodec<Integer, Integer>::encode(stream, real2unorm<ushort>(spherical.y));
            IntegerCodec<Integer, Integer>::encode(stream, real2unorm<ushort>(axisAngle.w / (2 * pi)));
        }
        /// @brief Decode
        template <class Stream>
        static void decode(Stream& stream, quat& object)
        {
            ushort encX, encY, angle;
            IntegerCodec<Integer, Integer>::decode(stream, encX);
            IntegerCodec<Integer, Integer>::decode(stream, encY);
            IntegerCodec<Integer, Integer>::decode(stream, angle);
            const float2 spherical(unorm2real<float>(encX), unorm2real<float>(encY));
            object = quat(sphere2normal(spherical), unorm2real<float>(angle) * 2 * pi);
        }
        /// @brief Get size
        static constexpr uint size()
        {
            return IntegerCodec<Integer, Integer>::size() * 3;
        }
    };
    template <>
    struct Codec<quat, Encoding::RotationAxisAngle> : AxisAngleRotationCodec<ushort>{};

    /// @brief Const Wrapper
    template <class Type, Type Const>
    struct ConstWrapper
    {
        /// @brief Const
        static const Type value = Const;
    };
    /// @brief Multi Codec Encode
    template <class Object, class Stream>
    struct MultiCodecEncode 
        : Noncopyable
    {
        /// @brief Ctor
        MultiCodecEncode(Encoding encoding, Stream& stream, const Object& object)
            : encoding(encoding)
            , stream(&stream)
            , object(&object)
        {
        }
        /// @brief Encode
        template <class Wrapper>
        bool operator() (TypeWrapper<Wrapper>)
        {
            if (Wrapper::value != encoding)
                return true;

            Codec<Object, Wrapper::value>::encode(*stream, *object);
            return false;
        }
    private:
        const Encoding encoding;
        Stream* const stream;
        const Object* const object;
    };
    /// @brief Multi Codec Decode
    template <class Object, class Stream>
    struct MultiCodecDecode 
        : Noncopyable
    {
        /// @brief Ctor
        MultiCodecDecode(Encoding encoding, Stream& stream, Object& object)
            : encoding(encoding)
            , stream(&stream)
            , object(&object)
        {
        }
        /// @brief Decode
        template <class Wrapper>
        bool operator() (TypeWrapper<Wrapper>)
        {
            if (Wrapper::value != encoding)
                return true;

            Codec<Object, Wrapper::value>::decode(*stream, *object);
            return false;
        }
    private:
        const Encoding encoding;
        Stream* const stream;
        Object* const object;
    };
    /// @brief Multi Codec Encode
    template <class Object>
    struct MultiCodecSize 
        : Noncopyable
    {
        /// @brief Ctor
        MultiCodecSize(Encoding encoding)
            : encoding(encoding)
        {
        }
        /// @brief Encode
        template <class Wrapper>
        bool operator() (TypeWrapper<Wrapper>)
        {
            if (Wrapper::value != encoding)
                return true;

            size = Codec<Object, Wrapper::value>::size();
            return false;
        }
        /// @brief Size
        uint size = 0;
    private:
        const Encoding encoding;
    };
    /// @brief Multi Codec
    template <class Object, Encoding ... Tags>
    struct MultiCodec
    {
        /// @brief Encoding Tags
        using EncodingTags = tuple<ConstWrapper<Encoding, Tags>...>;
        /// @brief Encode 
        template <class Stream>
        static void encode(const Encoding encoding, Stream& stream, const Object& object)
        {
            MultiCodecEncode<Object, Stream> iter(encoding, stream, object);
            const bool success = !for_each_type<EncodingTags>(iter);
            debug_assert(success, "Unlisted encoding [", encoding, "]");
        }
        /// @brief Decode
        template <class Stream>
        static void decode(const Encoding encoding, Stream& stream, Object& object)
        {
            MultiCodecDecode<Object, Stream> iter(encoding, stream, object);
            const bool success = !for_each_type<EncodingTags>(iter);
            debug_assert(success, "Unlisted encoding [", encoding, "]");
        }
        /// @brief Get size
        static uint size(const Encoding encoding)
        {
            MultiCodecSize<Object> iter(encoding);
            const bool success = !for_each_type<EncodingTags>(iter);
            debug_assert(success, "Unlisted encoding [", encoding, "]");
            return iter.size;
        }
    };

    /// @brief Universal Codec
    template <class Object, class Enable = void>
    struct UniversalCodec;
    /// @brief Integer Universal Codec
    template <class Integer>
    struct UniversalCodec<Integer, EnableIfInteger<Integer>> : MultiCodec<
        Integer
        , Encoding::Empty
        , Encoding::Byte
        , Encoding::Short
        , Encoding::Int
    >{};
    /// @brief Real Universal Codec
    template <class Real>
    struct UniversalCodec<Real, EnableIfReal<Real>> : MultiCodec<
        Real
        , Encoding::Empty
        , Encoding::Half
        , Encoding::Float
        , Encoding::Fixed3
        , Encoding::Fixed4
        , Encoding::Double
    >{};
    /// @brief Integer Vector Universal Codec
    template <class Integer, uint N>
    struct UniversalCodec<Vector<Integer, N>, EnableIfInteger<Integer>> : MultiCodec<
        Vector<Integer, N>
        , Encoding::Empty
        , Encoding::VectorByte
        , Encoding::VectorShort
        , Encoding::VectorInt
    >{};
    /// @brief Real Vector Universal Codec
    template <class Real, uint N>
    struct UniversalCodec<Vector<Real, N>, EnableIfReal<Real>> : MultiCodec<
        Vector<Real, N>
        , Encoding::Empty
        , Encoding::VectorHalf
        , Encoding::VectorFloat
        , Encoding::VectorFixed3
        , Encoding::VectorFixed4
        , Encoding::VectorDouble
    >{};
    /// @brief Direction Universal Codec
    template <>
    struct UniversalCodec<float3> : MultiCodec<
        float3
        , Encoding::Empty
        , Encoding::VectorHalf
        , Encoding::VectorFloat
        , Encoding::VectorFixed3
        , Encoding::VectorFixed4
        , Encoding::VectorDouble
        , Encoding::DirectionFloat3
        , Encoding::DirectionSphere
        , Encoding::DirectionDoubleSphere
        , Encoding::DirectionSphereLength
    >{};
    /// @brief Rotation Universal Codec
    template <>
    struct UniversalCodec<quat> : MultiCodec<
        quat
        , Encoding::Empty
        , Encoding::RotationPaq
        , Encoding::RotationAxisAngle
    >{};
    /// @brief Byte Array Universal Codec
    template <class T, uint N>
    struct UniversalCodec<array<T, N>, enable_if_t<sizeof(T) == 1>> : MultiCodec<
        array<T, N>
        , Encoding::Empty
        , Encoding::Byte
    >{};

    /// @brief Encode
    template <class Object, class Stream>
    void encodeObject(const Encoding tag, const Object& value, Stream& stream)
    {
        UniversalCodec<Object>::encode(tag, stream, value);
    }
    /// @brief Decode
    template <class Object, class Stream>
    void decodeObject(const Encoding tag, Object& value, Stream& stream)
    {
        UniversalCodec<Object>::decode(tag, stream, value);
    }
    /// @brief Size
    template <class Object>
    uint sizeOfObject(const Encoding tag)
    {
        return UniversalCodec<Object>::size(tag);
    }

    /// @brief Encode simple @a value and write it into @a stream
    template <Encoding Format, class Object, class Stream>
    void encode(Stream& stream, const Object& value)
    {
        Codec<Object, Format>::encode(stream, value);
    }
    /// @brief Read from @a stream and decode simple @a value
    template <Encoding Format, class Object, class Stream>
    void decode(Stream& stream, Object& value)
    {
        Codec<Object, Format>::decode(stream, value);
    }
    /// @brief Serialize simple @a value
    template <Encoding Format, class Object, class Stream>
    void serialize(Archive<Stream>& archive, Object& value)
    {
        if (archive.isWrite())
        {
            encode<Format>(archive.stream(), value);
        } 
        else
        {
            decode<Format>(archive.stream(), value);
        }
    }
    /// @brief Get size of simple @a value
    template <Encoding Format, class Object>
    uint sizeOf()
    {
        return Codec<Object, Format>::size();
    }
    /// @brief Encode simple @a value and write it into @a stream
    template <class Object, Encoding Format, class Stream>
    void encode(Stream& stream, const Encoded<Object, Format>& value)
    {
        Codec<Object, Format>::encode(stream, *value);
    }
    /// @brief Read from @a stream and decode simple @a value
    template <class Object, Encoding Format, class Stream>
    void decode(Stream& stream, Encoded<Object, Format>& value)
    {
        Codec<Object, Format>::decode(stream, *value);
    }
    /// @brief Serialize simple @a value
    template <class Object, Encoding Format, class Stream>
    void serialize(Archive<Stream>& archive, Encoded<Object, Format>& value)
    {
        if (archive.isWrite())
        {
            encode(archive.stream(), value);
        }
        else
        {
            decode(archive.stream(), value);
        }
    }
    /// @brief Get size of simple @a value
    template <class Object, Encoding Format>
    uint sizeOf(const Encoded<Object, Format>& /*value*/)
    {
        return Codec<Object, Format>::size();
    }

    /// @brief Operable decode helper
    template<class Stream>
    struct OperableDecodeHelper
        : Noncopyable
    {
        /// @brief Target stream
        Stream* stream = nullptr;
        /// @brief Ctor
        OperableDecodeHelper(Stream& stream)
            : stream(&stream)
        {
        }
        /// @brief Encode @a object
        template <class Object>
        void operator ()(Object& object)
        {
            decode(*stream, object);
        }
    };
    /// @brief Operable encode helper
    template<class Stream>
    struct OperableEncodeHelper
        : Noncopyable
    {
        /// @brief Target stream
        Stream* stream = nullptr;
        /// @brief Ctor
        OperableEncodeHelper(Stream& stream)
            : stream(&stream)
        {
        }
        /// @brief Encode @a object
        template <class Object>
        void operator ()(const Object& object)
        {
            encode(*stream, object);
        }
    };
    /// @brief Operable size helper
    struct OperableSizeHelper
    {
        /// @brief Computed size
        uint size = 0;
        /// @brief Encode @a object
        template <class Object>
        void operator ()(const Object& object)
        {
            size += sizeOf(object);
        }
    };
    /// @brief Encode operable complex @a value and write it into @a stream
    template <class Object, class Stream>
    EnableIfOperable<Object>
    encode(Stream& stream, const Object& value)
    {
        OperableEncodeHelper<Stream> foo(stream);
        operate(value, std::ref(foo));
    }
    /// @brief Read from @a stream and decode operable complex @a value
    template <class Object, class Stream>
    EnableIfOperable<Object>
    decode(Stream& stream, Object& value)
    {
        OperableDecodeHelper<Stream> foo(stream);
        operate(value, std::ref(foo));
    }
    /// @brief Get size of operable complex @a value
    template <class Object>
    EnableIfOperable<Object, uint>
    sizeOf(const Object& value)
    {
        OperableSizeHelper foo;
        operate(value, std::ref(foo));
        return foo.size;
    }

    /// @brief Encode serializable complex @a value and write it into @a stream
    template <class Object, class Stream>
    EnableIfSerializable<Object>
    encode(Stream& stream, const Object& value)
    {
        value.serialize(stream);
    }
    /// @brief Read from @a stream and decode serializable complex @a value
    template <class Object, class Stream>
    EnableIfSerializable<Object>
    decode(Stream& stream, Object& value)
    {
        value.deserialize(stream);
    }
    /// @brief Get size of serializable complex @a value
    template<class Object>
    EnableIfSerializable<Object, uint>
    sizeOf(const Object& value)
    {
        return value.size();
    }
    /// @}
}