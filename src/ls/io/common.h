/// @file ls/io/common.h
/// @brief Stream and File System declaration
#pragma once

#include "ls/common.h"

namespace ls
{
    class StreamInterface;
    class FileSystemInterface;

    /// @addtogroup LsIo
    /// @{

    /// @brief Shared stream
    using Stream = std::shared_ptr<StreamInterface>;
    /// @brief Shader file system
    using FileSystem = std::shared_ptr<FileSystemInterface>;
    /// @}
}
