/// @file ls/io/Encoding.h
/// @brief Encoding
#pragma once

#include "ls/common.h"

namespace ls
{
    /// @addtogroup LsIo
    /// @{
    
    /// @brief Encoding category
    enum class EncodingCategory
    {
        /// @brief Unknown
        Unknown,
        /// @brief Integer
        Integer,
        /// @brief Real
        Real,
        /// @brief Integer Vector
        IntegerVector,
        /// @brief Real Vector
        RealVector,
        /// @brief Direction in 3D
        Direction3,
        /// @brief Rotation in 3D
        Rotation3
    };
    /// @brief Encoding type
    enum class Encoding : ubyte
    {
        /// @brief Unknown
        Unknown = ubyte(-1),
        /// @brief Empty
        Empty = 0,

        /// @brief Byte (1 byte)
        Byte,
        /// @brief Short integer (2 bytes)
        Short,
        /// @brief Integer (4 bytes)
        Int,

        /// @brief Half
        Half,
        /// @brief Float
        Float,
        /// @brief Fixed3
        Fixed3,
        /// @brief Fixed4
        Fixed4,
        /// @brief Double
        Double,

        /// @brief Byte Vector
        VectorByte,
        /// @brief Short Vector
        VectorShort,
        /// @brief Int Vector
        VectorInt,

        /// @brief Half vector
        VectorHalf,
        /// @brief Float vector
        VectorFloat,
        /// @brief Fixed .3 vector
        VectorFixed3,
        /// @brief Fixed .4 vector
        VectorFixed4,
        /// @brief Double vector
        VectorDouble,

        /// @brief Float3 direction (12 bytes)
        DirectionFloat3,
        /// @brief Spheremap transform (4 bytes)
        DirectionSphere,
        /// @brief Double-precision spheremap transform (8 bytes)
        DirectionDoubleSphere,
        /// @brief Spheremap + length (6 bytes)
        DirectionSphereLength,

        /// @brief Packed quaternion (4 bytes)
        RotationPaq,
        /// @brief Axis and angle (6 bytes)
        RotationAxisAngle,

        /// @brief Number of
        COUNT
    };
    /// @brief Encoding by name
    Encoding encodingByName(const char* name);
    /// @brief Encoding category
    EncodingCategory encodingCategory(const Encoding encoding);
    /// @brief Name of encoding
    const char* asString(const Encoding encoding);
    /// @brief Is empty?
    inline bool isEmpty(const Encoding encoding)
    {
        return encoding == Encoding::Empty;
    }
    /// @brief Test category macro
#define LS_ADD_CATEGORY_TEST(CATEGORY)          \
    inline bool is##CATEGORY(Encoding encoding) \
    {                                           \
        return (encoding != Encoding::Unknown)  \
            && (encoding == Encoding::Empty     \
            || encodingCategory(encoding)       \
            == EncodingCategory::CATEGORY);     \
    }
    LS_ADD_CATEGORY_TEST(Integer);
    LS_ADD_CATEGORY_TEST(Real);
    LS_ADD_CATEGORY_TEST(IntegerVector);
    LS_ADD_CATEGORY_TEST(RealVector);
    LS_ADD_CATEGORY_TEST(Direction3);
    LS_ADD_CATEGORY_TEST(Rotation3);
#undef LS_ADD_CATEGORY_TEST
    /// @}
}