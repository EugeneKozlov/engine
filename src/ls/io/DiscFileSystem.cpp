#include "pch.h"
#include "ls/io/DiscFileSystem.h"

using namespace ls;
DiscFileSystem::DiscFileSystem(const Path path, const StreamAccessFlag access)
    : FileSystemInterface(access)
    , m_path(path)
{
    using namespace boost::filesystem;
    using namespace boost::system;
    error_code errorHandle;
    // Create if not exist
    if (!exists(m_path, errorHandle))
    {
        LS_THROW(access.is(StreamAccess::New),
                 FileSystemException,
                 "Folder [", m_path.string(), "] is not found");
        const bool success = create_directories(m_path, errorHandle);
        LS_THROW(success,
                 FileSystemException,
                 "Folder [", m_path.string(), "] creation failed "
                 "(", errorHandle.message(), ")");
    }
        // Check if directory
    else if (!errorHandle)
    {
        const bool isDirectory = is_directory(m_path, errorHandle);
        LS_THROW(isDirectory,
                 FileSystemException,
                 "Folder [", m_path.string(), "] is not a folder "
                 "(", errorHandle.message(), ")");
    }
    // Final test
    LS_THROW(!errorHandle,
             FileSystemException,
             "Folder [", m_path.string(), "] opening unknown error "
             "(", errorHandle.message(), ")");
}
string DiscFileSystem::decorateName(const string& name)
{
    return makeFull(name).string();
}
DiscFileSystem::Path DiscFileSystem::makeFull(const Path& shortName)
{
    return m_path / shortName;
}


// Manage
Stream DiscFileSystem::openStream(const string& name, const StreamAccessFlag access)
{
    // Check
    debug_assert(!(access.is(StreamAccess::Read) && !m_accessMode.is(StreamAccess::Read)));
    debug_assert(!(access.is(StreamAccess::Write) && !m_accessMode.is(StreamAccess::Write)));
    debug_assert(access.is(StreamAccess::Read) || access.is(StreamAccess::Write));

    // New
    try
    {
        return make_shared<FileStream>(makeFull(name), access);
    }
    catch (const FileSystemException&)
    {
        return nullptr;
    }
}
FileSystem DiscFileSystem::openSystem(const string& name,
                                      const string& password,
                                      const StreamAccessFlag access)
{
    using namespace boost::filesystem;
    using namespace boost::system;
    // Check
    debug_assert(!(access.is(StreamAccess::Read) && !m_accessMode.is(StreamAccess::Read)));
    debug_assert(!(access.is(StreamAccess::Write) && !m_accessMode.is(StreamAccess::Write)));
    debug_assert(access.is(StreamAccess::Read) || access.is(StreamAccess::Write));
    // As archive
    error_code errorHandle;
    const path file = makeFull(name);
    if (is_regular_file(file, errorHandle) && !errorHandle)
    {
        LS_THROW(0,
                 NotImplementedException);
        return nullptr;
    }
    else
    {
        // As dir
        try
        {
            return make_shared<DiscFileSystem>(file, access);
        }
        catch (const FileSystemException&)
        {
            return nullptr;
        }
    }
}
bool DiscFileSystem::deleteItem(const string& name)
{
    debug_assert(m_accessMode.is(StreamAccess::Write));
    using namespace boost::filesystem;
    using namespace boost::system;

    // Find path
    error_code errorHandle;
    const path fullName = makeFull(name);
    remove_all(fullName, errorHandle);
    return !!errorHandle;
}


// Gets
bool DiscFileSystem::isRegularFile(const string& fileName)
{
    boost::system::error_code errorHandle;
    using namespace boost::filesystem;
    const path name = makeFull(fileName);
    return is_regular_file(name, errorHandle) && !errorHandle;
}
std::time_t DiscFileSystem::lastWriteTime(const string& fileName)
{
    try
    {
        return boost::filesystem::last_write_time(makeFull(fileName));
    }
    catch (const boost::filesystem::filesystem_error& ex)
    {
        return static_cast<std::time_t>(0);
    }
}
void DiscFileSystem::enumerateItems(vector<string>& files)
{
    using namespace boost::filesystem;
    vector<path> paths;
    copy(directory_iterator(m_path),
         directory_iterator(),
         back_inserter(paths));
    for (auto iterFile : paths)
        files.push_back(iterFile.string());
}
bool DiscFileSystem::iterateItems(function<bool(StreamInterface&)> func)
{
    using namespace boost::filesystem;
    for (path file : make_pair(directory_iterator(m_path), directory_iterator()))
    {
        if (!is_regular_file(file))
            continue;
        try
        {
            FileStream stream(file, StreamAccess::Read);
            if (!func(stream))
                return false;
        }
        catch (const FileSystemException&)
        {
        }
    }
    return true;
}


// Ctor
FileSystem FileSystemInterface::open(const string& folder)
{
    try
    {
        return make_shared<DiscFileSystem>(DiscFileSystem::Path(folder), StreamAccess::ReadWriteNew);
    }
    catch (const FileSystemException&)
    {
        return nullptr;
    }
}
