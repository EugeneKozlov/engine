#include "pch.h"
#include "ls/scene/ObjectObserverInterface.h"
#include "ls/scene/ObjectState.h"
#include "ls/scene/ObjectInstance.h"
#include "ls/scene/ObjectPrototype.h"
#include "ls/scene/PoseAspect.h"

using namespace ls;


// Aspect
AspectInstance::AspectInstance(AspectPrototype const& prototype)
    : m_prototype(prototype)
{

}
AspectInstance::~AspectInstance()
{
}
void AspectInstance::destroy()
{
    m_prototype.destroyAspect(*this);
}
void AspectInstance::attach(ObjectInstance& object, uint slot)
{
    m_slot = slot;
    implAttach(object);
}
void AspectInstance::implAttach(ObjectInstance& /*object*/)
{
}
void AspectInstance::update(ObjectState const& /*data*/)
{
}




// Object
ObjectInstance::ObjectInstance()
{
}
ObjectInstance::ObjectInstance(ObjectInstance&& another) noexcept
: m_parent(another.m_parent)
, m_self(another.m_self)
, m_chunkWidth(another.m_chunkWidth)

, m_prototype(another.m_prototype)
, m_type(another.m_type)

, m_state(move(another.m_state))
, m_intialized(another.m_intialized)
, m_markedToRemove(another.m_markedToRemove)
, m_debugFlags(another.m_debugFlags)

, m_chunk(another.m_chunk)
, m_aspects(another.m_aspects)
{
    another.m_prototype = nullptr;
    another.m_aspects.fill(nullptr);
}
ObjectInstance::~ObjectInstance()
{
    reset();
}
void ObjectInstance::startup(ObjectObserverInterface& parent,
                             ObjectIterator self,
                             uint chunkWidth)
{
    m_parent = &parent;
    m_self = self;
    m_chunkWidth = chunkWidth;
}
void ObjectInstance::reset()
{
    if (isValid())
    {
        m_parent->onRemove(m_self);
        if (isInitalized())
            deinitialize();
    }
    m_prototype = nullptr;
    m_type = InvalidObjectType;
    m_state.reset();
    m_chunk = GhostChunk;
    m_intialized = false;
    m_markedToRemove = false;
    m_debugFlags = 0;
}
void ObjectInstance::destroy()
{
    reset();
}
void ObjectInstance::destroyLater()
{
    m_markedToRemove = true;
}
void ObjectInstance::reconstruct(ObjectState const& state)
{
    debug_assert(m_parent);
    debug_assert(m_self);
    debug_assert(state.isValid());
    debug_assert(state.isComplete());

    // Destroy
    if (isValid())
    {
        reset();
    }

    // Copy state
    m_state.~ObjectState();
    new (&m_state) ObjectState(state);

    // Init
    m_prototype = &state.prototype();
    m_type = m_prototype->type();
    m_chunk = computeChunk();
    m_markedToRemove = false;
    m_debugFlags = 0;

    // Callback
    m_parent->onAdd(id());
    m_parent->onRebind(m_self, m_chunk, m_chunk);
}
void ObjectInstance::initialize()
{
    debug_assert(isValid());
    debug_assert(!m_intialized);

    m_prototype->generate(m_aspects);
    m_intialized = true;

    // Update
    for (uint slot = 0; slot < MaxNumAspects; ++slot)
        if (m_aspects[slot])
            m_aspects[slot]->attach(*this, slot);
}
void ObjectInstance::deinitialize()
{
    debug_assert(isValid());
    debug_assert(m_intialized);

    for (AspectInstance* aspect : m_aspects)
        if (aspect)
            aspect->destroy();

    m_intialized = false;
    m_aspects.fill(nullptr);
}
void ObjectInstance::rebindObject()
{
    int2 newChunk = computeChunk();
    if (m_chunk != newChunk)
    {
        m_parent->onRebind(m_self, m_chunk, newChunk);
        m_chunk = newChunk;
    }
}
bool ObjectInstance::inject(ObjectState const& state)
{
    m_state.update(state);

    rebindObject();

    if (!m_intialized)
        return false;

    // Update
    for (AspectInstance* aspect : m_aspects)
        if (aspect)
            aspect->inject(state);
    return true;
}
bool ObjectInstance::update(ObjectState const& state)
{
    m_state.update(state);

    if (!m_intialized)
        return false;

    // Update
    for (AspectInstance* aspect : m_aspects)
        if (aspect)
            aspect->update(state);
    return true;
}
bool ObjectInstance::extract(ObjectState& state)
{
    if (!m_intialized)
        return false;

    for (AspectInstance* aspect : m_aspects)
        if (aspect)
            aspect->extract(state);
    return true;
}
AspectInstance* ObjectInstance::aspect(AspectCategory category)
{
    for (AspectInstance* aspect : m_aspects)
        if (aspect && aspect->prototype().category() == category)
            return aspect;
    return nullptr;
}


// Stuff
const double3* ObjectInstance::computePosition() const noexcept
{
    debug_assert(isValid());

    if (!m_state.hasSlot(PoseAspectSlot))
        return nullptr;
    PoseAspectState const& poseState = m_state
        .slot(PoseAspectSlot)
        .cast<PoseAspectState>();
    return &poseState.position;
}
int2 ObjectInstance::computeChunk() const noexcept
{
    const double3* pos = computePosition();
    if (!pos)
        return GhostChunk;
    double2 chunk = swiz<X, Z>(*pos) / static_cast<double>(m_chunkWidth);
    return static_cast<int2>(floor(chunk));
}
ObjectPose ObjectInstance::globalPose() const noexcept
{
    debug_assert(m_state.hasSlot(PoseAspectSlot));
    PoseAspectState const& poseState = m_state
        .slot(PoseAspectSlot)
        .cast<PoseAspectState>();
    return ObjectPose(poseState.position, poseState.rotation);
}
double3 ObjectInstance::position() const noexcept
{
    debug_assert(m_state.hasSlot(PoseAspectSlot));
    PoseAspectState const& poseState = m_state
        .slot(PoseAspectSlot)
        .cast<PoseAspectState>();
    return poseState.position;
}
void ObjectInstance::globalPose(ObjectPose const& pose) noexcept
{
    debug_assert(m_state.hasSlot(PoseAspectSlot));
    PoseAspectState& poseState = m_state
        .slot(PoseAspectSlot)
        .cast<PoseAspectState>();
    poseState.position = pose.position();
    poseState.rotation = pose.rotationQuat();
    rebindObject();
}
void ObjectInstance::position(double3 const& position) noexcept
{
    debug_assert(m_state.hasSlot(PoseAspectSlot));
    PoseAspectState& poseState = m_state
        .slot(PoseAspectSlot)
        .cast<PoseAspectState>();
    poseState.position = position;
    rebindObject();
}
