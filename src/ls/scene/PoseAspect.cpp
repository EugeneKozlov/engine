#include "pch.h"
#include "ls/scene/PoseAspect.h"
#include "ls/io/DynamicStream.h"

using namespace ls;

// Prototype
PoseAspectPrototype::PoseAspectPrototype(XmlNode aspectNode,
                                         AspectCategory category,
                                         const char* name,
                                         bool isTiny)
    : AspectPrototype(name, category, isTiny, PoseAspectSlot)
{
    const char* positionEncodingName = loadNodeAttrib(aspectNode,
                                                      "position", "type").as_string();
    const char* rotationEncodingName = loadNodeAttrib(aspectNode,
                                                      "rotation", "type").as_string();

    m_positionEncoding = encodingByName(positionEncodingName);
    m_rotationEncoding = encodingByName(rotationEncodingName);

    LS_THROW(isRealVector(m_positionEncoding),
             InvalidConfigException,
             "Invalid position [", positionEncodingName, "]");
    LS_THROW(isRotation3(m_rotationEncoding),
             InvalidConfigException,
             "Invalid rotation [", rotationEncodingName, "]");

    m_default.position = readDefault<double3, double>(aspectNode, "position");
    m_default.rotation = readDefault<quat, float>(aspectNode, "rotation");

    m_size += sizeOfObject<float3>(m_positionEncoding);
    m_size += sizeOfObject<float3>(m_rotationEncoding);
}
void PoseAspectPrototype::gatherState(AnyMap& source, AspectState& dest) const
{
    auto& state = dest.cast<PoseAspectState>();

    state.position = deref(any_cast<double3>(&source["position"]),
                           &m_default.position);
    state.rotation = deref(any_cast<quat>(&source["rotation"]),
                           &m_default.rotation);
}
void PoseAspectPrototype::encodeState(AspectState const& source,
                                      DynamicStream& dest) const
{
    auto& state = source.cast<PoseAspectState>();

    encodeObject(m_positionEncoding, state.position, dest);
    encodeObject(m_rotationEncoding, state.rotation, dest);

}
void PoseAspectPrototype::decodeState(DynamicStream& source,
                                      AspectState& dest) const
{
    auto& state = dest.cast<PoseAspectState>();

    decodeObject(m_positionEncoding, state.position, source);
    decodeObject(m_rotationEncoding, state.rotation, source);
}
void PoseAspectPrototype::interpolateState(AspectState const& left,
                                           AspectState const& right,
                                           AspectState& dest, float factor) const
{
    auto& leftState = left.cast<PoseAspectState>();
    auto& rightState = right.cast<PoseAspectState>();
    auto& destState = dest.cast<PoseAspectState>();

    destState.position = lerp(leftState.position, rightState.position, factor);
    destState.rotation = slerp(leftState.rotation, rightState.rotation, factor);
}


// Aspect
PoseAspect::PoseAspect(PoseAspectPrototype const& prototype)
    : AspectInstance(prototype)
{
}
PoseAspect::~PoseAspect()
{
}
void PoseAspect::extract(ObjectState& /*data*/)
{
}
void PoseAspect::inject(ObjectState const& data)
{
    PoseAspectState const& state = data.slot(m_slot).cast<PoseAspectState>();
    m_pose = ObjectPose(state.position, state.rotation);
}

