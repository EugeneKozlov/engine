#include "pch.h"
#include "ls/scene/ObjectState.h"
#include "ls/scene/ObjectPrototype.h"
#include "ls/scene/PoseAspect.h"

using namespace ls;


// Allocator
ObjectInfo const ObjectInfo::Invalid;
AspectBlobAllocator ObjectState::Allocator;


// Main
void ObjectState::repoint()
{
    m_data[PoseAspectSlot] = reinterpret_cast<AspectState*> (&m_poseData);
    m_data[MovableAspectSlot] = reinterpret_cast<AspectState*> (&m_movableData);
    m_data[InfoAspectSlot] = reinterpret_cast<AspectState*> (&m_infoData);
    m_data[PhysicalAspectSlot] = reinterpret_cast<AspectState*> (&m_physicalData);
}
ObjectState::ObjectState(ObjectPrototype const& prototype,
                         ObjectId objectId)
{
    construct(prototype, objectId);
}
ObjectState::ObjectState(ObjectState const& another)
    : m_prototype(another.m_prototype)
    , m_tinyFlag(another.m_tinyFlag)

    , m_complete(another.m_complete)
    , m_exist(another.m_exist)
    , m_flags(another.m_flags)
    , m_time(another.m_time)

    , m_id(another.m_id)
    , m_type(another.m_type)

    , m_poseData(another.m_poseData)
    , m_movableData(another.m_movableData)
    , m_infoData(another.m_infoData)
    , m_physicalData(another.m_physicalData)

    , m_data(nullptr)
{
    if (isTiny())
    {
        repoint();
    }
    else
    {
        for (uint slot = 0; slot < MaxNumAspects; ++slot)
        {
            if (another.m_data[slot])
            {
                void* storage = Allocator.allocate(AspectStateSize);
                m_data[slot] = new(storage) AspectState(*another.m_data[slot]);
            }
        }
    }
}
ObjectState::ObjectState(ObjectState&& another) noexcept
: m_prototype(another.m_prototype)
, m_tinyFlag(another.m_tinyFlag)

, m_complete(another.m_complete)
, m_exist(another.m_exist)
, m_flags(another.m_flags)
, m_time(another.m_time)

, m_id(another.m_id)
, m_type(another.m_type)

, m_poseData(another.m_poseData)
, m_movableData(another.m_movableData)
, m_infoData(another.m_infoData)
, m_physicalData(another.m_physicalData)

, m_data(another.m_data)
{
    if (isTiny())
    {
        repoint();
    }
    else
    {
        another.m_data.fill(nullptr);
    }
}
void ObjectState::reset() noexcept
{
    m_prototype = nullptr;
    m_tinyFlag = true;

    m_complete = false;
    m_exist = false;
    m_flags = 0;
    m_time = 0;

    m_id = InvalidObjectId;
    m_type = InvalidObjectType;

    m_data.fill(nullptr);
}
void ObjectState::construct(ObjectPrototype const& prototype,
                            ObjectId objectId)
{
    m_prototype = &prototype;
    m_tinyFlag = prototype.isTiny();

    m_complete = false;
    m_exist = false;
    m_flags = 0;
    m_time = 0;

    m_id = objectId;
    m_type = prototype.type();
    m_time = 0;

    m_data.fill(nullptr);
    if (isTiny())
    {
        repoint();
    }
}
void ObjectState::update(ObjectState const& another)
{
    // Check
    debug_assert(m_id == another.m_id);
    debug_assert(m_tinyFlag == another.m_tinyFlag);
    debug_assert(m_prototype == another.m_prototype);

    // Copy tiny
    if (another.hasSlot(PoseAspectSlot))
        m_poseData = another.m_poseData;
    if (another.hasSlot(MovableAspectSlot))
        m_movableData = another.m_movableData;
    if (another.hasSlot(InfoAspectSlot))
        m_infoData = another.m_infoData;
    if (another.hasSlot(PhysicalAspectSlot))
        m_physicalData = another.m_physicalData;

    // Copy fat
    for (uint slot = CustomAspectSlot; slot < MaxNumAspects; ++slot)
    {
        if (!another.hasSlot(slot))
            continue;

        setSlot(slot);
        copy_n(another.m_data[slot]->data(), AspectStateSize, m_data[slot]->data());
    }

    // Copy info
    m_flags |= another.m_flags;
    m_complete = m_flags == m_prototype->flags();
    m_exist = another.m_exist;
    m_time = another.m_time;
}



// Update
void ObjectState::resetTime(DiscreteTime time) noexcept
{
    m_time = time;
}
void ObjectState::resupTime(DiscreteTime time) noexcept
{
    m_time = max(m_time, time);
}
void ObjectState::setExistence(bool isExist) noexcept
{
    m_exist = isExist;
}
void ObjectState::materialize() noexcept
{
    m_exist = true;
}
void ObjectState::dematerialize() noexcept
{
    m_exist = false;
}
AspectState& ObjectState::setSlot(uint slot)
{
    debug_assert(slot < MaxNumAspects);
    m_flags |= 1 << slot;
    m_complete = m_flags == m_prototype->flags();

    // Allocate blob
    if (!m_data[slot])
    {
        debug_assert(!isTiny());
        void* storage = Allocator.allocate(AspectStateSize);
        AspectState* blob = new(storage) AspectState();
        m_data[slot] = blob;
    }

    return *m_data[slot];
}
void ObjectState::unsetSlot(uint slot)
{
    debug_assert(slot < MaxNumAspects);
    m_flags &= ~(1 << slot);
    m_complete = m_flags == m_prototype->flags();

    // Deallocate blob
    if (m_data[slot] && !isTiny())
    {
        Allocator.destroy(m_data[slot]);
        Allocator.deallocate(m_data[slot], AspectStateSize);

        m_data[slot] = nullptr;
    }
}

