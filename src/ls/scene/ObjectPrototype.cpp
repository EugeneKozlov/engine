#include "pch.h"
#include "ls/scene/ObjectPrototype.h"
#include "ls/scene/ObjectState.h"
#include "ls/scene/ObjectInstance.h"
#include "ls/io/DynamicStream.h"
// Aspects
#include "ls/scene/PoseAspect.h"
#include "ls/scene/MovableAspect.h"
#include "ls/scene/InfoAspect.h"
#include "ls/scene/PhysicalAspect.h"
#include "ls/scene/RigidAspect.h"

using namespace ls;


// Aspect
void AspectPrototype::copyState(AspectState& dest, AspectState const& source) const
{
    copy_n(source.data(), size(), dest.data());
}
void AspectPrototype::describeState(AnyMap& source, DynamicStream& dest) const
{
    AspectState state;
    gatherState(source, state);
    encodeState(state, dest);
}
void AspectPrototype::nearestState(AspectState const& left,
                                   AspectState const& right,
                                   AspectState& dest, float factor) const
{
    static auto const k = 1.0f - numeric_limits<float>::epsilon() * 10;
    if (factor < k)
        copyState(dest, left);
    else
        copyState(dest, right);
}
void AspectPrototype::interpolateState(AspectState const& left,
                                       AspectState const& right,
                                       AspectState& dest, float factor) const
{
    nearestState(left, right, dest, factor);
}


// Prototype
ObjectPrototype::ObjectPrototype(XmlNode node, ObjectType type)
    : m_type(type)
{
    // Aspects
    static const map<string, Contructor> constuctorMap = {
        aspectConstuctor<PoseAspectPrototype>(),
        aspectConstuctor<MovableAspectPrototype>(),
        aspectConstuctor<InfoAspectPrototype>(),
        aspectConstuctor<PhysicalAspectPrototype>(),

        aspectConstuctor<RigidAspectPrototype>()
    };
    uint aspectIndex = CustomAspectSlot;
    bool mayTiny = true;
    for (XmlNode nodeAspect : node)
    {
        const char* name = nodeAspect.name();
        if (name[0] == 'z')
            continue;

        auto iterConstructor = findOrDefault(constuctorMap, name);
        LS_THROW(iterConstructor,
                 InvalidConfigException,
                 "Unknown aspect [", name, "]");

        auto aspect = (*iterConstructor)(nodeAspect);
        uint defaultSlot = aspect->defaultSlot();
        bool tinyAspect = aspect->isTiny();

        if (defaultSlot == MaxNumAspects)
        {
            debug_assert(tinyAspect);
            mayTiny = false;

            // Add or fail
            LS_THROW(aspectIndex < MaxNumAspects,
                     InvalidConfigException,
                     "Too many aspects");
            defaultSlot = aspectIndex++;
        }
        else
        {
            if (!tinyAspect)
                mayTiny = false;

            // Set or fail
            AspectPrototype* another = m_aspects[defaultSlot].get();
            LS_THROW(!another,
                     InvalidConfigException,
                     "Aspect [", name, "] conflicts with [", another->name());
        }

        // Set
        m_size += aspect->size();
        m_flags |= 1 << defaultSlot;
        m_aspects[defaultSlot] = move(aspect);
    }

    // Flags
    auto tinyFlag = [this, mayTiny](XmlNode /*node*/)
    {
        LS_THROW(mayTiny,
                 InvalidConfigException,
                 "Object cannot be tiny");

        m_tinyFlag = true;
    };
    auto globalFlag = [this](XmlNode /*node*/)
    {
        m_globalFlag = true;
    };
    static const map<string, function<void(XmlNode node) >> flagMap = {
        flagInitializer("tiny", tinyFlag),
        flagInitializer("global", globalFlag)
    };
    for (XmlNode nodeFlag : node)
    {
        const char* name = nodeFlag.name();
        if (name[0] != 'z')
            continue;
        ++name;

        auto iterFlag = findOrDefault(flagMap, name);
        LS_THROW(iterFlag,
                 InvalidConfigException,
                 "Unknown flag [", name, "]");

        (*iterFlag)(nodeFlag);
    }
}
ObjectPrototype::ObjectPrototype(ObjectPrototype&& another) noexcept
: m_type(another.m_type)
, m_size(another.m_size)

, m_tinyFlag(another.m_tinyFlag)
, m_globalFlag(another.m_globalFlag)
, m_flags(another.m_flags)

, m_aspects(move(another.m_aspects))
{
}
ObjectPrototype::~ObjectPrototype()
{
}
void ObjectPrototype::generate(array<AspectInstance*, MaxNumAspects>& aspects) const
{
    for (uint slot = 0; slot < MaxNumAspects; ++slot)
    {
        AspectPrototype* proto = m_aspects[slot].get();
        if (proto)
            aspects[slot] = &proto->constructAspect();
        else
            aspects[slot] = nullptr;
    }
}
template <class F>
bool ObjectPrototype::foreach(F& foo)
{
    return foreach(foo, m_flags);
}
template <class F>
bool ObjectPrototype::foreach(F& foo, ObjectStateFlag flags)
{
    for (uint index = 0; index < MaxNumAspects; ++index)
        if ((flags & (1 << index)) != 0)
        {
            debug_assert(m_aspects[index]);
            if (!foo(index, *m_aspects[index]))
                return false;
        }
    return true;
}


// Object encoding
uint ObjectPrototype::size(ObjectStateFlag flags)
{
    uint flagSize = 0;
    auto foo = [this, &flagSize](uint /*index*/, AspectPrototype & proto)
    {
        flagSize += proto.size();
        return true;
    };
    foreach(foo, flags);
    return flagSize;
}
void ObjectPrototype::gather(ObjectState& state, ObjectId id, AnyMap& parameters)
{
    state.construct(*this, id);
    auto foo = [this, &parameters, &state](uint index, AspectPrototype & proto)
    {
        AspectState& slot = state.setSlot(index);
        proto.gatherState(parameters, slot);
        return true;
    };
    foreach(foo);
}
void ObjectPrototype::describe(DynamicStream& stream, AnyMap& parameters)
{
    uint startSize = stream.numWritten();
    auto foo = [this, &parameters, &stream](uint /*index*/, AspectPrototype & proto)
    {
        proto.describeState(parameters, stream);
        return true;
    };
    foreach(foo);

    uint size = stream.numWritten() - startSize;
    debug_assert(size == m_size, size, " bytes instead of ", m_size);
}
bool ObjectPrototype::encode(ObjectState const& objectState,
                             const ObjectState* defaultState,
                             DynamicStream& stream,
                             ObjectStateFlag flags, uint checkSize)
{
    uint startSize = stream.numWritten();
    auto foo = [&](uint index, AspectPrototype & proto)
    {
        bool isContained = objectState.hasSlot(index);
        bool hasDefault = !!defaultState && defaultState->hasSlot(index);
        // Fail if needed and invalid
        bool isValid = isContained || hasDefault;
        if (!isValid)
            return false;
        // Encode
        const ObjectState* source = isContained ? &objectState : defaultState;
        AspectState const& state = source->slot(index);
        proto.encodeState(state, stream);
        return true;
    };
    if (!foreach(foo, flags))
        return false;

    uint size = stream.numWritten() - startSize;
    debug_assert(size == checkSize,
                 size, " instead of ", checkSize);
    return true;
}
void ObjectPrototype::decode(ObjectState& objectState, DynamicStream& stream,
                             ObjectStateFlag flags,
                             uint checkSize)
{
    uint startSize = stream.numRead();
    auto foo = [&stream, &objectState](uint index, AspectPrototype & proto)
    {
        AspectState& state = objectState.setSlot(index);
        proto.decodeState(stream, state);
        return true;
    };
    foreach(foo, flags);
    uint size = stream.numRead() - startSize;
    debug_assert(size == checkSize,
                 size, " instead of ", checkSize);
}
