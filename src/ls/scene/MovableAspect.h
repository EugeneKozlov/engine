/** @file ls/scene/MovableAspect.h
 *  @brief Movable aspect
 */

#pragma once
#include "ls/common.h"
#include "ls/scene/ObjectInstance.h"
#include "ls/scene/ObjectPrototype.h"
#include "ls/io/Codec.h"
#include "ls/xml/common.h"

namespace ls
{
    /** @addtogroup LsScene
     *  @{
     */
    /** Movable aspect prototype
     */
    class MovableAspectPrototype
    : public AspectPrototype
    {
        LS_ASPECT(MovableAspect);
    public:
        /// Category
        static constexpr AspectCategory Category = AspectCategory::Unspecified;
        /// Aspect name
        static constexpr const char* Name()
        {
            return "movable";
        }
        /// Ctor
        MovableAspectPrototype(XmlNode aspectNode,
                               AspectCategory category = Category,
                               const char* name = Name(),
                               bool isTiny = true);
    public:
        virtual void gatherState(AnyMap& source, AspectState& dest) const override;
        virtual void encodeState(AspectState const& source, DynamicStream& dest) const override;
        virtual void decodeState(DynamicStream& source, AspectState& dest) const override;
        virtual void interpolateState(AspectState const& left, AspectState const& right,
                                      AspectState& dest, float factor) const override;

    private:
        Encoding m_velocityEncoding;
        Encoding m_torqueEncoding;
        MovableAspectState m_default;
    };
    /** Movable aspect
     */
    class MovableAspect
    : public AspectInstance
    {
    public:
        /** Ctor
         *  @param prototype
         *    Aspect prototype
         */
        MovableAspect(MovableAspectPrototype const& prototype);
        /// Dtor
        virtual ~MovableAspect();

        /** @name Gets
         *  @{
         */
        /// @}
    public:
        virtual void extract(ObjectState& data) override;
        virtual void inject(ObjectState const& data) override;
    protected:
    };
    /// @}
}

