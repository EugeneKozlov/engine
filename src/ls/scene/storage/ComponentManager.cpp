#include "pch.h"
#include "ls/scene/storage/ComponentManager.h"
#include "ls/scene/component/all.h"

using namespace ls;
ComponentInterface::~ComponentInterface()
{
}
ComponentManager::ComponentManager()
{
    m_interfaces.push_back(nullptr);
    iterate<1>();
}
ComponentManager::~ComponentManager()
{
}
const ComponentManager& ComponentManager::instance()
{
    static ComponentManager m_instance;
    return m_instance;
}
const ComponentInterface& ComponentManager::wrap(ComponentType type) const
{
    const uint idx = static_cast<uint>(type);
    debug_assert(idx < m_interfaces.size());
    return *m_interfaces[idx];
}
namespace ls
{
    template <uint N>
    void ComponentManager::iterate()
    {
        m_interfaces.emplace_back(make_unique<ComponentInterfaceT<(ComponentType) N>>());
        iterate<N + 1>();
    }
    template <>
    void ComponentManager::iterate<NumComponentTypes>()
    {
    }
}
