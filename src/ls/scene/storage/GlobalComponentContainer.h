/// @file ls/scene/GlobalComponentContainer.h
/// @brief Global Component Container
#pragma once

#include "ls/common.h"
#include "ls/scene/Component.h"
#include "ls/scene/storage/EntityPool.h"

namespace ls
{
    /// @addtogroup LsScene
    /// @{

    /// @brief Global Component Container
    class GlobalComponentContainer
        : Noncopyable
    {
    public:
        /// @brief Ctor
        GlobalComponentContainer();
        /// @brief Dtor
        ~GlobalComponentContainer();
        /// @brief Release all entities
        void release();
        /// @brief Serialize
        void serialize(Buffer& dest);
        /// @brief Deserialize
        void deserialize(Buffer& src);

        /// @brief Create component
        /// @throw OverflowException if too many shared components
        Component& createComponent(const ComponentType type);
        /// @brief Find component
        /// @note Thread-safe
        Component* findComponent(const ComponentId id);

        /// @brief Get begin
        vector<Component*>::iterator begin();
        /// @brief Get end
        vector<Component*>::iterator end();
    private:
        EntityPool m_pool;
        vector<Component*> m_components;
    };

    /// @}
}