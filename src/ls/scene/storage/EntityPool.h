/// @file ls/scene/EntityPool.h
/// @brief Entity Pool
#pragma once

#include "ls/common.h"
#include "ls/scene/Component.h"
#include "ls/scene/Entity.h"

namespace ls
{
    /// @addtogroup LsScene
    /// @{

    /// @brief Entity Pool
    class EntityPool
        : Noncopyable
    {
    public:
        /// @brief Ctor
        EntityPool();
        /// @brief Dtor
        ~EntityPool();
        /// @brief Allocate entity
        Entity& allocateEntity(const EntityId id);
        /// @brief Deallocate entity
        void deallocateEntity(Entity& entity);
        /// @brief Allocate component
        Component& allocateComponent(const ComponentType type, 
                                     const ComponentId id = InvalidComponentId);
        /// @brief Deallocate component
        void deallocateComponent(Component& component);
    private:
        list<boost::pool<>> m_poolsList;
        array<boost::pool<>*, NumComponentTypes> m_pools;
    };

    /// @}
}