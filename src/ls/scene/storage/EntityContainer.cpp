#include "pch.h"
#include "ls/scene/storage/EntityContainer.h"
#include "ls/io/DynamicStream.h"
#include "ls/io/Codec.h"

using namespace ls;
// Main
EntityContainer::EntityContainer(GlobalComponentContainer& globalComponents)
    : m_globalComponents(globalComponents)
{
    
}
EntityContainer::~EntityContainer()
{
    release();
}


// I/O
void EntityContainer::release()
{
    // Release entities
    for (Entity* entity : m_entities)
    {
        if (entity)
            m_pool.deallocateEntity(*entity);
    }
    m_entities.clear();

    // Release IDs
    m_ids.reset();
}
void EntityContainer::serialize(Buffer& dest, const double sortGridCell)
{
    // Sort
    if (sortGridCell > 0.0)
    {
        // #TODO add sort
    }

    // Write
    const uint numEntities = m_entities.size()
        - std::count(m_entities.begin(), m_entities.end(), nullptr);
    encode<Encoding::Int>(dest, numEntities);

    uint numActualEntities = 0;
    for (Entity* entity : m_entities)
    {
        if (!entity)
            continue;
        ++numActualEntities;

        // Save entity
        const EntityId id = entity->id(); 
        encode<Encoding::Int>(dest, id);
        entity->serialize(dest);
    }

    // Check
    debug_assert(numActualEntities == numEntities);
}
void EntityContainer::deserialize(Buffer& src)
{
    // Release all
    release();
    if (src.avalilable() == 0)
    {
        return;
    }

    // Read number of entities
    uint numEntities;
    decode<Encoding::Int>(src, numEntities);

    // Read entities
    m_entities.resize(numEntities);
    for (uint idx = 0; idx < numEntities; ++idx)
    {
        // Read header
        EntityId id;
        decode<Encoding::Int>(src, id);

        // Read entity
        Entity& entity = m_pool.allocateEntity(id);
        entity.sortIndex = idx;
        entity.deserialize(src, m_globalComponents);
        m_entities[id] = &entity;
    }

    // Update IDs
    for (EntityId id = 0; id < m_entities.size(); ++id)
    {
        EntityId refernceId = m_ids.generateNext();
        debug_assert(refernceId == id);
    }
    for (EntityId id = 0; id < m_entities.size(); ++id)
    {
        if (m_entities[id])
            continue;
        m_ids.release(id);
    }
    m_ids.synchronize();
}
void EntityContainer::synchronize()
{
    m_ids.synchronize();
}


// Regions
Entity& EntityContainer::createEntity()
{
    // Find ID
    EntityId id = m_ids.generate();

    // Allocate entity
    m_entities[id] = &m_pool.allocateEntity(id);

    return *m_entities[id];
}
void EntityContainer::destroyEntity(Entity& entity)
{
    const EntityId id = entity.id();
    debug_assert(id < m_entities.size(), "out of range entity ID: ", id);
    debug_assert(m_entities[id] == &entity, "entity mismatch ID: ", id);

    // Deallocate entity
    m_pool.deallocateEntity(entity);

    // Reset info
    m_entities[id] = nullptr;
}
Entity* EntityContainer::findEntity(EntityId entityId)
{
    if (entityId >= m_entities.size())
        return nullptr;
    return m_entities[entityId];
}

