#include "pch.h"
#include "ls/io/Codec.h"
#include "ls/io/StreamInterface.h"
#include "ls/scene/storage/ComponentManager.h"
#include "ls/scene/storage/GlobalComponentContainer.h"

using namespace ls;
GlobalComponentContainer::GlobalComponentContainer()
{

}
GlobalComponentContainer::~GlobalComponentContainer()
{
    release();
}        
void GlobalComponentContainer::release()
{
    for (Component* component : m_components)
    {
        m_pool.deallocateComponent(*component);
    }
    m_components.clear();
}
void GlobalComponentContainer::serialize(Buffer& dest)
{
    encode<Encoding::Int>(dest, m_components.size());
    for (Component* component : m_components)
    {
        ComponentType type = component->type();
        encode<Encoding::Short>(dest, static_cast<ushort>(type));
        ComponentManager::serialize(type, *component, 
                                    Archive<Buffer>(dest, ArchiveWrite));
    }
}
void GlobalComponentContainer::deserialize(Buffer& src)
{
    release();

    uint numComponents;
    decode<Encoding::Int>(src, numComponents);
    m_components.resize(numComponents);
    for (uint idx = 0; idx < numComponents; ++idx)
    {
        ushort encType;
        decode<Encoding::Short>(src, encType);
        ComponentType type = static_cast<ComponentType>(encType);
        Component& component = m_pool.allocateComponent(type, idx);
        ComponentManager::serialize(type, component, 
                                    Archive<Buffer>(src, ArchiveRead));
        m_components[idx] = &component;
    }
}
Component& GlobalComponentContainer::createComponent(const ComponentType type)
{
    LS_THROW(m_components.size() < InvalidComponentId,
             OverflowException, 
             "Too may shared components");

    const ComponentId id = static_cast<ComponentId>(m_components.size());
    Component& dest = m_pool.allocateComponent(type, id);
    m_components.push_back(&dest);
    return dest;
}
Component* GlobalComponentContainer::findComponent(const ComponentId id)
{
    if (id >= m_components.size())
        return nullptr;
    return m_components[id];
}
vector<Component*>::iterator GlobalComponentContainer::begin()
{
    return m_components.begin();
}
vector<Component*>::iterator GlobalComponentContainer::end()
{
    return m_components.end();
}