/// @file ls/scene/storage/EntityIdGenerator.h
/// @brief Entity Id Generator
#pragma once

#include "ls/common.h"
#include "ls/scene/Entity.h"

namespace ls
{
    /// @addtogroup LsScene
    /// @{

    /// @brief Entity Id Generator
    /// 
    /// Generated IDs are fully deterministic if:
    /// + order of @a synchronize, @a createEntity and @a destroyEntity is preserved
    /// + @a synchronize call should be first after deserialization
    class EntityIdGenerator
    {
    public:
        /// @brief Reset
        void reset()
        {
            m_nextId = InvalidEntityId;
            m_freeIds.clear();
        }
        /// @brief Generate ID after all
        EntityId generateNext()
        {
            return m_nextId++;
        }
        /// @brief Generate ID (any)
        EntityId generate()
        {
            // Get next
            if (m_freeIds.empty())
            { 
                return m_nextId++;
            }

            // Get free
            EntityId id = m_freeIds.back();
            m_freeIds.pop_back();
            return id + 1;
        }
        /// @brief Release ID
        void release(EntityId id)
        {
            m_freeIds.push_back(id - 1);
        }
        /// @brief Synchronize
        void synchronize()
        {
            if (m_freeIds.empty())
                return;
            std::sort(m_freeIds.begin(), m_freeIds.end());
            while (m_freeIds.back() == m_nextId)
            {
                m_freeIds.pop_back();
                --m_nextId;
            }
        }
    private:
        EntityId m_nextId = InvalidEntityId; ///< Next free ID
        std::vector<EntityId> m_freeIds; ///< Free IDs
    };

    /// @}
}