#include "pch.h"
#include "ls/scene/component/all.h"
#include "ls/scene/storage/ComponentManager.h"
#include "ls/scene/storage/EntityPool.h"

using namespace ls;
EntityPool::EntityPool()
{
    m_poolsList.emplace_back(sizeof(Entity));
    m_pools[0] = &m_poolsList.back();
    for (uint idx = 1; idx < NumComponentTypes; ++idx)
    {
        m_poolsList.emplace_back(ComponentManager::size((ComponentType) idx));
        m_pools[idx] = &m_poolsList.back();
    }
}
EntityPool::~EntityPool()
{

}
Entity& EntityPool::allocateEntity(const EntityId id)
{
    void* buffer = m_pools[0]->malloc();
    debug_assert(buffer);
    return *new(buffer) Entity(id, *this);
}
void EntityPool::deallocateEntity(Entity& entity)
{
    entity.~Entity();
    m_pools[0]->free(&entity);
}
Component& EntityPool::allocateComponent(const ComponentType type, const ComponentId id)
{
    debug_assert(type != ComponentType::Unknown && type < ComponentType::COUNT);
    void* buffer = m_pools[static_cast<uint>(type)]->malloc();
    Component& component = *static_cast<Component*>(buffer);
    ComponentManager::construct(type, component, id);
    return component;
}
void EntityPool::deallocateComponent(Component& component)
{
    const ComponentType type = component.type();
    ComponentManager::destroy(type, component);
    void* buffer = &component;
    m_pools[static_cast<uint>(type)]->free(buffer);
}
