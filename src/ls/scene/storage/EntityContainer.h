/// @file ls/scene/storage/DynamicEntityContainer.h
/// @brief Entity Container
#pragma once

#include "ls/common.h"
#include "ls/core/AutoExpandingVector.h"
#include "ls/core/StackAllocator.h"
#include "ls/scene/Entity.h"
#include "ls/scene/storage/EntityIdGenerator.h"
#include "ls/scene/storage/EntityPool.h"

namespace ls
{
    class GlobalComponentContainer;

    /// @addtogroup LsScene
    /// @{

    /// @brief Entity Container
    class EntityContainer
        : Noncopyable
    {
    public:
        /// @brief Ctor
        EntityContainer(GlobalComponentContainer& globalComponents);
        /// @brief Dtor
        ~EntityContainer();
        /// @brief Release all entities
        void release();
        /// @brief Serialize
        /// @param sortGridCell
        ///   Set sort grid cell size. Keep 0 to disable
        ///   Objects will be chunked by this grid and sorted before serialization.
        void serialize(Buffer& dest, const double sortGridCell = 0.0);
        /// @brief Deserialize
        void deserialize(Buffer& src);
        /// @brief Sorts free object IDs
        void synchronize();

        /// @brief Create entity
        Entity& createEntity();
        /// @brief Destroy entity
        void destroyEntity(Entity& entity);
        /// @brief Find entity by ID, return null if not found
        Entity* findEntity(EntityId entityId);

        /// @brief Begin
        auto begin()
        {
            return m_entities.cbegin();
        }
        /// @brief End
        auto end()
        {
            return m_entities.cend();
        }
    private:
        GlobalComponentContainer& m_globalComponents; ///< Global components pool

        EntityPool m_pool; ///< Pool
        AutoExpandingVector<Entity*> m_entities; ///< Entities
        EntityIdGenerator m_ids; ///< ID generator
    };
    /// @brief Shared Entity Container
    using SharedEntityContainer = shared_ptr<EntityContainer>;

    /// @}
}