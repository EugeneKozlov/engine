/// @file ls/scene/storage/ComponentManager.h
/// @brief Component Manager
#pragma once

#include "ls/common.h"
#include "ls/scene/Component.h"

namespace ls
{
    class DynamicStream;

    /// @addtogroup LsScene
    /// @{

    /// @brief Component Interface
    class ComponentInterface
    {
    public:
        /// @brief Dtor
        virtual ~ComponentInterface();
        /// @brief Get size
        virtual uint size() const = 0;
        /// @brief Copy component
        virtual void copy(Component& dest, const Component& src) const = 0;
        /// @brief Construct
        /// @note Do not call except from @a EntityPool
        virtual void construct(Component& component, const ComponentId id) const = 0;
        /// @brief Serialize
        virtual void serialize(Component& component, Archive<Buffer> archive) const = 0;
        /// @brief Destroy
        /// @note Do not call except from @a EntityPool
        virtual void destroy(Component& component) const = 0;
    };
    /// @brief Component Interface Impl
    template <ComponentType Type>
    class ComponentInterfaceT final 
        : public ComponentInterface
    {
        using DerivedComponent = ComponentByTypeT<Type>;
        virtual uint size() const
        {
            return sizeof(DerivedComponent);
        }
        virtual void copy(Component& dest, const Component& src) const override
        {
            DerivedComponent::copy(*dest.cast<DerivedComponent>(), 
                                   *src.cast<DerivedComponent>());
        }
        virtual void construct(Component& component, const ComponentId id) const override
        {
            new (&component) DerivedComponent(id);
        }
        virtual void serialize(Component& component, Archive<Buffer> archive) const override
        {
            component.cast<DerivedComponent>()->serialize(archive);
        }
        virtual void destroy(Component& component) const override
        {
            component.cast<DerivedComponent>()->~DerivedComponent();
        }
    };
    /// @brief Component Manager
    class ComponentManager
    {
    public:
        /// @brief Ctor
        ComponentManager();
        /// @brief Dtor
        ~ComponentManager();
        /// @brief Get instance
        static const ComponentManager& instance();
        /// @brief Get interface
        const ComponentInterface& wrap(ComponentType type) const;
    public:
        /// @brief Get size
        static uint size(ComponentType type)
        {
            return instance().wrap(type).size();
        }
        /// @brief Copy component, convert shared to prototyped
        static void copy(ComponentType type, Component& dest, const Component& src)
        {
            instance().wrap(type).copy(dest, src);
        }
        /// @brief Construct
        static void construct(ComponentType type, Component& component, const ComponentId id)
        {
            instance().wrap(type).construct(component, id);
        }
        /// @brief Serialize
        static void serialize(ComponentType type, Component& component, Archive<Buffer> archive)
        {
            instance().wrap(type).serialize(component, archive);
        }
        /// @brief Destroy
        static void destroy(ComponentType type, Component& component)
        {
            instance().wrap(type).destroy(component);
        }
    private:
        template <uint N>
        void iterate();
        vector<unique_ptr<ComponentInterface>> m_interfaces;
    };

    /// @}
}