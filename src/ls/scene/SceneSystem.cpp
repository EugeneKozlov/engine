#include "pch.h"
#include "ls/scene/SceneSystem.h"

#include "ls/asset/WorldAsset.h"

using namespace ls;
SceneSystem::SceneSystem()
{

}
SceneSystem::~SceneSystem()
{

}


// Frame info
void SceneSystem::setLastFrameTime(const float deltaSec, const DeltaTime deltaMsec)
{
    m_lastElapsedSec = deltaSec;
    m_lastElapsedMsec = deltaMsec;
}


// Dummies
SceneSystemCache SceneSystem::prepareStaticEntities(const EntitiesList& entities,
                                                    WorldRegion& region)
{
    // Do nothing by default
    return nullptr;
}
void SceneSystem::addStaticEntities(const EntitiesList& entities,
                                    WorldRegion& region,
                                    SceneSystemCache /*preparedData*/)
{
    vector<Entity*>& list = m_staticEntities[region.index()];
    for (const auto& entity : entities)
    {
        list.push_back(entity.first);
        addEntity(*entity.first, *entity.second);
    }
}
void SceneSystem::removeStaticEntities(const int2& regionIndex)
{
    vector<Entity*>& list = m_staticEntities[regionIndex];
    for (Entity* entity : list)
    {
        removeEntity(*entity);
    }

    m_staticEntities.erase(regionIndex);
}
void SceneSystem::syncUpdate()
{
}
void SceneSystem::asyncUpdate()
{
}


// Gets
uint SceneSystem::key() const
{
    return static_cast<uint>(keyComponent());
}
float SceneSystem::elapsedSec() const
{
    return m_lastElapsedSec;
}
DeltaTime SceneSystem::elapsedMsec() const
{
    return m_lastElapsedMsec;
}