/** @file ObjectPrototype.h
 *  @brief Object prototype
 */

#pragma once
#include "ls/common.h"
#include "ls/forward.h"
#include "ls/scene/common.h"
#include "ls/xml/common.h"

namespace ls
{
    class DynamicStream;

    /** @addtogroup LsScene
     *  @{
     */
    /** Object aspect prototype
     */
    class AspectPrototype
    {
    protected:
        /// Ctor
        AspectPrototype(const char* name, AspectCategory category,
                        bool isTiny,
                        uint defaultSlot = MaxNumAspects) noexcept
        : m_name(name)
        , m_category(category)
        , m_tiny(isTiny)
        , m_defaultSlot(defaultSlot)
        {
        }
    public:
        /// Dtor
        virtual ~AspectPrototype() = default;

        /** @name Gets
         *  @{
         */
        /// Get name
        const char* name() const noexcept
        {
            return m_name;
        }
        /// Get category
        AspectCategory category() const noexcept
        {
            return m_category;
        }
        /// Test if tiny
        bool isTiny() const noexcept
        {
            return m_tiny;
        }
        /// State size
        uint size() const noexcept
        {
            return m_size;
        }
        /// Get default slot
        uint defaultSlot() const noexcept
        {
            return m_defaultSlot;
        }
        /// @}

        /** @name Instance
         *  @{
         */
        /// Construct aspect
        template <class Prototype, class Aspect>
        AspectInstance& construct() const
        {
            AspectAllocator<Aspect> allocator;
            const Prototype* prototype = dynamic_cast<const Prototype*>(this);
            debug_assert(prototype, "Aspect type mismatch");

            Aspect* aspect = allocator.allocate(sizeof (Aspect));
            new (aspect) Aspect(*prototype);
            return *aspect;
        }
        /// Destroy aspect
        template <class Prototype, class Aspect>
        void destroy(AspectInstance& aspect) const
        {
            AspectAllocator<Aspect> allocator;
            Aspect* impl = dynamic_cast<Aspect*> (&aspect);

            debug_assert(impl, "Aspect type mismatch");

            allocator.destroy(impl);
            allocator.deallocate(impl, sizeof (Aspect));
        }
        /** Construct new aspect
         *  @return Aspect instance
         */
        virtual AspectInstance& constructAspect() const = 0;
        /** Destroy aspect instance
         *  @param aspect
         *    Aspect instance
         */
        virtual void destroyAspect(AspectInstance& aspect) const = 0;
        /// @}

        /** @name State
         *  @{
         */
        /// Copy state
        virtual void copyState(AspectState& dest, AspectState const& source) const;
        /// Read @a state from @a parameterMap
        virtual void gatherState(AnyMap& source, AspectState& dest) const = 0;
        /// Read state from @a parameterMap and encode it to @a stream
        virtual void describeState(AnyMap& source, DynamicStream& dest) const;
        /// Encode @a state to @a stream
        virtual void encodeState(AspectState const& source,
                                 DynamicStream& dest) const = 0;
        /// Decode @a state from @a stream
        virtual void decodeState(DynamicStream& source,
                                 AspectState& dest) const = 0;
        /// Raw and fast interpolation
        virtual void nearestState(AspectState const& left, AspectState const& right,
                                  AspectState& dest, float factor) const;
        /// Full and precise interpolation
        virtual void interpolateState(AspectState const& left, AspectState const& right,
                                      AspectState& dest, float factor) const;
        /// @}
    protected:
        /// Read default value of @a attribute from @a node
        template <class Object, class Element>
        Object readDefault(XmlNode node, const char* attribute,
                           Object const& def = Object())
        {
            XmlAttribute src = node.child(attribute).attribute("default");
            if (src.empty())
                return def;
            else
                return parseString<Object, Element>(src.value());
        }
    protected:
        /// Aspect name
        const char* m_name;
        /// Category
        AspectCategory m_category;
        /// State size
        uint m_size = 0;
        /// Is tiny?
        bool m_tiny;
        /// Default slot
        uint m_defaultSlot = MaxNumAspects;
    };
    /** Object prototype
     */
    class ObjectPrototype
    : public Noncopyable
    {
    public:
        /// Ctor
        ObjectPrototype(XmlNode node, ObjectType type);
        /// Move
        ObjectPrototype(ObjectPrototype && another) noexcept;
        /// Dtor
        ~ObjectPrototype();
        /// Generate aspects array
        void generate(array<AspectInstance*, MaxNumAspects>& aspects) const;

        /** @name Objects
         *  @{
         */
        /// Get object size
        uint size(ObjectStateFlag flags);
        /// Gather object by map
        void gather(ObjectState& state, ObjectId id, AnyMap& parameters);
        /// Describe object by map
        void describe(DynamicStream& stream, AnyMap& parameters);
        /// Encode object
        bool encode(ObjectState const& objectState,
                    const ObjectState* defaultState,
                    DynamicStream& stream, ObjectStateFlag flags,
                    uint checkSize);
        /// Decode object
        void decode(ObjectState& objectState, DynamicStream& stream,
                    ObjectStateFlag flags,
                    uint checkSize);
        /// @}

        /** @name Gets
         *  @{
         */
        /// Get type
        ObjectType type() const noexcept
        {
            return m_type;
        }
        /// Get size
        uint size() const noexcept
        {
            return m_size;
        }
        /// Get state flags
        ObjectStateFlag flags() const noexcept
        {
            return m_flags;
        }
        /// Test if tiny
        bool isTiny() const noexcept
        {
            return m_tinyFlag;
        }
        /// Test if global
        bool isGlobal() const noexcept
        {
            return m_globalFlag;
        }
        /// @}
    private:
        template <class Aspect>
        static unique_ptr<AspectPrototype> create(XmlNode node)
        {
            auto aspect = static_cast<AspectPrototype*> (new Aspect(node));
            return unique_ptr<AspectPrototype>(aspect);
        }
        using Contructor = function<unique_ptr<AspectPrototype>(XmlNode)>;
        template <class A>
        static pair<const string, Contructor> aspectConstuctor()
        {
            auto foo = bind(&ObjectPrototype::create<A>, _1);
            return make_pair<const string, Contructor>(A::Name(), foo);
        }
        using Initializer = function<void(XmlNode)>;
        template <class T>
        static pair<const string, Initializer> flagInitializer(const char* name, T foo)
        {
            return make_pair<const string, Initializer>(name, foo);
        }
        template <class F>
        bool foreach(F& foo);
        template <class F>
        bool foreach(F& foo, ObjectStateFlag flags);
    private:
        ObjectType m_type;
        uint m_size = 0;

        bool m_tinyFlag = true;
        bool m_globalFlag = false;
        ObjectStateFlag m_flags = 0;

        array<unique_ptr<AspectPrototype>, MaxNumAspects> m_aspects;
    };
    /// @}
}

/** @addtogroup LsScene
 *  @{
 */
/** @def LS_ASPECT_CONSTRUCT
 *  Implement aspect @b construct
 */
#define LS_ASPECT_CONSTRUCT(Prototype, Aspect)                  \
    virtual AspectInstance& constructAspect() const override    \
    {                                                           \
        return construct<Prototype, Aspect>();                  \
    }                                                           \

/** @def LS_ASPECT_DESTROY
 *  Implement aspect @b destroy
 */
#define LS_ASPECT_DESTROY(Prototype, Aspect)                            \
    virtual void destroyAspect(AspectInstance& aspect) const override   \
    {                                                                   \
        return destroy<Prototype, Aspect>(aspect);                      \
    }                                                                   \

/** @def LS_ASPECT
 *  Implement aspect @b construct and @b destroy
 */
#define LS_ASPECT(Aspect)                           \
public:                                             \
    LS_ASPECT_CONSTRUCT(Aspect##Prototype, Aspect)  \
    LS_ASPECT_DESTROY(Aspect##Prototype, Aspect)    \

/// @}
