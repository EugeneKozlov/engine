#include "pch.h"
#include "ls/scene/Component.h"

using namespace ls;
Component::Component(const ComponentType type, const ComponentId id) 
    : m_type(type)
    , m_id(id)
{
    if (id != InvalidComponentId)
    {
        m_flags = ComponentFlag::Shared;
    }
}
Component::~Component()
{

}
ComponentType Component::type() const
{
    return m_type;
}
ComponentFlags Component::flags() const
{
    return m_flags;
}
ComponentId Component::id() const
{
    return m_id;
}
bool Component::isShared() const
{
    return m_flags.is(ComponentFlag::Shared);
}
bool Component::isPrototyped() const
{
    return m_flags.is(ComponentFlag::Prototyped);
}
