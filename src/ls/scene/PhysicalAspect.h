/** @file ls/scene/PhysicalAspect.h
 *  @brief Physical aspect
 */

#pragma once
#include "ls/common.h"
#include "ls/scene/ObjectInstance.h"
#include "ls/scene/ObjectPrototype.h"
#include "ls/io/Codec.h"
#include "ls/xml/common.h"
#include "ls/physics/common.h"

namespace ls
{
    /** @addtogroup LsScene
     *  @{
     */
     /// Scene type
    using btWorld = btSoftRigidDynamicsWorld;
    /** Physical aspect prototype
     */
    class PhysicalAspectPrototype
    : public AspectPrototype
    {
        LS_ASPECT(PhysicalAspect);
    public:
        /// Category
        static constexpr AspectCategory Category = AspectCategory::Physical;
        /// Aspect name
        static constexpr const char* Name()
        {
            return "physical";
        }
        /// Ctor
        PhysicalAspectPrototype(XmlNode aspectNode,
                                const char* name = Name(),
                                bool isTiny = true);
    public:
        virtual void gatherState(AnyMap& source, AspectState& dest) const override;
        virtual void encodeState(AspectState const& source, DynamicStream& dest) const override;
        virtual void decodeState(DynamicStream& source, AspectState& dest) const override;
    private:
        Encoding m_corruptionEncoding;
        Encoding m_healthEncoding;
        PhysicalAspectState m_default;
    };
    /** Rigid body aspect
     */
    class PhysicalAspect
    : public AspectInstance
    {
    public:
        /// Ctor
        PhysicalAspect(PhysicalAspectPrototype const& prototype);
        /// Dtor
        virtual ~PhysicalAspect();

        /// Add to simulation
        void addToWorld(btWorld& world);
        /// Remove from current simulation
        void removeFromWorld();
        /// Set on-awake callback
        void setAwakeCallback(function<void(ObjectInstance& object) > callback);

        /** @name Gets
         *  @{
         */
        /// Test if added to world
        bool isSimualted() const noexcept
        {
            return !!m_world;
        }
        /// @}
    public:
        virtual void extract(ObjectState& data) override;
        virtual void inject(ObjectState const& data) override;
    private:
        virtual void implAttach(ObjectInstance& object) override;
        virtual void implAddToWorld()
        {
        };
        virtual void implRemoveFromWorld()
        {
        };
        virtual void implSetAwakeCallback()
        {
        }
        static function<void(ObjectInstance& object) > DefaultAwakeCallback;
    protected:
        /// Motion state
        struct MotionState
        : public btDefaultMotionState
        {
            /// Ctor
            MotionState(ObjectIterator object,
                        btTransform const& centerOfMass = btTransform::getIdentity())
                : btDefaultMotionState(btTransform::getIdentity(), centerOfMass)
                , object(object)
                , awake(DefaultAwakeCallback)
            {
            }
            /// Re-implement
            virtual void setWorldTransform(btTransform const& global) override
            {
                btDefaultMotionState::setWorldTransform(global);
                awake(*object);
            }
            /// Object
            ObjectIterator object;
            /// Awake callback
            std::reference_wrapper<function<void(ObjectInstance& object)> > awake;
        };
    protected:
        ObjectIterator m_object;
        btWorld* m_world;
        function<void(ObjectInstance& object) > onAwake;
    };
    /// @}
}

