#include "pch.h"
#include "ls/io/Codec.h"
#include "ls/scene/Entity.h"
#include "ls/scene/storage/ComponentManager.h"
#include "ls/scene/storage/GlobalComponentContainer.h"

using namespace ls;
EntityPool Entity::CommonEntityPool;
Entity::Entity()
    : Entity(0, CommonEntityPool)
{

}
Entity::Entity(EntityId entityId, EntityPool& pool)
    : m_pool(pool)
    , m_id(entityId)
{

}
Entity::~Entity()
{
    release();
}
void Entity::release()
{
    for (uint idx = 0; idx < m_numComponents; ++idx)
    {
        Component* component = m_components[idx];
        if (component && !component->isShared())
            m_pool.deallocateComponent(*component);
    }
    m_numComponents = 0;
    m_components.fill(nullptr);
    m_componentsTypes.fill(ComponentType::Unknown);
}
void Entity::serialize(Buffer& dest)
{
    encode<Encoding::Byte>(dest, m_numComponents);
    for (uint idx = 0; idx < m_numComponents; ++idx)
    {
        debug_assert(m_components[idx]);
        Component& component = *m_components[idx];
        const ComponentType type = component.type();
        const ComponentFlags flags = component.flags();
        const ushort info 
            = (static_cast<ushort>(type) & ComponentTypeMask)
            | (static_cast<ushort>(flags) & ComponentFlagsMask);

        // Save info
        encode<Encoding::Short>(dest, info);
        if (flags.is(ComponentFlag::Shared) || flags.is(ComponentFlag::Prototyped))
        {
            // Save ID
            const ComponentId componentId = component.id();
            debug_assert(componentId != InvalidComponentId);
            encode<Encoding::Short>(dest, componentId);
        }
        else
        {
            // Save all data
            ComponentManager::serialize(component.type(), component, 
                                        Archive<Buffer>(dest, ArchiveWrite));
        }
    }
}
void Entity::deserialize(Buffer& src, GlobalComponentContainer& global)
{
    decode<Encoding::Byte>(src, m_numComponents);
    for (uint idx = 0; idx < m_numComponents; ++idx)
    {
        // Read info
        ushort info;
        decode<Encoding::Short>(src, info);
        const ComponentType type = static_cast<ComponentType>(info & ComponentTypeMask);
        const ComponentFlags flags = static_cast<ComponentFlags>(info & ComponentFlagsMask);
        m_componentsTypes[idx] = type;

        const bool isShared = flags.is(ComponentFlag::Shared);
        const bool isPrototyped = flags.is(ComponentFlag::Prototyped);
        if (isShared || isPrototyped)
        {
            // Shared and prototyped components store only ID
            ComponentId componentId;
            decode<Encoding::Short>(src, componentId);
            Component* component = global.findComponent(componentId);
            LS_THROW(component, 
                     IoException, 
                     "Cannot find global component #", componentId);

            if (isShared)
            {
                // Share found component
                m_components[idx] = component;
            }
            else
            {
                // Copy found component
                Component& copy = m_pool.allocateComponent(type);
                ComponentManager::copy(type, copy, *component);
                m_components[idx] = &copy;
            }
        }
        else
        {
            // Allocate and read new component
            Component& component = m_pool.allocateComponent(type);
            ComponentManager::serialize(component.type(), component,
                                        Archive<Buffer>(src, ArchiveRead));
            m_components[idx] = &component;
        }
    }
}
EntityId Entity::clone(const Entity& src)
{
    release();
    m_numComponents = src.m_numComponents;
    m_componentsTypes = src.m_componentsTypes;
    for (uint idx = 0; idx < m_numComponents; ++idx)
    {
        Component& sourceComponent = *src.m_components[idx];
        if (sourceComponent.isShared())
        {
            m_components[idx] = &sourceComponent;
        }
        else
        {
            const ComponentType type = src.m_componentsTypes[idx];
            Component& destComponent = m_pool.allocateComponent(type);

            ComponentManager::copy(type, destComponent, sourceComponent);
            m_components[idx] = &destComponent;
        }
    }
    return m_id;
}


// Use
void Entity::addSharedComponent(Component& component)
{
    LS_THROW(component.isShared(),
             LogicException,
             "Component must be shared");
    LS_THROW(m_numComponents < MaxEntityComponents,
             OverflowException,
             "Too many components of entity #", m_id);

    m_componentsTypes[m_numComponents] = component.type();
    m_components[m_numComponents] = &component;
    ++m_numComponents;
}
void Entity::addPrototypedComponent(Component& prototype)
{
    LS_THROW(prototype.isShared(),
             LogicException,
             "Component must be shared");
    LS_THROW(m_numComponents < MaxEntityComponents,
             OverflowException,
             "Too many components of entity #", m_id);

    const ComponentType type = prototype.type();
    Component& component = m_pool.allocateComponent(type);
    ComponentManager::copy(type, component, prototype);

    m_componentsTypes[m_numComponents] = type;
    m_components[m_numComponents] = &component;
}
Component& Entity::addComponent(const ComponentType type)
{
    LS_THROW(m_numComponents < MaxEntityComponents,
             OverflowException,
             "Too many components of entity #", m_id);

    Component& component = m_pool.allocateComponent(type);
    m_componentsTypes[m_numComponents] = type;
    m_components[m_numComponents] = &component;
    ++m_numComponents;
    return component;
}
Component* Entity::findComponent(const ComponentType type)
{
    for (Component* component : *this)
    {
        debug_assert(component);
        if (component->type() == type)
            return component;
    }
    return nullptr;
}