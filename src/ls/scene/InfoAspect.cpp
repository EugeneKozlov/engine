#include "pch.h"
#include "ls/scene/InfoAspect.h"
#include "ls/io/DynamicStream.h"

using namespace ls;

// Prototype
InfoAspectPrototype::InfoAspectPrototype(XmlNode aspectNode,
                                         AspectCategory category,
                                         const char* name,
                                         bool isTiny)
    : AspectPrototype(name, category, isTiny, InfoAspectSlot)
{
    const char* anountEncodingName = loadNodeAttrib(aspectNode,
                                                    "amount", "type").as_string();
    const char* flagsEncodingName = loadNodeAttrib(aspectNode,
                                                   "flags", "type").as_string();
    const char* bufEncodingName = loadNodeAttrib(aspectNode,
                                                 "buf", "type").as_string();

    m_amountEncoding = encodingByName(anountEncodingName);
    m_flagsEncoding = encodingByName(flagsEncodingName);
    m_bufEncoding = encodingByName(bufEncodingName);

    LS_THROW(isInteger(m_amountEncoding),
             InvalidConfigException,
             "Invalid amount [", anountEncodingName, "]");
    LS_THROW(isInteger(m_flagsEncoding),
             InvalidConfigException,
             "Invalid flags [", flagsEncodingName, "]");
    LS_THROW(isInteger(m_bufEncoding),
             InvalidConfigException,
             "Invalid buf [", bufEncodingName, "]");

    m_default.amount = readDefault<uint, uint>(aspectNode, "amount");
    m_default.flags = readDefault<uint, uint>(aspectNode, "flags");
    m_default.buf = readDefault<uint, uint>(aspectNode, "buf");

    m_size += sizeOfObject<uint>(m_amountEncoding);
    m_size += sizeOfObject<uint>(m_flagsEncoding);
    m_size += sizeOfObject<uint>(m_bufEncoding);
}
void InfoAspectPrototype::gatherState(AnyMap& source, AspectState& dest) const
{
    auto& state = dest.cast<InfoAspectState>();

    state.amount = deref(any_cast<uint>(&source["amount"]),
                         &m_default.amount);
    state.flags = deref(any_cast<uint>(&source["flags"]),
                        &m_default.flags);
    state.buf = deref(any_cast<uint>(&source["buf"]),
                      &m_default.buf);
}
void InfoAspectPrototype::encodeState(AspectState const& source,
                                      DynamicStream& dest) const
{
    auto& state = source.cast<InfoAspectState>();

    encodeObject(m_amountEncoding, state.amount, dest);
    encodeObject(m_flagsEncoding, state.flags, dest);
    encodeObject(m_bufEncoding, state.buf, dest);

}
void InfoAspectPrototype::decodeState(DynamicStream& source,
                                      AspectState& dest) const
{
    auto& state = dest.cast<InfoAspectState>();

    decodeObject(m_amountEncoding, state.amount, source);
    decodeObject(m_flagsEncoding, state.flags, source);
    decodeObject(m_bufEncoding, state.buf, source);
}


// Aspect
InfoAspect::InfoAspect(InfoAspectPrototype const& prototype)
    : AspectInstance(prototype)
{
}
InfoAspect::~InfoAspect()
{
}
void InfoAspect::extract(ObjectState& /*data*/)
{
}
void InfoAspect::inject(ObjectState const& /*data*/)
{
}

