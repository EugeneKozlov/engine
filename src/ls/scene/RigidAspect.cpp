#include "pch.h"
#include "ls/scene/RigidAspect.h"
#include "ls/io/DynamicStream.h"

using namespace ls;

// Prototype
RigidAspectPrototype::RigidAspectPrototype(XmlNode aspectNode,
                                           const char* name,
                                           bool isTiny)
    : PhysicalAspectPrototype(aspectNode, name, isTiny)
{
}


// Aspect
RigidAspect::RigidAspect(RigidAspectPrototype const& prototype)
    : PhysicalAspect(prototype)
{
}
RigidAspect::~RigidAspect()
{
}
void RigidAspect::extract(ObjectState& data)
{
    PhysicalAspect::extract(data);
}
void RigidAspect::inject(ObjectState const& data)
{
    PhysicalAspect::inject(data);
}


// Rigid
void RigidAspect::implAddToWorld()
{

}
void RigidAspect::implRemoveFromWorld()
{

}
void RigidAspect::implSetAwakeCallback()
{

}
