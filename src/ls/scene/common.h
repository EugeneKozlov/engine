/** @file ls/scene/common.h
 *  @brief Common types and constants
 */

#pragma once
#include "ls/common.h"
#include "ls/core/Pool.h"
#include "ls/core/Blob.h"
#include "ls/scene/category.h"

namespace ls
{
    /** @addtogroup LsScene
     *  @{
     */
    class ObjectInstance;
    /// Scene object ID type
    using ObjectId = uint;
    /// Scene object type type
    using ObjectType = uint;
    /// Scene object state flag
    using ObjectStateFlag = ubyte;
    /// Invalid object ID
    static ObjectId const InvalidObjectId = ObjectId(-1);
    /// Invalid object type
    static ObjectType const InvalidObjectType = ObjectType(0);

    /// Max number of aspects
    static uint const MaxNumAspects = 8;
    /** Aspect slots.
     *  First 4 bytes must be real size of state buffer
     */
    enum DefaultAspectSlot : uint
    {
        /** Pose aspect slot.
         *  + Pose aspect must be inherited from @a PoseAspect
         *  + Pose aspect state must be compatible with @a PoseAspectState
         */
        PoseAspectSlot = 0,
        /** Movable aspect slot.
         *  + Movable aspect state must be compatible with @a MovableAspectState
         */
        MovableAspectSlot = 1,
        /** State aspect slot.
         *  + State aspect state must be compatible with @a InfoAspectState
         */
        InfoAspectSlot = 2,
        /** Physical aspect slot.
         *  + Physical aspect state must be compatible with @a PhysicalAspectState
         */
        PhysicalAspectSlot = 3,
        /** Custom slots begin with this index
         */
        CustomAspectSlot = 4
    };
    /// Max scene object aspect state size
    static uint const AspectStateSize = 2048;
    /// Max object byte-code size
    static uint const MaxObjectBytecode = 256;
    /// Ghost chunk x-y
    static sshort const GhostChunk = MinShort;
    /// Aspect state blob
    using AspectState = FixedBlob<AspectStateSize>;
    /// Any-type map
    using AnyMap = map<string, any>;
    /// Aspect allocator
    template <class Aspect>
    using AspectAllocator = FastAllocator<Aspect>;
    /// Aspect state small blob allocator
    using AspectBlobAllocator = FastAllocator<AspectState>;
    /// Object iterator type
    using ObjectIterator = Pool<ObjectInstance, ObjectId>::Iterator;
    /// Object constant iterator type
    using ObjectConstIterator = Pool<ObjectInstance, ObjectId>::ConstIterator;
    /// Object iterator list
    using ObjectsList = set<ObjectIterator>;
    /// @}
}