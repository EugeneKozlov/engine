/** @file ls/scene/ObjectInstance.h
 *  @brief Scene object instance
 */

#pragma once
#include "ls/common.h"
#include "ls/forward.h"
#include "ls/geom/Transform.h"
#include "ls/scene/ObjectState.h"
#include "ls/scene/ObjectPrototype.h"
#include "ls/scene/core/ObjectPose.h"

namespace ls
{
    class DynamicStream;

    /** @addtogroup LsScene
     *  @{
     */
    /** Scene object aspect instance.
     *  @see ObjectInstance for detail inject/update/extract description
     */
    class AspectInstance
    : public Noncopyable
    {
    protected:
        /// Ctor
        AspectInstance(AspectPrototype const& prototype);
    public:
        /** Do not destroy manually!
         */
        virtual ~AspectInstance();
        /** Destroy this
         */
        void destroy();
        /** Attach aspect to object
         *  @param object
         *    Object
         *  @param slot
         *    Target slot
         */
        void attach(ObjectInstance& object, uint slot);
        /** Inject state to aspect
         *  @param data
         *    Object data, should be valid
         */
        virtual void inject(ObjectState const& data) = 0;
        /** Update aspect state
         *  @param data
         *    Object data, should be valid
         */
        virtual void update(ObjectState const& data);
        /** Extract state from aspect
         *  @param [out] data
         *    Object data, some states will be rewritten
         */
        virtual void extract(ObjectState& data) = 0;

        /** @name Gets
         *  @{
         */
        /// Get prototype
        AspectPrototype const& prototype() noexcept
        {
            return m_prototype;
        }
        /// @}
    private:
        virtual void implAttach(ObjectInstance& object);
    protected:
        /// Aspect prototype
        AspectPrototype const& m_prototype;
        /// Attach slot
        uint m_slot = uint(-1);
    };
    /** Scene object instance
     */
    class ObjectInstance
    : public Noncopyable
    {
    public:
        /** Ctor
         */
        ObjectInstance();
        /// Move
        ObjectInstance(ObjectInstance&& another) noexcept;
        /// Dtor
        ~ObjectInstance();
        /// Startup initialization
        void startup(ObjectObserverInterface& parent,
                     ObjectIterator self,
                     uint chunkWidth);

        /** @name Main
         *  @{
         */
        /// Destroy this object (do not call this function inside callbacks))
        void destroy();
        /// Delayed destroy this object
        void destroyLater();
        /// Construct object with specified state
        void reconstruct(ObjectState const& state);
        /// Initialize aspects
        void initialize();
        /// De-initialize aspects
        void deinitialize();
        /** Inject object state.
         *  Resets all states and generators.
         *  If aspects are not initialized, same as #update.
         *  @param state
         *    Object state
         *  @return @c true if aspects are initialized
         */
        bool inject(ObjectState const& state);
        /** Update object.
         *  Does not reset generators, only updates aspects states.
         *  If aspects are not initialized, same as #inject.
         *  @param state
         *    Object state, must be complete
         *  @return @c true if aspects are initialized
         */
        bool update(ObjectState const& state);
        /** Extract object state from generation aspects.
         *  Object aspects must be initialized.
         *  @param [out] state
         *    Object state
         *  @return @c true if aspects are initialized
         */
        bool extract(ObjectState& state);
        /** Find aspect by category
         *  @param category
         *    Required category
         *  @return Aspect if found
         */
        AspectInstance* aspect(AspectCategory category);
        /// @}

        /** @name Gets
         *  @{
         */
        /// Test if valid
        bool isValid() const noexcept
        {
            return !!m_prototype;
        }
        /// Test if aspects are initialized
        bool isInitalized() const noexcept
        {
            return m_intialized;
        }
        /// Test if exist
        bool isExist() const noexcept
        {
            return m_state.isExist();
        }
        /// Test if object should be removed
        bool shouldRemove() const noexcept
        {
            return m_markedToRemove;
        }
        /// Get self
        ObjectIterator self() const noexcept
        {
            return m_self;
        }
        /// Get state
        ObjectState& state() noexcept
        {
            return m_state;
        }
        /// Get const state
        ObjectState const& state() const noexcept
        {
            return m_state;
        }
        /// Get this ID
        ObjectId id() const noexcept
        {
            return m_self.index();
        }
        /// Get this type
        ObjectType type() const noexcept
        {
            return m_type;
        }
        /// Get this chunk
        int2 const& chunk() const noexcept
        {
            return m_chunk;
        }
        /// Test if ghost
        bool isGhost() const noexcept
        {
            return m_chunk == int2(GhostChunk);
        }
        /// Test if global
        bool isGlobal() const noexcept
        {
            debug_assert(isValid());
            return m_prototype->isGlobal();
        }
        /// Get global pose (fail if ghost)
        ObjectPose globalPose() const noexcept;
        /// Get position
        double3 position() const noexcept;
        /// Set global pose (fail if ghost)
        void globalPose(ObjectPose const& pose) noexcept;
        /// Set position
        void position(double3 const& position) noexcept;
        /// Get debug flags
        ushort debugFlags() const noexcept
        {
            return m_debugFlags;
        }
        /// Set debug flags
        void debugFlags(ushort value) noexcept
        {
            m_debugFlags = value;
        }
        /// @}
    private:
        void reset();
        const double3* computePosition() const noexcept;
        int2 computeChunk() const noexcept;
        void rebindObject();
    private:
        ObjectObserverInterface* m_parent = nullptr;
        ObjectIterator m_self;
        uint m_chunkWidth;
    private:
        const ObjectPrototype* m_prototype = nullptr;
        ObjectType m_type = InvalidObjectType;

        ObjectState m_state;
        bool m_intialized = false;
        bool m_markedToRemove = false;
        ushort m_debugFlags = 0;
        int2 m_chunk;

        array<AspectInstance*, MaxNumAspects> m_aspects;
    };
    /// @}
}


