#include "pch.h"
#include "ls/scene/EntityManager.h"
#include "ls/asset/WorldAsset.h"

using namespace ls;
EntityManagerBase::EntityManagerBase(const vector<shared_ptr<SceneSystem>>& sceneSystems)
    : m_sceneSystems(sceneSystems)
    , m_globalComponents()
    , m_dynamicEntities(m_globalComponents)
{
    for (shared_ptr<SceneSystem>& sceneSystem : m_sceneSystems)
    {
        debug_assert(sceneSystem);
        const uint key = sceneSystem->key();
        m_sceneSystemsPerComponent[key].push_back(&*sceneSystem);
    }
}
EntityManagerBase::~EntityManagerBase()
{
    // Destroy systems in reverse order
    while (!m_sceneSystems.empty())
    {
        m_sceneSystems.pop_back();
    }
}
void EntityManagerBase::distributeEntities(EntityContainer& src,
                                           array<EntitiesList, NumComponentTypes>& dest)
{
    // Prepare mail lists
    for (Entity* entity : src)
    {
        // Skip empty
        if (!entity)
            continue;

        // Fill arrays
        for (Component* component : *entity)
        {
            debug_assert(component);
            const ComponentType type = component->type();
            const uint idx = static_cast<uint>(type);
            dest[idx].emplace_back(entity, component);
        }
    }
}


// IO
void EntityManagerBase::deserializeGlobalComponents(Buffer& src)
{
    m_globalComponents.deserialize(src);
}
void EntityManagerBase::releaseDynamics()
{
    array<EntitiesList, NumComponentTypes> entitiesToRemove;
    distributeEntities(m_dynamicEntities, entitiesToRemove);

    // Remove all
    for (shared_ptr<SceneSystem>& sceneSystem : m_sceneSystems)
    {
        const uint key = sceneSystem->key();
        for (const auto& entity : entitiesToRemove[key])
        {
            sceneSystem->removeEntity(*entity.first);
        }
    }
}
void EntityManagerBase::serializeDynamics(Buffer& dest)
{
    m_dynamicEntities.serialize(dest);
}
void EntityManagerBase::deserializeDynamics(Buffer& src)
{
    releaseDynamics();
    m_dynamicEntities.deserialize(src);

    // Prepare new entities
    array<EntitiesList, NumComponentTypes> entitiesToAdd;
    distributeEntities(m_dynamicEntities, entitiesToAdd);

    // Add entities
    for (shared_ptr<SceneSystem>& sceneSystem : m_sceneSystems)
    {
        const uint key = sceneSystem->key();
        for (const auto& entity : entitiesToAdd[key])
        {
            sceneSystem->addEntity(*entity.first, *entity.second);
        }
    }
}


// Entities
Component* EntityManagerBase::findSharedComponent(const ComponentId id)
{
    return m_globalComponents.findComponent(id);
}
EntityId EntityManagerBase::addEntity(Entity& entity)
{
    return m_dynamicEntities.createEntity().clone(entity);
}
void EntityManagerBase::removeEntity(Entity& entity)
{
    m_dynamicEntities.destroyEntity(entity);
}


// Process
void EntityManagerBase::setLastFrameTime(const float deltaSec, const DeltaTime deltaMsec)
{
    for (auto& sceneSystem : m_sceneSystems)
    {
        sceneSystem->setLastFrameTime(deltaSec, deltaMsec);
    }
}
void EntityManagerBase::syncUpdateSystems()
{
    for (auto& sceneSystem : m_sceneSystems)
    {
        sceneSystem->syncUpdate();
    }
}
void EntityManagerBase::asyncUpdateSystems()
{
    for (auto& sceneSystem : m_sceneSystems)
    {
        sceneSystem->asyncUpdate();
    }
}


// Build Mode - serialize
void EntityManagerBuildMode::serializeGlobalComponents(Buffer& dest)
{
    globalComponents().serialize(dest);
}
void EntityManagerBuildMode::releaseStatic(const int2& regionIndex)
{
    m_staticEntities.erase(regionIndex);
}
void EntityManagerBuildMode::serializeStatic(Buffer& dest, const int2& regionIndex)
{
    SharedEntityContainer container = getOrDefault(m_staticEntities, regionIndex);
    if (container)
    {
        container->serialize(dest);
    }
}
void EntityManagerBuildMode::deserializeStatic(Buffer& dest, const int2& regionIndex)
{
    // Find container
    SharedEntityContainer container = getOrDefault(m_staticEntities, regionIndex);
    if (!container)
    {
        container = make_shared<EntityContainer>(globalComponents());
        m_staticEntities.emplace(regionIndex, container);
    }

    // Read
    container->deserialize(dest);
}


// Build Mode - Create/Remove
Component& EntityManagerBuildMode::createSharedComponent(const ComponentType type)
{
    return globalComponents().createComponent(type);
}
EntityId EntityManagerBuildMode::addStaticEntity(Entity& entity, const int2& regionIndex)
{
    // Find container
    SharedEntityContainer container = getOrDefault(m_staticEntities, regionIndex);
    if (!container)
    {
        container = make_shared<EntityContainer>(globalComponents()); 
        m_staticEntities.emplace(regionIndex, container);
    }

    // Create entity
    return container->createEntity().clone(entity);
}
void EntityManagerBuildMode::removeStaticEntity(Entity& entity, const int2& regionIndex)
{
    // Find container
    SharedEntityContainer container = getOrDefault(m_staticEntities, regionIndex);
    debug_assert(container, "try to remove entity from invalid region", regionIndex);

    // Destroy entity
    container->destroyEntity(entity);
}


// Deploy Mode - Implement WorldObserverInterface
void EntityManager::doLoadChunksPre(WorldRegion& region, EntityManagerAsyncStorage& storage)
{
    // Get container from pool
    if (!m_containersPool.empty())
    {
        storage.container = m_containersPool.back();
        m_containersPool.pop_back();
    }
}
void EntityManager::doLoadChunks(WorldRegion& region, EntityManagerAsyncStorage& storage)
{
    // Allocate container if there was no container in the pool
    if (!storage.container)
    {
        storage.container = make_shared<EntityContainer>(globalComponents());
    }

    // Load entities
    Buffer& objectsData = region.chunksData().staticObjects();
    storage.container->deserialize(objectsData);
    objectsData.seek(0);

    // Prepare mail lists
    distributeEntities(*storage.container, storage.entities);

    // Prepare all scene systems
    const auto& sceneSystems = getSceneSystems();
    storage.systemsPreparedData.resize(sceneSystems.size());
    for (uint idx = 0; idx < sceneSystems.size(); ++idx)
    {
        SceneSystem& sceneSystem = *sceneSystems[idx];
        const uint key = sceneSystem.key();
        EntitiesList& entities = storage.entities[key];
        SceneSystemCache cache = sceneSystem.prepareStaticEntities(entities, region);
        storage.systemsPreparedData[idx] = cache;
    }
}
void EntityManager::doLoadChunksPost(WorldRegion& region, EntityManagerAsyncStorage& storage)
{
    // Put built container
    m_staticEntities.emplace(region.index(), storage.container);
    storage.container = nullptr;

    // Add objects
    const auto& sceneSystems = getSceneSystems();
    for (uint idx = 0; idx < sceneSystems.size(); ++idx)
    {
        SceneSystem& sceneSystem = *sceneSystems[idx];
        SceneSystemCache cache = storage.systemsPreparedData[idx];
        const uint key = sceneSystem.key();
        sceneSystem.addStaticEntities(storage.entities[key], region, cache);
    }
}
void EntityManager::onUnloadChunksData(WorldRegion& region)
{
    SharedEntityContainer container = getOrDefault(m_staticEntities, region.index());
    if (!container)
        return;

    // Save to pool and remove from map
    container->release();
    m_containersPool.emplace_back(container);
    m_staticEntities.erase(region.index());
}
