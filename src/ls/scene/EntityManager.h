/// @file ls/scene/EntityManager.h
/// @brief Entity Manager
#pragma once

#include "ls/common.h"
#include "ls/core/Nameable.h"
#include "ls/scene/Component.h"
#include "ls/scene/Entity.h"
#include "ls/scene/SceneSystem.h"
#include "ls/scene/storage/EntityContainer.h"
#include "ls/scene/storage/GlobalComponentContainer.h"
#include "ls/world/WorldObserverInterface.h"

namespace ls
{
    /// @addtogroup LsScene
    /// @{

    /// @brief Entity Manager (Base)
    class EntityManagerBase
        : Noncopyable
        , public Nameable<EntityManagerBase>
    {
    protected:
        /// @brief Ctor
        explicit EntityManagerBase(const vector<shared_ptr<SceneSystem>>& sceneSystems);
        /// @brief Dtor
        ~EntityManagerBase();
    public:
        /// @brief Set last frame time
        void setLastFrameTime(const float deltaSec, const DeltaTime deltaMsec);
        /// @brief Synchronous scene systems update
        void syncUpdateSystems();
        /// @brief Asynchronous scene system update
        void asyncUpdateSystems();

        /// @brief Deserialize global components
        void deserializeGlobalComponents(Buffer& src);
        /// @brief Release dynamics
        void releaseDynamics();
        /// @brief Serialize dynamic entities and components
        void serializeDynamics(Buffer& dest);
        /// @brief Deserialize dynamic entities and components
        void deserializeDynamics(Buffer& src);

        /// @brief Get shared components
        /// @note Slow
        template <class DerivedComponent>
        vector<DerivedComponent*> sharedComponents()
        {
            vector<DerivedComponent*> list;
            for (Component* component : globalComponents())
            {
                DerivedComponent* derivedComponent = dynamic_cast<DerivedComponent*>(component);
                if (derivedComponent != nullptr)
                {
                    list.push_back(derivedComponent);
                }
            }
        }
        /// @brief Find shared component
        Component* findSharedComponent(const ComponentId id);
        /// @brief Add entity
        EntityId addEntity(Entity& entity);
        /// @brief Remove entity
        void removeEntity(Entity& entity);

        /// @brief Filter scene systems
        template <class DerivedSystem>
        vector<DerivedSystem*> copySceneSystems() const
        {
            vector<DerivedSystem*> list;
            for (const shared_ptr<SceneSystem>& system : m_sceneSystems)
            {
                DerivedSystem* derivedSystems = dynamic_cast<DerivedSystem*>(system.get());
                if (derivedSystems != nullptr)
                {
                    list.push_back(derivedSystems);
                }
            }
            return list;
        }
    protected:
        /// @brief Get scene systems
        const vector<shared_ptr<SceneSystem>>& getSceneSystems() const
        {
            return m_sceneSystems;
        }
        /// @brief Get global components container
        GlobalComponentContainer& globalComponents()
        {
            return m_globalComponents;
        }
        /// @brief Distribute entities over array
        /// @note All pointers are guaranteed
        static void distributeEntities(EntityContainer& src,
                                       array<EntitiesList, NumComponentTypes>& dest);
    private:
        vector<shared_ptr<SceneSystem>> m_sceneSystems; ///< Scene systems list
        /// @brief Dirty list of systems interested in component
        array<vector<SceneSystem*>, NumComponentTypes> m_sceneSystemsPerComponent;

        GlobalComponentContainer m_globalComponents;
        EntityContainer m_dynamicEntities;
    };
    /// @brief Entity Manager (Build Mode)
    ///
    /// Additional functional stuff:
    /// - @a serializeGlobalComponents allows to save global components data
    /// - @a createSharedComponent allows to manage shared components
    /// - @a releaseStatic, @a serializeStatic and @a deserializeStatic
    ///   allow to manage static data
    /// - @a createStaticEntity and @a destroyStaticEntity allow to manage static entities
    class EntityManagerBuildMode
        : public EntityManagerBase
    {
    public:
        /// @brief Ctor
        explicit EntityManagerBuildMode(const vector<shared_ptr<SceneSystem>>& sceneSystems)
            : EntityManagerBase(sceneSystems)
        {
        }
        /// @brief Serialize global components
        void serializeGlobalComponents(Buffer& dest);

        /// @brief Release static objects
        void releaseStatic(const int2& regionIndex);
        /// @brief Serialize static entities
        void serializeStatic(Buffer& dest, const int2& regionIndex);
        /// @brief Deserialize static entities (overwrites existing entities at the region)
        void deserializeStatic(Buffer& dest, const int2& regionIndex);

        /// @brief Create shared component (compile-time type)
        template <class DerivedComponent>
        DerivedComponent& createSharedComponent()
        {
            return *createSharedComponent(DerivedComponent::Type)
                .template cast<DerivedComponent>();
        }
        /// @brief Create shared component
        Component& createSharedComponent(const ComponentType type);

        /// @brief Add static entity
        EntityId addStaticEntity(Entity& entity, const int2& regionIndex);
        /// @brief Remove static entity
        void removeStaticEntity(Entity& entity, const int2& regionIndex);
    private:
        hamap<int2, SharedEntityContainer> m_staticEntities;
    };
    /// @brief Async storage for Entity Manager
    struct EntityManagerAsyncStorage
    {
        SharedEntityContainer container; ///< Entity container
        array<EntitiesList, NumComponentTypes> entities; ///< Entities to send
        vector<SceneSystemCache> systemsPreparedData; ///< Data that is prepared by scene systems
    };
    /// @brief Entity Manager (deploy mode)
    class EntityManager
        : public EntityManagerBase
        , public WorldObserver<EntityManagerAsyncStorage>
    {
    public:
        /// @brief Ctor
        explicit EntityManager(const vector<shared_ptr<SceneSystem>>& sceneSystems)
            : EntityManagerBase(sceneSystems)
        {
        }
    private:
        /// @name Implement WorldObserverInterface
        /// @{
        virtual void doLoadChunksPre(WorldRegion& region, EntityManagerAsyncStorage& storage) override;
        virtual void doLoadChunks(WorldRegion& region, EntityManagerAsyncStorage& storage) override;
        virtual void doLoadChunksPost(WorldRegion& region, EntityManagerAsyncStorage& storage) override;

        virtual void onUnloadChunksData(WorldRegion& region) override;
        /// @}
    private:
        hamap<int2, SharedEntityContainer> m_staticEntities;
        vector<SharedEntityContainer> m_containersPool; ///< Allow to reuse static containers
    };

    /// @}
}
