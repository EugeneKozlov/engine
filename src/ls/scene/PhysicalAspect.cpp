#include "pch.h"
#include "ls/scene/PhysicalAspect.h"
#include "ls/io/DynamicStream.h"

using namespace ls;

// Prototype
PhysicalAspectPrototype::PhysicalAspectPrototype(XmlNode aspectNode,
                                                 const char* name,
                                                 bool isTiny)
    : AspectPrototype(name, Category, isTiny, PhysicalAspectSlot)
{
    const char* corruptionEncodingName = loadNodeAttrib(aspectNode,
                                                        "corruption", "type").as_string();
    const char* healthEncodingName = loadNodeAttrib(aspectNode,
                                                    "health", "type").as_string();

    m_corruptionEncoding = encodingByName(corruptionEncodingName);
    m_healthEncoding = encodingByName(healthEncodingName);

    LS_THROW(isInteger(m_corruptionEncoding),
             InvalidConfigException,
             "Invalid corruption [", corruptionEncodingName, "]");
    LS_THROW(isInteger(m_healthEncoding),
             InvalidConfigException,
             "Invalid health [", healthEncodingName, "]");

    m_default.corruption = readDefault<uint, uint>(aspectNode, "corruption");
    m_default.health = readDefault<uint, uint>(aspectNode, "health");

    m_size += sizeOfObject<uint>(m_corruptionEncoding);
    m_size += sizeOfObject<uint>(m_healthEncoding);
}
void PhysicalAspectPrototype::gatherState(AnyMap& source, AspectState& dest) const
{
    auto& state = dest.cast<PhysicalAspectState>();

    state.corruption = deref(any_cast<uint>(&source["corruption"]),
                             &m_default.corruption);
    state.health = deref(any_cast<uint>(&source["health"]),
                         &m_default.health);
}
void PhysicalAspectPrototype::encodeState(AspectState const& source,
                                          DynamicStream& dest) const
{
    auto& state = source.cast<PhysicalAspectState>();

    encodeObject(m_corruptionEncoding, state.corruption, dest);
    encodeObject(m_healthEncoding, state.health, dest);

}
void PhysicalAspectPrototype::decodeState(DynamicStream& source,
                                          AspectState& dest) const
{
    auto& state = dest.cast<PhysicalAspectState>();

    decodeObject(m_corruptionEncoding, state.corruption, source);
    decodeObject(m_healthEncoding, state.health, source);
}


// Aspect
function<void(ObjectInstance& object) >
PhysicalAspect::DefaultAwakeCallback =
    [](ObjectInstance&)
    {
    };
PhysicalAspect::PhysicalAspect(PhysicalAspectPrototype const& prototype)
    : AspectInstance(prototype)
{
}
PhysicalAspect::~PhysicalAspect()
{
}
void PhysicalAspect::addToWorld(btWorld& world)
{
    debug_assert(!m_world);

    m_world = &world;
    implAddToWorld();
}
void PhysicalAspect::removeFromWorld()
{
    debug_assert(m_world);

    implRemoveFromWorld();
    m_world = nullptr;
}
void PhysicalAspect::setAwakeCallback(function<void(ObjectInstance&) > callback)
{
    onAwake = callback;
    implSetAwakeCallback();
}
void PhysicalAspect::extract(ObjectState& /*data*/)
{
}
void PhysicalAspect::inject(ObjectState const& /*data*/)
{
}
void PhysicalAspect::implAttach(ObjectInstance& object)
{
    m_object = object.self();
}


