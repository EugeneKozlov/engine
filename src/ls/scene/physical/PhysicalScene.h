/// @file ls/scene/physical/PhysicalScene.h
/// @brief Physical Scene
#pragma once

#include "ls/common.h"
#include "ls/core/FlatArray.h"
#include "ls/physics/common.h"
#include "ls/world/WorldObserverInterface.h"

#include <BulletSoftBody/btSoftRigidDynamicsWorld.h>

namespace ls
{
    /// @addtogroup LsScene
    /// @{

    /// @brief Physical Scene Chunk
    struct PhysicalSceneChunk
    {
        /// @brief Dtor
        ~PhysicalSceneChunk();
        /// @brief Is added?
        bool isAdded = false;
        /// @brief Terrain shape
        btHeightfieldTerrainShape* terrainShape = nullptr;
        /// @brief Terrain body
        btRigidBody* terrainBody = nullptr;
    };
    /// @brief Physical Scene Region
    class PhysicalSceneRegion
    {
    public:
        /// @brief Ctor
        PhysicalSceneRegion(WorldRegion& region);
        /// @brief Load
        void load();
        /// @brief Add to scene
        void enable(const int2& chunkIndex, btSoftRigidDynamicsWorld& world);
        /// @brief Remove from scene
        void disable(const int2& chunkIndex, btSoftRigidDynamicsWorld& world);
        /// @brief Remove all from scene
        void disableAll(btSoftRigidDynamicsWorld& world);
    private:
        WorldRegion& m_region;
        FlatArray<PhysicalSceneChunk> m_chunks;
    };
    /// @brief Shared Physical Scene Region
    using SharedPhysicalSceneRegion = shared_ptr<PhysicalSceneRegion>;
    /// @brief Physical Scene
    class PhysicalScene
        : Noncopyable
        , public WorldObserverInterface
    {
    public:
        /// @brief Region size
        const uint regionSize;
        /// @brief #HACK : remove
        btRigidBody* _actor = nullptr;
    public:
        /// @brief Ctor
        PhysicalScene(uint regionSize);
        /// @brief Dtor
        ~PhysicalScene();

        /// @brief Update
        void update(float deltaTime, DeltaTime deltaTimeMs);

        /// @brief Enable chunk
        void enableChunk(const int2& chunkIndex);
        /// @brief Disable chunk
        void disableChunk(const int2& chunkIndex);
    public:
        virtual void onPreloadChunksData(WorldRegion& region) override;
        virtual void onAsyncLoadChunksData(WorldRegion& region) override;
        virtual void onPostloadChunksData(WorldRegion& region) override;
        virtual void onUnloadChunksData(WorldRegion& region) override;
    private:
        btSoftRigidDynamicsWorld* m_world = nullptr;

        hamap<int2, SharedPhysicalSceneRegion> m_regions;
        struct AsyncStorage
        {
            SharedPhysicalSceneRegion region;
        };

        hamap<int2, haset<int2>> m_chunksToEnable;
    };
    /// @}
}