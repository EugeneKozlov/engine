#include "pch.h"
#include "ls/scene/physical/PhysicalScene.h"
#include "ls/asset/WorldAsset.h"

using namespace ls;
// Region
PhysicalSceneChunk::~PhysicalSceneChunk()
{
    delete terrainBody;
    delete terrainShape;
}
PhysicalSceneRegion::PhysicalSceneRegion(WorldRegion& region)
    : m_region(region)
{
}
void PhysicalSceneRegion::load()
{
    if (!m_chunks.isEmpty())
        return;

    m_chunks.reset(m_region.config().regionInChunks,
                   m_region.config().regionInChunks);
    int2 local;
    for (local.y = 0; local.y < m_chunks.size().y; ++local.y)
        for (local.x = 0; local.x < m_chunks.size().x; ++local.x)
        {
            // Prepare
            WorldRegion::Chunk& src = m_region.chunksData()[local];
            PhysicalSceneChunk& dest = m_chunks[local];

            // Create shape 
            sint chunkWidth = (sint) m_region.config().chunkWidth;
            sint hmapWidth = chunkWidth + 1;
            dest.terrainShape = new btHeightfieldTerrainShape(
                hmapWidth, hmapWidth, src.heightMap.data(), 1.0f, 
                static_cast<float>(src.min()), static_cast<float>(src.max()), 1, PHY_FLOAT, false);
            float2 center = (static_cast<float2>(src.index) + 0.5f) * static_cast<float>(chunkWidth);

            dest.terrainBody = new btRigidBody(0.0f, nullptr, dest.terrainShape);
            btTransform pose(btQuaternion::getIdentity(),
                             btVector3(center.x,
                                       float(src.min() + src.max()) / 2,
                                       center.y));
            dest.terrainBody->setWorldTransform(pose);
        }
}
void PhysicalSceneRegion::enable(const int2& chunkIndex, btSoftRigidDynamicsWorld& world)
{
    int2 local = m_region.global2local(chunkIndex);
    PhysicalSceneChunk& chunk = m_chunks[local];

    chunk.isAdded = true;
    world.addRigidBody(chunk.terrainBody);
}
void PhysicalSceneRegion::disable(const int2& chunkIndex, btSoftRigidDynamicsWorld& world)
{
    int2 local = m_region.global2local(chunkIndex);
    PhysicalSceneChunk& chunk = m_chunks[local];

    chunk.isAdded = false;
    world.removeRigidBody(chunk.terrainBody);
}
void PhysicalSceneRegion::disableAll(btSoftRigidDynamicsWorld& world)
{
    for (PhysicalSceneChunk& chunk : m_chunks)
    {
        if (!chunk.isAdded)
            continue;
        chunk.isAdded = false;
        world.removeRigidBody(chunk.terrainBody);
    }
}


// Scene
PhysicalScene::PhysicalScene(uint regionSize)
    : regionSize(regionSize)
{
    // Prepare world
    btBroadphaseInterface* broadphase = new btDbvtBroadphase();
    btDefaultCollisionConfiguration* collisionConfiguration = new btDefaultCollisionConfiguration();
    btCollisionDispatcher* dispatcher = new btCollisionDispatcher(collisionConfiguration);
    btSequentialImpulseConstraintSolver* constraintSolver = new btSequentialImpulseConstraintSolver();

    // Init world
    m_world = new btSoftRigidDynamicsWorld(dispatcher, broadphase, constraintSolver, collisionConfiguration);

    // #HACK
    m_world->setGravity(btVector3(0, -9.8f, 0));
    btVector3 localInertia;

    auto motionState = new btDefaultMotionState();
    motionState->setWorldTransform(btTransform(btQuaternion::getIdentity(), btVector3(-7, 16, -7)));
    auto actorShape = new btCapsuleShape(0.5f, 1.88f);
    actorShape->calculateLocalInertia(1.0f, localInertia);
    _actor = new btRigidBody(1.0f, motionState, actorShape, localInertia);
    _actor->setAngularFactor(0.0f);
    m_world->addRigidBody(_actor);

    auto groundShape = new btStaticPlaneShape(btVector3(0, 1, 0), 0);
    auto ground = new btRigidBody(0.0f, nullptr, groundShape);
    m_world->addRigidBody(ground);
}
PhysicalScene::~PhysicalScene()
{
    // Delete world
    btBroadphaseInterface* broadphase = m_world->getBroadphase();
    btCollisionDispatcher* dispatcher = dynamic_cast<btCollisionDispatcher*>(m_world->getDispatcher());
    btCollisionConfiguration* collisionConfiguration = dispatcher->getCollisionConfiguration();
    btConstraintSolver* constraintSolver = m_world->getConstraintSolver();
    delete m_world;
    delete constraintSolver;
    delete dispatcher;
    delete collisionConfiguration;
    delete broadphase;
}


// Update
void PhysicalScene::update(float deltaTime, DeltaTime deltaTimeMs)
{
    m_world->stepSimulation(deltaTime, 4);
}
void PhysicalScene::enableChunk(const int2& chunkIndex)
{
    int2 regionIndex = static_cast<int2>(floor(static_cast<float2>(chunkIndex) 
                                               / static_cast<float>(regionSize)));
    auto region = getOrDefault(m_regions, chunkIndex);
    if (region)
    {
        // Enable loaded chunk
        region->enable(chunkIndex, *m_world);
    }
    else
    {
        // List chunk
        auto& chunkList = m_chunksToEnable[regionIndex];
        bool added = chunkList.emplace(chunkIndex).second;
        if (!added)
        {
            logWarning("Try to enable non-loaded and already listed chunk ", chunkIndex);
        }
    }
}
void PhysicalScene::disableChunk(const int2& chunkIndex)
{
    int2 regionIndex = static_cast<int2>(floor(static_cast<float2>(chunkIndex) 
                                               / static_cast<float>(regionSize)));
    auto region = getOrDefault(m_regions, chunkIndex);
    if (region)
    {
        // Disable loaded chunk
        region->disable(chunkIndex, *m_world);
    }
    else
    {
        auto& chunkList = m_chunksToEnable[regionIndex];
        bool removed = chunkList.erase(chunkIndex) > 0;
        if (!removed)
        {
            logWarning("Try to disable non-loaded non-listed chunk ", chunkIndex);
        }
    }
}


// Callbacks
void PhysicalScene::onPreloadChunksData(WorldRegion& region)
{
    // Allocate
    region.allocateAsyncStorage<AsyncStorage>(this);
    AsyncStorage& storage = region.getAsyncStorage<AsyncStorage>(this);

    // Prepare
    storage.region = make_shared<PhysicalSceneRegion>(region);
}
void PhysicalScene::onAsyncLoadChunksData(WorldRegion& region)
{
    AsyncStorage& storage = region.getAsyncStorage<AsyncStorage>(this);
    PhysicalSceneRegion& physical = *storage.region;

    physical.load();
}
void PhysicalScene::onPostloadChunksData(WorldRegion& region)
{
    // Get
    AsyncStorage& storage = region.getAsyncStorage<AsyncStorage>(this);

    // Store
    m_regions.emplace(region.index(), storage.region);

    // Enable
    auto& chunkList = m_chunksToEnable[region.index()];
    for (int2 chunkIndex : chunkList)
        storage.region->enable(chunkIndex, *m_world);
    m_chunksToEnable.erase(region.index());

    // Release
    region.releaseAsyncStorage(this);
}
void PhysicalScene::onUnloadChunksData(WorldRegion& region)
{
    auto iterRegion = m_regions.find(region.index());
    debug_assert(iterRegion != m_regions.end());

    PhysicalSceneRegion& physical = *iterRegion->second;
    physical.disableAll(*m_world);

    m_regions.erase(iterRegion);
}
