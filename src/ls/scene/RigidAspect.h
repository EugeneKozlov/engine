/** @file ls/scene/RigidAspect.h
 *  @brief Rigid body physical aspect
 */

#pragma once
#include "ls/common.h"
#include "ls/scene/ObjectInstance.h"
#include "ls/scene/ObjectPrototype.h"
#include "ls/scene/PhysicalAspect.h"
#include "ls/io/Codec.h"
#include "ls/xml/common.h"

namespace ls
{
    /** @addtogroup LsScene
     *  @{
     */
    /** Rigid body aspect prototype
     */
    class RigidAspectPrototype
    : public PhysicalAspectPrototype
    {
    public:
        /// Aspect name
        static constexpr const char* Name()
        {
            return "rigid";
        }
        /// Ctor
        RigidAspectPrototype(XmlNode aspectNode,
                             const char* name = Name(),
                             bool isTiny = true);
    public:
    private:
        Encoding m_amountEncoding;
        Encoding m_flagsEncoding;
        Encoding m_bufEncoding;
    };
    /** Rigid body aspect
     */
    class RigidAspect
    : public PhysicalAspect
    {
    public:
        /** Ctor
         *  @param prototype
         *    Aspect prototype
         */
        RigidAspect(RigidAspectPrototype const& prototype);
        /// Dtor
        virtual ~RigidAspect();

        /** @name Gets
         *  @{
         */
        /// @}
    public:
        virtual void extract(ObjectState& data) override;
        virtual void inject(ObjectState const& data) override;
    protected:
        virtual void implAddToWorld();
        virtual void implRemoveFromWorld();
        virtual void implSetAwakeCallback();
    protected:
        btRigidBody* m_rigidBody = nullptr;
    };
    /// @}
}

