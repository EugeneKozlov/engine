#include "pch.h"
#include "ls/scene/MovableAspect.h"
#include "ls/io/DynamicStream.h"

using namespace ls;

// Prototype
MovableAspectPrototype::MovableAspectPrototype(XmlNode aspectNode,
                                               AspectCategory category,
                                               const char* name,
                                               bool isTiny)
    : AspectPrototype(name, category, isTiny, MovableAspectSlot)
{
    const char* velocityEncodingName = loadNodeAttrib(aspectNode,
                                                      "velocity", "type").as_string();
    const char* torqueEncodingName = loadNodeAttrib(aspectNode,
                                                    "torque", "type").as_string();

    m_velocityEncoding = encodingByName(velocityEncodingName);
    m_torqueEncoding = encodingByName(torqueEncodingName);

    LS_THROW(isDirection3(m_velocityEncoding),
             InvalidConfigException,
             "Invalid velocity [", velocityEncodingName, "]");
    LS_THROW(isDirection3(m_torqueEncoding),
             InvalidConfigException,
             "Invalid torque [", torqueEncodingName, "]");

    m_default.velocity = readDefault<float3, float>(aspectNode, "velocity");
    m_default.torque = readDefault<float3, float>(aspectNode, "torque");

    m_size += sizeOfObject<float3>(m_velocityEncoding);
    m_size += sizeOfObject<float3>(m_torqueEncoding);
}
void MovableAspectPrototype::gatherState(AnyMap& source, AspectState& dest) const
{
    auto& state = dest.cast<MovableAspectState>();

    state.velocity = deref(any_cast<float3>(&source["velocity"]),
                           &m_default.velocity);
    state.torque = deref(any_cast<float3>(&source["torque"]),
                         &m_default.torque);
}
void MovableAspectPrototype::encodeState(AspectState const& source,
                                         DynamicStream& dest) const
{
    auto& state = source.cast<MovableAspectState>();

    encodeObject(m_velocityEncoding, state.velocity, dest);
    encodeObject(m_torqueEncoding, state.torque, dest);

}
void MovableAspectPrototype::decodeState(DynamicStream& source,
                                         AspectState& dest) const
{
    auto& state = dest.cast<MovableAspectState>();

    decodeObject(m_velocityEncoding, state.velocity, source);
    decodeObject(m_torqueEncoding, state.torque, source);
}
void MovableAspectPrototype::interpolateState(AspectState const& left,
                                              AspectState const& right,
                                              AspectState& dest, float factor) const
{
    auto& leftState = left.cast<MovableAspectState>();
    auto& rightState = right.cast<MovableAspectState>();
    auto& destState = dest.cast<MovableAspectState>();

    destState.velocity = lerp(leftState.velocity, rightState.velocity, factor);
    destState.torque = lerp(leftState.torque, rightState.torque, factor);
}


// Aspect
MovableAspect::MovableAspect(MovableAspectPrototype const& prototype)
    : AspectInstance(prototype)
{
}
MovableAspect::~MovableAspect()
{
}
void MovableAspect::extract(ObjectState& /*data*/)
{
}
void MovableAspect::inject(ObjectState const& /*data*/)
{
}

