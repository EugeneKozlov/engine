/** @file ls/scene/ObjectObserverInterface.h
 *  @brief
 */

#pragma once
#include "ls/common.h"
#include "ls/forward.h"

namespace ls
{
    /** @addtogroup LsScene
     *  @{
     */
    /** Object observer interface.
     */
    class ObjectObserverInterface
    {
    public:
        /// Dtor
        virtual ~ObjectObserverInterface() = default;

        /** @name Callbacks
         *  @{
         */
        /// Add object to scene
        virtual void onAdd(ObjectId id) = 0;
        /// Remove object
        virtual void onRemove(ObjectIterator object) = 0;
        /// Rebind object
        virtual void onRebind(ObjectIterator object,
                              int2 const& fromChunk, int2 const& toChunk) = 0;
        /// Add object to acted list (once)
        virtual void onAct(ObjectIterator object) = 0;
        /// @}
    };
}