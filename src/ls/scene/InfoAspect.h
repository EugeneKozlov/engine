/** @file ls/scene/InfoAspect.h
 *  @brief Info aspect
 */

#pragma once
#include "ls/common.h"
#include "ls/scene/ObjectInstance.h"
#include "ls/scene/ObjectPrototype.h"
#include "ls/io/Codec.h"
#include "ls/xml/common.h"

namespace ls
{
    /** @addtogroup LsScene
     *  @{
     */
    /** Info aspect prototype
     */
    class InfoAspectPrototype
    : public AspectPrototype
    {
        LS_ASPECT(InfoAspect);
    public:
        /// Category
        static constexpr AspectCategory Category = AspectCategory::Unspecified;
        /// Aspect name
        static constexpr const char* Name()
        {
            return "info";
        }
        /// Ctor
        InfoAspectPrototype(XmlNode aspectNode,
                            AspectCategory category = Category,
                            const char* name = Name(),
                            bool isTiny = true);
    public:
        virtual void gatherState(AnyMap& source, AspectState& dest) const override;
        virtual void encodeState(AspectState const& source, DynamicStream& dest) const override;
        virtual void decodeState(DynamicStream& source, AspectState& dest) const override;
    private:
        Encoding m_amountEncoding;
        Encoding m_flagsEncoding;
        Encoding m_bufEncoding;
        InfoAspectState m_default;
    };
    /** Info aspect
     */
    class InfoAspect
    : public AspectInstance
    {
    public:
        /// Ctor
        InfoAspect(InfoAspectPrototype const& prototype);
        /// Dtor
        virtual ~InfoAspect();

        /** @name Gets
         *  @{
         */
        /// @}
    public:
        virtual void extract(ObjectState& data) override;
        virtual void inject(ObjectState const& data) override;
    protected:
    };
    /// @}
}

