/// @file ls/scene/core/ObjectPose.h
/// @brief Object global Pose
#pragma once

#include "ls/common.h"

namespace ls
{
    /// @addtogroup LsScene
    /// @{

    /// @brief Object global Pose
    class ObjectPose
    {
    public:
        /// @brief Empty pose
        ObjectPose() noexcept
            : m_position(0.0)
            , m_scaling(1.0f)
        {
        }
        /// @brief By @a position, @a rotation and @a scaling
        ObjectPose(const double3& position,
                   const quat& rotation,
                   float scaling = 1.0f) noexcept
            : m_position(position)
            , m_rotationQuat(rotation)
            , m_rotationMatrix(matrix::rotationQuaternion(rotation))
            , m_scaling(scaling)
        {
        }
        /// @brief Transform position
        double3 transform(const double3& pos) const
        {
            return pos * m_scaling * m_rotationMatrix + m_position;
        }
        /// @brief Transform AABB
        AABB transform(AABB aabb) const
        {
            // Scale
            aabb.min *= m_scaling;
            aabb.max *= m_scaling;

            // Rotate
            aabb *= m_rotationMatrix;

            // Translate
            aabb.min += m_position;
            aabb.max += m_position;

            // Return
            return aabb;
        }

        /// @brief Get position
        const double3& position() const noexcept
        {
            return m_position;
        }
        /// @brief Get local position
        float3 localPosition(const double3& zero) const
        {
            return static_cast<float3>(m_position - zero);
        }
        /// @brief Get rotation quaternion
        const quat& rotationQuat() const noexcept
        {
            return m_rotationQuat;
        }
        /// @brief Get rotation matrix
        const float3x3& rotationMatrix() const noexcept
        {
            return m_rotationMatrix;
        }
        /// @brief Get scaling
        float scaling() const noexcept
        {
            return m_scaling;
        }
        /// @brief Get matrix
        float4x4 matrix(const double3& zero) const noexcept
        {
            float4x4 mat = m_rotationMatrix * m_scaling;
            mat.row(3, static_cast<float3>(m_position - zero));
            return mat;
        }
        /// @brief Get chunk
        int2 chunk(double width) const noexcept
        {
            return static_cast<int2>(floor(swiz<X, Z>(m_position) / width));
        }

        /// @brief Set @a position
        void position(const float3& position, const double3& zero) noexcept
        {
            m_position = static_cast<double3>(position) + zero;
        }
        /// @brief Set @a position
        void position(const double3& position) noexcept
        {
            m_position = position;
        }
        /// @brief Set @a rotation quaternion
        void rotation(const quat& rotation) noexcept
        {
            m_rotationQuat = rotation;
            m_rotationMatrix = matrix::rotationQuaternion(rotation);
        }
        /// @brief Set @a rotation @b normalized matrix
        void rotation(const float3x3& rotation)
        {
            m_rotationMatrix = rotation;
            m_rotationQuat = matrix::extractRotation(rotation);
        }
        /// @brief Set @a scaling
        void scaling(float scaling) noexcept
        {
            m_scaling = scaling;
        }
    protected:
        /// @brief Position
        double3 m_position;
        /// @brief Rotation quaternion
        quat m_rotationQuat;
        /// @brief Rotation matrix
        float3x3 m_rotationMatrix;
        /// @brief Scaling
        float m_scaling;
    };
    /// @}
}