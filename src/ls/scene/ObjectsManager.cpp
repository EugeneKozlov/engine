#include "pch.h"
#include "ls/scene/ObjectsManager.h"

using namespace ls;

// Observer
void ObjectsManagerObserverInterface::onCreateObject(ObjectIterator /*object*/)
{
}
void ObjectsManagerObserverInterface::onDestroyObject(ObjectIterator /*object*/)
{
}
void ObjectsManagerObserverInterface::onCreateGhost(ObjectIterator /*object*/)
{
}
void ObjectsManagerObserverInterface::onDestroyGhost(ObjectIterator /*object*/)
{
}
void ObjectsManagerObserverInterface::onRebindObject(ObjectIterator /*object*/,
                                                     int2 const& /*from*/, int2 const& /*to*/)
{
}
void ObjectsManagerObserverInterface::onAddObject(ObjectInstance& /*object*/)
{
}
void ObjectsManagerObserverInterface::onRemoveObject(ObjectInstance& /*object*/)
{
}


// Chunk container
ChunkInterface::ChunkInterface()
    : m_chunk(GhostChunk)
    , m_objects()
    , m_actedMap()
{
}
class ObjectsManager::Chunk
: public ChunkInterface
{
public:
    /// Ctor
    Chunk()
    {
    }
    /// Initialize
    void init(int2 const& chunk)
    {
        m_chunk = chunk;
    }
    /// Allocate
    void allocate()
    {
        m_allocated = true;
    }
    /// Deallocate
    void deallocate()
    {
        m_allocated = false;
    }


    /// Add new object
    void addObject(ObjectIterator const& object)
    {
        auto& target = !object->isGlobal() ? m_objects : m_globals;
        bool wasAdded = target.insert(object).second;

        debug_assert(wasAdded, "object is already added");
    }
    /// Remove object.
    void removeObject(ObjectIterator const& object)
    {
        auto& target = !object->isGlobal() ? m_objects : m_globals;
        bool wasRemoved = target.erase(object) != 0;

        debug_assert(wasRemoved, "object is not added");
    }
    /// Add acted object
    void actObject(ObjectIterator const& object)
    {
        if (object->isGlobal())
            return;
        debug_assert(contain(m_objects, object), "object is not added");
        m_actedMap.insert(object);
    }
    /// Remove acted object
    bool disactObject(ObjectIterator const& object)
    {
        if (object->isGlobal())
            return false;
        debug_assert(contain(m_objects, object), "object is not added");
        return m_actedMap.erase(object) != 0;
    }
    /// Find object
    ObjectIterator find(ObjectInstance const& object)
    {
        auto finder = [&object](ObjectIterator const& iterObject) -> bool
        {
            return object.id() == iterObject.index();
        };
        auto iterObject = std::find_if(m_objects.begin(),
                                       m_objects.end(),
                                       finder);
        debug_assert(iterObject != m_objects.end(), "object is not found");
        return *iterObject;
    }
    /// Clear chunk
    void clear()
    {
        resetActed();
        m_objects.clear();
    }
    /// @}
};

/// Chunks group (region) container.
class ObjectsManager::Region
: public Noncopyable
{
public:
    /// Ctor.
    Region(uint regionWidth, int2 const& regionIndex)
        : m_chunks(regionWidth, regionWidth)
    {
        int2 chunkIndex;
        int2 size = m_chunks.size();
        for (chunkIndex.x = 0; chunkIndex.x < size.x; ++chunkIndex.x)
            for (chunkIndex.y = 0; chunkIndex.y < size.y; ++chunkIndex.y)
                m_chunks[chunkIndex].init(regionIndex * regionWidth + chunkIndex);
    }
    /// Get chunk.
    Chunk& operator [](int2 const& theIndex)
    {
        return m_chunks[theIndex];
    }
protected:
    /// Chunks array
    FlatArray<Chunk> m_chunks;
};
void ChunkInterface::resetActed()
{
    m_actedMap.clear();
}


// Manager
ObjectsManager::ObjectsManager(uint chunkWidth, uint regionWidth,
                               bool logObjects)
    : Nameable<ObjectsManager>("Objects storage")
    , m_chunkWidth(chunkWidth)
    , m_regionWidth(regionWidth)
    , m_logObjects(logObjects)
{
    logInfo("Storage is created");
}
ObjectsManager::~ObjectsManager()
{
    m_logObjects = false;
    for (ObjectInstance& object : m_objectsPool)
        object.destroy();
    logInfo("Storage is destroyed");
}


// Chunks
void ObjectsManager::allocateChunk(int2 const& chunk)
{
    Chunk& impl = findChunk(chunk, true);
    if (!impl.isAllocated())
    {
        for (ObjectIterator object : impl.objects())
            initializeObject(*object);
    }
    impl.allocate();
}
void ObjectsManager::destroyChunk(int2 const& chunk)
{
    Chunk* impl = findChunkUnsafe(chunk);
    if (!impl)
        return;
    if (impl->isAllocated())
    {
        for (ObjectIterator object : impl->objects())
            deinitializeObject(*object);
    }
    impl->allocate();
}


// Objects
void ObjectsManager::onAdd(ObjectId id)
{
    if (!isObject(id))
        addObject(id);

    ObjectIterator object = findObject(id);
    ObjectInstance& ref = *object;

    if (!ref.isGhost())
    {
        if (ref.isGlobal())
        {
            m_globalObjects.insert(object);
            initializeObject(*object);
        }

        m_reboundObjects.insert(id);

        // Inform
        for (Observer* observer : m_observers)
            observer->onCreateObject(object);
    }
    else
    {
        // Inform
        for (Observer* observer : m_observers)
            observer->onCreateGhost(object);
    }
    // Log
    logObject(ref, "is created");
}
void ObjectsManager::onRebind(ObjectIterator object,
                              int2 const& fromChunk, int2 const& toChunk)
{
    // Log
    logObject(*object, "has been rebound "
                  "from ", fromChunk, " to ", toChunk);

    // Move from old position to new
    Chunk& sourceChunk = findChunk(fromChunk, fromChunk == toChunk);
    Chunk& destChunk = findChunk(toChunk, true);

    // Init
    if (destChunk.isAllocated())
        initializeObject(*object);

    // Add to new
    destChunk.addObject(object);
    // Act if needed
    if (sourceChunk.disactObject(object))
        destChunk.actObject(object);
    // Remove from previous
    if (fromChunk != toChunk)
        sourceChunk.removeObject(object);

    // Inform
    m_reboundObjects.insert(object->id());
    for (Observer* observer : m_observers)
        observer->onRebindObject(object, fromChunk, toChunk);
}
void ObjectsManager::onRemove(ObjectIterator object)
{
    debug_assert(!!object);
    ObjectInstance& ref = *object;

    if (!ref.isGhost())
    {
        if (ref.isGlobal())
            m_globalObjects.erase(object);

        // De-init
        if (ref.isInitalized())
            deinitializeObject(ref);

        // Chunk
        int2 chunkIndex = ref.chunk();
        Chunk& chunk = findChunk(chunkIndex, false);
        chunk.disactObject(object);
        chunk.removeObject(object);

        // Inform
        for (Observer* observer : m_observers)
            observer->onDestroyObject(object);

        // Log
        logObject(ref, "has been removed from ", chunkIndex);
    }
    else
    {
        // Inform
        for (Observer* observer : m_observers)
            observer->onDestroyGhost(object);

        // Log
        logObject(*object, "has been removed");
    }

    // Kill
    destroyObject(object.index());
}
void ObjectsManager::onAct(ObjectIterator object)
{
    debug_assert(!!object);
    // Get chunk
    int2 chunkIndex = object->chunk();
    Chunk* chunk = findChunkUnsafe(chunkIndex);
    // Bad call
    if (!chunk)
    {
        logWarning("Try to record action of incorrectly bound to "
                       , chunkIndex, " object [", object.index(), "]");
    }
        // Mark as acted
    else
    {
        chunk->actObject(object);
    }
}
ObjectIterator ObjectsManager::addObject(ObjectId id)
{
    debug_assert(!isObject(id), "occupied object ID");

    // Init
    if (id == InvalidObjectId)
        id = m_objectsPool.add();
    else
        m_objectsPool.add(id);
    ObjectIterator object = m_objectsPool.iterator(id);

    object->startup(*this, object, m_chunkWidth);

    return object;
}
void ObjectsManager::destroyObject(ObjectId object)
{
    m_reboundObjects.erase(object);
    m_objectsPool.remove(object);
}
void ObjectsManager::initializeObject(ObjectInstance& object)
{
    for (Observer* observer : m_observers)
        observer->onAddObject(object);

    logObject(object, "is allocated");
}
void ObjectsManager::deinitializeObject(ObjectInstance& object)
{
    for (Observer* observer : m_observers)
        observer->onRemoveObject(object);

    logObject(object, "is deallocated");
}
void ObjectsManager::removeObject(ObjectIterator object)
{
    object->destroy();
}
void ObjectsManager::removeObject(ObjectId object)
{
    findObject(object)->destroy();
}
ObjectIterator ObjectsManager::findObject(ObjectId id)
{
    auto iterObject = m_objectsPool.iterator(id);
    if (!!iterObject)
        return iterObject;
    else
        return ObjectIterator();
}
ObjectInstance& ObjectsManager::getObject(ObjectId id)
{
    auto iterObject = findObject(id);
    debug_assert(!!iterObject, "object is not found");
    return *iterObject;
}
bool ObjectsManager::isObject(ObjectId id)
{
    return m_objectsPool.isElement(id);
}
bool ObjectsManager::wasRebound(ObjectId theID, bool theClear)
{
    if (theClear)
        return !!m_reboundObjects.erase(theID);
    else
        return contain(m_reboundObjects, theID);
}
void ObjectsManager::clearChunk(int2 const& chunk)
{
    // Chunk
    Chunk& ref = findChunk(chunk, true);
    logDebug("Chunk ", chunk, " is cleared");

    // Objects
    for (ObjectIterator object : ref.objects())
    {
        for (Observer* observer : m_observers)
            observer->onDestroyObject(object);
        logDebug("Object [", object->id(), ":", object->type(), "] is cleared");
        destroyObject(object->id());
    }
    ref.clear();
}
int2 ObjectsManager::chunk2region(int2 const& globalChunk, int2& localChunk)
{
    sint width = (sint) m_regionWidth;
    localChunk = globalChunk % width;
    if (localChunk.x < 0) localChunk.x += width;
    if (localChunk.y < 0) localChunk.y += width;
    return (globalChunk - localChunk) / width;
}


// Detail
ObjectsManager::RegionMap::iterator
ObjectsManager::findArea(int2 const& chunk,
                         int2& localChunkIndex, int2& regionIndex,
                         bool canCreateRegion)
{
    regionIndex = chunk2region(chunk, localChunkIndex);
    auto iterRegion = m_regions.find(regionIndex);
    if (iterRegion == m_regions.end())
    {
        debug_assert(canCreateRegion, regionIndex);
        // New area
        m_regions[regionIndex].reset(new Region(m_regionWidth, regionIndex));
        // Inform about new chunk
        return m_regions.find(regionIndex);
    }
    return iterRegion;
}
ObjectsManager::RegionMap::iterator
ObjectsManager::findRegionUnsafe(int2 const& chunk,
                                 int2& localChunkIndex, int2& areaIndex)
{
    areaIndex = chunk2region(chunk, localChunkIndex);
    return m_regions.find(areaIndex);
}
ObjectsManager::Chunk* ObjectsManager::findChunkUnsafe(int2 const& chunk)
{
    int2 localChunkIndex, areaIndex;
    auto area = findRegionUnsafe(chunk, localChunkIndex, areaIndex);
    return (area == m_regions.end()) ? nullptr : &(*area->second)[localChunkIndex];
}
ObjectsManager::Chunk& ObjectsManager::findChunk(int2 const& chunk,
                                                 bool canCreateRegion)
{
    int2 localChunkIndex, areaIndex;
    return (*findArea(chunk, localChunkIndex, areaIndex, canCreateRegion)->second)
        [localChunkIndex];
}


// Gets
ChunkInterface& ObjectsManager::chunk(int2 const& theChunk)
{
    return static_cast<ChunkInterface&> (findChunk(theChunk, true));
}
ObjectsList& ObjectsManager::globalObjects()
{
    return m_globalObjects;
}


// Debug
void ObjectsManager::debugPlot(function<void(ObjectInstance& object, float4x4 const& matrix) > plotter,
                               double3 const& zero, double3 const& viewer, uint distance)
{
    ObjectsList list;
    auto painter = [&plotter, zero](ObjectsList& objects)
    {
        for (auto& iterObject : objects)
        {
            auto& object = *iterObject;
            if (!object.isExist() || object.isGhost())
                continue;
            float4x4 pose = object.globalPose().matrix(zero);
        }
    };
    // Global
    painter(m_globalObjects);
    // Chunk
    sint dist = (sint) distance;
    int2 center(sint(floor(viewer.x / m_chunkWidth)),
                sint(floor(viewer.z / m_chunkWidth)));
    int2 chunkGlobal, chunkLocal;
    for (chunkGlobal.x = center.x - dist; chunkGlobal.x <= center.x + dist; ++chunkGlobal.x)
        for (chunkGlobal.y = center.y - dist; chunkGlobal.y <= center.y + dist; ++chunkGlobal.y)
        {
            int2 squareDist2 = abs(chunkGlobal - center);
            sint squareDist = max(squareDist2.x, squareDist2.y) + 1;
            if (squareDist > dist)
                continue;
            int2 area = chunk2region(chunkGlobal, chunkLocal);
            auto iterArea = m_regions.find(area);
            if (iterArea == m_regions.end())
                continue;
            auto& chunk = (*iterArea->second)[chunkLocal];
            painter(chunk.objects());
        }
}
Pool<ObjectInstance> const& ObjectsManager::debugPool()
{
    return m_objectsPool;
}
