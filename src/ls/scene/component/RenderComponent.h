/// @file ls/scene/component/RenderComponent.h
/// @brief Render Component
#pragma once

#include "ls/common.h"
#include "ls/asset/ModelAsset.h"
#include "ls/io/DynamicStream.h"
#include "ls/scene/Component.h"
#include "ls/scene/Entity.h"
#include "ls/scene/component/ModelComponent.h"

namespace ls
{
    class ModelComponent;
    struct ModelSystemObjectHandle;

    /// @addtogroup LsScene
    /// @{

    LS_DECLARE_COMPONENT(Render);
    /// @brief Render Component
    class RenderComponent final
        : public ComponentT<ComponentType::Render, RenderComponent>
    {
    public:
        using Base::Base;
        EntityId parentEntity = InvalidEntityId; ///< Parent entity ID
        ModelSystemObjectHandle* handle = nullptr; ///< Internal handle

        /// @name Model switch
        /// @{
        ModelComponent* primaryModel = nullptr; ///< First model (fade in)
        ModelComponent* secondaryModel = nullptr; ///< Second model (fade out)
        uint primaryLod = ModelAsset::MaxLods; ///< First model LOD
        uint secondaryLod = ModelAsset::MaxLods; ///< Second model LOD
        float mixFactor = 0.0f; ///< Mix factor (0 is primary model, 1 is secondary)
        /// @}
    public:
        /// @brief Serialize
        void serialize(Archive<Buffer> archive)
        {
            ls::serialize<Encoding::Int>(archive, parentEntity);
        }
    };

    /// @}
}