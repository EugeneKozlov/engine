/// @file ls/scene/component/all.h
/// @brief All components
#pragma once

#include "ls/common.h"
#include "ls/scene/component/CameraComponent.h"
#include "ls/scene/component/ChunkStreamingComponent.h"
#include "ls/scene/component/GrassComponent.h"
#include "ls/scene/component/HeightfieldTerrainComponent.h"
#include "ls/scene/component/ModelComponent.h"
#include "ls/scene/component/RenderComponent.h"
#include "ls/scene/component/TransformComponent.h"
