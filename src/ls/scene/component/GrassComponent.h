/// @file ls/scene/component/GrassComponent.h
/// @brief Height-field Geometry Component
#pragma once

#include "ls/common.h"
#include "ls/io/Codec.h"
#include "ls/scene/Component.h"

namespace ls
{
    class ModelAsset;

    /// @addtogroup LsScene
    /// @{

    LS_DECLARE_COMPONENT(Grass);
    /// @brief Height-field Geometry Component
    ///
    /// This component allows to add surface geometry to the main height field terrain
    /// @note Add this component to any entity in the scene
    class GrassComponent
        : public ComponentT<ComponentType::Grass, GrassComponent>
    {
    public:
        using Base::Base;
        /// @brief Model asset name
        ResourceName assetName;
        /// @brief Model asset
        shared_ptr<ModelAsset> asset;

        /// @brief Minimal distance between instances
        float minDistance = 1.0f;
    public:
        void serialize(Archive<Buffer> archive)
        {
            ls::serialize<Encoding::Byte>(archive, assetName.cast());
            ls::serialize<Encoding::Float>(archive, minDistance);
        }
    };
    /// @}
}