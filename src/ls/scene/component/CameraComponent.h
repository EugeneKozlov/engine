/// @file ls/scene/component/CameraComponent
/// @brief Camera Component
#pragma once

#include "ls/common.h"
#include "ls/scene/Component.h"

namespace ls
{
    /// @addtogroup LsScene
    /// @{

    LS_DECLARE_COMPONENT(Camera);
    /// @brief Camera Component
    class CameraComponent
        : public ComponentT<ComponentType::Camera, CameraComponent>
    {
    public:
        using Base::Base;
        /// @brief Rendering mode
        enum class Rendering : ubyte
        {
            /// @brief Use deferred shading when rendering from this camera
            Deferred
        };
        /// @brief Projection mode
        enum class Projection : ubyte
        {
            /// @brief Perspective
            /// @note @a fieldOfView is vertical view angle
            Perspective
        };
    public:
        /// @brief Parameters to be exported to render thread
        struct Param
        {
            Rendering lighting = Rendering::Deferred; ///< Lighting mode
            Projection projection = Projection::Perspective; ///< Projection mode
            float fieldOfView = 60.0f; ///< Field of view for perspective projection
        } param; ///< Renderer parameters
    public:
        void serialize(Archive<Buffer> archive)
        {
            serializeBinary<Rendering>(archive, param.lighting);
            serializeBinary<Projection>(archive, param.projection);
            serializeBinary<float>(archive, param.fieldOfView);
        }
    };
    /// @}
}