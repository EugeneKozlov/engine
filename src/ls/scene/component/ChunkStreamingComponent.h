/// @file ls/scene/component/StreamingComponent.h
/// @brief Streaming Component
#pragma once

#include "ls/common.h"
#include "ls/scene/Component.h"

namespace ls
{
    /// @addtogroup LsScene
    /// @{

    LS_DECLARE_COMPONENT(ChunkStreaming);
    /// @brief Chunk Streaming Component
    class ChunkStreamingComponent
        : public ComponentT<ComponentType::ChunkStreaming, ChunkStreamingComponent>
    {
    public:
        using Base::Base;
    public:
        void serialize(Archive<Buffer> archive)
        {
        }
    };
    /// @}
}