/// @file ls/scene/component/HeightfieldTerrainComponent.h
/// @brief Height-field Terrain Component
#pragma once

#include "ls/common.h"
#include "ls/scene/Component.h"

namespace ls
{
    /// @addtogroup LsScene
    /// @{

    LS_DECLARE_COMPONENT(HeightfieldTerrain);
    /// @brief Height-field Terrain Component
    ///
    /// World can have only one streaming height-field terrain
    class HeightfieldTerrainComponent
        : public ComponentT<ComponentType::HeightfieldTerrain, HeightfieldTerrainComponent>
    {
    public:
        using Base::Base;
    public:
        void serialize(Archive<Buffer> archive)
        {
        }
    };
    /// @}
}