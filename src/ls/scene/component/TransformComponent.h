/// @file ls/scene/component/TransformComponent.h
/// @brief Transform Component
#pragma once

#include "ls/common.h"
#include "ls/io/Codec.h"
#include "ls/io/DynamicStream.h"
#include "ls/scene/Component.h"
#include "ls/scene/core/ObjectPose.h"

namespace ls
{
    /// @addtogroup LsScene
    /// @{

    LS_DECLARE_COMPONENT(Transform);
    /// @brief Transform Component
    class TransformComponent final
        : public ComponentT<ComponentType::Transform, TransformComponent>
    {
    public:
        /// @brief Flag
        enum Flag
        {
            /// @brief Multiple transforms
            MultipleTransforms,
        };
    public:
        using Base::Base;
        /// @brief Flags
        FlagSet<Flag> flags;
        /// @brief Object Pose
        ObjectPose pose;
        /// @brief Transforms
        vector<float4x4> transforms;
    public:
        /// @brief Serialize
        void serialize(Archive<Buffer> archive)
        {
            if (archive.isWrite())
            {
                encode<Encoding::VectorFixed4>(archive.stream(), pose.position());
                encode<Encoding::RotationPaq>(archive.stream(), pose.rotationQuat());
                encode<Encoding::Half>(archive.stream(), pose.scaling());
            }
            else
            {
                double3 position;
                quat rotation;
                float scaling;
                decode<Encoding::VectorFixed4>(archive.stream(), position);
                decode<Encoding::RotationPaq>(archive.stream(), rotation);
                decode<Encoding::Half>(archive.stream(), scaling);
                pose = ObjectPose(position, rotation, scaling);
            }
        }
    };

    /// @}
}