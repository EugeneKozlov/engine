/// @file ls/scene/component/ModelComponent.h
/// @brief Model Component
#pragma once

#include "ls/common.h"
#include "ls/asset/ModelAsset.h"
#include "ls/io/Codec.h"
#include "ls/scene/Component.h"

namespace ls
{
    class RenderComponent;
    class TransformComponent;
    struct ModelSystemModelHandle;

    /// @addtogroup LsScene
    /// @{

    LS_DECLARE_COMPONENT(Model);

    /// @brief Model Component
    class ModelComponent final
        : public ComponentT<ComponentType::Model, ModelComponent>
    {
    public:
        using Base::Base;

        // #TODO rename to 'model'
        ResourceName assetName; ///< Model asset name
        shared_ptr<ModelAsset> asset; ///< Model asset
        ModelSystemModelHandle* handle = nullptr; ///< Internal handle for renderer

        /// @name Loaded from model asset by ModelSystem
        /// @{
        array<float, ModelAsset::MaxLods> innerDistances2; ///< Inner distances ^2
        array<float, ModelAsset::MaxLods> outerDistances2; ///< Outer distances ^2
        ubyte numLods = 0; ///< Number of LODs
        /// @}

    public:
        /// @brief Serialize
        void serialize(Archive<Buffer> archive)
        {
            ls::serialize<Encoding::Byte>(archive, assetName.cast());
        }
    };

    /// @}
}