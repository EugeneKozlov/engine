#include "pch.h"
#include "ls/scene/ObjectsFactory.h"
#include "ls/io/DynamicStream.h"
#include "ls/io/FileSystemInterface.h"
#include "ls/io/Codec.h"

using namespace ls;


// Main
ObjectsFactory::ObjectsFactory()
{
}
ObjectsFactory::~ObjectsFactory()
{
}


// Proto
ObjectPrototype& ObjectsFactory::findPrototype(const char* name)
{
    auto id = m_protoNames.find(name);
    return prototype((id != m_protoNames.end()) ? id->second : InvalidObjectType);
}
ObjectPrototype& ObjectsFactory::prototype(ObjectType id)
{
    auto proto = m_prototypes.find(id);
    LS_THROW(proto != m_prototypes.end(),
             BadArgException,
             "Prototype [", id, "] not found");
    return proto->second;
}
void ObjectsFactory::loadProto(XmlNode nodeProto)
{
    using namespace pugi;
    // ID and name
    ObjectType typeId = loadAttrib(nodeProto, "id").as_uint();
    string typeName = loadAttrib(nodeProto, "name").as_string();
    // Skip example
    if (typeId == InvalidObjectType)
        return;
    LS_THROW(typeId > 0,
             InvalidConfigException,
             "Negative prototype ID <", fullName(nodeProto), "> "
             ": id=[", typeId, "], name=[", typeName, "]>");

    // Parse traits
    try
    {
        m_prototypes.emplace(typeId, ObjectPrototype(nodeProto, typeId));
        m_protoNames.emplace(typeName, typeId);
    }
    catch (InvalidConfigException const& ex)
    {
        LS_THROW(0,
                 InvalidConfigException,
                 "Prototype reading error <", fullName(nodeProto), "> : "
                 "id = [", typeId, "]; "
                 "name = [", typeName, "]; "
                 "text = [", ex.what(), "]");
    }
}
void ObjectsFactory::inject(string const& xml)
{
    using namespace pugi;

    // Load
    XmlDocument xmlDocument;
    xml_parse_result result = xmlDocument.load(xml.c_str());
    LS_THROW(result.status == status_ok,
             InvalidConfigException,
             "Bad prototype XML: ", (uint) result.status);

    // Parse
    for (XmlNode nodePrototypes : xmlDocument.children("prototype"))
    {
        for (XmlNode nodeProto : nodePrototypes.children("proto"))
            loadProto(nodeProto);
    }
}
void ObjectsFactory::read(FileSystemInterface& fileSystem, string const& name)
{
    auto source = fileSystem.openStream(name, StreamAccess::Read);
    LS_THROW(source,
             BadArgException,
             "Prototype file [", name, "] is not found");
    string data = source->text();
    inject(data);
}
void ObjectsFactory::resetParameter(const char* name)
{
    if (name)
        m_initArray.erase(name);
    else
        m_initArray.clear();
}


// Encode
uint ObjectsFactory::describe(ObjectType type,
                              void* streamData, uint streamSize,
                              bool resetParameters)
{
    ObjectPrototype& proto = prototype(type);
    DynamicStream stream(streamData, streamSize);
    if (!stream.canWrite(proto.size() + sizeof (ObjectType) + sizeof (uint)))
        return 0;

    ObjectInfo handle(InvalidObjectId, proto.flags());
    // Encode
    ls::encode<Encoding::Int>(stream, handle.encode());
    ls::encode<Encoding::Int>(stream, type);
    proto.describe(stream, m_initArray);

    // Reset
    if (resetParameters)
        resetParameter();
    return stream.size();
}
bool ObjectsFactory::build(ObjectState& state,
                           ObjectId id, ObjectPrototype& proto,
                           bool resetParameters)
{
    // Create
    proto.gather(state, id, m_initArray);
    state.materialize();
    // Reset
    if (resetParameters)
        resetParameter();
    return true;
}
uint ObjectsFactory::encode(const ObjectState* defaultState,
                            ObjectState const& objectState,
                            void*streamData, uint streamSize, bool encodeAll)
{
    // Find prototype
    ObjectPrototype& proto = prototype(objectState.type());
    // States mask to encode
    ObjectStateFlag mask = encodeAll ? proto.flags() : objectState.flags();
    // Encoding data size
    uint size = encodeAll ? proto.size() : proto.size(mask);
    // Object handle
    ObjectInfo handle(objectState.id(), mask);
    // Data stream
    DynamicStream stream(streamData, streamSize);
    // Check data size
    if (!stream.canWrite(size + sizeof (ObjectType) + sizeof (uint)))
        return 0;
    // Write type
    ls::encode<Encoding::Int>(stream, handle.encode());
    ls::encode<Encoding::Int>(stream, objectState.type());
    // Encode object
    if (!proto.encode(objectState, defaultState, stream, mask, size))
        return 0;

    // Return valid descriptor
    return stream.numWritten();
}
uint ObjectsFactory::decode(ObjectState& objectState,
                            const void* streamData, uint streamSize)
{
    // Open and check stream
    DynamicStream stream(streamData, streamSize);
    if (!stream.canRead(sizeof (ObjectType)))
        return 0;
    // Handle
    uint encodedHandle;
    ls::decode<Encoding::Int>(stream, encodedHandle);
    ObjectInfo handle(encodedHandle);
    // Read type
    ObjectType realType;
    ObjectStateFlag realMask = handle.state;
    ls::decode<Encoding::Int>(stream, realType);
    // Find proto
    ObjectPrototype& proto = prototype(realType);
    // Calc size
    uint realSize = proto.size(realMask);
    // Too few data
    if (!stream.canRead(realSize))
        return 0;
    // Invalid type
    if (!realType)
        return 0;
    // Init frame
    objectState.construct(proto, handle.id);
    objectState.materialize();
    // Decode
    proto.decode(objectState, stream, realMask, realSize);

    return stream.numRead();
}
bool ObjectsFactory::prepare(ObjectInstance& object, ObjectState const& objectState)
{
    // If object is empty
    bool isStateComplete = objectState.isComplete();
    bool isObjectComplete = object.isValid() && (objectState.type() == object.type());
    // All good, no preparation required
    if (isObjectComplete)
        return true;
    // If there is not enough data, the only way is...
    if (!isStateComplete)
        return false;
    // Prototype
    ObjectPrototype& proto = prototype(objectState.type());
    // Construct object
    object.reconstruct(objectState);
    return true;
}
