/// @file ls/scene/graphical/TerrainBuffer.h
/// @brief Terrain Buffer
#pragma once

#include "ls/common.h"
#include "ls/asset/WorldAsset.h"
#include "ls/gapi/common.h"
#include "ls/world/WorldObserverInterface.h"
#include "ls/scene/graphical/TerrainTree.h"

namespace ls
{
    class Renderer;
    class GraphicConfig;

    /// @addtogroup LsScene
    /// @{

    /// @brief Terrain Region Buffer
    class TerrainRegionBuffer
        : Noncopyable
    {
    public:
        /// @brief Map Formats
        enum Map
        {
            /// @brief Height Map
            HeightMap,
            /// @brief Normal Map
            NormalMap,
            /// @brief Color Map
            ColorMap,
            /// @brief #0 Blend Map
            BlendMap0,
            /// @brief #1 Blend Map
            BlendMap1,
            /// @brief #2 Blend Map
            BlendMap2,
            /// @brief #3 Blend Map
            BlendMap3,
            /// @brief Number of maps
            MapCOUNT,

            /// @brief Blend texture
            SlotBlend = BlendMap0 + 0,
            /// @brief Noise texture
            SlotNoise = SlotBlend + 1,
            /// @brief Diffuse texture #0
            SlotDiffuse0 = SlotBlend + 2,
            /// @brief Diffuse texture #1
            SlotDiffuse1 = SlotBlend + 3,
        };
        /// @brief Formats
        static const Format TexelFormat[MapCOUNT];
        /// @brief Mipmap flag
        static const bool GenerateMipmap[MapCOUNT];
        /// @brief Async buffer
        struct Async
        {
            /// @brief Per-map data array
            array<vector<ubyte>, MapCOUNT> buffers;
        };
    public:
        /// @brief Renderer
        Renderer& renderer;
        /// @brief Is readable?
        const bool isReadable;
        /// @brief Region
        WorldRegion* currentRegion = nullptr;

        /// @brief Chunk sizes
        array<uint, MapCOUNT> chunkSizes;
        /// @brief Map sizes
        array<uint, MapCOUNT> mapSizes;
        /// @brief Map row strides
        array<uint, MapCOUNT> mapRowStrides;
        /// @brief Map texel sizes
        array<uint, MapCOUNT> texelSizes;
        /// @brief Maps textures
        array<Texture2DHandle, MapCOUNT> textures;
        /// @brief Base resource bucket
        ResourceBucket resourcesBase;
        /// @brief Detail resource bucket
        ResourceBucket resourcesDetail;
        /// @brief Render target
        RenderTargetHandle renderTarget;

        /// @brief Tree node
        shared_ptr<TerrainTreeRegion> treeNode;
    public:
        /// @brief Ctor
        TerrainRegionBuffer(Renderer& renderer, const WorldRegion::Config& config,
                            Texture2DHandle cachedColor, bool readable);
        /// @brief Dtor
        ~TerrainRegionBuffer();

        /// @brief Get reosurce bucket contains blend map for specified slice
        const ResourceBucket& sliceResources(uint slice)
        {
            resourcesDetail.popResource();
            resourcesDetail.addResource(SlotBlend, textures[BlendMap0 + slice], ShaderType::Pixel);
            return resourcesDetail;
        }

        /// @brief Update buffers
        void update(WorldRegion& region);
        /// @brief Split commits
        void split(uint numPortions);
        /// @brief Commit one map
        void commitMap(Map map);
        /// @brief Commit one portion
        bool commitOnce();
        /// @brief Commit all portions
        void commitAll();
        /// @brief Extract one map
        void extractMap(Map map);
        /// @brief Extract all maps from GAPI
        void extractAll();
        /// @brief Read one map
        void readMap(Map map, WorldRegion& region);
        /// @brief Rewrite chunks
        void readAll(WorldRegion& region);
        /// @brief Clear maps
        void clearAll(float height);
    private:
        void commitMapPortion(Map map, uint portion, uint numPortions);
    private:
        /// @brief Buffers
        array<vector<ubyte>, MapCOUNT> m_buffers;
        /// @brief Number of commits
        uint m_numCommits = 1;
        /// @brief Number of portions
        uint m_numPortions = 1;

        /// @brief Current portion
        uint m_currentCommit = 0;
    };
    /// @brief Terrain Region Buffer Pool
    class TerrainRegionBufferPool
        : Noncopyable
    {
    public:
        /// @brief Renderer
        Renderer& renderer;
    public:
        /// @brief Ctor
        TerrainRegionBufferPool(Renderer& renderer, Texture2DHandle cachedColor);
        /// @brief Allocate buffer
        TerrainRegionBuffer& receiveBuffer(WorldRegion& region);
        /// @brief Release buffer
        void releaseBuffer(TerrainRegionBuffer& buffer);
    private:
        Texture2DHandle m_cachedColor;

        list<TerrainRegionBuffer> m_pool;
        vector<TerrainRegionBuffer*> m_unused;
    };
    /// @brief Terrain Clip Map
    class TerrainClipMap
        : Noncopyable
    {
    public:
        /// @brief Renderer
        Renderer& renderer;
        /// @brief Region width
        const uint regionWidth;
        /// @brief Cache size
        const uint cacheSize;
    public:
        /// @brief Ctor
        TerrainClipMap(Renderer& renderer, uint regionWidth, uint cacheSize);
        /// @brief Dtor
        ~TerrainClipMap();
        /// @brief Move
        void move(const int2& regionIndex);
        /// @brief Update
        bool update(const int2& regionIndex, Texture2DHandle source);

        /// @brief Is region stored?
        bool isStored(const int2& regionIndex) const;
        /// @brief Get begin
        int2 beginRegion() const
        {
            return m_begin;
        }
        /// @brief Get end
        int2 endRegion() const
        {
            return m_end;
        }
        /// @brief Get texture
        Texture2DHandle texture() const
        {
            return m_texture;
        }
        /// @brief Get cache size
        uint textureSize() const
        {
            return cacheSize * regionWidth;
        }
    private:
        uint m_width;
        Texture2DHandle m_texture;
        int2 m_begin;
        int2 m_end;
    };
    /// @brief Terrain Buffer
    class TerrainBuffer
        : Noncopyable
        , public WorldObserverInterface
    {
    public:
        /// @brief Renderer
        Renderer& renderer;
        /// @brief Number of portions
        const uint numPortions;
        /// @brief Chunks multilist
        TerrainTreeChunksList chunkList;
    public:
        /// @brief Ctor
        TerrainBuffer(Renderer& renderer,
                      const WorldRegion::Config& config);
        /// @brief Dtor
        ~TerrainBuffer();

        /// @brief Update
        void flush();

        /// @brief Get tree
        TerrainTree& tree()
        {
            return m_tree;
        }
        /// @brief Get region 
        TerrainRegionBuffer* region(const int2& regionIndex);
        /// @brief Get detail cache
        TerrainClipMap& detailCache()
        {
            return m_detailCache;
        }
    public:
        virtual void onLoadRegionData(WorldRegion& region) override;
        virtual void onUnloadRegionData(WorldRegion& region) override;
        virtual void onPreloadChunksData(WorldRegion& region) override;
        virtual void onAsyncLoadChunksData(WorldRegion& region) override;
        virtual void onPostloadChunksData(WorldRegion& region) override;
        virtual void onUnloadChunksData(WorldRegion& region) override;
        virtual void onUpdateAllRegionData(const int2& currentRegion) override;
    private:
        void finalizeBuffer(TerrainRegionBuffer& buffer);
    private:
        TerrainClipMap m_detailCache;

        TerrainRegionBufferPool m_pool;
        vector<pair<int2, TerrainRegionBuffer*>> m_incomingRegions;
        hamap<int2, TerrainRegionBuffer*> m_loadedRegions;
        hamap<int2, TerrainRegionBuffer*> m_cachedRegions;

        TerrainTree m_tree;
    };
    /// @}
}

