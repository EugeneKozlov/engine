#include "pch.h"
#include "ls/scene/graphical/GraphicalMaterial.h"
#include "ls/asset/AssetManagerOld.h"
#include "ls/render/Texture2D.h"

using namespace ls;


// Sets
Material::Material(AssetManagerOld& manager, const string& surfaceShader)
    : m_manager(manager)
    , m_surfaceShader(surfaceShader)
{
}
void Material::loadTextures(Renderer& renderer)
{
    if (m_loaded)
        return;
    for (uint idx = 0; idx < MaxShaderResources; ++idx)
    {
        if (m_textureNames[idx].empty())
            continue;

        auto texture = m_manager.get<Texture2D>(m_textureNames[idx]);
        if (!texture)
            continue;

        texture->create(renderer);
        texture->wrap(isWrapped);

        m_textures[idx] = texture;
        m_resourceBucket.addResource(m_textureSlots[idx], texture->texture(), m_textureShaders[idx]);
    }
    m_loaded = true;
}
shared_ptr<Texture2D> Material::texture(uint idx)
{
    debug_assert(idx < m_numTextures);
    return m_textures[idx];
}
ResourceBucket const& Material::resources()
{
    debug_assert(m_loaded);
    return m_resourceBucket;
}
