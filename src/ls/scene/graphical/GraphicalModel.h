/// @file ls/scene/graphical/GraphicalModel.h
/// @brief Scene Object Graphical Model
#pragma once

#include "ls/common.h"
#include "ls/scene/graphical/limits.h"
#include "ls/scene/graphical/GraphicalMaterial.h"

namespace ls
{
    class AssetManagerOld;
    class Mesh;
    class Renderer;

    /// @addtogroup LsScene
    /// @{

    /// @brief Graphical Model Materials Array
    using GraphicalModelMaterialsArray = array<shared_ptr<Material>, MaxModelSubsets>;
    /// @brief Graphical Model
    class GraphicalModel
        : Noncopyable
        , public DisposableConcept
    {
    public:
        /// @brief External user index
        uint userIndex = 0;
        /// @brief Lod description
        struct Lod
        {
            /// @brief Fade-in distance
            double fadeIn;
            /// @brief Fade-out distance
            double fadeOut;
        };
    public:
        /// @brief Ctor
        GraphicalModel(AssetManagerOld& manager);
        /// @brief Dtor
        ~GraphicalModel();
        /// @brief Add lod
        void addLod(const Lod& lod, const string& meshName,
                    uint numSubsets, const GraphicalModelMaterialsArray& materials);

        /// @brief Load geometry and materials for specified LOD
        void loadGeometry(uint lod, Renderer& renderer);
        /// @brief Get mesh (load if needed)
        Mesh* mesh(uint lod);

        /// @brief Get number of LODs
        uint numLods() const
        {
            return m_numLods;
        }
        /// @brief Get number of subsets of LOD
        uint numSubsets(uint lod) const;
        /// @brief Get LOD description
        const Lod& lod(uint lod) const;
        /// @brief Get material of subset of LOD
        Material* material(uint lod, uint subset);
    private:
        struct LodData : Lod
        {
            bool isLoaded = false;

            string meshName;
            shared_ptr<Mesh> mesh;

            uint numSubsets = 0;
            GraphicalModelMaterialsArray materials;
        };
    private:
        AssetManagerOld& m_manager;

        array<LodData, MaxModelLods> m_lods;
        uint m_numLods = 0;
    };
    /// @}
}

