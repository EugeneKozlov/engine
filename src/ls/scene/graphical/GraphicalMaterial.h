/// @file ls/scene/graphical/GraphicalMaterial.h
/// @brief Scene Object Graphical Material
#pragma once

#include "ls/common.h"
#include "ls/gapi/common.h"

namespace ls
{
    class AssetManagerOld;
    class Texture2D;
    class Renderer;

    /// @addtogroup LsScene
    /// @{

    /// @brief Scene Object Graphical Material
    class Material
        : Noncopyable
    {
    public:
        /// @brief Is transparent?
        bool isTransparent = false;
        /// @brief Is translucent?
        bool isTranslucent = false;
        /// @brief Is wrapped?
        bool isWrapped = false;
    public:
        /// @brief Ctor
        Material(AssetManagerOld& manager, const string& surfaceShader);

        /// @brief Load textures
        void loadTextures(Renderer& renderer);

        /// @brief Get texture by index
        shared_ptr<Texture2D> texture(uint idx);
        /// @brief Get resource bucket
        const ResourceBucket& resources();
        /// @brief Get shader name
        const string& surfaceShader() const
        {
            return m_surfaceShader;
        }
    public:
        /// @brief Add texture (internal)
        bool internalAddTexture(ShaderType shader, uint slot, const string& name)
        {
            if (m_numTextures == MaxShaderResources)
                return false;
            uint idx = m_numTextures++;

            m_textureSlots[idx] = slot;
            m_textureShaders[idx] = shader;
            m_textureNames[idx] = name;
            return true;
        }
    private:
        AssetManagerOld& m_manager;
        bool m_loaded = false;

        string m_surfaceShader;

        uint m_numTextures = 0;
        array<uint, MaxShaderResources> m_textureSlots;
        array<ShaderType, MaxShaderResources> m_textureShaders;
        array<string, MaxShaderResources> m_textureNames;
        array<shared_ptr<Texture2D>, MaxShaderResources> m_textures;

        ResourceBucket m_resourceBucket;
    };

    using Material = Material;
    /// @}
}
