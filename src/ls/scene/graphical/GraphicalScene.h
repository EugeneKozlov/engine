/// @file ls/scene/graphical/GraphicalScene.h
/// @brief Graphic Scene
#pragma once

#include "ls/common.h"
#include "ls/render/DrawingPolicy.h"
#include "ls/render/CascadedShadowMap.h"

namespace ls
{
    class GraphicConfig;
    class Camera;
    class RenderSystem;

    /// @addtogroup LsScene
    /// @{

    /// @brief Graphic Scene
    class GraphicalScene
        : Noncopyable
    {
    public:
        /// @brief Render System Iterator Range
        using RenderSystemRange = pair<vector<RenderSystem*>::iterator, vector<RenderSystem*>::iterator>;
    public:
        /// @brief Ctor
        GraphicalScene(const GraphicConfig& graphicConfig);
        /// @brief Dtor
        ~GraphicalScene();
        /// @brief Update projection
        void updateViewport(float width, float height);
        /// @brief Recenter
        void recenter(const double3& zero);

        /// @brief Set main camera
        void mainCamera(Camera& camera);
        /// @brief Get main camera
        Camera& mainCamera();

        /// @brief Add render systems
        void addRenderSystem(RenderSystem& renderSystem);
        /// @brief Get render systems
        RenderSystemRange renderSystems();

        /// @brief Get cascaded shadow map helper
        CascadedShadowMap& cascadedShadowMapHelper();
    public:
        /// @brief Graphic config
        const GraphicConfig& graphicConfig;
    private:
        void updateMainCameraParameters(Camera& camera);
    private:
        // Camera
        float m_viewportWidth = 0;
        float m_viewportHeight = 0;
        double3 m_worldZero = 0.0;
        Camera* m_mainCamera = nullptr;

        // Objects
        vector<RenderSystem*> m_renderSystems;
        CascadedShadowMap m_cascadedShadowMapHelper;
    };
    /// @}
}

