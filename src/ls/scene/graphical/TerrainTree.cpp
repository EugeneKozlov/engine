#include "pch.h"
#include "ls/scene/graphical/TerrainTree.h"

using namespace ls;

// Node
TerrainTreeNode::TerrainTreeNode(ubyte level, uint maxDepth,
                                 const int2& localIndex, const int2& globalIndex, 
                                 const AABB& aabb)
    : localIndex(localIndex)
    , globalIndex(globalIndex)
    , level(level)
    , isLeaf(level == maxDepth)

    , aabb(aabb)
    , currentLevel(0)
{
}
void TerrainTreeNode::splitThis(AABB children[4])
{
    // Max center
    double3 center = (aabb.min + aabb.max) / 2;
    center.y = aabb.max.y;
    // Min borders
    double3 borders[4] = {
        double3(aabb.min.x, aabb.min.y, aabb.min.z),
        double3(aabb.max.x, aabb.min.y, aabb.min.z),
        double3(aabb.min.x, aabb.min.y, aabb.max.z),
        double3(aabb.max.x, aabb.min.y, aabb.max.z),
    };
    // Split
    for (uint i = 0; i < 4; ++i)
    {
        children[i] = center;
        children[i] += borders[i];
    }
}
void TerrainTreeNode::allocate(vector<TerrainTreeNode>& pool, uint maxDepth,
                               const FlatArray<TerrainTreeChunkDesc>& chunksInfo)
{
    // Leaf
    info.index = globalIndex;
    if (level == maxDepth)
    {
        const TerrainTreeChunkDesc& desc = chunksInfo[localIndex];
        aabb.min.y = desc.bounds.x;
        aabb.max.y = desc.bounds.y;

        info.numLayers = desc.numLayers;
        info.layers = desc.materials;
        return;
    }
    else
    {
        info.numLayers = 0;
        info.layers.fill(0);
    }

    // Boxes
    AABB childrenBoxes[4];
    splitThis(childrenBoxes);

    // Parent and children
    ubyte childLevel = level + 1;
    int2 baseLocalIndex = localIndex * 2;
    int2 baseGlobalIndex = globalIndex * 2;

    // Allocate children
    pool.emplace_back(childLevel, maxDepth, 
                      baseLocalIndex + int2(0, 0), baseGlobalIndex + int2(0, 0), childrenBoxes[0]);
    next[0][0] = &pool.back();
    pool.emplace_back(childLevel, maxDepth, 
                      baseLocalIndex + int2(1, 0), baseGlobalIndex + int2(1, 0), childrenBoxes[1]);
    next[1][0] = &pool.back();
    pool.emplace_back(childLevel, maxDepth, 
                      baseLocalIndex + int2(0, 1), baseGlobalIndex + int2(0, 1), childrenBoxes[2]);
    next[0][1] = &pool.back();
    pool.emplace_back(childLevel, maxDepth, 
                      baseLocalIndex + int2(1, 1), baseGlobalIndex + int2(1, 1), childrenBoxes[3]);
    next[1][1] = &pool.back();

    // Init
    for (uint i = 0; i < 4; ++i)
        next[i % 2][i / 2]->allocate(pool, maxDepth, chunksInfo);

    // Update aabbs
    aabb.min.y = next[0][0]->aabb.min.y;
    aabb.max.y = next[0][0]->aabb.max.y;
    for (uint i = 1; i < 4; ++i)
    {
        const AABB& childAabb = next[i % 2][i / 2]->aabb;
        aabb.min.y = min(aabb.min.y, childAabb.min.y);
        aabb.max.y = max(aabb.max.y, childAabb.max.y);
    }
}
void TerrainTreeNode::updateChunkLods(bool resetCurrentLod, const double3& position,
                                      const double innerDistances2[], const double outerDistances2[])
{
    // Update this
    double dist2 = square_distance(aabb, position);
    bool wasCurrentLod = isCurrentLod;
    if (isCurrentLod || resetCurrentLod)
        isCurrentLod = dist2 >= innerDistances2[level];
    else
        isCurrentLod = dist2 >= outerDistances2[level];
    currentLevel = level;

    // Update distance
    if (isLeaf)
    {
        info.distance = sqrt(dist2);
    }

    // Go deeper
    if (!isCurrentLod)
    {
        debug_assert(!isLeaf);
        for (uint x = 0; x < 2; ++x)
            for (uint y = 0; y < 2; ++y)
            {
                next[x][y]->updateChunkLods(wasCurrentLod || resetCurrentLod, 
                                            position, 
                                            innerDistances2, outerDistances2);
            }
    }
    else if (isCurrentLod && !wasCurrentLod)
    {
        fillChunkLods(level);
    }
}
void TerrainTreeNode::fillChunkLods(uint value)
{
    currentLevel = value;
    isCurrentLod = currentLevel == level;
    if (isLeaf)
        return;
    for (uint x = 0; x < 2; ++x)
        for (uint y = 0; y < 2; ++y)
            next[x][y]->fillChunkLods(value);
}
void TerrainTreeNode::findChunks(TerrainTreeRegionChunksList& chunks, const Frustum& frustum) const
{
    // Break if invisible
    if (!frustum.isAabbIntersect(aabb))
        return;

    if (!isCurrentLod)
    {
        // Submerge
        for (uint x = 0; x < 2; ++x)
            for (uint y = 0; y < 2; ++y)
            {
                next[x][y]->findChunks(chunks, frustum);
            }
    }
    else
    {
        // Add chunk
        chunks.perLevelLists[level].push_back(info);
    }
}
void TerrainTreeNode::updateEdges(TerrainTreeNode* xneg, TerrainTreeNode* xpos,
                                  TerrainTreeNode* zneg, TerrainTreeNode* zpos)
{
    if (isCurrentLod)
    {
        // Update edges
        info.edges.x = !xneg ? 0 : (sint) (currentLevel - xneg->currentLevel);
        info.edges.y = !xpos ? 0 : (sint) (currentLevel - xpos->currentLevel);
        info.edges.z = !zneg ? 0 : (sint) (currentLevel - zneg->currentLevel);
        info.edges.w = !zpos ? 0 : (sint) (currentLevel - zpos->currentLevel);
    }
    else
    {
        debug_assert(!isLeaf);
        // Go deeper
        next[0][0]->updateEdges(!xneg ? nullptr : xneg->next[1][0], next[1][0],
                                !zneg ? nullptr : zneg->next[0][1], next[0][1]);
        next[1][0]->updateEdges(next[0][0], !xpos ? nullptr : xpos->next[0][0], 
                                !zneg ? nullptr : zneg->next[1][1], next[1][1]);
        next[0][1]->updateEdges(!xneg ? nullptr : xneg->next[1][1], next[1][1],
                                next[0][0], !zpos ? nullptr : zpos->next[0][0]);
        next[1][1]->updateEdges(next[0][1], !xpos ? nullptr : xpos->next[0][1],
                                next[1][0], !zpos ? nullptr : zpos->next[1][0]);
    }
}

// Region
TerrainTreeRegion::TerrainTreeRegion(uint maxDepth, const int2& globalIndex, double regionSize,
                                             const FlatArray<TerrainTreeChunkDesc>& chunksInfo)
    : TerrainTreeNode(0, maxDepth, int2(0, 0), globalIndex, 
                      AABB(static_cast<double3>(swiz<X, O, Y>(globalIndex)) * regionSize,
                           static_cast<double3>(swiz<X, O, Y>(globalIndex + int2(1))) * regionSize))
{
    // Count nodes
    uint numNodes = 0;
    for (ubyte idx = 1; idx <= maxDepth; ++idx)
        numNodes += 1 << (2 * idx);

    // Allocate
    m_pool.clear();
    m_pool.reserve(numNodes);
    allocate(m_pool, maxDepth, chunksInfo);
}



// Base
ubyte TerrainTree::computeChunkDepth(uint regionWidth, uint chunkWidth)
{
    ubyte depth = 0;
    while (chunkWidth < regionWidth)
    {
        regionWidth /= 2;
        ++depth;
    }
    return depth;
}
TerrainTree::TerrainTree(uint regionWidth, uint chunkWidth)
    : regionWidth(regionWidth)
    , chunkWidth(chunkWidth)
    , chunkDepth(computeChunkDepth(regionWidth, chunkWidth))
{
}
TerrainTree::~TerrainTree()
{
}
shared_ptr<TerrainTreeRegion> TerrainTree::createRegion(const int2& regionIndex,
                                                        const FlatArray<TerrainTreeChunkDesc>& chunksInfo) const
{
    return make_shared<TerrainTreeRegion>(chunkDepth, regionIndex, regionWidth, chunksInfo);
}
void TerrainTree::addRegion(shared_ptr<TerrainTreeRegion> region)
{
    debug_assert(!!region);
    m_regions.emplace(region->globalIndex, region);
}
void TerrainTree::removeRegion(const int2& regionIndex)
{
    m_regions.erase(regionIndex);
}


// Process
void TerrainTree::updateChunks(const double3& position,
                              const double innerDistances2[], const double outerDistances2[])
{
    for (auto& iterRoot : m_regions)
    {
        TerrainTreeNode& root = *iterRoot.second;
        root.updateChunkLods(false, position, innerDistances2, outerDistances2);
    }
    for (auto& iterRoot : m_regions)
    {
        TerrainTreeNode& root = *iterRoot.second;
        int2 rootIndex = iterRoot.first;

        root.updateEdges(getOrDefault(m_regions, rootIndex + int2(-1, 0)).get(),
                         getOrDefault(m_regions, rootIndex + int2(+1, 0)).get(),
                         getOrDefault(m_regions, rootIndex + int2(0, -1)).get(),
                         getOrDefault(m_regions, rootIndex + int2(0, +1)).get());
    }
}
void TerrainTree::findChunks(TerrainTreeChunksList& chunks, const Frustum& frustum) const
{
    for (auto& iterRegion : m_regions)
    {
        const int2& regionIndex = iterRegion.first;
        const TerrainTreeRegion& region = *iterRegion.second;

        // Skip invalid 
        auto& list = chunks[regionIndex];
        if (!list.isInitialized())
            continue;

        // Clear
        for (auto& layer : list.detailed)
            layer.clear();
        for (auto& level : list.perLevelLists)
            level.clear();

        // Find
        region.findChunks(list, frustum);
    }
}


// Debug
void TerrainTreeNode::debugPrintCurrentLevel(const int2& baseIndex, string& buffer, uint stride)
{
    if (isLeaf)
    {
        int2 idx = globalIndex - baseIndex;
        if (currentLevel <= 9)
            buffer[idx.x + idx.y * stride] = char('0' + currentLevel);
        else
            buffer[idx.x + idx.y * stride] = char('A' + currentLevel - 10);
    }
    else
    {
        for (uint x = 0; x < 2; ++x)
            for (uint y = 0; y < 2; ++y)
            {
                next[x][y]->debugPrintCurrentLevel(baseIndex, buffer, stride);
            }
    }
}
string TerrainTree::debugPrintCurrentLevel(const int2& rootIndex)
{
    TerrainTreeRegion* rootNode = getOrDefault(m_regions, rootIndex).get();
    if (!rootNode)
        return "";
    string buffer;
    uint width = 1 << chunkDepth;
    for (uint y = 0; y < width; ++y)
    {
        for (uint x = 0; x < width; ++x)
            buffer += '-';
        buffer += '\n';
    }
    rootNode->debugPrintCurrentLevel(rootIndex * width, buffer, width + 1);
    return buffer;
}
