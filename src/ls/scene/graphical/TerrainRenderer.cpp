#include "pch.h"
#include "ls/scene/graphical/TerrainRenderer.h"

#include "ls/asset/TextureAsset.h"
#include "ls/render/BatchRenderer.h"
#include "ls/render/Camera.h"
#include "ls/render/SceneView.h"
#include "ls/scene/graphical/TerrainBuffer.h"
using namespace ls;

// Generate
uint TerrainRenderer::generateVertices(uint width, uint scaling,
                                       vector<TerrainVertex>& buffer)
{
    for (uint z = 0; z <= width; ++z)
        for (uint x = 0; x <= width; ++x)
        {
            TerrainVertex v(int2(x * scaling, z * scaling),
                            float2(static_cast<float>(x) * scaling,
                                   static_cast<float>(z) * scaling),
                            int2(x == width, z == width));
            buffer.push_back(v);

        }
    return (width + 1) * (width + 1);
}
uint TerrainRenderer::generateIndices(uint width,
                                      vector<Quad>& buffer,
                                      const int4& wrapping,
                                      uint baseVertex)
{
    int4 wrapScale(1 << wrapping.x, 1 << wrapping.y, 1 << wrapping.z, 1 << wrapping.w);

    for (uint z = 0; z < width; ++z)
    {
        for (uint x = 0; x < width; ++x)
        {
            Quad q(width, baseVertex, int2(x, z), int2(x + 1, z), int2(x + 1, z + 1), int2(x, z + 1));
            // Left
            if (wrapping.x && x == 0)
            {
                sint ws = wrapScale.x;
                sint d = (ws - (z + 1) % ws) % ws;
                if (z % ws != 0)
                    q.v0.y += d + 1;
                q.v3.y += d;
            }
            // Right
            if (wrapping.y && x == width - 1)
            {
                sint ws = wrapScale.y;
                sint d = (ws - (z + 1) % ws) % ws;
                if (z % ws != 0)
                    q.v1.y += d + 1;
                q.v2.y += d;
            }
            // Bottom
            if (wrapping.z && z == 0)
            {
                sint ws = wrapScale.z;
                sint d = (ws - (x + 1) % ws) % ws;
                if (x % ws != 0)
                    q.v0.x += d + 1;
                q.v1.x += d;
            }
            // Top
            if (wrapping.w && z == width - 1)
            {
                sint ws = wrapScale.w;
                sint d = (ws - (x + 1) % ws) % ws;
                if (x % ws != 0)
                    q.v3.x += d + 1;
                q.v2.x += d;
            }
            buffer.push_back(q);
        }
    }
    return width * width;
}
vector<TerrainRenderer::TerrainIndex> TerrainRenderer::generateTriangles(const vector<Quad>& indices)
{
    vector<TerrainIndex> triIndices(indices.size() * 6, 0);
    uint j = 0;
    for (auto& i : indices)
    {
        auto index = [&i](int2 v)
        {
            return uint(v.x + v.y * (i.width + 1));
        };
        // Generate tri
        triIndices[j++] = index(i.v0) + i.base;
        triIndices[j++] = index(i.v2) + i.base;
        triIndices[j++] = index(i.v1) + i.base;
        triIndices[j++] = index(i.v0) + i.base;
        triIndices[j++] = index(i.v3) + i.base;
        triIndices[j++] = index(i.v2) + i.base;
    }
    return triIndices;
}
vector<TerrainRenderer::TerrainIndex> TerrainRenderer::generateQuads(const vector<Quad>& indices)
{
    vector<TerrainIndex> quadIndices(indices.size() * 4, 0);
    uint j = 0;
    for (auto& i : indices)
    {
        auto index = [&i](int2 v)
        {
            return uint(v.x + v.y * (i.width + 1));
        };
        // Generate quad
        quadIndices[j++] = index(i.v0) + i.base;
        quadIndices[j++] = index(i.v1) + i.base;
        quadIndices[j++] = index(i.v2) + i.base;
        quadIndices[j++] = index(i.v3) + i.base;
    }
    return quadIndices;
}


// Main
TerrainRenderer::TerrainRenderer(shared_ptr<TerrainBuffer> terrainBuffer,
                                 shared_ptr<TerrainEffect> terrainEffect,
                                 const WorldRegion::Config& worldConfig,
                                 const WorldStreamingConfig& streamingConfig,
                                 bool editable)
    : renderer(terrainBuffer->renderer)
    , editable(editable)
    , worldConfig(worldConfig)
    , streamingConfig(streamingConfig)

    , chunkWidth(worldConfig.chunkWidth)
    , packWidth(4)
    , regionWidth(worldConfig.regionInChunks)

    , m_terrainBuffer(terrainBuffer)
    , m_terrainEffect(terrainEffect)
{
    initConfig();
    initEffect();

    initVertexData();
    initIndexData();
    //initDistances(100.0, 2 / 3.0);
    initDistances(30.0, 60.0, 2 / 3.0);
    initLods();
    setWire(float3(1.0f), 0.01f);

    // Init modes
    initSimpleRenderer();
    if (renderer.hasTessellation())
    {
        initTessellationRenderer();
    }

    // Initialize
    setTessellation(false);
}
void TerrainRenderer::initConfig()
{
    uint minChunkCacheSize = (2 * *streamingConfig.packLoadingDist + *streamingConfig.packLoadingInertia + 1) * packWidth;
    uint minRegionCacheSize = (2 * *streamingConfig.regionLoadingDist + *streamingConfig.regionLoadingInertia + 1);
    LS_THROW(minChunkCacheSize <= *streamingConfig.chunkCacheSize,
             LogicException,
             "Too small chunk cache size, should be at least [", minChunkCacheSize, "] chunks");
    LS_THROW(minRegionCacheSize <= *streamingConfig.regionCacheSize,
             LogicException,
             "Too small region cache size, should be at least [", minRegionCacheSize, "] regions");
}
void TerrainRenderer::initEffect()
{
    EffectHandle effect = m_terrainEffect->effect();
    m_handleConstant = renderer.createUniformBuffer(sizeof(Constant));
    m_handlePerView = renderer.createUniformBuffer(sizeof(PerView));
    m_handlePerBatch = renderer.createUniformBuffer(sizeof(PerBatch));
    m_handlePerPrimitive = renderer.createUniformBuffer(sizeof(PerPrimitive));
    renderer.attachUniformBuffer(effect, m_handleConstant, "Constant");
    renderer.attachUniformBuffer(effect, m_handlePerView, "PerView");
    renderer.attachUniformBuffer(effect, m_handlePerBatch, "PerLod");
    renderer.attachUniformBuffer(effect, m_handlePerPrimitive, "PerChunk");

    // Init shaders
    for (auto shader : enumerate(TerrainEffect::TerrainDetailBasePass, TerrainEffect::NumShaders))
        m_shaders[shader] = &m_terrainEffect->shader(shader);
}
void TerrainRenderer::initVertexData()
{
    vector<TerrainVertex> vertices;
    generateVertices(chunkWidth, 1, vertices);

    // Gapi
    m_vertexBuffer = renderer.createBuffer(vertices.size() * sizeof(TerrainVertex),
                                             BufferType::Vertex, ResourceUsage::Constant,
                                             vertices.data());
}
void TerrainRenderer::initIndexData()
{
    // Generate indices
    uint offset = 0;
    for (uint x = 0; x <= 2; ++x)
        for (uint y = 0; y <= 2; ++y)
            for (uint z = 0; z <= 2; ++z)
                for (uint w = 0; w <= 2; ++w)
                {
                    Wrap& wrap = m_wraps[x][y][z][w];
                    wrap.numQuads = generateIndices(chunkWidth,
                                                    m_indexData,
                                                    int4(x, y, z, w),
                                                    0);
                    wrap.startQuad = offset;
                    offset += wrap.numQuads;
                }
}
void TerrainRenderer::initSimpleRenderer()
{
    RendererImpl& mode = m_rendererModes[false];
    mode.numCellIndices = 6;

    // Buffers
    vector<TerrainIndex> indices = generateTriangles(m_indexData);
    mode.indexBuffer = renderer
        .createBuffer(indices.size() * sizeof(TerrainIndex),
                      BufferType::Index, ResourceUsage::Constant, indices.data());

    // Geometry
    mode.geometry.indexBuffer = mode.indexBuffer;
    mode.geometry.indexFormat = TerrainIndexFormat;
    mode.geometry.numVertexBuffers = 1;
    mode.geometry.vertexBuffers[0] = m_vertexBuffer;
}
void TerrainRenderer::initTessellationRenderer()
{
    RendererImpl& mode = m_rendererModes[true];

    mode.numCellIndices = 6;

    // Buffers
    vector<TerrainIndex> indices = generateQuads(m_indexData);
    mode.indexBuffer = renderer
        .createBuffer(indices.size() * sizeof(TerrainIndex),
                      BufferType::Index, ResourceUsage::Constant, indices.data());

    // Geometry
    mode.geometry.indexBuffer = mode.indexBuffer;
    mode.geometry.indexFormat = TerrainIndexFormat;
    mode.geometry.numVertexBuffers = 1;
    mode.geometry.vertexBuffers[0] = m_vertexBuffer;
}
void TerrainRenderer::initDistances(double detail, double firstInner, double innerOffset)
{
    m_detailDrawDistance = detail;

    // Base distance
    double outerPackDistance = (*streamingConfig.packLoadingDist - sqrt(2.0))
        * packWidth * chunkWidth;
    uint packLod = log2u(packWidth);
    uint maxLod = log2u(regionWidth);
    assert(packLod > 0);

    // Compute factor
    double firstOuter = firstInner / innerOffset;
    double factor = pow(outerPackDistance / firstOuter, 1.0 / packLod);
    m_innerDistances.resize(maxLod + 1);
    m_outerDistances.resize(maxLod + 1);

    // Leaf has no inner/outer
    m_innerDistances[maxLod] = 0;
    m_outerDistances[maxLod] = 0;

    double outer = firstOuter;
    double inner = firstInner;
    for (sint lod = (sint) maxLod - 1; lod >= 0; --lod)
    {
        m_innerDistances[lod] = inner;
        m_outerDistances[lod] = outer;
        double prevOuter = outer;
        outer *= factor;
        inner = lerp(prevOuter, outer, innerOffset);
    }

    // Pow2
    for (double& dist : m_innerDistances)
        dist *= dist;
    for (double& dist : m_outerDistances)
        dist *= dist;

    // Set
    m_numLods = maxLod + 1;
}
void TerrainRenderer::initLods()
{
    uint width = chunkWidth;
    for (uint lod = 0; lod < m_numLods; ++lod)
    {
        LodInfo info;

        // Init params
        info.width = width;
        info.scale = 1 << lod;

        m_lods.push_back(info);
        width *= 2;
    }
}


// Destroy
TerrainRenderer::~TerrainRenderer()
{
    destroyRenderingMode(m_rendererModes[false]);
    destroyRenderingMode(m_rendererModes[true]);
    renderer.destroyBuffer(m_vertexBuffer);

    renderer.destroyBuffer(m_handleConstant);
    renderer.destroyBuffer(m_handlePerView);
    renderer.destroyBuffer(m_handlePerBatch);
    renderer.destroyBuffer(m_handlePerPrimitive);
}
void TerrainRenderer::destroyRenderingMode(RendererImpl& mode)
{
    if (!!mode.indexBuffer)
        renderer.destroyBuffer(mode.indexBuffer);

    mode = RendererImpl();
}


// Render
void TerrainRenderer::render(BatchRenderer& batchRenderer, const SceneView* sceneView)
{
    debug_assert(sceneView);
    if (sceneView->policy == CascadedShadowPolicy)
    {
        return;
    }

    // Update LODs
    const Camera& camera = sceneView->camera;
    m_terrainBuffer->tree().updateChunks(camera.position(),
                                         m_innerDistances.data(), m_outerDistances.data());

    // Update visible
    m_terrainBuffer->tree().findChunks(m_terrainBuffer->chunkList, sceneView->frustum);

    // Update per-frame
    PerView perView;
    perView.view = camera.viewMatrix();
    perView.viewProj = camera.viewProjMatrix();
    batchRenderer.commitBuffer(m_handlePerView, perView);

    // Non-detailed pass
    for (auto& regionInfo : m_terrainBuffer->chunkList)
    {
        const int2& regionIndex = regionInfo.first;
        TerrainRegionBuffer* region = m_terrainBuffer->region(regionIndex);
        if (!region)
            continue;
        const double2 regionBegin = static_cast<double2>(regionIndex) * static_cast<double>(worldConfig.regionWidth);
        auto& multilist = regionInfo.second;

        for (uint lod = 0; lod < m_numLods; ++lod)
        {
            renderChunkList(*sceneView, batchRenderer, lod, multilist.detailed,
                            regionBegin, m_shaderCachedColor, region->sliceResources(0),
                            multilist.perLevelLists[m_numLods - 1 - lod]);
        }
    }

    // Detailed base pass
    for (auto const& regionInfo : m_terrainBuffer->chunkList)
    {
        const int2& regionIndex = regionInfo.first;
        TerrainRegionBuffer* region = m_terrainBuffer->region(regionIndex);
        if (!region)
            continue;
        const double2 regionBegin = static_cast<double2>(regionIndex) * static_cast<double>(worldConfig.regionWidth);
        const auto& multilist = regionInfo.second;

        renderChunkListDetailed(*sceneView, batchRenderer, 0, regionBegin,
                                m_shaderDetailBasePass, region->sliceResources(0),
                                multilist.detailed[0]);
    }

    // Detailed second pass
    for (auto const& regionInfo : m_terrainBuffer->chunkList)
    {
        const int2& regionIndex = regionInfo.first;
        TerrainRegionBuffer* region = m_terrainBuffer->region(regionIndex);
        if (!region)
            continue;
        const double2 regionBegin = static_cast<double2>(regionIndex) * static_cast<double>(worldConfig.regionWidth);
        const auto& multilist = regionInfo.second;

        for (uint layer = 1; layer < WorldRegion::MaxBlendLayers; ++layer)
        {
            renderChunkListDetailed(*sceneView, batchRenderer, layer, regionBegin,
                                    m_shaderDetailSecondPass, region->sliceResources(layer),
                                    multilist.detailed[layer]);
        }
    }
}
void TerrainRenderer::setNoiseTexture(shared_ptr<TextureAsset> texture)
{
    m_noiseTextureHolder = texture;
    m_noiseTexture = texture->gapiResource();

    renderer.setTexture2DSampler(texture->gapiResource(), Filter::Anisotropic, Addressing::Wrap, 2);
}
void TerrainRenderer::resetTextures()
{
    m_diffuseMapsHolder.clear();
    m_diffuseMaps.clear();
}
void TerrainRenderer::setTexture(uint slot, shared_ptr<TextureAsset> diffuse)
{
    m_diffuseMapsHolder[slot] = diffuse;
    m_diffuseMaps[slot] = diffuse->gapiResource();

    renderer.setTexture2DSampler(diffuse->gapiResource(), Filter::Anisotropic, Addressing::Wrap, 2);
}
void TerrainRenderer::setWire(const float3& color, float thickness)
{
    m_constant.wireColor = color;
    m_constant.wireThreshold = thickness;
    renderer.writeBuffer(m_handleConstant, m_constant);
}
bool TerrainRenderer::setTessellation(bool tessellated)
{
    if (tessellated && !renderer.hasTessellation())
        return false;
    m_tessellated = tessellated;

    // Init shaders
    if (!m_tessellated)
    {
        m_shaderDetailBasePass = TerrainEffect::TerrainDetailBasePass;
        m_shaderDetailSecondPass = TerrainEffect::TerrainDetailSecondPass;
        m_shaderCachedColor = TerrainEffect::TerrainCachedColor;
    }
    else
    {
        m_shaderDetailBasePass = TerrainEffect::TerrainDetailBasePassTess;
        m_shaderDetailSecondPass = TerrainEffect::TerrainDetailSecondPassTess;
        m_shaderCachedColor = TerrainEffect::TerrainCachedColorTess;
    }
    return true;
}
void TerrainRenderer::renderChunkList(const SceneView& sceneView, BatchRenderer& batchRenderer,
                                      uint lod, TerrainTreeDetailedChunksList& detailed,
                                      const double2& regionBegin, TerrainEffect::Shader shader,
                                      const ResourceBucket& regionMaps, const vector<TerrainTreeChunk>& chunkList)
{
    // Extract detailed
    uint numDetailedChunks = 0;
    if (lod == 0)
    {
        for (TerrainTreeChunk const& chunk : chunkList)
        {
            // Skip far
            if (chunk.distance > m_detailDrawDistance)
                continue;

            // Delay draw
            for (uint layer = 0; layer < chunk.numLayers; ++layer)
                detailed[layer].push_back(chunk);
            ++numDetailedChunks;
        }
    }

    // Skip if all detailed
    if (numDetailedChunks == chunkList.size())
        return;

    // Current renderer mode
    const TerrainEffect::Trait& shaderTraits = TerrainEffect::shaderTrait(shader);
    RendererImpl& currentRendererMode = m_rendererModes[shaderTraits.isTessellated];

    // Lod info
    LodInfo& lodInfo = m_lods[lod];

    // Init state and geometry
    batchRenderer.setPipelineState(m_shaders[shader]->pipelineState(sceneView));
    batchRenderer.setGeometry(currentRendererMode.geometry);

    // Fill resources
    ResourceBucket resources = regionMaps;
    resources.addResource(TerrainRegionBuffer::SlotNoise, m_noiseTexture, ShaderType::Pixel);
    batchRenderer.setResources(resources);

    // Init batch const
    TerrainClipMap& detailCache = m_terrainBuffer->detailCache();
    PerBatch perBatch;
    perBatch.regionBegin = static_cast<float2>(regionBegin - swiz<X, Z>(sceneView.camera.worldCenter()));
    perBatch.cacheOffset = static_cast<float2>(swiz<X, Z>(sceneView.camera.worldCenter()) % detailCache.textureSize());
    perBatch.regionSize = static_cast<float>(worldConfig.regionWidth);
    perBatch.cacheSize = static_cast<float>(detailCache.textureSize());
    perBatch.density = 1.0f;
    batchRenderer.commitBuffer(m_handlePerBatch, perBatch);

    // Render all chunks
    for (const TerrainTreeChunk& chunk : chunkList)
    {
        if (lod == 0 && chunk.distance <= m_detailDrawDistance)
            continue;

        // Draw
        renderChunk(chunk.index, chunk.edges,
                    lodInfo.scale, currentRendererMode.numCellIndices,
                    sceneView, batchRenderer);
    }
}
void TerrainRenderer::renderChunkListDetailed(const SceneView& sceneView, BatchRenderer& batchRenderer,
                                              uint layer,
                                              const double2& regionBegin, TerrainEffect::Shader shader,
                                              const ResourceBucket& regionMaps, const vector<TerrainTreeChunk>& chunkList)
{
    // Skip empty
    if (chunkList.empty())
        return;

    // Current renderer mode
    const TerrainEffect::Trait& shaderTraits = TerrainEffect::shaderTrait(shader);
    debug_assert(shaderTraits.isDetailed);
    RendererImpl& currentRendererMode = m_rendererModes[shaderTraits.isTessellated];

    // Lod info
    LodInfo& lodInfo = m_lods[0];

    // Init batch const
    TerrainClipMap& detailCache = m_terrainBuffer->detailCache();
    PerBatch perBatch;
    perBatch.regionBegin = static_cast<float2>(regionBegin - swiz<X, Z>(sceneView.camera.worldCenter()));
    perBatch.cacheOffset = static_cast<float2>(swiz<X, Z>(sceneView.camera.worldCenter()) % detailCache.textureSize());
    perBatch.regionSize = static_cast<float>(worldConfig.regionWidth);
    perBatch.density = 1.0f;
    perBatch.cacheSize = static_cast<float>(detailCache.textureSize());
    batchRenderer.commitBuffer(m_handlePerBatch, perBatch);

    // Render all chunks
    for (TerrainTreeChunk const& chunk : chunkList)
    {
        // Get layer material
        int2 material = chunk.layers[layer];

        // Start batch
        batchRenderer.setPipelineState(m_shaders[shader]->pipelineState(sceneView));
        batchRenderer.setGeometry(currentRendererMode.geometry);

        // Fill resources
        ResourceBucket resources = regionMaps;
        resources.addResource(TerrainRegionBuffer::SlotNoise, m_noiseTexture, ShaderType::Pixel);
        resources.addResource(TerrainRegionBuffer::SlotDiffuse0, m_diffuseMaps[material.x], ShaderType::Pixel);
        resources.addResource(TerrainRegionBuffer::SlotDiffuse1, m_diffuseMaps[material.y], ShaderType::Pixel);
        batchRenderer.setResources(resources);

        // Draw
        renderChunk(chunk.index, chunk.edges,
                    lodInfo.scale, currentRendererMode.numCellIndices,
                    sceneView, batchRenderer);
    }
}
void TerrainRenderer::renderChunk(const int2& chunkIndex, const int4& chunkEdges,
                                  uint chunkScale, uint cellIndices,
                                  const SceneView& sceneView, BatchRenderer& batchRenderer)
{
    // Chunk const data
    double2 chunkPosition = static_cast<double2>(chunkIndex) * chunkScale * chunkWidth;
    Wrap& wrap = m_wraps.at(chunkEdges[0], chunkEdges[1], chunkEdges[2], chunkEdges[3]);

    // Fill per-primitive
    PerPrimitive perPrimitive;
    perPrimitive.offset = static_cast<float3>(swiz<X, O, Y>(chunkPosition) - sceneView.camera.worldCenter());
    perPrimitive.scale = static_cast<float>(chunkScale);
    batchRenderer.commitBuffer(m_handlePerPrimitive, perPrimitive);

    // Draw
    batchRenderer.drawBatch(wrap.startQuad * cellIndices, wrap.numQuads * cellIndices, 0);
}


// Implement SceneSystem
void TerrainRenderer::addEntity(Entity& entity, Component& component)
{
//     throw std::logic_error("The method or operation is not implemented.");
}
void TerrainRenderer::removeEntity(Entity& entity)
{
//     throw std::logic_error("The method or operation is not implemented.");
}
ComponentType TerrainRenderer::keyComponent() const
{
    return ComponentType::HeightfieldTerrain;
}