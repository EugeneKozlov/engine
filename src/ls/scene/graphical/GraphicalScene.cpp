#include "pch.h"
#include "ls/scene/graphical/GraphicalScene.h"
#include "ls/render/GraphicConfig.h"
#include "ls/render/Camera.h"

using namespace ls;
GraphicalScene::GraphicalScene(const GraphicConfig& graphicConfig)
    : graphicConfig(graphicConfig)
{
}
GraphicalScene::~GraphicalScene()
{
}
void GraphicalScene::updateViewport(float width, float height)
{
    m_viewportWidth = width;
    m_viewportHeight = height;
    if (m_mainCamera)
    {
        updateMainCameraParameters(*m_mainCamera);
    }
}
void GraphicalScene::updateMainCameraParameters(Camera& camera)
{
    float fovy = deg2rad(*graphicConfig.fovyAngle);
    float znear = *graphicConfig.nearDistance;
    float zfar = *graphicConfig.farDistance;
    camera.setProjection(Projection(m_viewportWidth, m_viewportHeight, fovy, znear, zfar));
    camera.setCenter(m_worldZero);
}
void GraphicalScene::recenter(const double3& zero)
{
    m_worldZero = zero;

    // Update camera
    if (m_mainCamera)
    {
        updateMainCameraParameters(*m_mainCamera);
    }
}


// Camera
void GraphicalScene::mainCamera(Camera& camera)
{
    m_mainCamera = &camera;
}
Camera& GraphicalScene::mainCamera()
{
    debug_assert(m_mainCamera);
    return *m_mainCamera;
}


// Lights
CascadedShadowMap& GraphicalScene::cascadedShadowMapHelper()
{
    return m_cascadedShadowMapHelper;
}


// Render systems
void GraphicalScene::addRenderSystem(RenderSystem& renderSystem)
{
    m_renderSystems.push_back(&renderSystem);
}
GraphicalScene::RenderSystemRange GraphicalScene::renderSystems()
{
    return make_pair(m_renderSystems.begin(), m_renderSystems.end());
}