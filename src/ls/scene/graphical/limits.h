/// @file ls/scene/graphical/limits.h
/// @brief Graphical limits
#pragma once

#include "ls/common.h"

namespace ls
{
    class GraphicalComponent;

    /// @addtogroup LsScene
    /// @{

    /// @brief Max number of model LODs
    static const uint MaxModelLods = 4;
    /// @brief Max number of model subsets
    static const uint MaxModelSubsets = 8;
    /// @brief Unknown LOD
    static const ubyte UnknownLod = 255;
    /// @brief Rendered Object
    struct RenderedObject
    {
        /// @brief Ctor
        RenderedObject() = default;
        /// @brief Ctor
        RenderedObject(GraphicalComponent* graphical, float fadeFactor)
            : graphical(graphical)
            , fadeFactor(fadeFactor)
        {
        }
        /// @brief Graphical
        GraphicalComponent* graphical = nullptr;
        /// @brief Fade factor
        float fadeFactor = 0.0f;
    };
    /// @brief Rendered Objects List
    using RenderedObjectsList = vector<RenderedObject, FastPoolAllocator<RenderedObject>>;
    /// @}
}