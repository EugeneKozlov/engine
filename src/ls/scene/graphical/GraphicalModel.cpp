#include "pch.h"
#include "ls/scene/graphical/GraphicalModel.h"
#include "ls/asset/AssetManagerOld.h"
#include "ls/render/Mesh.h"

using namespace ls;


// Sets
GraphicalModel::GraphicalModel(AssetManagerOld& manager)
    : m_manager(manager)
{
}
GraphicalModel::~GraphicalModel()
{
}
void GraphicalModel::addLod(const Lod& lod, const string& meshName,
                            uint numSubsets, const GraphicalModelMaterialsArray& materials)
{
    debug_assert(m_numLods < MaxModelLods);
    LodData& dest = m_lods[m_numLods++];

    (Lod&) dest = lod;
    dest.meshName = meshName;
    dest.numSubsets = numSubsets;
    dest.materials = materials;
}


// Load
void GraphicalModel::loadGeometry(uint lod, Renderer& renderer)
{
    // Check
    debug_assert(lod < m_numLods);
    LodData& data = m_lods[lod];
    if (data.isLoaded)
        return;

    // Load mesh
    data.isLoaded = true;
    data.mesh = m_manager.get<Mesh>(data.meshName);
    if (!data.mesh)
        return;
    data.mesh->create(renderer);

    // Load materials
    for (uint subset = 0; subset < data.numSubsets; ++subset)
    {
        Material* mat = data.materials[subset].get();
        if (!mat)
            continue;

        mat->loadTextures(renderer);
    }
}


// Gets
Mesh* GraphicalModel::mesh(uint lod)
{
    debug_assert(lod < m_numLods);
    LodData& data = m_lods[lod];
    debug_assert(data.isLoaded);

    return data.mesh.get();
}
uint GraphicalModel::numSubsets(uint lod) const
{
    debug_assert(lod < m_numLods);
    return m_lods[lod].numSubsets;
}
const GraphicalModel::Lod& GraphicalModel::lod(uint lod) const
{
    debug_assert(lod < m_numLods);
    return m_lods[lod];
}
Material* GraphicalModel::material(uint lod, uint subset)
{
    debug_assert(lod < m_numLods);
    LodData& data = m_lods[lod];

    debug_assert(subset < data.numSubsets);
    return data.materials[subset].get();
}
