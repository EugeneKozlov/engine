/// @file ls/scene/graphical/TerrainTree.h
/// @brief Terrain Tree
#pragma once
#include "ls/common.h"
#include "ls/core/FlatArray.h"
#include "ls/geom/Transform.h"
#include "ls/scene/core/ObjectPose.h"
#include "ls/asset/WorldAsset.h"

namespace ls
{
    /// @addtogroup LsScene
    /// @{

    /// @brief Terrain Tree Chunk Desc
    struct TerrainTreeChunkDesc
    {
        /// @brief Bounds 
        double2 bounds;
        /// @brief Number of layers
        uint numLayers;
        /// @brief Layer materials
        array<int2, WorldRegion::MaxBlendLayers> materials;
    };
    /// @brief Terrain Tree Chunk
    struct TerrainTreeChunk
    {
        /// @brief Chunk index
        int2 index;
        /// @brief Number of layers
        uint numLayers = 0;
        /// @brief Layer textures
        array<int2, WorldRegion::MaxBlendLayers> layers;
        /// @brief Chunk edges
        int4 edges;
        /// @brief Distance to camera
        double distance = numeric_limits<double>::max();
    };
    /// @brief Terrain Tree Detailed Chunks List
    using TerrainTreeDetailedChunksList = array<vector<TerrainTreeChunk>, WorldRegion::MaxBlendLayers>;
    /// @brief Terrain Tree Chunk List
    struct TerrainTreeRegionChunksList
    {
        /// @brief Initialize
        void initialize(uint numLods)
        {
            perLevelLists.resize(numLods);
        }
        /// @brief Initialized
        bool isInitialized() const
        {
            return !perLevelLists.empty();
        }
        /// @brief Nearest LOD layers
        TerrainTreeDetailedChunksList detailed;
        /// @brief Far LODs (reversed order)
        vector<vector<TerrainTreeChunk>> perLevelLists;
    };
    /// @brief Terrain Tree Chunk List
    using TerrainTreeChunksList = hamap<int2, TerrainTreeRegionChunksList>;
    /// @brief Terrain Tree Node
    class TerrainTreeNode
    {
    public:
        /// @brief Ctor
        TerrainTreeNode(ubyte level, uint maxDepth, 
                        const int2& localIndex, const int2& globalIndex,
                        const AABB& aabb);
        void operator = (const TerrainTreeNode& another) = delete;
        /// @brief Allocate
        void allocate(vector<TerrainTreeNode>& pool, uint maxDepth, const FlatArray<TerrainTreeChunkDesc>& chunksInfo);
        /// @brief Split this node box into 4 children
        void splitThis(AABB children[4]);

        /// @brief Update chunk LODs
        void updateChunkLods(bool resetCurrentLod, const double3& position,
            const double innerDistances2[], const double outerDistances2[]);
        /// @brief Fill chunk current LOD
        void fillChunkLods(uint value);
        /// @brief Update edge offset
        void updateEdges(TerrainTreeNode* xneg, TerrainTreeNode* xpos,
                         TerrainTreeNode* zneg, TerrainTreeNode* zpos);

        /// @brief Find chunks
        void findChunks(TerrainTreeRegionChunksList& chunks, const Frustum& frustum) const;

        /// @brief Debug print current levels
        void debugPrintCurrentLevel(const int2& baseIndex, string& buffer, uint stride);
    public:
        /// @brief Node local index
        const int2 localIndex;
        /// @brief Node global index
        const int2 globalIndex;
        /// @brief Depth level
        const ubyte level;
        /// @brief Is leaf?
        const bool isLeaf;

        /// @brief Next level
        TerrainTreeNode* next[2][2];

        /// @brief Node AABB
        AABB aabb;

        /// @brief Current LOD depth
        uint currentLevel;
        /// @brief Whether this LOD is current
        bool isCurrentLod = false;

        /// @brief Info
        TerrainTreeChunk info;
    };
    /// @brief Terrain Tree Region Node
    class TerrainTreeRegion
        : public TerrainTreeNode
    {
    public:
        /// @brief Ctor
        TerrainTreeRegion(uint maxDepth, const int2& globalIndex, double regionSize, 
                          const FlatArray<TerrainTreeChunkDesc>& chunksInfo);
    private:
        vector<TerrainTreeNode> m_pool;
    };

    /// @brief Terrain Tree
    class TerrainTree
        : Noncopyable
    {
    public:
        /// @brief Root cell width
        const uint regionWidth;
        /// @brief Chunk width
        const uint chunkWidth;
        /// @brief Max depth
        const ubyte chunkDepth;
    public:
        /// @brief Ctor
        /// @param regionWidth
        ///   Root tree region width
        /// @param chunkWidth
        ///   Chunk width, must be less than @a regionWidth
        ///   @code regionWidth/chunkWidth = 2^n @endcode
        TerrainTree(uint regionWidth, uint chunkWidth);
        /// @brief Dtor
        ~TerrainTree();

        /// @brief Allocate region
        shared_ptr<TerrainTreeRegion> createRegion(const int2& regionIndex, 
                                                   const FlatArray<TerrainTreeChunkDesc>& chunksInfo) const;
        /// @brief Add region
        void addRegion(shared_ptr<TerrainTreeRegion> region);
        /// @brief Remove region
        void removeRegion(const int2& regionIndex);

        /// @brief Update chunks LODs
        void updateChunks(const double3& position, 
                          const double innerDistances2[], const double outerDistances2[]);
        /// @brief Find chunks
        void findChunks(TerrainTreeChunksList& chunks, const Frustum& frustum) const;

        /// @brief Print current level
        string debugPrintCurrentLevel(const int2& rootIndex);
    private:
        static ubyte computeChunkDepth(uint regionWidth, uint chunkWidth);
    private:
        hamap<int2, shared_ptr<TerrainTreeRegion>> m_regions;
    };
    /// @}
}
