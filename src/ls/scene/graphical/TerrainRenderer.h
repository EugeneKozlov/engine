/// @file ls/scene/graphical/TerrainRenderer.h
/// @brief Terrain height-field renderer
#pragma once

#include "ls/common.h"
#include "ls/asset/WorldRegion.h"
#include "ls/core/AutoExpandingVector.h"
#include "ls/gapi/Renderer.h"
#include "ls/render/shader/TerrainEffect.h"
#include "ls/scene/RenderSystem.h"
#include "ls/scene/SceneSystem.h"
#include "ls/scene/graphical/TerrainTree.h"
#include "ls/world/WorldStreamingConfig.h"

namespace ls
{
    class SceneView;
    class TerrainBuffer;
    class TextureAsset;

    /// @addtogroup LsScene
    /// @{

    /// @brief Terrain Renderer
    ///
    /// Cross-mapping does not work properly with manual chunk loading/unloading.
    /// You must be sure that all simultaneously loaded chunks fit into cache.
    class TerrainRenderer
        : Noncopyable
        , public SceneSystem
        , public RenderSystem
    {
    public:
        /// @brief Terrain index
        using TerrainIndex = uint;
        /// @brief Terrain index format
        static const IndexFormat TerrainIndexFormat =
            (sizeof(TerrainRenderer::TerrainIndex) == 4) ? IndexFormat::Uint : IndexFormat::Ushort;

        /// @brief Terrain vertex
        struct TerrainVertex
        {
            /// @brief Ctor
            TerrainVertex(int2 pos = int2(0),
                          float2 tex = float2(0.0f),
                          int2 flag = int2(0))
                          : pos(float(pos.x), 0.0f, float(pos.y), 1.0f)
                          , tex(tex)
                          , flag(flag)
            {
            }
            /// @brief Flat position
            float4 pos;
            /// @brief HD texture coordinate
            float2 tex;
            /// @brief Flag
            int2 flag;
        };
        /// @brief Per-view constants
        struct PerView
        {
            /// @brief View-projection matrix
            float4x4 viewProj;
            /// @brief View matrix
            float4x4 view;
        };
        /// @brief Per-batch constants
        struct PerBatch
        {
            /// @brief Region begin
            float2 regionBegin;
            /// @brief Cache offset
            float2 cacheOffset;
            /// @brief Region size
            float regionSize;
            /// @brief Cache size
            float cacheSize;
            /// @brief Region density
            float density;
            /// @brief Reserve #1
            float reserve1;
        };
        /// @brief Per-primitive constants
        struct PerPrimitive
        {
            /// @brief Chunk offset
            float3 offset;
            /// @brief Chunk scale
            float scale;
        };
    public:
        /// @brief Renderer
        Renderer& renderer;
        /// @brief Is editable
        const bool editable;
        /// @brief World config
        const WorldRegion::Config worldConfig;
        /// @brief Streaming config
        const WorldStreamingConfig streamingConfig;
        /// @brief Chunk width (in units)
        const uint chunkWidth;
        /// @brief Pack width (in chunks)
        const uint packWidth;
        /// @brief Region width (in chunks)
        const uint regionWidth;
    public:
        /// @brief Ctor
        /// @param renderer
        ///   Renderer interface
        /// @param worldConfig
        ///   World data configuration
        /// @param streamingConfig
        ///   World streaming configuration
        /// @param effect
        ///   Rendering effect
        /// @param editable
        ///   Editable mode allows you to set callbacks and edit terrain data
        TerrainRenderer(shared_ptr<TerrainBuffer> terrainBuffer,
                        shared_ptr<TerrainEffect> terrainEffect,
                        const WorldRegion::Config& worldConfig,
                        const WorldStreamingConfig& streamingConfig,
                        bool editable);
        /// @brief Dtor
        virtual ~TerrainRenderer();

        /// @brief Set noise texture
        void setNoiseTexture(shared_ptr<TextureAsset> texture);
        /// @brief Reset textures
        void resetTextures();
        /// @brief Set terrain texture at slot
        void setTexture(uint slot, shared_ptr<TextureAsset> diffuse);
        /// @brief Enable wire
        void setWire(const float3& color, float thickness);
        /// @brief Set tessellation
        bool setTessellation(bool tessellated);

        /// @brief Get current renderer
        bool hasTessellation() const
        {
            return m_tessellated;
        }
    private:
        /// @name Implement SceneSystem
        /// @{
        virtual void addEntity(Entity& entity, Component& component) override;
        virtual void removeEntity(Entity& entity) override;
        virtual ComponentType keyComponent() const override;
        /// @}

        /// @name Implement RenderSystem
        /// @{
        virtual void render(BatchRenderer& batchRenderer, const SceneView* sceneView) override;
        /// @}
    private:
        enum MaterialType
        {
            MaterialTextured,
            MaterialBaked,
            MaterialCOUNT
        };
        struct Quad
        {
            Quad(uint width = 0, uint base = 0,
                 int2 v0 = int2(0), int2 v1 = int2(0),
                 int2 v2 = int2(0), int2 v3 = int2(0))
                : width(width)
                , base(base)
                , v0(v0)
                , v1(v1)
                , v2(v2)
                , v3(v3)
            {
            };
            uint width;
            uint base;
            int2 v0, v1, v2, v3;
        };
        struct RendererImpl
        {
            uint numCellIndices = 0;
            BufferHandle indexBuffer;

            GeometryBucket geometry;
        };

        // Init data
        void initConfig();
        void initEffect();
        void initVertexData();
        void initIndexData();
        void initSimpleRenderer();
        void initTessellationRenderer();
        void initDistances(double detail, double firstInner, double innerOffset);
        void initLods();
        void destroyRenderingMode(RendererImpl& mode);

        // Generate geometry
        static uint generateVertices(uint width, uint scaling,
                                     vector<TerrainVertex>& buffer);
        static uint generateIndices(uint width, vector<Quad>& buffer,
                                    const int4& wrapping, uint baseVertex);
        static vector<TerrainIndex> generateTriangles(const vector<Quad>& indices);
        static vector<TerrainIndex> generateQuads(const vector<Quad>& indices);

        // Render
        void renderChunkList(const SceneView& sceneView, BatchRenderer& batchRenderer,
                             uint lod, TerrainTreeDetailedChunksList& detailed,
                             const double2& regionBegin, TerrainEffect::Shader shader,
                             const ResourceBucket& regionMaps, const vector<TerrainTreeChunk>& chunkList);
        void renderChunkListDetailed(const SceneView& sceneView, BatchRenderer& batchRenderer,
                                     uint layer,
                                     const double2& regionBegin, TerrainEffect::Shader shader,
                                     const ResourceBucket& regionMaps, const vector<TerrainTreeChunk>& chunkList);
        void renderChunk(const int2& chunkIndex, const int4& chunkEdges,
                         uint chunkScale, uint cellIndices,
                         const SceneView& sceneView, BatchRenderer& batchRenderer);

        // Lod
        struct LodInfo
        {
            /// @brief LOD width
            uint width;
            /// @brief LOD scale
            uint scale;
        };
        struct Wrap
        {
            /// @brief Start index for rendering
            uint startQuad = 0;
            /// @brief Number of indices for rendering
            uint numQuads = 0;
        };
        static inline uint log2u(uint value)
        {
            uint power = 0;
            while (value >>= 1)
                ++power;
            return power;
        }
    private:
        // Main
        shared_ptr<TerrainBuffer> m_terrainBuffer;
        shared_ptr<TerrainEffect> m_terrainEffect;

        // Textures
        shared_ptr<TextureAsset> m_noiseTextureHolder;
        Texture2DHandle m_noiseTexture;
        AutoExpandingVector<shared_ptr<TextureAsset>> m_diffuseMapsHolder;
        AutoExpandingVector<Texture2DHandle> m_diffuseMaps;

        // Handles
        BufferHandle m_handleConstant;
        struct Constant
        {
            float3 wireColor;
            float wireThreshold;
        } m_constant;

        BufferHandle m_handlePerView;

        BufferHandle m_handlePerBatch;

        BufferHandle m_handlePerPrimitive;

        // Grid
        BufferHandle m_vertexBuffer;
        vector<Quad> m_indexData;
        MultiArray<Wrap, 3, 3, 3, 3> m_wraps;

        // States
        bool m_tessellated = false;
        array<RendererImpl, 2> m_rendererModes;
        array<SurfaceShader*, TerrainEffect::NumShaders> m_shaders;
        TerrainEffect::Shader m_shaderDetailBasePass;
        TerrainEffect::Shader m_shaderDetailSecondPass;
        TerrainEffect::Shader m_shaderCachedColor;

        // LODs
        double m_detailDrawDistance;
        vector<double> m_innerDistances;
        vector<double> m_outerDistances;
        vector<LodInfo> m_lods;
        uint m_numLods = 0;

        // World data
        map<int2, WorldRegion::Chunk*> m_chunks;
    };
    /// @}
}

