#include "pch.h"
#include "ls/scene/graphical/TerrainBuffer.h"
#include "ls/gapi/Renderer.h"
#include "ls/render/GraphicConfig.h"
#include "ls/asset/WorldAsset.h"

using namespace ls;
// Terrain Region Decl
Format const TerrainRegionBuffer::TexelFormat[TerrainRegionBuffer::MapCOUNT] =
{
    Format::R32_FLOAT,
    Format::R8G8B8A8_SNORM,
    Format::R8G8B8A8_UNORM,
    Format::R8G8_UNORM,
    Format::R8G8_UNORM,
    Format::R8G8_UNORM,
    Format::R8G8_UNORM,
};
bool const TerrainRegionBuffer::GenerateMipmap[TerrainRegionBuffer::MapCOUNT] =
{
    false,
    false,
    true,
    false,
    false,
    false,
    false,
};


// Terrain Region
TerrainRegionBuffer::TerrainRegionBuffer(Renderer& renderer,
                                         const WorldRegion::Config& config,
                                         Texture2DHandle cachedColor,
                                         bool readable)
    : renderer(renderer)
    , isReadable(readable)
{
    // Init sizes
    uint regionSize = config.regionInChunks;
    chunkSizes[HeightMap] = config.chunkWidth;
    chunkSizes[NormalMap] = config.chunkWidth;
    chunkSizes[ColorMap] = config.chunkWidth;
    for (uint idx = 0; idx < WorldRegion::MaxBlendLayers; ++idx)
        chunkSizes[BlendMap0 + idx] = config.chunkWidth;

    // Update sizes
    for (Map map : enumerate(HeightMap, MapCOUNT))
        mapSizes[map] = chunkSizes[map] * regionSize + 1;
    for (Map map : enumerate(HeightMap, MapCOUNT))
        texelSizes[map] = formatStride(TexelFormat[map]);
    for (Map map : enumerate(HeightMap, MapCOUNT))
        mapRowStrides[map] = mapSizes[map] * texelSizes[map];

    // Allocate textures
    auto flags = ResourceBinding::ShaderResource | ResourceBinding::RenderTarget;
    if (isReadable)
        flags |= ResourceBinding::Readable;
    for (Map map : enumerate(HeightMap, MapCOUNT))
    {
        // Init buffer
        m_buffers[map].resize(mapSizes[map] * mapSizes[map] * texelSizes[map]);

        // Create
        textures[map] = renderer
            .createTexture2D(mapSizes[map], mapSizes[map],
                             GenerateMipmap[map], TexelFormat[map],
                             ResourceUsage::Static, flags);

        // Init sampler
        renderer.setTexture2DSampler(textures[map],
                                     Filter::Linear, Addressing::Wrap);
    }

    // Create render target
    renderTarget = renderer.createRenderTarget(MapCOUNT, nullptr);
    renderer.attachColorTexture2D(renderTarget, HeightMap, textures[HeightMap]);
    renderer.attachColorTexture2D(renderTarget, NormalMap, textures[NormalMap]);
    renderer.attachColorTexture2D(renderTarget, ColorMap, textures[ColorMap]);
    for (uint idx = 0; idx < WorldRegion::MaxBlendLayers; ++idx)
        renderer.attachColorTexture2D(renderTarget, BlendMap0 + idx, textures[BlendMap0 + idx]);

    // Init bucket
    resourcesBase.addResource(HeightMap, textures[HeightMap], ShaderType::Vertex);
    resourcesBase.addResource(HeightMap, textures[HeightMap], ShaderType::TessEval);
    resourcesBase.addResource(NormalMap, textures[NormalMap], ShaderType::Pixel);
    resourcesBase.addResource(ColorMap, cachedColor, ShaderType::Pixel);
    resourcesBase.addResource(BlendMap0, nullptr, ShaderType::Pixel);
    resourcesDetail = resourcesBase;
}
TerrainRegionBuffer::~TerrainRegionBuffer()
{
    // Destroy render target
    renderer.destroyRenderTarget(renderTarget);

    for (Map map : enumerate(HeightMap, MapCOUNT))
    {
        renderer.destroyTexture2D(textures[map]);
    }
}
void TerrainRegionBuffer::update(WorldRegion& region)
{
    int2 baseChunk = region.index() * region.config().regionInChunks;
    for (Map map : enumerate(HeightMap, MapCOUNT))
    {
        for (WorldRegion::Chunk& chunk : region.chunks())
        {
            // Border?
            bool lastX = uint(chunk.index.x - baseChunk.x + 1) == region.config().regionInChunks;
            bool lastY = uint(chunk.index.y - baseChunk.y + 1) == region.config().regionInChunks;

            // Get data
            void* chunkData[MapCOUNT] =
            {
                chunk.heightMap.data(),
                chunk.normalMap.data(),
                chunk.colorMap.data(),
                chunk.blendMap[0].data(),
                chunk.blendMap[1].data(),
                chunk.blendMap[2].data(),
                chunk.blendMap[3].data(),
            };

            // Compute start
            int2 chunkIndex = chunk.index - baseChunk;
            uint chunkOffset = (chunkIndex.x + chunkIndex.y * mapSizes[map]) * chunkSizes[map] * texelSizes[map];
            ubyte* dest = m_buffers[map].data() + chunkOffset;
            const ubyte* src = static_cast<const ubyte*>(chunkData[map]);

            // Fill
            for (uint y = 0; y < chunk.width + lastY; ++y)
            {
                memcpy(dest, src, (chunkSizes[map] + lastX) * texelSizes[map]);
                dest += mapRowStrides[map];
                src += (chunkSizes[map] + 1) * texelSizes[map];
            }
        }
    }
}
void TerrainRegionBuffer::split(uint numPortions)
{
    m_currentCommit = 0;
    if (numPortions == 0)
    {
        m_numCommits = 1;
        m_numPortions = 1;
    }
    else
    {
        m_numCommits = numPortions * MapCOUNT;
        m_numPortions = numPortions;
        debug_assert(!(m_numPortions & (m_numPortions - 1)));
    }
}
void TerrainRegionBuffer::commitMap(Map map)
{
    commitMapPortion(map, 0, 1);
}
bool TerrainRegionBuffer::commitOnce()
{
    // All's done
    if (m_currentCommit == m_numCommits)
        return false;

    if (m_numCommits == 1)
    {
        // Commit all
        for (Map map : enumerate(HeightMap, MapCOUNT))
            commitMapPortion(map, 0, 1);
    }
    else
    {
        // Commit one
        uint map = m_currentCommit / m_numPortions;
        uint portion = m_currentCommit % m_numPortions;
        commitMapPortion((Map) map, portion, m_numPortions);
    }

    // Next
    ++m_currentCommit;
    return true;
}
void TerrainRegionBuffer::commitMapPortion(Map map, uint portion, uint numPortions)
{
    bool last = portion + 1 == numPortions;
    uint sizeY = (mapSizes[map] - 1) / numPortions;
    ubyte* data = m_buffers[map].data() + portion * sizeY * mapRowStrides[map];
    renderer.writeTexture2D(textures[map], 0, portion * sizeY, mapSizes[map], sizeY + last, data);

    // Generate mips
    if (last && GenerateMipmap[map])
        renderer.generateMipmaps(textures[map]);
}
void TerrainRegionBuffer::commitAll()
{
    while (commitOnce())
        ;
}


// Edit
void TerrainRegionBuffer::extractMap(Map map)
{
    debug_assert(isReadable);
    renderer.readTexture2D(textures[map], m_buffers[map].data());
}
void TerrainRegionBuffer::extractAll()
{
    debug_assert(isReadable);
    extractMap(HeightMap);
    extractMap(NormalMap);
    extractMap(ColorMap);
    for (uint idx = 0; idx < WorldRegion::MaxBlendLayers; ++idx)
        extractMap(Map(BlendMap0 + idx));
}
void TerrainRegionBuffer::readMap(Map map, WorldRegion& region)
{
    debug_assert(isReadable);
    // Read all chunks
    for (WorldRegion::Chunk& chunk : region.chunks())
    {
        // Get data
        void* chunkData[MapCOUNT] =
        {
            chunk.heightMap.data(),
            chunk.normalMap.data(),
            chunk.colorMap.data(),
            chunk.blendMap[0].data(),
            chunk.blendMap[1].data(),
            chunk.blendMap[2].data(),
            chunk.blendMap[3].data(),
        };

        // Compute start
        int2 chunkIndex = chunk.index - region.index() * region.chunksData().size();
        uint chunkOffset = (chunkIndex.x + chunkIndex.y * mapSizes[map]) * chunkSizes[map] * texelSizes[map];
        ubyte* dest = static_cast<ubyte*>(chunkData[map]);
        const ubyte* src = m_buffers[map].data() + chunkOffset;

        // Fill
        for (uint y = 0; y < chunk.width + 1; ++y)
        {
            memcpy(dest, src, (chunkSizes[map] + 1) * texelSizes[map]);
            src += mapRowStrides[map];
            dest += (chunkSizes[map] + 1) * texelSizes[map];
        }
    }

    // Update aabbs 
    if (map == HeightMap)
    {
        for (WorldRegion::Chunk& chunk : region.chunks())
        {
            double2 bounds = chunk.heightMap.data()[0];
            for (double height : chunk.heightMap)
            {
                bounds.x = min(bounds.x, height);
                bounds.y = max(bounds.y, height);
            }
            chunk.bounds = bounds;
        }
    }
}
void TerrainRegionBuffer::readAll(WorldRegion& region)
{
    debug_assert(isReadable);
    for (Map map : enumerate(HeightMap, MapCOUNT))
        readMap(map, region);
}
void TerrainRegionBuffer::clearAll(float height)
{
    renderer.clearColor(renderTarget, HeightMap, float4(height));
    renderer.clearColor(renderTarget, NormalMap, float4(0, 1, 0, 0));
    renderer.clearColor(renderTarget, ColorMap, float4(1, 0, 0, 1));
    for (uint idx = 0; idx < WorldRegion::MaxBlendLayers; ++idx)
        renderer.clearColor(renderTarget, BlendMap0 + idx, float4(0, 0, 0, 0));
}


// Terrain Region Buffer Pool
TerrainRegionBufferPool::TerrainRegionBufferPool(Renderer& renderer,
                                                 Texture2DHandle cachedColor)
    : renderer(renderer)
    , m_cachedColor(cachedColor)
{
}
TerrainRegionBuffer& TerrainRegionBufferPool::receiveBuffer(WorldRegion& region)
{
    if (m_unused.empty())
    {
        m_pool.emplace_back(renderer, region.config(), m_cachedColor, false);
        m_unused.push_back(&m_pool.back());
    }
    TerrainRegionBuffer* buffer = m_unused.back();
    buffer->currentRegion = &region;
    m_unused.pop_back();
    return *buffer;
}
void TerrainRegionBufferPool::releaseBuffer(TerrainRegionBuffer& buffer)
{
    buffer.currentRegion = nullptr;
    m_unused.push_back(&buffer);
}


// Terrain Clip Map
TerrainClipMap::TerrainClipMap(Renderer& renderer, uint regionWidth, uint cacheSize)
    : renderer(renderer)
    , regionWidth(regionWidth)
    , cacheSize(cacheSize)
{
    m_texture = renderer
        .createTexture2D(regionWidth * cacheSize, regionWidth * cacheSize,
                         false, Format::R8G8B8A8_UNORM, ResourceUsage::Static,
                         ResourceBinding::ShaderResource | ResourceBinding::RenderTarget);
    renderer.setTexture2DSampler(m_texture,
                                 Filter::Linear, Addressing::Wrap);
}
TerrainClipMap::~TerrainClipMap()
{
    renderer.destroyTexture2D(m_texture);
}
void TerrainClipMap::move(const int2& regionIndex)
{
    m_begin = regionIndex - cacheSize / 2;
    m_end = m_begin + (sint) cacheSize;
}
bool TerrainClipMap::update(const int2& regionIndex, Texture2DHandle source)
{
    if (!isStored(regionIndex))
    {
        return false;
    }

    int2 idx = (regionIndex % cacheSize + cacheSize) % cacheSize;
    renderer.copyTexture2D(m_texture, source, 
                           idx.x * regionWidth, idx.y * regionWidth,
                           0, 0, regionWidth, regionWidth);
    return true;
}
bool TerrainClipMap::isStored(const int2& regionIndex) const
{
    return regionIndex.x >= m_begin.x && regionIndex.y >= m_begin.y
        && regionIndex.x < m_end.x && regionIndex.y < m_end.y;
}


// Ctor
TerrainBuffer::TerrainBuffer(Renderer& renderer, const WorldRegion::Config& config)
    : renderer(renderer)
    // #HACK Use variable from config
    , numPortions(/*config.numRegionUpdatePortions*/ 1)

    // #HACK Use variable from config
    , m_detailCache(renderer, config.regionWidth, /*config.regionDetailCacheSize*/ 3)
    , m_pool(renderer, m_detailCache.texture())

    , m_tree(config.regionWidth, config.chunkWidth)
{
}
TerrainBuffer::~TerrainBuffer()
{
}
void TerrainBuffer::flush()
{
    while (!m_incomingRegions.empty())
    {
        // Commit and break
        auto incoming = m_incomingRegions.back();
        if (incoming.second->commitOnce())
            break;

        // Pop and continue
        finalizeBuffer(*incoming.second);
        m_incomingRegions.pop_back();
    }
}
void TerrainBuffer::finalizeBuffer(TerrainRegionBuffer& buffer)
{
    WorldRegion& region = *buffer.currentRegion;

    // Update cache
    bool isCached = m_detailCache.update(region.index(), buffer.textures[TerrainRegionBuffer::ColorMap]);
    if (isCached)
    {
        m_cachedRegions.emplace(region.index(), &buffer);
        m_tree.addRegion(buffer.treeNode);
    }
    else
    {
        m_loadedRegions.emplace(region.index(), &buffer);
    }

    // Update list
    chunkList[region.index()].initialize(m_tree.chunkDepth + 1);
}


// Get
TerrainRegionBuffer* TerrainBuffer::region(const int2& regionIndex)
{
    return getOrDefault(m_cachedRegions, regionIndex);
}


// Callbacks
void TerrainBuffer::onLoadRegionData(WorldRegion& /*region*/)
{
}
void TerrainBuffer::onUnloadRegionData(WorldRegion& /*region*/)
{
}
void TerrainBuffer::onPreloadChunksData(WorldRegion& region)
{
    TerrainRegionBuffer& buffer = m_pool.receiveBuffer(region);

    region.allocateAsyncStorage<TerrainRegionBuffer*>(this);
    region.getAsyncStorage<TerrainRegionBuffer*>(this) = &buffer;
}
void TerrainBuffer::onAsyncLoadChunksData(WorldRegion& region)
{
    TerrainRegionBuffer& buffer = *region.getAsyncStorage<TerrainRegionBuffer*>(this);
    buffer.update(region);
    buffer.split(numPortions);

    // Compute bounds
    uint numChunks = region.config().regionInChunks;
    int2 baseIndex = region.index() * (sint) numChunks;
    FlatArray<TerrainTreeChunkDesc> chunksInfo(numChunks, numChunks);
    for (WorldRegion::Chunk& chunk : region.chunks())
    {
        // Find chunk
        int2 idx = chunk.index - baseIndex;
        TerrainTreeChunkDesc& info = chunksInfo[idx];

        // Fill info
        info.bounds = chunk.bounds;
        info.materials = chunk.materials;
        info.numLayers = chunk.numLayers;
    }

    // Allocate node
    buffer.treeNode = m_tree.createRegion(region.index(), chunksInfo);
}
void TerrainBuffer::onPostloadChunksData(WorldRegion& region)
{
    // Get
    TerrainRegionBuffer& buffer = *region.getAsyncStorage<TerrainRegionBuffer*>(this);

    if (numPortions == 0)
    {
        // Commit and save
        buffer.commitAll();
        finalizeBuffer(buffer);
    }
    else
    {
        // Put aside
        m_incomingRegions.emplace_back(make_pair(region.index(), &buffer));
    }

    // Update list
    chunkList[region.index()].initialize(m_tree.chunkDepth + 1);

    // Release
    region.releaseAsyncStorage(this);
}
void TerrainBuffer::onUnloadChunksData(WorldRegion& region)
{
    // Search regions
    TerrainRegionBuffer* buffer = getOrDefault(m_loadedRegions, region.index());

    // Search cache
    if (!buffer)
    {
        buffer = getOrDefault(m_cachedRegions, region.index());
    }

    // Destroy anyway
    if (buffer)
    {
        m_pool.releaseBuffer(*buffer);
        m_loadedRegions.erase(region.index());
        m_cachedRegions.erase(region.index());
    }

    // Drop deps
    m_tree.removeRegion(region.index());
    chunkList.erase(region.index());
}
void TerrainBuffer::onUpdateAllRegionData(const int2& currentRegion)
{
    m_detailCache.move(currentRegion);

    // Unload
    auto eraser = [this](pair<const int2, TerrainRegionBuffer*>& region)
    {
        if (m_detailCache.isStored(region.first))
            return false;
        m_loadedRegions.emplace(region);
        m_tree.removeRegion(region.first);
        return true;
    };
    erase_if(m_cachedRegions, eraser);

    // Load
    for (int2 regionIndex : makeRange(m_detailCache.beginRegion(), m_detailCache.endRegion()))
    {
        TerrainRegionBuffer* buffer = getOrDefault(m_loadedRegions, regionIndex, nullptr);
        if (!buffer)
            continue;
        m_tree.addRegion(buffer->treeNode);
        m_detailCache.update(regionIndex, buffer->textures[TerrainRegionBuffer::ColorMap]);
        m_cachedRegions.emplace(regionIndex, buffer);
        m_loadedRegions.erase(regionIndex);
    }
}
