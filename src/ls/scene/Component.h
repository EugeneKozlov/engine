/// @file ls/scene/Component.h
/// @brief Scene entity Component
#pragma once

#include "ls/common.h"
#include "ls/core/FixedString.h"
#include "ls/io/Buffer.h"

namespace ls
{
    /// @addtogroup LsScene
    /// @{

    /// @brief Component types (14 bits)
    enum class ComponentType : ushort
    {
        /// @brief Unknown
        Unknown = 0,
        /// @brief Global transform and set of local
        Transform,
        /// @brief Model with several LODs and per-subset materials
        Model,
        /// @brief Rendered object with single model
        Render,
        /// @brief Detail geometry, e.g. on height-field terrain
        Grass,
        /// @brief Height-field terrain
        HeightfieldTerrain,
        /// @brief Chunk streaming provider
        ChunkStreaming,
        /// @brief Camera
        Camera,
        /// @brief Counter
        COUNT
    };
    /// @brief Component Flag (2 bits)
    enum class ComponentFlag : ushort
    {
        /// @brief Prototyped component
        Prototyped = 1 << 14,
        /// @brief Shared component
        Shared = 1 << 15,
    };
    /// @brief Component Flags
    using ComponentFlags = FlagSet<ComponentFlag>;
    /// @brief Number of component types
    static const uint NumComponentTypes = static_cast<uint>(ComponentType::COUNT);
    /// @brief Max resource name length
    static const uint MaxResourceNameLength = 128;
    /// @brief 
    using ResourceName = FixedString<MaxResourceNameLength>;
    /// @brief Component ID
    using ComponentId = uint;
    /// @brief Invalid Component ID
    static const ComponentId InvalidComponentId = static_cast<ComponentId>(-1);

    /// @brief Component By Type
    template <ComponentType Type>
    struct ComponentByType;
    /// @brief Component By Type - Type
    template <ComponentType Type>
    using ComponentByTypeT = typename ComponentByType<Type>::Component;
    /// @brief Component By Type - Impl
    template <class DerivedComponent>
    struct ComponentByTypeImpl
    {
        /// @brief Real type
        using Component = DerivedComponent;
    };
    /// @brief Scene entity Component
    class Component
    {
    public:
        /// @brief Ctor
        Component(const ComponentType type, const ComponentId id);
        /// @brief Dtor
        ~Component();

        /// @brief Serialize
        void serialize(Archive<Buffer> archive) = delete;

        /// @brief Get type
        ComponentType type() const;
        /// @brief Get flags
        ComponentFlags flags() const;
        /// @brief Get ID
        /// @note Only global components has ID
        ComponentId id() const;
        /// @brief Is shared?
        bool isShared() const;
        /// @brief Is prototyped?
        bool isPrototyped() const;
        /// @brief Downcast
        template <class DerivedComponent>
        DerivedComponent* cast()
        {
            if (m_type == DerivedComponent::Type)
                return static_cast<DerivedComponent*>(this);
            else
                return nullptr;
        }
        /// @brief Downcast const
        template <class DerivedComponent>
        const DerivedComponent* cast() const
        {
            if (m_type == DerivedComponent::Type)
                return static_cast<const DerivedComponent*>(this);
            else
                return nullptr;
        }
    protected:
        /// @brief Convert to prototyped if shared
        void convertSharedToPrototyped()
        {
            if (m_flags.is(ComponentFlag::Shared))
            {
                m_flags -= ComponentFlag::Shared;
                m_flags |= ComponentFlag::Prototyped;
            }
        }
    private:
        ComponentType m_type;
        ComponentFlags m_flags;
        ComponentId m_id;
    };
    /// @brief Component template base
    template <ComponentType T, class DerivedComponent>
    class ComponentT : public Component
    {
    public:
        static_assert(is_same<ComponentByTypeT<T>, DerivedComponent>::value, 
                      "mismatching component type");
        /// @brief Type
        static const ComponentType Type = T;
        /// @brief This type
        using This = DerivedComponent;
        /// @brief Base type
        using Base = ComponentT<T, DerivedComponent>;
    public:
        /// @brief Ctor
        ComponentT(const ComponentId id)
            : Component(Type, id)
        {
        }
        /// @brief Copy component
        /// @note Convert shared component to prototyped
        static void copy(DerivedComponent& dest, const DerivedComponent& src)
        {
            dest = src;
            dest.convertSharedToPrototyped();
        }
    };
    /// @brief Declare Component
#define LS_DECLARE_COMPONENT(X) \
    class X##Component; \
    template <> \
    struct ComponentByType<ComponentType::X> \
        : ComponentByTypeImpl<X##Component> {};

    /// @}
}