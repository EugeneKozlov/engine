#include "pch.h"
#include "ls/scene/type/FlatScene.h"

#include "ls/asset/AssetManager.h"
#include "ls/core/GlobalVariableT.h"
#include "ls/scene/graphical/GraphicalScene.h"
#include "ls/scene/graphical/TerrainRenderer.h"
#include "ls/scene/graphical/TerrainBuffer.h"
#include "ls/scene/graphical/GraphicalMaterial.h"
#include "ls/scene/physical/PhysicalScene.h"
#include "ls/render/shader/TerrainEffect.h"

#include "ls/scene/system/ModelSystem.h"
#include "ls/scene/system/GrassSystem.h"

// #HACK
#include "ls/io/StreamInterface.h"
#include "ls/asset/TextureAsset.h"
using namespace ls;

GlobalVariableT<bool> g_sceneMultithread(
    "scene:multithread", "scMultithread",
    GlobalVariableFlag::Modifiable | GlobalVariableFlag::Console,
    "Update and rendering are performed in multiple threads if enabled\n",
    true);

FlatScene::FlatScene(shared_ptr<AssetManager> assetManager,
                     Renderer* renderer,
                     Worker& worker, const SceneInfo& sceneInfo)
    : SceneInterface()

    , sceneInfo(sceneInfo)
    , m_assetManager(assetManager)
    , m_renderer(renderer)
    , m_mainThread(1)

    , m_world(Asset::cast<WorldAsset>(assetManager->loadAsset(AssetType::World, sceneInfo.worldName)))
    , m_worldLoader(m_world, sceneInfo.worldStreamingConfig)
{
    m_worldLoader.addObserver(*this);

    // Model Render System
    if (m_renderer)
    {
        auto& modelSystem = addSystem(make_shared<ModelSystem>(m_assetManager, *m_renderer, pi / 3));
        auto& grassSystem = addSystem(make_shared<GrassSystem>(m_assetManager, modelSystem.modelSystemRenderer(), *m_world));

        // Init terrain renderer    
        m_terrainBuffer = make_shared<TerrainBuffer>(*m_renderer, m_world->config);
        m_worldLoader.addObserver(*m_terrainBuffer);
        Asset::Ptr terrainShader = m_assetManager->loadAsset(AssetType::Effect, "architect/common/effect/terrain_shader");
        auto& terrainRenderer = addSystem(make_shared<TerrainRenderer>(m_terrainBuffer,
                                                                       make_shared<TerrainEffect>(*m_renderer, terrainShader),
                                                                       m_world->config,
                                                                       sceneInfo.worldStreamingConfig,
                                                                       true));
        terrainRenderer.setNoiseTexture(Asset::cast<TextureAsset>(m_assetManager->loadAsset(AssetType::Texture, "architect/nature/terrain/noiseA")));
        uint materialIdx = 0;
        for (const string& materialName : m_world->materials)
        {
            shared_ptr<TextureAsset> texture = Asset::cast<TextureAsset>(m_assetManager->loadAsset(AssetType::Texture, materialName));
            terrainRenderer.setTexture(materialIdx, texture);
            ++materialIdx;
        }
    }
    m_entityManager = make_unique<EntityManager>(m_sceneSystems);
    m_worldLoader.addObserver(*m_entityManager);
    debug_assert(m_world->globalComponents.size() != 0);
    m_entityManager->deserializeGlobalComponents(m_world->globalComponents);
    debug_assert(m_world->dynamicEntities.size() != 0);
    m_entityManager->deserializeDynamics(m_world->dynamicEntities);

    WindInfo directionalWind;
    directionalWind.intensity = 4.0f;
    directionalWind.pulseFrequency = 0.5f;
    directionalWind.pulseMagnitude = 2.0f;
    m_environment.windFlow.setDirectionalWind(float3(1, 0, 1), directionalWind);
}
FlatScene::~FlatScene()
{
    m_sceneSystems.clear();
    m_renderSystems.clear();
    m_worldLoader.discardObservers();
}


// Callbacks
void FlatScene::onRecenter(const int2& oldCenter,
                           const int2& newCenter,
                           const double3& newCenterPosition)
{
    GraphicalScene* gs = graphicalScene();
    if (gs)
    {
        gs->recenter(newCenterPosition);
    }
}



// Update
void FlatScene::syncUpdate(float deltaTime, DeltaTime deltaTimeMs)
{
    const double3 cameraPosition = graphicalScene() ? graphicalScene()->mainCamera().position() : double3(0.0);

    // Update systems
    m_entityManager->setLastFrameTime(deltaTime, deltaTimeMs);
    m_entityManager->syncUpdateSystems();

    // #HACK Get rid of this shit
    dynamic_cast<ModelSystem&>(*m_sceneSystems[0]).setCamera(cameraPosition);
    dynamic_cast<GrassSystem&>(*m_sceneSystems[1]).setCamera(cameraPosition);
    m_worldLoader.updateByPosition(swiz<X, Z>(cameraPosition));
    m_worldLoader.flush();
    m_terrainBuffer->flush();
}
void FlatScene::beginAsyncUpdate()
{
    if (g_sceneMultithread.value())
    {
        // Schedule async update task
        m_mainThread.schedule([this]() {
            m_entityManager->asyncUpdateSystems();
        });
    }
    else
    {
        // Perform async update task right now
        m_entityManager->asyncUpdateSystems();
    }
}
void FlatScene::endAsyncUpdate()
{
    // Wait for finishing
    m_mainThread.wait();
}


// Create
void FlatScene::createGraphicScene(Renderer& renderer,
                                   const GraphicConfig& graphicConfig)
{
    auto gs = make_unique<GraphicalScene>(graphicConfig);

    // Init terrain buffer

    // Recenter
    gs->recenter(m_worldLoader.worldCenterPosition());

    for (RenderSystem* renderSystem : m_entityManager->copySceneSystems<RenderSystem>())
    {
        gs->addRenderSystem(*renderSystem);
    }

    // Init
    setGraphicalScene(move(gs));
}
void FlatScene::createPhysicalScene()
{
    // Create
    auto ps = make_unique<PhysicalScene>(m_world->config.regionWidth);
    m_worldLoader.addObserver(*ps);

    // #HACK
//     for (int2 chunkIndex : makeRange(int2(-3, -3), int2(3, 3)))
//         ps->enableChunk(chunkIndex);

    // Init
    setPhysicalScene(move(ps));
}
vector<shared_ptr<RenderSystem>>& FlatScene::renderSystems()
{
    return m_renderSystems;
}
