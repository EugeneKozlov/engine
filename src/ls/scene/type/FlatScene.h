/// @file ls/scene/type/FlatScene.h
/// @brief Flat Scene impl
#pragma once

#include "ls/common.h"
#include "ls/core/Worker.h"
#include "ls/scene/EntityManager.h"
#include "ls/scene/RenderSystem.h"
#include "ls/scene/SceneInterface.h"
#include "ls/scene/env/SceneEnvironment.h"
#include "ls/world/StreamingWorld.h"

namespace ls
{
    class AssetManager;
    class TerrainBuffer;
    class SceneSystem;
    class RenderSystem;
    class GraphicConfig;

    /// @addtogroup LsScene
    /// @{

    /// @brief Flat Scene Info
    struct SceneInfo
    {
        /// @brief World name
        string worldName;
        /// @brief Initial chunk
        int2 initialChunk;
        /// @brief World Streaming Config
        WorldStreamingConfig worldStreamingConfig;
    };
    /// @brief Flat Scene impl
    class FlatScene
        : public SceneInterface
        , public WorldObserverInterface
    {
    public:
        /// @brief Scene Info
        const SceneInfo sceneInfo;
    public:
        /// @brief Ctor
        FlatScene(shared_ptr<AssetManager> assetManager, 
                  Renderer* renderer,
                  Worker& worker,
                  const SceneInfo& sceneInfo);
        /// @brief Dtor
        virtual ~FlatScene();
        /// @brief Synchronous scene update
        void syncUpdate(float deltaTime, DeltaTime deltaTimeMs);
        /// @brief Start asynchronous update
        void beginAsyncUpdate();
        /// @brief End asynchronous update
        void endAsyncUpdate();

        /// @brief Create Graphical Scene
        void createGraphicScene(Renderer& renderer,
                                const GraphicConfig& graphicConfig);
        /// @brief Create Physical Scene
        void createPhysicalScene();

        /// @brief Get renderer system
        RenderSystemVector& renderSystems();
    public:
        virtual void onRecenter(const int2& oldCenter,
                                const int2& newCenter,
                                const double3& newCenterPosition) override;
    private:
        // #TODO fix addSystem template
        template <class SceneSystem>
        SceneSystem& addSystem(shared_ptr<SceneSystem> ss)
        {
            m_sceneSystems.push_back(ss);
            return *ss;
        }
    private:
        shared_ptr<AssetManager> m_assetManager;
        Renderer* m_renderer;

        Worker m_mainThread;

        // Objects
        SceneEnvironment m_environment;

        SceneSystemVector m_sceneSystems;
        RenderSystemVector m_renderSystems;
        unique_ptr<EntityManager> m_entityManager;

        // Geometry
        shared_ptr<WorldAsset> m_world;
        StreamingWorld m_worldLoader;

        // Rendering
        shared_ptr<TerrainBuffer> m_terrainBuffer;
    };
    /// @}
}
