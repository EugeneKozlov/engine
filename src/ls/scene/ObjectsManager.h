/** @file ls/scene/ObjectsManager.h
 *  @brief Scene objects manager
 */
#pragma once
#include "ls/common.h"
#include "ls/forward.h"
#include "ls/core/Nameable.h"
#include "ls/world/StreamingWorld.h"
#include "ls/scene/ObjectObserverInterface.h"
#include "ls/scene/ObjectInstance.h"
#include "ls/scene/ObjectPrototype.h"
#include "ls/render/Plotter.h"

namespace ls
{
    /** @addtogroup LsScene
     *  @{
     */
    /** Interface of storage observer.
     */
    class ObjectsManagerObserverInterface
    {
    public:
        /// Add new @a object and bind it to @a chunk
        virtual void onCreateObject(ObjectIterator object);
        /// Remove @a object
        virtual void onDestroyObject(ObjectIterator object);
        /// Add new ghost @a object
        virtual void onCreateGhost(ObjectIterator object);
        /// Remove ghost @a object
        virtual void onDestroyGhost(ObjectIterator object);

        /// Rebind @a object @a from one @a to another chunk
        virtual void onRebindObject(ObjectIterator object,
                                    int2 const& from, int2 const& to);
        /// Add object to scene (aspects are constructed right before this call)
        virtual void onAddObject(ObjectInstance& object);
        /// Remove objects from scene (aspects are destroyed right after this call)
        virtual void onRemoveObject(ObjectInstance& object);
    };
    /// Chunk interface
    class ChunkInterface
    {
    public:
        /// Ctor
        ChunkInterface();
        /// Reset acted objects from chunk
        void resetActed();

        /** @name Gets
         *  @{
         */
        /// Test if objects allocated
        bool isAllocated() const noexcept
        {
            return m_allocated;
        }
        /// Get chunk objects (never modify)
        ObjectsList& objects() noexcept
        {
            return m_objects;
        }
        /// Get globals (never modify)
        ObjectsList& globals() noexcept
        {
            return m_globals;
        }
        /// Get objects acted in chunk (never modify)
        ObjectsList& acted() noexcept
        {
            return m_actedMap;
        }
        /// @}
    protected:
        /// Chunk index
        int2 m_chunk;
        /// Bound objects list
        ObjectsList m_objects;
        /// Global objects list
        ObjectsList m_globals;
        /// Acted objects map
        ObjectsList m_actedMap;
        /// True if chunk is initialized
        bool m_allocated = false;
    };
    /** Storage for all game objects.
     *
     *  - <b>Storage</b>
     *
     *    Each object is bound to chunk.
     *    Internally it is only grid for better access, but width of chunk
     *    must be equal to real chunk width, because external classes
     *    may request information about objects bound to specified chunk.
     *    Chunks are contained in regions, and this containing
     *    has no affection on external usage of this class.
     *    Objects are bound to chunks and regions, but in fact
     *    they are stored in only @b Pool storage.
     *
     *  - <b>Objects</b>
     *
     *    + Objects with placeable aspect are bound to some chunk
     *
     *    + Objects bound to chunk
     *
     *    Objects are marked as acted each frame and should be reset
     *    as soon as they will be transmitted and/or processed.
     *
     */
    class ObjectsManager
    : public Nameable<ObjectsManager>
    , public ObjectObserverInterface
    , public ObservableInterface<ObjectsManagerObserverInterface>
    {
    public:
        /** @name Main
         *  @{
         */
        /// Ctor
        ObjectsManager(uint chunkWidth, uint regionWidth, bool logObjects);
        /// Dtor
        ~ObjectsManager();
        /// @}

        /** @name Objects
         *  @{
         */
        /** Add new object
         *  @param id
         *    Object's ID, fail if occupied
         *  @return Object iterator valid until object removing
         */
        ObjectIterator addObject(ObjectId id = InvalidObjectId);
        /// Remove @b valid object by iterator
        void removeObject(ObjectIterator object);
        /// Remove @b valid object by id.
        void removeObject(ObjectId object);
        /// Test if object ID is used by some object.
        bool isObject(ObjectId id);
        /** Test if object is rebound between chunks
         *  @param id
         *    Object ID
         *  @param clearFlag
         *    Set to reset this flag
         *  @return @c true if object was rebound, @c false otherwise
         */
        bool wasRebound(ObjectId id, bool clearFlag = true);
        /// Get object by id, return invalid if not found.
        ObjectIterator findObject(ObjectId id);
        /// Get object by id, fail if not found.
        ObjectInstance& getObject(ObjectId id);
        /// @}

        /** @name Chunks
         *  @{
         */
        /// Allocate chunk and construct all objects inside
        void allocateChunk(int2 const& chunk);
        /// Deallocate chunk and destroy all objects inside
        void destroyChunk(int2 const& chunk);
        /** Get global objects
         *  @return Objects list
         */
        ObjectsList& globalObjects();
        /// Remove all objects from chunk
        void clearChunk(int2 const& chunk);
        /// Get chunk
        ChunkInterface& chunk(int2 const& chunk);
        /// @}

        /** @name Gets
         *  @{
         */
        /// Get chunk width
        uint chunkWidth() const noexcept
        {
            return m_chunkWidth;
        }
        /// Get region width
        uint regionWidth() const noexcept
        {
            return m_regionWidth;
        }
        /// @}

        /** @name Debug
         *  @{
         */
        /** Debug plot objects
         *  @param plotter
         *    Plot function
         *  @param zero
         *    Virtual zero offset from real real world center
         *  @param viewer
         *    Viewer position
         *  @param distance
         *    Rendering distance
         */
        void debugPlot(function<void(ObjectInstance& object, float4x4 const& matrix) > plotter,
                       double3 const& zero, double3 const& viewer, uint distance);
        /// Get objects pool for @b debug purposes.
        Pool<ObjectInstance> const& debugPool();
        /// @}
    protected:
        virtual void onAdd(ObjectId id) override;
        virtual void onRebind(ObjectIterator object,
                              int2 const& fromChunk, int2 const& toChunk) override;
        virtual void onRemove(ObjectIterator object) override;
        virtual void onAct(ObjectIterator object) override;
    private:
        class Chunk;
        class Region;
        /// Objects pool type
        using ObjectsPool = Pool<ObjectInstance>;
        /// Areas map
        using RegionMap = map<int2, shared_ptr<Region>>;
        /// Find VALID area by chunk index
        RegionMap::iterator findArea(int2 const& chunk,
                                     int2& localChunkIndex,
                                     int2& regionIndex, bool canCreateRegion);
        /// Find VALID chunk by chunk index
        Chunk& findChunk(int2 const& chunk, bool canCreateRegion);
        /// Find area by chunk index
        RegionMap::iterator findRegionUnsafe(int2 const& chunk,
                                             int2& localChunkIndex,
                                             int2& regionIndex);
        /// Find chunk by chunk index
        Chunk* findChunkUnsafe(int2 const& chunk);
        /// Convert chunk index to area index
        int2 chunk2region(int2 const& globalChunk, int2& localChunk);
        /// Destroy object
        void destroyObject(ObjectId object);
        /// Initialize aspects
        void initializeObject(ObjectInstance& object);
        /// De-initialize aspects
        void deinitializeObject(ObjectInstance& object);
        /// Log object info
        template <class ... Args>
        void logObject(ObjectInstance& object, Args const& ... args)
        {
            if (!m_logObjects)
                return;
            const char* tag = object.isGlobal() ? "Global object" : "Object";
            if (object.isGhost())
                tag = "Ghost object";
            logDebug(tag, " [", object.id(), ":", object.type(), "] ", args...);
        }
    protected:
        /// Width of chunk (in meters)
        uint const m_chunkWidth;
        /// Number of chunks in region
        uint const m_regionWidth;
        /// Set to log objects
        bool m_logObjects;

        /// Objects pool
        ObjectsPool m_objectsPool;
        /// Regions
        RegionMap m_regions;
        /// Global objects
        ObjectsList m_globalObjects;

        /// Rebound objects
        set<ObjectId> m_reboundObjects;
    };
    /// @}
}
