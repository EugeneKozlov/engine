/// @file ls/scene/SceneInterface.h
/// @brief Scene Interface
#pragma once

#include "ls/common.h"
#include "ls/render/Camera.h"

namespace ls
{
    class Renderer;
    class AssetManagerOld;
    class GraphicalScene;
    class PhysicalScene;

    /// @addtogroup LsScene
    /// @{

    /// @brief Scene Manager
    class SceneInterface
        : Noncopyable
    {
    public:
        /// @brief Ctor
        SceneInterface();
        /// @brief Dtor
        virtual ~SceneInterface();

        /// @brief Get Graphical Scene
        GraphicalScene* graphicalScene();
        /// @brief Get Physical Scene
        PhysicalScene* physicalScene();
    protected:
        void setGraphicalScene(unique_ptr<GraphicalScene>&& graphicalScene);
        void setPhysicalScene(unique_ptr<PhysicalScene>&& physicalScene);
    private:
        unique_ptr<GraphicalScene> m_graphicalScene;
        unique_ptr<PhysicalScene> m_physicalScene;
    };
    /// @}
}
