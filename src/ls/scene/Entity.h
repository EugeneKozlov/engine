/// @file ls/scene/Entity.h
/// @brief
#pragma once

#include "ls/common.h"
#include "ls/scene/Component.h"

namespace ls
{
    class EntityPool;
    class GlobalComponentContainer;

    /// @addtogroup LsScene
    /// @{

    /// @brief Entity ID
    using EntityId = uint;
    /// @brief Invalid Entity ID
    static const EntityId InvalidEntityId = 0;
    /// @brief Max number of entity components
    static const uint MaxEntityComponents = 12;
    /// @brief Entity
    class Entity
        : Noncopyable
    {
    public:
        /// @brief Common Entity Pool is used for temporary objects
        static EntityPool CommonEntityPool;
        /// @brief Component Type Mask
        static const ushort ComponentTypeMask = 0x3fff;
        /// @brief Component Flags Offset
        static const ushort ComponentFlagsMask = 0xc000;
        static_assert((ComponentTypeMask ^ ComponentFlagsMask) == 0xffff, "bad masks");
    public:
        /// @brief Temporary ctor
        Entity();
        /// @brief Ctor
        Entity(EntityId entityId, EntityPool& pool);
        /// @brief Dtor
        ~Entity();
        /// @brief Get entity ID
        EntityId id() const
        {
            return m_id;
        }
        /// @brief Release all components
        void release();
        /// @brief Serialize
        void serialize(Buffer& dest);
        /// @brief Deserialize
        /// @throw IoException if cannot read data
        /// @throw IoException if cannot find global component
        void deserialize(Buffer& src, GlobalComponentContainer& global);
        /// @brief Clone entity
        /// @return Entity ID
        EntityId clone(const Entity& src);

        /// @brief Add shared component
        /// @throw OverflowException if too many components
        /// @throw LogicException if component is not shared
        void addSharedComponent(Component& component);
        /// @brief Add prototyped component
        /// @throw OverflowException if too many components
        /// @throw LogicException if component is not shared
        void addPrototypedComponent(Component& prototype);
        /// @brief Create and add component
        /// @throw OverflowException if too many components
        Component& addComponent(const ComponentType type);
        /// @brief Create and add component (compile-time type)
        /// @throw OverflowException if too many components
        template <class DerivedComponent>
        DerivedComponent& addComponent()
        {
            return *addComponent(DerivedComponent::Type)
                .template cast<DerivedComponent>();
        }

        /// @brief Find component by type
        Component* findComponent(const ComponentType type);
        /// @brief Find component by type
        template <class DerivedComponent>
        DerivedComponent* findComponent()
        {
            Component* component = findComponent(DerivedComponent::Type);
            return component ? component->template cast<DerivedComponent>() : nullptr;
        }
        /// @brief Begin
        auto begin()
        {
            return m_components.begin();
        }
        /// @brief End
        auto end()
        {
            return m_components.begin() + m_numComponents;
        }
    public:
        /// Internal index used for sorting entities (optional)
        uint sortIndex = 0;
    private:
        EntityPool& m_pool;
        const EntityId m_id;

        uint m_numComponents = 0;

        array<ComponentType, MaxEntityComponents> m_componentsTypes;
        array<Component*, MaxEntityComponents> m_components;
    };
    /// @brief Entity and component group
    template <class ... Args>
    struct EntityComponentTuple
    {
        /// @brief Entity pointer
        Entity* entity = nullptr;
        /// @brief Component pointers
        tuple<Args*...> components;
    private:
        struct Filler
        {
            Entity* entity;
            template <class DerivedComponent>
            void operator() (DerivedComponent*& component)
            {
                component = entity->findComponent<DerivedComponent>();
            }
        };
        struct Checker
        {
            bool valid = true;
            template <class DerivedComponent>
            void operator() (DerivedComponent* component)
            {
                valid = valid && !!component;
            }
        };
    public:
        /// @brief Default ctor
        EntityComponentTuple() = default;
        /// @brief Ctor
        EntityComponentTuple(Entity& entity)
            : entity(&entity)
        {
            for_each(components, Filler{ &entity });
        }
        /// @brief Get component by type
        template <class DerivedComponent>
        DerivedComponent* get() const
        {
            return elementByType<DerivedComponent*>(components);
        }
        /// @brief Is every pointer valid?
        bool vaild() const
        {
            if (!entity)
                return false;
            Checker checker;
            for_each(components, std::ref(checker));
            return checker.valid;
        }
    };
    /// @}
}
