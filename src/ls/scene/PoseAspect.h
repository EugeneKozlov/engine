/** @file ls/scene/PoseAspect.h
 *  @brief Simplest pose aspect
 */

#pragma once
#include "ls/common.h"
#include "ls/scene/ObjectInstance.h"
#include "ls/scene/ObjectPrototype.h"
#include "ls/io/Codec.h"
#include "ls/xml/common.h"
#include "ls/scene/core/ObjectPose.h"

namespace ls
{
    /** @addtogroup LsScene
     *  @{
     */
    /** Simplest pose aspect prototype.
     *
     *  You can override 'tiny' flag because
     *  you should inherit any pose from this class
     */
    class PoseAspectPrototype
    : public AspectPrototype
    {
        LS_ASPECT(PoseAspect);
    public:
        /// Category
        static constexpr AspectCategory Category = AspectCategory::Unspecified;
        /// Aspect name
        static constexpr const char* Name()
        {
            return "pose";
        }
        /// Ctor
        PoseAspectPrototype(XmlNode aspectNode,
                            AspectCategory category = Category,
                            const char* name = Name(),
                            bool isTiny = true);
    public:
        virtual void gatherState(AnyMap& source, AspectState& dest) const override;
        virtual void encodeState(AspectState const& source, DynamicStream& dest) const override;
        virtual void decodeState(DynamicStream& source, AspectState& dest) const override;
        virtual void interpolateState(AspectState const& left, AspectState const& right,
                                      AspectState& dest, float factor) const override;

    private:
        Encoding m_positionEncoding;
        Encoding m_rotationEncoding;
        PoseAspectState m_default;
    };
    /** Simplest pose aspect
     */
    class PoseAspect
    : public AspectInstance
    {
    public:
        /** Ctor
         *  @param prototype
         *    Aspect prototype
         */
        PoseAspect(PoseAspectPrototype const& prototype);
        /// Dtor
        virtual ~PoseAspect();

        /** @name Gets
         *  @{
         */
        /// Get position
        double3 const& position() const noexcept
        {
            return m_pose.position();
        }
        /// @}
    public:
        virtual void extract(ObjectState& data) override;
        virtual void inject(ObjectState const& data) override;
    protected:
        /// State
        ObjectPose m_pose;
    };
    /// @}
}

