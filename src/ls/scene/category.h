/** @file ls/scene/category.h
 *  @brief Scene object categories
 */

#pragma once
#include "ls/common.h"

namespace ls
{
    /** @addtogroup LsScene
     *  @{
     */
    /// Category
    enum class AspectCategory : uint
    {
        Unspecified = 0,
        /// Physical
        Physical
    };
    /// @}
}