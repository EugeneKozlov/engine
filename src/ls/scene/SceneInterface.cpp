#include "pch.h"
#include "ls/scene/SceneInterface.h"
#include "ls/scene/graphical/GraphicalScene.h"
#include "ls/scene/physical/PhysicalScene.h"

using namespace ls;
SceneInterface::SceneInterface()
{
}
SceneInterface::~SceneInterface()
{
}


// Sets/Gets
void SceneInterface::setGraphicalScene(unique_ptr<GraphicalScene>&& graphicalScene)
{
    m_graphicalScene = move(graphicalScene);
}
void SceneInterface::setPhysicalScene(unique_ptr<PhysicalScene>&& physicalScene)
{
    m_physicalScene = move(physicalScene);
}
GraphicalScene* SceneInterface::graphicalScene()
{
    return m_graphicalScene.get();
}
PhysicalScene* SceneInterface::physicalScene()
{
    return m_physicalScene.get();
}
