/** @file ls/scene/ObjectsFactory.h
 *  @brief Scene objects factory
 */
#pragma once
#include "ls/common.h"
#include "ls/xml/common.h"
#include "ls/scene/ObjectState.h"
#include "ls/scene/ObjectInstance.h"
#include "ls/scene/ObjectPrototype.h"

namespace ls
{
    /** Scene objects factory. Class is used to:
     *  -# Create objects by prototypes
     *  -# Create objects by encoded information transmitted from
     *  -# Encode/decode objects to network interaction
     */
    class ObjectsFactory
    {
    public:
        /** @name Main
         *  @{
         */
        /// Ctor
        ObjectsFactory();
        /// Dtor
        ~ObjectsFactory();
        /** Inject prototypes from XML.
         *  @param xml
         *    XML config text
         */
        void inject(string const& xml);
        /** Read prototypes from file
         *  @param fileSystem
         *    File system
         *  @param name
         *    Config file name
         */
        void read(FileSystemInterface& fileSystem, string const& name);
        /// @}

        /** @name Factory
         *  @{
         */
        /// Set parameter
        template<class T>
        void addParameter(const char* name, T const& param)
        {
            m_initArray[name] = param;
        }
        /// Reset parameter by name
        void resetParameter(const char* name = nullptr);
        /// Get parameter pointer, 0 if not found
        template <class T>
        T* getParameter(const char* name)
        {
            map<string, any>::iterator param = m_initArray.find(name);
            if (param != m_initArray.end())
                return any_cast<T>(&param->second);
            else
                return nullptr;
        }
        /// @}

        /** @name Gets
         *  @{
         */
        /// Get prototype by name, throw on error
        ObjectPrototype& findPrototype(const char* name);
        /// Get prototype by ID, throw on error
        ObjectPrototype& prototype(ObjectType type);
        /// @}

        /** @name Construct/encode by parameter map
         *  @{
         */
        /// Encode object described by parameters to byte array
        uint describe(ObjectType type,
                      void* streamData, uint streamSize,
                      bool resetParameters = true);
        /// Create object described by parameters
        bool build(ObjectState& state,
                   ObjectId id, ObjectPrototype& proto,
                   bool resetParameters = true);
        /// @}

        /** @name Encode/decode objects
         *  @{
         */
        /// Encode frame to byte array, fully or partial.
        uint encode(const ObjectState* defaultState, ObjectState const& objectState,
                    void* streamData, uint streamSize,
                    bool encodeAll);
        /// Decode frame from byte array, film will be rewritten.
        uint decode(ObjectState& objectState,
                    const void* streamData, uint streamSize);
        /// Prepare @b any object for frame injection, frame will @b not injected; object will not be created, it just will be registered.
        bool prepare(ObjectInstance& object, ObjectState const& objectState);
        /// @}
    private:
        void loadProto(XmlNode nodeProto);
    private:
        /// Prototypes
        map<ObjectType, ObjectPrototype> m_prototypes;
        /// Prototypes map
        map<string, ObjectType> m_protoNames;
        /// Maker array
        map<string, any> m_initArray;
    };
}
