/// @file ls/scene/RenderSystem.h
/// @brief Render System
#pragma once

#include "ls/common.h"
#include "ls/render/BatchRenderer.h"

namespace ls
{
    class SceneView;

    /// @addtogroup LsScene
    /// @{

    /// @brief Render System
    class RenderSystem
    {
    public:
        /// @brief Ctor
        RenderSystem();
        /// @brief Dtor
        virtual ~RenderSystem();

        /// @brief Synchronous update call
        virtual void syncUpdate(const float deltaTime, const DeltaTime deltaTimeMs);

        /// @name Render
        /// @note It should be safe to call this function from main rendering thread
        /// @note Prefer not to lock anything in rendering thread
        /// @{

        /// @brief Perform rendering
        virtual void render(BatchRenderer& batchRenderer, const SceneView* sceneView) = 0;
        /// @brief Resize main view-port
        virtual void resizeViewport(const uint width, const uint height);
        /// @}
    };
    /// @brief Render Systems
    using RenderSystemVector = vector<shared_ptr<RenderSystem>>;
    /// @}
}