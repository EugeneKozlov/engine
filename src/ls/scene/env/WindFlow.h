/// @file ls/scene/env/WindFlow.h
/// @brief Wind Flow
#pragma once

#include "ls/common.h"

namespace ls
{
    /// @addtogroup LsScene
    /// @{

    /// @brief Wind Info
    struct WindInfo
    {
        /// @brief Intensity
        float intensity = 0.0f;
        /// @brief Turbulence
        float turbulence = 0.0f;
        /// @brief Pulse magnitude
        float pulseMagnitude = 0.0f;
        /// @brief Pulse frequency
        float pulseFrequency = 1.0f;
    };
    /// @brief Wind Flow
    class WindFlow
    {
    public:
        /// @brief Ctor
        WindFlow();
        /// @brief Dtor
        ~WindFlow();
        /// @brief Update
        void update(float deltaTime);
        /// @brief Set wind
        void setDirectionalWind(const float3& direction, const WindInfo& info);

        /// @brief Compute wind
        float3 computeWind(const double3& position, const float phaseOffset) const;
    private:
        static float triangleWave(const float x);
        static float smoothWave(const float x);
    private:
        float m_directionalFactor = 0.0f;
        float3 m_direction;
        WindInfo m_directionalInfo;
    };

    /// @}
}