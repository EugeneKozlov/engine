/// @file ls/scene/env/SceneEnvironment.h
/// @brief Scene Environment
#pragma once

#include "ls/common.h"
#include "ls/scene/env/WindFlow.h"

namespace ls
{
    /// @addtogroup
    /// @{

    /// @brief Scene Environment contains common info about scene
    class SceneEnvironment
    {
    public:
        /// @brief Wind flow info
        WindFlow windFlow;
    public:
        /// @brief Ctor
        SceneEnvironment();
        /// @brief Dtor
        ~SceneEnvironment();
        /// @brief Reset time
        void resetTime();
        /// @brief Update
        void update(const float deltaTime, const DeltaTime deltaTimeMs,
                    const double3& viewerPosition);

        /// @brief Get current viewer position
        const double3& viewerPosition() const 
        {
            return m_viewerPosition;
        }
        /// @brief Get last delta time
        float lastDeltaTime() const
        {
            return m_lastDeltaTime;
        }
        /// @brief Get last delta time (ms)
        DeltaTime lastDeltaTimeMs() const
        {
            return m_lastDeltaTimeMs;
        }
        /// @brief Get current time
        double currentTime() const
        {
            return m_currentTime;
        }
        /// @brief Get current time (ms)
        DeltaTime currentTimeMs() const
        {
            return m_currentTimeMs;
        }
    private:
        double3 m_viewerPosition;
        float m_lastDeltaTime = 0.0f;
        DeltaTime m_lastDeltaTimeMs = 0;
        double m_currentTime = 0.0;
        DiscreteTime m_currentTimeMs = 0;
    };

    /// @}
}