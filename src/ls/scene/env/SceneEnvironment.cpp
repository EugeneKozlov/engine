#include "pch.h"
#include "ls/scene/env/SceneEnvironment.h"

using namespace ls;
SceneEnvironment::SceneEnvironment()
{

}
SceneEnvironment::~SceneEnvironment()
{

}        
void SceneEnvironment::resetTime()
{
    m_currentTime = 0.0;
    m_currentTimeMs = 0;
}
void SceneEnvironment::update(const float deltaTime, const DeltaTime deltaTimeMs,
                              const double3& viewerPosition)
{
    m_viewerPosition = viewerPosition;

    m_lastDeltaTime = deltaTime;
    m_lastDeltaTimeMs = deltaTimeMs;
    m_currentTimeMs += deltaTimeMs;
    m_currentTime = m_currentTimeMs * 0.001;

    windFlow.update(deltaTime);
}

