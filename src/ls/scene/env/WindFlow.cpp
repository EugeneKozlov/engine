#include "pch.h"
#include "ls/scene/env/WindFlow.h"

using namespace ls;
WindFlow::WindFlow()
{

}
WindFlow::~WindFlow()
{

}    
void WindFlow::update(float deltaTime)
{
    m_directionalFactor += deltaTime * m_directionalInfo.pulseFrequency;
    m_directionalFactor = fract(m_directionalFactor);
}
void WindFlow::setDirectionalWind(const float3& direction, const WindInfo& info)
{
    m_direction = normalize(direction);
    m_directionalInfo = info;
}
float3 WindFlow::computeWind(const double3& /*position*/, const float phaseOffset) const
{
    const float dirFactor = 2.0f * smoothWave(fract(m_directionalFactor + phaseOffset)) - 1.0f;
    const float dirIntensity = m_directionalInfo.intensity + m_directionalInfo.pulseMagnitude * dirFactor;
    return m_direction * dirIntensity;
}


// Math
float WindFlow::triangleWave(const float x)
{
    return 2.0f * abs(x - 0.5f);
}
float WindFlow::smoothWave(const float x)
{
    const float y = triangleWave(x);
    return y * y * (3.0f - 2.0f * y);
}
