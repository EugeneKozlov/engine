#include "pch.h"
#include "ls/scene/system/ModelSystem.h"

#include "ls/asset/AssetManager.h"
#include "ls/scene/Entity.h"
#include "ls/scene/component/ModelComponent.h"
#include "ls/scene/component/RenderComponent.h"
#include "ls/scene/component/TransformComponent.h"
#include "ls/scene/system/ModelSystemRenderer.h"
#include "ls/scene/system/StaticModelRenderTree.h"
#include "ls/asset/WorldAsset.h"

using namespace ls;
ModelSystem::ModelSystem(shared_ptr<AssetManager> assetManager,
                         Renderer& renderer, const float baseFovY)
    : m_assetManager(assetManager)
    , m_renderer(renderer)
    , m_baseFovY(baseFovY)
    , m_impl(make_unique<ModelSystemRenderer>(assetManager))
{

}
ModelSystem::~ModelSystem()
{

}
void ModelSystem::setCamera(const double3& position)
{
    m_cameraPosition = position;
}


// Render
void ModelSystem::render(BatchRenderer& renderer, const SceneView* sceneView)
{
    m_impl->processNotifications();
    m_impl->render(renderer, sceneView);
}


// Implement Scene System
void ModelSystem::syncUpdate()
{
    // Update renderer
    m_impl->syncUpdate(elapsedSec(), elapsedMsec());
}
void ModelSystem::asyncUpdate()
{
    // Update static entities
    for (auto& iterRegion : m_staticEntities)
    {
        const vector<Node>& entities = iterRegion.second;
        for (const Node& node : entities)
        {
            // #TODO Remove elapsedSec
            updateNodeLod(node, elapsedSec());
        }
    }
}
struct ModelSystem::ModelSystemCache : SceneSystemCacheInterface
{
    vector<ModelSystem::Node> components;
    shared_ptr<StaticModelRenderTree> tree;
};
SceneSystemCache ModelSystem::prepareStaticEntities(const EntitiesList& entities, WorldRegion& region)
{
    shared_ptr<ModelSystemCache> cache = make_shared<ModelSystemCache>();

    // Load components
    for (const auto& entity : entities)
    {
        Node node = readComponents(*entity.first, *entity.second);
        if (!node.isValid())
            continue;

        initializeModelComponent(*node.modelComponent);
        node.renderComponent->primaryModel = node.modelComponent;
        node.renderComponent->secondaryModel = node.modelComponent;
        cache->components.push_back(node);
    }

    // Build tree
    // #TODO Make 5 as constant
    cache->tree = make_shared<StaticModelRenderTree>(5, entities, m_impl->allocator);

    return cache;
}
void ModelSystem::addStaticEntities(const EntitiesList& entities,
                                    WorldRegion& region,
                                    SceneSystemCache preparedData)
{
    ModelSystemCache& cache = dynamic_cast<ModelSystemCache&>(*preparedData);

    // Move to pool
    m_staticEntities.emplace(region.index(), move(cache.components));
    m_staticTrees.emplace(region.index(), cache.tree);

    // Send notification
    ModelSystemAddStaticTreeNotif notif;
    notif.tree = cache.tree;
    m_impl->sendNotification(notif);
}
void ModelSystem::removeStaticEntities(const int2& regionIndex)
{
    // Send notification
    ModelSystemRemoveStaticTreeNotif notif;
    notif.tree = m_staticTrees[regionIndex];
    m_impl->sendNotification(notif);

    // Clear pools
    m_staticEntities.erase(regionIndex);
    m_staticTrees.erase(regionIndex);
}
void ModelSystem::addEntity(Entity& entity, Component& component)
{
    //throw std::logic_error("The method or operation is not implemented.");
}
void ModelSystem::removeEntity(Entity& entity)
{
    //throw std::logic_error("The method or operation is not implemented.");
}
ComponentType ModelSystem::keyComponent() const
{
    return ComponentType::Model;
}


// Init/update nodes
ModelSystem::Node ModelSystem::readComponents(Entity& entity, Component& modelComponent)
{
    Node node;
    node.modelComponent = modelComponent.cast<ModelComponent>();
    node.transformComponent = entity.findComponent<TransformComponent>();
    node.renderComponent = entity.findComponent<RenderComponent>();
    debug_assert(node.modelComponent);
    return node;
}
void ModelSystem::initializeModelComponent(ModelComponent& modelComponent)
{
    // Initialize model component if has no handle
    if (!modelComponent.handle)
    {
        // Try to load asset
        if (!modelComponent.asset)
        {
            try
            {
                modelComponent.asset = Asset::cast<ModelAsset>(
                    m_assetManager->loadAsset(AssetType::Model, modelComponent.assetName.str()));
            }
            catch (IoException& ex)
            {
                logError(ex.what());
            }
            catch (LogicException& ex)
            {
                logError(ex.what());
            }
        }

        // Create model internal handle
        if (modelComponent.asset)
        {
            modelComponent.handle = m_impl->allocator.allocateModelHandle(modelComponent.asset);
        }
    }

    // Skip if failed
    if (!modelComponent.asset)
    {
        return;
    }

    // Compute distances
    modelComponent.numLods = modelComponent.asset->numLevels();
    for (uint lodIndex = 0; lodIndex < modelComponent.numLods; ++lodIndex)
    {
        const float switchSize = modelComponent.asset->switchSize(lodIndex);
        const float modelSize = static_cast<float>(modelComponent.asset->aabb().maxExtent());
        const float inoutGap = modelComponent.asset->inoutGap(lodIndex);
        const float inner = computeSwitchDistance(switchSize, modelSize, m_baseFovY);
        const float outer = inner + inoutGap;

        modelComponent.innerDistances2[lodIndex] = inner * inner;
        modelComponent.outerDistances2[lodIndex] = outer * outer;
    }
}
void ModelSystem::updateNodeLod(const Node& node, const float deltaTime)
{
    TransformComponent& transformComponent = *node.transformComponent;
    ModelComponent& modelComponent = *node.modelComponent;
    RenderComponent& renderComponent = *node.renderComponent;

    // Mix and skip if mixing
    if (renderComponent.mixFactor > 0.0)
    {
        // #TODO customize LOD switch speed
        renderComponent.mixFactor = max(0.0f, renderComponent.mixFactor - deltaTime);

        // Send notification
        ModelSystemChangeModelNotif notif;
        notif.handle = node.renderComponent->handle;
        notif.primaryModel = node.renderComponent->primaryModel->handle;
        notif.primaryLod = node.renderComponent->primaryLod;
        notif.secondaryModel = node.renderComponent->secondaryModel->handle;
        notif.secondaryLod = node.renderComponent->secondaryLod;
        notif.mixFactor = renderComponent.mixFactor;
        m_impl->sendNotification(notif);
        return;
    }

    // Compute lod
    const double dist2 = square_distance(transformComponent.pose.position(), m_cameraPosition);
    const uint newLod = computeBestLod(modelComponent, 
                                       renderComponent.primaryLod, 
                                       static_cast<float>(dist2));

    // Switch
    if (renderComponent.primaryLod != newLod)
    {
        // Start fade
        renderComponent.secondaryLod = renderComponent.primaryLod;
        renderComponent.primaryLod = newLod;
        renderComponent.mixFactor = 1.0;

        // Send notification
        ModelSystemChangeModelNotif notif;
        notif.handle = node.renderComponent->handle;
        notif.primaryModel = node.renderComponent->primaryModel->handle;
        notif.primaryLod = node.renderComponent->primaryLod;
        notif.secondaryModel = node.renderComponent->secondaryModel->handle;
        notif.secondaryLod = node.renderComponent->secondaryLod;
        notif.mixFactor = renderComponent.mixFactor;
        m_impl->sendNotification(notif);
    }
}


// Compute LOD
float ModelSystem::computeSwitchDistance(const float switchSize, const float modelSize, const float fovY)
{
    return modelSize / (2 * switchSize * std::tan(fovY / 2));
}
uint ModelSystem::computeBestLod(ModelComponent& modelComponent,
                                 const uint currentLod, const float dist2)
{
    // Skip if has no model
    if (!modelComponent.asset)
    {
        return ModelAsset::MaxLods;
    }

    // Compute best LOD
    uint bestLod = ModelAsset::MaxLods;
    for (uint lodIndex = 0; lodIndex < modelComponent.numLods; ++lodIndex)
    {
        if (dist2 < modelComponent.innerDistances2[lodIndex])
        {
            bestLod = lodIndex;
            break;
        }
        else if (dist2 < modelComponent.outerDistances2[lodIndex])
        {
            bestLod = clamp<ubyte>(currentLod, lodIndex, lodIndex + 1);
            break;
        }
    }

    // Skip if not rendered
    if (bestLod >= modelComponent.numLods)
    {
        return ModelAsset::MaxLods;
    }

    // Request best lod and find approximated
    bestLod = modelComponent.asset->findLevel(bestLod);

    return bestLod;
}
