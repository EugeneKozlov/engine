/// @file ls/scene/system/ModelSystemRenderer.h
/// @brief Model Render System
#pragma once

#include "ls/common.h"
#include "ls/asset/MaterialAsset.h"
#include "ls/core/Pool.h"
#include "ls/core/Notification.h"
#include "ls/scene/RenderSystem.h"
#include "ls/scene/SceneSystem.h"
#include "ls/scene/system/ModelSystemObjectHandle.h"

namespace ls
{
    class AssetManager;
    class MaterialAsset;
    class MeshAsset;
    class ModelAsset;

    class StaticModelRenderTree;

    /// @addtogroup LsScene
    /// @{

    struct ModelSystemModelHandle;
    struct ModelSystemObjectHandle;
    struct ModelSystemDrawable
    {
        ObjectPose pose; ///< Object pose
        float2 fadeFactor; ///< Fade factor
        float distance2; ///< Square of distance
    };
    /// @brief Model System Add Static Tree Notification
    struct ModelSystemAddStaticTreeNotif
    {
        shared_ptr<StaticModelRenderTree> tree; ///< Constructed tree
    };
    /// @brief Model System Remove Static Tree Notification
    struct ModelSystemRemoveStaticTreeNotif
    {
        shared_ptr<StaticModelRenderTree> tree; ///< Tree to remove
    };
    /// @brief Model System Change Model Notification
    struct ModelSystemChangeModelNotif
    {
        ModelSystemObjectHandle* handle = nullptr;
        ModelSystemModelHandle* primaryModel = nullptr;
        uint primaryLod = 0;
        ModelSystemModelHandle* secondaryModel = nullptr;
        uint secondaryLod = 0;
        float mixFactor = 0.0f;
    };
    /// @brief Model System Object Instance Cloud
    struct ModelSystemInstanceCloud
    {
        vector<ModelSystemDrawable> instances;
        AABB aabb;
        ModelSystemModelHandle* modelHandle = nullptr;
        bool drawIfDepthOnly = false;
    };
    /// @brief Shared Pointer on Model System Object Instance Cloud
    using ModelSystemInstanceCloudSPtr = shared_ptr<ModelSystemInstanceCloud>;
    /// @brief Model System Add Instance Cloud Notification
    struct ModelSystemAddCloudNotification
    {
        ModelSystemInstanceCloudSPtr cloud;
    };
    /// @brief Model System Remove Instance Cloud Notification
    struct ModelSystemRemoveCloudNotification
    {
        ModelSystemInstanceCloudSPtr cloud;
    };
    /// @brief Model System Notification
    using ModelSystemNotif = NotificationT<
        ModelSystemAddStaticTreeNotif,
        ModelSystemRemoveStaticTreeNotif,
        ModelSystemChangeModelNotif,
        ModelSystemAddCloudNotification,
        ModelSystemRemoveCloudNotification
    >;

    /// @brief Model System Renderer Allocator
    class ModelSystemRendererAllocator
    {
    public:
        /// @brief Allocate model handle
        ModelSystemModelHandle* allocateModelHandle(shared_ptr<ModelAsset> modelAsset);
        /// @brief Allocate object handle
        ModelSystemObjectHandle* allocateObjectHandle();
        /// @brief Deallocate object handle
        void deallocateObjectHandle(ModelSystemObjectHandle* handle);

        /// @brief Ctor
        ModelSystemRendererAllocator();
        /// @brief Dtor
        ~ModelSystemRendererAllocator();
    private:
        Pool<ModelSystemModelHandle> m_modelPool; ///< Pool of models
        Pool<ModelSystemObjectHandle> m_objectPool; ///< Pool of objects

        hamap<shared_ptr<ModelAsset>, ModelSystemModelHandle*> m_modelHandles; ///< Model handles
    };

    /// @brief Model System Renderer
    /// @note It would be better to pre-request at least last lod of each object
    class ModelSystemRenderer
        : public RenderSystem
        , public NotificationReceiver<ModelSystemRenderer, ModelSystemNotif>
    {
    public:
        /// @brief Draw Instance
        struct DrawInstance
        {
            ObjectPose pose; ///< Pose in world
            float2 fade; ///< Fade factors
        };
        /// @brief Rendering Batch
        struct Batch
        {
            ProgramHandle shaderProgram; ///< 1st, program
            MeshAsset* mesh;             ///< 2nd, mesh
            MaterialAsset* material;     ///< 3rd, material

            StateHandle state; ///< Renderer state
            GeometryBucket geometry; ///< Geometry
            ResourceBucket resources; ///< Resources
            uint startIndex = 0; ///< Start index
            uint numIndices = 0; ///< Number of indices
            uint baseVertex = 0; ///< Base vertex

            ConstArrayWrapper<ModelSystemDrawable> entities; ///< Entities to render
        };
        /// @brief Allocator of internal stuff
        /// @note This class is designed for using in update thread.
        ///       Don't use it in rendering thread.
        ModelSystemRendererAllocator allocator;
    public:
        /// @brief Ctor
        ModelSystemRenderer(shared_ptr<AssetManager> assetManager);
        /// @brief Dtor
        ~ModelSystemRenderer();

        /// @brief Process Add Static Tree Notification
        void processNotification(ModelSystemAddStaticTreeNotif& notif);
        /// @brief Process Remove Static Tree Notification
        void processNotification(ModelSystemRemoveStaticTreeNotif& notif);
        /// @brief Process Model System Change Model Notification
        void processNotification(ModelSystemChangeModelNotif& notif);
        /// @brief Process Model System Add Cloud Notification
        void processNotification(ModelSystemAddCloudNotification& notif);
        /// @brief Process Model System Remove Cloud Notification
        void processNotification(ModelSystemRemoveCloudNotification& notif);

        /// @name Implement Render System
        /// @{
        virtual void syncUpdate(const float deltaTime, const DeltaTime deltaTimeMs) override;
        virtual void render(BatchRenderer& batchRenderer, const SceneView* sceneView) override;
        /// @}
    public:
        /// @brief Add drawable to render lists
        static void addDrawableToList(const ModelSystemGeometryDesc& geometry,
                                      const ModelSystemDrawable& drawable,
                                      ModelRenderBatchList& batches);
        /// @brief Add object to render lists
        static void addObjectToList(ModelSystemObjectHandle* object,
                                    const float distance2,
                                    ModelRenderBatchList& batches);
    private:
        /// @brief Clear all buffers before rendering
        void clearBuffers();
        void commitConstants(const SceneView& sceneView, BatchRenderer& batchRenderer) const;
        /// @brief Build batches for all static trees
        void buildStaticTreeBatches(const SceneView& sceneView);
        void buildInstanceCloudBatches(const SceneView& sceneView);
        void compileModelBatches(const SceneView& sceneView);
        void buildBatchByModel(const ModelSystemGeometryDesc& modelBatch, vector<Batch>& batches,
                               const SceneView& sceneView) const;
        void drawBatches(const vector<Batch>& batches, const SceneView& sceneView,
                         BatchRenderer& batchRenderer);

        /// @name Write instances to buffer
        /// @{
        template <class Buffer>
        void writeInstanceToBuffer(const SceneView& sceneView,
                                   const ModelSystemDrawable& src, Buffer& dest);
        /// @brief Write part of source array to destination
        template <class Buffer>
        void writeInstanceArrayToBuffer(const SceneView& sceneView,
                                        ConstArrayWrapper<ModelSystemDrawable> srcArray,
                                        ArrayWrapper<Buffer> destArray,
                                        const uint offset, const uint count);

        /// @}
    private:
        shared_ptr<AssetManager> m_assetManager;
        Renderer* m_renderer = nullptr;

        /// @name Objects to render
        /// @{
        haset<shared_ptr<StaticModelRenderTree>> m_staticTrees;
        haset<ModelSystemInstanceCloudSPtr> m_instanceClouds;
        /// @}

        /// @name Constant buffer
        /// @{
        BufferHandle m_handleGlobalUniformBuffer;
        BufferHandle m_handleModelUniformBuffer;
        /// @}

        /// @name Batches
        /// @{
        ModelRenderBatchList m_modelBatches;
        vector<Batch> m_batches;
        /// @}

        ulong m_rendererTime = 0;
    };
    /// @}
}
