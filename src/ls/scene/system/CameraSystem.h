/// @file ls/scene/system/CameraSystem.h
/// @brief Camera System
#pragma once

#include "ls/common.h"
#include "ls/scene/SceneSystem.h"
#include "ls/scene/RenderSystem.h"
#include "ls/scene/component/CameraComponent.h"

namespace ls
{
    class CameraSystemRenderer;

    /// @addtogroup LsScene
    /// @{

    /// @brief Camera System
    class CameraSystem
        : public SceneSystem
        , public RenderSystem
    {
    public:
        /// @brief Ctor
        CameraSystem();
        /// @brief Dtor
        ~CameraSystem();

    private:
        /// @name Implement RenderSystem
        /// @{
        virtual void render(BatchRenderer& renderer, const SceneView* sceneView) override;
        virtual void resizeViewport(const uint width, const uint height) override;
        /// @}

        /// @name Implement SceneSystem
        /// @{
        virtual void addEntity(Entity& entity, Component& component) override;
        virtual void removeEntity(Entity& entity) override;
        virtual ComponentType keyComponent() const override;
        /// @}
    private:
        unique_ptr<CameraSystemRenderer> m_impl;
    };

    /// @}
}