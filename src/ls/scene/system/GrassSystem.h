/// @file ls/scene/system/GrassSystem.h
/// @brief Grass System
#pragma once

#include "ls/common.h"
#include "ls/gen/PoissonRandom.h"
#include "ls/scene/SceneSystem.h"
#include "ls/scene/RenderSystem.h"
#include "ls/scene/SceneSystem.h"
#include "ls/scene/system/ModelSystemRenderer.h"

namespace ls
{
    class WorldAsset;

    // #TODO remove
    class AssetManager;

    /// @addtogroup LsScene
    /// @{

    /// @brief Grass System
    class GrassSystem
        : public SceneSystem
    {
    public:
        /// @brief Ctor
        GrassSystem(shared_ptr<AssetManager> assetManager,
                          ModelSystemRenderer& modelRenderSystem, WorldAsset& world);
        /// @brief Dtor
        ~GrassSystem();

        /// @brief Set camera
        /// #TODO make more elegant
        void setCamera(const double3& position);
    private:
        struct ComponentCache;
        struct ChunkCache;
        struct ChunkComponentCache;

        /// @name Implement RenderSystem
        /// @{
        virtual void addEntity(Entity& entity, Component& component) override;
        virtual void removeEntity(Entity& entity) override;
        virtual ComponentType keyComponent() const override;
        virtual void syncUpdate() override;
        virtual void asyncUpdate() override;
        /// @}

        void updateChunks();
        void computeChunk(ChunkCache& chunk, ComponentCache& component, ChunkComponentCache& chunkComponent);

        struct GrassInstance
        {
            ObjectPose pose; ///< Pose in world
            float noise; ///< Grass noise

            float fadeOutBeginDistance;
            float fadeOutEndDistance;
            float fadeFactor;
            float distance2;
        };
    private:
        shared_ptr<AssetManager> m_assetManager;
        ModelSystemRenderer& m_modelRenderSystem;
        WorldAsset& m_world;

        float m_pointCloudStep = 0.05f; ///< Precomputed points step
        PointCloud2DNorm m_pointCloud; ///< Precomputed points

        /// @brief All grass components
        hamap<Entity*, unique_ptr<ComponentCache>> m_components;

        /// @brief Current camera position
        double3 m_currentPosition = std::numeric_limits<double>::infinity();
        /// @brief Last processed position
        double3 m_lastPosition = std::numeric_limits<double>::infinity();
        /// @brief Set to update chunks
        bool m_updateChunks = false;

        /// @brief Sync update step
        uint m_currentUpdatePhase = 0;

        /// @brief Stored chunks
        hamap<int2, unique_ptr<ChunkCache>> m_chunks;
    };

    /// @}
}