#include "pch.h"
#include "ls/scene/system/HeightfieldTerrainPhysicalSystem.h"

using namespace ls;
HeightfieldTerrainPhysicalSystem::HeightfieldTerrainPhysicalSystem()
{

}
HeightfieldTerrainPhysicalSystem::~HeightfieldTerrainPhysicalSystem()
{

}


// Implement SceneSystem
void HeightfieldTerrainPhysicalSystem::addEntity(Entity& entity, Component& component)
{
    throw std::logic_error("The method or operation is not implemented.");
}
void HeightfieldTerrainPhysicalSystem::removeEntity(Entity& entity)
{
    throw std::logic_error("The method or operation is not implemented.");
}
ComponentType HeightfieldTerrainPhysicalSystem::keyComponent() const
{
    return ComponentType::HeightfieldTerrain;
}