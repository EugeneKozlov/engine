#include "pch.h"
#include "ls/scene/system/StaticModelRenderTree.h"

#include "ls/core/AutoExpandingVector.h"
#include "ls/scene/Entity.h"
#include "ls/scene/component/ModelComponent.h"
#include "ls/scene/component/RenderComponent.h"
#include "ls/scene/component/TransformComponent.h"
#include "ls/scene/system/ModelSystemRenderer.h"

using namespace ls;
// Node
struct StaticModelRenderTree::Node
{
public:
    uint depth = 0;
    Node* firstChild = nullptr; ///< First child node
    Node* nextSibling = nullptr; ///< Next sibling node
    AABB aabb; ///< Bounding box

public:
    /// @brief Does the node have object?
    bool isEmpty() const
    {
        return !handle;
    }
    ModelSystemObjectHandle* handle = nullptr; ///< Internal object

public:
    ModelRenderTransformComponent ec;
};


// Main
StaticModelRenderTree::StaticModelRenderTree(const uint depth, 
                                             const EntitiesList &entities,
                                             ModelSystemRendererAllocator& allocator)
    : m_allocator(allocator)
    , m_depth(depth)
    , m_numChunks(1 << depth)

    , m_leaves(1 << depth, 1 << depth)
{
    // Prepare nodes
    vector<Node> preparedEntities;
    preparedEntities.reserve(entities.size());
    for (const auto& item : entities)
    {
        // Load components
        ModelRenderTransformComponent ec = *item.first;
        ModelComponent* modelComponent = ec.get<ModelComponent>();
        TransformComponent* transformComponent = ec.get<TransformComponent>();
        RenderComponent* renderComponent = ec.get<RenderComponent>();

        // If valid...
        if (ec.vaild())
        {
            // Create node
            Node node;
            node.ec = ec;
            const AABB modelAabb = modelComponent->asset->aabb();
            node.aabb = transformComponent->pose.transform(modelAabb);

            // Allocate internal handle
            debug_assert(!node.handle);
            node.handle = m_allocator.allocateObjectHandle();
            node.handle->mixFactor = 0.0f;
            node.handle->pose = transformComponent->pose;
            node.handle->primaryView.handle = modelComponent->handle;
            node.handle->primaryView.model = modelComponent->asset.get();
            node.handle->primaryView.lod = ModelAsset::MaxLods;
            renderComponent->handle = node.handle;

            // Store node
            preparedEntities.push_back(node);

            // Compute region AABB
            m_regionAabb += node.aabb;
        }
    }

    // Compute region and chunk size
    m_regionSize = m_regionAabb.maxExtent();
    m_chunkSize = m_regionSize / (1 << depth);

    // Count nodes
    uint numNodes = entities.size();
    for (uint idx = 0; idx <= m_depth; ++idx)
        numNodes += 1 << (2 * idx);

    // Allocate memory
    m_pool.reserve(numNodes);

    // Allocate grid nodes
    m_root = allocateNode();
    createHierarchy(*m_root, int2(0, 0));

    // Allocate entities
    vector<Node*> sortedNodes(preparedEntities.size());
    AutoExpandingVector<Node*> nodeById;
    for (uint idx = 0; idx < preparedEntities.size(); idx++)
    {
        Node* node = allocateNode(&preparedEntities[idx]);
        const EntityId id = node->ec.entity->id();
        sortedNodes[idx] = node;
        nodeById[id] = node;
    }

    // Link entities in reverse order
    for (auto iter = sortedNodes.rbegin(); iter != sortedNodes.rend(); ++iter)
    {
        Node* node = *iter;

        // Find parent
        Node* parentNode;
        const EntityId parentId = node->ec.get<RenderComponent>()->parentEntity;
        if (parentId == InvalidEntityId)
        {
            const double3 globalPosition = node->ec.get<TransformComponent>()->pose.position();
            const double2 localPosition = swiz<X, Z>(globalPosition - m_regionAabb.min);
            const int2 localChunk = static_cast<int2>(floor(localPosition / m_chunkSize));
            const int2 localChunkClamped = clamp(localChunk, int2(0), int2(m_numChunks));
            parentNode = m_leaves[localChunkClamped];
        }
        else
        {
            parentNode = nodeById[parentId];
        }
        LS_THROW(parentNode, LogicException,
                 "Entity #", node->ec.entity->id(), " parent #", parentId, "is missing");

        // Attach
        node->nextSibling = parentNode->firstChild;
        parentNode->firstChild = node;
    }

    // Update AABB
    updateAabb(*m_root);
}
AABB StaticModelRenderTree::updateAabb(Node& node)
{
    // Update children
    if (node.firstChild)
    {
        node.aabb += updateAabb(*node.firstChild);
    }
    // Compute family aabb
    AABB familyAabb = node.aabb;
    if (node.nextSibling)
    {
        familyAabb += updateAabb(*node.nextSibling);
    }
    return familyAabb;
}
StaticModelRenderTree::~StaticModelRenderTree()
{
    // Deallocate all nodes
    for (Node& node : m_pool)
    {
        if (node.handle)
        {
            m_allocator.deallocateObjectHandle(node.handle);
        }
    }
}
StaticModelRenderTree::Node* StaticModelRenderTree::allocateNode(Node* base)
{
    debug_assert(m_pool.size() < m_pool.capacity());
    m_pool.emplace_back();
    Node* node = &m_pool.back();
    if (base)
    {
        node->aabb = base->aabb;
        node->handle = base->handle;
        node->ec = base->ec;
    }
    return node;
}
void StaticModelRenderTree::createHierarchy(Node& node, const int2& localIndex)
{
    // Compute aabb
    const double gridSize = m_regionSize / (1 << node.depth);

    // List and break if leaf
    if (node.depth == m_depth)
    {
        m_leaves[localIndex] = &node;
        return;
    }

    // Allocate children
    Node* children[4];
    for (Node*& child : children)
    {
        child = allocateNode();
        child->depth = node.depth + 1;
    }

    // Update hierarchy
    static const int2 pad[4] = { int2(0, 0), int2(1, 0), int2(0, 1), int2(1, 1) };
    for (uint idx = 0; idx < 4; ++idx)
    {
        createHierarchy(*children[idx], localIndex * 2 + pad[idx]);
    }

    // Fill pointers
    node.firstChild = children[0];
    node.firstChild->nextSibling = children[1];
    node.firstChild->nextSibling->nextSibling = children[2];
    node.firstChild->nextSibling->nextSibling->nextSibling = children[3];
}


// Use
void StaticModelRenderTree::listVisible(const Frustum& frustum, 
                                        ModelRenderBatchList& renderedBatches)
{
    debug_assert(m_root);
    listVisible(*m_root, frustum, renderedBatches);
}
void StaticModelRenderTree::listVisible(Node& node, const Frustum& frustum, 
                                        ModelRenderBatchList& renderedBatches)
{
    // Process visible
    if (!node.aabb.isEmpty() && frustum.isAabbIntersect(node.aabb))
    {
        // Add non-empty
        if (!node.isEmpty())
        {
            const float distance2 = static_cast<float>(square_distance(node.aabb, frustum.points[0]));
            ModelSystemRenderer::addObjectToList(node.handle, 
                                                  distance2,
                                                  renderedBatches);
        }
        // Process child
        if (node.firstChild)
        {
            listVisible(*node.firstChild, frustum, renderedBatches);
        }
    }
    // Process sibling
    if (node.nextSibling)
    {
        listVisible(*node.nextSibling, frustum, renderedBatches);
    }
}
