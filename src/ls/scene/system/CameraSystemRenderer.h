/// @file ls/scene/system/CameraSystemRenderer.h
/// @brief Camera System Renderer
#pragma once

#include "ls/common.h"
#include "ls/core/Notification.h"
#include "ls/scene/RenderSystem.h"
#include "ls/scene/component/CameraComponent.h"

namespace ls
{
    /// @addtogroup LsScene
    /// @{

    /// @brief Camera System Add Component Notification
    struct CameraSystemAddComponentNotif
    {
        Component* id;
        CameraComponent::Param param;
    };
    /// @brief Camera System Remove Component Notification
    struct CameraSystemRemoveComponentNotif
    {
        Component* id;
    };
    /// @brief Camera System Notification
    using CameraSystemNotif = NotificationT<
        CameraSystemAddComponentNotif,
        CameraSystemRemoveComponentNotif>;
    /// @brief Camera System Renderer
    class CameraSystemRenderer
        : public RenderSystem
        , public NotificationReceiver<CameraSystemRenderer, CameraSystemNotif>
    {
    public:
        /// @brief Ctor
        CameraSystemRenderer();
        /// @brief Dtor
        ~CameraSystemRenderer();

        /// @name Renderer
        /// @{
        virtual void render(BatchRenderer& renderer, const SceneView* sceneView) override;
        virtual void resizeViewport(const uint width, const uint height) override;
        /// @}

        /// @name Notifications
        /// @{
        void processNotification(CameraSystemAddComponentNotif& notif);
        void processNotification(CameraSystemRemoveComponentNotif& notif);
        /// @}
    private:
        /// @brief Camera Component implementation
        struct CameraComponentDetail
        {
            Component* id = nullptr; ///< Component in main thread, do not access
            CameraComponent::Param param; ///< Component parameters
        };

        hamap<Component*, CameraComponentDetail> m_components;
    };

    /// @}
}