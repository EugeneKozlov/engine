/// @file ls/scene/system/HeightfieldTerrainPhysicalSystem.h
/// @brief HeightfieldTerrainPhysicalSystem
#pragma once

#include "ls/common.h"
#include "ls/scene/SceneSystem.h"

namespace ls
{
    /// @addtogroup LsScene
    /// @{

    /// @brief HeightfieldTerrainPhysicalSystem
    class HeightfieldTerrainPhysicalSystem
        : public SceneSystem
    {
    public:
        /// @brief Ctor
        HeightfieldTerrainPhysicalSystem();
        /// @brief Dtor
        ~HeightfieldTerrainPhysicalSystem();

    public:
        /// @name Implement SceneSystem
        /// @{
        virtual void addEntity(Entity& entity, Component& component) override;
        virtual void removeEntity(Entity& entity) override;
        virtual ComponentType keyComponent() const override;
        /// @}
    private:

    };

    /// @}
}