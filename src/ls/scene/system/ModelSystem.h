/// @file ls/scene/system/ModelSystem.h
/// @brief Model System
#pragma once

#include "ls/common.h"
#include "ls/core/AutoExpandingVector.h"
#include "ls/scene/SceneSystem.h"
#include "ls/scene/RenderSystem.h"

namespace ls
{
    class AssetManager;
    class ModelSystemRenderer;
    class ModelSystemRendererAllocator;
    class Renderer;
    class StaticModelRenderTree;

    class ModelComponent;
    class RenderComponent;
    class TransformComponent;

    /// @addtogroup LsScene
    /// @{

    /// @brief Model System
    /// 
    /// Provides model loading and lod switch
    class ModelSystem
        : public SceneSystem
        , public RenderSystem
    {
    public:
        /// @brief Ctor
        /// @param baseFovY
        ///   Vertical field of view is used to compute LOD distances
        ModelSystem(shared_ptr<AssetManager> assetManager,
                    Renderer& renderer, const float baseFovY);
        /// @brief Dtor
        ~ModelSystem();
        /// @brief Set camera
        /// #TODO make more elegant
        void setCamera(const double3& position);
        /// @brief Get renderer
        /// #TODO remove this grass hack
        ModelSystemRenderer& modelSystemRenderer()
        {
            return *m_impl;
        }
    private:
        /// @name Implement RenderSystem
        /// @{
        virtual void render(BatchRenderer& renderer, const SceneView* sceneView) override;
        /// @}

        /// @name Implement Scene System
        /// @{
        virtual void syncUpdate() override;
        virtual void asyncUpdate() override;
        virtual SceneSystemCache prepareStaticEntities(const EntitiesList& entities, WorldRegion& region) override;
        virtual void addStaticEntities(const EntitiesList& entities,
                                       WorldRegion& region,
                                       SceneSystemCache preparedData) override;
        virtual void removeStaticEntities(const int2& regionIndex) override;
        virtual void addEntity(Entity& entity, Component& component) override;
        virtual void removeEntity(Entity& entity) override;
        virtual ComponentType keyComponent() const override;
        /// @}
    private:
        struct ModelSystemCache;
        struct Node
        {
            TransformComponent* transformComponent = nullptr; ///< Provides position
            ModelComponent* modelComponent = nullptr; ///< Store invariants
            RenderComponent* renderComponent = nullptr; ///< Store per-object data
            /// @brief Is valid?
            bool isValid() const
            {
                return transformComponent && modelComponent && renderComponent;
            }
        };
        /// @brief Read components
        static Node readComponents(Entity& entity, Component& modelComponent);
        /// @brief Load asset and compute distances
        void initializeModelComponent(ModelComponent& modelComponent);
        /// @brief Compute switch distance
        static float computeSwitchDistance(const float switchSize, const float modelSize, const float fovY);
        /// @brief Compute LOD
        /// @note Requests optimal LOD and returns nearest to optimal
        /// @note Initializes ModelComponent batches if needed
        uint computeBestLod(ModelComponent& modelComponent, const uint currentLod, const float dist2);
        /// @brief Update node LOD
        void updateNodeLod(const Node& node, const float deltaTime);
    private:
        shared_ptr<AssetManager> m_assetManager;
        Renderer& m_renderer; ///< Renderer to allocate models
        const float m_baseFovY; ///< Vertical field of view

        unique_ptr<ModelSystemRenderer> m_impl;

        hamap<int2, shared_ptr<StaticModelRenderTree>> m_staticTrees;
        hamap<int2, vector<Node>> m_staticEntities;
        AutoExpandingVector<Node> m_dynamicEntities;

        double3 m_cameraPosition;
    };

    /// @}
}