/// @file ls/scene/system/ModelRenderObject.h
/// @brief Model Render Object
#pragma once

#include "ls/common.h"
#include "ls/asset/ModelAsset.h"
#include "ls/scene/Entity.h"
#include "ls/scene/core/ObjectPose.h"

namespace ls
{
    struct ModelSystemModelHandle;

    class TransformComponent;
    class RenderComponent;
    class ModelComponent;

    /// @addtogroup LsScene
    /// @{

    /// @brief Model System Geometry Description
    struct ModelSystemGeometryDesc
    {
        ModelSystemModelHandle* handle = nullptr; ///< Internal model handle
        ModelAsset* model = nullptr; ///< Model to draw
        uint lod = ModelAsset::MaxLods; ///< LoD to draw
    };
    /// @brief Model System Object Handle
    struct ModelSystemObjectHandle
    {
        ModelSystemGeometryDesc primaryView; ///< Primary model view
        ModelSystemGeometryDesc secondaryView; ///< Secondary model view
        float mixFactor = 0.0f; ///< 0 means @a primary view, 1 means @a secondary one
        ObjectPose pose; ///< Base object pose
    };
    /// @brief List of model batches
    using ModelRenderBatchList = vector<ModelSystemGeometryDesc>;
    /// @brief Container of transform, model and render components
    using ModelRenderTransformComponent = EntityComponentTuple<TransformComponent, ModelComponent, RenderComponent>;
    /// @}
}