#include "pch.h"
#include "ls/scene/system/CameraSystemRenderer.h"

using namespace ls;
CameraSystemRenderer::CameraSystemRenderer()
{

}
CameraSystemRenderer::~CameraSystemRenderer()
{

}


// Notifications
void CameraSystemRenderer::processNotification(CameraSystemAddComponentNotif& notif)
{
    // Add new component
    debug_assert(m_components.count(notif.id) == 0);
    CameraComponentDetail& component = m_components[notif.id];
    component.id = notif.id;
    component.param = notif.param;
}
void CameraSystemRenderer::processNotification(CameraSystemRemoveComponentNotif& notif)
{
    // Remove component
    m_components.erase(notif.id);
}


// Render
void CameraSystemRenderer::render(BatchRenderer& renderer, const SceneView* sceneView)
{
    debug_assert(!sceneView);
}
void CameraSystemRenderer::resizeViewport(const uint width, const uint height)
{

}