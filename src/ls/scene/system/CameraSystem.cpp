#include "pch.h"
#include "ls/scene/system/CameraSystem.h"

#include "ls/scene/Entity.h"
#include "ls/scene/system/CameraSystemRenderer.h"

using namespace ls;
CameraSystem::CameraSystem()
    : m_impl(make_unique<CameraSystemRenderer>())
{

}
CameraSystem::~CameraSystem()
{

}


// Render
void CameraSystem::render(BatchRenderer& renderer, const SceneView* sceneView)
{
    m_impl->processNotifications();
    m_impl->render(renderer, sceneView);
}
void CameraSystem::resizeViewport(const uint width, const uint height)
{
    m_impl->processNotifications();
    m_impl->resizeViewport(width, height);
}


// Implement scene system
void CameraSystem::addEntity(Entity& entity, Component& component)
{
    // Get component
    CameraComponent& cameraComponent = *component.cast<CameraComponent>();

    // Send notification
    CameraSystemAddComponentNotif notif;
    notif.id = &cameraComponent;
    notif.param = cameraComponent.param;
    m_impl->sendNotification(notif);
}
void CameraSystem::removeEntity(Entity& entity)
{
    // Get component
    CameraComponent& cameraComponent = *entity.findComponent<CameraComponent>();

    // Send notification
    CameraSystemRemoveComponentNotif notif;
    notif.id = &cameraComponent;
    m_impl->sendNotification(notif);
}
ComponentType CameraSystem::keyComponent() const
{
    return ComponentType::Camera;
}
