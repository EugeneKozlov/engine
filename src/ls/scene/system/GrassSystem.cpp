#include "pch.h"
#include "ls/scene/system/GrassSystem.h"

#include "ls/asset/AssetManager.h"
#include "ls/asset/ModelAsset.h"
#include "ls/core/GlobalVariableT.h"
#include "ls/core/UniformRandom.h"
#include "ls/scene/Entity.h"
#include "ls/scene/component/GrassComponent.h"
#include "ls/scene/component/HeightfieldTerrainComponent.h"
#include "ls/scene/core/ObjectPose.h"
#include "ls/render/SceneView.h"
#include "ls/asset/WorldAsset.h"

using namespace ls;
// Config
GlobalVariableT<float> g_grassUpdateStep(
    "render:grassUpdateStep", "",
    GlobalVariableFlag::Modifiable,
    "Grass is updated every [value] meters\n"
    "Must be in range [0, Inf)\n",
    5.0f, [](const float value) { return value >= 0; });

GlobalVariableT<float> g_grassRenderDistance(
    "render:grassRenderDistance", "rrGrassDistance",
    GlobalVariableFlag::Modifiable | GlobalVariableFlag::Console,
    "Grass is rendered in [value] meters around viewer\n"
    "Must be in range [0, Inf)\n",
    48.0f, [](const float value) { return value >= 0; });

GlobalVariableT<float> g_grassFadeBeginDistance(
    "render:grassFadeBeginDistance", "",
    GlobalVariableFlag::Modifiable,
    "Grass is randomly fading out starting from [value] meters\n"
    "Must be in range [0, Inf)\n",
    10.0f, [](const float value) { return value >= 0; });

GlobalVariableT<float> g_grassFadeRange(
    "render:grassFadeBeginDistance", "",
    GlobalVariableFlag::Modifiable,
    "Grass fade is [value] meters starting from fade distance\n"
    "Must be in range [0, Inf)\n",
    5.0f, [](const float value) { return value >= 0; });

GlobalVariableT<float> g_grassFadePower(
    "render:grassFadePower", "",
    GlobalVariableFlag::ReadOnly,
    "Grass is faded out using [value] power\n"
    "1 is linear fade; 2 is square density fall\n"
    "Must be in range (0, Inf)\n",
    2.0f, [](const float value) { return value > 0; });

GlobalVariableT<float> g_grassDensity(
    "render:grassDensity", "rrGrassDensity",
    GlobalVariableFlag::Modifiable | GlobalVariableFlag::Console,
    "Grass density modifier, only [value]*100% of grass is rendered\n"
    "Must be in range [0, 1]\n",
    0.5f, [](const float value) { return value >= 0 && value <= 1; });

GlobalVariableT<uint> g_grassNumUpdatePhases(
    "render:grassNumUpdatePhases", "",
    GlobalVariableFlag::ReadOnly,
    "Grass is updated during [value] frames\n"
    "Smaller is better, higher is faster\n"
    "Must be in range [1, 8]\n",
    3, [](const uint value) { return value >= 1 && value <= 8; });

// Main
GrassSystem::GrassSystem(shared_ptr<AssetManager> assetManager,
                                     ModelSystemRenderer& modelRenderSystem, WorldAsset& world)
    : m_assetManager(assetManager)
    , m_modelRenderSystem(modelRenderSystem)
    , m_world(world)
{
    // Precompute point cloud
    // #TODO Customize parameters
    m_pointCloud = PoissonRandom(42).generate(m_pointCloudStep, 30, 10000);
}
GrassSystem::~GrassSystem()
{

}
void GrassSystem::setCamera(const double3& position)
{
    m_currentPosition = position;
    if (distance(position, m_lastPosition) > g_grassUpdateStep.value())
    {
        m_lastPosition = position;
        m_updateChunks = true;
    }
}


// Internal data containers
struct GrassSystem::ComponentCache
{
    GrassComponent* grassComponent = nullptr;
    HeightfieldTerrainComponent* heightFieldTerrainComponent = nullptr;
};
struct GrassSystem::ChunkComponentCache : Noncopyable
{
    ModelSystemInstanceCloudSPtr instanceCloud;
    vector<GrassInstance> grassInstances;

    ModelSystemRenderer* modelSystemRenderer;
    /// @brief Dtor to send 'remove' notification
    ~ChunkComponentCache()
    {
        if (modelSystemRenderer)
        {
            ModelSystemRemoveCloudNotification notif;
            notif.cloud = instanceCloud;
            modelSystemRenderer->sendNotification(notif);
        }
    }
};
struct GrassSystem::ChunkCache
{
    WorldRegion::Chunk* worldChunk = nullptr;
    hamap<Entity*, ChunkComponentCache> componentCaches;
};


// Implement RenderSystem
void GrassSystem::addEntity(Entity& entity, Component& component)
{
    // Get components
    ComponentCache componentCache;
    componentCache.grassComponent = component.cast<GrassComponent>();
    componentCache.heightFieldTerrainComponent = entity.findComponent<HeightfieldTerrainComponent>();

    // Load model
    componentCache.grassComponent->asset
        = Asset::cast<ModelAsset>(m_assetManager->loadAsset(AssetType::Model, componentCache.grassComponent->assetName.str()));

    // #HACK Make more elegant
    componentCache.grassComponent->asset->loadLevel(0);

    // Details over meshes are not supported yet
    if (!componentCache.heightFieldTerrainComponent)
    {
        logError("Details over meshes are not supported yet; "
                 "entity #", entity.id(), " should have "
                 "height-field terrain component");
        return;
    }

    // Compute chunks
    for (auto& item : m_chunks)
    {
        ChunkCache& chunkCache = *item.second;
        ChunkComponentCache& chunkComponentCache
            = chunkCache.componentCaches[&entity];
        computeChunk(chunkCache, componentCache, chunkComponentCache);
    }

    // Save component
    m_components.emplace(&entity, make_unique<ComponentCache>(componentCache));
}
void GrassSystem::removeEntity(Entity& entity)
{
    // Remove cached data
    for (auto& item : m_chunks)
    {
        ChunkCache& chunkCache = *item.second;
        chunkCache.componentCaches.erase(&entity);
    }

    // Remove component
    m_components.erase(&entity);
}
ComponentType GrassSystem::keyComponent() const
{
    return ComponentType::Grass;
}
void GrassSystem::syncUpdate()
{
    const uint numPhases = g_grassNumUpdatePhases.value();

    // Flush fade
    for (auto& iterChunk : m_chunks)
    {
        for (auto& iterChunkComponent : iterChunk.second->componentCaches)
        {
            ChunkComponentCache& chunkComponent = iterChunkComponent.second;
            const uint numInstances = chunkComponent.grassInstances.size();

            const uint portionSize = numInstances / numPhases;
            const bool isLast = m_currentUpdatePhase + 1 == numPhases;
            const uint fromInstance = portionSize * m_currentUpdatePhase;
            const uint toInstance = isLast ? numInstances : (fromInstance + portionSize);

            for (uint idx = fromInstance; idx < toInstance; ++idx)
            {
                GrassInstance& grassInstance = chunkComponent.grassInstances[idx];
                ModelSystemDrawable& drawable = chunkComponent.instanceCloud->instances[idx];
                drawable.fadeFactor.y = grassInstance.fadeFactor;
                drawable.distance2 = grassInstance.distance2;
            }
        }
    }

    // Next step
    m_currentUpdatePhase = (m_currentUpdatePhase + 1) % numPhases;
}
void GrassSystem::asyncUpdate()
{
    // Update chunks
    if (m_updateChunks)
    {
        m_updateChunks = false;
        updateChunks();
    }

    // Update fade
    for (auto& iterChunk : m_chunks)
    {
        for (auto& chunkComponent : iterChunk.second->componentCaches)
        {
            for (GrassInstance& grassInstance : chunkComponent.second.grassInstances)
            {
                const float dist = static_cast<float>(distance(grassInstance.pose.position(), m_currentPosition));
                grassInstance.fadeFactor = 1.0f - unlerp(grassInstance.fadeOutBeginDistance,
                                                         grassInstance.fadeOutEndDistance,
                                                         dist);
                grassInstance.distance2 = dist * dist;
            }
        }
    }
}
void GrassSystem::updateChunks()
{
    // Do nothing is position is invalid
    if (!isFinite(m_lastPosition))
    {
        return;
    }

    // Should update chunks in radius [max+epsilon]
    const float maxDistance = g_grassRenderDistance.value() + g_grassUpdateStep.value();

    // Compute bounding chunks
    const float chunkSize = static_cast<float>(m_world.config.chunkWidth);
    const double2 beginPosition = swiz<X, Z>(m_lastPosition) - maxDistance;
    const double2 endPosition = swiz<X, Z>(m_lastPosition) + maxDistance;
    const int2 beginChunk = static_cast<int2>(floor(beginPosition / chunkSize));
    const int2 endChunk = static_cast<int2>(floor(endPosition / chunkSize));

    // Drop chunks outside
    erase_if(m_chunks,
             [beginChunk, endChunk](const pair<const int2, unique_ptr<ChunkCache>>& item)
    {
        return !insideInc(item.first, beginChunk, endChunk);
    });

    // Iterate over chunks in region
    int2 chunkIndex;
    for (chunkIndex.x = beginChunk.x; chunkIndex.x <= endChunk.x; ++chunkIndex.x)
    {
        for (chunkIndex.y = beginChunk.y; chunkIndex.y <= endChunk.y; ++chunkIndex.y)
        {
            // Skip already computed chunks
            if (m_chunks.count(chunkIndex) != 0)
            {
                continue;
            }

            // Find chunk data
            WorldRegion::Chunk* worldChunk = m_world.findChunk(chunkIndex);

            // Wait if chunk is not loaded yet
            if (!worldChunk)
            {
                m_updateChunks = true;
                continue;
            }

            // Allocate new chunk in cache
            m_chunks[chunkIndex] = make_unique<ChunkCache>();
            ChunkCache& chunkCache = *m_chunks[chunkIndex];
            chunkCache.worldChunk = worldChunk;

            // Compute if not exist for each component
            for (auto& item : m_components)
            {
                ChunkComponentCache& chunkComponentCache = chunkCache.componentCaches[item.first];
                computeChunk(chunkCache, *item.second, chunkComponentCache);
            }
        }
    }
}
void GrassSystem::computeChunk(ChunkCache& chunk, ComponentCache& component, 
                                     ChunkComponentCache& chunkComponent)
{
    // Skip if zero density
    const float densityFactor = g_grassDensity.value();
    if (densityFactor <= epsilon())
    {
        return;
    }

    // Construct instance cloud
    ModelSystemInstanceCloudSPtr instanceCloud = make_shared<ModelSystemInstanceCloud>();

    // Load model
    shared_ptr<ModelAsset> model = component.grassComponent->asset;
    const AABB modelAabb = model->aabb();
    instanceCloud->modelHandle = m_modelRenderSystem.allocator.allocateModelHandle(model);

    // Update AABB
    instanceCloud->aabb = chunk.worldChunk->aabb();
    instanceCloud->aabb.min -= modelAabb.extents();
    instanceCloud->aabb.max += modelAabb.extents();

    // Generate random points for instances
    const int2 chunkIndex = chunk.worldChunk->index;
    const double2 chunkBegin = swiz<X, Z>(instanceCloud->aabb.min);
    const double2 chunkEnd = swiz<X, Z>(instanceCloud->aabb.max);

    const float scale = (component.grassComponent->minDistance / densityFactor) / m_pointCloudStep;
    const PointCloud2D points = samplePointCloud(m_pointCloud, chunkBegin, chunkEnd, scale);

    // Fill grass instances
    const uint seed = static_cast<uint>(chunkIndex.x ^ chunkIndex.y * 3);
    UniformRandom uniformRandom(seed);
    for (auto& point : points)
    {
        // Compute parameters
        const double2 localPosition = point - chunkBegin;
        const float terrainHeight = chunk.worldChunk->height(static_cast<float2>(localPosition));

        // Create instance
        GrassInstance instance;
        instance.pose = ObjectPose(double3(point.x, terrainHeight, point.y),
                                   quat(float3(0, 1, 0), uniformRandom.randomReal_01() * pi * 2));
        instance.noise = std::pow(uniformRandom.randomReal_01(), g_grassFadePower.value());
        instance.fadeOutEndDistance = lerp(g_grassFadeBeginDistance.value(), g_grassRenderDistance.value(), instance.noise);
        instance.fadeOutBeginDistance = std::max(0.0f, instance.fadeOutEndDistance - g_grassFadeRange.value());
        instance.fadeFactor = 0.0f;
        instance.distance2 = 0.0f;

        chunkComponent.grassInstances.push_back(instance);
    }

    // Fill instances in cloud
    for (const GrassInstance& instance : chunkComponent.grassInstances)
    {
        ModelSystemDrawable drawable;
        drawable.distance2 = instance.distance2;
        drawable.fadeFactor = float2(1.0f, instance.fadeFactor);
        drawable.pose = instance.pose;
        instanceCloud->instances.push_back(drawable);
    }

    // Send notification
    ModelSystemAddCloudNotification notif;
    notif.cloud = instanceCloud;
    m_modelRenderSystem.sendNotification(notif);

    // Shall send remove notification
    chunkComponent.instanceCloud = instanceCloud;
    chunkComponent.modelSystemRenderer = &m_modelRenderSystem;
}
