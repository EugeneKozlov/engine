/// @file ls/scene/system/StaticModelRenderTree.h
/// @brief Static Model Render Tree
#pragma once

#include "ls/common.h"
#include "ls/core/FlatArray.h"
#include "ls/scene/SceneSystem.h"
#include "ls/scene/system/ModelSystemObjectHandle.h"

namespace ls
{
    class Entity;
    class ModelSystemRendererAllocator;

    class TransformComponent;
    class RenderComponent;
    class ModelComponent;

    /// @addtogroup LsScene
    /// @{

    /// @brief Static Model Render Tree
    class StaticModelRenderTree
        : Noncopyable
    {
    public:
        /// @brief Ctor
        /// @param entities
        ///   List of entities and Model components
        StaticModelRenderTree(const uint depth, 
                              const EntitiesList &entities,
                              ModelSystemRendererAllocator& allocator);
        /// @brief Dtor
        ~StaticModelRenderTree();
        /// @brief List visible non-empty nodes
        void listVisible(const Frustum& frustum, ModelRenderBatchList& renderedBatches);
    private:
        struct Node;

        /// @brief Allocate node (guaranteed)
        Node* allocateNode(Node* base = nullptr);
        /// @brief Allocate node hierarchy
        void createHierarchy(Node& node, const int2& localIndex);
        /// @brief List visible non-empty nodes
        void listVisible(Node& node, const Frustum& frustum, ModelRenderBatchList& renderedBatches);
        /// @brief Update AABB
        static AABB updateAabb(Node& node);
    private:
        ModelSystemRendererAllocator& m_allocator;
        const uint m_depth;
        const sint m_numChunks; ///< RegionSize / ChunkSize

        AABB m_regionAabb;
        double m_regionSize;
        double m_chunkSize;

        vector<Node> m_pool; ///< Memory for nodes
        FlatArray<Node*> m_leaves; ///< Flat array of leaf nodes
        Node* m_root = nullptr; ///< Root node
    };

    /// @}
}