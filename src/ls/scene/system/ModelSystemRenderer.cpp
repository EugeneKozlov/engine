#include "pch.h"
#include "ls/asset/AssetManager.h"
#include "ls/gapi/Renderer.h"
#include "ls/render/BatchRenderer.h"
#include "ls/render/SceneView.h"
#include "ls/scene/component/ModelComponent.h"
#include "ls/scene/component/RenderComponent.h"
#include "ls/scene/component/TransformComponent.h"
#include "ls/scene/system/ModelSystemRenderer.h"
#include "ls/scene/system/StaticModelRenderTree.h"
#include "ls/scene/system/ModelSystemObjectHandle.h"
#include "ls/asset/WorldAsset.h"

using namespace ls;

// Handles
struct ls::ModelSystemModelHandle
{
    shared_ptr<ModelAsset> modelAsset; ///< Model Asset

    bool m_renderListSorted = false; ///< Is render list sorted?
    array<vector<ModelSystemDrawable>, ModelAsset::MaxLods> m_renderList; ///< All render lists
public:
    /// @name Render list
    /// @{

    /// @brief Is render list empty?
    bool isRenderListEmpty(const uint lod) const
    {
        return m_renderList[lod].empty();
    }
    /// @brief Is render list sorted?
    bool isRenderListSorted() const
    {
        return m_renderListSorted;
    }
    /// @brief Sort render list
    void sortRenderList()
    {
        for (vector<ModelSystemDrawable>& renderList : m_renderList)
        {
            std::sort(renderList.begin(), renderList.end(),
                      [](const ModelSystemDrawable& lhs, const ModelSystemDrawable& rhs)
            {
                return lhs.distance2 < rhs.distance2;
            });
        }
        m_renderListSorted = true;
    }
    /// @brief Reset lists
    void clearRenderLists()
    {
        for (vector<ModelSystemDrawable>& renderList : m_renderList)
        {
            renderList.clear();
        }
        m_renderListSorted = true;
    }
    /// @brief Get render list
    const vector<ModelSystemDrawable>& constRenderList(const uint lod) const
    {
        debug_assert(lod < ModelAsset::MaxLods);
        return m_renderList[lod];
    }
    /// @brief Add drawable to list
    void addDrawable(const uint lod, const ModelSystemDrawable& handle)
    {
        m_renderList[lod].push_back(handle);
        m_renderListSorted = false;
    }
    /// @}
};


// Allocator
ModelSystemModelHandle* ModelSystemRendererAllocator::allocateModelHandle(shared_ptr<ModelAsset> modelAsset)
{
    // Try to find existing handle
    ModelSystemModelHandle* modelHandle = getOrDefault(m_modelHandles, modelAsset);
    if (modelHandle)
    {
        return modelHandle;
    }

    // Create new handle
    modelHandle = m_modelPool.construct();
    modelHandle->modelAsset = modelAsset;
    m_modelHandles.emplace(modelAsset, modelHandle);
    return modelHandle;
}
ModelSystemObjectHandle* ModelSystemRendererAllocator::allocateObjectHandle()
{
    return m_objectPool.construct();
}
void ModelSystemRendererAllocator::deallocateObjectHandle(ModelSystemObjectHandle* handle)
{
    debug_assert(handle);
    m_objectPool.destroy(handle);
}
ModelSystemRendererAllocator::ModelSystemRendererAllocator()
{
}
ModelSystemRendererAllocator::~ModelSystemRendererAllocator()
{
}


// Main
ModelSystemRenderer::ModelSystemRenderer(shared_ptr<AssetManager> assetManager)
    : m_assetManager(assetManager)
{
    debug_assert(!!m_assetManager);
    m_renderer = m_assetManager->renderer();
    debug_assert(!!m_renderer);

    m_handleGlobalUniformBuffer = m_renderer
        ->createUniformBuffer(sizeof(MaterialAsset::GlobalUniformBuffer));
    m_handleModelUniformBuffer = m_renderer
        ->createUniformBuffer(sizeof(MaterialAsset::ModelUniformBuffer));
}
ModelSystemRenderer::~ModelSystemRenderer()
{
    m_renderer->destroyBuffer(m_handleGlobalUniformBuffer);
    m_renderer->destroyBuffer(m_handleModelUniformBuffer);
}


// Synchronous update
void ModelSystemRenderer::syncUpdate(const float /*deltaTime*/, const DeltaTime deltaTimeMs)
{
    m_rendererTime += deltaTimeMs;
}


// Notifications
void ModelSystemRenderer::processNotification(ModelSystemAddStaticTreeNotif& notif)
{
    m_staticTrees.emplace(notif.tree);
}
void ModelSystemRenderer::processNotification(ModelSystemRemoveStaticTreeNotif& notif)
{
    m_staticTrees.erase(notif.tree);
}
void ModelSystemRenderer::processNotification(ModelSystemChangeModelNotif& notif)
{
    notif.handle->primaryView.handle = notif.primaryModel;
    notif.handle->primaryView.model = notif.primaryModel->modelAsset.get();
    notif.handle->primaryView.lod = notif.primaryLod;
    notif.handle->secondaryView.handle = notif.secondaryModel;
    notif.handle->secondaryView.model = notif.secondaryModel->modelAsset.get();
    notif.handle->secondaryView.lod = notif.secondaryLod;
    notif.handle->mixFactor = notif.mixFactor;
}
void ModelSystemRenderer::processNotification(ModelSystemAddCloudNotification& notif)
{
    debug_assert(m_instanceClouds.count(notif.cloud) == 0);
    m_instanceClouds.insert(notif.cloud);
}
void ModelSystemRenderer::processNotification(ModelSystemRemoveCloudNotification& notif)
{
    const uint numErased = m_instanceClouds.erase(notif.cloud);
    debug_assert(numErased == 1);
}


// Render lists
void ModelSystemRenderer::addDrawableToList(const ModelSystemGeometryDesc& geometry,
                                            const ModelSystemDrawable& drawable,
                                            ModelRenderBatchList& batches)
{
    // Add new batch
    if (geometry.handle->isRenderListEmpty(geometry.lod))
    {
        batches.push_back(geometry);
    }

    // Add drawable
    geometry.handle->addDrawable(geometry.lod, drawable);
}
void ModelSystemRenderer::addObjectToList(ModelSystemObjectHandle* object,
                                          const float distance2,
                                          ModelRenderBatchList& batches)
{
    ModelSystemGeometryDesc& primary = object->primaryView;
    if (primary.handle && primary.lod != ModelAsset::MaxLods)
    {
        ModelSystemDrawable drawable;
        drawable.pose = object->pose;
        drawable.distance2 = distance2;
        drawable.fadeFactor.x = 1.0f - object->mixFactor;
        drawable.fadeFactor.y = 1.0f;
        addDrawableToList(primary, drawable, batches);
    }
    ModelSystemGeometryDesc& secondary = object->secondaryView;
    if (secondary.handle && secondary.lod != ModelAsset::MaxLods && object->mixFactor != 0.0f)
    {
        ModelSystemDrawable drawable;
        drawable.pose = object->pose;
        drawable.distance2 = distance2;
        drawable.fadeFactor.x = 2.0f - object->mixFactor;
        drawable.fadeFactor.y = 1.0f;
        addDrawableToList(secondary, drawable, batches);
    }
}


// Implement Render System
void ModelSystemRenderer::render(BatchRenderer& batchRenderer, const SceneView* sceneView)
{
    debug_assert(sceneView);

    clearBuffers();
    buildStaticTreeBatches(*sceneView);
    buildInstanceCloudBatches(*sceneView);
    compileModelBatches(*sceneView);

    // Sort batches
    std::sort(m_batches.begin(), m_batches.end(),
              [](const Batch& lhs, const Batch& rhs)
    {
        if (lhs.shaderProgram.pointer() != rhs.shaderProgram.pointer())
            return lhs.shaderProgram.pointer() < rhs.shaderProgram.pointer();
        if (lhs.mesh != rhs.mesh)
            return lhs.mesh < rhs.mesh;
        return lhs.material < rhs.material;
    });

    // Commit constants
    commitConstants(*sceneView, batchRenderer);

    // Draw batches
    drawBatches(m_batches, *sceneView, batchRenderer);
}
void ModelSystemRenderer::clearBuffers()
{
    // Clear model batches
    for (const ModelSystemGeometryDesc& modelBatch : m_modelBatches)
    {
        modelBatch.handle->clearRenderLists();
    }
    m_modelBatches.clear();

    // Clear batches
    m_batches.clear();
}
void ModelSystemRenderer::commitConstants(const SceneView& sceneView,
                                          BatchRenderer& batchRenderer) const
{
    MaterialAsset::GlobalUniformBuffer globalUniformBuffer;

    globalUniformBuffer.mProjection = sceneView.camera.projMatrix();
    globalUniformBuffer.mView = sceneView.camera.viewMatrix();
    globalUniformBuffer.vCameraPosition = float4(sceneView.eyePosition, 1.0f);
    // #TODO period
    globalUniformBuffer.vTime.x = m_rendererTime % 1000000 * 0.001f;
    globalUniformBuffer.vTime.y = m_rendererTime % 1000 * 0.001f;
    globalUniformBuffer.vTime.z = static_cast<float>(m_rendererTime % 1000000);
    globalUniformBuffer.vTime.w = static_cast<float>(m_rendererTime % 1000);
    // #TODO use ambient
    globalUniformBuffer.vAmbientColor = 1.0f;
    globalUniformBuffer.vAmbientFadeIntensity = float2(0.1f, 0.5f);
    globalUniformBuffer.vAmbientFadeDistance = 50.0f;

    // Commit
    batchRenderer.commitBuffer(m_handleGlobalUniformBuffer, globalUniformBuffer);
}
void ModelSystemRenderer::buildStaticTreeBatches(const SceneView& sceneView)
{
    // Find visible models of static trees
    for (auto& staticTree : m_staticTrees)
    {
        staticTree->listVisible(sceneView.frustum, m_modelBatches);
    }
}
void ModelSystemRenderer::buildInstanceCloudBatches(const SceneView& sceneView)
{
    for (auto& instanceCloud : m_instanceClouds)
    {
        // Skip if depth-only and not needed
        if (sceneView.shaderMode == MaterialAsset::ShaderMode::Depth && !instanceCloud->drawIfDepthOnly)
        {
            continue;
        }

        // Skip invisible
        if (!sceneView.frustum.isAabbIntersect(instanceCloud->aabb))
        {
            continue;
        }

        // Make geometry description
        ModelSystemGeometryDesc geometryDesc;
        geometryDesc.handle = instanceCloud->modelHandle;
        geometryDesc.model = instanceCloud->modelHandle->modelAsset.get();
        geometryDesc.lod = 0;

        // Add each instance to render list
        for (ModelSystemDrawable& instance : instanceCloud->instances)
        {
            // Skip invisible
            if (instance.fadeFactor.y == 0)
            {
                continue;
            }

            // Add visible drawable to list
            ModelSystemRenderer::addDrawableToList(geometryDesc, instance, m_modelBatches);
        }
    }
}
void ModelSystemRenderer::compileModelBatches(const SceneView& sceneView)
{
    // Build batches for each rendered model
    for (const ModelSystemGeometryDesc& modelBatch : m_modelBatches)
    {
        buildBatchByModel(modelBatch, m_batches, sceneView);
    }
    // Render lists are already sorted
}
void ModelSystemRenderer::buildBatchByModel(const ModelSystemGeometryDesc& modelBatch,
                                            vector<Batch>& batches, 
                                            const SceneView& sceneView) const
{
    Batch batch;

    // Pick mesh for LOD
    ModelAsset& model = *modelBatch.model;
    MeshAsset* mesh = model.mesh(modelBatch.lod);
    if (!mesh)
    {
        logWarning("Mesh is not ready: "
                    "Model=", modelBatch.model, " "
                    "Lod=", modelBatch.lod);
        return;
    }
    batch.mesh = mesh;
    batch.geometry = mesh->gapiGeometry();

    // For each subset pick material
    for (uint subsetIndex = 0; subsetIndex < mesh->numSubsets(); ++subsetIndex)
    {
        // Pick material
        const MeshAsset::Subset& subset = mesh->subset(subsetIndex);
        MaterialAsset* material = model.material(modelBatch.lod, subset.materialIndex);
        if (!material)
        {
            logWarning("Material is not loaded for "
                        "Model=", modelBatch.model, " "
                        "Lod=", modelBatch.lod, " ",
                        "Subset=", subsetIndex, " ",
                        "Material=", subset.materialIndex, " ");
            continue;
        }

        // Attach buffers
        material->attachUniformBuffer(MaterialAsset::UniformBuffer::Global,
                                      m_handleGlobalUniformBuffer,
                                      MaterialAsset::GlobalUniformBuffer::name());
        material->attachUniformBuffer(MaterialAsset::UniformBuffer::Model,
                                      m_handleModelUniformBuffer,
                                      MaterialAsset::ModelUniformBuffer::name());

        // Initialize batch material
        batch.material = material;
        batch.state = material->gapiState(sceneView.materialCacheContext);

        // Pick effect program
        ProgramHandle program = material->gapiProgram(sceneView.shaderMode);
        if (!program)
        {
            logWarning("Program is not loaded for "
                        "Model=", modelBatch.model, " "
                        "Lod=", modelBatch.lod, " ",
                        "Subset=", subsetIndex, " ",
                        "Material=", subset.materialIndex, " ");
            continue;
        }
        batch.shaderProgram = program;

        // Pick resources
        batch.resources = material->gapiResources(sceneView.shaderMode);

        // Get draw info
        batch.baseVertex = 0;
        batch.startIndex = subset.offset;
        batch.numIndices = subset.numIndices;

        // Pick and sort entities
        modelBatch.handle->sortRenderList();
        batch.entities = modelBatch.handle->constRenderList(modelBatch.lod);

        // Add batch
        debug_assert(batch.entities.size() > 0);
        batches.push_back(batch);
    }
}
void ModelSystemRenderer::drawBatches(const vector<Batch>& batches, const SceneView& sceneView,
                                      BatchRenderer& batchRenderer)
{
    for (Batch& batch : m_batches)
    {
        ConstArrayWrapper<ModelSystemDrawable>& entities = batch.entities;

        // Set model parameters
        MaterialAsset::ModelUniformBuffer modelUniformBuffer;
        modelUniformBuffer.vParam = batch.mesh->uniformBuffer();
        batchRenderer.commitBuffer(m_handleModelUniformBuffer, modelUniformBuffer);
        batchRenderer.setGeometryAndPool(batch.geometry);
        batchRenderer.setResources(batch.resources);
        batchRenderer.setPipelineState(batch.state);

        // Draw
        const uint maxAlloc = batchRenderer.maxAllocSize<MaterialAsset::SimpleInstanceBuffer>();
        for (uint base = 0; base < entities.size(); base += maxAlloc)
        {
            const uint count = std::min(entities.size() - base, maxAlloc);

            // Fill objects
            ArrayWrapper<MaterialAsset::SimpleInstanceBuffer> perPrimitiveArray = batchRenderer
                .allocatePool<MaterialAsset::SimpleInstanceBuffer>(count);
            writeInstanceArrayToBuffer(sceneView, entities, perPrimitiveArray, base, count);

            // Draw
            batchRenderer.drawBatchInstanced(batch.startIndex, batch.numIndices, batch.baseVertex,
                                             0, count);
        }
    }
}
template <class Buffer>
void ModelSystemRenderer::writeInstanceArrayToBuffer(const SceneView& sceneView,
                                                     ConstArrayWrapper<ModelSystemDrawable> srcArray,
                                                     ArrayWrapper<Buffer> destArray,
                                                     const uint offset, const uint count)
{
    debug_assert(offset + count <= srcArray.size());
    for (uint idx = 0; idx < count; ++idx)
    {
        writeInstanceToBuffer(sceneView, srcArray[idx + offset], destArray[idx]);
    }
}


// Implement output
namespace ls
{
    template <>
    void ModelSystemRenderer::writeInstanceToBuffer<MaterialAsset::SimpleInstanceBuffer>(
        const SceneView& sceneView, const ModelSystemDrawable& src, MaterialAsset::SimpleInstanceBuffer& dest)
    {
        const float4x4& modelMatrix = src.pose
            .matrix(sceneView.camera.worldCenter());

        dest.modelMatrix = transpose(modelMatrix);
        dest.fade1 = static_cast<half>(src.fadeFactor.x);
        dest.fade2 = static_cast<half>(src.fadeFactor.y);

        // #HACK fix wind
        dest.windDirection = ubyte3(255, 128, 128);

        int wind = 0;

        switch (wind)
        {
        case 0:
            // Weak
            dest.windMain = static_cast<half>(2.5f);
            dest.windPulseFrequency = static_cast<half>(0.25f);
            dest.windPulseMagnitude = static_cast<half>(0.5f);
            dest.windTurbulence = static_cast<half>(0.4f);
            break;
        case 1:
            // Strong
            dest.windMain = static_cast<half>(10.0f);
            dest.windPulseFrequency = static_cast<half>(0.25f);
            dest.windPulseMagnitude = static_cast<half>(3.0f);
            dest.windTurbulence = static_cast<half>(3.5f);
            break;
        case 2:
            // Tornado
            dest.windMain = static_cast<half>(20.0f);
            dest.windPulseFrequency = static_cast<half>(0.25f);
            dest.windPulseMagnitude = static_cast<half>(10.0f);
            dest.windTurbulence = static_cast<half>(5.0f);
            break;
        case 3:
        {
            const float period = 60.0f;
            float phase = m_rendererTime * 0.001f / period;
            float wave = sin(phase * 2.0f * pi) * 0.5f + 0.5f;
    //         float wave = abs(fract(phase) - 0.5f) * 2.0f;
            dest.windMain = static_cast<half>(lerp(2.5f, 20.0f, wave));
            dest.windPulseFrequency = static_cast<half>(lerp(0.25f, 0.25f, wave));
            dest.windPulseMagnitude = static_cast<half>(lerp(0.5f, 10.0f, wave));
            dest.windTurbulence = static_cast<half>(lerp(0.4f, 5.0f, wave));
        }
        default:
            break;
        }

    //     dest.windMain = real2unorm<ubyte>(0.0f);
    //     dest.windPulseFrequency = real2unorm<ubyte>(0.1f);
    //     dest.windPulseMagnitude = real2unorm<ubyte>(0.0f);
    //     dest.windTurbulence = real2unorm<ubyte>(1.0f);
    }
}
