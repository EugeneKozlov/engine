/// @file ls/scene/SceneSystem.h
/// @brief Scene System
#pragma once

#include "ls/common.h"
#include "ls/scene/Component.h"

namespace ls
{
    class Entity;
    class WorldRegion;

    /// @addtogroup LsScene
    /// @{

    /// @brief Static Entities List
    using EntitiesList = vector<pair<Entity*, Component*>>;
    /// @brief Interface of Scene System Cache
    struct SceneSystemCacheInterface
    {
        /// @brief Dtor
        virtual ~SceneSystemCacheInterface() {}
    };
    /// @brief Scene System Cache
    using SceneSystemCache = shared_ptr<SceneSystemCacheInterface>;
    /// @brief Scene System
    class SceneSystem
    {
    public:
        /// @brief Ctor
        SceneSystem();
        /// @brief Dtor
        virtual ~SceneSystem();

        /// @brief Set last frame duration
        void setLastFrameTime(const float deltaSec, const DeltaTime deltaMsec);
        /// @brief Synchronous and safe scene system update
        /// @note Always called from main thread 
        /// @note No threads are running during this update
        virtual void syncUpdate();
        /// @brief Asynchronous scene system update
        /// @note Work with render systems shall be performed only using notifications
        virtual void asyncUpdate();

        /// @brief Prepare static entities to add
        /// @note Should be thread-safe
        /// @note Decide to make heavy computations and allocations here
        virtual SceneSystemCache prepareStaticEntities(const EntitiesList& entities, 
                                                       WorldRegion& region);
        /// @brief Add static entities
        virtual void addStaticEntities(const EntitiesList& entities,
                                       WorldRegion& region,
                                       SceneSystemCache preparedData);
        /// @brief Remove static entities
        virtual void removeStaticEntities(const int2& regionIndex);
        /// @brief Add entity
        virtual void addEntity(Entity& entity, Component& component) = 0;
        /// @brief Remove entity
        virtual void removeEntity(Entity& entity) = 0;

        /// @brief Get required component
        virtual ComponentType keyComponent() const = 0;
        /// @brief Get required component index
        uint key() const;
    protected:
        /// @brief Get last frame duration (sec)
        float elapsedSec() const;
        /// @brief Get last frame duration (msec)
        DeltaTime elapsedMsec() const;
    private:
        hamap<int2, vector<Entity*>> m_staticEntities;
        float m_lastElapsedSec = 0.0f;
        DeltaTime m_lastElapsedMsec = 0;
    };
    /// @brief Scene Systems
    using SceneSystemVector = vector<shared_ptr<SceneSystem>>;

    /// @}
}