/** @file ls/scene/ObjectState.h
 *  @brief Scene object
 */

#pragma once
#include "ls/common.h"
#include "ls/forward.h"

namespace ls
{
    /** @addtogroup LsScene
     *  @{
     */
    /** Object info buffer (24-bit ID and 8-bit state flags)
     */
    struct ObjectInfo
    {
    public:
        /// Invalid
        static ObjectInfo const Invalid;
        /// Empty ctor
        ObjectInfo() noexcept
        : id(InvalidObjectId)
        , state(0)
        {
        }
        /// Decode
        ObjectInfo(uint handle) noexcept
        : id(handle & 0x00ffffff)
        , state((handle & 0xff000000) >> 24)
        {
        }
        /// Pair ctor
        ObjectInfo(ObjectId id, ObjectStateFlag state) noexcept
        : id(id)
        , state(state)
        {
        }
        /// Equality
        bool operator ==(ObjectInfo const& another) noexcept
        {
            return id == another.id && state == another.state;
        }
        /// Non-equality
        bool operator !=(ObjectInfo const& another) noexcept
        {
            return !(*this == another);
        }
        /// Encode to uint
        uint encode() const noexcept
        {
            return (id & 0x00ffffff) | ((uint) state << 24);
        }
    public:
        /// Object ID
        ObjectId id;
        /// Object state
        ObjectStateFlag state;
    };
    /** Pose aspect state
     */
    struct PoseAspectState
    {
        /// My size
        uint size = sizeof(PoseAspectState);
        /// Global position
        double3 position;
        /// Global rotation
        quat rotation;
    };
    /** Movable aspect state
     */
    struct MovableAspectState
    {
        /// My size
        uint size = sizeof(MovableAspectState);
        /// Velocity
        float3 velocity;
        /// Torque
        float3 torque;
    };
    /** Info aspect state
     */
    struct InfoAspectState
    {
        /// My size
        uint size = sizeof(InfoAspectState);
        /// 'Amount'
        uint amount;
        /// 'Flags'
        uint flags;
        /// Reserved buffer
        uint buf;
    };
    /** Physical aspect state
     */
    struct PhysicalAspectState
    {
        /// My size
        uint size = sizeof(PhysicalAspectState);
        /// Corruption flags
        uint corruption;
        /// Health points
        uint health;
    };
    /** Scene object state
     */
    class ObjectState
    : public Noncopyable
    {
    public:
        /** @name Main
         *  @{
         */
        /// Empty
        ObjectState() noexcept = default;
        /// Dtor
        ~ObjectState() = default;
        /// Copy
        ObjectState(ObjectState const& another);
        /// Move
        ObjectState(ObjectState&& another) noexcept;
        /// Construct
        ObjectState(ObjectPrototype const& prototype,
                    ObjectId objectId);
        /// Reset
        void reset() noexcept;
        /// Construct
        void construct(ObjectPrototype const& prototype,
                       ObjectId objectId);
        /// Put states
        void update(ObjectState const& another);
        /// @}

        /** @name Sets
         *  @{
         */
        /// Reset time
        void resetTime(DiscreteTime time) noexcept;
        /// Reset time if new time is greaten
        void resupTime(DiscreteTime time) noexcept;
        /// Set object existence
        void setExistence(bool isExist) noexcept;
        /// Set object existence
        void materialize() noexcept;
        /// Unset object existence
        void dematerialize() noexcept;
        /// Set slot
        AspectState& setSlot(uint slot);
        /// Unset slot
        void unsetSlot(uint slot);
        /// @}

        /** @name Gets
         *  @{
         */
        /// Test if valid
        bool isValid() const noexcept
        {
            return !!m_prototype;
        }
        /// Get ID
        ObjectId id() const noexcept
        {
            return m_id;
        }
        /// Get type
        ObjectType type() const noexcept
        {
            return m_type;
        }
        /// Get prototype
        ObjectPrototype const& prototype() const noexcept
        {
            debug_assert(isValid());
            return *m_prototype;
        }
        /// Test if complete
        bool isComplete() const noexcept
        {
            return m_complete;
        }
        /// Test if tiny
        bool isTiny() const noexcept
        {
            return m_tinyFlag;
        }
        /// Test if exist
        bool isExist() const noexcept
        {
            return m_exist;
        }
        /// Test if object has specified state
        bool hasSlot(uint slot) const noexcept
        {
            debug_assert(slot < MaxNumAspects);
            return !!(m_flags & (1 << slot));
        }
        /// Get state
        AspectState& slot(uint index) noexcept
        {
            debug_assert(hasSlot(index));
            return *m_data[index];
        }
        /// Get const state
        AspectState const& slot(uint index) const noexcept
        {
            debug_assert(hasSlot(index));
            return *m_data[index];
        }
        /// Get casted state
        template <class State>
        State& slot(uint index) noexcept
        {
            return slot(index).cast<State>();
        }
        /// Get casted const state
        template <class State>
        State const& slot(uint index) const noexcept
        {
            return slot(index).cast<State>();
        }
        /// Get flags
        ObjectStateFlag flags() const noexcept
        {
            return m_flags;
        }
        /// @}
    private:
        static AspectBlobAllocator Allocator;
        void repoint();
    private:
        // Type info
        const ObjectPrototype* m_prototype;
        bool m_tinyFlag;

        // Frame info
        bool m_complete;
        bool m_exist;
        ObjectStateFlag m_flags;
        DiscreteTime m_time;

        // Object info
        ObjectId m_id;
        ObjectType m_type;

        // Tiny states
        PoseAspectState m_poseData;
        MovableAspectState m_movableData;
        InfoAspectState m_infoData;
        PhysicalAspectState m_physicalData;

        // Fat states
        MultiArray<AspectState*, MaxNumAspects> m_data;
    };
    /// @}
}


