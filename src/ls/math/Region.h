/// @file ls/math/Region.h
/// @brief Closed Region impl
#pragma once

#include "ls/math/Vector.h"

namespace ls
{
    /// @addtogroup LsMath
    /// @{

    /// @brief N-D Closed Region
    template <class T, int N>
    class ClosedRegion
    {
    public:
        /// @brief Empty
        ClosedRegion()
            : finite(false)
            , begin(static_cast<T>(0))
            , end(static_cast<T>(0))
        {
        }
        /// @brief Create
        ClosedRegion(const Vector<T, N>& begin, const Vector<T, N>& end)
            : finite(true)
            , begin(begin)
            , end(end)
        {
        }
        /// @brief Add region
        ClosedRegion& add(const ClosedRegion& other)
        {
            if (other.finite)
            {
                if (!finite)
                {
                    begin = other.begin;
                    end = other.end;
                }
                else
                {
                    begin = min(other.begin, begin);
                    end = max(other.end, end);
                }
                finite = true;
            }
            return *this;
        }
        /// @brief Add region
        ClosedRegion& add(const Vector<T, N>& begin, const Vector<T, N>& end)
        {
            return add(ClosedRegion(begin, end));
        }
        /// @brief Intersect with region
        ClosedRegion& mul(const ClosedRegion& other)
        {
            if (other.finite && finite)
            {
                begin = max(other.begin, begin);
                end = min(other.end, end);
                if (min(begin, end) != begin || max(begin, end) != end)
                    reset();
            }
            else
            {
                reset();
            }
            return *this;
        }
        /// @brief Intersect with region
        ClosedRegion& mul(const Vector<T, N>& begin, const Vector<T, N>& end)
        {
            return mul(ClosedRegion(begin, end));
        }
        /// @brief Test if region is equal to other
        bool equal(const ClosedRegion& other) const
        {
            if (finite)
                return finite == other.finite && begin == other.begin && end == other.end;
            else
                return !other.finite;
        }
        /// @brief Reset
        void reset()
        {
            finite = false;
            begin = end = static_cast<T>(0);
        }

        /// @brief Test if point is inside region
        bool inside(const Vector<T, N>& point) const
        {
            return finite && insideInc(point, begin, end);
        }
    public:
        /// @brief Non-empty flag
        bool finite;
        /// @brief Region begin
        Vector<T, N> begin;
        /// @brief Region end
        Vector<T, N> end;
    };
    /// @brief Conjunct regions
    template<class T, int N>
    ClosedRegion<T, N> operator + (const ClosedRegion<T, N>& lhs,
        const ClosedRegion<T, N>& rhs)
    {
        ClosedRegion<T, N> tmp = lhs;
        return tmp.add(rhs);
    }
    /// @brief Disjunct regions
    template<class T, int N>
    ClosedRegion<T, N> operator *(const ClosedRegion<T, N>& lhs,
        const ClosedRegion<T, N>& rhs)
    {
        ClosedRegion<T, N> tmp = lhs;
        return tmp.mul(rhs);
    }
    /// @}
}
