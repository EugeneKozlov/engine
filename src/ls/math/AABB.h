/// @file ls/math/AABB.h
/// @brief AABB
#pragma once

#include "ls/math/Vector.h"
#include "ls/math/Matrix.h"

namespace ls
{
    /// @addtogroup LsMath
    /// @{

    /// @brief Abstract 3D axis-aligned bounding box
    template <class T>
    struct AxisAlignedBoundingBox
    {
        /// @brief Scalar
        using Scalar = T;
        /// @brief Vector
        using Vector3 = Vector<Scalar, 3>;
    public:
        /// @brief Empty
        AxisAlignedBoundingBox() noexcept = default;
        /// @brief Copy
        AxisAlignedBoundingBox(const AxisAlignedBoundingBox& another) noexcept
            : min(another.min)
            , max(another.max)
        {
        }
        /// @brief Ctor
        AxisAlignedBoundingBox(const Vector3& point) noexcept
            : AxisAlignedBoundingBox(point, point)
        {
        }
        /// @brief Ctor
        AxisAlignedBoundingBox(const Vector3& min, const Vector3& max) noexcept
            : min(min)
            , max(max)
        {
        }
        /// @brief By points array
        AxisAlignedBoundingBox(const Vector3 points[], const uint num)
        {
            debug_assert(num >= 1);
            min = points[0];
            max = points[0];
            for (uint i = 1; i < num; i++)
            {
                min = ls::min(min, points[i]);
                max = ls::max(max, points[i]);
            }
        }
        /// @brief By min and max points transformed by matrix
        template <class Q, uint N>
        AxisAlignedBoundingBox(const Vector3& min, const Vector3& max,
                               const Matrix<Q, N, N>& mat)
            : AxisAlignedBoundingBox(asArray(
        {
            Vector3(min.x, min.y, min.z) * mat,
            Vector3(max.x, min.y, min.z) * mat,
            Vector3(min.x, max.y, min.z) * mat,
            Vector3(min.x, min.y, max.z) * mat,
            Vector3(max.x, max.y, min.z) * mat,
            Vector3(min.x, max.y, max.z) * mat,
            Vector3(max.x, min.y, max.z) * mat,
            Vector3(max.x, max.y, max.z) * mat
        }), 8)
        {
        }
        /// @brief By another AABB transformed by matrix
        template <class Q, uint N>
        AxisAlignedBoundingBox(const AxisAlignedBoundingBox& aabb,
                               const Matrix<Q, N, N>& mat)
            : AxisAlignedBoundingBox(aabb.min, aabb.max, mat)
        {
        }
        /// @brief By another AABB
        template <class Q>
        explicit AxisAlignedBoundingBox(const AxisAlignedBoundingBox<Q>& aabb)
            : AxisAlignedBoundingBox(static_cast<Vector3>(aabb.min), static_cast<Vector3>(aabb.max))
        {
        }

        /// @brief Get convex data
        void convex(Vector3 dest[8]) const
        {
            dest[0] = Vector3(min.x, min.y, min.z);
            dest[1] = Vector3(min.x, min.y, max.z);
            dest[2] = Vector3(min.x, max.y, min.z);
            dest[3] = Vector3(max.x, min.y, min.z);
            dest[4] = Vector3(max.x, max.y, min.z);
            dest[5] = Vector3(min.x, max.y, max.z);
            dest[6] = Vector3(max.x, min.y, max.z);
            dest[7] = Vector3(max.x, max.y, max.z);
        }
        /// @brief Get transformed convex data
        template <class Q>
        void convex(Vector3 dest[8], const Matrix4x4<Q>& mat) const
        {
            convex(dest);
            for (int i = 0; i < 8; ++i)
                dest[i] *= mat;
        }

        /// @brief Expand AABB by another
        AxisAlignedBoundingBox& operator += (const AxisAlignedBoundingBox& rhs)
        {
            min = ls::min(min, rhs.min);
            max = ls::max(max, rhs.max);
            return *this;
        }
        /// @brief Expand AABB by another
        AxisAlignedBoundingBox operator + (const AxisAlignedBoundingBox& rhs) const
        {
            AxisAlignedBoundingBox r(*this);
            r += rhs;
            return r;
        }
        /// @brief Expand AABB by point
        AxisAlignedBoundingBox& operator += (const Vector<T, 3>& rhs)
        {
            min = ls::min(min, rhs);
            max = ls::max(max, rhs);
            return *this;
        }
        /// @brief Expand AABB by point
        AxisAlignedBoundingBox operator + (const Vector<T, 3>& rhs) const
        {
            AxisAlignedBoundingBox r(*this);
            r += rhs;
            return r;
        }
        /// @brief Transform AABB
        template <class Q, uint N>
        AxisAlignedBoundingBox operator * (const Matrix<Q, N, N>& mat) const
        {
            return AxisAlignedBoundingBox(*this, mat);
        }
        /// @brief Transform AABB
        template <class Q, uint N>
        AxisAlignedBoundingBox& operator *= (const Matrix<Q, N, N>& mat)
        {
            return *this = *this * mat;
        }

        /// @brief Test if empty
        bool isEmpty() const
        {
            return min.x > max.x;
        }
        /// @brief Get extents
        Vector<T, 3> extents() const
        {
            return max - min;
        }
        /// @brief Get extents
        Vector<T, 3> center() const
        {
            return (min + max) / 2;
        }
        /// @brief Get max extent
        T maxExtent() const 
        {
            Vector<T, 3> ext = extents();
            return std::max(std::max(ext.x, ext.y), ext.z);
        }
    public:
        /// @brief Lower bound
        Vector3 min = Vector3::max();
        /// @brief Upper bound
        Vector3 max = -Vector3::max();
    };
    /// @brief Square distance between AABB and point.
    template <class T>
    T square_distance(const AxisAlignedBoundingBox<T>& aabb, const Vector<T, 3>& point)
    {
        const Vector<T, 3> aabb_center = (aabb.min + aabb.max) / 2;
        const Vector<T, 3> a = abs(aabb_center - point) - (aabb.max - aabb_center);
        return square_length(max(a, Vector<T, 3>(static_cast<T>(0))));
    }
    /// @brief Distance between AABB and point.
    template <class T>
    T distance(const AxisAlignedBoundingBox<T>& aabb, const Vector<T, 3>& point)
    {
        return sqrt(square_distance(aabb, point));
    }
    /// @}
}