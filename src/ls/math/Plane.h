/// @file ls/math/Plane.h
/// @brief Plane template
#pragma once

#include "ls/math/Vector.h"

namespace ls
{
    /// @addtogroup LsMath
    /// @{

    /// @brief 3D plane
    template <class T>
    class Plane
    {
    public:
        /// @brief Vector type
        using Vector3 = Vector<T, 3>;
    public:
        /// @brief Degenerate
        Plane()
            : n(0, 0, 0)
            , dist(0)
        {
        }
        /// @brief By parameters
        Plane(const T x, const T y, const T z, const T d)
            : n(normalize(Vector3(x, y, z)))
            , dist(d)
        {
        }
        /// @brief By normal and signed distance
        Plane(const Vector3& vn, const T d)
            : n(normalize(vn))
            , dist(d)
        {
        }
        /// @brief By points on plane
        Plane(const Vector3& v1, const Vector3& v2, const Vector3& v3)
            : n(normalize(cross(v2 - v1, v3 - v1)))
            , dist(-dot(n, v1))
        {
        }
        /// @brief By normal and point on plane
        Plane(const Vector3& vn, const Vector3& vp)
            : n(normalize(vn))
            , dist(-dot(n, vp))
        {
        }
    public:
        /// @brief Normal
        Vector3 n;
        /// @brief Projection vector from plane to 0 on normal
        T dist;
    };
    /// @brief Signed distance from plane to point (plane should be normalized)
    template <class T>
    T dot(const Plane<T>& plane, const Vector<T, 3>& vec)
    {
        return dot(plane.n, vec) + plane.dist;
    }
    /// @}
}
