/// @file ls/math/matrix_construct.h
/// @brief Matrix construction
#pragma once

#include "ls/math/Vector.h"
#include "ls/math/Matrix.h"
#include "ls/math/Quaternion.h"

namespace ls
{
    /// @addtogroup LsMath
    /// @{

    /// @brief Matrix construction
    namespace matrix
    {
        /// @brief Matrix by vector scaling
        template <class T, uint N>
        Matrix<T, N, N> scale(const Vector<T, N>& scale)
        {
            Matrix<T, N, N> ret;
            for (uint i = 0; i < N; ++i)
                ret(i, i) = scale[i];
            return ret;
        }
        /// @brief Matrix by translation
        template <class T, uint N>
        Matrix<T, N + 1, N + 1> translate(const Vector<T, N>& tr)
        {
            Matrix<T, N + 1, N + 1> ret;
            ret.row(N, tr);
            return ret;
        }
        /// @brief Matrix by basis
        template <class T, uint N>
        Matrix<T, N, N> fromBasis(const Vector<T, N>& xaxis,
                                  const Vector<T, N>& yaxis,
                                  const Vector<T, N>& zaxis)
        {
            Matrix<T, N, N> ret;
            ret.row(0, xaxis);
            ret.row(1, yaxis);
            ret.row(2, zaxis);
            return ret;
        }
        /// @brief Matrix by x-axis rotation
        template <class T>
        Matrix3x3<T> rotationX(const T x)
        {
            const T cosA = cos(x);
            const T sinA = sin(x);
            return{ 1, 0, 0,
                    0, cosA, sinA,
                    0, -sinA, cosA };
        }
        /// @brief Matrix by y-axis rotation
        template <class T>
        Matrix3x3<T> rotationY(const T y)
        {
            const T cosA = cos(y);
            const T sinA = sin(y);
            return{ cosA, 0, -sinA,
                    0, 1, 0,
                    sinA, 0, cosA };
        }
        /// @brief Matrix by z-axis rotation
        template <class T>
        Matrix3x3<T> rotationZ(const T z)
        {
            const T cosA = cos(z);
            const T sinA = sin(z);
            return{ cosA, sinA, 0,
                    -sinA, cosA, 0,
                    0, 0, 1 };
        }
        /// @brief Matrix by x,y,z-axis rotation
        template <class T>
        Matrix3x3<T> rotationXYZ(const T x, const T y, const T z)
        {
            return rotationX(x) * rotationY(y) * rotationZ(z);
        }
        /// @brief Matrix by y,x,z-axis rotation
        template <class T>
        Matrix3x3<T> rotationYXZ(const T x, const T y, const T z)
        {
            return rotationY(y) * rotationX(x) * rotationZ(z);
        }
        /// @brief Matrix by rotation around axis
        template <class T>
        Matrix3x3<T> rotationAxis(const Vector<T, 3>& axis, const T angle)
        {
            const T sinA = sin(angle);
            const T cosA = cos(angle), cosInv = 1 - cosA;
            const T len2 = square_length(axis);
            const T len = sqrtf(len2);
            return{ (axis.x * axis.x + (axis.y * axis.y + axis.z * axis.z) * cosA) / len2,
                    (axis.x * axis.y * cosInv - axis.z * len * sinA) / len2,
                    (axis.x * axis.z * cosInv + axis.y * len * sinA) / len2,

                    (axis.x * axis.y * cosInv + axis.z * len * sinA) / len2,
                    (axis.y * axis.y + (axis.x * axis.x + axis.z * axis.z) * cosA) / len2,
                    (axis.z * axis.y * cosInv - axis.x * len * sinA) / len2,

                    (axis.x * axis.z * cosInv - axis.y * len * sinA) / len2,
                    (axis.y * axis.z * cosInv + axis.x * len * sinA) / len2,
                    (axis.z * axis.z + (axis.y * axis.y + axis.x * axis.x) * cosA) / len2 };
        }
        /// @brief Matrix by quaternion
        template <class T>
        Matrix3x3<T> rotationQuaternion(const Quaternion<T>& q)
        {
            const T x2 = q.x * q.x;
            const T y2 = q.y * q.y;
            const T z2 = q.z * q.z;
            const T xy = q.x * q.y;
            const T zw = q.z * q.w;
            const T xz = q.x * q.z;
            const T yw = q.y * q.w;
            const T yz = q.y * q.z;
            const T xw = q.x * q.w;
            return{ 1 - 2 * y2 - 2 * z2, 2 * (xy + zw), 2 * (xz - yw),
                    2 * (xy - zw), 1 - 2 * x2 - 2 * z2, 2 * (yz + xw),
                    2 * (xz + yw), 2 * (yz - xw), 1 - 2 * x2 - 2 * y2 };
        }
        /// @brief Matrix by quaternion
        template <class T>
        Matrix4x4<T> rotationQuaternion4(const Quaternion<T>& q)
        {
            return (float4x4) rotationQuaternion(q);
        }
        /// @brief Perspective projection matrix for left-handed system
        template<class T>
        Matrix4x4<T> perspectiveLeft(const T fovy, const T aspect, const T znear, const T zfar)
        {
            const T f = 1 / tanf(fovy / 2);
            const T A = zfar / (zfar - znear);
            const T B = znear * zfar / (zfar - znear);

            return{ f / aspect, 0, 0, 0,
                    0, f, 0, 0,
                    0, 0, A, 1,
                    0, 0, -B, 0 };
        }
        /// @brief Perspective projection matrix for right-handed system
        template <class T>
        Matrix4x4<T> perspectiveRight(const T fovy, const T aspect, const T znear, const T zfar)
        {
            const T f = 1 / tanf(fovy / 2);
            const T A = zfar / (zfar - znear);
            const T B = znear * zfar / (zfar - znear);

            return{ f / aspect, 0, 0, 0,
                    0, f, 0, 0,
                    0, 0, A, -1,
                    0, 0, B, 0 };
        }
        /// @brief Ortho projection matrix in left-handed system
        template <class T>
        Matrix4x4<T> orthoLeft(const T left, const T right, const T bottom, const T top,
                               const T znear, const T zfar)
        {
            const T tx = (left + right) / (left - right);
            const T ty = (top + bottom) / (bottom - top);
            const T tz = znear / (znear - zfar);
            return{ 2 / (right - left), 0, 0, 0,
                    0, 2 / (top - bottom), 0, 0,
                    0, 0, 1 / (zfar - znear), 0,
                    tx, ty, tz, 1 };
        }
        /// @brief Ortho projection matrix in right-handed system
        template <class T>
        Matrix4x4<T> orthoRight(const T left, const T right, const T bottom, const T top,
                                const T znear, const T zfar)
        {
            const T tx = (left + right) / (left - right);
            const T ty = (top + bottom) / (bottom - top);
            const T tz = znear / (znear - zfar);
            return{ 2 / (right - left), 0, 0, 0,
                    0, 2 / (top - bottom), 0, 0,
                    0, 0, 1 / (znear - zfar), 0,
                    tx, ty, tz, 1 };
        }
        /// @brief Restore ortho projection matrix parameters in left-handed system
        template <class T>
        void restoreOrthoLeft(const Matrix4x4<T>& m,
                              T& right, T& left,
                              T& bottom, T& top,
                              T& znear, T& zfar)
        {
            const T A = m.element(0, 0);
            const T B = m.element(1, 1);
            const T C = m.element(2, 2);
            const T D = m.element(3, 0);
            const T E = m.element(3, 1);
            const T F = m.element(3, 2);
            right = (1 - D) / A;
            top = (1 - E) / B;
            znear = -F / C;
            left = right - 2 / A;
            bottom = top - 2 / B;
            zfar = 1 / C + znear;
        }
        /// @brief Matrix by vector scaling, rotation and translation.
        template <class T>
        Matrix4x4<T> compose(const Vector<T, 3>& pos,
                             const Quaternion<T>& rot,
                             const Vector<T, 3>& scaling)
        {
            Matrix4x4<T> t = scale(scaling) * rotationQuaternion(rot);
            t.row(3, pos);
            return t;
        }
        /// @brief Matrix by scalar scaling, rotation and translation.
        template <class T>
        Matrix4x4<T> compose(const Vector<T, 3>& pos,
                             const Quaternion<T>& rot,
                             const T scaling)
        {
            Matrix4x4<T> t = scale(scaling) * rotationQuaternion(rot);
            t.row(3, pos);
            return t;
        }
        /// @brief Extract translation from matrix
        template <class T>
        Vector<T, 3> extractTranslation(const Matrix4x4<T>& source)
        {
            return source.template row<3>(3);
        }
        /// @brief Extract scaling from matrix
        template <class T, uint N>
        Vector<T, 3> extractScaling(const Matrix<T, N, N>& source)
        {
            return{ length(source.template row<3>(0)),
                    length(source.template row<3>(1)),
                    length(source.template row<3>(2)) };
        }
        /// @brief Extract rotation from @b normalized matrix
        template <class T, uint N>
        Quaternion<T> extractRotation(const Matrix<T, N, N>& source)
        {
            Quaternion<T> rotation;

            if (1 + source.element(0, 0) + source.element(1, 1) + source.element(2, 2) > std::numeric_limits<T>::epsilon())
            {
                rotation.w = std::sqrt(1 + source.element(0, 0) + source.element(1, 1) + source.element(2, 2)) / 2;
                rotation.x = -(source.element(2, 1) - source.element(1, 2)) / (4 * rotation.w);
                rotation.y = -(source.element(0, 2) - source.element(2, 0)) / (4 * rotation.w);
                rotation.z = -(source.element(1, 0) - source.element(0, 1)) / (4 * rotation.w);
            }
            else if (source.element(0, 0) > source.element(1, 1) && source.element(0, 0) > source.element(2, 2))
            {
                rotation.x = -std::sqrt(1 + source(0, 0) - source(1, 1) - source(2, 2)) / 2;
                rotation.y = -(source(0, 1) + source(1, 0)) / (4 * rotation.x);
                rotation.z = -(source(0, 2) + source(2, 0)) / (4 * rotation.x);
                rotation.w = (source(2, 1) - source(1, 2)) / (4 * rotation.x);
            }
            else if (source.element(1, 1) > source.element(2, 2))
            {
                rotation.y = -std::sqrt(1 + source.element(1, 1) - source.element(0, 0) - source.element(2, 2)) / 2;
                rotation.x = -(source.element(0, 1) + source.element(1, 0)) / (4 * rotation.y);
                rotation.z = -(source.element(1, 2) + source.element(2, 1)) / (4 * rotation.y);
                rotation.w = (source.element(0, 2) - source.element(2, 0)) / (4 * rotation.y);
            }
            else
            {
                rotation.z = -std::sqrt(1 + source.element(2, 2) - source(0, 0) - source.element(1, 1)) / 2;
                rotation.x = -(source.element(0, 2) + source.element(2, 0)) / (4 * rotation.z);
                rotation.y = -(source.element(1, 2) + source.element(2, 1)) / (4 * rotation.z);
                rotation.w = (source.element(1, 0) - source.element(0, 1)) / (4 * rotation.z);
            }

            return normalize(rotation);
        }
        /// @brief Decompose transform matrix
        template <class T>
        void decompose(Matrix4x4<T> source,
                       Vector<T, 3>& translation,
                       Quaternion<T>& rotation,
                       Vector<T, 3>& scaling)
        {
            translation = extractTranslation(source);
            scaling = extractScaling(source);

            // Normalize
            Vector<T, 3> invScaling = Vector<T, 3>(1.0f) / scaling;
            source.element(0, 0) *= invScaling[0];
            source.element(0, 1) *= invScaling[0];
            source.element(0, 2) *= invScaling[0];
            source.element(1, 0) *= invScaling[1];
            source.element(1, 1) *= invScaling[1];
            source.element(1, 2) *= invScaling[1];
            source.element(2, 0) *= invScaling[2];
            source.element(2, 1) *= invScaling[2];
            source.element(2, 2) *= invScaling[2];

            rotation = extractRotation(source);
        }
    }
    /// @brief Pack axises to quaternion
    template <class T>
    Quaternion<T> packAxises(const Vector<T, 3>& xaxis, const Vector<T, 3>& yaxis, const Vector<T, 3>& zaxis)
    {
        Vector<T, 3> pos, scale;
        Quaternion<T> rot;
        Matrix4x4<T> mat;

        mat.row(0, xaxis);
        mat.row(1, yaxis);
        mat.row(2, zaxis);
        matrix::decompose(mat, pos, rot, scale);

        return normalize(rot);
    }
    /// @}
}
