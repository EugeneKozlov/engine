/// @file ls/math/alias.h
/// @brief Math classes aliases
#pragma once

#include "ls/common/integer.h"
#include "ls/math/common.h"
#include "ls/math/half.h"
#include "ls/math/Vector.h"
#include "ls/math/Matrix.h"
#include "ls/math/Quaternion.h"
#include "ls/math/AABB.h"
#include "ls/math/Frustum.h"
#include "ls/math/Region.h"
#include "ls/math/Plane.h"

namespace ls
{
    /// @addtogroup LsMath
    /// @{

    /// @brief Fixed point scalar with 2^-14 precision (+-262144 ~ 0.0001)
    using int4fixed = fpml::fixed_point<sint, 32 - 14>;
    /// @brief Fixed point scalar with 2^-10 precision (+-4194304 ~ 0.001)
    using int3fixed = fpml::fixed_point<sint, 32 - 10>;
    /// @brief Is real?
    template <class T>
    using EnableIfReal = enable_if_t<
        is_floating_point<T>::value
        || is_same<T, int4fixed>::value
        || is_same<T, int3fixed>::value>;

    /// @brief Byte 2D vector
    using byte2 = Vector<sbyte, 2>;
    /// @brief Byte 3D vector
    using byte3 = Vector<sbyte, 3>;
    /// @brief Byte 4D vector
    using byte4 = Vector<sbyte, 4>;
    /// @brief Ubyte 2D vector
    using ubyte2 = Vector<ubyte, 2>;
    /// @brief Ubyte 3D vector
    using ubyte3 = Vector<ubyte, 3>;
    /// @brief Ubyte 4D vector
    using ubyte4 = Vector<ubyte, 4>;
    /// @brief Float 2D vector
    using float2 = Vector<float, 2>;
    /// @brief Float 3D vector
    using float3 = Vector<float, 3>;
    /// @brief Float 4D vector
    using float4 = Vector<float, 4>;
    /// @brief Float 2D vector
    using double2 = Vector<double, 2>;
    /// @brief Float 3D vector
    using double3 = Vector<double, 3>;
    /// @brief Float 4D vector
    using double4 = Vector<double, 4>;
    /// @brief Short 2D vector
    using short2 = Vector<sshort, 2>;
    /// @brief Short 3D vector
    using short3 = Vector<sshort, 3>;
    /// @brief Integer 2D vector
    using int2 = Vector<sint, 2>;
    /// @brief Integer 3D vector
    using int3 = Vector<sint, 3>;
    /// @brief Integer 4D vector
    using int4 = Vector<sint, 4>;
    /// @brief Unsigned Integer 2D vector
    using uint2 = Vector<uint, 2>;
    /// @brief Unsigned Integer 3D vector
    using uint3 = Vector<uint, 3>;
    /// @brief Unsigned Integer 4D vector
    using uint4 = Vector<uint, 4>;
    /// @brief 2D fixed3 vector
    using int3fixed2 = Vector<int3fixed, 2>;
    /// @brief 3D fixed3 vector
    using int3fixed3 = Vector<int3fixed, 3>;
    /// @brief 4D fixed3 vector
    using int3fixed4 = Vector<int3fixed, 4>;
    /// @brief 2D fixed4 vector
    using int4fixed2 = Vector<int4fixed, 2>;
    /// @brief 3D fixed4 vector
    using int4fixed3 = Vector<int4fixed, 3>;
    /// @brief 4D fixed4 vector
    using int4fixed4 = Vector<int4fixed, 4>;

    /// @brief Double 4x4 matrix
    using double4x4 = Matrix4x4<double>;
    /// @brief Double 3x3 matrix
    using double3x3 = Matrix3x3<double>;
    /// @brief Double 3x4 matrix
    using double3x4 = Matrix<double, 3, 4>;
    /// @brief Float 4x4 matrix
    using float4x4 = Matrix4x4<float>;
    /// @brief Float 3x3 matrix
    using float3x3 = Matrix3x3<float>;
    /// @brief Float 3x4 matrix
    using float3x4 = Matrix<float, 3, 4>;
    /// @brief Integer 4x4 matrix
    using int4x4 = Matrix4x4<sint>;

    /// @brief Quaternion
    using quat = Quaternion<float>;
    /// @brief Frustum
    using Frustum = FrustumPyramide<double>;
    /// @brief Region
    using Region = ClosedRegion<sint, 2>;
    /// @brief AABB
    using AABB = AxisAlignedBoundingBox<double>;
    /// @brief AABB
    using FloatAABB = AxisAlignedBoundingBox<float>;
    /// @}
}