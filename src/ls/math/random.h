/// @file ls/math/random.h
/// @brief Random generator
#pragma once

#include "ls/common/integer.h"

namespace ls
{
    /// @addtogroup LsMath
    /// @{

    /// @brief Set seed for random numbers generator
    void randomSeed(const uint seed);
    /// @brief Random number in range [0, 4294967295]
    uint random();
    /// @brief Random real number in range [min, max]
    template <class Number>
    inline EnableIfFloat<Number, Number> random(const Number& min,
                                                const Number& max)
    {
        return Number((long double) (random()) / MaxUint * (max - min) + min);
    }
    /// @brief Random integer number in range [min, max]
    template <class Number>
    inline EnableIfInteger<Number, Number> random(const Number& min,
                                                  const Number& max)
    {
        return Number(random() % (max - min + 1)) + min;
    }
    /// @}
}
