/// @file ls/math/color.h
/// @brief Color functions templates
#pragma once

#include "ls/math/alias.h"

namespace ls
{
    /// @addtogroup LsMath
    /// @{

    /// @brief Convert color from float vector to uint32.
    template <class Scalar>
    inline uint rgb2hex(const Scalar& c);
    /// @brief Convert color from uint32 to float vector.
    template <class Scalar>
    inline Scalar hex2rgb(const uint c);
    /// @brief Convert color from float3 to uint32.
    template <>
    inline uint rgb2hex(const float3& c)
    {
        const uint r = clamp<uint>(static_cast<uint>(floor(c.x*255.0f + 0.5f)), 0, 255);
        const uint g = clamp<uint>(static_cast<uint>(floor(c.y*255.0f + 0.5f)), 0, 255);
        const uint b = clamp<uint>(static_cast<uint>(floor(c.z*255.0f + 0.5f)), 0, 255);
        return 0xff000000 | r | (g << 8) | (b << 16);
    }
    /// @brief Convert color from float4 to uint32.
    template <>
    inline uint rgb2hex(const float4& c)
    {
        const uint r = clamp<uint>(static_cast<uint>(floor(c.x*255.0f + 0.5f)), 0, 255);
        const uint g = clamp<uint>(static_cast<uint>(floor(c.y*255.0f + 0.5f)), 0, 255);
        const uint b = clamp<uint>(static_cast<uint>(floor(c.z*255.0f + 0.5f)), 0, 255);
        const uint a = clamp<uint>(static_cast<uint>(floor(c.w*255.0f + 0.5f)), 0, 255);
        return r | (g << 8) | (b << 16) | (a << 24);
    }
    template <>
    inline float3 hex2rgb(const uint c)
    {
        return float3(static_cast<float>(c % 256),
                      static_cast<float>((c >> 8) % 256),
                      static_cast<float>((c >> 16) % 256))
            / 255.0f;
    }
    template<>
    inline float4 hex2rgb(const uint c)
    {
        return float4(static_cast<float>(c % 256),
                      static_cast<float>((c >> 8) % 256),
                      static_cast<float>((c >> 16) % 256),
                      static_cast<float>((c >> 24) % 256))
            / 255.0f;
    }
    /// @brief Interpolate uint colors
    /// @param c1,c2
    ///   Colors
    /// @param k
    ///   Factor
    /// @return Interpolated color
    ///
    inline uint clerp(const uint colorA, const uint colorB, const float factor)
    {
        return rgb2hex(lerp(hex2rgb<float4>(colorA), 
                            hex2rgb<float4>(colorB), 
                            factor));
    }
    /// @brief Color brightness
    /// @tparam T Type of color vector components
    /// @param color
    ///   Color
    /// @return Brightness
    ///
    template <class Scalar>
    inline Scalar luma(const Vector<Scalar, 3>& color)
    {
        return dot(Vector<Scalar, 3>(static_cast<Scalar>(0.299),
                                     static_cast<Scalar>(0.587),
                                     static_cast<Scalar>(0.114)),
                   color);
    }
    /// @}
}
