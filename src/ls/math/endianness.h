/// @file ls/math/endianness.h
/// @brief Endianness conversions
#pragma once

#include "ls/common.h"
#include "ls/math/common.h"

namespace ls
{
    /// @addtogroup LsMath
    /// @{

    /// @brief Swap bytes in number
    template <class Integer>
    inline Integer swapBytes(const Integer value)
    {
        Integer ret;
        const int size = sizeof(Integer);
        const ubyte* src = reinterpret_cast<const ubyte*>(&value);
        ubyte* dest = reinterpret_cast<ubyte*>(&ret);
        for (int i = 0; i < size; ++i)
            dest[i] = src[size - i - 1];
        return ret;
    }
    /// @brief Test endianness
    inline bool isBigEndianness()
    {
        static const int value = 1;
        static const bool flag = *reinterpret_cast<const char*>(&value) == value;
        return flag;
    }
    /// @brief Swap bytes in integer
    template <class Integer, class = EnableIfInteger<Integer>>
    inline Integer swapEndianness(const Integer value)
    {
        if (isBigEndianness())
            return swapBytes(value);
        else
            return value;
    }
    /// @brief Convert host to network
    template <class Integer, class = EnableIfInteger<Integer>>
    inline Integer hton(const Integer value)
    {
        return swapEndianness(value);
    }
    /// @brief Convert network to host
    template <class Integer, class = EnableIfInteger<Integer>>
    inline Integer ntoh(const Integer value)
    {
        return swapEndianness(value);
    }
    /// @}
}
