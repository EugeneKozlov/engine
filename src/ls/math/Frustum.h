/// @file ls/math/Frustum.h
/// @brief Frustum template
#pragma once

#include "ls/math/Vector.h"
#include "ls/math/Plane.h"
#include "ls/math/AABB.h"

namespace ls
{
    /// @addtogroup LsMath
    /// @{

    /// @brief Frustum.
    template <class T>
    class FrustumPyramide
    {
    public:
        /// @brief Vector type
        using Vector3 = Vector<T, 3>;
    public:
        /// @brief Degenerate
        FrustumPyramide()
        {
        }
        /// @brief By array of 8 points
        FrustumPyramide(const Vector3 p[8])
        {
            calc(p);
        }
        /// @brief By array of 8 points
        void calc(const Vector3 p[8])
        {
            std::copy(p, p + 8, points);
            planes[Left] = Plane<T>(points[0], points[4], points[2]);
            planes[Right] = Plane<T>(points[1], points[3], points[5]);
            planes[Bottom] = Plane<T>(points[0], points[1], points[4]);
            planes[Top] = Plane<T>(points[2], points[6], points[3]);
            planes[Near] = Plane<T>(points[0], points[2], points[1]);
            planes[Far] = Plane<T>(points[4], points[5], points[6]);
        }

        /// @brief Intersect with convex
        bool isConvexIntersect(const Vector<T, 3> p[], const uint cnt) const
        {
            for (int i = 0; i < 6; i++)
            {
                bool PlaneTest = true;
                for (uint j = 0; j < cnt; j++)
                    if (dot(planes[i], p[j]) < 0)
                    {
                        PlaneTest = false;
                        break;
                    }

                if (PlaneTest)
                    return false;
            }
            return true;
        }
        /// @brief Intersect with AABB
        bool isAabbIntersect(const Vector<T, 3>& min,
                             const Vector<T, 3>& max) const
        {
            return internalFrustumAabbTest(min, max);
        }
        /// @brief Intersect with AABB
        bool isAabbIntersect(const AxisAlignedBoundingBox<T>& aabb) const
        {
            return isAabbIntersect(aabb.min, aabb.max);
        }
        /// @brief Intersect with point
        bool isPointIntersect(const Vector<T, 3>& p) const
        {
            return isConvexIntersect(&p, 1);
        }
    private:
        /// @brief AABB intersection test
        inline bool internalFrustumAabbTest(const Vector<T, 3>& min,
                                            const Vector<T, 3>& max) const
        {
            const Vector3 convex[8] ={
                Vector3(min.x, min.y, min.z),
                Vector3(min.x, min.y, max.z),
                Vector3(min.x, max.y, min.z),
                Vector3(max.x, min.y, min.z),
                Vector3(max.x, max.y, min.z),
                Vector3(min.x, max.y, max.z),
                Vector3(max.x, min.y, max.z),
                Vector3(max.x, max.y, max.z)
            };
            return isConvexIntersect(convex, 8);
        }
    public:
        /// @brief Frustum planes IDs
        enum PlaneIndex
        {
            /// @brief Left
            Left = 0,
            /// @brief Right
            Right,
            /// @brief Bottom
            Bottom,
            /// @brief Top
            Top,
            /// @brief Near
            Near,
            /// @brief Far
            Far
        };
        /// @brief Frustum points IDs
        enum PointIndex
        {
            LeftBottomNear = 0,
            RightBottomNear,
            LeftUpNear,
            RightUpNear,
            LeftBottomFar,
            RightBottomFar,
            LeftUpFar,
            RightUpFar,
        };
        /// @brief AABB planes
        Plane<T> planes[6];
        /// @brief AABB points
        Vector3 points[8];
    };
    /// @}
}
