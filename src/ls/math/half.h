/// @file ls/math/half.h
/// @brief Half float
#pragma once

#include "ls/common/import.h"
#include "ls/common/concept.h"
#include <fpml/fixed_point.h>

namespace ls
{
    /// @addtogroup LsMath
    /// @{

    /// @brief Convert float to half
    ushort float2half(const float value);
    /// @brief Convert half to float
    float half2float(const ushort value);
    /// @brief Half
    struct half
    {
    public:
        /// @brief Ctor
        half() = default;
        /// @brief Ctor
        explicit half(const float value)
            : value(float2half(value))
        {
        }
        /// @brief Ctor
        explicit half(const double value)
            : half(static_cast<float>(value))
        {
        }
        /// @brief Ctor
        template <class B, unsigned char I>
        explicit half(const fpml::fixed_point<B, I> value)
            : half(static_cast<double>(value))
        {
        }
        /// @brief Cast
        explicit operator float() const
        {
            return half2float(value);
        }
        /// @brief Cast
        explicit operator double() const
        {
            return static_cast<double>(static_cast<float>(*this));
        }
        /// @brief Cast
        template <class B, unsigned char I>
        explicit operator fpml::fixed_point<B, I>() const
        {
            return fpml::fixed_point<B, I>(static_cast<float>(*this));
        }
    public:
        /// @brief Value
        ushort value = 0;
    };
    /// @}
}
