/// @file ls/math/Matrix.h
/// @brief Matrix templates
#pragma once

#include "ls/common/assert.h"
#include "ls/math/Vector.h"

namespace ls
{
    /// @addtogroup LsMath
    /// @{
    
    /// @brief Transpose Matrix Tag
    struct TransposeMatrixTag {};
    /// @brief Base row-major Matrix type
    template <class T, uint N, uint M>
    class Matrix
    {
    public:
        /// @brief Element type
        using Scalar = T;
        /// @brief Number of rows
        static const uint NumRows = N;
        /// @brief Number of columns
        static const uint NumColumns = M;
        /// @brief Number of components
        static const int NumCells = NumRows * NumColumns;
    public:
        /// @brief Ctor
        Matrix()
        {
            for (int i = 0; i < NumColumns; i++)
                for (int j = 0; j < NumRows; j++)
                {
                    element(i, j) = (i == j) ? Scalar(1) : Scalar(0);
                }
        }
        /// @brief Ctor by matrix
        template <uint N2, uint M2>
        Matrix(const Matrix<T, N2, M2>& another)
        {
            for (uint j = 0; j < NumRows; ++j)
                for (uint i = 0; i < NumColumns; ++i)
                {
                    if (i < M2 && j < N2)
                        element(j, i) = another.element(j, i);
                    else
                        element(j, i) = Scalar(i == j);
                }
        }
        /// @brief Ctor by transposed matrix
        template <uint N2, uint M2>
        Matrix(const Matrix<T, N2, M2>& another, TransposeMatrixTag)
        {
            for (uint j = 0; j < NumRows; ++j)
                for (uint i = 0; i < NumColumns; ++i)
                {
                    if (i < N2 && j < M2)
                        element(j, i) = another.element(i, j);
                    else
                        element(j, i) = Scalar(i == j);
                }
        }
        /// @brief By scalar
        Matrix(const Scalar t) noexcept
        {
            for (uint j = 0; j < NumRows; ++j)
                for (uint i = 0; i < NumColumns; ++i)
                {
                    element(j, i) = t;
                }
        }
        /// @brief By scalar array
        Matrix(const Scalar p[NumCells]) noexcept
        {
            for (uint j = 0; j < NumRows; ++j)
                for (uint i = 0; i < NumColumns; ++i)
                {
                    element(j, i) = p[j * NumColumns + i];
                }
        }
        /// @brief Ctor by iterators
        template <class Iterator, class = decltype(*std::declval<Iterator>())>
        Matrix(Iterator begin, Iterator end)
        {
            for (int i = 0; i < NumCells; ++i)
            {
                debug_assert(begin != end);
                m[i] = *begin;
                ++begin;
            }
            debug_assert(begin == end);
        }
        /// @brief Ctor by init-list
        Matrix(initializer_list<Scalar> p)
            : Matrix(p.begin(), p.end())
        {
        }
    public:
        /// @brief Get cell
        Scalar& operator [](const uint idx)
        {
            return m[idx];
        }
        /// @brief Get constant cell
        const Scalar& operator [](const uint idx) const
        {
            return m[idx];
        }
        /// @brief Get element
        Scalar& element(const uint row, const uint col) noexcept
        {
            return m[col + row * NumColumns];
        }
        /// @brief Get constant element
        const Scalar& element(const uint row, const uint col) const noexcept
        {
            return m[col + row * NumColumns];
        }
        /// @brief Get element
        Scalar& operator ()(const uint row, const uint col) noexcept
        {
            return element(row, col);
        }
        /// @brief Get constant element
        const Scalar& operator ()(const uint row, const uint col) const noexcept
        {
            return element(row, col);
        }
        /// @brief Get row
        const Vector<Scalar, NumColumns>& row(const uint row) const noexcept
        {
            return *reinterpret_cast<const Vector<Scalar, NumColumns>*>(&m[row * NumColumns]);
        }
        /// @brief Get row
        template <uint K, class = enable_if_t<K <= NumColumns>>
        const Vector<Scalar, K>& row(const uint row) const noexcept
        {
            return *reinterpret_cast<const Vector<Scalar, K>*>(&m[row * NumColumns]);
        }
        /// @brief Set row
        template <uint K, class = enable_if_t<K <= NumColumns>>
        void row(const uint row, const Vector<Scalar, K>& value) noexcept
        {
            for (uint i = 0; i < K; ++i)
                element(row, i) = value[i];
        }
        /// @brief Get column
        const Vector<Scalar, NumRows> column(const uint col) const noexcept
        {
            Vector<Scalar, NumRows> ret;
            for (uint i = 0; i < NumRows; ++i)
                ret[i] = element(i, col);
            return ret;
        }
        /// @brief Get column
        template <uint K, class = enable_if_t<K <= NumRows>>
        const Vector<Scalar, K> column(const uint col) const noexcept
        {
            Vector<Scalar, K> ret;
            for (uint i = 0; i < K; ++i)
                ret[i] = element(i, col);
            return ret;
        }
        /// @brief Set column
        template <uint K, class = enable_if_t<K <= NumRows>>
        void column(const uint col, const Vector<Scalar, K>& value) noexcept
        {
            for (uint i = 0; i < K; ++i)
                element(i, row) = value[i];
        }
        /// @brief Get scalar array
        Scalar* value() noexcept
        {
            return m;
        }
        /// @brief Get constant scalar array
        const Scalar* value() const noexcept
        {
            return m;
        }
        /// @brief Cast
        template <class Q>
        explicit operator Matrix<Q, N, M>() const
        {
            Matrix<Q, N, M> ret;
            for (int i = 0; i < NumColumns; i++)
                for (int j = 0; j < NumRows; j++)
                {
                    ret(i, j) = static_cast<Q>(element(i, j));
                }
            return ret;
        }
    public:
        /// @brief Data
        Scalar m[NumCells];
    };
    /// @brief Multiple matrix by matrix
    template <class T, uint N, uint M, uint K>
    Matrix<T, N, K> multiple(const Matrix<T, N, M>& lhs, const Matrix<T, M, K>& rhs)
    {
        Matrix<T, N, K> ret;
        for (uint i = 0; i < N; ++i)
            for (uint j = 0; j < K; ++j)
            {
                T value = 0;
                for (uint k = 0; k < M; ++k)
                    value += lhs.element(i, k) * rhs.element(k, j);
                ret.element(i, j) = value;
            }
        return ret;
    }
    /// @brief Multiple matrix by matrix
    template <class T, uint N, uint M, uint K>
    Matrix<T, N, K> operator * (const Matrix<T, N, M>& lhs, const Matrix<T, M, K>& rhs)
    {
        return multiple(lhs, rhs);
    }
    /// @brief Multiple matrix by matrix
    template <class T, uint N>
    Matrix<T, N, N>& operator *= (Matrix<T, N, N>& lhs, const Matrix<T, N, N>& rhs)
    {
        lhs = multiple(lhs, rhs);
        return lhs;
    }
    /// @brief Multiple vector by matrix
    template <class T, class Q, uint N, uint M>
    Vector<Q, M> operator * (const Vector<Q, N>& lhs, const Matrix<T, N, M>& rhs)
    {
        Vector<Q, M> ret;
        for (uint i = 0; i < M; ++i)
        {
            Q value = 0;
            for (uint j = 0; j < N; ++j)
                value += static_cast<Q>(lhs[j] * rhs.element(j, i));
            ret[i] = value;
        }
        return ret;
    }
    /// @brief Multiple matrix by vector
    template <class T, class Q, uint N, uint M>
    Vector<Q, N> operator * (const Matrix<T, N, M>& lhs, const Vector<Q, M>& rhs)
    {
        Vector<Q, N> ret;
        for (uint i = 0; i < N; ++i)
        {
            Q value = 0;
            for (uint j = 0; j < M; ++j)
                value += static_cast<Q>(rhs[j] * lhs.element(i, j));
            ret[i] = value;
        }
        return ret;
    }
    /// @brief Multiple matrix by scalar
    template <class T, uint N, uint M>
    Matrix<T, N, M>& operator *= (Matrix<T, N, M>& lhs, const T& rhs) noexcept
    {
        for (uint i = 0; i < N * M; i++)
            lhs.m[i] *= rhs;
        return lhs;
    }
    /// @brief Multiple matrix by scalar
    template <class T, uint N, uint M>
    Matrix<T, N, M> operator * (Matrix<T, N, M> lhs, const T& rhs)
    {
        lhs *= rhs;
        return lhs;
    }
    /// @brief Multiple scalar by matrix
    template <class T, uint N, uint M>
    Matrix<T, N, M> operator * (const T& lhs, Matrix<T, N, M> rhs)
    {
        rhs *= lhs;
        return rhs;
    }
    /// @brief Divide matrix by matrix
    template <class T, uint N>
    Matrix<T, N, N> operator / (Matrix<T, N, N> lhs, const Matrix<T, N, N>& rhs)
    {
        lhs *= inverse(rhs);
        return lhs;
    }
    /// @brief Divide matrix by matrix
    template <class T, uint N>
    Matrix<T, N, N>& operator /= (Matrix<T, N, N>& lhs, const Matrix<T, N, N>& rhs)
    {
        lhs *= inverse(rhs);
        return lhs;
    }
    /// @brief Divide matrix by scalar
    template <class T, uint N, uint M>
    Matrix<T, N, M>& operator /= (Matrix<T, N, M>& lhs, const T& rhs) noexcept
    {
        lhs *= T(1) / rhs;
        return lhs;
    }
    /// @brief Divide matrix by scalar
    template <class T, uint N, uint M>
    Matrix<T, N, M> operator / (Matrix<T, N, M> lhs, const T& rhs) noexcept
    {
        lhs /= rhs;
        return lhs;
    }
    /// @brief Subtract matrix from matrix
    template <class T, uint N, uint M>
    Matrix<T, N, M>& operator -= (Matrix<T, N, M>& lhs, const Matrix<T, N, M>& rhs) noexcept
    {
        for (uint i = 0; i < N * M; i++)
            lhs.m[i] -= rhs.m[i];
        return lhs;
    }
    /// @brief Subtract matrix from matrix
    template <class T, uint N, uint M>
    Matrix<T, N, M> operator - (Matrix<T, N, M> lhs, const Matrix<T, N, M>& rhs) noexcept
    {
        lhs -= rhs;
        return lhs;
    }
    /// @brief mat + mat
    template <class T, uint N, uint M>
    Matrix<T, N, M>& operator += (Matrix<T, N, M>& lhs, const Matrix<T, N, M>& rhs) noexcept
    {
        for (uint i = 0; i < N * M; i++)
            lhs.m[i] += rhs.m[i];
        return lhs;
    }
    /// @brief mat + mat
    template <class T, uint N, uint M>
    Matrix<T, N, M> operator + (Matrix<T, N, M> lhs, const Matrix<T, N, M>& rhs) noexcept
    {
        lhs += rhs;
        return lhs;
    }
    /// @brief Is equal?
    template <class T, uint N, uint M>
    bool operator == (const Matrix<T, N, M>& lhs, const Matrix<T, N, M>& rhs) noexcept
    {
        for (uint i = 0; i < N * M; i++)
        {
            if (lhs.m[i] != rhs.m[i])
                return false;
        }
        return true;
    }
    /// @brief Is not equal?
    template <class T, uint N, uint M>
    bool operator != (const Matrix<T, N, M>& lhs, const Matrix<T, N, M>& rhs) noexcept
    {
        return !(lhs == rhs);
    }
    /// @brief Transpose matrix
    template <class T, uint N, uint M>
    Matrix<T, M, N> transpose(const Matrix<T, N, M>& lhs)
    {
        Matrix<T, M, N> ret;
        for (uint i = 0; i < M; ++i)
            for (uint j = 0; j < N; ++j)
            {
                ret.element(i, j) = lhs.element(j, i);
            }
        return ret;
    }
    /// @brief Inverse matrix 4x4
    template <class T>
    Matrix<T, 4, 4> inverse(const Matrix<T, 4, 4>& lhs)
    {
        Matrix<T, 4, 4> ret;
        ret[0] = lhs[5] * lhs[10] * lhs[15] - lhs[5] * lhs[11] * lhs[14] - lhs[9] * lhs[6] * lhs[15] +
            lhs[9] * lhs[7] * lhs[14] + lhs[13] * lhs[6] * lhs[11] - lhs[13] * lhs[7] * lhs[10];
        ret[4] = -lhs[4] * lhs[10] * lhs[15] + lhs[4] * lhs[11] * lhs[14] + lhs[8] * lhs[6] * lhs[15] -
            lhs[8] * lhs[7] * lhs[14] - lhs[12] * lhs[6] * lhs[11] + lhs[12] * lhs[7] * lhs[10];
        ret[8] = lhs[4] * lhs[9] * lhs[15] - lhs[4] * lhs[11] * lhs[13] - lhs[8] * lhs[5] * lhs[15] +
            lhs[8] * lhs[7] * lhs[13] + lhs[12] * lhs[5] * lhs[11] - lhs[12] * lhs[7] * lhs[9];
        ret[12] = -lhs[4] * lhs[9] * lhs[14] + lhs[4] * lhs[10] * lhs[13] + lhs[8] * lhs[5] * lhs[14] -
            lhs[8] * lhs[6] * lhs[13] - lhs[12] * lhs[5] * lhs[10] + lhs[12] * lhs[6] * lhs[9];
        ret[1] = -lhs[1] * lhs[10] * lhs[15] + lhs[1] * lhs[11] * lhs[14] + lhs[9] * lhs[2] * lhs[15] -
            lhs[9] * lhs[3] * lhs[14] - lhs[13] * lhs[2] * lhs[11] + lhs[13] * lhs[3] * lhs[10];
        ret[5] = lhs[0] * lhs[10] * lhs[15] - lhs[0] * lhs[11] * lhs[14] - lhs[8] * lhs[2] * lhs[15] +
            lhs[8] * lhs[3] * lhs[14] + lhs[12] * lhs[2] * lhs[11] - lhs[12] * lhs[3] * lhs[10];
        ret[9] = -lhs[0] * lhs[9] * lhs[15] + lhs[0] * lhs[11] * lhs[13] + lhs[8] * lhs[1] * lhs[15] -
            lhs[8] * lhs[3] * lhs[13] - lhs[12] * lhs[1] * lhs[11] + lhs[12] * lhs[3] * lhs[9];
        ret[13] = lhs[0] * lhs[9] * lhs[14] - lhs[0] * lhs[10] * lhs[13] - lhs[8] * lhs[1] * lhs[14] +
            lhs[8] * lhs[2] * lhs[13] + lhs[12] * lhs[1] * lhs[10] - lhs[12] * lhs[2] * lhs[9];
        ret[2] = lhs[1] * lhs[6] * lhs[15] - lhs[1] * lhs[7] * lhs[14] - lhs[5] * lhs[2] * lhs[15] +
            lhs[5] * lhs[3] * lhs[14] + lhs[13] * lhs[2] * lhs[7] - lhs[13] * lhs[3] * lhs[6];
        ret[6] = -lhs[0] * lhs[6] * lhs[15] + lhs[0] * lhs[7] * lhs[14] + lhs[4] * lhs[2] * lhs[15] -
            lhs[4] * lhs[3] * lhs[14] - lhs[12] * lhs[2] * lhs[7] + lhs[12] * lhs[3] * lhs[6];
        ret[10] = lhs[0] * lhs[5] * lhs[15] - lhs[0] * lhs[7] * lhs[13] - lhs[4] * lhs[1] * lhs[15] +
            lhs[4] * lhs[3] * lhs[13] + lhs[12] * lhs[1] * lhs[7] - lhs[12] * lhs[3] * lhs[5];
        ret[14] = -lhs[0] * lhs[5] * lhs[14] + lhs[0] * lhs[6] * lhs[13] + lhs[4] * lhs[1] * lhs[14] -
            lhs[4] * lhs[2] * lhs[13] - lhs[12] * lhs[1] * lhs[6] + lhs[12] * lhs[2] * lhs[5];
        ret[3] = -lhs[1] * lhs[6] * lhs[11] + lhs[1] * lhs[7] * lhs[10] + lhs[5] * lhs[2] * lhs[11] -
            lhs[5] * lhs[3] * lhs[10] - lhs[9] * lhs[2] * lhs[7] + lhs[9] * lhs[3] * lhs[6];
        ret[7] = lhs[0] * lhs[6] * lhs[11] - lhs[0] * lhs[7] * lhs[10] - lhs[4] * lhs[2] * lhs[11] +
            lhs[4] * lhs[3] * lhs[10] + lhs[8] * lhs[2] * lhs[7] - lhs[8] * lhs[3] * lhs[6];
        ret[11] = -lhs[0] * lhs[5] * lhs[11] + lhs[0] * lhs[7] * lhs[9] + lhs[4] * lhs[1] * lhs[11] -
            lhs[4] * lhs[3] * lhs[9] - lhs[8] * lhs[1] * lhs[7] + lhs[8] * lhs[3] * lhs[5];
        ret[15] = lhs[0] * lhs[5] * lhs[10] - lhs[0] * lhs[6] * lhs[9] - lhs[4] * lhs[1] * lhs[10] +
            lhs[4] * lhs[2] * lhs[9] + lhs[8] * lhs[1] * lhs[6] - lhs[8] * lhs[2] * lhs[5];

        T det = (T) (1.0 / (lhs[0] * ret[0] + lhs[1] * ret[4] + lhs[2] * ret[8] + lhs[3] * ret[12]));
        return ret * det;
    }
    /// @brief Is finite?
    template <class T, uint N, uint M>
    bool isFinite(const Matrix<T, N, M>& lhs)
    {
        for (uint i = 0; i < N * M; ++i)
            if (!isFinite(lhs.m[i]))
                return false;
        return true;
    }

    /// @brief 2x2 Matrix
    template <class T>
    using Matrix2x2 = Matrix<T, 2, 2>;
    /// @brief 3x3 Matrix
    template <class T>
    using Matrix3x3 = Matrix<T, 3, 3>;
    /// @brief 4x4 Matrix
    template <class T>
    using Matrix4x4 = Matrix<T, 4, 4>;
    /// @brief Transform 3D vector, w-component is 1
    template <class T, class Q>
    Vector<Q, 3> operator * (const Vector<Q, 3>& lhs, const Matrix4x4<T>& rhs)
    {
        Vector<Q, 4> res = Vector<Q, 4>(lhs, 1) * rhs;
        return Vector<Q, 3>(res) / res.w;
    }
    /// @brief Transform 3D vector, w-component is 1
    template <class T, class Q>
    Vector<Q, 3> operator * (const Matrix4x4<T>& lhs, const Vector<Q, 3>& rhs)
    {
        Vector<Q, 4> res = lhs * Vector<Q, 4>(rhs, 1);
        return Vector<Q, 3>(res) / res.w;
    }
    /// @}
}
