/// @file ls/math/common.h
/// @brief Common math functions templates
#pragma once

#include "ls/common/import.h"
#include "ls/common/concept.h"

namespace ls
{
    /// @addtogroup LsMath
    /// @{

    /// @brief Pi value getter
    template <class Scalar>
    struct Pi
    {
        /// @brief Pi value
        static const Scalar value;
    };
    template <class Scalar>
    const Scalar Pi<Scalar>::value = static_cast<Scalar>(3.1415926535897932384626433832795);
    /// @brief Float pi
    static const float pi = Pi<float>::value;

    /// @brief Get epsilon
    inline float epsilon()
    {
        return std::numeric_limits<float>::epsilon();
    }
    /// @brief Check whether scalars are almost equal
    template <class Scalar>
    bool almostEqual(const Scalar lhs, const Scalar rhs, 
                     const Scalar eps = static_cast<Scalar>(0.00001))
    {
        return abs(lhs - rhs) < eps;
    }
    /// @brief Round scalar to integer
    template <class Output, class Input, class = EnableIfScalar<Input>>
    Output roundi(const Input x)
    {
        return static_cast<Output>(floor(x + 0.5));
    }
    /// @brief Round scalar to integer
    template <class Scalar, class = EnableIfScalar<Scalar>>
    Scalar round(const Scalar x)
    {
        return static_cast<Scalar>(floor(x + 0.5));
    }
    /// @brief Round scalar to base
    template <class Scalar, class = EnableIfScalar<Scalar>>
    Scalar roundn(const Scalar x, const Scalar base)
    {
        return static_cast<Scalar>(floor(x / base + 0.5) * base);
    }
    /// @brief Get fractional part of number
    template <class Scalar, class = EnableIfScalar<Scalar>>
    Scalar fract(const Scalar value)
    {
        return value - floor(value);
    }
    /// @brief Check whether scalar is finite
    template <class Scalar, class = EnableIfScalar<Scalar>>
    bool isFinite(const Scalar value)
    {
        return std::isfinite(value);
    }
    /// @brief From degrees to radians
    template <class Scalar, class = EnableIfFloat<Scalar>>
    Scalar deg2rad(const Scalar x)
    {
        return Scalar(x * Pi<double>::value / 180.0);
    }
    /// @brief From radians to degrees
    template <class Scalar, class = EnableIfFloat<Scalar>>
    Scalar rad2deg(const Scalar x)
    {
        return Scalar(x * 180.0 / Pi<double>::value);
    }

    /// @brief Linear interpolation
    template <class Vector, class Scalar, class = EnableIfScalar<Scalar>>
    Vector lerp(const Vector& a, const Vector& b, const Scalar factor)
    {
        return Vector(a * (1 - factor) + b * factor);
    }
    /// @brief Clamp number
    template <class Scalar, class = EnableIfScalar<Scalar>>
    Scalar clamp(const Scalar value, const Scalar min, const Scalar max)
    {
        if (value < min) return min;
        if (value > max) return max;
        return value;
    }
    /// @brief Smooth interpolation of number.
    template <class Scalar, class = EnableIfScalar<Scalar>>
    Scalar unlerp(const Scalar min, const Scalar max, const Scalar value)
    {
        return clamp((value - min) / (max - min), 
                     static_cast<Scalar>(0), 
                     static_cast<Scalar>(1));
    }
    /// @brief Round @b integer to previous power-of-two.
    template <class Integer>
    Integer floor2p(const Integer value)
    {
        Integer a = value;
        const Integer m = 1;
        while (a & (a + 1))
            a = a | (a >> m);
        a = a - (a >> 1);
        return a;
    }
    /// @brief Round @b integer to next power-of-two.
    template <class Integer>
    Integer ceil2p(Integer value)
    {
        Integer a = value - 1;
        const Integer m = 1;
        while (a & (a + 1))
            a = a | (a >> m);
        return a + 1;
    }

    /// @brief Convert float/double/long double @a real in [-1, 1] -> signed int8/16/32
    template <class Integer, class Real>
    Integer real2snorm(const Real real)
    {
        static const Integer Min = numeric_limits<Integer>::min();
        static const Integer Max = numeric_limits<Integer>::max();

        const Real a = clamp(real, Real(-1), Real(1));
        const Integer b = a >= 0
            ? static_cast<Integer>(min<uint>(static_cast<uint>(a * Max), Max))
            : static_cast<Integer>(max<uint>(static_cast<uint>(-a * Min), Min));

        return b;
    }
    /// @brief Convert signed 8/16/32 @a integer -> signed float/double/long double [-1, 1]
    template <class Real, class Integer>
    Real snorm2real(const Integer integer)
    {
        static const Integer Min = numeric_limits<Integer>::min();
        static const Integer Max = numeric_limits<Integer>::max();

        return integer > 0
            ? static_cast<Real>(integer / static_cast<double>(Max))
            : -static_cast<Real>(integer / static_cast<double>(Min));
    }
    /// @brief Convert float/double/long double @a real in [0, 1] -> unsigned int8/16/32
    template <class Integer, class Real>
    Integer real2unorm(const Real real)
    {
        static const Integer Max = numeric_limits<Integer>::max();

        const double a = clamp<double>(real, 0.0, 1.0) * Max;
        return static_cast<Integer>(a);
    }
    /// @brief Convert unsigned 8/16/32 @a integer -> signed float/double/long double [0, 1]
    template <class Real, class Integer>
    Real unorm2real(const Integer integer)
    {
        static const Integer Max = numeric_limits<Integer>::max();

        return static_cast<Real>(integer / static_cast<double>(Max));
    }
    /// @brief Convert @a real to normalized integer (signed)
    template <class Integer, class Real>
    EnableIfSigned<Integer, Integer> real2norm(const Real real)
    {
        return real2snorm<Integer, Real>(real);
    }
    /// @brief Convert @a real to normalized integer (unsigned)
    template <class Integer, class Real>
    EnableIfUnsigned<Integer, Integer> real2norm(const Real real)
    {
        return real2unorm<Integer, Real>(real);
    }
    /// @brief Convert normalized @a integer to real (signed)
    template <class Real, class Integer>
    EnableIfSigned<Integer, Real> norm2real(const Integer integer)
    {
        return snorm2real<Real, Integer>(integer);
    }
    /// @brief Convert normalized @a integer to real (unsigned)
    template <class Real, class Integer>
    EnableIfUnsigned<Integer, Real> norm2real(const Integer integer)
    {
        return unorm2real<Real, Integer>(integer);
    }
    /// @brief Convert array object (such as vector) from @a Input type to @a Output
    template <class Output, class Input>
    Output convert(const Input& lhs)
    {
        static const int inSize = Input::size;
        static const int outSize = Output::size;
        static const int size = std::min(inSize, outSize);
        Output ret;
        for (int i = 0; i < size; ++i)
            ret[i] = typename Output::Scalar(lhs[i]);
        return ret;
    }
    /// @brief Floor to zero
    template <class Any>
    Any floor0(const Any& value)
    {
        if (value < 0.0)
            return ceil(value);
        else
            return floor(value);
    }
    /// @brief Ceil from zero
    template <class Any>
    Any ceil0(const Any& value)
    {
        if (value > 0.0)
            return ceil(value);
        else
            return floor(value);
    }
    /// @brief Integer log 2
    template <class Integer, class = enable_if_t<is_unsigned<Integer>::value>>
    Integer log2u(Integer value)
    {
        uint power = 0;
        while (value >>= 1)
            ++power;
        return power;
    }
    /// @}
}
