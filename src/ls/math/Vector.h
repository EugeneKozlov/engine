/// @file ls/math/Vector.h
/// @brief Vectors templates
#pragma once

#include "ls/common/assert.h"
#include "ls/common/concept.h"
#include "ls/common/integer.h"

namespace ls
{
    /// @addtogroup LsMath
    /// @{

    /// @brief N-D Vector
    template <class T, uint N>
    class Vector;
    /// @brief Dot product
    template <class T, uint N>
    T dot(const Vector<T, N>& lhs, const Vector<T, N>& rhs)
    {
        T ret = 0;
        for (uint i = 0; i < N; ++i)
            ret += lhs[i] * rhs[i];
        return ret;
    }
    /// @brief Project vector on axis
    template <class T, uint N>
    T project(const Vector<T, N>& axis, const Vector<T, N>& vector)
    {
        return dot(vector, axis) / axis.length();
    }
    /// @brief 2D Vector
    template <class T>
    class Vector<T, 2>
        : PrintableConcept
    {
    public:
        /// @brief Print
        void toString(String& str) const noexcept
        {
            str.print('(', x, ',', y, ')');
        }
    public:
        /// @brief Value type
        typedef T Scalar;
        /// @brief size
        static const uint size = 2;
        /// @brief Min vector
        static Vector<Scalar, size> min() noexcept
        {
            return Vector<Scalar, size>(numeric_limits<Scalar>::lowest());
        }
        /// @brief Max vector
        static Vector<Scalar, size> max() noexcept
        {
            return Vector<Scalar, size>(numeric_limits<Scalar>::max());
        }

        /// @brief By scalar
        Vector(const Scalar t = 0) noexcept
            : x(t)
            , y(t)
        {
        }
        /// @brief By scalar array
        Vector(const Scalar tp[2]) noexcept
            : x(tp[0])
            , y(tp[1])
        {
        }
        /// @brief By components
        Vector(const Scalar x, const Scalar y) noexcept
            : x(x)
            , y(y)
        {
        }
        /// @brief By another type
        template <class Q>
        explicit Vector(const Vector<Q, 2>& v)
            : Vector(static_cast<Scalar>(v.x), static_cast<Scalar>(v.y))
        {
        }
        /// @brief By first two components of 3D Vector
        explicit Vector(const Vector<Scalar, 3>& u) noexcept
            : x(u.x)
            , y(u.y)
        {
        }
        /// @brief By first two components of 4D Vector
        explicit Vector(const Vector<Scalar, 4>& u) noexcept
            : x(u.x)
            , y(u.y)
        {
        }
        /// @brief By iterators
        template <class Iterator, class = decltype(*std::declval<Iterator>())>
        Vector(Iterator begin, Iterator end) noexcept
        {
            debug_assert(begin != end);
            x = *begin++;
            debug_assert(begin != end);
            y = *begin++;
        }
        /// @brief By range
        template <class Iterator, class = decltype(*std::declval<Iterator>())>
        Vector(pair<Iterator, Iterator> range) noexcept
            : Vector(range.first, range.second)
        {
        }
        /// @brief By init list
        Vector(initializer_list<Scalar> tp) noexcept
            : Vector(tp.begin(), tp.end())
        {
        }
        /// @brief Lerp
        Vector(const Vector& first, const Vector& second, const Scalar factor) noexcept
            : Vector(first * (1 - factor) + second * factor)
        {
        }

        /// @brief Get pointer
        Scalar* value() noexcept
        {
            return (&x);
        }
        /// @brief Get length^2
        Scalar length2() const noexcept
        {
            return dot(*this, *this);
        }
        /// @brief Get length
        Scalar length() const noexcept
        {
            return sqrt(length2());
        }
        /// @brief Get constant pointer
        const Scalar* value() const noexcept
        {
            return (&x);
        }
        /// @brief Get element
        Scalar& operator [](const uint i) noexcept
        {
            return value()[i];
        }
        /// @brief Get constant element
        const Scalar& operator [](const uint i) const noexcept
        {
            return value()[i];
        }
    public:
        /// @brief x-component
        Scalar x = 0;
        /// @brief y-component
        Scalar y = 0;
    };
    /// @brief 3D Vector
    template <class T>
    class Vector<T, 3>
        : PrintableConcept
    {
    public:
        /// @brief Print
        void toString(String& str) const noexcept
        {
            str.print('(', x, ',', y, ',', z, ')');
        }
    public:
        /// @brief Value type
        typedef T Scalar;
        /// @brief size
        static const uint size = 3;
        /// @brief Min vector
        static Vector<Scalar, size> min() noexcept
        {
            return Vector<Scalar, size>(numeric_limits<Scalar>::min());
        }
        /// @brief Max vector
        static Vector<Scalar, size> max() noexcept
        {
            return Vector<Scalar, size>(numeric_limits<Scalar>::max());
        }

        /// @brief By scalar
        Vector(const Scalar t = 0) noexcept
            : x(t)
            , y(t)
            , z(t)
        {
        }
        /// @brief By scalar array
        Vector(const Scalar tp[3]) noexcept
            : x(tp[0])
            , y(tp[1])
            , z(tp[2])
        {
        }
        /// @brief By components
        Vector(const Scalar x, const Scalar y, const Scalar z) noexcept
            : x(x)
            , y(y)
            , z(z)
        {
        }
        /// @brief By another type
        template <class Q>
        explicit Vector(const Vector<Q, 3>& v)
            : Vector(static_cast<Scalar>(v.x), static_cast<Scalar>(v.y), static_cast<Scalar>(v.z))
        {
        }
        /// @brief By 2D Vector and #z component
        Vector(const Vector<Scalar, 2> &v, const Scalar z) noexcept
            : x(v.x)
            , y(v.y)
            , z(z)
        {
        }
        /// @brief By #x component and 2D Vector
        Vector(Scalar x, const Vector<Scalar, 2> &v) noexcept
            : x(x)
            , y(v.x)
            , z(v.y)
        {
        }
        /// @brief By #x, #y and #z components of 4D Vector
        explicit Vector(const Vector<Scalar, 4> &u) noexcept
            : x(u.x)
            , y(u.y)
            , z(u.z)
        {
        }
        /// @brief By iterators
        template <class Iterator>
        Vector(Iterator begin, Iterator end)
        {
            debug_assert(begin != end);
            x = *begin++;
            debug_assert(begin != end);
            y = *begin++;
            debug_assert(begin != end);
            z = *begin++;
        }
        /// @brief By range
        template <class Iterator, class = decltype(*std::declval<Iterator>())>
        Vector(pair<Iterator, Iterator> range) noexcept
            : Vector(range.first, range.second)
        {
        }
        /// @brief By init list
        Vector(initializer_list<Scalar> tp)
            : Vector(tp.begin(), tp.end())
        {
        }
        /// @brief Lerp
        Vector(const Vector& first, const Vector& second, Scalar factor) noexcept
            : Vector(first * (1 - factor) + second * factor)
        {
        }

        /// @brief Get pointer
        Scalar* value() noexcept
        {
            return (&x);
        }
        /// @brief Get constant pointer
        const Scalar* value() const noexcept
        {
            return (&x);
        }
        /// @brief Get length^2
        Scalar length2() const noexcept
        {
            return dot(*this, *this);
        }
        /// @brief Get length
        Scalar length() const noexcept
        {
            return sqrt(length2());
        }
        /// @brief Get element
        Scalar& operator [] (const size_t i) noexcept
        {
            return value()[i];
        }
        /// @brief Get constant element
        const Scalar& operator [] (const size_t i) const noexcept
        {
            return value()[i];
        }
    public:
        /// @brief x-component
        Scalar x = 0;
        /// @brief y-component
        Scalar y = 0;
        /// @brief z-component
        Scalar z = 0;
    };
    /// @brief 4D Vector
    template <class T>
    class Vector<T, 4>
        : PrintableConcept
    {
    public:
        /// @brief Print
        void toString(String& str) const noexcept
        {
            str.print('(', x, ',', y, ',', z, ',', w, ')');
        }
    public:
        /// @brief Value type
        typedef T Scalar;
        /// @brief size
        static const uint size = 4;
        /// @brief Min vector
        static Vector<Scalar, size> min() noexcept
        {
            return Vector<Scalar, size>(numeric_limits<Scalar>::min());
        }
        /// @brief Max vector
        static Vector<Scalar, size> max() noexcept
        {
            return Vector<Scalar, size>(numeric_limits<Scalar>::max());
        }

        /// @brief By scalar
        Vector(const Scalar t = 0) noexcept
            : x(t)
            , y(t)
            , z(t)
            , w(t)
        {
        }
        /// @brief By scalar array
        Vector(const Scalar tp[4]) noexcept
            : x(tp[0])
            , y(tp[1])
            , z(tp[2])
            , w(tp[3])
        {
        }
        /// @brief By components
        Vector(const Scalar x, const Scalar y, const Scalar z, const Scalar w) noexcept
            : x(x)
            , y(y)
            , z(z)
            , w(w)
        {
        }
        /// @brief By another type
        template <class Q>
        explicit Vector(const Vector<Q, 4>& v)
            : Vector(static_cast<Scalar>(v.x), static_cast<Scalar>(v.y), 
                     static_cast<Scalar>(v.z), static_cast<Scalar>(v.w))
        {
        }
        /// @brief By 3D Vector and #w component
        Vector(const Vector<Scalar, 3>& u0, const Scalar w) noexcept
            : x(u0.x)
            , y(u0.y)
            , z(u0.z)
            , w(w)
        {
        }
        /// @brief By #x component and 3D Vector.
        Vector(const Scalar x, const Vector<Scalar, 3>& u0) noexcept
            : x(x)
            , y(u0.x)
            , z(u0.y)
            , w(u0.z)
        {
        }
        /// @brief By 2D Vector and #z,#w component.
        Vector(const Vector<Scalar, 2>& u0, const Scalar z, const Scalar w) noexcept
            : x(u0.x)
            , y(u0.y)
            , z(z)
            , w(w)
        {
        }
        /// @brief By 2D Vector and #x,#w component.
        explicit Vector(const Scalar x, const Vector<Scalar, 2>& u0, const Scalar w) noexcept
            : x(x)
            , y(u0.x)
            , z(u0.y)
            , w(w)
        {
        }
        /// @brief By #x,#y component and 2D Vector.
        Vector(const Scalar x, const Scalar y, const Vector<Scalar, 2>& u0) noexcept
            : x(x)
            , y(y)
            , z(u0.x)
            , w(u0.y)
        {
        }
        /// @brief By two 2D Vectors.
        Vector(const Vector<Scalar, 2>& u0, const Vector<Scalar, 2>& u1) noexcept
            : x(u0.x)
            , y(u0.y)
            , z(u1.x)
            , w(u1.y)
        {
        }
        /// @brief By iterators
        template <class Iterator>
        Vector(Iterator begin, Iterator end)
        {
            debug_assert(begin != end);
            x = *begin++;
            debug_assert(begin != end);
            y = *begin++;
            debug_assert(begin != end);
            z = *begin++;
            debug_assert(begin != end);
            w = *begin++;
        }
        /// @brief By range
        template <class Iterator, class = decltype(*std::declval<Iterator>())>
        Vector(pair<Iterator, Iterator> range) noexcept
            : Vector(range.first, range.second)
        {
        }
        /// @brief By init list
        Vector(initializer_list<Scalar> tp)
            : Vector(tp.begin(), tp.end())
        {
        }
        /// @brief Lerp
        Vector(const Vector& first, const Vector& second, Scalar factor) noexcept
            : Vector(first * (1 - factor) + second * factor)
        {
        }

        /// @brief Get pointer
        Scalar* value() noexcept
        {
            return (&x);
        }
        /// @brief Get constant pointer
        const Scalar* value() const noexcept
        {
            return (&x);
        }
        /// @brief Get length^2
        Scalar length2() const noexcept
        {
            return dot(*this, *this);
        }
        /// @brief Get length
        Scalar length() const noexcept
        {
            return sqrt(length2());
        }
        /// @brief Get element
        Scalar& operator [] (uint i) noexcept
        {
            return value()[i];
        }
        /// @brief Get constant element
        const Scalar& operator [] (uint i) const noexcept
        {
            return value()[i];
        }
    public:
        /// @brief x-component
        Scalar x = 0;
        /// @brief y-component
        Scalar y = 0;
        /// @brief z-component
        Scalar z = 0;
        /// @brief w-component
        Scalar w = 0;
    };
    /// @brief vec * vec (component-wise)
    template <class T, uint N>
    Vector<T, N>&
    operator *= (Vector<T, N>& lhs, const Vector<T, N>& rhs) noexcept
    {
        for (uint i = 0; i < N; i++)
            lhs[i] *= rhs[i];
        return lhs;
    }
    /// @brief vec / vec (component-wise)
    template <class T, uint N>
    Vector<T, N>&
    operator /= (Vector<T, N>& lhs, const Vector<T, N>& rhs) noexcept
    {
        for (uint i = 0; i < N; i++)
            lhs[i] /= rhs[i];
        return lhs;
    }
    /// @brief vec mod vec (component-wise)
    template <class T, uint N>
    EnableIfFloat<T, Vector<T, N>&>
    operator %= (Vector<T, N>& lhs, const Vector<T, N>& rhs) noexcept
    {
        for (uint i = 0; i < N; i++)
            lhs[i] = fmod(lhs[i], rhs[i]);
        return lhs;
    }
    /// @brief vec mod vec (component-wise)
    template <class T, uint N>
    EnableIfInteger<T, Vector<T, N>&>
    operator %= (Vector<T, N>& lhs, const Vector<T, N>& rhs) noexcept
    {
        for (uint i = 0; i < N; i++)
            lhs[i] %= rhs[i];
        return lhs;
    }
    /// @brief vec + vec
    template <class T, uint N>
    Vector<T, N>&
    operator += (Vector<T, N>& lhs, const Vector<T, N>& rhs) noexcept
    {
        for (uint i = 0; i < N; i++)
            lhs[i] += rhs[i];
        return lhs;
    }
    /// @brief vec - vec
    template<class T, uint N>
    Vector<T, N>&
    operator -= (Vector<T, N>& lhs, const Vector<T, N>& rhs) noexcept
    {
        for (uint i = 0; i < N; i++)
            lhs[i] -= rhs[i];
        return lhs;
    }
    /// @brief vec * scalar
    template <class T, uint N, class Scalar>
    Vector<T, N>&
    operator *= (Vector<T, N>& lhs, const Scalar rhs) noexcept
    {
        using S = typename Vector<T, N>::Scalar;
        for (uint i = 0; i < N; i++)
            lhs[i] = S(lhs[i] * rhs);
        return lhs;
    }
    /// @brief vec / scalar
    template <class T, uint N, class Scalar>
    Vector<T, N>&
    operator /= (Vector<T, N>& lhs, const Scalar rhs) noexcept
    {
        using S = typename Vector<T, N>::Scalar;
        for (uint i = 0; i < N; i++)
            lhs[i] = S(lhs[i] / rhs);
        return lhs;
    }
    /// @brief vec mod scalar
    template <class T, uint N>
    EnableIfFloat<T, Vector<T, N>&>
    operator %= (Vector<T, N>& lhs, const typename Vector<T, N>::Scalar rhs) noexcept
    {
        for (uint i = 0; i < N; i++)
            lhs[i] = fmod(lhs[i], rhs);
        return lhs;
    }
    /// @brief vec mod scalar
    template <class T, uint N>
    EnableIfInteger<T, Vector<T, N>&>
    operator %= (Vector<T, N>& lhs, const typename Vector<T, N>::Scalar rhs) noexcept
    {
        for (uint i = 0; i < N; i++)
            lhs[i] %= rhs;
        return lhs;
    }
    /// @brief vec + scalar
    template <class T, uint N>
    Vector<T, N>&
    operator += (Vector<T, N>& lhs, const typename Vector<T, N>::Scalar rhs) noexcept
    {
        for (uint i = 0; i < N; i++)
            lhs[i] += rhs;
        return lhs;
    }
    /// @brief vec - scalar
    template <class T, uint N>
    Vector<T, N>&
    operator -= (Vector<T, N>& lhs, const typename Vector<T, N>::Scalar rhs) noexcept
    {
        for (uint i = 0; i < N; i++)
            lhs[i] -= rhs;
        return lhs;
    }

    /// @brief vec + vec
    template <class T, uint N>
    Vector<T, N>
    operator + (const Vector<T, N>& lhs, const Vector<T, N>& rhs) noexcept
    {
        Vector<T, N> rt(lhs);
        return rt += rhs;
    }
    /// @brief vec - vec
    template <class T, uint N>
    Vector<T, N>
    operator - (const Vector<T, N>& lhs, const Vector<T, N>& rhs) noexcept
    {
        Vector<T, N> rt(lhs);
        return rt -= rhs;
    }
    /// @brief vec * vec (component-wise)
    template <class T, uint N>
    Vector<T, N>
    operator * (const Vector<T, N>& lhs, const Vector<T, N>& rhs) noexcept
    {
        Vector<T, N> rt(lhs);
        return rt *= rhs;
    }
    /// @brief vec / vec (component-wise)
    template <class T, uint N>
    Vector<T, N>
    operator / (const Vector<T, N>& lhs, const Vector<T, N>& rhs) noexcept
    {
        Vector<T, N> rt(lhs);
        return rt /= rhs;
    }
    /// @brief vec mod vec (component-wise)
    template <class T, uint N>
    Vector<T, N>
    operator % (const Vector<T, N>& lhs, const Vector<T, N>& rhs) noexcept
    {
        Vector<T, N> rt(lhs);
        return rt %= rhs;
    }
    /// @brief Compare equal
    template <class T, uint N>
    bool
    operator == (const Vector<T, N>& lhs, const Vector<T, N>& rhs) noexcept
    {
        bool r = true;
        for (uint i = 0; i < N; i++)
            r &= lhs[i] == rhs[i];
        return r;
    }
    /// @brief Compare not equal
    template <class T, uint N>
    bool
    operator != (const Vector<T, N>& lhs, const Vector<T, N>& rhs) noexcept
    {
        return !(lhs == rhs);
    }
    /// @brief Less
    template <class T, uint N>
    bool
    operator < (const Vector<T, N>& lhs, const Vector<T, N>& rhs) noexcept
    {
        uint s = N;
        for (uint i = 0; i < s; i++)
            if (lhs[i] < rhs[i])
                return true;
            else if (rhs[i] < lhs[i])
                return false;
        return lhs[s - 1] < rhs[s - 1];
    }

    /// @brief vec * scalar
    template <class T, uint N, class Scalar>
    EnableIfScalar<Scalar, Vector<T, N>>
    operator * (const Vector<T, N>& lhs, const Scalar rhs) noexcept
    {
        Vector<T, N> rt(lhs);
        return rt *= rhs;
    }
    /// @brief scalar * vec
    template <class T, uint N, class Scalar>
    EnableIfScalar<Scalar, Vector<T, N>>
    operator * (const Scalar rhs, const Vector<T, N>& lhs) noexcept
    {
        Vector<T, N> rt(lhs);
        return rt *= rhs;
    }
    /// @brief vec + scalar
    template <class T, uint N>
    Vector<T, N>
    operator + (const Vector<T, N>& lhs, const typename Vector<T, N>::Scalar rhs) noexcept
    {
        Vector<T, N> rt(lhs);
        return rt += rhs;
    }
    /// @brief scalar + vec
    template <class T, uint N>
    Vector<T, N>
    operator + (const typename Vector<T, N>::Scalar rhs, const Vector<T, N>& lhs) noexcept
    {
        Vector<T, N> rt(lhs);
        return rt += rhs;
    }
    /// @brief vec - scalar
    template <class T, uint N>
    Vector<T, N>
    operator - (const Vector<T, N>& lhs, const typename Vector<T, N>::Scalar rhs) noexcept
    {
        Vector<T, N> rt(lhs);
        return rt -= rhs;
    }
    /// @brief scalar - vec
    template <class T, uint N>
    Vector<T, N>
    operator - (const typename Vector<T, N>::Scalar rhs, const Vector<T, N>& lhs) noexcept
    {
        Vector<T, N> rt(lhs);
        return rt -= rhs;
    }
    /// @brief vec / scalar
    template <class T, uint N>
    Vector<T, N>
    operator / (const Vector<T, N>& lhs, const typename Vector<T, N>::Scalar rhs) noexcept
    {
        Vector<T, N> rt(lhs);
        return rt /= rhs;
    }
    /// @brief vec mod scalar
    template <class T, uint N>
    Vector<T, N>
    operator % (const Vector<T, N>& lhs, const typename Vector<T, N>::Scalar rhs) noexcept
    {
        Vector<T, N> rt(lhs);
        return rt %= rhs;
    }

    /// @brief Unary minus
    template <class T, uint N>
    Vector<T, N>
    operator - (const Vector<T, N>& lhs) noexcept
    {
        Vector<T, N> rv;
        for (uint i = 0; i < N; i++)
            rv[i] = -lhs[i];
        return rv;
    }

    /// @brief Normalize vector
    template <class T, uint N>
    Vector<T, N> normalize(const Vector<T, N>& lhs)
    {
        const T len = lhs.length();
        if (len < numeric_limits<T>::epsilon())
            return Vector<T, N>();
        return lhs / len;
    }
    /// @brief Get vector length
    template <class T, uint N>
    T length(const Vector<T, N>& lhs)
    {
        return lhs.length();
    }
    /// @brief Get vector length^2
    template <class T, uint N>
    T square_length(const Vector<T, N>& lhs)
    {
        return lhs.length2();
    }
    /// @brief Get distance
    template <class T, uint N>
    T distance(const Vector<T, N>& lhs, const Vector<T, N>& rhs)
    {
        return length(lhs - rhs);
    }
    /// @brief Get distance^2
    template <class T, uint N>
    T square_distance(const Vector<T, N>& lhs, const Vector<T, N>& rhs)
    {
        return square_length(lhs - rhs);
    }
    /// @brief Linear interpolation
    template <class T, uint N>
    Vector<T, N> lerp(const Vector<T, N>& lhs, const Vector<T, N>& rhs, const Vector<T, N>& factor)
    {
        Vector<T, N> rv;
        for (uint i = 0; i < N; i++)
            rv[i] = lerp(lhs[i], rhs[i], factor[i]);
        return rv;
    }
    /// @brief Floor vec
    template <class T, uint N>
    Vector<T, N> floor(const Vector<T, N>& lhs) noexcept
    {
        Vector<T, N> rv;
        for (uint i = 0; i < N; i++)
            rv[i] = floor(lhs[i]);
        return rv;
    }
    /// @brief Ceil vec
    template <class T, uint N>
    Vector<T, N> ceil(const Vector<T, N>& lhs) noexcept
    {
        Vector<T, N> rv;
        for (uint i = 0; i < N; i++)
            rv[i] = ceil(lhs[i]);
        return rv;
    }
    /// @brief Round vec
    template <class T, uint N>
    Vector<T, N> round(const Vector<T, N>& lhs) noexcept
    {
        Vector<T, N> rv;
        for (uint i = 0; i < N; i++)
            rv[i] = round(lhs[i]);
        return rv;
    }
    /// @brief Fract vec
    template <class T, uint N>
    Vector<T, N> fract(const Vector<T, N>& lhs) noexcept
    {
        Vector<T, N> rv;
        for (uint i = 0; i < N; i++)
            rv[i] = fract(lhs[i]);
        return rv;
    }

    /// @brief |vec| (component-wise)
    template <class T, uint N>
    Vector<T, N> abs(const Vector<T, N>& lhs) noexcept
    {
        Vector<T, N> rv;
        for (uint i = 0; i < N; i++)
            rv[i] = abs(lhs[i]);
        return rv;
    }

    /// @brief Check whether finite
    template <class T, uint N>
    bool isFinite(const Vector<T, N>& lhs) noexcept
    {
        for (uint i = 0; i < N; ++i)
            if (!isFinite(lhs[i]))
                return false;
        return true;
    }
    /// @brief Check whether almost equal
    template <class T, uint N>
    bool almostEqual(const Vector<T, N>& lhs,
                            const Vector<T, N>& rhs,
                            const T eps = static_cast<T>(0.00001))
    {
        const auto vd = lhs - rhs;
        const T d = max(abs(vd.x), max(abs(vd.y), abs(vd.z)));
        return almostEqual(d, static_cast<T>(0), eps);
    }
    /// @brief max(vec, vec) (component-wise)
    template <class T, uint N>
    Vector<T, N> max(const Vector<T, N>& lhs,
                     const Vector<T, N>& rhs) noexcept
    {
        Vector<T, N> rv;
        for (uint i = 0; i < N; i++)
            rv[i] = max(lhs[i], rhs[i]);
        return rv;
    }
    /// @brief min(vec, vec) (component-wise)
    template <class T, uint N>
    Vector<T, N> min(const Vector<T, N>& lhs,
                     const Vector<T, N>& rhs) noexcept
    {
        Vector<T, N> rv;
        for (uint i = 0; i < N; i++)
            rv[i] = min(lhs[i], rhs[i]);
        return rv;
    }
    /// @brief Clamp vec by vec
    template <class T, uint N>
    Vector<T, N> clamp(const Vector<T, N>& value,
                       const Vector<T, N>& min,
                       const Vector<T, N>& max) noexcept
    {
        Vector<T, N> rv;
        for (uint i = 0; i < N; i++)
            rv[i] = clamp(value[i], min[i], max[i]);
        return rv;
    }
    /// @brief Clamp vec by scalar
    template <class T, uint N>
    Vector<T, N> clamp(const Vector<T, N>& value,
                       const typename Vector<T, N>::Scalar min,
                       const typename Vector<T, N>::Scalar max) noexcept
    {
        Vector<T, N> rv;
        for (uint i = 0; i < N; i++)
            rv[i] = clamp(value[i], min, max);
        return rv;
    }
    /// @brief Un-lerp vec by vec
    template <class T, uint N>
    Vector<T, N> unlerp(const Vector<T, N>& min,
                        const Vector<T, N>& max,
                        const Vector<T, N>& value) noexcept
    {
        Vector<T, N> rv;
        for (uint i = 0; i < N; i++)
            rv[i] = unlerp(min[i], max[i], value[i]);
        return rv;
    }
    /// @brief Un-lerp vec by scalar
    template <class T, uint N>
    Vector<T, N> unlerp(const typename Vector<T, N>::Scalar min,
                        const typename Vector<T, N>::Scalar max,
                        const Vector<T, N>& value) noexcept
    {
        Vector<T, N> rv;
        for (uint i = 0; i < N; i++)
            rv[i] = unlerp(min, max, value[i]);
        return rv;
    }
    /// @brief Check whether vector is inside area (excluding maximum)
    template <class T, uint N>
    bool inside(const Vector<T, N>& value,
                const Vector<T, N>& min,
                const Vector<T, N>& max) noexcept
    {
        Vector<T, N> rv;
        for (uint i = 0; i < N; i++)
            if (value[i] < min[i] || value[i] >= max[i])
                return false;
        return true;
    }
    /// @brief Check whether vector is inside area (including maximum)
    template <class T, uint N>
    bool insideInc(const Vector<T, N>& value,
                   const Vector<T, N>& min,
                   const Vector<T, N>& max) noexcept
    {
        Vector<T, N> rv;
        for (uint i = 0; i < N; i++)
            if (value[i] < min[i] || value[i] > max[i])
                return false;
        return true;
    }
    /// @brief Check whether vector is inside area (excluding maximum)
    template <class T, uint N>
    bool inside(const Vector<T, N>& value,
                const typename Vector<T, N>::Scalar min,
                const typename Vector<T, N>::Scalar max) noexcept
    {
        Vector<T, N> rv;
        for (uint i = 0; i < N; i++)
            if (value[i] < min || value[i] >= max)
                return false;
        return true;
    }
    /// @brief Check whether vector is inside area (including maximum)
    template <class T, uint N>
    bool insideInc(const Vector<T, N>& value,
                   const typename Vector<T, N>::Scalar min,
                   const typename Vector<T, N>::Scalar max) noexcept
    {
        Vector<T, N> rv;
        for (uint i = 0; i < N; i++)
            if (value[i] < min || value[i] > max)
                return false;
        return true;
    }

    /// @brief Cast integer vector to larger grid; return local coordinates
    template <class T, uint N>
    EnableIfInteger<T, Vector<T, N>>
    upcastLocal(const Vector<T, N>& vec,
                const typename Vector<T, N>::Scalar div) noexcept
    {
        Vector<T, N> b = vec % div;
        for (uint i = 0; i < N; ++i)
            if (b[i] < 0)
                b[i] += div;
        return b;
    }
    /// @brief Cast integer vector to larger grid; return local coordinates
    template <class T, uint N>
    EnableIfInteger<T, Vector<T, N>>
    upcastLocal(const Vector<T, N>& vec,
                const Vector<T, N>& div) noexcept
    {
        Vector<T, N> b = vec % div;
        for (uint i = 0; i < N; ++i)
            if (b[i] < 0)
                b[i] += div[i];
        return b;
    }
    /// @brief Cast integer vector to larger grid; return global coordinates
    template <class T, uint N>
    EnableIfInteger<T, Vector<T, N>>
    upcast(const Vector<T, N>& vec,
           const typename Vector<T, N>::Scalar div,
           Vector<T, N>* local = 0)
    {
        auto rv = upcastLocal(vec, div);
        if (local)
            *local = rv;
        return (vec - rv) / div;
    }
    /// @brief Cast integer vector to larger grid; return global coordinates
    template <class T, uint N>
    EnableIfInteger<T, Vector<T, N>>
    upcast(const Vector<T, N>& vec,
           const Vector<T, N>& div,
           Vector<T, N>* local = 0)
    {
        auto rv = upcastLocal(vec, div);
        if (local)
            *local = rv;
        return (vec - rv) / div;
    }
    /// @brief Cast real vector to larger grid
    template <class T, uint N>
    EnableIfFloat<T, Vector<T, N>>
    upcast(const Vector<T, N>& vec,
           const typename Vector<T, N>::Scalar div) noexcept
    {
        Vector<T, N> rv = vec / div;
        for (uint i = 0; i < N; ++i)
            rv[i] = floor(rv[i]);
        return rv;
    }
    /// @brief Cross-product
    template<class T>
    Vector<T, 3> cross(const Vector<T, 3>& lhs, const Vector<T, 3>& rhs)
    {
        return Vector<T, 3>(lhs.y * rhs.z - lhs.z * rhs.y,
                            lhs.z * rhs.x - lhs.x * rhs.z,
                            lhs.x * rhs.y - lhs.y * rhs.x);
    }
    /// @brief Make orthogonal vector
    /// @note XOY plane is preferred
    template<class T>
    Vector<T, 3> makeOrthoVector(const Vector<T, 3>& vec)
    {
        if (abs(vec.y) < 0.999f)
            return normalize(Vector<T, 3>(-vec.z, 0.0f, vec.x));
        else
            return normalize(Vector<T, 3>(-vec.y, vec.x, 0.0f));
    }
    /// @brief Encode direction to sphere-map
    /// @param n
    ///   Direction in 3D
    /// @return Direction in sphere-map
    template<class T>
    Vector<T, 2> normal2sphere(const Vector<T, 3>& n)
    {
        const T f = max<T>(8 * n.z + 8, 0);
        return Vector<T, 2>(n) / sqrt(f) + Vector<T, 2>(0.5);
    }
    /// @brief Decode direction from sphere-map
    /// @param s
    ///   Direction in sphere-map
    /// @return Direction in 3D
    template<class T>
    Vector<T, 3> sphere2normal(const Vector<T, 2>& s)
    {
        const Vector<T, 2> q = s * 4 - Vector<T, 2>(2);
        const T f = square_length(q);
        const T g = sqrt(max<T>(1 - f / 4, 0));
        const Vector<T, 3> n = Vector<T, 3>(q*g, 1 - f / 2);
        return normalize(n);
    }

    /// @brief Vector component type
    enum VectorComponent
    {
        /// @brief X
        X,
        /// @brief Y
        Y,
        /// @brief Z
        Z,
        /// @brief W
        W,
        /// @brief O
        O,
        /// @brief I
        I
    };

    /// @name Swizzling
    /// @{

    /// @brief Vector swizzling generator
    template<VectorComponent C>
    struct Swizzler;
    /// @brief Ditto
    template<>
    struct Swizzler<X>
    {
        /// @brief Do swizzle
        template <class T, uint N>
        static T get(const Vector<T, N> &v)
        {
            return v.x;
        }
    };
    /// @brief Ditto
    template<>
    struct Swizzler<Y>
    {
        /// @brief Do swizzle
        template <class T, uint N>
        static T get(const Vector<T, N> &v)
        {
            return v.y;
        }
    };
    /// @brief Ditto
    template<>
    struct Swizzler<Z>
    {
        /// @brief Do swizzle
        template <class T, uint N>
        static T get(const Vector<T, N> &v)
        {
            return v.z;
        }
    };
    /// @brief Ditto
    template<>
    struct Swizzler<W>
    {
        /// @brief Do swizzle
        template <class T, uint N>
        static T get(const Vector<T, N> &v)
        {
            return v.w;
        }
    };
    /// @brief Ditto
    template<>
    struct Swizzler<O>
    {
        /// @brief Do swizzle
        template <class T, uint N>
        static T get(Vector<T, N> const& /*v*/)
        {
            return 0;
        }
    };
    /// @brief Ditto
    template<>
    struct Swizzler<I>
    {
        /// @brief Do swizzle
        template <class T, uint N>
        static T get(Vector<T, N> const& /*v*/)
        {
            return 1;
        }
    };
    /// @brief Get 1-component
    template <VectorComponent S1, class T, uint N>
    T swiz(const Vector<T, N> &v)
    {
        return Swizzler<S1>::get(v);
    }
    /// @brief Get 2-component
    template <VectorComponent S1, VectorComponent S2, class T, uint N>
    Vector<T, 2> swiz(const Vector<T, N>& v)
    {
        return Vector<T, 2>(Swizzler<S1>::get(v),
                            Swizzler<S2>::get(v));
    }
    /// @brief Get 3-component
    template <VectorComponent S1, VectorComponent S2, VectorComponent S3, class T, uint N>
    Vector<T, 3> swiz(const Vector<T, N>& v)
    {
        return Vector<T, 3>(Swizzler<S1>::get(v),
                            Swizzler<S2>::get(v),
                            Swizzler<S3>::get(v));
    }
    /// @brief Get 4-component
    template <VectorComponent S1, VectorComponent S2, VectorComponent S3, VectorComponent S4, class T, uint N>
    Vector<T, 4> swiz(const Vector<T, N>& v)
    {
        return Vector<T, 4>(Swizzler<S1>::get(v),
                            Swizzler<S2>::get(v),
                            Swizzler<S3>::get(v),
                            Swizzler<S4>::get(v));
    }
    /// @}
}

namespace std
{
    /// @brief short2 vector hasher
    template <>
    struct hash<ls::Vector<ls::sshort, 2>>
    {
        /// @brief Hasher
        size_t operator () (const ls::Vector<ls::sshort, 2>& k) const
        {
            return static_cast<size_t>(((ls::sint) k.x << 16) ^ k.y);
        }
    };
    /// @brief int2 vector hasher
    template <>
    struct hash<ls::Vector<ls::sint, 2>>
    {
        /// @brief Hasher
        size_t operator () (const ls::Vector<ls::sint, 2>& k) const
        {
            return static_cast<size_t>((k.x << 16) ^ k.y);
        }
    };
}