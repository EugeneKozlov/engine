/// @file ls/math/Quaternion.h
/// @brief Quaternion template
#pragma once

#include "ls/math/Vector.h"
#include "ls/math/Matrix.h"

namespace ls
{
    /// @addtogroup LsMath
    /// @{

    /// @brief Quaternion
    template <class T>
    struct Quaternion
    {
    public:
        /// @brief Type of components
        typedef T Scalar;
        /// @brief Vector representation type
        using Vector4 = Vector<Scalar, 4>;
        /// @brief Axis vector type
        using Vector3 = Vector<Scalar, 3>;
        /// @brief Number of components
        static const int size = 4;
        /// @brief Get identity
        static const Quaternion<Scalar>& identity()
        {
            static const Quaternion<Scalar> q;
            return q;
        }

        /// @brief Identity
        Quaternion() noexcept
            : x(0)
            , y(0)
            , z(0)
            , w(1)
        {
        }
        /// @brief By scalar
        Quaternion(const Scalar t) noexcept
            : x(t)
            , y(t)
            , z(t)
            , w(t)
        {
        }
        /// @brief By components
        Quaternion(const Scalar x, const Scalar y, const Scalar z, const Scalar w) noexcept
            : x(x)
            , y(y)
            , z(z)
            , w(w)
        {
        }
        /// @brief By eiler angles, from X to Z
        Quaternion(const Scalar ax, const Scalar ay, const Scalar az) noexcept
        {
            const Scalar c1 = cos(ax * 0.5f);
            const Scalar s1 = sin(ax * 0.5f);
            const Scalar c2 = cos(ay * 0.5f);
            const Scalar s2 = sin(ay * 0.5f);
            const Scalar c3 = cos(az * 0.5f);
            const Scalar s3 = sin(az * 0.5f);
            x = s1 * c2 * c3 - c1 * s2 * s3;
            y = c1 * s2 * c3 + s1 * c2 * s3;
            z = c1 * c2 * s3 - s1 * s2 * c3;
            w = c1 * c2 * c3 + s1 * s2 * s3;
            const Scalar len = sqrt(x * x + y * y + z * z + w * w);
            x /= len;
            y /= len;
            z /= len;
            w /= len;
        }
        /// @brief By axis and angle
        Quaternion(const Vector3& axis, const Scalar angle) noexcept
        {
            const Vector3 v = normalize(axis);
            const Scalar half_angle = angle * static_cast<Scalar>(0.5);
            const Scalar sin_a = sin(half_angle);
            x = v.x * sin_a;
            y = v.y * sin_a;
            z = v.z * sin_a;
            w = cos(half_angle);
            const Scalar len = sqrt(x * x + y * y + z * z + w * w);
            x /= len;
            y /= len;
            z /= len;
            w /= len;
        }
        /// @brief Rotation from @a lhs to @a rhs
        Quaternion(const Vector3& lsh, const Vector3& rhs) noexcept
        {
            *this = Quaternion(normalize(cross(lsh, rhs)),
                               acos(clamp(dot(lsh, rhs), 
                                          static_cast<Scalar>(-1), 
                                          static_cast<Scalar>(1))));
        }
        /// @brief Cast
        explicit Quaternion(const Vector4& lsh) noexcept
            : x(lsh.x)
            , y(lsh.y)
            , z(lsh.z)
            , w(lsh.w)
        {
        }
        /// @brief Lerp
        Quaternion(const Quaternion& lhs,
                   const Quaternion& rhs,
                   const Scalar factor) noexcept
        {
            // Calculate cosine theta
            Scalar cosom = lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z + lhs.w * rhs.w;
            // Adjust signs (if necessary)
            Quaternion end = rhs;
            if (cosom < 0.0f)
            {
                cosom = -cosom;
                end.x = -end.x; // Reverse all signs
                end.y = -end.y;
                end.z = -end.z;
                end.w = -end.w;
            }
            // Calculate coefficients
            Scalar sclp, sclq;
            if ((1.0f - cosom) > 0.0001f) // 0.0001 -> some epsillon
            {
                // Standard case (slerp)
                Scalar omega, sinom;
                omega = acos(cosom); // Extract theta from dot product's cos theta
                sinom = sin(omega);
                sclp = sin((1.0f - factor) * omega) / sinom;
                sclq = sin(factor * omega) / sinom;
            }
            else
            {
                // Lerp
                sclp = 1.0f - factor;
                sclq = factor;
            }

            // Set
            x = sclp * lhs.x + sclq * end.x;
            y = sclp * lhs.y + sclq * end.y;
            z = sclp * lhs.z + sclq * end.z;
            w = sclp * lhs.w + sclq * end.w;
        }

        /// @brief Get elements array
        Scalar* value() noexcept
        {
            return (&x);
        }
        /// @brief Get constant elements array
        const Scalar* value() const noexcept
        {
            return (&x);
        }
        /// @brief Get element
        Scalar& operator [](const size_t idx) noexcept
        {
            return value()[idx];
        }
        /// @brief Get constant element
        const Scalar& operator [](const size_t idx) const noexcept
        {
            return value()[idx];
        }
        /// @brief Get axis-angle (normalized only)
        Vector4 asAxisAngle() const noexcept
        {
            Vector4 tmp;
            tmp.w = 2 * acos(w);
            const Scalar s = sqrt(1 - w * w);
            if (s < 0.0001)
            {
                tmp.x = 1;
                tmp.y = tmp.z = 0;
            }
            else
            {
                tmp.x = x / s;
                tmp.y = y / s;
                tmp.z = z / s;
            }
            return tmp;
        }
        /// @brief Get matrix
        Matrix3x3<Scalar> asMatrix() const noexcept
        {
            const Scalar x2 = x*x;
            const Scalar y2 = y*y;
            const Scalar z2 = z*z;
            const Scalar xy = x*y;
            const Scalar zw = z*w;
            const Scalar xz = x*z;
            const Scalar yw = y*w;
            const Scalar yz = y*z;
            const Scalar xw = x*w;
            return{ 1 - 2 * y2 - 2 * z2, 2 * (xy + zw), 2 * (xz - yw), 
                    2 * (xy - zw), 1 - 2 * x2 - 2 * z2, 2 * (yz + xw), 
                    2 * (xz + yw), 2 * (yz - xw), 1 - 2 * x2 - 2 * y2 };
        }

        /// @brief Get square norm
        Scalar norm2() const noexcept
        {
            return x * x + y * y + z * z + w*w;
        }
        /// @brief Conjugate quaternion.
        Quaternion conjugate() const noexcept
        {
            return Quaternion(-x, -y, -z, w);
        }
        /// @brief Inverse quaternion
        Quaternion inverse() const noexcept
        {
            return conjugate() / norm2();
        }
        /// @brief quat * quat
        Quaternion& operator *= (const Quaternion& rhs) noexcept
        {
            *this = Quaternion(rhs.x * w + rhs.y * z - rhs.z * y + rhs.w * x,
                               -rhs.x * z + rhs.y * w + rhs.z * x + rhs.w * y,
                               rhs.x * y - rhs.y * x + rhs.z * w + rhs.w * z,
                               -rhs.x * x - rhs.y * y - rhs.z * z + rhs.w * w);
            return *this;
        }
        /// @brief quat * quat
        Quaternion operator * (const Quaternion& rhs) const noexcept
        {
            auto temp = *this;
            temp *= rhs;
            return temp;
        }
        /// @brief quat * scalar
        Quaternion& operator *= (const Scalar rhs) noexcept
        {
            x *= rhs;
            y *= rhs;
            z *= rhs;
            w *= rhs;
            return *this;
        }
        /// @brief quat * scalar
        Quaternion operator * (const Scalar rhs) const
        {
            Quaternion temp = *this;
            temp *= rhs;
            return temp;
        }
        /// @brief quat / scalar
        Quaternion operator / (Scalar rhs) const
        {
            Quaternion temp = *this;
            temp *= Scalar(1) / rhs;
            return temp;
        }
        /// @brief Is equal?
        bool operator == (const Quaternion& rhs) const noexcept
        {
            return x == rhs.x && y == rhs.y && z == rhs.z && w == rhs.w;
        }
        /// @brief Is not equal?
        bool operator != (const Quaternion& rhs) const noexcept
        {
            return !(*this == rhs);
        }
    public:
        /// @brief x component
        Scalar x;
        /// @brief y component
        Scalar y;
        /// @brief z component
        Scalar z;
        /// @brief w component
        Scalar w;
    };
    /// @brief Check whether finite
    template <class T>
    inline bool isFinite(const Quaternion<T>& lhs)
    {
        return isFinite(lhs.x) && isFinite(lhs.y) && isFinite(lhs.z) && isFinite(lhs.w);
    }
    /// @brief Check whether almost equal
    template <class T>
    inline bool almostEqual(const Quaternion<T>& lhs,
                            const Quaternion<T>& rhs,
                            const T eps = static_cast<T>(0.00001))
    {
        T d = max(max(abs(lhs.x - rhs.x), abs(lhs.y - rhs.y)),
                  max(abs(lhs.z - rhs.z), abs(lhs.w - rhs.w)));
        return almostEqual(d, static_cast<T>(0), eps);
    }
    /// @brief Normalize
    template <class T>
    Quaternion<T> normalize(const Quaternion<T>& lhs)
    {
        return lhs / sqrt(lhs.norm2());
    }
    /// @brief Spherical interpolation
    template <class T>
    inline Quaternion<T> slerp(const Quaternion<T>& lhs,
                               const Quaternion<T>& rhs,
                               const T factor)
    {
        return Quaternion<T>(lhs, rhs, factor);
    }
    /// @}
}
