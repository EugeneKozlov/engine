#include "pch.h"
#include "ls/lua/LuaApi.h"

#include "ls/core/UniformRandom.h"
#include "ls/io/FileSystemInterface.h"
#include "ls/io/StreamInterface.h"

using namespace ls;
LuaApi::LuaApi(const string& name)
    : Nameable<LuaApi>(name)
    , m_scriptFolder({})
    , m_generator(make_unique<UniformRandom>(0))
{
    function<void(const string&)> funImport = bind(&LuaApi::import, this, _1);
    function<uint()> funRandom = bind(&LuaApi::random, this);

    function<void(const string&)> funDebug = bind(&LuaApi::logDebug<string>, this, _1);
    function<void(const string&)> funInfo = bind(&LuaApi::logInfo<string>, this, _1);
    function<void(const string&)> funNotice = bind(&LuaApi::logNotice<string>, this, _1);
    function<void(const string&)> funWarning = bind(&LuaApi::logWarning<string>, this, _1);

    function<LuaIntf::LuaRef(LuaIntf::LuaRef&)> funConcatenateArray = bind(&LuaApi::luaConcatenateArray, this, _1);
    
    m_lua = LuaState::newState();
    m_lua.openLibs();

    LuaBinding(m_lua)
        .beginModule("e")
        /**/.addFunction("import", funImport)
        /**/.addFunction("random", funRandom)

        /**/.addFunction("debug", funDebug)
        /**/.addFunction("info", funInfo)
        /**/.addFunction("notice", funNotice)
        /**/.addFunction("warning", funWarning)

        /**/.addFunction("array_concat", funConcatenateArray)

        /**/.beginClass<UniformRandom>("uniform")
        /**//**/.addConstructor(LUA_ARGS(uint))
        /**//**/.addFunction("random", &UniformRandom::random)
        /**//**/.addFunction("int_in", &UniformRandom::randomInt)
        /**//**/.addFunction("real_u", &UniformRandom::randomReal_01)
        /**//**/.addFunction("real_s", &UniformRandom::randomReal_11)
        /**//**/.addFunction("real_in", &UniformRandom::randomReal)
        /**/.endClass()
        .endModule();
}
LuaApi::~LuaApi()
{
    m_lua.close();
}
void LuaApi::pushFolder(FileSystem fileSystem)
{
    m_scriptFolder.pushBack(fileSystem);
}
void LuaApi::popFolder()
{
    m_scriptFolder.popBack();
}
bool LuaApi::import(const string& module)
{
    // Load
    Stream stm = m_scriptFolder.openStream(module, StreamAccess::Read);
    if (!stm)
    {
        logWarning("Unknown module to import : ", module);
        return false;
    }
    const string text = stm->text();
    const uint len = text.length() - 1;

    // Do
    int result = m_lua.loadBuffer(text.c_str(), len, module.c_str());
    if (!result)
        result = m_lua.pcall(0, 0, 0);

    // Log
    if (result)
    {
        const char* error = m_lua.toString(-1);
        if (error)
            logWarning(error);
        else
            logWarning("Unknown error");
        m_lua.pop();
        return false;
    }
    return true;
}
void LuaApi::registerModule(BuiltinModule& module)
{
    module.exportToLua(m_lua);
}

// Random
void LuaApi::initializeRandomSeed(uint seed)
{
    m_generator = make_unique<UniformRandom>(seed);
}
uint LuaApi::random()
{
    return m_generator->random();
}


// Built-in
LuaIntf::LuaRef LuaApi::luaConcatenateArray(LuaIntf::LuaRef src)
{
    using namespace LuaIntf;
    LS_THROW(src.isTable(), ScriptException, "Input must be a table");

    const int numTables = src.rawlen();
    LuaRef dest = LuaRef::createTable(m_lua);
    int destIndex = 1;
    for (int srcIndex = 1; srcIndex <= numTables; ++srcIndex)
    {
        LuaRef elem = src[srcIndex].value<>();
        if (!elem.isTable())
        {
            dest[destIndex++] = elem;
        }
        else
        {
            const int numSubtables = elem.rawlen();
            for (int subIndex = 1; subIndex <= numSubtables; ++subIndex)
            {
                LuaRef subElem = elem[subIndex].value<>();
                dest[destIndex++] = subElem;
            }
        }
    }
    return dest;
}
