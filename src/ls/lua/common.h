/// @file ls/lua/common.h
/// @brief Common lua stuff
#pragma once

#include "ls/common.h"

#pragma warning(push, 0) 
#include <LuaIntf/LuaIntf.h>
#pragma warning(pop) 

namespace ls
{
    /// @addtogroup LsLua
    /// @{

    /// @brief Lua state
    using LuaState = LuaIntf::LuaState;
    /// @brief Lua reference
    using LuaRef = LuaIntf::LuaRef;
    /// @brief Lua binding
    using LuaIntf::LuaBinding;
    /// @}
}

/// @brief LuaIntf namespace
namespace LuaIntf
{
    /// @brief LuaIntf vector mapping
    template <class Element>
    struct LuaTypeMapping <std::vector<Element>>
    {
        /// @brief Impl
        static void push(lua_State* L, const std::vector<Element>& v)
        {
            if (v.empty())
            {
                lua_newtable(L);
            }
            else
            {
                Lua::pushList(L, v);
            }
        }
        /// @brief Impl
        static std::vector<Element> get(lua_State* L, int index)
        {
            if (lua_isnoneornil(L, index))
            {
                return std::vector<Element>();
            }
            else
            {
                return Lua::getList<std::vector<Element>>(L, index);
            }
        }
        /// @brief Impl
        static std::vector<Element> opt(lua_State* L, int index, const std::vector<Element>& def)
        {
            if (lua_isnoneornil(L, index))
            {
                return def;
            }
            else
            {
                return Lua::getList<std::vector<Element>>(L, index);
            }
        }
    };
    /// @brief LuaIntf math vector mapping
    template <class Element, int N>
    struct LuaTypeMapping <ls::Vector<Element, N>>
    {
        /// @brief Impl
        static void push(lua_State* L, const ls::Vector<Element, N>& v)
        {
            lua_newtable(L);
            for (int i = 0; i < N; ++i)
            {
                Lua::push(L, v[i]);
                lua_rawseti(L, -2, i + 1);
            }
        }
        /// @brief Impl
        static ls::Vector<Element, N> get(lua_State* L, int index)
        {
            return opt(L, index, ls::Vector<Element, N>());
        }
        /// @brief Impl
        static ls::Vector<Element, N> opt(lua_State* L, int index, const ls::Vector<Element, N>& def)
        {
            ls::Vector<Element, N> vec = def;
            if (!lua_isnoneornil(L, index))
            {
                luaL_checktype(L, index, LUA_TTABLE);
                int n = (int) luaL_len(L, index);
                for (int i = 1; i <= std::min(N, n); i++)
                {
                    lua_rawgeti(L, index, i);
                    vec[i - 1] = Lua::pop<Element>(L);
                }
            }
            return vec;
        }
    };
}