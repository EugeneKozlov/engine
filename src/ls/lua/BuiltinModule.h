/// @file ls/lua/BuiltinModule.h
/// @brief Built-in Module
#pragma once

#include "ls/common.h"
#include "ls/lua/common.h"

namespace ls
{
    /// @addtogroup LsLua
    /// @{

    /// @brief Built-in Module
    class BuiltinModule
    {
    public:
        /// @brief Dtor
        virtual ~BuiltinModule() {}
        /// @brief Export to Lua Machine
        virtual void exportToLua(LuaState& state) = 0;
    };

    /// @}
}