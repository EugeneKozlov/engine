/// @file ls/lua/LuaApi.h
/// @brief Script Lua API
#pragma once

#include "ls/common.h"
#include "ls/core/Nameable.h"
#include "ls/io/MultiFileSystem.h"
#include "ls/lua/common.h"
#include "ls/lua/BuiltinModule.h"

namespace ls
{
    class BuiltinModule;
    class UniformRandom;

    /// @addtogroup LsLua 
    /// @{

    /// @brief Script Lua API
    class LuaApi
        : public Nameable<LuaApi>
    {
    public:
        /// @brief Ctor
        /// @param name
        ///   Implementation class name
        LuaApi(const string& name);
        /// @brief Dtor
        ~LuaApi();
        /// @brief Push script location
        void pushFolder(FileSystem fileSystem);
        /// @brief Pop script location
        void popFolder();
        /// @brief Import built-in module
        /// @note Will own module object
        template <class Module, class ... Args>
        Module& importModule(Args && ... args)
        {
            m_modules.emplace_back(make_unique<Module>(std::forward<Args>(args)...));
            m_modules.back()->exportToLua(m_lua);
            return static_cast<Module&>(*m_modules.back());
        }
        /// @brief Register built-in module
        /// @note Will @b not own module object
        void registerModule(BuiltinModule& module);
        /// @brief Import @a module
        bool import(const string& module);

        /// @brief Set seed
        void initializeRandomSeed(uint seed);
        /// @brief Get random
        uint random();
        /// @brief Get lua state
        LuaState& luaState()
        {
            return m_lua;
        }
        /// @brief Get lua binding
        LuaIntf::CppBindModule luaBinding()
        {
            return LuaBinding(m_lua);
        }
    private:
        LuaIntf::LuaRef luaConcatenateArray(LuaIntf::LuaRef src);
    private:
        MultiFileSystem m_scriptFolder;
        LuaState m_lua;
        unique_ptr<UniformRandom> m_generator;
        vector<unique_ptr<BuiltinModule>> m_modules;
    };
    /// @brief Lua API folder holder
    class LuaFolderHolder
        : Noncopyable
    {
    public:
        LuaFolderHolder(LuaApi& api, FileSystem fileSystem)
            : m_api(api)
        {
            api.pushFolder(fileSystem);
        }
        ~LuaFolderHolder()
        {
            m_api.popFolder();
        }
    private:
        LuaApi& m_api;
    };
    /// @}
}

