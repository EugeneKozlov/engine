/// @file ls/gapi/Renderer.h
/// @brief Abstract renderer interface.
#pragma once

#include "ls/common.h"
#include "ls/core/Nameable.h"
#include "ls/gapi/BufferPool.h"
#include "ls/gapi/common.h"
#include "ls/gapi/desc/vertices.h"
#include "ls/gapi/functions.h"
#include "ls/gapi/usl/usl.h"
#include "ls/io/common.h"

namespace ls
{
    /// @addtogroup LsGapi
    /// @{

    /// @brief Renderer statistic
    struct RendererStatistic
    {
        /// @brief Number of drawn primitives
        MultiArray<ulong, (uint) PrimitiveType::Count> numDrawnPrimitives = 0;
        /// @brief Number of drawn lines
        ulong numLines = 0;
        /// @brief Number of drawn triangles
        ulong numTriangles = 0;
        /// @brief Number of draw calls
        ulong numDrawCalls = 0;

        /// @brief Get number of drawn indices
        ulong numDrawnIndices(PrimitiveType primitive) const
        {
            return numDrawnPrimitives[(uint) primitive];
        }
    };
    /// @brief Abstract renderer interface
    ///
    /// All bind calls shall be cached
    class Renderer
        : Noncopyable
        , public Nameable<Renderer>
    {
    protected:
        /// @brief Ctor
        /// @param rendererConfig
        ///   Config
        Renderer();
        /// @brief Log
        template <class... Args>
        void logStuff(Args&&... args)
        {
            if (m_hasLogging)
                logDebug(args...);
        }
    public:
        /// @brief Post-initialize
        void initialize();
        /// @brief Pre-deinitialize
        void deinitialize();
        /// @brief Dtor
        virtual ~Renderer();

        /// @brief Add effect library chunk
        /// @note Chunk is overwritten if it is already loaded
        void addEffectLibraryChunk(const string& name, const string& text);
        /// @brief Compile effect
        virtual Stream compileEffect(usl::Effect& source) = 0;
        /// @brief Create effect
        /// @note Thread-safe
        virtual EffectHandle createEffect(StreamInterface& compiled) = 0;
        /// @brief Destroy effect
        /// @note Thread-safe
        virtual void destroyEffect(EffectHandle effectId) = 0;
        /// @brief Attach uniform buffer to effect
        /// @param effectId
        ///   Effect ID
        /// @param bufferId
        ///   Uniform buffer ID
        /// @param uniformBufferName
        ///   Uniform name
        /// @return @c true on success
        /// @note Avoid this call in render loop because it discards currently bound effect
        virtual bool attachUniformBuffer(EffectHandle effectId,
                                         BufferHandle bufferId,
                                         const char* uniformBufferName) = 0;
        /// @brief Detach uniform buffer from effect
        /// @param effectId
        ///   Effect ID
        /// @param uniformBufferName
        ///   Uniform name
        /// @return @c true on success
        bool detachUniformBuffer(EffectHandle effectId,
                                 const char* uniformBufferName)
        {
            return attachUniformBuffer(effectId, nullptr, uniformBufferName);
        }
        /// @brief Get program handle
        /// @param effectId
        ///   Effect ID
        /// @param programName
        ///   Program name
        /// @return Program handle
        virtual ProgramHandle handleProgram(EffectHandle effectId,
                                            const char* programName) = 0;
        /// @brief Get resource handle
        /// @param effectId
        ///   Effect ID
        /// @param resourceName
        ///   Resource name
        /// @return Resource handle
        virtual EffectResourceHandle handleResource(EffectHandle effectId,
                                                    const char* resourceName) = 0;

        /// @brief Create state object
        /// @param desc
        ///   State desc
        virtual StateHandle createState(const StateDesc& desc) = 0;
        /// @brief Destroy state object
        virtual void destroyState(StateHandle stateId) = 0;

        /// @brief Create buffer
        /// @param size
        ///   Buffer size
        /// @param type
        ///   Buffer type
        /// @param usage
        ///   Data usage
        /// @param data
        ///   Data pointer (may be null)
        /// @note Thread-safe
        /// @return Buffer ID
        virtual BufferHandle createBuffer(const uint size,
                                          const BufferType type,
                                          const ResourceUsage usage,
                                          const void* data) = 0;
        /// @brief Create uniform buffer
        BufferHandle createUniformBuffer(const uint size)
        {
            return createBuffer(size, BufferType::Uniform, ResourceUsage::Static, nullptr);
        }
        /// @brief Destroy buffer
        /// @note Thread-safe
        virtual void destroyBuffer(BufferHandle bufferId) = 0;
        /// @brief Map buffer
        /// @param bufferId
        ///   Buffer ID
        /// @param type
        ///   Map type
        /// @return Data pointer
        virtual void* mapBuffer(BufferHandle bufferId,
                                const MappingFlag type = MappingFlag::WriteDiscard) = 0;
        /// @brief Unmap mapped buffer
        /// @param bufferId
        ///   Buffer ID
        virtual void unmapBuffer(BufferHandle bufferId) = 0;
        /// @brief Write buffer data
        /// @param bufferId
        ///   Buffer ID
        /// @param offset
        ///   Data offset
        /// @param size
        ///   Data size
        /// @param data
        ///   Data pointer
        virtual void writeBuffer(BufferHandle bufferId, const uint offset, const uint size,
                                 const void* data) = 0;
        /// @brief Write buffer by object
        /// @param bufferId
        ///   Buffer ID
        /// @param buffer
        ///   Data buffer
        template <class Object>
        void writeBuffer(BufferHandle bufferId, const Object& buffer)
        {
            writeBuffer(bufferId, 0, sizeof(Object), &buffer);
        }

        /// @brief Create vertex layout
        /// @param programId
        ///   Effect program related to geometry streams.
        ///   Same layout is compatible with @a any program with same vertex input.
        /// @param strides
        ///   Geometry streams strides
        /// @param numStreams
        ///   Number of geometry streams
        /// @param format,formatSize
        ///   Vertex format description
        /// @return Layout ID
        virtual LayoutHandle createLayout(ProgramHandle programId,
                                          const uint strides[], const uint numStreams,
                                          const VertexFormat format[], const uint formatSize) = 0;
        /// @brief Create vertex layout
        /// @param programId
        ///   Effect program related to geometry streams.
        ///   Same layout is compatible with @a any program with same vertex input.
        /// @param strides
        ///   Geometry streams strides
        /// @param format
        ///   Vertex format 
        /// @return Layout ID
        LayoutHandle createLayout(ProgramHandle programId,
                                  initializer_list<uint> strides,
                                  initializer_list<VertexFormat> format)
        {
            return createLayout(programId, strides.begin(), strides.size(),
                                format.begin(), format.size());
        }
        /// @brief Create layout for single input
        /// @param programId
        ///   Effect program related to geometry streams.
        ///   Same layout is compatible with @a any program with same vertex input.
        /// @param stride
        ///   Geometry stream stride
        /// @param format,formatSize
        ///   Vertex format and its size
        /// @return Layout ID
        LayoutHandle createLayoutSingle(ProgramHandle programId,
                                        const uint stride,
                                        const VertexFormat format[], const uint formatSize)
        {
            return createLayout(programId, &stride, 1,
                                format, formatSize);
        }
        /// @brief Destroy vertex layout
        /// @param layoutId
        ///   Layout ID
        virtual void destroyLayout(LayoutHandle layoutId) = 0;

        /// @brief Create 2D texture
        /// @param numArraySlices
        ///   Number of array slices
        /// @param numMipLevels
        ///   Number of MipMap levels
        /// @param width,height
        ///   Texture size
        /// @param format
        ///   Pixel format
        /// @param usage
        ///   Resource usage
        /// @param binding
        ///   Resource binding
        /// @param data
        ///   Optional data
        /// @note Thread-safe
        /// @return 2D texture ID
        virtual Texture2DHandle createTexture2D(const uint numArraySlices, const uint numMipLevels,
                                                const uint width, const uint height,
                                                const Format format,
                                                const ResourceUsage usage,
                                                const FlagSet<ResourceBinding> binding,
                                                const void* const data[] = nullptr) = 0;
        /// @brief Create single texture
        /// @note Thread-safe
        Texture2DHandle createTexture2D(const uint width, const uint height, const bool hasMipmaps,
                                        const Format format,
                                        const ResourceUsage usage,
                                        const FlagSet<ResourceBinding> binding,
                                        const void* const data[] = nullptr)
        {
            const uint numMipmaps = hasMipmaps
                ? (log2u(max(width, height)) + 1)
                : 1;
            return createTexture2D(1, numMipmaps, width, height, format,
                                   usage, binding, data);
        }
        /// @brief Destroy 2D texture
        /// @note Thread-safe
        virtual void destroyTexture2D(Texture2DHandle textureId) = 0;
        /// @brief Load texture from stream (break @b Program)
        /// @param source
        ///   Source stream
        /// @param binding
        ///   Resource binding
        /// @return Texture ID
        /// @note Thread-safe
        virtual Texture2DHandle loadTexture2D(const TextureStorage& source,
                                              const FlagSet<ResourceBinding> binding = ResourceBinding::ShaderResource) = 0;
        /// @brief Save texture to stream
        /// @param dest
        ///   Destination stream
        /// @param textureId
        ///   Texture ID
        virtual void unloadTexture2D(TextureStorage& dest, Texture2DHandle textureId);
        /// @brief Write data to texture
        /// @param textureId
        ///   Destination texture
        /// @param arraySlice
        ///   Destination array slice
        /// @param mipLevel
        ///   Destination mipmap level
        /// @param offsetX,offsetY
        ///   Destination offset
        /// @param width,height
        ///   Source size
        /// @param data
        ///   Source data
        virtual void writeTexture2D(Texture2DHandle textureId,
                                    const uint arraySlice, const uint mipLevel,
                                    const uint offsetX, const uint offsetY,
                                    const uint width, const uint height,
                                    const void* data) = 0;
        /// @brief Write flat 2D texture region
        /// @param textureId
        ///   Destination texture
        /// @param offsetX,offsetY
        ///   Destination offset
        /// @param width,height
        ///   Source size
        /// @param data
        ///   Source data
        void writeTexture2D(Texture2DHandle textureId,
                            const uint offsetX, const uint offsetY,
                            const uint width, const uint height,
                            const void* data)
        {
            writeTexture2D(textureId, 0, 0, offsetX, offsetY, width, height, data);
        }
        /// @brief Overwrite full 2D texture with specified size
        /// @param textureId
        ///   Destination texture
        /// @param width,height
        ///   Source size
        /// @param data
        ///   Source data
        void writeTexture2D(Texture2DHandle textureId, const uint width, const uint height, const void* data)
        {
            writeTexture2D(textureId, 0, 0, 0, 0, width, height, data);
        }
        /// @brief Read pixels from texture (break @b RenderTarget)
        /// @param textureId
        ///   Texture 2D
        /// @param arraySlice
        ///   Array slice to read
        /// @param mipLevel
        ///   Mip-map level to read
        /// @param x,y
        ///   Offset
        /// @param width,height
        ///   Area size
        /// @param [out] data
        ///   Destination data
        virtual void readTexture2D(Texture2DHandle textureId,
                                   const uint arraySlice, const uint mipLevel,
                                   const uint x, const uint y,
                                   const uint width, const uint height,
                                   void* data) = 0;
        /// @brief Read pixels from first slice and zero mipmap level of texture
        /// @param textureId
        ///   Texture 2D
        /// @param x,y
        ///   Offset
        /// @param width,height
        ///   Area size
        /// @param [out] data
        ///   Destination data
        void readTexture2D(Texture2DHandle textureId,
                           const uint x, const uint y,
                           const uint width, const uint height,
                           void* data)
        {
            readTexture2D(textureId, 0, 0, x, y, width, height, data);
        }
        /// @brief Read whole texture (zero slice)
        /// @param textureId
        ///   Texture ID
        /// @param [out] data
        ///   Destination data
        void readTexture2D(Texture2DHandle textureId,
                           void* data)
        {
            readTexture2D(textureId, 0, 0, 0, 0,
                          textureId->width, textureId->height, data);
        }
        /// @brief Copy part of one 2D texture array slice to another
        /// @param destId
        ///   Destination texture ID
        /// @param sourceId
        ///   Source texture ID
        /// @param destSlice,sourceSlice
        ///   Source and destination array slices
        /// @param destMip,sourceMip
        ///   Source and destination mip level
        /// @param destX,destY
        ///   Position in destination texture
        /// @param sourceX,sourceY
        ///   Position in source texture
        /// @param width,height
        ///   Region size
        virtual void copyTexture2D(Texture2DHandle destId, Texture2DHandle sourceId,
                                   const uint destSlice, const uint sourceSlice,
                                   const uint destMip, const uint sourceMip,
                                   const uint destX, const uint destY,
                                   const uint sourceX, const uint sourceY,
                                   const uint width, const uint height) = 0;
        /// @brief Copy part of one 2D texture (first array slice and zero mipmap) to another
        /// @param destId
        ///   Destination texture ID
        /// @param sourceId
        ///   Source texture ID
        /// @param destX,destY
        ///   Position in destination texture
        /// @param sourceX,sourceY
        ///   Position in source texture
        /// @param width,height
        ///   Region size
        void copyTexture2D(Texture2DHandle destId, Texture2DHandle sourceId,
                           const uint destX, const uint destY,
                           const uint sourceX, const uint sourceY,
                           const uint width, const uint height)
        {
            copyTexture2D(destId, sourceId, 0, 0, 0, 0,
                          destX, destY, sourceX, sourceY,
                          width, height);
        }
        /// @brief Copy whole 2D texture (first slice and zero mipmap) to another.
        /// @brief Sizes must match.
        /// @param destId
        ///   Destination texture ID
        /// @param sourceId
        ///   Source texture ID
        void copyTexture2D(Texture2DHandle destId, Texture2DHandle sourceId)
        {
            const uint destWidth = destId->width, destHeight = destId->height;
            const uint sourceWidth = sourceId->width, sourceHeight = sourceId->height;

            debug_assert(destWidth == sourceWidth);
            debug_assert(destHeight == sourceHeight);

            copyTexture2D(destId, sourceId, 0, 0, 0, 0,
                          sourceWidth, sourceHeight);
        }
        /// @brief Generate texture mipmaps.
        /// @brief Texture should be @a ShaderResource and @a RenderTarget
        virtual void generateMipmaps(Texture2DHandle textureId) = 0;

        /// @brief Set texture sampling mode
        /// @param textureId
        ///   Texture ID
        /// @param filter
        ///   Texture filtering
        /// @param anisotropyLevel
        ///   Anisotropic filtering level
        /// @param addressing
        ///   Texture addressing mode
        /// @param minLod,maxLod
        ///   Min and max LODs
        /// @param lodBias
        ///   LOD bias
        /// @param shadow
        ///   Set to create shadow sampler
        /// @param comparison
        ///   Shadow texture comparison function;
        virtual void setTexture2DSampler(Texture2DHandle textureId,
                                         const Filter filter,
                                         const Addressing addressing,
                                         const uint anisotropyLevel = 1,
                                         const float minLod = -1000.0f,
                                         const float maxLod = 1000.0f,
                                         const float lodBias = 0.0f,
                                         const bool shadow = false,
                                         const Comparison comparison = Comparison::LessEqual) = 0;
        /// @brief Set texture shadow sampling mode 
        void setTexture2DSampler(Texture2DHandle textureId,
                                 const Filter filter, const Addressing addressing,
                                 const bool shadow, const Comparison comparison)
        {
            setTexture2DSampler(textureId, filter, addressing,
                                1, -1000.0f, 1000.0f, 0.0f,
                                shadow, comparison);
        }

        /// @brief Create render target (break @b RenderTarget)
        /// @param numColorChannels
        ///   Number of color channels
        /// @param depthTextureId
        ///   Depth texture ID
        /// @return Render target ID
        virtual RenderTargetHandle createRenderTarget(const uint numColorChannels,
                                                      Texture2DHandle depthTextureId) = 0;
        /// @brief Create render target (break @b RenderTarget)
        /// @param colorTextureId
        ///   Color texture ID
        /// @param depthTextureId
        ///   Depth texture ID
        /// @return Render target ID
        RenderTargetHandle createRenderTarget(Texture2DHandle colorTextureId,
                                              Texture2DHandle depthTextureId)
        {
            RenderTargetHandle rt = createRenderTarget(1, depthTextureId);
            attachColorTexture2D(rt, 0, colorTextureId);
            return rt;
        }
        /// @brief Attach color 2D texture to render target (break @b RenderTarget)
        /// @param renderTargetId
        ///   Render target ID
        /// @param target
        ///   Color target index
        /// @param textureId
        ///   Color texture ID
        virtual void attachColorTexture2D(RenderTargetHandle renderTargetId,
                                          const uint target, Texture2DHandle textureId) = 0;
        /// @brief Detach texture from render target (break @b RenderTarget)
        /// @param renderTargetId
        ///   Render target ID
        /// @param target
        ///   Color target index
        virtual void detachColorTarget(RenderTargetHandle renderTargetId, const uint target) = 0;
        /// @brief Destroy render target
        /// @param renderTargetId
        ///   Render target ID
        virtual void destroyRenderTarget(RenderTargetHandle renderTargetId) = 0;

        /// @brief Bind render target region to pipeline
        /// @param renderTargetId
        ///   Render target ID
        /// @param x,y
        ///   Viewport offset
        /// @param w,h
        ///   Viewport size
        virtual void bindRenderTargetRegion(RenderTargetHandle renderTargetId,
                                            const uint x, const uint y,
                                            const uint w, const uint h) = 0;
        /// @brief Bind render target to pipeline
        void bindRenderTarget(RenderTargetHandle renderTargetId)
        {
            bindRenderTargetRegion(renderTargetId, 0, 0, 0, 0);
        }
        /// @brief Clear current render target color
        /// @param renderTargetId
        ///   Render target ID
        /// @param target
        ///   Target ID
        /// @param color
        ///   Clear color
        virtual void clearColor(RenderTargetHandle renderTargetId, 
                                const uint target, const float4& color) = 0;
        /// @brief Clear current render target depth and stencil
        /// @param renderTargetId
        ///   Render target ID
        /// @param depth
        ///   Clear depth
        /// @param stencil
        ///   Clear stencil value
        virtual void clearDepthStencil(RenderTargetHandle renderTargetId, 
                                       const float depth, const ubyte stencil) = 0;
        /// @brief Bind geometry source with current layout
        /// @param numVertexBuffers
        ///   Number of source buffer
        /// @param vertexBuffers
        ///   Source vertex buffers
        /// @param indexBuffer
        ///   Source index buffer
        virtual void bindGeometry(const uint numVertexBuffers, const BufferHandle vertexBuffers[],
                                  BufferHandle indexBuffer, const IndexFormat indexFormat) = 0;
        /// @brief Bind single vertex buffer with current layout
        void bindGeometry(BufferHandle vertexBuffer,
                          BufferHandle indexBuffer, IndexFormat indexFormat)
        {
            bindGeometry(1, &vertexBuffer, indexBuffer, indexFormat);
        }
        /// @brief Bind geometry bucket
        void bindGeometry(const GeometryBucket& geometry)
        {
            bindGeometry(geometry.numVertexBuffers, geometry.vertexBuffers.data(),
                         geometry.indexBuffer, geometry.indexFormat);
        }
        /// @brief Bind blend state to pipeline
        /// @param stateId
        ///   State ID
        /// @param stencilRef
        ///   Stencil reference value
        /// @param blendColor
        ///   Blend reference color
        virtual void bindState(StateHandle stateId, 
                               const ubyte stencilRef = 0, 
                               const float4& blendColor = float4(0.0f)) = 0;
        /// @brief Set effect resource by 2D texture
        /// @param resourceId
        ///   Resource ID
        /// @param textureId
        ///   2D texture ID
        /// @param shader
        ///   Resource binding shader
        /// @return @c true on success
        virtual bool bindTexture2D(EffectResourceHandle resourceId,
                                   Texture2DHandle textureId,
                                   const ShaderType shader) = 0;
        /// @brief Set effect resource
        /// @param slot
        ///   Destination slot
        /// @param resource
        ///   Resource handle
        /// @param shader
        ///   Destination shader
        void bindTexture(const uint slot, Texture2DHandle resource, const ShaderType shader)
        {
            ResourceBucket bucket;
            bucket.addResource(slot, resource, shader);
            bindResources(bucket);
        }
        /// @brief Set effect resources bucket
        virtual void bindResources(const ResourceBucket& bucket) = 0;

        /// @brief Draw non-indexed primitives
        /// @param startVertex
        ///   Start vertex
        /// @param numVertices
        ///   Number of vertices
        virtual void draw(const uint startVertex, const uint numVertices) = 0;
        /// @brief Draw non-indexed instanced primitives
        /// @param startVertex
        ///   Start vertex
        /// @param numVertices
        ///   Number of vertices
        /// @param startInstance
        ///   Start instance index
        /// @param numInstances
        ///   Number of instances
        virtual void drawInstanced(const uint startVertex, const uint numVertices,
                                   const uint startInstance, const uint numInstances) = 0;
        /// @brief Draw indexed primitives
        /// @param startIndex
        ///   Start index
        /// @param numIndices
        ///   Number of indices
        /// @param baseVertex
        ///   Base vertex
        virtual void drawIndexed(const uint startIndex, const uint numIndices,
                                 const uint baseVertex) = 0;
        /// @brief Draw indexed instanced primitives
        /// @param startIndex
        ///   Start index
        /// @param numIndices
        ///   Number of indices
        /// @param baseVertex
        ///   Base vertex
        /// @param startInstance
        ///   Start instance index
        /// @param numInstances
        ///   Number of instances
        virtual void drawIndexedInstanced(const uint startIndex, const uint numIndices,
                                          const uint baseVertex, 
                                          const uint startInstance, const uint numInstances) = 0;

        /// @brief Resize main viewport
        void resize(const uint width, const uint height) noexcept;
        /// @brief Get main viewport width
        const uint width() const noexcept;
        /// @brief Get main viewport height
        const uint height() const noexcept;
        /// @brief Check whether tessellation is supported
        bool hasTessellation() const;
        /// @brief Get effect includes library
        const usl::Library& effectLibrary() const;
        /// @brief Get effect target language
        virtual usl::TargetLanguage effectLanguage() const = 0;
        /// @brief Get statistics
        RendererStatistic getStats() const;
        /// @brief Reset statistics
        void resetStats();
    public:
        /// @brief Vertex buffer pool
        unique_ptr<BufferPool> vertexBufferPool;
    protected:
        /// @brief Whether logging is supported
        const bool& m_hasLogging;

        /// @brief Whether debug output is supported
        bool m_debugOutputSupport = false;
        /// @brief Whether tessellation is supported
        bool m_tessellationSupport = false;

        /// @brief Width
        uint m_width = 0;
        /// @brief Height
        uint m_height = 0;

        /// @brief Library
        usl::Library m_effectLibrary;

        /// @brief Statistics
        RendererStatistic m_stats;
    };
    /// @}
}

