#include "pch.h"
#include "ls/gapi/usl/compiler_hlsl.h"

#if LS_COMPILE_DX11 != 0
#include "ls/gapi/dx11/ImportDll.h"

using namespace ls;
namespace
{
    /// @name Converters
    /// @{
    const char* uniformTypeName(const UniformType type)
    {
        switch (type)
        {
        case UniformType::Float: return "float";
        case UniformType::Int: return "int";
        case UniformType::Uint: return "uint";
        case UniformType::Float2: return "float2";
        case UniformType::Int2: return "int2";
        case UniformType::Uint2: return "uint2";
        case UniformType::Float4: return "float4";
        case UniformType::Int4: return "int4";
        case UniformType::Uint4: return "uint4";
        case UniformType::Float4x4: return "float4x4";
        default: return "";
        }
    }
    const char* attribTypeName(const AttribType type)
    {
        switch (type)
        {
        case AttribType::Float: return "float";
        case AttribType::Int: return "int";
        case AttribType::Uint: return "uint";
        case AttribType::Float2: return "float2";
        case AttribType::Int2: return "int2";
        case AttribType::Uint2: return "uint2";
        case AttribType::Float3: return "float3";
        case AttribType::Int3: return "int3";
        case AttribType::Uint3: return "uint3";
        case AttribType::Float4: return "float4";
        case AttribType::Int4: return "int4";
        case AttribType::Uint4: return "uint4";
        default: return "";
        }
    }
    const char* resourceSamplerName(const usl::ResourceType type)
    {
        switch (type)
        {
        case usl::ResourceType::Texture2D:
            return "SamplerState";
        case usl::ResourceType::Shadow2D:
            return "SamplerComparisonState";
        default:
            return nullptr;
        }
    }
    const char* resourceTypeName(const usl::ResourceType type)
    {
        switch (type)
        {
        case usl::ResourceType::Texture2D:
        case usl::ResourceType::Shadow2D:
            return "Texture2D";
        default:
            return "";
        }
    }
    const char* geometryInput(const usl::GeometryInput type)
    {
        switch (type)
        {
        case usl::GeometryInput::Point:
            return "point";
        case usl::GeometryInput::Line:
            return "line";
        case usl::GeometryInput::LineAdjacency:
            return "lineadj";
        case usl::GeometryInput::Triangle:
            return "triangle";
        case usl::GeometryInput::TriangleAdjacency:
            return "triangleadj";
        default:
            return "";
        }
    }
    const char* geometryOutput(const usl::GeometryOutput type)
    {
        switch (type)
        {
        case usl::GeometryOutput::Point:
            return "PointStream";
        case usl::GeometryOutput::LineStrip:
            return "LineStream";
        case usl::GeometryOutput::TriangleStrip:
            return "TriangleStream";
        default:
            return "";
        }
    }
    uint geometryInputSize(const usl::GeometryInput type)
    {
        switch (type)
        {
        case usl::GeometryInput::Point:
            return 1;
        case usl::GeometryInput::Line:
            return 2;
        case usl::GeometryInput::LineAdjacency:
            return 4;
        case usl::GeometryInput::Triangle:
            return 3;
        case usl::GeometryInput::TriangleAdjacency:
            return 6;
        default:
            return 0;
        }
    }
    const char* tessDomain(const usl::TessPrimitive type)
    {
        switch (type)
        {
        case usl::TessPrimitive::Triangle:
            return "tri";
        case usl::TessPrimitive::Quad:
            return "quad";
        default:
            return "";
        }
    }
    const char* tessSpacing(const usl::TessSpacing type)
    {
        switch (type)
        {
        case usl::TessSpacing::Integer:
            return "integer";
        case usl::TessSpacing::Even:
            return "fractional_even";
        case usl::TessSpacing::Odd:
            return "fractional_odd";
        default:
            return "";
        }
    }
    const char* tessOutput(const usl::TessOutput type)
    {
        switch (type)
        {
        case usl::TessOutput::CW:
            return "triangle_cw";
        case usl::TessOutput::CCW:
            return "triangle_ccw";
        default:
            return "";
        }
    }
    string generateUniform(const usl::Effect::Uniform& var)
    {
        string text;
        text += uniformTypeName(var.type);
        text += " ";
        text += var.name;
        if (var.length > 1)
        {
            text += "[";
            text += format("{*}", var.length);
            text += "]";
        }
        return text;
    }
    string generateAttrib(const usl::Effect::Attrib& var)
    {
        string text;
        text += attribTypeName(var.type);
        text += " ";
        text += var.name;
        if (var.length > 1)
        {
            text += "[";
            text += format("{*}", var.length);
            text += "]";
        }
        return text;
    }
    /// @}

    /// @brief Expand arrays
    void expandArrays(string& text)
    {
        // I DO know...
        size_t dog = 0;
        while ((dog = text.find_first_of('@', dog)) != string::npos)
        {
            // Find array
            if (text.length() <= dog)
                break;
            const size_t opener = text.find_first_of('{', dog + 1);
            if (opener == string::npos)
                break;
            if (text.length() <= opener)
                break;
            const size_t closer = text.find_first_of('}', opener + 1);
            if (closer == string::npos)
                break;

            // Do array
            for (size_t index = dog; index < opener; ++index)
                text[index] = ' ';
        }
    }
    /// @brief Compile source code
    vector<ubyte> compileSource(const string& name, const string& text, const ShaderType type)
    {
        // Version
        string version;
        switch (type)
        {
        case ShaderType::Vertex:
            version = "vs_4_0";
            break;
        case ShaderType::TessControl:
            version = "hs_5_0";
            break;
        case ShaderType::TessEval:
            version = "ds_5_0";
            break;
        case ShaderType::Geometry:
            version = "gs_4_0";
            break;
        case ShaderType::Pixel:
            version = "ps_4_0";
            break;
        default:
            break;
        }

        // Compile
        DWORD flags = D3DCOMPILE_OPTIMIZATION_LEVEL3;
        flags |= D3DCOMPILE_PACK_MATRIX_COLUMN_MAJOR;
        flags |= D3DCOMPILE_ENABLE_BACKWARDS_COMPATIBILITY;

        ID3D10Blob* shaderBlob;
        ID3D10Blob* errorBlob;
        const HRESULT result = D3DCompileDll(text.c_str(), text.length(),
                                             nullptr, nullptr, nullptr,
                                             "main", version.c_str(), flags, 0,
                                             &shaderBlob, &errorBlob);
        // Error
        if (FAILED(result))
        {
            stringstream error;
            if (!errorBlob)
            {
                error << "Unknown error [" << result << "] in shader [" << name << "]";
            }
            else
            {
                error << "Shader [" << name << "] compilation error:" << endl;
                error << reinterpret_cast<const char*>(errorBlob->GetBufferPointer());
            }
            error << "\nShader listing:\n\n";
            error << usl::generateListing(text);
            LS_THROW(0, GapiEffectException, error.str());
        }

        // Save
        const uint shaderSize = (uint) shaderBlob->GetBufferSize();
        vector<ubyte> buffer(shaderSize);
        memcpy(buffer.data(), shaderBlob->GetBufferPointer(), shaderSize);
        return std::move(buffer);
    }
}

namespace ls
{
    namespace usl
    {
        string Compiler<TargetLanguage::HLSL30>::generateHeader(const usl::Effect& effect)
        {
            stringstream text;

            // Write constants
            uint bufferIndex = 0;
            for (auto& buffer : effect.buffers)
            {
                text << "//! Constant buffer : " << buffer.name << endl;
                text << "cbuffer " << buffer.name << " : register(b" << bufferIndex << ")" << endl;
                text << "{" << endl;
                uint uniformIndex = 0;
                for (string const& uniformName : buffer.uniformsOrder)
                {
                    auto& uniform = getOrFail(buffer.uniforms, uniformName);
                    text << "    " << generateUniform(uniform) << ": packoffset(c" << uniformIndex << ");" << endl;
                    uniformIndex += uniformRegisterStride(uniform.type) * uniform.length;
                }
                text << "};" << endl << endl;
                ++bufferIndex;
            }

            // Write resources
            uint resourceIndex = 0;
            for (auto& resource : effect.resources)
            {
                const char* sampler = resourceSamplerName(resource.type);
                const string typeName = resourceTypeName(resource.type);
                const string componentName = uniformTypeName(resource.component);
                text << "//! Resource : " << resource.name << endl;
                if (sampler)
                {
                    text << sampler << " _" << resource.name << "_sampler "
                        << ": register(s" << resourceIndex << ");" << endl;
                }
                text << typeName << "<" << componentName << "> " << resource.name
                    << ": register(t" << resourceIndex << ");" << endl;
                ++resourceIndex;
            }
            text << endl;

            return text.str();
        }
        vector<ubyte> Compiler<TargetLanguage::HLSL30>::compile(
            const string& header, 
            const usl::Effect::VertexShader& shader) const
        {
            stringstream text;
            text << header << endl;

            // Write layout
            text << "//! Layout : " << shader.layout->name << endl;
            text << "struct _InputStruct" << endl;
            text << "{" << endl;
            for (string const& attribName : shader.layout->attribsOrder)
            {
                auto& attrib = getOrFail(shader.layout->attribs, attribName);
                text << "    " << generateAttrib(attrib) << " : " << attrib.name << ";" << endl;
            }
            text << "    uint _VertexID : SV_VertexID;" << endl;
            text << "    uint _InstanceID : SV_InstanceID;" << endl;
            text << "};" << endl;
            text << endl;

            // Write input aliases
            for (string const& attribName : shader.layout->attribsOrder)
            {
                text << "#define in" << attribName << " _in." << attribName << endl;
            }
            text << endl;

            // Write output
            text << "//! Output : " << shader.output->name << endl;
            text << "struct _OutputStruct" << endl;
            text << "{" << endl;
            for (auto& iterOutput : shader.output->vars)
            {
                auto& out = iterOutput.second;
                if (out.name != "Position")
                    text << "    " << generateAttrib(out) << " : " << out.name << ";" << endl;
                else
                    text << "    " << generateAttrib(out) << " : SV_POSITION;" << endl;
            }
            text << "};" << endl;

            // Write output aliases
            for (auto& iterOutput : shader.output->vars)
            {
                auto& outName = iterOutput.second.name;
                text << "#define out" << outName << " _out." << outName << endl;
            }
            text << endl;

            // Write includes
            text << "//! Includes" << endl;
            text << shader.text << endl;
            text << endl;

            // Write defines
            text << "#define inVertexID _in._VertexID" << endl;
            text << "#define inInstanceID _in._InstanceID" << endl;

            // Write main
            text << "//! Main" << endl;
            text << "_OutputStruct main(_InputStruct _in)" << endl;
            text << "{" << endl;
            text << "    _OutputStruct _out;" << endl;
            text << shader.main << endl;
            text << "    return _out;";
            text << "}" << endl;

            string str = text.str();
            expandArrays(str);
            return compileSource(shader.name, str, shader.Type);
        }
        vector<ubyte> Compiler<TargetLanguage::HLSL30>::compile(
            const string& header, 
            const usl::Effect::HullShader& shader) const
        {
            stringstream text;
            text << header << endl;

            // Write input
            text << "//! Input : " << shader.input->name << endl;
            text << "struct _InputStruct" << endl;
            text << "{" << endl;
            for (auto& iterInput : shader.input->vars)
            {
                auto& input = iterInput.second;
                if (input.name != "Position")
                    text << "    " << generateAttrib(input) << " : " << input.name << ";" << endl;
                else
                    text << "    " << generateAttrib(input) << " : SV_POSITION;" << endl;
            }
            text << "};" << endl;
            text << endl;

            // Write input aliases
            for (auto& iterInput : shader.input->vars)
            {
                const string& inputName = iterInput.first;
                text << "#define in" << inputName << "(I) _in[I]." << inputName << endl;
            }
            text << endl;

            // Write output
            text << "//! Output : " << shader.output->name << endl;
            text << "struct _OutputStruct" << endl;
            text << "{" << endl;
            for (auto& iterOutput : shader.output->vars)
            {
                auto& out = iterOutput.second;
                if (out.name != "Position")
                    text << "    " << generateAttrib(out) << " : " << out.name << ";" << endl;
                else
                    text << "    " << generateAttrib(out) << " : SV_POSITION;" << endl;
            }
            text << "};" << endl;

            // Write output aliases
            for (auto& iterOutput : shader.output->vars)
            {
                auto& outName = iterOutput.second.name;
                text << "#define out" << outName << " _out." << outName << endl;
            }
            text << endl;

            // Write output const and output const aliases
            text << "//! Constant : " << shader.output->name << endl;
            text << "struct _ConstOutputStruct" << endl;
            text << "{" << endl;
            switch (shader.param.primitive)
            {
            case TessPrimitive::Quad:
                text << "    float _Inner[2] : SV_InsideTessFactor; " << endl;
                text << "    float _Outer[4] : SV_TessFactor; " << endl;
                text << "#define outInnerFactor(I) _out._Inner[I]" << endl;
                text << "#define outOuterFactor(I) _out._Outer[I]" << endl;
                break;
            case TessPrimitive::Triangle:
                text << "    float _Inner : SV_InsideTessFactor; " << endl;
                text << "    float _Outer[3] : SV_TessFactor; " << endl;
                text << "#define outInnerFactor(I) _out._Inner" << endl;
                text << "#define outOuterFactor(I) _out._Outer[I]" << endl;
            default:
                break;
            }
            text << "};" << endl;
            text << endl;

            // Write includes
            text << "//! Includes" << endl;
            text << shader.text << endl;
            text << endl;

            // Write const function
            text << "_ConstOutputStruct _ConstHullFunc("
                << "InputPatch<_InputStruct, " << shader.param.inputSize << "> _in, "
                << "uint _PatchID : SV_PrimitiveID)" << endl;
            text << "{" << endl;
            text << "    _ConstOutputStruct _out;" << endl;
            text << shader.mainConst;
            text << "    return _out;" << endl;
            text << "}" << endl;
            text << endl;

            // Write defines
            text << "#define inCurrentVertex _PointID" << endl;

            // Write main
            text << "//! Main" << endl;
            text << "[domain(\"" << tessDomain(shader.param.primitive) << "\")]" << endl;
            text << "[partitioning(\"" << tessSpacing(shader.param.spacing) << "\")]" << endl;
            text << "[outputtopology(\"" << tessOutput(shader.param.output) << "\")]" << endl;
            text << "[outputcontrolpoints(" << shader.param.outputSize << ")]" << endl;
            text << "[patchconstantfunc(\"_ConstHullFunc\")]" << endl;
            text << "_OutputStruct main(InputPatch<_InputStruct, " << shader.param.inputSize << "> _in, "
                << "uint _PointID : SV_OutputControlPointID, "
                << "uint _PatchID : SV_PrimitiveID)" << endl;
            text << "{" << endl;
            text << "    _OutputStruct _out;" << endl;
            text << shader.main << endl;
            text << "    return _out;" << endl;
            text << "}" << endl;

            string str = text.str();
            expandArrays(str);
            return compileSource(shader.name, str, shader.Type);
        }
        vector<ubyte> Compiler<TargetLanguage::HLSL30>::compile(
            const string& header, 
            const usl::Effect::DomainShader& shader) const
        {
            stringstream text;
            text << header << endl;

            // Write input
            text << "//! Input : " << shader.input->name << endl;
            text << "struct _InputStruct" << endl;
            text << "{" << endl;
            for (auto& iterInput : shader.input->vars)
            {
                auto& input = iterInput.second;
                if (input.name != "Position")
                    text << "    " << generateAttrib(input) << " : " << input.name << ";" << endl;
                else
                    text << "    " << generateAttrib(input) << " : SV_POSITION;" << endl;
            }
            text << "};" << endl;
            text << endl;

            // Write input aliases
            for (auto& iterInput : shader.input->vars)
            {
                const string& inputName = iterInput.first;
                text << "#define in" << inputName << "(I) _in[I]." << inputName << endl;
            }
            text << endl;

            // Write output
            text << "//! Output : " << shader.output->name << endl;
            text << "struct _OutputStruct" << endl;
            text << "{" << endl;
            for (auto& iterOutput : shader.output->vars)
            {
                auto& out = iterOutput.second;
                if (out.name != "Position")
                    text << "    " << generateAttrib(out) << " : " << out.name << ";" << endl;
                else
                    text << "    " << generateAttrib(out) << " : SV_POSITION;" << endl;
            }
            text << "};" << endl;

            // Write output aliases
            for (auto& iterOutput : shader.output->vars)
            {
                auto& outName = iterOutput.second.name;
                text << "#define out" << outName << " _out." << outName << endl;
            }
            text << endl;

            // Write intrinsics
            text << "#define qlerp(F) lerp("
                "lerp(F(0), F(1), _TessCoord.x), "
                "lerp(F(3), F(2), _TessCoord.x), "
                "_TessCoord.y)" << endl;

            // Write hull output const and output const aliases
            text << "//! Constant : " << shader.output->name << endl;
            text << "struct _ConstOutputStruct" << endl;
            text << "{" << endl;
            switch (shader.param.primitive)
            {
            case TessPrimitive::Quad:
                text << "    float _Inner[2] : SV_InsideTessFactor; " << endl;
                text << "    float _Outer[4] : SV_TessFactor; " << endl;
                text << "#define outInnerFactor(I) _constIn._Inner[I]" << endl;
                text << "#define outOuterFactor(I) _constIn._Outer[I]" << endl;
                break;
            case TessPrimitive::Triangle:
                text << "    float _Inner : SV_InsideTessFactor; " << endl;
                text << "    float _Outer[3] : SV_TessFactor; " << endl;
                text << "#define outInnerFactor(I) _constIn._Inner" << endl;
                text << "#define outOuterFactor(I) _constIn._Outer[I]" << endl;
            default:
                break;
            }
            text << "};" << endl;
            text << endl;

            // Write includes
            text << "//! Includes" << endl;
            text << shader.text << endl;
            text << endl;

            // Write main
            text << "//! Main" << endl;
            text << "[domain(\"" << tessDomain(shader.param.primitive) << "\")]" << endl;
            text << "_OutputStruct main(OutputPatch<_InputStruct, " << shader.param.inputSize << "> _in, "
                << "_ConstOutputStruct _constIn, ";
            switch (shader.param.primitive)
            {
            case TessPrimitive::Quad:
                text << "float2 _TessCoord : SV_DomainLocation)" << endl;
                break;
            case TessPrimitive::Triangle:
                text << "float3 _TessCoord : SV_DomainLocation)" << endl;
                break;
            default:
                break;
            }
            text << "{" << endl;
            text << "    _OutputStruct _out;" << endl;
            text << shader.main << endl;
            text << "    return _out;" << endl;
            text << "}" << endl;

            string str = text.str();
            expandArrays(str);
            return compileSource(shader.name, str, shader.Type);
        }
        vector<ubyte> Compiler<TargetLanguage::HLSL30>::compile(
            const string& header, 
            const usl::Effect::GeometryShader& shader) const
        {
            stringstream text;
            text << header << endl;

            // Write input
            text << "//! Input : " << shader.input->name << endl;
            text << "struct _InputStruct" << endl;
            text << "{" << endl;
            for (auto& iterInput : shader.input->vars)
            {
                auto& input = iterInput.second;
                if (input.name != "Position")
                    text << "    " << generateAttrib(input) << " : " << input.name << ";" << endl;
                else
                    text << "    " << generateAttrib(input) << " : SV_POSITION;" << endl;
            }
            text << "};" << endl;
            text << endl;

            // Write input aliases
            for (auto& iterInput : shader.input->vars)
            {
                const string& inputName = iterInput.first;
                text << "#define in" << inputName << "(I) _in[I]." << inputName << endl;
            }
            text << endl;

            // Write output
            text << "//! Output : " << shader.output->name << endl;
            text << "struct _OutputStruct" << endl;
            text << "{" << endl;
            for (auto& iterOutput : shader.output->vars)
            {
                auto& out = iterOutput.second;
                if (out.name != "Position")
                    text << "    " << generateAttrib(out) << " : " << out.name << ";" << endl;
                else
                    text << "    " << generateAttrib(out) << " : SV_POSITION;" << endl;
            }
            text << "};" << endl;

            // Write output aliases
            for (auto& iterOutput : shader.output->vars)
            {
                auto& outName = iterOutput.second.name;
                text << "#define out" << outName << " _out." << outName << endl;
            }
            text << endl;

            // Write intrinsics
            text << "#define emitVertex _outStream.Append(_out)" << endl;
            text << "#define endPrimitive _outStream.RestartStrip()" << endl;

            // Write includes
            text << "//! Includes" << endl;
            text << shader.text << endl;
            text << endl;

            // Write main
            uint inputSize = geometryInputSize(shader.inputPrimitive);
            text << "//! Main" << endl;
            text << "[maxvertexcount(" << shader.outputSize << ")]" << endl;
            text << "void main(" << geometryInput(shader.inputPrimitive) << " _InputStruct _in[" << inputSize << "], "
                << "inout " << geometryOutput(shader.outputPrimitive) << "<_OutputStruct> _outStream)" << endl;
            text << "{" << endl;
            text << "    _OutputStruct _out;" << endl;
            text << shader.main;
            text << "}" << endl;

            string str = text.str();
            expandArrays(str);
            return compileSource(shader.name, str, shader.Type);
        }
        vector<ubyte> Compiler<TargetLanguage::HLSL30>::compile(
            const string& header, 
            const usl::Effect::PixelShader& shader) const
        {
            stringstream text;
            text << header << endl;

            // Write input
            text << "//! Input : " << shader.input->name << endl;
            text << "struct _InputStruct" << endl;
            text << "{" << endl;
            for (auto& iterInput : shader.input->vars)
            {
                auto& input = iterInput.second;
                if (input.name != "Position")
                    text << "    " << generateAttrib(input) << " : " << input.name << ";" << endl;
                else
                    text << "    " << generateAttrib(input) << " : SV_POSITION;" << endl;
            }
            text << "    uint _IsFrontFace : SV_IsFrontFace;" << endl;
            text << "};" << endl;
            text << endl;

            // Write input aliases
            for (auto& iterAttrib : shader.input->vars)
            {
                const string& name = iterAttrib.first;
                text << "#define in" << name << " _in." << name << endl;
            }

            // Write output
            text << "//! Output : " << shader.target->name << endl;
            text << "struct _OutputStruct" << endl;
            text << "{" << endl;
            for (auto& iterTarget : shader.target->vars)
            {
                auto& target = iterTarget.second;
                if (target.name != "Depth")
                    text << "    float4 " << target.name << " : SV_Target" << target.index << ";" << endl;
                else
                    text << "    float Depth : SV_Depth;" << endl;
            }
            text << "};" << endl;

            // Write output aliases
            text << "//! Target : " << shader.target->name << endl;
            for (auto& iterTarget : shader.target->vars)
            {
                auto& target = iterTarget.second;
                text << "#define out" << target.name << " _out." << target.name << "" << endl;
            }
            text << endl;

            // Write includes
            text << "//! Includes" << endl;
            text << shader.text << endl;
            text << endl;

            // Write defines
            text << "#define isFrontFace _in._IsFrontFace" << endl;
            text << "#define isBackFace !_in._IsFrontFace" << endl;

            // Write main
            text << "//! Main" << endl;
            text << "_OutputStruct main(_InputStruct _in)" << endl;
            text << "{" << endl;
            text << "    _OutputStruct _out;" << endl;
            text << shader.main << endl;
            text << "    return _out;";
            text << "}" << endl;

            string str = text.str();
            expandArrays(str);
            return compileSource(shader.name, str, shader.Type);
        }
    }
}
#endif