/// @file ls/gapi/usl/EffectCompilerDX11.h
/// @brief USL Effect Compiler
#pragma once
#include "ls/common.h"
#include "ls/io/StreamInterface.h"
#include "ls/gapi/desc/EffectDesc.h"
#include "ls/gapi/usl/usl.h"

namespace ls
{
    namespace usl
    {
        /// @brief Compiler
        template <TargetLanguage Target>
        class Compiler;
        /// @brief Compile shaders of specified type
        template <TargetLanguage Target, class Shader>
        void compileShaders(Compiler<Target> compiler, const string& header,
                            const map<string, uint>& indices, const map<string, Shader>& shaders,
                            vector<ShaderType>& shadersTypes, vector<uint>& shadersSizes,
                            vector<vector<ubyte>>& shadersData)
        {
            for (auto& iterShader : shaders)
            {
                const string& shaderName = iterShader.first;
                const Shader& shader = iterShader.second;

                const uint shaderIndex = getOrFail(indices, shaderName);
                const vector<ubyte> data = compiler.compile(header, shader);

                shadersTypes[shaderIndex] = Shader::Type;
                shadersSizes[shaderIndex] = data.size();
                shadersData[shaderIndex] = move(data);
            }
        }
        /// @brief Compile effect and save to stream
        template <TargetLanguage Target>
        void compileEffect(Compiler<Target> compiler,
                           const usl::Effect& effect, StreamInterface& dest)
        {
            const string header = compiler.generateHeader(effect);

            // Prepare layouts
            const uint numLayouts = effect.numLayouts();
            vector<EffectLayoutDesc> layouts;
            map<string, uint> layoutsNames;
            for (auto& iterLayout : effect.layouts)
            {
                const string& layoutName = iterLayout.first;
                auto& sourceLayout = iterLayout.second;
                EffectLayoutDesc layout;
                string2array<MaxNameLength>(layout.name, layoutName);
                layout.numAttribs = sourceLayout.attribs.size();
                uint index = 0;
                for (string const& attribName : sourceLayout.attribsOrder)
                {
                    string2array<MaxNameLength>(layout.attribs[index], attribName);
                    ++index;
                }
                layoutsNames[sourceLayout.name] = (uint) layouts.size();
                layouts.push_back(layout);
            }

            // Prepare programs
            vector<EffectProgramDesc> programs;
            for (auto& iterProgram : effect.programs)
            {
                auto& sourceProgram = iterProgram.second;
                EffectProgramDesc program;
                string2array<MaxNameLength>(program.name, sourceProgram.name);

                // Do pixel-vertex
                program.layout = layoutsNames[sourceProgram.vertex->layout->name];
                program.vertex = getOrFail(effect.shaderIndices, sourceProgram.vertex->name);
                program.pixel = getOrFail(effect.shaderIndices, sourceProgram.pixel->name);
                if (sourceProgram.geometry)
                    program.geometry = getOrFail(effect.shaderIndices, sourceProgram.geometry->name);
                if (sourceProgram.hull)
                    program.hull = getOrFail(effect.shaderIndices, sourceProgram.hull->name);
                if (sourceProgram.domain)
                    program.domain = getOrFail(effect.shaderIndices, sourceProgram.domain->name);

                programs.push_back(program);
            }
            const uint numPrograms = (uint) programs.size();

            // Prepare shaders
            const uint numShaders = effect.numShaders();
            vector<uint> shadersSizes(numShaders);
            vector<ShaderType> shadersTypes(numShaders);
            vector<vector<ubyte>> shadersSources(numShaders);

            compileShaders(compiler, header, effect.shaderIndices, effect.vertexShaders,
                           shadersTypes, shadersSizes, shadersSources);
            compileShaders(compiler, header, effect.shaderIndices, effect.hullShaders,
                           shadersTypes, shadersSizes, shadersSources);
            compileShaders(compiler, header, effect.shaderIndices, effect.domainShaders,
                           shadersTypes, shadersSizes, shadersSources);
            compileShaders(compiler, header, effect.shaderIndices, effect.geometryShaders,
                           shadersTypes, shadersSizes, shadersSources);
            compileShaders(compiler, header, effect.shaderIndices, effect.pixelShaders,
                           shadersTypes, shadersSizes, shadersSources);

            // Prepare uniform buffers
            const uint numBuffers = effect.buffers.size();
            vector<MultiArray<ubyte, MaxNameLength>> bufferNames;
            vector<uint> buffersSizes;
            for (auto& buffer : effect.buffers)
            {
                const string& blockName = buffer.name;
                MultiArray<ubyte, MaxNameLength> name = 0;
                string2array<MaxNameLength>(name, blockName);

                bufferNames.push_back(name);
                buffersSizes.push_back(buffer.size);
            }

            // Prepare resources
            const uint numResources = (uint) effect.resources.size();
            vector<MultiArray<char, MaxNameLength>> resourceNames;
            for (auto& resource : effect.resources)
            {
                const string& resourceName = resource.name;
                MultiArray<char, MaxNameLength> name = 0;
                strncpy(name.data(), resourceName.c_str(), resourceName.length());
                resourceNames.push_back(name);
            }

            // Write
            dest.write(&numPrograms, sizeof(uint));
            dest.write(&numShaders, sizeof(uint));
            dest.write(&numLayouts, sizeof(uint));
            dest.write(&numBuffers, sizeof(uint));
            dest.write(&numResources, sizeof(uint));
            dest.write(programs.data(), sizeof(EffectProgramDesc) * numPrograms);
            dest.write(shadersTypes.data(), sizeof(ShaderType) * numShaders);
            dest.write(shadersSizes.data(), sizeof(uint) * numShaders);
            for (vector<ubyte> const& shader : shadersSources)
                dest.write(shader.data(), shader.size());
            dest.write(layouts.data(), sizeof(EffectLayoutDesc) * numLayouts);
            dest.write(bufferNames.data(), MaxNameLength * numBuffers);
            dest.write(buffersSizes.data(), sizeof(uint) * numBuffers);
            dest.write(resourceNames.data(), MaxNameLength * numResources);
        }
        /// @brief Read effect data from stream
        inline void deserializeEffect(StreamInterface& source, EffectDesc& desc)
        {
            // Header
            uint numPrograms;
            uint numShaders;
            uint numLayouts;
            uint numBuffers;
            uint numResources;

            // Read header
            source.read(&numPrograms, sizeof(uint));
            source.read(&numShaders, sizeof(uint));
            source.read(&numLayouts, sizeof(uint));
            source.read(&numBuffers, sizeof(uint));
            source.read(&numResources, sizeof(uint));

            // Read shaders info
            desc.programs.resize(numPrograms);
            desc.shadersSizes.resize(numShaders);
            desc.shadersTypes.resize(numShaders);
            source.read(desc.programs.data(), sizeof(EffectProgramDesc) * numPrograms);
            source.read(desc.shadersTypes.data(), sizeof(ShaderType) * numShaders);
            source.read(desc.shadersSizes.data(), sizeof(uint) * numShaders);

            // Update sizes
            uint offset = 0;
            for (uint& size : desc.shadersSizes)
            {
                desc.shadersOffsets.push_back(offset);
                offset += size;
            }
            const uint totalSize = std::accumulate(desc.shadersSizes.begin(),
                                                   desc.shadersSizes.end(),
                                                   0);

            // Read shaders
            desc.shadersData.resize(totalSize);
            source.read(desc.shadersData.data(), totalSize);

            // Read layouts
            desc.layouts.resize(numLayouts);
            if (numLayouts > 0)
                source.read(desc.layouts.data(), sizeof(EffectLayoutDesc) * numLayouts);

            // Read buffers
            vector<array<char, MaxNameLength> > bufferNames(numBuffers);
            desc.buffersSizes.resize(numBuffers);
            if (numBuffers > 0)
            {
                source.read(bufferNames.data(), MaxNameLength * numBuffers);
                source.read(desc.buffersSizes.data(), sizeof(uint) * numBuffers);
            }

            // Read resources
            vector<array<char, MaxNameLength> > resourceNames(numResources);
            if (numResources > 0)
                source.read(resourceNames.data(), MaxNameLength * numResources);

            // Parse programs
            for (uint programIndex = 0; programIndex < numPrograms; ++programIndex)
            {
                const char* programName = reinterpret_cast<const char*>(desc.programs[programIndex].name.data());
                desc.programNames.emplace(programName, programIndex);
            }

            // Parse layouts
            for (EffectLayoutDesc& layout : desc.layouts)
            {
                // Decode
                const string layoutName = reinterpret_cast<const char*>(layout.name.data());

                hamap<string, uint> attribNames;
                for (uint attribIndex = 0; attribIndex < layout.numAttribs; ++attribIndex)
                {
                    attribNames[layout.attribName(attribIndex)] = attribIndex;
                }

                // Save
                desc.layoutsUnpacked.push_back(move(attribNames));
                desc.layoutsNames.emplace(layoutName, desc.layoutsNames.size());
            }

            // Parse buffers
            for (uint bufferIndex = 0; bufferIndex < numBuffers; ++bufferIndex)
            {
                desc.bufferNames.emplace(bufferNames[bufferIndex].data(),
                                         bufferIndex);
            }

            // Parse resources
            for (uint resourceIndex = 0; resourceIndex < numResources; ++resourceIndex)
            {
                desc.resourceNames.emplace(resourceNames[resourceIndex].data(),
                                           resourceIndex);
            }
        }
    }
}
