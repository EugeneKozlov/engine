#include "pch.h"
#include "ls/gapi/usl/usl.h"

#define BOOST_WAVE_SUPPORT_MS_EXTENSIONS 0
#include <boost/algorithm/string.hpp>
#include <boost/exception/diagnostic_information.hpp>
#include <boost/wave.hpp>
#include <boost/wave/cpplexer/cpp_lex_iterator.hpp>
#include <regex>

using namespace ls;
namespace ls
{
    template <class Stream>
    Stream& operator >>(Stream& stm, usl::ResourceType& value)
    {
        using namespace usl;
        const static map<string, ResourceType> m = {
            make_pair(string("texture2D"), ResourceType::Texture2D),
            make_pair(string("shadow2D"), ResourceType::Shadow2D),
        };
        return defaultEnumReader(stm, value, m, ResourceType::Unknown);
    }
    template <class Stream>
    Stream& operator >>(Stream& stm, usl::GeometryInput& value)
    {
        using namespace usl;
        const static map<string, GeometryInput> m = {
            make_pair(string("point"), GeometryInput::Point),
            make_pair(string("line"), GeometryInput::Line),
            make_pair(string("line_adj"), GeometryInput::LineAdjacency),
            make_pair(string("triangle"), GeometryInput::Triangle),
            make_pair(string("triangle_adj"), GeometryInput::TriangleAdjacency)
        };
        return defaultEnumReader(stm, value, m, GeometryInput::Unknown);
    }
    template <class Stream>
    Stream& operator >>(Stream& stm, usl::GeometryOutput& value)
    {
        using namespace usl;
        const static map<string, GeometryOutput> m = {
            make_pair(string("point"), GeometryOutput::Point),
            make_pair(string("line_strip"), GeometryOutput::LineStrip),
            make_pair(string("triangle_strip"), GeometryOutput::TriangleStrip),
        };
        return defaultEnumReader(stm, value, m, GeometryOutput::Unknown);
    }
    template <class Stream>
    Stream& operator >>(Stream& stm, usl::TessPrimitive& value)
    {
        using namespace usl;
        const static map<string, TessPrimitive> m = {
            make_pair(string("triangle"), TessPrimitive::Triangle),
            make_pair(string("quad"), TessPrimitive::Quad),
        };
        return defaultEnumReader(stm, value, m, TessPrimitive::Unknown);
    }
    template <class Stream>
    Stream& operator >>(Stream& stm, usl::TessOutput& value)
    {
        using namespace usl;
        const static map<string, TessOutput> m = {
            make_pair(string("cw"), TessOutput::CW),
            make_pair(string("ccw"), TessOutput::CCW),
        };
        return defaultEnumReader(stm, value, m, TessOutput::Unknown);
    }
    template <class Stream>
    Stream& operator >>(Stream& stm, usl::TessSpacing& value)
    {
        using namespace usl;
        const static map<string, TessSpacing> m = {
            make_pair(string("integer"), TessSpacing::Integer),
            make_pair(string("even"), TessSpacing::Even),
            make_pair(string("odd"), TessSpacing::Odd),
        };
        return defaultEnumReader(stm, value, m, TessSpacing::Unknown);
    }
}
namespace
{
    using namespace boost::wave;
    /// @brief Base Context
    template <
        class InputPolicy = iteration_context_policies::load_file_to_string,
        class ContextPolicy = context_policies::eat_whitespace<cpplexer::lex_token<>>
    >
    using BaseContext = context<
        string::iterator,
        cpplexer::lex_iterator<cpplexer::lex_token<>>,
        InputPolicy, ContextPolicy
    >;
    /// @brief Accumulate tokens to string
    template <class Iterator>
    string accumulateTokens(Iterator begin, Iterator end)
    {
        string text;
        for (auto& iter : make_pair(begin, end))
        {
            text += iter.get_value().c_str();
        }
        return text;
    }
    /// @brief Custom Context
    template <class InputPolicy, class ContextPolicy>
    class WaveContext
        : public BaseContext<InputPolicy, ContextPolicy>
    {
    public:
        /// @brief Ctor
        WaveContext(const string& name, string& text, const usl::Library& includes)
            : BaseContext<InputPolicy, ContextPolicy>(text.begin(), text.end(), name.c_str())
            , m_includes(includes)
        {

        }
        /// @brief Has include?
        bool has_include(const string& name) const
        {
            return m_includes.count(name) != 0;
        }
        /// @brief Find include
        string load_include(const string& name) const
        {
            return getOrFail(m_includes, name) + "\n";
        }
        /// @brief Process
        string process()
        {
            return accumulateTokens(this->begin(), this->end());
        }
    private:
        const usl::Library& m_includes;
    };
    /// @brief Input Policy for Wave Context
    struct LoadFileFromHash
    {
        template <class IterContextT>
        class inner
        {
        public:
            template <class PositionT>
            static void init_iterators(IterContextT &iter_ctx,
                                       PositionT const &act_pos, language_support language);
        private:
            std::string instring;
        };
    };
    /// @brief Pre-process unknown directive
    template <class ContextT, class ContainerT>
    bool preprocessUnknownDirective(ContextT const& ctx, ContainerT const& lineTokens,
                                    ContainerT& pending)
    {
        // Make nested context to parse directive
        string line = accumulateTokens(std::next(std::find(lineTokens.begin(),
                                                           lineTokens.end(),
                                                           T_POUND)),
                                       lineTokens.end());
        BaseContext<> inner(line.begin(), line.end());
        for (const auto& macroName : make_pair(ctx.macro_names_begin(),
                                               ctx.macro_names_end()))
        {
            bool hasPars = false;
            bool predef = false;
            BaseContext<>::position_type pos;
            std::vector<BaseContext<>::token_type> pars;
            BaseContext<>::token_sequence_type def;
            ctx.get_macro_definition(macroName, hasPars, predef, pos, pars, def);
            if (!inner.is_defined_macro(macroName))
            {
                inner.add_macro_definition(macroName, pos, hasPars, pars, def, predef);
            }
        }

        // Build tokens
        pending.push_back(typename ContainerT::value_type(T_POUND, "#", BaseContext<>::position_type()));
        pending.insert(pending.end(), inner.begin(), inner.end());
        return true;
    }
    /// @brief Context Policy
    struct ContextHooks
        : boost::wave::context_policies::eat_whitespace<cpplexer::lex_token<>>
    {
        template <class ContextT>
        bool locate_include_file(ContextT& ctx, std::string &file_path,
                                 bool /*is_system*/, char const * /*current_name*/,
                                 std::string &/*dir_path*/,
                                 std::string &native_name)
        {
            auto& context = static_cast<WaveContext<LoadFileFromHash, ContextHooks>&>(ctx);
            if (!context.has_include(file_path))
                return false;
            native_name = file_path;
            return true;
        }
        template <class ContextT, class ContainerT>
        bool found_unknown_directive(ContextT const& ctx, ContainerT const& lineTokens,
                                     ContainerT& pending)
        {
            return preprocessUnknownDirective(ctx, lineTokens, pending);
        }
    };
    template <class IterContextT>
    template <class PositionT>
    void LoadFileFromHash::inner<IterContextT>::init_iterators(IterContextT &iter_ctx,
                                                               PositionT const &act_pos, language_support language)
    {
        typedef typename IterContextT::iterator_type iterator_type;
        auto& context = static_cast<WaveContext<LoadFileFromHash, ContextHooks>&>(iter_ctx.ctx);

        // Check
        if (!context.has_include(iter_ctx.filename.c_str()))
        {
            BOOST_WAVE_THROW_CTX(iter_ctx.ctx, preprocess_exception,
                                 bad_include_file, iter_ctx.filename.c_str(), act_pos);
            return;
        }

        // Read
        iter_ctx.instring = context.load_include(iter_ctx.filename.c_str());

        iter_ctx.first = iterator_type(
            iter_ctx.instring.begin(), iter_ctx.instring.end(),
            PositionT(iter_ctx.filename), language);
        iter_ctx.last = iterator_type();
    }
}
// Extract regions
const char* usl::asString(const TargetLanguage target)
{
    switch (target)
    {
    case TargetLanguage::HLSL30:
        return "HLSL30";
    default:
        return "";
    }
}
void usl::extractRegionsFromSource(string& source, Library& regions)
{
    // Regexes
    static std::regex beginRegionRegex(R"(\s*#\s*region(?:[ \t]+([\w\.]+))?\s*)", std::regex::optimize);
    static std::regex endRegionRegex(R"(\s*#\s*endregion\s*)", std::regex::optimize);
    static std::regex redefineRegex(R"(\s*#\s*redefine\s+(\w+)(?:\s+(.*))?)");

    // Prepare source text
    if (source.back() == 0)
    {
        source.erase(source.size() - 1);
    }
    boost::erase_all(source, "\r");

    try
    {
        // Prepare
        vector<string> sourceLines;
        boost::split(sourceLines, source, boost::is_any_of("\n"), boost::token_compress_off);

        // Process source lines
        enum class State
        {
            Transition,
            TransitionRegion,
            ExtractingRegion,
        };
        string destLines;
        string regionName;
        string regionBuffer;
        State currentState = State::Transition;
        std::smatch beginRegionMatch;
        std::smatch endRegionMatch;
        std::smatch redefineMatches;
        uint lineNumber = 0;
        for (const string& line : sourceLines)
        {
            ++lineNumber;

            // Try to match directives
            const bool isMatchedRegionBegin = std::regex_match(line, beginRegionMatch, beginRegionRegex);
            const bool isMatchedRegionEnd = std::regex_match(line, endRegionMatch, endRegionRegex);

            if (isMatchedRegionBegin)
            {
                // Catch '#region'
                LS_THROW(currentState == State::Transition,
                         FormatException,
                         "Duplicate #region directive at line ", lineNumber);

                // Start named or anonymous region
                if (beginRegionMatch[1].matched)
                {
                    currentState = State::ExtractingRegion;
                    regionName = beginRegionMatch[1].str();
                    regionBuffer = "";
                }
                else
                {
                    currentState = State::TransitionRegion;
                }
            }
            else if (isMatchedRegionEnd)
            {
                // Catch '#endregion'
                LS_THROW(currentState == State::ExtractingRegion
                         || currentState == State::TransitionRegion,
                         FormatException,
                         "Unexpected #endregion directive at line ", lineNumber);

                // End named or anonymous region
                if (currentState == State::ExtractingRegion)
                {
                    currentState = State::Transition;
                    regions.emplace(regionName, regionBuffer);
                }
                else if (currentState == State::TransitionRegion)
                {
                    currentState = State::Transition;
                }
            }
            else
            {
                // Process redefine
                const bool isMatchedRedefine = std::regex_match(line, redefineMatches, redefineRegex);
                const string lineToPut = !isMatchedRedefine
                    ? line
                    : "#undef " + redefineMatches[1].str() + "\n"
                    + "#define " + redefineMatches[1].str() + " " + redefineMatches[2].str();

                // Store to region or source
                if (currentState == State::ExtractingRegion)
                {
                    regionBuffer += lineToPut + "\n";
                }
                else if (currentState == State::Transition
                         || currentState == State::TransitionRegion)
                {
                    destLines += lineToPut + "\n";
                }
                else
                {
                    debug_assert(0);
                }
            }
        }
        // Catch '#region'
        LS_THROW(currentState == State::Transition,
                 FormatException,
                 "#region directive is not closed ");

        // Put sources
        source = destLines;
    }
    catch (std::regex_error e)
    {
        LS_THROW(0, FormatException, e.what());
    }
}
void usl::preprocessSource(string& source, const string& name,
                           const Library& includes)
{
    try
    {
        // Pre-process
        WaveContext<LoadFileFromHash, ContextHooks> context(name, source, includes);
        source = context.process();
    }
    catch (const boost::wave::preprocess_exception& ex)
    {
        LS_THROW(0, FormatException,
                 ex.file_name(), "(", ex.line_no(), "): ", ex.description());
    }
    catch (const boost::wave::cpplexer::lexing_exception& ex)
    {
        LS_THROW(0, FormatException,
                 ex.file_name(), "(", ex.line_no(), "): ", ex.description());
    }
    catch (const std::exception& ex)
    {
        LS_THROW(0, FormatException, ex.what());
    }
}


// Parse effect
namespace
{
    /// @brief Enum
    enum TextAccumulator
    {
        DefaultAccumulator,
        MainAccumulator,
        ControlAccumulator,
        ControlConstAccumulator,
        AccumulatorCOUNT
    };
    /// @brief Parser context
    struct ParserContext
    {
        unique_ptr<usl::Effect> effect;
        std::unique_ptr<usl::Effect::UniformBuffer> currentBuffer;
        std::unique_ptr<usl::Effect::Layout> currentLayout;
        std::unique_ptr<usl::Effect::Output> currentOutput;
        std::unique_ptr<usl::Effect::Target> currentTarget;
        std::unique_ptr<usl::Effect::VertexShader> currentVertexShader;
        std::unique_ptr<usl::Effect::HullShader> currentHullShader;
        std::unique_ptr<usl::Effect::DomainShader> currentDomainShader;
        std::unique_ptr<usl::Effect::GeometryShader> currentGeometryShader;
        std::unique_ptr<usl::Effect::PixelShader> currentPixelShader;
        TextAccumulator currentAccumulator = DefaultAccumulator;
        array<string, AccumulatorCOUNT> textAccumulator;
        string globalTextAccumulator;
    public:
        void drop()
        {
            if (!currentVertexShader
                && !currentHullShader && !currentDomainShader
                && !currentGeometryShader && !currentPixelShader)
            {
                globalTextAccumulator += textAccumulator[DefaultAccumulator];
                textAccumulator.fill("");
            }
            if (currentBuffer)
            {
                effect->bufferNames.emplace(currentBuffer->name);
                effect->buffers.push_back(*currentBuffer);
                currentBuffer = nullptr;
            }
            if (currentLayout)
            {
                effect->layouts.emplace(currentLayout->name, *currentLayout);
                currentLayout = nullptr;
            }
            if (currentOutput)
            {
                currentOutput->hasPosition = currentOutput->vars.count("Position") == 1;
                effect->outputs.emplace(currentOutput->name, *currentOutput);
                currentOutput = nullptr;
            }
            if (currentTarget)
            {
                effect->targets.emplace(currentTarget->name, *currentTarget);
                currentTarget = nullptr;
            }
            if (currentVertexShader)
            {
                currentVertexShader->text = globalTextAccumulator + textAccumulator[DefaultAccumulator];
                currentVertexShader->main = textAccumulator[MainAccumulator];
                effect->vertexShaders[currentVertexShader->name] = *currentVertexShader;
                currentVertexShader = nullptr;
                textAccumulator.fill("");
            }
            debug_assert(!!currentHullShader == !!currentDomainShader);
            if (currentHullShader && currentDomainShader)
            {
                currentHullShader->text = globalTextAccumulator + textAccumulator[DefaultAccumulator];
                currentHullShader->main = textAccumulator[ControlAccumulator];
                currentHullShader->mainConst = textAccumulator[ControlConstAccumulator];
                currentDomainShader->text = globalTextAccumulator + textAccumulator[DefaultAccumulator];
                currentDomainShader->main = textAccumulator[MainAccumulator];

                effect->hullShaders[currentHullShader->name] = *currentHullShader;
                effect->domainShaders[currentDomainShader->name] = *currentDomainShader;

                currentHullShader = nullptr;
                currentDomainShader = nullptr;
                textAccumulator.fill("");
            }
            if (currentGeometryShader)
            {
                currentGeometryShader->text = globalTextAccumulator + textAccumulator[DefaultAccumulator];
                currentGeometryShader->main = textAccumulator[MainAccumulator];
                effect->geometryShaders[currentGeometryShader->name] = *currentGeometryShader;
                currentGeometryShader = nullptr;
                textAccumulator.fill("");
            }
            if (currentPixelShader)
            {
                currentPixelShader->text = globalTextAccumulator + textAccumulator[DefaultAccumulator];
                currentPixelShader->main = textAccumulator[MainAccumulator];
                effect->pixelShaders[currentPixelShader->name] = *currentPixelShader;
                currentPixelShader = nullptr;
                textAccumulator.fill("");
            }
        }
        template <class Shader>
        void updateShaders(const map<string, Shader>& shaders)
        {
            for (auto& shader : shaders)
            {
                const string& shaderName = shader.first;
                const uint shaderIndex = effect->shaderNames.size();
                effect->shaderIndices.emplace(shaderName, shaderIndex);
                effect->shaderNames.push_back(shaderName);
            }
        }
        void updateShaders()
        {
            updateShaders(effect->vertexShaders);
            updateShaders(effect->pixelShaders);
            updateShaders(effect->geometryShaders);
            updateShaders(effect->hullShaders);
            updateShaders(effect->domainShaders);
        }
    };
    /// @brief Parse variable
    void parseVariable(ParserContext& context, const std::smatch& mathces)
    {
        using namespace usl;
        if (context.currentBuffer)
        {
            // Read uniform to uniform buffer
            Effect::Uniform uniform;
            uniform.length = mathces[2].matched ? fromString<uint>(mathces[2].str()) : 1;
            uniform.name = mathces[3].str();
            uniform.type = fromString<UniformType>(mathces[1].str());

            LS_THROW(uniform.type != UniformType::Unknown, FormatException,
                     "Invalid uniform type '", mathces[1], "'");
            LS_THROW(context.currentBuffer->uniforms.count(uniform.name) == 0, FormatException,
                     "Duplicate uniform '", uniform.name, "'");

            // Update buffer
            context.currentBuffer->uniforms[uniform.name] = uniform;
            context.currentBuffer->uniformsOrder.push_back(uniform.name);

            // Update size
            LS_THROW(uniformTypeSize(uniform.type) % 16 == 0 || uniform.length == 1,
                     GapiEffectException,
                     "Uniform '", uniform.name, "' is unaligned array");
            const uint uniformSize = uniformTypeSize(uniform.type) * uniform.length;
            context.currentBuffer->size += (uniformSize + 15) / 16 * 16;

        }
        else
        {
            // Read attribute
            Effect::Attrib attrib;
            attrib.length = mathces[2].matched ? fromString<uint>(mathces[2].str()) : 1;
            attrib.name = mathces[3].str();
            attrib.type = fromString<AttribType>(mathces[1].str());

            LS_THROW(attrib.type != AttribType::Unknown, FormatException,
                     "Invalid attribute type '", mathces[1], "'");

            // Store in layout or output
            if (context.currentLayout)
            {
                LS_THROW(context.currentLayout->attribs.count(attrib.name) == 0, FormatException,
                         "Duplicate attribute '", attrib.name, "'");
                context.currentLayout->attribs[attrib.name] = attrib;
                context.currentLayout->attribsOrder.push_back(attrib.name);
            }
            else if (context.currentOutput)
            {
                LS_THROW(context.currentOutput->vars.count(attrib.name) == 0, FormatException,
                         "Duplicate attribute '", attrib.name, "'");
                context.currentOutput->vars[attrib.name] = attrib;
            }
            else
            {
                debug_assert(0);
            }
        }
    }
    /// @brief Parse line
    void parseLine(ParserContext& context, const string& line)
    {
        using namespace usl;
        static std::regex lineRegex(R"(#\s*line\s+.+)");
        static std::regex regionRegex(R"(#\s*region\s+.+)");
        static std::regex endregionRegex(R"(#\s*endregion\s+.+)");
        static std::regex constantRegex(R"(#\s*constant\s+(\w+).*)");
        static std::regex variableRegex(R"(\s*(\w+)(?:\[(\d+)\])?\s+(\w+)\s*;.*)");
        static std::regex resourceRegex(R"(#\s*resource\s+(\w+)\s*\(\s*(\w+)\s*\)\s*(\w+).*)");
        static std::regex layoutRegex(R"(#\s*layout\s+(\w+).*)");
        static std::regex outputRegex(R"(#\s*output\s+(\w+).*)");
        static std::regex targetRegex(R"(#\s*target\s+(\w+).*)");
        static std::regex targetVariableRegex(R"(\s*(\w+)\s*;.*)");
        static std::regex vertexShaderRegex(R"(#\s*vertex\s+(\w+)\s*\(\s*layout\s*=\s*(\w+)\s*,\s*output\s*=\s*(\w+)\s*\).*)");
        static std::regex tessShaderRegex(R"(#\s*tess\s+(\w+)\s*\(\s*input\s*=\s*(\w+)\s*,\s*input_primitive\s*=\s*(\w+)\s*,\s*input_size\s*=\s*(\w+)\s*,\s*output_size\s*=\s*(\w+)\s*,\s*tess\s*=\s*(\w+)\s*,\s*tess_output\s*=\s*(\w+)\s*,\s*output_spacing\s*=\s*(\w+)\s*,\s*output\s*=\s*(\w+)\s*\).*)");
        static std::regex geometryShaderRegex(R"(#\s*geometry\s+(\w+)\s*\(\s*input\s*=\s*(\w+)\s*,\s*input_primitive\s*=\s*(\w+)\s*\,\s*output\s*=\s*(\w+)\s*\,\s*output_primitive\s*=\s*(\w+)\s*\,\s*output_size\s*=\s*(\w+)\s*\).*)");
        static std::regex pixelShaderRegex(R"(#\s*pixel\s+(\w+)\s*\(\s*input\s*=\s*(\w+)\s*,\s*target\s*=\s*(\w+)\s*\).*)");
        static std::regex textTagRegex(R"(#\s*text.*)");
        static std::regex mainTagRegex(R"(#\s*main.*)");
        static std::regex constTagRegex(R"(#\s*const.*)");
        static std::regex controlTagRegex(R"(#\s*control.*)");
        static std::regex programRegex(R"(#\s*program\s+(\w+)\s*\(\s*vertex\s*=\s*(\w+)\s*(?:,\s*tess\s*=\s*(\w+)\s*)?(?:,\s*geometry\s*=\s*(\w+)\s*)?,\s*pixel\s*=\s*(\w+)\s*\).*)");
        static const string HullSuffix = "__Hull";
        static const string DomainSuffix = "__Domain";
        std::smatch mathces;
        // #TODO use it
#define CHECK_MATCH(CONDITION, STR, IDX) \
        LS_THROW(CONDITION, FormatException, STR, " '", mathces[IDX], "'");

        if (boost::trim_copy(line).empty())
        {
            // Empty line, do nothing
        }
        else if (std::regex_match(line, mathces, lineRegex))
        {
            // Line directive, skip
        }
        else if (std::regex_match(line, mathces, regionRegex)
                 || std::regex_match(line, mathces, endregionRegex))
        {
            // Region/endregion directive, skip
        }
        else if ((context.currentBuffer
                 || context.currentLayout || context.currentOutput)
                 && std::regex_match(line, mathces, variableRegex))
        {
            parseVariable(context, mathces);
        }
        else if (context.currentTarget
                 && std::regex_match(line, mathces, targetVariableRegex))
        {
            Effect::TargetItem item;
            item.index = context.currentTarget->vars.size();
            item.name = mathces[1];
            context.currentTarget->vars[item.name] = item;
        }
        else if (std::regex_match(line, mathces, constantRegex))
        {
            context.drop();

            // Read new uniform buffer
            context.currentBuffer = std::make_unique<Effect::UniformBuffer>();
            context.currentBuffer->name = mathces[1];

            LS_THROW(context.effect->bufferNames.count(context.currentBuffer->name) == 0, FormatException,
                     "Duplicate uniform buffer '", context.currentBuffer->name, "'");
        }
        else if (std::regex_match(line, mathces, resourceRegex))
        {
            context.drop();

            // Read new resource
            Effect::Resource resource;
            resource.type = fromString<ResourceType>(mathces[1].str());
            resource.component = fromString<UniformType>(mathces[2].str());
            resource.name = mathces[3].str();

            LS_THROW(resource.type != ResourceType::Unknown, FormatException,
                     "Invalid resource type '", mathces[1], "'");
            LS_THROW(resource.component != UniformType::Unknown, FormatException,
                     "Invalid underlying type '", mathces[2], "'");
            LS_THROW(context.effect->resourceNames.count(resource.name) == 0, FormatException,
                     "Duplicate resource '", resource.name, "'");

            context.effect->resourceNames.emplace(resource.name);
            context.effect->resources.push_back(resource);
        }
        else if (std::regex_match(line, mathces, layoutRegex))
        {
            context.drop();

            // Read new layout
            context.currentLayout = std::make_unique<Effect::Layout>();
            context.currentLayout->name = mathces[1];

            LS_THROW(context.effect->layouts.count(context.currentLayout->name) == 0, FormatException,
                     "Duplicate layout '", context.currentLayout->name, "'");
        }
        else if (std::regex_match(line, mathces, outputRegex))
        {
            context.drop();

            // Read new output
            context.currentOutput = std::make_unique<Effect::Output>();
            context.currentOutput->name = mathces[1];

            LS_THROW(context.effect->outputs.count(context.currentOutput->name) == 0, FormatException,
                     "Duplicate output '", context.currentOutput->name, "'");
        }
        else if (std::regex_match(line, mathces, targetRegex))
        {
            context.drop();

            // Read new
            context.currentTarget = std::make_unique<Effect::Target>();
            context.currentTarget->name = mathces[1];

            LS_THROW(context.effect->targets.count(context.currentTarget->name) == 0, FormatException,
                     "Duplicate target '", context.currentTarget->name, "'");
        }
        else if (std::regex_match(line, mathces, vertexShaderRegex))
        {
            context.drop();
            context.currentAccumulator = DefaultAccumulator;

            // Read new
            context.currentVertexShader = std::make_unique<Effect::VertexShader>();
            context.currentVertexShader->name = mathces[1];
            context.currentVertexShader->layout = findOrDefault(context.effect->layouts, mathces[2]);
            context.currentVertexShader->output = findOrDefault(context.effect->outputs, mathces[3]);

            LS_THROW(context.currentVertexShader->layout, FormatException,
                     "Layout is not found '", mathces[2], "'");
            LS_THROW(context.currentVertexShader->output, FormatException,
                     "Output is not found '", mathces[3], "'");
            LS_THROW(context.effect->vertexShaders.count(context.currentVertexShader->name) == 0, FormatException,
                     "Duplicate vertex shader '", context.currentVertexShader->name, "'");
        }
        else if (std::regex_match(line, mathces, tessShaderRegex))
        {
            context.drop();
            context.currentAccumulator = DefaultAccumulator;

            // Load parameters
            Effect::TessParam param;
            const string baseName = mathces[1].str();
            Effect::Output* input = findOrDefault(context.effect->outputs, mathces[2]);
            param.primitive = fromString<TessPrimitive>(mathces[3].str());
            param.inputSize = fromString<uint>(mathces[4].str());
            param.outputSize = fromString<uint>(mathces[5].str());
            Effect::Output* tess = findOrDefault(context.effect->outputs, mathces[6]);
            param.output = fromString<TessOutput>(mathces[7].str());
            param.spacing = fromString<TessSpacing>(mathces[8].str());
            Effect::Output* output = findOrDefault(context.effect->outputs, mathces[9]);

            // Check
            CHECK_MATCH(input,
                        "Input is not found", 2);
            CHECK_MATCH(param.primitive != TessPrimitive::Unknown,
                        "Input primitive is invalid", 3);
            CHECK_MATCH(param.inputSize > 0,
                        "Input size is invalid", 4);
            CHECK_MATCH(param.outputSize > 0,
                        "Output size is invalid", 5);
            CHECK_MATCH(tess,
                        "Intermediate tess input/output is not found", 6);
            CHECK_MATCH(param.output != TessOutput::Unknown,
                        "Output type is invalid", 7);
            CHECK_MATCH(param.spacing != TessSpacing::Unknown,
                        "Spacing is invalid", 8);
            CHECK_MATCH(output,
                        "Output is not found", 9);

            // Read new
            context.currentHullShader = std::make_unique<Effect::HullShader>();
            context.currentHullShader->name = baseName + HullSuffix;
            context.currentHullShader->input = input;
            context.currentHullShader->output = tess;
            context.currentHullShader->param = param;

            context.currentDomainShader = std::make_unique<Effect::DomainShader>();
            context.currentDomainShader->name = baseName + DomainSuffix;
            context.currentDomainShader->input = tess;
            context.currentDomainShader->output = output;
            context.currentDomainShader->param = param;

            LS_THROW(context.effect->hullShaders.count(context.currentHullShader->name) == 0, FormatException,
                     "Duplicate hull shader '", context.currentHullShader->name, "'");
            LS_THROW(context.effect->domainShaders.count(context.currentDomainShader->name) == 0, FormatException,
                     "Duplicate domain shader '", context.currentDomainShader->name, "'");
        }
        else if (std::regex_match(line, mathces, geometryShaderRegex))
        {
            context.drop();
            context.currentAccumulator = DefaultAccumulator;

            // Read new
            context.currentGeometryShader = std::make_unique<Effect::GeometryShader>();
            context.currentGeometryShader->name = mathces[1];
            context.currentGeometryShader->input = findOrDefault(context.effect->outputs, mathces[2]);
            context.currentGeometryShader->inputPrimitive = fromString<GeometryInput>(mathces[3].str());
            context.currentGeometryShader->output = findOrDefault(context.effect->outputs, mathces[4]);
            context.currentGeometryShader->outputPrimitive = fromString<GeometryOutput>(mathces[5].str());
            context.currentGeometryShader->outputSize = fromString<uint>(mathces[6].str());

            LS_THROW(context.currentGeometryShader->input, FormatException,
                     "Input is not found '", mathces[2], "'");
            LS_THROW(context.currentGeometryShader->inputPrimitive != GeometryInput::Unknown, FormatException,
                     "Input primitive is not invalid '", mathces[3], "'");
            LS_THROW(context.currentGeometryShader->output, FormatException,
                     "Output is not found '", mathces[4], "'");
            LS_THROW(context.currentGeometryShader->outputPrimitive != GeometryOutput::Unknown, FormatException,
                     "Output primitive is not invalid '", mathces[5], "'");
            LS_THROW(context.effect->geometryShaders.count(context.currentGeometryShader->name) == 0, FormatException,
                     "Duplicate geometry shader shader '", context.currentGeometryShader->name, "'");
        }
        else if (std::regex_match(line, mathces, pixelShaderRegex))
        {
            context.drop();
            context.currentAccumulator = DefaultAccumulator;

            // Read new
            context.currentPixelShader = std::make_unique<Effect::PixelShader>();
            context.currentPixelShader->name = mathces[1];
            context.currentPixelShader->input = findOrDefault(context.effect->outputs, mathces[2]);
            context.currentPixelShader->target = findOrDefault(context.effect->targets, mathces[3]);

            LS_THROW(context.currentPixelShader->input, FormatException,
                     "Input is not found '", mathces[2], "'");
            LS_THROW(context.currentPixelShader->target, FormatException,
                     "Target is not found '", mathces[3], "'");
            LS_THROW(context.effect->pixelShaders.count(context.currentPixelShader->name) == 0, FormatException,
                     "Duplicate pixel shader shader '", context.currentPixelShader->name, "'");
        }
        else if (std::regex_match(line, mathces, textTagRegex))
        {
            context.currentAccumulator = DefaultAccumulator;
        }
        else if (std::regex_match(line, mathces, mainTagRegex))
        {
            context.currentAccumulator = MainAccumulator;
        }
        else if (std::regex_match(line, mathces, constTagRegex))
        {
            context.currentAccumulator = ControlConstAccumulator;
        }
        else if (std::regex_match(line, mathces, controlTagRegex))
        {
            context.currentAccumulator = ControlAccumulator;
        }
        else if (std::regex_match(line, mathces, programRegex))
        {
            context.drop();

            Effect::Program program;
            program.name = mathces[1];
            program.vertex = findOrDefault(context.effect->vertexShaders, mathces[2]);
            program.hull = findOrDefault(context.effect->hullShaders, mathces[3].str() + HullSuffix);
            program.domain = findOrDefault(context.effect->domainShaders, mathces[3].str() + DomainSuffix);
            program.geometry = findOrDefault(context.effect->geometryShaders, mathces[4]);
            program.pixel = findOrDefault(context.effect->pixelShaders, mathces[5]);

            LS_THROW(program.vertex, FormatException,
                     "Vertex shader is not found '", mathces[2], "'");
            LS_THROW((program.hull && program.domain) || !mathces[3].matched, FormatException,
                     "Hull/domain shaders are not found '", mathces[3], "'");
            LS_THROW(program.geometry || !mathces[4].matched, FormatException,
                     "Pixel shader is not found '", mathces[4], "'");
            LS_THROW(program.pixel, FormatException,
                     "Pixel shader is not found '", mathces[5], "'");
            LS_THROW(context.effect->programs.count(program.name) == 0, FormatException,
                     "Duplicate program '", program.name, "'");
            context.effect->programs.emplace(program.name, program);
        }
        else
        {
            LS_THROW(!context.currentBuffer && !context.currentLayout
                     && !context.currentOutput && !context.currentTarget,
                     FormatException, "Incorrect line in declaration: ", line);
            context.textAccumulator[context.currentAccumulator] += line;
            context.textAccumulator[context.currentAccumulator] += "\n";
        }
    }
}
unique_ptr<usl::Effect> usl::parseEffect(const string& source)
{
    // Prepare
    vector<string> sourceLines;
    boost::split(sourceLines, source, boost::is_any_of("\n"), boost::token_compress_off);

    // Current stuff
    string currentFile = "<Unknown>";
    uint currentLine = 1;
    ParserContext context;
    context.effect = std::make_unique<usl::Effect>();
    for (const string& line : sourceLines)
    {
        try
        {
            parseLine(context, line);
        }
        catch (const FormatException& ex)
        {
            LS_THROW(0, FormatException,
                     "Error at \"", currentFile, "\"(", currentLine, "):", ex.what());
        }
        ++currentLine;
    }
    context.drop();

    // Enumerate shaders
    context.updateShaders();

    return std::move(context.effect);
}
unique_ptr<usl::Effect> usl::loadEffect(const string& source, const Library& includes)
{
    string sourceCopy = source;
    Library includesCopy = includes;
    extractRegionsFromSource(sourceCopy, includesCopy);
    preprocessSource(sourceCopy, "<Unknown>", includesCopy);
    return parseEffect(sourceCopy);
}
string usl::generateListing(const string& text)
{
    auto chop = [](const string& line)
    {
        static char spaces[] = { ' ', '\t' };
        const size_t begin = line.find_first_not_of(spaces);
        const size_t end = line.find_last_not_of(spaces);
        if (begin == string::npos)
            return string();
        else if (end == string::npos)
            return line.substr(begin);
        else
            return line.substr(begin, end - begin + 1);
    };
    string listing;
    string buffer;
    int line = 1;
    size_t lineBegin = 0, lineEnd = 0;
    do
    {
        // Listing mark
        listing += "[";
        listing += toString(line);
        if (line < 10)
            listing += "] \t";
        else
            listing += "]\t";
        // String
        lineEnd = text.find('\n', lineBegin);
        buffer = chop(text.substr(lineBegin, lineEnd - lineBegin));
        lineBegin = lineEnd + 1;
        listing += buffer;
        listing += "\n";
        ++line;
    } while (lineEnd != string::npos);
    return listing;
}
