/// @file ls/gapi/usl/usl.h
/// @brief USL base file
#pragma once

#include "ls/common.h"
#include "ls/gapi/common.h"

namespace ls
{
    /// @namespace ls::usl
    /// @brief Unified Shader Language implementation
    namespace usl
    {
        /// @brief Compiler target
        enum class TargetLanguage
        {
            /// @brief HLSL 3.0, DX11 with geometry and tessellation shaders
            HLSL30
        };
        /// @brief Compiler target to string
        const char* asString(const TargetLanguage target);
        /// @brief Effect Library
        using Library = hamap<string, string>;
        /// @brief Extract regions from string
        /// @throw FormatException if cannot parse source
        void extractRegionsFromSource(string& source, 
                                      Library& regions);
        /// @brief Pre-process source
        /// @throw FormatException if cannot parse source
        void preprocessSource(string& source, const string& name,
                              const Library& includes);
        /// @brief Geometry shader input type
        enum class GeometryInput
        {
            Unknown = 0,
            Point,
            Line,
            LineAdjacency,
            Triangle,
            TriangleAdjacency
        };
        /// @brief Geometry shader output type
        enum class GeometryOutput
        {
            Unknown = 0,
            Point,
            LineStrip,
            TriangleStrip
        };
        /// @brief Tess shader primitive
        enum class TessPrimitive
        {
            Unknown = 0,
            /// @brief Triangle
            Triangle,
            /// @brief Quad
            Quad
        };
        /// @brief Tess shader output type
        enum class TessOutput
        {
            Unknown = 0,
            /// @brief Clockwise triangles
            CW,
            /// @brief Counter-clockwise triangles
            CCW
        };
        /// @brief Tess shader spacing
        enum class TessSpacing
        {
            Unknown = 0,
            /// @brief Integer spacing
            Integer,
            /// @brief Fractional even
            Even,
            /// @brief Fractional odd
            Odd
        };
        /// @brief Resource type
        enum class ResourceType
        {
            /// @brief Unknown
            Unknown = 0,
            /// @brief Texture 2D
            Texture2D,
            /// @brief Shadow texture
            Shadow2D
        };
        /// @brief USL Effect
        struct Effect
        {
        public:
            /// @brief Uniform
            struct Uniform
            {
                /// @brief Type Type
                using Type = UniformType;
                /// @brief Type
                UniformType type;
                /// @brief Array
                uint length;
                /// @brief Name
                string name;
            };
            /// @brief Uniform buffer
            struct UniformBuffer
            {
                /// @brief Name
                string name;
                /// @brief Size
                uint size;
                /// @brief Uniforms
                map<string, Uniform> uniforms;
                /// @brief Uniforms order
                vector<string> uniformsOrder;
            };
            /// @brief Resources
            struct Resource
            {
                /// @brief Name
                string name;
                /// @brief Resource type
                ResourceType type;
                /// @brief Component type
                UniformType component;
            };
            /// @brief Attrib
            struct Attrib
            {
                /// @brief Type Type
                using Type = AttribType;
                /// @brief Type
                AttribType type;
                /// @brief Array
                uint length;
                /// @brief Name
                string name;
            };
            /// @brief Layout
            struct Layout
            {
                /// @brief Name
                string name;
                /// @brief Attributes order
                vector<string> attribsOrder;
                /// @brief Attributes
                map<string, Attrib> attribs;
            };
            /// @brief Output
            struct Output
            {
                /// @brief Name
                string name;
                /// @brief Variables
                map<string, Attrib> vars;
                /// @brief Is empty? (position is ignored)
                bool isEmpty;
                /// @brief Has position?
                bool hasPosition;
            };
            /// @brief Render target elem
            struct TargetItem
            {
                /// @brief Name
                string name;
                /// @brief Index
                sint index;
            };
            /// @brief Render target
            struct Target
            {
                /// @brief Name
                string name;
                /// @brief Vars
                map<string, TargetItem> vars;
            };

            /// @brief Vertex shader
            struct VertexShader
            {
                /// @brief Type
                static const ShaderType Type = ShaderType::Vertex;
                /// @brief Name
                string name;
                /// @brief Layout
                Layout* layout = nullptr;
                /// @brief Output
                Output* output = nullptr;
                /// @brief Text
                string text;
                /// @brief Main
                string main;
            };
            /// @brief Pixel shader
            struct PixelShader
            {
                /// @brief Type
                static const ShaderType Type = ShaderType::Pixel;
                /// @brief Name
                string name;
                /// @brief Input
                Output* input = nullptr;
                /// @brief Output
                Target* target = nullptr;
                /// @brief Text
                string text;
                /// @brief Main
                string main;
            };
            /// @brief Geometry shader
            struct GeometryShader
            {
                /// @brief Type
                static const ShaderType Type = ShaderType::Geometry;
                /// @brief Name
                string name;
                /// @brief Input
                Output* input = nullptr;
                /// @brief Output
                Output* output = nullptr;
                /// @brief Text
                string text;
                /// @brief Main
                string main;
                /// @brief Input primitive
                GeometryInput inputPrimitive;
                /// @brief Output primitive
                GeometryOutput outputPrimitive;
                /// @brief Output size
                uint outputSize;
            };
            /// @brief Tess parameters
            struct TessParam
            {
                /// @brief Tess primitive
                TessPrimitive primitive;
                /// @brief Tess input patch size
                uint inputSize;
                /// @brief Tess output type
                TessOutput output;
                /// @brief Tess output patch size
                uint outputSize;
                /// @brief Tess spacing type
                TessSpacing spacing;
            };
            /// @brief Control shader
            struct HullShader
            {
                /// @brief Type
                static const ShaderType Type = ShaderType::TessControl;
                /// @brief Name
                string name;
                /// @brief Input
                Output* input = nullptr;
                /// @brief Output
                Output* output = nullptr;
                /// @brief Info
                TessParam param;
                /// @brief Includes
                string text;
                /// @brief Const function
                string mainConst;
                /// @brief Main
                string main;
            };
            /// @brief Evaluate shader
            struct DomainShader
            {
                /// @brief Type
                static const ShaderType Type = ShaderType::TessEval;
                /// @brief Name
                string name;
                /// @brief Input
                Output* input = nullptr;
                /// @brief Output
                Output* output = nullptr;
                /// @brief Info
                TessParam param;
                /// @brief Includes
                string text;
                /// @brief Main
                string main;
            };

            /// @brief Program
            struct Program
            {
                /// @brief Name
                string name;
                /// @brief Vertex shader
                VertexShader* vertex = nullptr;
                /// @brief Pixel shader
                PixelShader* pixel = nullptr;
                /// @brief Geometry shader
                GeometryShader* geometry = nullptr;
                /// @brief Tess control shader
                HullShader* hull = nullptr;
                /// @brief Tess evaluate shader
                DomainShader* domain = nullptr;
            };
        public:
            /// @brief Const buffers
            vector<UniformBuffer> buffers;
            /// @brief Const buffers names
            set<string> bufferNames;
            /// @brief Resources
            vector<Resource> resources;
            /// @brief Resources names
            set<string> resourceNames;
            /// @brief Layouts
            map<string, Layout> layouts;
            /// @brief Outputs
            map<string, Output> outputs;
            /// @brief Targets
            map<string, Target> targets;

            /// @brief Vertex shaders
            map<string, VertexShader> vertexShaders;
            /// @brief Pixel shaders
            map<string, PixelShader> pixelShaders;
            /// @brief Geometry shaders
            map<string, GeometryShader> geometryShaders;
            /// @brief Control shaders
            map<string, HullShader> hullShaders;
            /// @brief Evaluate shaders
            map<string, DomainShader> domainShaders;

            /// @brief Programs
            map<string, Program> programs;
            /// @brief Enumerated shaders
            vector<string> shaderNames;
            /// @brief Shader index by name
            map<string, uint> shaderIndices;
        public:
            /// @brief Get number of shaders
            uint numShaders() const
            {
                return (uint) shaderNames.size();
            }
            /// @brief Get number of layouts
            uint numLayouts() const
            {
                return (uint) layouts.size();
            }
        };
        /// @brief Read effect from preprocessed source text
        /// @throw FormatException if cannot parse source
        unique_ptr<Effect> parseEffect(const string& source);
        /// @brief Load effect from text
        unique_ptr<Effect> loadEffect(const string& source, const Library& includes);
        /// @brief Make listing by text
        string generateListing(const string& text);
    }
    /// @}
}