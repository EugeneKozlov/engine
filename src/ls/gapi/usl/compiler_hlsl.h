/// @file ls/gapi/usl/EffectCompilerDX11.h
/// @brief Effect Compiler for DX11
#pragma once
#include "ls/common.h"
#include "ls/gapi/usl/compiler.h"

#if LS_COMPILE_DX11 != 0
#include <dx11.h>

namespace ls
{
    namespace usl
    {
        template <>
        class Compiler<TargetLanguage::HLSL30>
        {
        public:
            string generateHeader(const usl::Effect& effect);
            vector<ubyte> compile(const string& header,
                                  const usl::Effect::VertexShader& shader) const;
            vector<ubyte> compile(const string& header,
                                  const usl::Effect::HullShader& shader) const;
            vector<ubyte> compile(const string& header,
                                  const usl::Effect::DomainShader& shader) const;
            vector<ubyte> compile(const string& header,
                                  const usl::Effect::GeometryShader& shader) const;
            vector<ubyte> compile(const string& header,
                                  const usl::Effect::PixelShader& shader) const;
        };
        /// @brief Effect Compiler for DX11
        class EffectCompilerDX11
            : Noncopyable
        {
        public:
            /// @brief Ctor
            EffectCompilerDX11(const usl::Effect& parser);
            /// @brief Get parsed effect
            const usl::Effect& parsed() const
            {
                return m_parser;
            }
            /// @brief Vertex shader
            vector<ubyte> makeShader(const usl::Effect::VertexShader& shader);
            /// @brief Hull shader
            vector<ubyte> makeShader(const usl::Effect::HullShader& shader);
            /// @brief Domain shader
            vector<ubyte> makeShader(const usl::Effect::DomainShader& shader);
            /// @brief Geometry shader
            vector<ubyte> makeShader(const usl::Effect::GeometryShader& shader);
            /// @brief Pixel shader
            vector<ubyte> makeShader(const usl::Effect::PixelShader& shader);
        private:
            const static char* uniformTypeName(const UniformType type);
            const static char* attribTypeName(const AttribType type);
            const static char* resourceSamplerName(const usl::ResourceType type);
            const static char* resourceTypeName(const usl::ResourceType type);
            const static char* geometryInput(const usl::GeometryInput type);
            const static char* geometryOutput(const usl::GeometryOutput type);
            static uint geometryInputSize(const usl::GeometryInput type);
            const static char* tessDomain(const usl::TessPrimitive type);
            const static char* tessSpacing(const usl::TessSpacing type);
            const static char* tessOutput(const usl::TessOutput type);
            static string generateUniform(const usl::Effect::Uniform& var);
            static string generateAttrib(const usl::Effect::Attrib& var);
            static void generateArrays(string& text);
            void generateHeader();
            vector<ubyte> compile(const string& name, const string& text, const ShaderType type);
        private:
            const usl::Effect& m_parser;
            string m_header;
        };
    }
}
#endif
