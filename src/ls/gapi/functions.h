/// @file ls/gapi/Renderer.h
/// @brief Abstract renderer interface.
#pragma once

#include "ls/common.h"
#include "ls/gapi/common.h"

namespace ls
{
    /// @addtogroup LsGapi
    /// @{

    /// @brief Resource usage as string
    const char* asString(const ResourceUsage value);
    /// @brief Buffer type as string
    const char* asString(const BufferType value);
    /// @brief Index buffer element type as string
    const char* asString(const IndexFormat value);
    /// @brief Index buffer element stride
    ubyte indexStride(const IndexFormat value);
    /// @brief Shader type as string
    const char* asString(const ShaderType value);
    /// @brief Test if shader type is 'advanced'
    bool isAdvanced(const ShaderType value);
    /// @brief Test if shader type is tessellation
    bool isTessellation(const ShaderType value);
    /// @brief Uniform type as string
    const char* asString(const UniformType value);
    /// @brief Get uniform type size
    uint uniformTypeSize(const UniformType type);
    /// @brief Get uniform type register size
    uint uniformRegisterStride(const UniformType type);
    /// @brief Uniform type by string
    template<>
    UniformType fromString<UniformType>(const char* name);
    /// @brief Attrib type by string
    template<>
    AttribType fromString<AttribType>(const char* name);
    /// @brief Compression by string
    template<>
    Compression fromString<Compression>(const char* name);
    /// @brief Uniform type as string
    const char* asString(const PrimitiveType value);
    /// @brief Format as string
    const char* asString(const Format value);
    /// @brief Uniform type as string
    const char* asString(const Comparison value);
    /// @brief Get format number of components
    uint formatComponents(const Format format);
    /// @brief Get format block size
    uint formatBlockSize(const Format format);
    /// @brief Get format stride
    uint formatStride(const Format format);
    /// @brief Get format row and slice pitch
    void formatPitch(const Format format, const uint width, const uint height,
                     uint& rowPitch, uint& slicePitch);
    /// @brief Whether format is shadow
    bool isShadowFormat(const Format format);
    /// @brief Primitive indices
    uint primitiveIndices(const PrimitiveType primitive, const uint count);
    /// @}
}

