/// @file ls/gapi/common.h
/// @brief Common GAPI types
#pragma once

#include "ls/common.h"

namespace ls
{
    /// @addtogroup LsGapi
    /// @{

    /// @brief Allocator
    template <class Base, class Impl>
    class GapiAllocator
    {
    public:
        /// @brief Allocator type
        using Allocator = FastAllocator<Impl>;
        /// @brief Construct
        GapiPointer<Base> construct(const Impl& desc)
        {
            ++m_alive;
            Impl* ptr = m_alloc.allocate(1);
            m_alloc.construct(ptr, desc);
            return GapiPointer<Base>(ptr);
        }
        /// @brief Destroy
        void destroy(GapiPointer<Base> object)
        {
            debug_assert(object);
            Impl* ptr = &object.template get<Impl>();
            m_alloc.destroy(ptr);
            m_alloc.deallocate(ptr, 1);
            --m_alive;
        }
        /// @brief Get number of alive objects
        uint numAlive() const
        {
            return m_alive;
        }
    private:
        Allocator m_alloc;
        uint m_alive = 0;
    };
    /// @}
}
