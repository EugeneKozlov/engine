/// @file ls/gapi/enum.h
/// @brief GAPI enums
#pragma once

#include "ls/common.h"

namespace ls
{
    /// @addtogroup LsGapi
    /// @{

    /// @brief GAPI type
    enum class Gapi
    {
        /// @brief Any
        Any,
        /// @brief OpenGL
        OpenGL,
        /// @brief Direct3D 11
        Direct3D11
    };
    /// @brief Max name length (shader, program, uniform, etc)
    static const uint MaxNameLength = 64;
    /// @brief Max vertex layout size
    static const uint MaxVertexLayoutSize = 32;
    /// @brief Max number of render targets
    static const uint MaxRenderTargets = 8;
    /// @brief Max number of streams
    static const uint MaxStreams = 16;
    /// @brief Max number of shader uniform buffers
    static const uint MaxShaderUniformBuffers = 14;
    /// @brief Max number of shader resources
    static const uint MaxShaderResources = 16;
    /// @brief Resource pool
    enum class ResourceUsage
    {
        /// @brief Immutable resource
        Constant,
        /// @brief Static resource
        Static,
        /// @brief Dynamic resource
        Dynamic
    };
    /// @brief Resource binding
    enum class ResourceBinding
    {
        /// @brief Useless
        Useless = 0,

        /// @brief Resource can be copied back to RAM
        Readable = 1 << 0,

        /// @brief Used as shader resource
        ShaderResource = 1 << 1,
        /// @brief Used as render target
        RenderTarget = 1 << 2,
        /// @brief Used as depth-stencil buffer
        DepthStencil = 1 << 3
    };
    /// @brief Buffer type
    enum class BufferType
    {
        /// @brief Vertex buffer
        Vertex,
        /// @brief Index buffer
        Index,
        /// @brief Uniform buffer
        Uniform
    };
    /// @brief Index buffer element type
    enum class IndexFormat
    {
        /// @brief Invalid
        Unknown = 0,
        /// @brief 8-bit index buffer
        Ubyte,
        /// @brief 16-bit index buffer
        Ushort,
        /// @brief 32-bit index buffer
        Uint
    };
    /// @brief Shader type
    enum class ShaderType
    {
        /// @brief Vertex
        Vertex = 0,
        /// @brief Tess control
        TessControl,
        /// @brief Tess evaluate
        TessEval,
        /// @brief Geometry
        Geometry,
        /// @brief Pixel
        Pixel,
        /// @brief Number of shaders
        COUNT
    };
    /// @brief Uniform type
    enum class UniformType
    {
        /// @brief Unknown
        Unknown = 0,
        /// @brief Float
        Float,
        /// @brief Int
        Int,
        /// @brief Uint
        Uint,
        /// @brief Float2
        Float2,
        /// @brief Int2
        Int2,
        /// @brief Uint2
        Uint2,
        /// @brief Float4
        Float4,
        /// @brief Int4
        Int4,
        /// @brief Uint4
        Uint4,
        /// @brief Float4x4
        Float4x4
    };
    /// @brief Attribute type
    enum class AttribType
    {
        /// @brief Unknown
        Unknown = 0,
        /// @brief Float
        Float,
        /// @brief Int
        Int,
        /// @brief Uint
        Uint,
        /// @brief Float2
        Float2,
        /// @brief Int2
        Int2,
        /// @brief Uint2
        Uint2,
        /// @brief Float3
        Float3,
        /// @brief Int3
        Int3,
        /// @brief Uint3
        Uint3,
        /// @brief Float4
        Float4,
        /// @brief Int4
        Int4,
        /// @brief Uint4
        Uint4,
    };
    /// @brief Primitive type
    enum class PrimitiveType
    {
        /// @brief Point list
        Point = 0,
        /// @brief Lines list
        Line,
        /// @brief Lines strip
        LineStrip,
        /// @brief Triangles list
        Triangle,
        /// @brief Triangles strip
        TriangleStrip,
        /// @brief 3-control-points patch
        Patch3,
        /// @brief 4-control-points patch
        Patch4,
        /// @brief Counter
        Count
    };
    /// @brief Vertex or texture element format
    enum class Format
    {
        /// @brief Unknown format
        Unknown = 0,

        /// @brief 16-bit normalized depth, no stencil
        D16_UNORM,
        /// @brief 24-bit normalized depth, 8-bit stencil
        D24_UNORM_S8_UINT,
        /// @brief 32-bit float depth, no stencil
        D32_FLOAT,

        /// @brief BGRA color
        B8G8R8A8_UNORM,
        /// @brief RGBA with extended color space
        R10G10B10A2_UNORM,
        /// @brief RGB encoded as half
        R11G11B10_FLOAT,

        /// @brief Unsigned byte scalar
        R8_UINT,
        /// @brief Unsigned byte 2d vector
        R8G8_UINT,
        /// @brief Unsigned byte 4d vector
        R8G8B8A8_UINT,
        /// @brief Signed byte scalar
        R8_INT,
        /// @brief Signed byte 2d vector
        R8G8_INT,
        /// @brief Signed byte 4d vector
        R8G8B8A8_INT,
        /// @brief Unsigned normalized byte scalar
        R8_UNORM,
        /// @brief Unsigned normalized byte 2d vector
        R8G8_UNORM,
        /// @brief Unsigned normalized byte 4d vector
        R8G8B8A8_UNORM,
        /// @brief Signed normalized byte scalar
        R8_SNORM,
        /// @brief Signed normalized byte 2d vector
        R8G8_SNORM,
        /// @brief Signed normalized byte 4d vector
        R8G8B8A8_SNORM,

        /// @brief Unsigned short scalar
        R16_UINT,
        /// @brief Unsigned short 2d vector
        R16G16_UINT,
        /// @brief Unsigned short 4d vector
        R16G16B16A16_UINT,
        /// @brief Signed short scalar
        R16_INT,
        /// @brief Signed short 2d vector
        R16G16_INT,
        /// @brief Signed short 4d vector
        R16G16B16A16_INT,
        /// @brief Unsigned normalized short scalar
        R16_UNORM,
        /// @brief Unsigned normalized short 2d vector
        R16G16_UNORM,
        /// @brief Unsigned normalized short 4d vector
        R16G16B16A16_UNORM,
        /// @brief Signed normalized short scalar
        R16_SNORM,
        /// @brief Signed normalized short 2d vector
        R16G16_SNORM,
        /// @brief Signed normalized short 4d vector
        R16G16B16A16_SNORM,
        /// @brief Half scalar
        R16_FLOAT,
        /// @brief Half 2d vector
        R16G16_FLOAT,
        /// @brief Half 4d vector
        R16G16B16A16_FLOAT,

        /// @brief Unsigned long scalar
        R32_UINT,
        /// @brief Unsigned long 2d vector
        R32G32_UINT,
        /// @brief Unsigned long 3d vector
        R32G32B32_UINT,
        /// @brief Unsigned long 4d vector
        R32G32B32A32_UINT,
        /// @brief Signed normalized long scalar
        R32_INT,
        /// @brief Signed long 2d vector
        R32G32_INT,
        /// @brief Signed long 3d vector
        R32G32B32_INT,
        /// @brief Signed long 4d vector
        R32G32B32A32_INT,
        /// @brief Float scalar
        R32_FLOAT,
        /// @brief Float 2d vector
        R32G32_FLOAT,
        /// @brief Float 3d vector
        R32G32B32_FLOAT,
        /// @brief Float 4d vector
        R32G32B32A32_FLOAT,

        /// @brief BC 1
        BC1,
        /// @brief BC 2
        BC2,
        /// @brief BC 3
        BC3
    };
    /// @brief Texture compression type
    enum class Compression
    {
        /// @brief No compression
        No,
        /// @brief BC1/DXT1
        Block1,
        /// @brief BC2/DXT3
        Block2,
        /// @brief BC3/DXT5
        Block3,
        /// @brief Counter
        COUNT
    };
    /// @brief Blend factor
    enum class BlendFactor
    {
        /// @brief (0)
        Zero = 0,
        /// @brief (1)
        One,
        /// @brief RGB from PS
        SrcColor,
        /// @brief 1 - RGB from PS
        SrcColorInv,
        /// @brief Alpha from PS
        SrcAlpha,
        /// @brief 1 - Alpha from PS
        SrcAlphaInv,
        /// @brief RGB from RT
        DestColor,
        /// @brief 1 - RGB from RT
        DestColorInv,
        /// @brief Alpha from RT
        DestAlpha,
        /// @brief 1 - Alpha from RT
        DestAlphaInv,
    };
    /// @brief Blend operation
    enum class BlendOperation
    {
        /// @brief SrcColor * SrcBlend + DestColor * DestBlend
        Add = 0,
        /// @brief SrcColor * SrcBlend - DestColor * DestBlend
        Sub,
        /// @brief DestColor * DestBlend - SrcColor * SrcBlend
        SubInv,
    };
    /// @brief Comparison function
    enum class Comparison
    {
        /// @brief Always @c false
        Never = 0,
        /// @brief Always @c true
        Always,
        /// @a new == @a old
        Equal,
        /// @a new != @a old
        NotEqual,
        /// @a new < @a old
        Less,
        /// @a new <= @a old
        LessEqual,
        /// @a new > @a old
        Greater,
        /// @a new >= @a old
        GreaterEqual
    };
    /// @brief Stencil operation
    enum class StencilOperation
    {
        /// @brief Keep old value
        Keep = 0,
        /// @brief Replace by zero
        Zero,
        /// @brief Replace by @a value
        Replace,
        /// @brief Increment by 1 and wrap
        Inc,
        /// @brief Increment by 1 and clamp
        IncSat,
        /// @brief Decrement by 1 and wrap
        Dec,
        /// @brief Decrement by 1 and clamp
        DecSat,
        /// @brief Invert
        Invert
    };
    /// @brief Texture filtering
    enum class Filter
    {
        /// @brief Point texture sampling, point mipmap sampling
        Point,
        /// @brief Point texture sampling, linear mipmap sampling
        PointLinear,
        /// @brief Linear texture sampling, point mipmap sampling
        LinearPoint,
        /// @brief Linear texture sampling, linear mipmap sampling
        Linear,
        /// @brief Anisotropic
        Anisotropic
    };
    /// @brief Texture address mode
    enum class Addressing
    {
        /// @brief Use fractional texture coordinate part, tile the texture
        Wrap,
        /// @brief Seamless tiling because of mirror
        Mirror,
        /// @brief Clamp texture coordinates outside the range [0, 1]
        Clamp,
        /// @brief Use absolute part of texture coordinate and clamp by [0, 1] range
        MirrorOnce
    }; 
    /// @brief Culling
    enum class Culling
    {
        /// @brief Back face culling
        Back,
        /// @brief No culling
        None,
        /// @brief Front face culling
        Front
    };
    /// @brief Resource mapping type
    enum class MappingFlag
    {
        /// @brief Read
        Read,
        /// @brief Write
        Write,
        /// @brief Write and discard
        WriteDiscard
    };
    /// @}
}