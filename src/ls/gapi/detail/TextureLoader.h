/// @file ls/gapi/detail/TextureLoader.h
/// @brief Texture Loader
#pragma once

#include "ls/common.h"
#include "ls/gapi/Renderer.h"

namespace ls
{
    class StreamInterface;
    class Buffer;

    namespace gapi
    {
        /// @brief Load DDS from stream
        gli::texture loadDDS(StreamInterface& source);
        /// @brief Write DDS to stream
        void saveDDS(StreamInterface& dest, const gli::texture& storage);
        /// @brief Load DDS from buffer
        gli::texture loadDDS(Buffer& source);
        /// @brief Write DDS to buffer
        void saveDDS(Buffer& dest, const gli::texture& storage);
        /// @brief Get GLI format by internal
        gli::format gliFormat(const Format format);
        /// @brief Get internal format by GLI
        Format gliUnformat(const gli::format format);
        /// @brief Get squish flags
        int squishFlags(const Compression compression);
        /// @brief Get compressed format
        gli::format compressedFormat(const Compression compression);
    }
}