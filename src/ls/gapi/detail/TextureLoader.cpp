#include "pch.h"
#include "ls/gapi/detail/TextureLoader.h"
#include "ls/io/Buffer.h"
#include "ls/io/StreamInterface.h"
#include <squish/squish.h>

using namespace ls;
using namespace ls::gapi;
gli::texture ls::gapi::loadDDS(StreamInterface& source)
{
    vector<ubyte> data = source.data();
    return gli::load_dds(reinterpret_cast<const char*>(data.data()), data.size());
}
void gapi::saveDDS(StreamInterface& dest, const gli::texture& source)
{
    vector<char> data;
    gli::save_dds(source, data);
    dest.write(data.data(), data.size());
}
gli::texture ls::gapi::loadDDS(Buffer& source)
{
    return gli::load_dds(reinterpret_cast<const char*>(source.data()), 
                         source.size());
}
void gapi::saveDDS(Buffer& dest, const gli::texture& source)
{
    vector<char> data;
    gli::save_dds(source, data);
    dest.write(data.data(), data.size());
}
gli::format gapi::gliFormat(const Format format)
{
    switch (format)
    {
    case Format::D16_UNORM: return gli::FORMAT_D16_UNORM_PACK16;
    case Format::D24_UNORM_S8_UINT: return gli::FORMAT_D24_UNORM_S8_UINT_PACK32;
    case Format::D32_FLOAT: return gli::FORMAT_D32_SFLOAT_PACK32;

    case Format::B8G8R8A8_UNORM: return gli::FORMAT_BGRA8_UNORM_PACK8;
    case Format::R10G10B10A2_UNORM: return gli::FORMAT_BGR10A2_UNORM_PACK32;
    case Format::R11G11B10_FLOAT: return gli::FORMAT_RG11B10_UFLOAT_PACK32;

    case Format::R8_INT: return gli::FORMAT_R8_SINT_PACK8;
    case Format::R8G8_INT: return gli::FORMAT_RG8_SINT_PACK8;
    case Format::R8G8B8A8_INT: return gli::FORMAT_RGBA8_SINT_PACK8;
    case Format::R8_SNORM: return gli::FORMAT_R8_SNORM_PACK8;
    case Format::R8G8_SNORM: return gli::FORMAT_RG8_SNORM_PACK8;
    case Format::R8G8B8A8_SNORM: return gli::FORMAT_RGBA8_SNORM_PACK8;

    case Format::R8_UINT: return gli::FORMAT_R8_UINT_PACK8;
    case Format::R8G8_UINT: return gli::FORMAT_RG8_UINT_PACK8;
    case Format::R8G8B8A8_UINT: return gli::FORMAT_RGBA8_UINT_PACK8;
    case Format::R8_UNORM: return gli::FORMAT_R8_UNORM_PACK8;
    case Format::R8G8_UNORM: return gli::FORMAT_RG8_UNORM_PACK8;
    case Format::R8G8B8A8_UNORM: return gli::FORMAT_RGBA8_UNORM_PACK8;

    case Format::R16_INT: return gli::FORMAT_R16_SINT_PACK16;
    case Format::R16G16_INT: return gli::FORMAT_RG16_SINT_PACK16;
    case Format::R16G16B16A16_INT: return gli::FORMAT_RGBA16_SINT_PACK16;
    case Format::R16_SNORM: return gli::FORMAT_R16_SNORM_PACK16;
    case Format::R16G16_SNORM: return gli::FORMAT_RG16_SNORM_PACK16;
    case Format::R16G16B16A16_SNORM: return gli::FORMAT_RGBA16_SNORM_PACK16;

    case Format::R16_UINT: return gli::FORMAT_R16_UINT_PACK16;
    case Format::R16G16_UINT: return gli::FORMAT_RG16_UINT_PACK16;
    case Format::R16G16B16A16_UINT: return gli::FORMAT_RGBA16_UINT_PACK16;
    case Format::R16_UNORM: return gli::FORMAT_R16_UNORM_PACK16;
    case Format::R16G16_UNORM: return gli::FORMAT_RG16_UNORM_PACK16;
    case Format::R16G16B16A16_UNORM: return gli::FORMAT_RGBA16_UNORM_PACK16;

    case Format::R16_FLOAT: return gli::FORMAT_R16_SFLOAT_PACK16;
    case Format::R16G16_FLOAT: return gli::FORMAT_RG16_SFLOAT_PACK16;
    case Format::R16G16B16A16_FLOAT: return gli::FORMAT_RGBA16_SFLOAT_PACK16;

    case Format::R32_INT: return gli::FORMAT_R32_SINT_PACK32;
    case Format::R32_UINT: return gli::FORMAT_R32_UINT_PACK32;
    case Format::R32_FLOAT: return gli::FORMAT_R32_SFLOAT_PACK32;

    case Format::R32G32_INT: return gli::FORMAT_RG32_SINT_PACK32;
    case Format::R32G32_UINT: return gli::FORMAT_RG32_UINT_PACK32;
    case Format::R32G32_FLOAT: return gli::FORMAT_RG32_SFLOAT_PACK32;

    case Format::R32G32B32_INT: return gli::FORMAT_RGB32_SINT_PACK32;
    case Format::R32G32B32_UINT: return gli::FORMAT_RGB32_UINT_PACK32;
    case Format::R32G32B32_FLOAT: return gli::FORMAT_RGB32_SFLOAT_PACK32;

    case Format::R32G32B32A32_INT: return gli::FORMAT_RGBA32_SINT_PACK32;
    case Format::R32G32B32A32_UINT: return gli::FORMAT_RGBA32_UINT_PACK32;
    case Format::R32G32B32A32_FLOAT: return gli::FORMAT_RGBA32_SFLOAT_PACK32;
    
    case Format::BC1: return gli::FORMAT_RGBA_DXT1_UNORM_BLOCK8;
    case Format::BC2: return gli::FORMAT_RGBA_DXT3_UNORM_BLOCK16;
    case Format::BC3: return gli::FORMAT_RGBA_DXT5_UNORM_BLOCK16;

    case Format::Unknown: 
    default:
        return gli::FORMAT_UNDEFINED;
    }
}
Format gapi::gliUnformat(const gli::format format)
{
    switch (format)
    {
    case gli::FORMAT_D16_UNORM_PACK16: return Format::D16_UNORM;
    case gli::FORMAT_D24_UNORM_S8_UINT_PACK32: return Format::D24_UNORM_S8_UINT;
    case gli::FORMAT_D32_SFLOAT_PACK32: return Format::D32_FLOAT;

    case gli::FORMAT_BGRA8_UNORM_PACK8: return Format::B8G8R8A8_UNORM;
    case gli::FORMAT_RGB10A2_UNORM_PACK32: return Format::R10G10B10A2_UNORM;
    case gli::FORMAT_RG11B10_UFLOAT_PACK32: return Format::R11G11B10_FLOAT;

    case gli::FORMAT_R8_SINT_PACK8: return Format::R8_INT;
    case gli::FORMAT_RG8_SINT_PACK8: return Format::R8G8_INT;
    case gli::FORMAT_RGBA8_SINT_PACK8: return Format::R8G8B8A8_INT;
    case gli::FORMAT_R8_SNORM_PACK8: return Format::R8_SNORM;
    case gli::FORMAT_RG8_SNORM_PACK8: return Format::R8G8_SNORM;
    case gli::FORMAT_RGBA8_SNORM_PACK8: return Format::R8G8B8A8_SNORM;

    case gli::FORMAT_R8_UINT_PACK8: return Format::R8_UINT;
    case gli::FORMAT_RG8_UINT_PACK8: return Format::R8G8_UINT;
    case gli::FORMAT_RGBA8_UINT_PACK8: return Format::R8G8B8A8_UINT;
    case gli::FORMAT_R8_UNORM_PACK8: return Format::R8_UNORM;
    case gli::FORMAT_RG8_UNORM_PACK8: return Format::R8G8_UNORM;
    case gli::FORMAT_RGBA8_UNORM_PACK8: return Format::R8G8B8A8_UNORM;

    case gli::FORMAT_R16_SINT_PACK16: return Format::R16_INT;
    case gli::FORMAT_RG16_SINT_PACK16: return Format::R16G16_INT;
    case gli::FORMAT_RGBA16_SINT_PACK16: return Format::R16G16B16A16_INT;
    case gli::FORMAT_R16_SNORM_PACK16: return Format::R16_SNORM;
    case gli::FORMAT_RG16_SNORM_PACK16: return Format::R16G16_SNORM;
    case gli::FORMAT_RGBA16_SNORM_PACK16: return Format::R16G16B16A16_SNORM;

    case gli::FORMAT_R16_UINT_PACK16: return Format::R16_UINT;
    case gli::FORMAT_RG16_UINT_PACK16: return Format::R16G16_UINT;
    case gli::FORMAT_RGBA16_UINT_PACK16: return Format::R16G16B16A16_UINT;
    case gli::FORMAT_R16_UNORM_PACK16: return Format::R16_UNORM;
    case gli::FORMAT_RG16_UNORM_PACK16: return Format::R16G16_UNORM;
    case gli::FORMAT_RGBA16_UNORM_PACK16: return Format::R16G16B16A16_UNORM;

    case gli::FORMAT_R16_SFLOAT_PACK16: return Format::R16_FLOAT;
    case gli::FORMAT_RG16_SFLOAT_PACK16: return Format::R16G16_FLOAT;
    case gli::FORMAT_RGBA16_SFLOAT_PACK16: return Format::R16G16B16A16_FLOAT;

    case gli::FORMAT_R32_SINT_PACK32: return Format::R32_INT;
    case gli::FORMAT_R32_UINT_PACK32: return Format::R32_UINT;
    case gli::FORMAT_R32_SFLOAT_PACK32: return Format::R32_FLOAT;

    case gli::FORMAT_RG32_SINT_PACK32: return Format::R32G32_INT;
    case gli::FORMAT_RG32_UINT_PACK32: return Format::R32G32_UINT;
    case gli::FORMAT_RG32_SFLOAT_PACK32: return Format::R32G32_FLOAT;

    case gli::FORMAT_RGB32_SINT_PACK32: return Format::R32G32B32_INT;
    case gli::FORMAT_RGB32_UINT_PACK32: return Format::R32G32B32_UINT;
    case gli::FORMAT_RGB32_SFLOAT_PACK32: return Format::R32G32B32_FLOAT;

    case gli::FORMAT_RGBA32_SINT_PACK32: return Format::R32G32B32A32_INT;
    case gli::FORMAT_RGBA32_UINT_PACK32: return Format::R32G32B32A32_UINT;
    case gli::FORMAT_RGBA32_SFLOAT_PACK32: return Format::R32G32B32A32_FLOAT;

    case gli::FORMAT_RGB_DXT1_UNORM_BLOCK8: return Format::BC1;
    case gli::FORMAT_RGBA_DXT1_UNORM_BLOCK8: return Format::BC1;
    case gli::FORMAT_RGBA_DXT3_UNORM_BLOCK16: return Format::BC2;
    case gli::FORMAT_RGBA_DXT5_UNORM_BLOCK16: return Format::BC3;

    case gli::FORMAT_UNDEFINED:
    default:
        return Format::Unknown;
    }
}
int gapi::squishFlags(const Compression compression)
{
    int flags = squish::kColourClusterFit;
    //int flags = squish::kColourRangeFit;
    switch (compression)
    {
    case Compression::Block1:
        flags |= squish::kDxt1;
        break;
    case Compression::Block2:
        flags |= squish::kDxt3;
        break;
    case Compression::Block3:
        flags |= squish::kDxt5;
        break;
    default:
        break;
    }
    return flags;
}
gli::format gapi::compressedFormat(const Compression compression)
{
    switch (compression)
    {
    case Compression::Block1:
        return gli::FORMAT_RGBA_DXT1_UNORM_BLOCK8;
    case Compression::Block2:
        return gli::FORMAT_RGBA_DXT3_UNORM_BLOCK16;
    case Compression::Block3:
        return gli::FORMAT_RGBA_DXT5_UNORM_BLOCK16;
    case Compression::No:
    default:
        return gli::FORMAT_UNDEFINED;
    }
}
