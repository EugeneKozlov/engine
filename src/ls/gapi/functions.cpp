#include "pch.h"
#include "ls/gapi/functions.h"

using namespace ls;
template<>
UniformType ls::fromString<UniformType>(const char* name)
{
    static map<string, UniformType> m = {
        make_pair("Float", UniformType::Float),
        make_pair("Int", UniformType::Int),
        make_pair("Uint", UniformType::Uint),
        make_pair("Float2", UniformType::Float2),
        make_pair("Int2", UniformType::Int2),
        make_pair("Uint2", UniformType::Uint2),
        make_pair("Float4", UniformType::Float4),
        make_pair("Int4", UniformType::Int4),
        make_pair("Uint4", UniformType::Uint4),
        make_pair("Float4x4", UniformType::Float4x4)
    };
    return getOrDefault(m, name, UniformType::Unknown);
}
template<>
AttribType ls::fromString<AttribType>(const char* name)
{
    static map<string, AttribType> m = {
        make_pair("Float", AttribType::Float),
        make_pair("Int", AttribType::Int),
        make_pair("Uint", AttribType::Uint),
        make_pair("Float2", AttribType::Float2),
        make_pair("Int2", AttribType::Int2),
        make_pair("Uint2", AttribType::Uint2),
        make_pair("Float3", AttribType::Float3),
        make_pair("Int3", AttribType::Int3),
        make_pair("Uint3", AttribType::Uint3),
        make_pair("Float4", AttribType::Float4),
        make_pair("Int4", AttribType::Int4),
        make_pair("Uint4", AttribType::Uint4),
    };
    return getOrDefault(m, name, AttribType::Unknown);
}
template<>
Compression ls::fromString<Compression>(const char* name)
{
    static map<string, Compression> m = {
        make_pair("No", Compression::No),
        make_pair("Block1", Compression::Block1),
        make_pair("Block2", Compression::Block2),
        make_pair("Block3", Compression::Block3),
    };
    return getOrDefault(m, name, Compression::No);
}
const char* ls::asString(const ResourceUsage value)
{
    switch (value)
    {
        LS_E2S_CASE(ResourceUsage, Constant);
        LS_E2S_CASE(ResourceUsage, Static);
        LS_E2S_CASE(ResourceUsage, Dynamic);
    }
    return "";
}
const char* ls::asString(const BufferType value)
{
    switch (value)
    {
        LS_E2S_CASE(BufferType, Vertex);
        LS_E2S_CASE(BufferType, Index);
        LS_E2S_CASE(BufferType, Uniform);
    }
    return "";
}
const char* ls::asString(const IndexFormat value)
{
    switch (value)
    {
        LS_E2S_CASE(IndexFormat, Unknown);
        LS_E2S_CASE(IndexFormat, Ubyte);
        LS_E2S_CASE(IndexFormat, Ushort);
        LS_E2S_CASE(IndexFormat, Uint);
    }
    return "";
}
ubyte ls::indexStride(const IndexFormat value)
{
    switch (value)
    {
    case IndexFormat::Ubyte: return 1;
    case IndexFormat::Ushort: return 2;
    case IndexFormat::Uint: return 4;
    case IndexFormat::Unknown: return 0;
    }
    return 0;
}
const char* ls::asString(const ShaderType value)
{
    switch (value)
    {
        LS_E2S_CASE(ShaderType, Vertex);
        LS_E2S_CASE(ShaderType, Pixel);
        LS_E2S_CASE(ShaderType, Geometry);
        LS_E2S_CASE(ShaderType, TessControl);
        LS_E2S_CASE(ShaderType, TessEval);
    }
    return "";
}
bool ls::isAdvanced(const ShaderType type)
{
    switch (type)
    {
    case ShaderType::Vertex:
    case ShaderType::Geometry:
    case ShaderType::Pixel:
        return false;
    case ShaderType::TessControl:
    case ShaderType::TessEval:
        return true;
    }
    return false;
}
bool ls::isTessellation(const ShaderType value)
{
    switch (value)
    {
    case ShaderType::Vertex:
    case ShaderType::Geometry:
    case ShaderType::Pixel:
        return false;
    case ShaderType::TessControl:
    case ShaderType::TessEval:
        return true;
    }
    return false;
}
const char* ls::asString(const UniformType value)
{
    switch (value)
    {
        LS_E2S_CASE(UniformType, Unknown);
        LS_E2S_CASE(UniformType, Float);
        LS_E2S_CASE(UniformType, Int);
        LS_E2S_CASE(UniformType, Uint);
        LS_E2S_CASE(UniformType, Float2);
        LS_E2S_CASE(UniformType, Int2);
        LS_E2S_CASE(UniformType, Uint2);
        LS_E2S_CASE(UniformType, Float4);
        LS_E2S_CASE(UniformType, Int4);
        LS_E2S_CASE(UniformType, Uint4);
        LS_E2S_CASE(UniformType, Float4x4);
    }
    return "";
}
uint ls::uniformTypeSize(const UniformType type)
{
    switch (type)
    {
    case UniformType::Unknown:
        return 0;
    case UniformType::Float:
    case UniformType::Int:
    case UniformType::Uint:
        return 4;
    case UniformType::Float2:
    case UniformType::Int2:
    case UniformType::Uint2:
        return 8;
    case UniformType::Float4:
    case UniformType::Int4:
    case UniformType::Uint4:
        return 16;
    case UniformType::Float4x4:
        return 64;
    }
    return 0;
}
uint ls::uniformRegisterStride(const UniformType type)
{
    switch (type)
    {
    case UniformType::Unknown:
        return 0;
    case UniformType::Float:
    case UniformType::Int:
    case UniformType::Uint:
    case UniformType::Float2:
    case UniformType::Int2:
    case UniformType::Uint2:
    case UniformType::Float4:
    case UniformType::Int4:
    case UniformType::Uint4:
        return 1;
    case UniformType::Float4x4:
        return 4;
    }
    return 0;
}
const char* ls::asString(const PrimitiveType value)
{
    switch (value)
    {
        LS_E2S_CASE(PrimitiveType, Point);
        LS_E2S_CASE(PrimitiveType, Line);
        LS_E2S_CASE(PrimitiveType, LineStrip);
        LS_E2S_CASE(PrimitiveType, Triangle);
        LS_E2S_CASE(PrimitiveType, TriangleStrip);
        LS_E2S_CASE(PrimitiveType, Patch3);
        LS_E2S_CASE(PrimitiveType, Patch4);
        LS_E2S_CASE(PrimitiveType, Count);
    }
    return "";
}
const char* ls::asString(const Format value)
{
    switch (value)
    {
        LS_E2S_CASE(Format, Unknown);

        LS_E2S_CASE(Format, D16_UNORM);
        LS_E2S_CASE(Format, D24_UNORM_S8_UINT);
        LS_E2S_CASE(Format, D32_FLOAT);

        LS_E2S_CASE(Format, B8G8R8A8_UNORM);

        LS_E2S_CASE(Format, R8_UINT);
        LS_E2S_CASE(Format, R8G8_UINT);
        LS_E2S_CASE(Format, R8G8B8A8_UINT);
        LS_E2S_CASE(Format, R8_INT);
        LS_E2S_CASE(Format, R8G8_INT);
        LS_E2S_CASE(Format, R8G8B8A8_INT);

        LS_E2S_CASE(Format, R8_UNORM);
        LS_E2S_CASE(Format, R8G8_UNORM);
        LS_E2S_CASE(Format, R8G8B8A8_UNORM);
        LS_E2S_CASE(Format, R8_SNORM);
        LS_E2S_CASE(Format, R8G8_SNORM);
        LS_E2S_CASE(Format, R8G8B8A8_SNORM);

        LS_E2S_CASE(Format, R16_UINT);
        LS_E2S_CASE(Format, R16G16_UINT);
        LS_E2S_CASE(Format, R16G16B16A16_UINT);
        LS_E2S_CASE(Format, R16_INT);
        LS_E2S_CASE(Format, R16G16_INT);
        LS_E2S_CASE(Format, R16G16B16A16_INT);

        LS_E2S_CASE(Format, R16_UNORM);
        LS_E2S_CASE(Format, R16G16_UNORM);
        LS_E2S_CASE(Format, R16G16B16A16_UNORM);
        LS_E2S_CASE(Format, R16_SNORM);
        LS_E2S_CASE(Format, R16G16_SNORM);
        LS_E2S_CASE(Format, R16G16B16A16_SNORM);

        LS_E2S_CASE(Format, R32_UINT);
        LS_E2S_CASE(Format, R32G32_UINT);
        LS_E2S_CASE(Format, R32G32B32_UINT);
        LS_E2S_CASE(Format, R32G32B32A32_UINT);

        LS_E2S_CASE(Format, R32_INT);
        LS_E2S_CASE(Format, R32G32_INT);
        LS_E2S_CASE(Format, R32G32B32_INT);
        LS_E2S_CASE(Format, R32G32B32A32_INT);

        LS_E2S_CASE(Format, R32_FLOAT);
        LS_E2S_CASE(Format, R32G32_FLOAT);
        LS_E2S_CASE(Format, R32G32B32_FLOAT);
        LS_E2S_CASE(Format, R32G32B32A32_FLOAT);
        
        LS_E2S_CASE(Format, BC1);
        LS_E2S_CASE(Format, BC2);
        LS_E2S_CASE(Format, BC3);
    }
    return "";
}
const char* ls::asString(const Comparison value)
{
    switch (value)
    {
        LS_E2S_CASE(Comparison, Never);
        LS_E2S_CASE(Comparison, Always);
        LS_E2S_CASE(Comparison, Equal);
        LS_E2S_CASE(Comparison, NotEqual);
        LS_E2S_CASE(Comparison, Less);
        LS_E2S_CASE(Comparison, LessEqual);
        LS_E2S_CASE(Comparison, Greater);
        LS_E2S_CASE(Comparison, GreaterEqual);
    }
    return "";
}
uint ls::formatComponents(const Format format)
{
    switch (format)
    {
    case Format::R8_INT:
    case Format::R8_SNORM:
    case Format::R8_UINT:
    case Format::R8_UNORM:
    case Format::R16_INT:
    case Format::R16_SNORM:
    case Format::R16_UINT:
    case Format::R16_UNORM:
    case Format::R16_FLOAT:
        return 1;
    case Format::R8G8_INT:
    case Format::R8G8_SNORM:
    case Format::R8G8_UINT:
    case Format::R8G8_UNORM:
    case Format::R16G16_INT:
    case Format::R16G16_SNORM:
    case Format::R16G16_UINT:
    case Format::R16G16_UNORM:
    case Format::R16G16_FLOAT:
        return 2;
    case Format::R8G8B8A8_INT:
    case Format::R8G8B8A8_SNORM:
    case Format::R8G8B8A8_UINT:
    case Format::R8G8B8A8_UNORM:
    case Format::R16G16B16A16_INT:
    case Format::R16G16B16A16_SNORM:
    case Format::R16G16B16A16_UINT:
    case Format::R16G16B16A16_UNORM:
    case Format::R16G16B16A16_FLOAT:
        return 4;
    case Format::R32_INT:
    case Format::R32_UINT:
    case Format::R32_FLOAT:
        return 1;
    case Format::R32G32_INT:
    case Format::R32G32_UINT:
    case Format::R32G32_FLOAT:
        return 2;
    case Format::R32G32B32_INT:
    case Format::R32G32B32_UINT:
    case Format::R32G32B32_FLOAT:
        return 3;
    case Format::R32G32B32A32_INT:
    case Format::R32G32B32A32_UINT:
    case Format::R32G32B32A32_FLOAT:
        return 4;
    case Format::B8G8R8A8_UNORM:
        return 4;
    case Format::R10G10B10A2_UNORM:
        return 4;
    case Format::R11G11B10_FLOAT:
        return 3;
    default:
        return 0;
    }
}
uint ls::formatBlockSize(const Format format)
{
    switch (format)
    {
    case Format::R8_INT:
    case Format::R8_SNORM:
    case Format::R8_UINT:
    case Format::R8_UNORM:
    case Format::R16_INT:
    case Format::R16_SNORM:
    case Format::R16_UINT:
    case Format::R16_UNORM:
    case Format::R16_FLOAT:
    case Format::R8G8_INT:
    case Format::R8G8_SNORM:
    case Format::R8G8_UINT:
    case Format::R8G8_UNORM:
    case Format::R8G8B8A8_INT:
    case Format::R8G8B8A8_SNORM:
    case Format::R8G8B8A8_UINT:
    case Format::R8G8B8A8_UNORM:
    case Format::R16G16_INT:
    case Format::R16G16_SNORM:
    case Format::R16G16_UINT:
    case Format::R16G16_UNORM:
    case Format::R16G16_FLOAT:
    case Format::R32_INT:
    case Format::R32_UINT:
    case Format::R32_FLOAT:
    case Format::R16G16B16A16_INT:
    case Format::R16G16B16A16_SNORM:
    case Format::R16G16B16A16_UINT:
    case Format::R16G16B16A16_UNORM:
    case Format::R16G16B16A16_FLOAT:
    case Format::R32G32_INT:
    case Format::R32G32_UINT:
    case Format::R32G32_FLOAT:
    case Format::R32G32B32_FLOAT:
    case Format::R32G32B32_INT:
    case Format::R32G32B32_UINT:
    case Format::R32G32B32A32_INT:
    case Format::R32G32B32A32_UINT:
    case Format::R32G32B32A32_FLOAT:
    case Format::D16_UNORM:
    case Format::D24_UNORM_S8_UINT:
    case Format::D32_FLOAT:
    case Format::B8G8R8A8_UNORM:
    case Format::R10G10B10A2_UNORM:
    case Format::R11G11B10_FLOAT:
        return 1;
    case Format::BC1:
    case Format::BC2:
    case Format::BC3:
        return 4;
    default:
        return 0;
    }
}
uint ls::formatStride(const Format format)
{
    switch (format)
    {
    case Format::R8_INT:
    case Format::R8_SNORM:
    case Format::R8_UINT:
    case Format::R8_UNORM:
        return 1;
    case Format::R16_INT:
    case Format::R16_SNORM:
    case Format::R16_UINT:
    case Format::R16_UNORM:
    case Format::R16_FLOAT:
    case Format::R8G8_INT:
    case Format::R8G8_SNORM:
    case Format::R8G8_UINT:
    case Format::R8G8_UNORM:
    case Format::D16_UNORM:
        return 2;
    case Format::R8G8B8A8_INT:
    case Format::R8G8B8A8_SNORM:
    case Format::R8G8B8A8_UINT:
    case Format::R8G8B8A8_UNORM:
    case Format::R16G16_INT:
    case Format::R16G16_SNORM:
    case Format::R16G16_UINT:
    case Format::R16G16_UNORM:
    case Format::R16G16_FLOAT:
    case Format::R32_INT:
    case Format::R32_UINT:
    case Format::R32_FLOAT:
    case Format::D24_UNORM_S8_UINT:
    case Format::D32_FLOAT:
    case Format::B8G8R8A8_UNORM:
    case Format::R10G10B10A2_UNORM:
    case Format::R11G11B10_FLOAT:
        return 4;
    case Format::R16G16B16A16_INT:
    case Format::R16G16B16A16_SNORM:
    case Format::R16G16B16A16_UINT:
    case Format::R16G16B16A16_UNORM:
    case Format::R16G16B16A16_FLOAT:
    case Format::R32G32_INT:
    case Format::R32G32_UINT:
    case Format::R32G32_FLOAT:
        return 8;
    case Format::R32G32B32_FLOAT:
    case Format::R32G32B32_INT:
    case Format::R32G32B32_UINT:
        return 12;
    case Format::R32G32B32A32_INT:
    case Format::R32G32B32A32_UINT:
    case Format::R32G32B32A32_FLOAT:
        return 16;
    case Format::BC1:
        return 8;
    case Format::BC2:
    case Format::BC3:
        return 16;
    default:
        return 0;
    }
}
void ls::formatPitch(const Format format, const uint width, const uint height,
                     uint& rowPitch, uint& slicePitch)
{
    uint buffersize = formatBlockSize(format);
    uint stride = formatStride(format);
    uint blockWidth = (width + buffersize - 1) / buffersize;
    uint blockHeight = (height + buffersize - 1) / buffersize;

    rowPitch = blockWidth * stride;
    slicePitch = blockHeight * rowPitch;
}
bool ls::isShadowFormat(const Format format)
{
    switch (format)
    {
    case Format::D16_UNORM:
    case Format::D24_UNORM_S8_UINT:
    case Format::D32_FLOAT:
        return true;
    default:
        return false;
    }
}
uint ls::primitiveIndices(const PrimitiveType primitive, const uint count)
{
    switch (primitive)
    {
    case PrimitiveType::Point:
        return 1 * count;
    case PrimitiveType::Line:
        return 2 * count;
    case PrimitiveType::LineStrip:
        return 1 + 1 * count;
    case PrimitiveType::Triangle:
        return 3 * count;
    case PrimitiveType::TriangleStrip:
        return 2 + 1 * count;
    case PrimitiveType::Patch3:
        return 3 * count;
    case PrimitiveType::Patch4:
        return 4 * count;
    default:
        return 0;
    }
}
