/// @file ls/gapi/desc/vertices.h
/// @brief Vertex formats 
#pragma once

#include "ls/common.h"
#include "ls/gapi/enum.h"
#include "ls/gapi/GapiPointer.h"
#include "ls/gapi/desc/StateDesc.h"

namespace ls
{
    /// @addtogroup LsGapi
    /// @{

    /// @brief 2D/3D vertex with color
    struct ColoredVertex
    {
    public:
        /// @brief Input format
        static const VertexFormat Layout[];
        /// @brief Input format size
        static const uint LayoutSize;
        /// @brief Position
        float4 pos;
        /// @brief Color
        uint color = 0;
    public:
        /// @brief Empty
        ColoredVertex() = default;
        /// @brief By 2D position
        ColoredVertex(const float2& pos)
            : pos(pos, 0.5f, 1.0f)
        {
        }
        /// @brief By 3D position
        ColoredVertex(const float3& pos)
            : pos(pos, 1.0f)
        {
        }
        /// @brief By 2D position and color
        ColoredVertex(const float2& pos, const uint color)
            : pos(pos, 0.5f, 1.0f)
            , color(color)
        {
        }
        /// @brief By 3D position and color
        ColoredVertex(const float3& pos, const uint color)
            : pos(pos, 1.0f)
            , color(color)
        {
        }
    };
    /// @brief 2D/3D vertex with color and texture.
    struct TexturedVertex
    {
    public:
        /// @brief Input format
        static const VertexFormat Layout[];
        /// @brief Input format size
        static const uint LayoutSize;
        /// @brief Position
        float4 pos;
        /// @brief Texture coord
        float2 uv;
        /// @brief Color
        uint color = 0xFFFFFFFF;
    public:
        /// @brief Empty
        TexturedVertex() = default;
        /// @brief By 2D position and texture
        TexturedVertex(const float2& pos, const float2& uv)
            : pos(pos, 0.5f, 1.0f)
            , uv(uv)
        {
        }
        /// @brief By 3D position and texture
        TexturedVertex(const float3& pos, const float2& uv)
            : pos(pos, 1.0f)
            , uv(uv)
        {
        }
        /// @brief By 4D position and texture
        TexturedVertex(const float4& pos, const float2& uv)
            : pos(pos)
            , uv(uv)
        {
        }
        /// @brief By 2D position, texture and color
        TexturedVertex(const float2& pos, const float2& uv, const uint color)
            : pos(pos, 0.5f, 1.0f)
            , uv(uv)
            , color(color)
        {
        }
        /// @brief By 3D position, texture and color
        TexturedVertex(const float3& pos, const float2& uv, const uint color)
            : pos(pos, 1.0f)
            , uv(uv)
            , color(color)
        {
        }
        /// @brief By 4D position, texture and color
        TexturedVertex(const float4& pos, const float2& uv, const uint color)
            : pos(pos)
            , uv(uv)
            , color(color)
        {
        }
    };
    /// @brief 3D oriented vertex
    struct LightedVertex
    {
    public:
        /// @brief Input format
        static const VertexFormat Layout[];
        /// @brief Input format size
        static const uint LayoutSize;
        /// @brief Position
        float3 pos;
        /// @brief Texture coord
        float2 uv;
        /// @brief Normal
        float3 norm;
    public:
        /// @brief Empty
        LightedVertex() = default;
        /// @brief By 3D position, texture and normal
        LightedVertex(const float3& pos, const float2& uv, const float3& norm)
            : pos(pos)
            , uv(uv)
            , norm(norm)
        {
        }
    };
    /// @}
}

