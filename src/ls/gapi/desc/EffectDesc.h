/// @file ls/gapi/desc/EffectDesc.h
/// @brief Renderer effect description
#pragma once

#include "ls/common.h"
#include "ls/gapi/enum.h"
#include "ls/gapi/GapiPointer.h"
#include "ls/gapi/desc/TextureDesc.h"

namespace ls
{
    /// @addtogroup LsGapi
    /// @{

    /// @brief Effect Layout Desc
    struct EffectLayoutDesc
    {
        /// @brief Layout name
        MultiArray<ubyte, MaxNameLength> name;
        /// @brief Number of attributes
        uint numAttribs;
        /// @brief Layout attributes packed names
        MultiArray<ubyte, MaxVertexLayoutSize, MaxNameLength> attribs;
        /// @brief Get attribute name
        const char* attribName(const uint idx) const
        {
            return reinterpret_cast<const char*>(attribs[idx].data());
        }
    };
    /// @brief Effect Program Desc
    struct EffectProgramDesc
    {
        /// @brief Program packed name
        MultiArray<ubyte, MaxNameLength> name;
        /// @brief Layout ID
        uint layout = 0;

        /// @brief Vertex Shader ID
        sint vertex = -1;
        /// @brief Hull Shader ID
        sint hull = -1;
        /// @brief Domain Shader ID
        sint domain = -1;
        /// @brief Geometry Shader ID
        sint geometry = -1;
        /// @brief Vertex Shader ID
        sint pixel = -1;
    };
    /// @brief Effect Desc
    struct EffectDesc
    {
        /// @brief Shaders sizes
        vector<uint> shadersSizes;
        /// @brief Shaders offsets
        vector<uint> shadersOffsets;
        /// @brief Shaders types
        vector<ShaderType> shadersTypes;
        /// @brief Shaders data
        vector<ubyte> shadersData;

        /// @brief Programs
        vector<EffectProgramDesc> programs;
        /// @brief Programs names
        hamap<string, uint> programNames;

        /// @brief Uniform Buffers names
        hamap<string, uint> bufferNames;
        /// @brief Uniform Buffers sizes
        vector<uint> buffersSizes;

        /// @brief Layouts data
        vector<EffectLayoutDesc> layouts;
        /// @brief Layouts mapping
        vector<hamap<string, uint>> layoutsUnpacked;
        /// @brief Layouts names
        hamap<string, uint> layoutsNames;

        /// @brief Resource data
        hamap<string, uint> resourceNames;
    };
    /// @brief Effect Handle
    using EffectHandle = GapiPointer<EffectDesc>;
    /// @brief Program Desc
    struct ProgramDesc
    {
        /// @brief Effect 
        EffectHandle effect;
        /// @brief Index 
        uint index;
    };
    /// @brief Program Handle
    using ProgramHandle = GapiPointer<ProgramDesc>;
    /// @brief Effect Resource Desc
    struct EffectResourceDesc
    {
        /// @brief Effect
        EffectHandle effect = nullptr;
        /// @brief Used slot
        uint slot = 0;
    };
    /// @brief Effect Resource Handle
    using EffectResourceHandle = GapiPointer<EffectResourceDesc>;
    /// @brief Effect Resource Bucket
    struct ResourceBucket
    {
    public:
        /// @brief Ctor
        ResourceBucket() = default;
        /// @brief Reset
        void reset()
        {
            numResources = 0;
        }
        /// @brief Add resource (should be sorted)
        void addResource(const uint slot, Texture2DHandle resource, const ShaderType shader)
        {
            debug_assert(numResources < MaxShaderResources);
            debug_assert(numResources == 0 || slot >= slots[numResources - 1]);
            slots[numResources] = slot;
            shaders[numResources] = shader;
            resources[numResources] = resource;
            ++numResources;
        }
        /// @brief Pop resource
        void popResource()
        {
            debug_assert(numResources > 0);
            --numResources;
        }
    public:
        /// @brief Number of resources
        uint numResources = 0;
        /// @brief Dest slots
        array<uint, MaxShaderResources> slots;
        /// @brief Dest shaders
        array<ShaderType, MaxShaderResources> shaders;
        /// @brief Resources to bind
        array<Texture2DHandle, MaxShaderResources> resources;
    };
    /// @}
}

