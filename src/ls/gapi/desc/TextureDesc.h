/// @file ls/gapi/desc/BufferDesc.h
/// @brief Renderer Buffer Desc 
#pragma once

#include "ls/common.h"
#include "ls/gapi/enum.h"
#include "ls/gapi/GapiPointer.h"

#pragma warning(push, 0)
#include "gli/gli.hpp"
#pragma warning(pop)

namespace ls
{
    /// @addtogroup LsGapi
    /// @{

    /// @brief Texture storage
    using TextureStorage = gli::texture2DArray;

    /// @brief Texture 2D Desc
    struct Texture2DDesc
    {
        /// @brief Width
        uint width;
        /// @brief Height
        uint height;
        /// @brief Size
        int2 size() const
        {
            return int2((sint) width, (sint) height);
        }
        /// @brief Number of slices
        uint numSlices;
        /// @brief Number of mips
        uint numMips;
        /// @brief Format
        Format format;
    };
    /// @brief Texture 2D Handle
    using Texture2DHandle = GapiPointer<Texture2DDesc>;
    /// @brief Render Target Desc
    struct RenderTargetDesc
    {
        /// @brief Depth texture
        Texture2DHandle depthTexture = nullptr;
        /// @brief Number of color textures
        uint numColorChannels = 0;
        /// @brief Color textures
        array<Texture2DHandle, MaxRenderTargets> colorTextures;

        /// @brief Number of attached color channels
        uint numAttached = 0;
        /// @brief Width
        uint width = 0;
        /// @brief Height
        uint height = 0;
    public:
        /// @brief Resize RT (impl)
        bool implResize(const uint newWidth, const uint newHeight)
        {
            if (!width && !height)
            {
                width = newWidth;
                height = newHeight;
                return true;
            }
            else
            {
                return width == newWidth && height == newHeight;
            }
        }
        /// @brief Drop RT (impl)
        void implDrop()
        {
            if (!depthTexture && numAttached == 0)
            {
                width = 0;
                height = 0;
            }
        }
    };
    /// @brief Render Target Handle
    using RenderTargetHandle = GapiPointer<RenderTargetDesc>;
    /// @}
}

