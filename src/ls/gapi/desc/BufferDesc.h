/// @file ls/gapi/desc/BufferDesc.h
/// @brief Renderer Buffer Desc 
#pragma once

#include "ls/common.h"
#include "ls/gapi/enum.h"
#include "ls/gapi/GapiPointer.h"
#include "ls/gapi/desc/StateDesc.h"

namespace ls
{
    /// @addtogroup LsGapi
    /// @{

    /// @brief Buffer Desc
    struct BufferDesc
    {
        /// @brief Buffer size
        uint size;
        /// @brief Buffer type
        BufferType type;
        /// @brief Resource usage mode
        ResourceUsage usage;
    };
    /// @brief Buffer Handle
    using BufferHandle = GapiPointer<BufferDesc>;
    /// @brief Geometry Bucket
    struct GeometryBucket
    {
        /// @brief Ctor
        GeometryBucket() = default;
        /// @brief Ctor
        GeometryBucket(const uint numVertexBuffers,
                       BufferHandle indexBuffer,
                       const IndexFormat indexFormat = IndexFormat::Unknown)
            : numVertexBuffers(numVertexBuffers)
            , indexBuffer(indexBuffer)
            , indexFormat(indexFormat)
        {
        }
        /// @brief Ctor
        GeometryBucket(BufferHandle vertexBuffer,
                       BufferHandle indexBuffer,
                       const IndexFormat indexFormat = IndexFormat::Unknown)
            : numVertexBuffers(1)
            , indexBuffer(indexBuffer)
            , indexFormat(indexFormat)
        {
            vertexBuffers[0] = vertexBuffer;
        }
        /// @brief Add vertex buffer
        void addVertexBuffer(BufferHandle buffer)
        {
            debug_assert(numVertexBuffers < MaxStreams);
            vertexBuffers[numVertexBuffers++] = buffer;
        }
        /// @brief Num vertex buffers
        uint numVertexBuffers = 0;
        /// @brief Vertex buffers
        array<BufferHandle, MaxStreams> vertexBuffers;
        /// @brief Index buffer
        BufferHandle indexBuffer;
        /// @brief Index format
        IndexFormat indexFormat = IndexFormat::Unknown;
    };
    /// @}
}

