/// @file ls/gapi/desc/StateDesc.h
/// @brief GAPI state desc
#pragma once

#include "ls/common.h"
#include "ls/gapi/enum.h"
#include "ls/gapi/GapiPointer.h"
#include "ls/gapi/desc/EffectDesc.h"

namespace ls
{
    /// @addtogroup LsGapi
    /// @{

    /// @brief Disable Output tag
    struct DisableOutputTag
    {
    };
    /// @brief No Blending tag
    struct NoBlendingTag
    {
    };
    /// @brief Alpha Blending tag
    struct AlphaBlendingTag
    {
    };
    /// @brief Additive Blending tag
    struct AdditiveBlendingTag
    {
    };
    /// @brief Blending Desc
    struct BlendingDesc
    {
        /// @brief No blending
        BlendingDesc() = default;
        /// @brief No blending
        BlendingDesc(NoBlendingTag)
        {
        }
        /// @brief Disable output
        BlendingDesc(DisableOutputTag)
            : writeMask(0, 0, 0, 0)
        {
        }
        /// @brief Alpha blending
        BlendingDesc(AlphaBlendingTag)
            : hasBlending(true)
            , sourceColor(BlendFactor::SrcAlpha)
            , sourceAlpha(BlendFactor::One)
            , destColor(BlendFactor::SrcAlphaInv)
            , destAlpha(BlendFactor::Zero)
            , colorOp(BlendOperation::Add)
            , alphaOp(BlendOperation::Add)
        {
        }
        /// @brief Additive blending
        BlendingDesc(AdditiveBlendingTag)
            : hasBlending(true)
            , sourceColor(BlendFactor::One)
            , sourceAlpha(BlendFactor::One)
            , destColor(BlendFactor::One)
            , destAlpha(BlendFactor::One)
            , colorOp(BlendOperation::Add)
            , alphaOp(BlendOperation::Add)
        {
        }
        /// @brief Is blending enabled?
        bool hasBlending = false;
        /// @brief Source color blend factor
        BlendFactor sourceColor = BlendFactor::One;
        /// @brief Source alpha blend factor
        BlendFactor sourceAlpha = BlendFactor::One;
        /// @brief Destination color blend factor
        BlendFactor destColor = BlendFactor::Zero;
        /// @brief Destination alpha blend factor
        BlendFactor destAlpha = BlendFactor::Zero;
        /// @brief Color operation
        BlendOperation colorOp = BlendOperation::Add;
        /// @brief Alpha operation
        BlendOperation alphaOp = BlendOperation::Add;
        /// @brief Write mask
        ubyte4 writeMask = ubyte4(1, 1, 1, 1);
    };
    /// @brief Rasterizer Desc
    struct RasterizerDesc
    {
        /// @brief Is wireframe?
        bool isWireframe = false;
        /// @brief Culling
        Culling culling = Culling::None;
        /// @brief Front face
        bool ccwFront = false;
        /// @brief Depth bias
        sint depthBias = 0;
        /// @brief Depth slope-scaled bias
        float depthSlopeScaledBias = 0.0f;
        /// @brief Depth bias clamp
        float depthBiasClamp = 0.0f;
    };
    /// @brief Stencil Operation Info
    struct StencilDesc
    {
        /// @brief Per-side operation
        struct SideOp
        {
            /// @brief Operation on stencil test fail
            StencilOperation stencilFail = StencilOperation::Keep;
            /// @brief Operation on depth test fail
            StencilOperation depthFail = StencilOperation::Keep;
            /// @brief Operation on success
            StencilOperation pass = StencilOperation::Keep;
            /// @brief Stencil test function
            Comparison stencilFun = Comparison::Never;
        };
        /// @brief Front operation
        SideOp front;
        /// @brief Back operation
        SideOp back;
        /// @brief Stencil write mask
        ubyte writeMask = 0;
        /// @brief Stencil read mask
        ubyte readMask = 0;
    };
    /// @brief Vertex Layout Element
    struct VertexFormat
    {
        /// @brief Ctor
        VertexFormat() = default;
        /// @brief Ctor
        VertexFormat(const Format type, const char* name,
                     const uint stream, const uint offset,
                     const bool isInstanced)
            : type(type)
            , name(name)
            , stream(stream)
            , offset(offset)
            , isInstanced(isInstanced)
        {
        }
        /// @brief Type
        Format type = Format::Unknown;
        /// @brief Name
        string name = "";
        /// @brief Source stream index
        uint stream = 0;
        /// @brief Offset
        uint offset = 0;
        /// @brief Is instanced
        bool isInstanced = false;
    };
    /// @brief Layout Desc
    struct LayoutDesc
    {
        /// @brief Number of streams
        uint numStreams;
        /// @brief Streams strides
        array<uint, MaxStreams> strides;
        /// @brief Vertex layout size
        uint numElements = 0;
        /// @brief Vertex layout
        array<VertexFormat, MaxVertexLayoutSize> elements;
    };
    /// @brief Layout Handle
    using LayoutHandle = GapiPointer<LayoutDesc>;
    /// @brief State Desc
    struct StateDesc
    {
        /// @brief Ctor
        StateDesc() = default;
        /// @brief Quick ctor
        StateDesc(ProgramHandle program)
            : program(program)
        {
        }
        /// @brief Program
        ProgramHandle program = nullptr;
        /// @brief Use independent blending mode 
        bool hasIndependentBlending = false;
        /// @brief Blending desc
        array<BlendingDesc, MaxRenderTargets> blending;
        /// @brief Rasterizer desc
        RasterizerDesc rasterizer;
        /// @brief Is depth test enabled?
        bool hasDepthTest = true;
        /// @brief Is depth write enabled?
        bool hasDepthWrite = false;
        /// @brief Depth function
        Comparison depthFunction = Comparison::LessEqual;
        /// @brief Is stencil test
        bool hasStencil = false;
        /// @brief Stencil op desc
        StencilDesc stencil;
        /// @brief Layout
        LayoutHandle layout = nullptr;
        /// @brief Primitive type
        PrimitiveType primitive = PrimitiveType::TriangleStrip;
    };
    /// @brief State Handle
    using StateHandle = GapiPointer<StateDesc>;
    /// @}
}

