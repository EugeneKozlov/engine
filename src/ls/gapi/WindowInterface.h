/// @file ls/gapi/WindowInterface.h
/// @brief Abstract window interface.
#pragma once

#include "ls/common.h"
#include "ls/gapi/Renderer.h"

namespace ls
{
    /// @addtogroup LsGapi
    /// @{

    /// @brief Abstract window interface.
    class WindowInterface
    {
    protected:
        /// @brief Ctor
        WindowInterface();
    public:
        /// @brief Swap buffers
        virtual void swap() = 0;
        /// @brief Resize window
        virtual void resize(const uint width, const uint height) = 0;
        /// @brief Get renderer
        virtual Renderer& renderer() = 0;
        /// @brief Dtor
        virtual ~WindowInterface();
    protected:
    };
    /// @}
}

