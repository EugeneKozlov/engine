#include "pch.h"
#include "ls/gapi/BufferPool.h"
#include "ls/gapi/Renderer.h"

using namespace ls;
// Bucket
BufferPool::Bucket::Bucket(const BucketDesc& desc, uint size)
    : renderer(desc.renderer)
{
    buffer = renderer->createBuffer(size, desc.bufferType,
                                    ResourceUsage::Dynamic,
                                    nullptr);
    LS_THROW(!!buffer,
             OutOfMemoryException,
             "Cannot allocate buffer of [", size, "] bytes");
}
void* BufferPool::Bucket::data()
{
    if (!ptr)
    {
        ptr = renderer->mapBuffer(buffer);
    }
    return ptr;
}
void BufferPool::Bucket::commit()
{
    if (ptr)
    {
        renderer->unmapBuffer(buffer);
        ptr = nullptr;
    }
}
void BufferPool::Bucket::dispose()
{
    commit();
    if (!!buffer)
    {
        renderer->destroyBuffer(buffer);
        buffer = nullptr;
    }
}


// Main
BufferPool::BufferPool(Renderer& renderer, BufferType bufferType, 
                       uint bufferSize, uint resizeFactor)
    : impl(bufferSize, resizeFactor, BucketDesc(renderer, bufferType))
{
}
BufferPool::~BufferPool()
{
}


// Allocate
BufferPoolAllocation BufferPool::allocate(uint count, uint size)
{
    auto info = impl.allocate(count * size, size);

    BufferPoolAllocation ref;
    ref.ptr = info.ptr;
    ref.offset = info.startBlock;
    ref.count = count;
    ref.size = count * size;
    ref.buffer = info.bucket->buffer;
    return ref;
}


// Manage
void BufferPool::commit()
{
    impl.commit();
}
bool BufferPool::discard()
{
    return impl.discard();
}
void BufferPool::purge()
{
    impl.purge();
}
