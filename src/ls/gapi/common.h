/// @file ls/gapi/common.h
/// @brief Common GAPI types and enums
#pragma once

#include "ls/common.h"
#include "ls/gapi/enum.h"
#include "ls/gapi/functions.h"
#include "ls/gapi/desc/TextureDesc.h"
#include "ls/gapi/desc/BufferDesc.h"
#include "ls/gapi/desc/StateDesc.h"
#include "ls/gapi/desc/EffectDesc.h"
