/// @file ls/gapi/common.h
/// @brief Common GAPI types
#pragma once

#include "ls/common.h"

namespace ls
{
    /// @addtogroup LsGapi
    /// @{

    /// @brief Pointer
    template <class Base>
    class GapiPointer
        : HashableConcept
    {
    public:
        /// @brief Null 
        GapiPointer() = default;
        /// @brief Null
        GapiPointer(nullptr_t)
        {
        }
        /// @brief Valid
        GapiPointer(Base* ptr)
            : m_ptr(ptr)
        {
        }
        /// @brief Copy
        GapiPointer(const GapiPointer<Base>& another)
            : m_ptr(another.m_ptr)
        {
        }
        /// @brief Get hash
        uint hash() const noexcept
        {
            return reinterpret_cast<uint>(m_ptr);
        }
        /// @brief Get pure pointer
        const void* pointer() const
        {
            return static_cast<void*>(m_ptr);
        }
        /// @brief Dereference
        const Base* operator -> () const
        {
            return m_ptr;
        }
        /// @brief Compare
        bool operator == (const GapiPointer<Base>& another) const
        {
            return m_ptr == another.m_ptr;
        }
        /// @brief Compare
        bool operator != (const GapiPointer<Base>& another) const
        {
            return m_ptr != another.m_ptr;
        }
        /// @brief Compare
        bool operator < (const GapiPointer<Base>& another) const
        {
            return m_ptr < another.m_ptr;
        }
        /// @brief Cast to bool
        bool operator !() const
        {
            return !m_ptr;
        }
        /// @brief Cast to impl
        template <class Impl>
        Impl& get()
        {
            debug_assert(m_ptr);
            return *static_cast<Impl*>(m_ptr);
        }
        /// @brief Cast to impl
        template <class Impl>
        const Impl& get() const
        {
            debug_assert(m_ptr);
            return *static_cast<const Impl*>(m_ptr);
        }
    private:
        Base* m_ptr = nullptr;
    };
    /// @}
}