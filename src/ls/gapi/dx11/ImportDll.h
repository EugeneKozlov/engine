/** @file ls/gapi/dx11/ImportDll.h
 *  @brief DX11 DLL loader
 */
#pragma once
#include "ls/common/config.h"

#if LS_COMPILE_DX11 != 0

#include <dx11.h>

/// @brief CreateDXGIFactory type
typedef HRESULT(WINAPI* PFN_CREATE_DXGI_FACTORY)(REFIID, void * );
/// CreateDXGIFactory function
extern PFN_CREATE_DXGI_FACTORY CreateDXGIFactoryDll;
/// D3D11CreateDevice function
extern PFN_D3D11_CREATE_DEVICE D3D11CreateDeviceDll;
/// D3D11CreateDeviceAndSwapChain function
extern PFN_D3D11_CREATE_DEVICE_AND_SWAP_CHAIN D3D11CreateDeviceAndSwapChainDll;

/// @brief D3DGetInputSignatureBlob type
typedef HRESULT(WINAPI* PFN_D3D_GET_INPUT_SIGNATURE_BLOB)(LPCVOID, SIZE_T, ID3DBlob**);
/// D3DCompile type
typedef pD3DCompile PFN_D3D_COMPILE;
/// D3DDisassemble type
typedef pD3DDisassemble PFN_D3D_DISASSEMBLE;
/// D3DReflect type
typedef HRESULT(WINAPI* PFN_D3D_REFLECT)(LPCVOID, SIZE_T, REFIID, void**);
/// D3DGetInputSignatureBlob function
extern PFN_D3D_GET_INPUT_SIGNATURE_BLOB D3DGetInputSignatureBlobDll;
/// D3DCompile function
extern PFN_D3D_COMPILE D3DCompileDll;
/// D3DReflect function
extern PFN_D3D_REFLECT D3DReflectDll;
/// D3DDisassemble function
extern PFN_D3D_DISASSEMBLE D3DDisassembleDll;
/// IID of ID3D11ShaderReflection
extern const GUID iid_ID3D11ShaderReflection;

/// @brief Load functions
bool loadDirect3D11();
/// Unload functions
void unloadDirect3D11();

#endif