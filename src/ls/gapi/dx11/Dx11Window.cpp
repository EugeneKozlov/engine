#include "pch.h"
#include "ls/gapi/dx11/Dx11Window.h"
#include "ls/gapi/dx11/ImportDll.h"

#if LS_COMPILE_DX11 != 0

using namespace ls;
using namespace ls::gapi;

Dx11Window::Dx11Window(GLFWwindow* holder, const uint width, const uint height)
    : WindowInterface()
    , m_holder(holder)

    , m_width(width)
    , m_height(height)
{
    loadDirect3D11();
    initWindow();
    initDevice();

    initRenderTarget();
    m_renderer->setDefaultRenderTarget(m_renderTarget);
    m_renderer->resize((uint) width, (uint) height);
}
void Dx11Window::initWindow()
{
    m_parent = glfwGetWin32Window(m_holder);
    int width, height;
    glfwGetFramebufferSize(m_holder, &width, &height);
    m_handle = CreateWindowA("STATIC", "", WS_CHILD, 0, 0,
                             width, height, m_parent, 0, 0, 0);
    LS_THROW(m_handle,
             ApplicationFailException,
             "Cannot create window");
    ShowWindow(m_handle, SW_SHOW);
    UpdateWindow(m_handle);
    m_hDC = GetWindowDC(m_handle);
}
void Dx11Window::destroyWindow()
{
    // Window
    DestroyWindow(m_handle);
}
void Dx11Window::initDevice()
{
    int width, height;
    glfwGetFramebufferSize(m_holder, &width, &height);

    // Init swap chain
    HRESULT result;
    DXGI_SWAP_CHAIN_DESC sd;
    ZeroMemory(&sd, sizeof (sd));
    sd.BufferCount = 2;
    sd.BufferDesc.Width = (UINT) width;
    sd.BufferDesc.Height = (UINT) height;
//     sd.BufferDesc.Format = DXGI_FORMAT_R10G10B10A2_UNORM;
    sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    sd.BufferDesc.RefreshRate.Numerator = 60;
    sd.BufferDesc.RefreshRate.Denominator = 1;
    sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    sd.OutputWindow = m_handle;
    sd.SampleDesc.Count = 1;
    sd.SampleDesc.Quality = 0;
    sd.Windowed = TRUE;

    // Create all
    const UINT flags = D3D11_CREATE_DEVICE_DEBUG;

//     IDXGIFactory * pFactory;
//     HRESULT hr = CreateDXGIFactoryDll(__uuidof(IDXGIFactory), (void**) (&pFactory));
//     IDXGIAdapter * pAdapters[4];
//     pFactory->EnumAdapters(0, pAdapters + 0);
//     pFactory->EnumAdapters(1, pAdapters + 1);
//     pFactory->EnumAdapters(2, pAdapters + 2);
//     pFactory->EnumAdapters(3, pAdapters + 3);
//     DXGI_ADAPTER_DESC pAdapterDescs[3];
//     pAdapters[0]->GetDesc(pAdapterDescs + 0);
//     pAdapters[1]->GetDesc(pAdapterDescs + 1);
//     pAdapters[2]->GetDesc(pAdapterDescs + 2);
//     result = D3D11CreateDeviceAndSwapChainDll(pAdapters[1], D3D_DRIVER_TYPE_UNKNOWN, NULL, flags, NULL, 0,
//                                               D3D11_SDK_VERSION, &sd,
//                                               &m_swapChain, &m_device, &m_featureLevel, &m_context);
    result = D3D11CreateDeviceAndSwapChainDll(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, flags, NULL, 0,
                                              D3D11_SDK_VERSION, &sd,
                                              &m_swapChain, &m_device, &m_featureLevel, &m_context);
    LS_THROW(SUCCEEDED(result), ApplicationFailException, "Cannot create DX11 device");

    // Init renderer
    m_renderer = make_unique<Dx11Renderer>(m_device, m_context);
}
void Dx11Window::destroyDevice()
{
    m_renderer->deinitialize();
    m_renderer.reset();

    safeRelease(m_renderTarget);
    safeRelease(m_context);
    safeRelease(m_swapChain);
    safeRelease(m_device);
}
void Dx11Window::initRenderTarget()
{
    ID3D11Texture2D* backBuffer = nullptr;
    HRESULT result = m_swapChain->GetBuffer(0, IID_ID3D11Texture2D, reinterpret_cast<LPVOID*>(&backBuffer));
    LS_THROW(SUCCEEDED(result), ApplicationFailException, "Cannot get frame buffer");

    result = m_device->CreateRenderTargetView(backBuffer, NULL, &m_renderTarget);
    LS_THROW(SUCCEEDED(result), ApplicationFailException, "Cannot create render target");
    backBuffer->Release();
}
Dx11Window::~Dx11Window()
{
    destroyDevice();
    destroyWindow();
    unloadDirect3D11();
}
void Dx11Window::swap()
{
    if (m_locked)
    {
        SetWindowPos(m_handle, NULL, 0, 0, (int) m_width, (int) m_height, 0);
        safeRelease(m_renderTarget);
        m_swapChain->ResizeBuffers(2, m_width, m_height, DXGI_FORMAT_R10G10B10A2_UNORM , 0);
        initRenderTarget();
        m_renderer->resize(m_width, m_height);
        m_renderer->setDefaultRenderTarget(m_renderTarget);
        m_locked = false;
    }
    m_swapChain->Present(0, 0);
}
void Dx11Window::resize(const uint width, const uint height)
{
    m_width = width;
    m_height = height;
    m_locked = true;
}
Renderer& Dx11Window::renderer()
{
    return *m_renderer;
}

#endif