#include "pch.h"
#include "ls/gapi/dx11/Dx11Renderer.h"
#include "ls/gapi/usl/compiler_hlsl.h"

#if LS_COMPILE_DX11 != 0

using namespace ls;
using namespace ls::gapi;
Dx11Renderer::Dx11Renderer(ID3D11Device* device, ID3D11DeviceContext* context)
    : Renderer()
    , m_device(device)
    , m_context(context)
{
    m_tessellationSupport = device->GetFeatureLevel() >= D3D_FEATURE_LEVEL_11_0;
    m_tessellationSupport = false;
    m_debugOutputSupport = true;

    // Default sampler
    CD3D11_SAMPLER_DESC desc(CD3D11_DEFAULT{});
    m_device->CreateSamplerState(&desc, &m_defaultSampler);

    // Reset
    m_currentVertexBuffers.fill(nullptr);
}
Dx11Renderer::~Dx11Renderer()
{
    m_defaultSampler->Release();
}


// Effect
Stream gapi::Dx11Renderer::compileEffect(usl::Effect& source)
{
    Stream dest = StreamInterface::open();
    usl::compileEffect(usl::Compiler<usl::TargetLanguage::HLSL30>(), source, *dest);
    return dest;
}
EffectHandle Dx11Renderer::createEffect(StreamInterface& compiled)
{
    EffectImpl impl;
    Disposer<EffectImpl> guard(impl);
    try
    {
        usl::deserializeEffect(compiled, impl);
        
        // Create shaders
        const uint numShaders = impl.shadersTypes.size();
        for (uint index = 0; index < numShaders; ++index)
        {
            const ShaderType shaderType = impl.shadersTypes[index];
            const uint shaderOffset = impl.shadersOffsets[index];
            const uint shaderSize = impl.shadersSizes[index];
            const ubyte* shaderData = impl.shadersData.data() + shaderOffset;

            // Skip tessellation if not supported
            if (isTessellation(shaderType) && !m_tessellationSupport)
            {
                impl.m_shaders.push_back(nullptr);
                continue;
            }

            // Create
            ID3D11DeviceChild* shader = nullptr;
            switch (shaderType)
            {
            case ShaderType::Vertex:
                m_device->CreateVertexShader(shaderData, shaderSize, nullptr,
                                             (ID3D11VertexShader**) &shader);
                break;
            case ShaderType::TessControl:
                m_device->CreateHullShader(shaderData, shaderSize, nullptr,
                                           (ID3D11HullShader**) &shader);
                break;
            case ShaderType::TessEval:
                m_device->CreateDomainShader(shaderData, shaderSize, nullptr,
                                             (ID3D11DomainShader**) &shader);
                break;
            case ShaderType::Geometry:
                m_device->CreateGeometryShader(shaderData, shaderSize, nullptr,
                                               (ID3D11GeometryShader**) &shader);
                break;
            case ShaderType::Pixel:
                m_device->CreatePixelShader(shaderData, shaderSize, nullptr,
                                            (ID3D11PixelShader**) &shader);
                break;
            default:
                break;
            }

            // Save
            impl.m_shaders.push_back(shader);
        }

        // Init programs
        const uint numPrograms = impl.programs.size();
        for (uint index = 0; index < numPrograms; ++index)
        {
            auto& program = impl.programs[index];

            ProgramImpl pimpl;
            pimpl.index = index;
            pimpl.m_vertexShader = impl.findShader<ID3D11VertexShader>(program.vertex);
            pimpl.m_hullShader = impl.findShader<ID3D11HullShader>(program.hull);
            pimpl.m_domainShader = impl.findShader<ID3D11DomainShader>(program.domain);
            pimpl.m_geometryShader = impl.findShader<ID3D11GeometryShader>(program.geometry);
            pimpl.m_pixelShader = impl.findShader<ID3D11PixelShader>(program.pixel);

            impl.m_programs.push_back(pimpl);
        }

        // Clear cached
        impl.m_cachedBuffers.fill(nullptr);

        // Init resources
        const uint numResources = impl.resourceNames.size();
        for (uint index = 0; index < numResources; ++index)
        {
            EffectResourceImpl rimpl;
            rimpl.slot = index;

            impl.m_resources.push_back(rimpl);
        }
    }
    catch (const Exception& ex)
    {
        logError("Cannot create effect: ", ex.what());
        return nullptr;
    }

    guard.deactivate();

    lock_guard<mutex> ts(m_mutex);
    EffectHandle handle = m_pooledEffects.construct(impl);

    // Init self
    EffectImpl& effect = handle.get<EffectImpl>();
    for (ProgramImpl& program : effect.m_programs)
    {
        program.effect = handle;
        program.m_effect = &effect;
    }
    for (EffectResourceImpl& resource : effect.m_resources)
    {
        resource.effect = handle;
        resource.m_effect = &effect;
    }
    return handle;
}
void Dx11Renderer::destroyEffect(EffectHandle effectId)
{
    lock_guard<mutex> ts(m_mutex);
    destroyResource(m_pooledEffects, effectId);
}
bool Dx11Renderer::attachUniformBuffer(EffectHandle effectId,
                                       BufferHandle bufferId,
                                       const char* uniformBufferName)
{
    EffectImpl& effect = effectId.get<EffectImpl>();
    const uint* bufferIndex = findOrDefault(effect.bufferNames, uniformBufferName);
    if (!bufferIndex)
    {
        logWarning("Uniform buffer cannot be attached [", uniformBufferName, "]");
        return false;
    }

    if (effect.m_cachedBuffers[*bufferIndex])
    {
        effect.m_cachedBuffers[*bufferIndex]->Release();
        effect.m_cachedBuffers[*bufferIndex] = nullptr;
    }
    if (!!bufferId)
    {
        BufferImpl& buffer = bufferId.get<BufferImpl>();
        buffer.m_buffer->AddRef();
        effect.m_cachedBuffers[*bufferIndex] = buffer.m_buffer;
    }
    m_currentEffect = nullptr;
    return true;
}
ProgramHandle Dx11Renderer::handleProgram(EffectHandle effectId,
                                          const char* programName)
{
    EffectImpl& effect = effectId.get<EffectImpl>();
    uint* programIndex = findOrDefault(effect.programNames, programName);
    if (!programIndex)
        return nullptr;
    else
        return ProgramHandle(&effect.m_programs[*programIndex]);
}
EffectResourceHandle Dx11Renderer::handleResource(EffectHandle effectId,
                                                  const char* resourceName)
{
    EffectImpl& effect = effectId.get<EffectImpl>();
    const uint* resourceIndex = findOrDefault(effect.resourceNames, resourceName);
    if (!resourceIndex)
        return nullptr;
    else
        return EffectResourceHandle(&effect.m_resources[*resourceIndex]);
}


// State
namespace
{
    static D3D11_BLEND dx11BlendFactor(const BlendFactor factor)
    {
        switch (factor)
        {
        case BlendFactor::Zero: return D3D11_BLEND_ZERO;
        case BlendFactor::One: return D3D11_BLEND_ONE;
        case BlendFactor::SrcColor: return D3D11_BLEND_SRC_COLOR;
        case BlendFactor::SrcColorInv: return D3D11_BLEND_INV_SRC_COLOR;
        case BlendFactor::SrcAlpha: return D3D11_BLEND_SRC_ALPHA;
        case BlendFactor::SrcAlphaInv: return D3D11_BLEND_INV_SRC_ALPHA;
        case BlendFactor::DestColor: return D3D11_BLEND_DEST_COLOR;
        case BlendFactor::DestColorInv: return D3D11_BLEND_INV_DEST_COLOR;
        case BlendFactor::DestAlpha: return D3D11_BLEND_DEST_ALPHA;
        case BlendFactor::DestAlphaInv: return D3D11_BLEND_INV_DEST_ALPHA;
        default: return (D3D11_BLEND) 0;
        }
    }
    static D3D11_BLEND_OP dx11BlendOperation(const BlendOperation op)
    {
        switch (op)
        {
        case BlendOperation::Add: return D3D11_BLEND_OP_ADD;
        case BlendOperation::Sub: return D3D11_BLEND_OP_SUBTRACT;
        case BlendOperation::SubInv: return D3D11_BLEND_OP_REV_SUBTRACT;
        default: return (D3D11_BLEND_OP) 0;
        }
    }
    static D3D11_PRIMITIVE_TOPOLOGY dx11Primitive(const PrimitiveType primitive)
    {
        switch (primitive)
        {
        case PrimitiveType::Point: return D3D11_PRIMITIVE_TOPOLOGY_POINTLIST;
        case PrimitiveType::Line: return D3D11_PRIMITIVE_TOPOLOGY_LINELIST;
        case PrimitiveType::LineStrip: return D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP;
        case PrimitiveType::Triangle: return D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
        case PrimitiveType::TriangleStrip: return D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP;
        case PrimitiveType::Patch3: return D3D11_PRIMITIVE_TOPOLOGY_3_CONTROL_POINT_PATCHLIST;
        case PrimitiveType::Patch4: return D3D11_PRIMITIVE_TOPOLOGY_4_CONTROL_POINT_PATCHLIST;
        default: return (D3D11_PRIMITIVE_TOPOLOGY) 0;
        }
    }
    static D3D11_CULL_MODE dx11CullMode(const Culling culling)
    {
        switch (culling)
        {
        case Culling::None: return D3D11_CULL_NONE;
        case Culling::Back: return D3D11_CULL_BACK;
        case Culling::Front: return D3D11_CULL_FRONT;
        default: return (D3D11_CULL_MODE) 0;
        }
    }
    static D3D11_COMPARISON_FUNC dx11Comparison(const Comparison comparison)
    {
        switch (comparison)
        {
        case Comparison::Never: return D3D11_COMPARISON_NEVER;
        case Comparison::Always: return D3D11_COMPARISON_ALWAYS;
        case Comparison::Equal: return D3D11_COMPARISON_EQUAL;
        case Comparison::NotEqual: return D3D11_COMPARISON_NOT_EQUAL;
        case Comparison::Less: return D3D11_COMPARISON_LESS;
        case Comparison::LessEqual: return D3D11_COMPARISON_LESS_EQUAL;
        case Comparison::Greater: return D3D11_COMPARISON_GREATER;
        case Comparison::GreaterEqual: return D3D11_COMPARISON_GREATER_EQUAL;
        default: return D3D11_COMPARISON_ALWAYS;
        }
    }
    static D3D11_STENCIL_OP dx11StencilOp(const StencilOperation op)
    {
        switch (op)
        {
        case StencilOperation::Keep: return D3D11_STENCIL_OP_KEEP;
        case StencilOperation::Zero: return D3D11_STENCIL_OP_ZERO;
        case StencilOperation::Replace: return D3D11_STENCIL_OP_REPLACE;
        case StencilOperation::Inc: return D3D11_STENCIL_OP_INCR;
        case StencilOperation::IncSat: return D3D11_STENCIL_OP_INCR_SAT;
        case StencilOperation::Dec: return D3D11_STENCIL_OP_DECR;
        case StencilOperation::DecSat: return D3D11_STENCIL_OP_DECR_SAT;
        case StencilOperation::Invert: return D3D11_STENCIL_OP_INVERT;
        default: return (D3D11_STENCIL_OP) 0;
        }
    }
}
StateHandle Dx11Renderer::createState(const StateDesc& desc)
{
    StateImpl impl;
    Disposer<StateImpl> guard(impl);
    static_cast<StateDesc&>(impl) = desc;
    HRESULT result;

    // Blend state
    D3D11_BLEND_DESC bsDesc;
    zeroMemory(bsDesc);
    bsDesc.AlphaToCoverageEnable = false;
    bsDesc.IndependentBlendEnable = desc.hasIndependentBlending;
    for (uint index = 0; index < (!desc.hasIndependentBlending ? 1 : MaxRenderTargets); ++index)
    {
        auto& src = desc.blending[index];
        auto& dest = bsDesc.RenderTarget[index];

        dest.BlendEnable = src.hasBlending;
        dest.RenderTargetWriteMask = 0;
        if (src.writeMask.x) dest.RenderTargetWriteMask |= D3D11_COLOR_WRITE_ENABLE_RED;
        if (src.writeMask.y) dest.RenderTargetWriteMask |= D3D11_COLOR_WRITE_ENABLE_GREEN;
        if (src.writeMask.z) dest.RenderTargetWriteMask |= D3D11_COLOR_WRITE_ENABLE_BLUE;
        if (src.writeMask.w) dest.RenderTargetWriteMask |= D3D11_COLOR_WRITE_ENABLE_ALPHA;

        dest.SrcBlend = dx11BlendFactor(src.sourceColor);
        dest.DestBlend = dx11BlendFactor(src.destColor);
        dest.BlendOp = dx11BlendOperation(src.colorOp);

        dest.SrcBlendAlpha = dx11BlendFactor(src.sourceAlpha);
        dest.DestBlendAlpha = dx11BlendFactor(src.destAlpha);
        dest.BlendOpAlpha = dx11BlendOperation(src.alphaOp);
    }
    result = m_device->CreateBlendState(&bsDesc, &impl.m_bs);
    if (FAILED(result))
        return nullptr;

    // Rasterizer
    D3D11_RASTERIZER_DESC rsDesc;
    rsDesc.FillMode = desc.rasterizer.isWireframe
        ? D3D11_FILL_WIREFRAME : D3D11_FILL_SOLID;
    rsDesc.CullMode = dx11CullMode(desc.rasterizer.culling);
    rsDesc.FrontCounterClockwise = desc.rasterizer.ccwFront;
    rsDesc.DepthBias = desc.rasterizer.depthBias;
    rsDesc.SlopeScaledDepthBias = desc.rasterizer.depthSlopeScaledBias;
    rsDesc.DepthBiasClamp = desc.rasterizer.depthBiasClamp;
    rsDesc.DepthClipEnable = true;
    rsDesc.ScissorEnable = false;
    rsDesc.MultisampleEnable = false;
    rsDesc.AntialiasedLineEnable = false;
    result = m_device->CreateRasterizerState(&rsDesc, &impl.m_rs);
    if (FAILED(result))
        return nullptr;

    // Ds
    D3D11_DEPTH_STENCIL_DESC dssDesc;
    dssDesc.DepthEnable = desc.hasDepthTest;
    dssDesc.DepthWriteMask = desc.hasDepthWrite
        ? D3D11_DEPTH_WRITE_MASK_ALL
        : D3D11_DEPTH_WRITE_MASK_ZERO;
    dssDesc.DepthFunc = dx11Comparison(desc.depthFunction);
    dssDesc.StencilEnable = desc.hasStencil;
    dssDesc.StencilReadMask = desc.stencil.readMask;
    dssDesc.StencilWriteMask = desc.stencil.writeMask;
    dssDesc.BackFace.StencilFunc = dx11Comparison(desc.stencil.back.stencilFun);
    dssDesc.BackFace.StencilFailOp = dx11StencilOp(desc.stencil.back.stencilFail);
    dssDesc.BackFace.StencilDepthFailOp = dx11StencilOp(desc.stencil.back.depthFail);
    dssDesc.BackFace.StencilPassOp = dx11StencilOp(desc.stencil.back.pass);
    dssDesc.FrontFace.StencilFunc = dx11Comparison(desc.stencil.front.stencilFun);
    dssDesc.FrontFace.StencilFailOp = dx11StencilOp(desc.stencil.front.stencilFail);
    dssDesc.FrontFace.StencilDepthFailOp = dx11StencilOp(desc.stencil.front.depthFail);
    dssDesc.FrontFace.StencilPassOp = dx11StencilOp(desc.stencil.front.pass);
    result = m_device->CreateDepthStencilState(&dssDesc, &impl.m_dss);
    if (FAILED(result))
        return nullptr;

    // Effect
    ProgramImpl& program = impl.program.get<ProgramImpl>();
    EffectImpl& effect = *program.m_effect;
    impl.m_vertexShader = program.m_vertexShader;
    impl.m_hullShader = program.m_hullShader;
    impl.m_domainShader = program.m_domainShader;
    impl.m_geometryShader = program.m_geometryShader;
    impl.m_pixelShader = program.m_pixelShader;

    impl.m_uniformBuffers = effect.m_cachedBuffers.data();

    impl.m_topology = dx11Primitive(desc.primitive);
    if (!!desc.layout)
    {
        const LayoutImpl& layout = desc.layout.get<LayoutImpl>();
        impl.m_layout = layout.m_layout;
        impl.m_strides = &layout.strides[0];
    }

    guard.deactivate();
    return m_pooledStates.construct(impl);
}
void Dx11Renderer::destroyState(StateHandle stateId)
{
    destroyResource(m_pooledStates, stateId);
}


// Buffer
namespace
{
    static UINT dx11BindFlags(const BufferType type)
    {
        switch (type)
        {
        case BufferType::Vertex:
            return D3D11_BIND_VERTEX_BUFFER;
        case BufferType::Index:
            return D3D11_BIND_INDEX_BUFFER;
        case BufferType::Uniform:
            return D3D11_BIND_CONSTANT_BUFFER;
        }
        return 0;
    }
    static UINT dx11CpuAccess(const ResourceUsage usage)
    {
        switch (usage)
        {
        case ResourceUsage::Constant:
        case ResourceUsage::Static:
            return 0;
        case ResourceUsage::Dynamic:
            return D3D11_CPU_ACCESS_WRITE;
        }
        return 0;
    }
    static D3D11_USAGE dx11Usage(const ResourceUsage usage)
    {
        switch (usage)
        {
        case ResourceUsage::Constant:
            return D3D11_USAGE_IMMUTABLE;
        case ResourceUsage::Static:
            return D3D11_USAGE_DEFAULT;
        case ResourceUsage::Dynamic:
            return D3D11_USAGE_DYNAMIC;
        }
        return (D3D11_USAGE) 0;
    }
    static D3D11_MAP dx11MapFlag(const MappingFlag mapping)
    {
        switch (mapping)
        {
        case MappingFlag::Read:
            return D3D11_MAP_READ;
        case MappingFlag::Write:
            return D3D11_MAP_WRITE;
        case MappingFlag::WriteDiscard:
            return D3D11_MAP_WRITE_DISCARD;
        }
        return (D3D11_MAP) 0;
    }
}
BufferHandle Dx11Renderer::createBuffer(const uint size, const BufferType type, const ResourceUsage usage,
                                        const void* data)
{
    BufferImpl impl;
    impl.size = size;
    impl.type = type;
    impl.usage = usage;

    const uint realSize = std::max<uint>(16, size);
    impl.m_desc.BindFlags = dx11BindFlags(type);
    impl.m_desc.ByteWidth = realSize;
    impl.m_desc.CPUAccessFlags = dx11CpuAccess(usage);
    impl.m_desc.MiscFlags = 0;
    impl.m_desc.StructureByteStride = 0;
    impl.m_desc.Usage = dx11Usage(usage);

    D3D11_SUBRESOURCE_DATA src;
    src.SysMemPitch = realSize;
    src.SysMemSlicePitch = realSize;
    src.pSysMem = data;

    HRESULT result = m_device->CreateBuffer(&impl.m_desc, data ? &src : nullptr, &impl.m_buffer);
    if (FAILED(result))
        return nullptr;

    lock_guard<mutex> ts(m_mutex);
    return m_pooledBuffers.construct(impl);
}
void Dx11Renderer::destroyBuffer(BufferHandle bufferId)
{
    lock_guard<mutex> ts(m_mutex);
    destroyResource(m_pooledBuffers, bufferId);
}
void* Dx11Renderer::mapBuffer(BufferHandle bufferId, const MappingFlag type)
{
    debug_assert(bufferId);

    BufferImpl& impl = bufferId.get<BufferImpl>();

    D3D11_MAPPED_SUBRESOURCE data;
    m_context->Map(impl.m_buffer, 0, dx11MapFlag(type), 0, &data);
    return data.pData;
}
void Dx11Renderer::unmapBuffer(BufferHandle bufferId)
{
    debug_assert(bufferId);

    BufferImpl& buffer = bufferId.get<BufferImpl>();
    m_context->Unmap(buffer.m_buffer, 0);
}
void Dx11Renderer::writeBuffer(BufferHandle bufferId, const uint offset, const uint size,
                               const void* data)
{
    BufferImpl& impl = bufferId.get<BufferImpl>();
    if (impl.type != BufferType::Uniform)
    {
        debug_assert(offset + size <= impl.size, "invalid buffer range");
        D3D11_BOX box;
        box.left = offset;
        box.top = 0;
        box.front = 0;
        box.right = offset + size;
        box.bottom = 1;
        box.back = 1;
        m_context->UpdateSubresource(impl.m_buffer, 0, &box, data, size, 0);
    }
    else
    {
        debug_assert(size >= impl.size && offset == 0, "uniform buffer shall be fully updated");
        m_context->UpdateSubresource(impl.m_buffer, 0, nullptr, data, size, 0);
    }
}


// Binding
namespace
{
    static DXGI_FORMAT dx11Format(const Format format)
    {
        switch (format)
        {
        case Format::Unknown: return DXGI_FORMAT_UNKNOWN;
        case Format::D16_UNORM: return DXGI_FORMAT_R16_TYPELESS;
        case Format::D24_UNORM_S8_UINT: return DXGI_FORMAT_R24G8_TYPELESS;
        case Format::D32_FLOAT: return DXGI_FORMAT_R32_TYPELESS;

        case Format::B8G8R8A8_UNORM: return DXGI_FORMAT_B8G8R8A8_UNORM;
        case Format::R10G10B10A2_UNORM: return DXGI_FORMAT_R10G10B10A2_UNORM;
        case Format::R11G11B10_FLOAT: return DXGI_FORMAT_R11G11B10_FLOAT;

        case Format::R8_UINT: return DXGI_FORMAT_R8_UINT;
        case Format::R8G8_UINT: return DXGI_FORMAT_R8G8_UINT;
        case Format::R8G8B8A8_UINT: return DXGI_FORMAT_R8G8B8A8_UINT;

        case Format::R8_INT: return DXGI_FORMAT_R8_SINT;
        case Format::R8G8_INT: return DXGI_FORMAT_R8G8_SINT;
        case Format::R8G8B8A8_INT: return DXGI_FORMAT_R8G8B8A8_SINT;

        case Format::R8_UNORM: return DXGI_FORMAT_R8_UNORM;
        case Format::R8G8_UNORM: return DXGI_FORMAT_R8G8_UNORM;
        case Format::R8G8B8A8_UNORM: return DXGI_FORMAT_R8G8B8A8_UNORM;

        case Format::R8_SNORM: return DXGI_FORMAT_R8_SNORM;
        case Format::R8G8_SNORM: return DXGI_FORMAT_R8G8_SNORM;
        case Format::R8G8B8A8_SNORM: return DXGI_FORMAT_R8G8B8A8_SNORM;

        case Format::R16_UINT: return DXGI_FORMAT_R16_UINT;
        case Format::R16G16_UINT: return DXGI_FORMAT_R16G16_UINT;
        case Format::R16G16B16A16_UINT: return DXGI_FORMAT_R16G16B16A16_UINT;

        case Format::R16_INT: return DXGI_FORMAT_R16_SINT;
        case Format::R16G16_INT: return DXGI_FORMAT_R16G16_SINT;
        case Format::R16G16B16A16_INT: return DXGI_FORMAT_R16G16B16A16_SINT;

        case Format::R16_UNORM: return DXGI_FORMAT_R16_UNORM;
        case Format::R16G16_UNORM: return DXGI_FORMAT_R16G16_UNORM;
        case Format::R16G16B16A16_UNORM: return DXGI_FORMAT_R16G16B16A16_UNORM;

        case Format::R16_SNORM: return DXGI_FORMAT_R16_SNORM;
        case Format::R16G16_SNORM: return DXGI_FORMAT_R16G16_SNORM;
        case Format::R16G16B16A16_SNORM: return DXGI_FORMAT_R16G16B16A16_SNORM;

        case Format::R16_FLOAT: return DXGI_FORMAT_R16_FLOAT;
        case Format::R16G16_FLOAT: return DXGI_FORMAT_R16G16_FLOAT;
        case Format::R16G16B16A16_FLOAT: return DXGI_FORMAT_R16G16B16A16_FLOAT;

        case Format::R32_UINT: return DXGI_FORMAT_R32_UINT;
        case Format::R32G32_UINT: return DXGI_FORMAT_R32G32_UINT;
        case Format::R32G32B32_UINT: return DXGI_FORMAT_R32G32B32_UINT;
        case Format::R32G32B32A32_UINT: return DXGI_FORMAT_R32G32B32A32_UINT;

        case Format::R32_INT: return DXGI_FORMAT_R32_SINT;
        case Format::R32G32_INT: return DXGI_FORMAT_R32G32_SINT;
        case Format::R32G32B32_INT: return DXGI_FORMAT_R32G32B32_SINT;
        case Format::R32G32B32A32_INT: return DXGI_FORMAT_R32G32B32A32_SINT;

        case Format::R32_FLOAT: return DXGI_FORMAT_R32_FLOAT;
        case Format::R32G32_FLOAT: return DXGI_FORMAT_R32G32_FLOAT;
        case Format::R32G32B32_FLOAT: return DXGI_FORMAT_R32G32B32_FLOAT;
        case Format::R32G32B32A32_FLOAT: return DXGI_FORMAT_R32G32B32A32_FLOAT;

        case Format::BC1: return DXGI_FORMAT_BC1_UNORM;
        case Format::BC2: return DXGI_FORMAT_BC2_UNORM;
        case Format::BC3: return DXGI_FORMAT_BC3_UNORM;

        default:
            return (DXGI_FORMAT) 0;
        }
    }
    static DXGI_FORMAT dx11IndexType(const IndexFormat type)
    {
        switch (type)
        {
        case IndexFormat::Ubyte: return DXGI_FORMAT_R8_UINT;
        case IndexFormat::Ushort: return DXGI_FORMAT_R16_UINT;
        case IndexFormat::Uint: return DXGI_FORMAT_R32_UINT;
        default: return DXGI_FORMAT_UNKNOWN;
        }
    }
}
LayoutHandle Dx11Renderer::createLayout(ProgramHandle programId,
                                        const uint strides[], const uint numStreams,
                                        const VertexFormat format[], const uint formatSize)
{
    debug_assert(programId);
    debug_assert(numStreams < MaxStreams);
    debug_assert(formatSize < MaxVertexLayoutSize);

    ProgramImpl& program = programId.get<ProgramImpl>();
    EffectImpl& effect = *program.m_effect;

    sint vertexShaderIndex = effect.programs[program.index].vertex;
    debug_assert(vertexShaderIndex >= 0);
    const uint vertexShaderSize = effect.shadersSizes[(uint) vertexShaderIndex];
    const uint vertexShaderOffset = effect.shadersOffsets[(uint) vertexShaderIndex];
    const ubyte* vertexShaderData = effect.shadersData.data() + vertexShaderOffset;

    LayoutImpl impl;
    impl.numStreams = numStreams;
    impl.numElements = formatSize;
    copy(strides, strides + numStreams, impl.strides.begin());
    copy(format, format + formatSize, impl.elements.begin());

    D3D11_INPUT_ELEMENT_DESC elementsDesc[MaxVertexLayoutSize];
    for (uint index = 0; index < formatSize; ++index)
    {
        const VertexFormat& src = format[index];
        D3D11_INPUT_ELEMENT_DESC& dest = elementsDesc[index];

        dest.SemanticName = src.name.c_str();
        dest.SemanticIndex = 0;
        dest.Format = dx11Format(src.type);
        dest.InputSlot = src.stream;
        dest.AlignedByteOffset = src.offset;
        dest.InputSlotClass = !src.isInstanced
            ? D3D11_INPUT_PER_VERTEX_DATA
            : D3D11_INPUT_PER_INSTANCE_DATA;
        dest.InstanceDataStepRate = src.isInstanced ? 1 : 0;
    }
    const HRESULT result = m_device->CreateInputLayout(elementsDesc, formatSize,
                                                       vertexShaderData, vertexShaderSize,
                                                       &impl.m_layout);
    if (FAILED(result))
        return nullptr;

    return m_pooledLayouts.construct(impl);
}
void Dx11Renderer::destroyLayout(LayoutHandle layoutId)
{
    destroyResource(m_pooledLayouts, layoutId);
}


// Texture 2D
namespace
{
    static UINT dx11BindFlags(const FlagSet<ResourceBinding> flags)
    {
        UINT bindFlags = 0;
        if (flags.is(ResourceBinding::ShaderResource))
            bindFlags |= D3D11_BIND_SHADER_RESOURCE;
        if (flags.is(ResourceBinding::RenderTarget))
            bindFlags |= D3D11_BIND_RENDER_TARGET;
        if (flags.is(ResourceBinding::DepthStencil))
            bindFlags |= D3D11_BIND_DEPTH_STENCIL;
        return bindFlags;
    }
    static DXGI_FORMAT dx11FixFormatSRV(const DXGI_FORMAT format)
    {
        switch (format)
        {
        case DXGI_FORMAT_R32_TYPELESS:
            return DXGI_FORMAT_R32_FLOAT;
        case DXGI_FORMAT_R24G8_TYPELESS:
            return DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
        case DXGI_FORMAT_R16_TYPELESS:
            return DXGI_FORMAT_R16_UNORM;
        default:
            return format;
        }
    }
    static DXGI_FORMAT dx11FixFormatRTV(const DXGI_FORMAT format)
    {
        switch (format)
        {
        case DXGI_FORMAT_R32_TYPELESS:
        case DXGI_FORMAT_R24G8_TYPELESS:
        case DXGI_FORMAT_R16_TYPELESS:
            return DXGI_FORMAT_UNKNOWN;
        default:
            return format;
        }
    }
    static DXGI_FORMAT dx11FixFormatDSV(const DXGI_FORMAT format)
    {
        switch (format)
        {
        case DXGI_FORMAT_R32_TYPELESS:
            return DXGI_FORMAT_D32_FLOAT;
        case DXGI_FORMAT_R24G8_TYPELESS:
            return DXGI_FORMAT_D24_UNORM_S8_UINT;
        case DXGI_FORMAT_R16_TYPELESS:
            return DXGI_FORMAT_D16_UNORM;
        default:
            return format;
        }
    }
}
Texture2DHandle Dx11Renderer::createTexture2D(const uint numArraySlices, const uint numMipLevels,
                                              const uint width, const uint height,
                                              Format format, DXGI_FORMAT dxgiFormat,
                                              const ResourceUsage usage,
                                              const FlagSet<ResourceBinding> binding,
                                              D3D11_SUBRESOURCE_DATA data[])
{
    Texture2DImpl impl;
    Disposer<Texture2DImpl> guard(impl);
    impl.numSlices = numArraySlices;
    impl.numMips = numMipLevels;
    impl.width = width;
    impl.height = height;
    impl.format = format;
    impl.m_sampler = m_defaultSampler;

    // Texture desc
    CD3D11_TEXTURE2D_DESC desc(dxgiFormat, width, height,
                               numArraySlices, numMipLevels);
    desc.Usage = dx11Usage(usage);
    desc.BindFlags = dx11BindFlags(binding);
    if (usage == ResourceUsage::Dynamic)
        desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    if (binding.is(ResourceBinding::ShaderResource)
        && binding.is(ResourceBinding::RenderTarget))
        desc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;

    // Texture data
    HRESULT result = m_device->CreateTexture2D(&desc, data,
                                               &impl.m_texture);
    if (FAILED(result))
        return nullptr;

    // Srv
    if (binding.is(ResourceBinding::ShaderResource))
    {
        D3D11_SHADER_RESOURCE_VIEW_DESC srvdesc;
        srvdesc.Format = dx11FixFormatSRV(desc.Format);
        if (numArraySlices > 1)
        {
            srvdesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
            srvdesc.Texture2DArray.ArraySize = numArraySlices;
            srvdesc.Texture2DArray.FirstArraySlice = 0;
            srvdesc.Texture2DArray.MipLevels = numMipLevels;
            srvdesc.Texture2DArray.MostDetailedMip = 0;
        }
        else
        {
            srvdesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
            srvdesc.Texture2D.MipLevels = numMipLevels;
            srvdesc.Texture2D.MostDetailedMip = 0;
        }
        result = m_device->CreateShaderResourceView(impl.m_texture, &srvdesc,
                                                    &impl.m_srv);
        if (FAILED(result))
            return nullptr;
    }

    // Rtv
    if (binding.is(ResourceBinding::RenderTarget))
    {
        D3D11_RENDER_TARGET_VIEW_DESC rtvdesc;
        rtvdesc.Format = dx11FixFormatRTV(desc.Format);
        if (numArraySlices > 1)
        {
            rtvdesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DARRAY;
            rtvdesc.Texture2DArray.ArraySize = numArraySlices;
            rtvdesc.Texture2DArray.FirstArraySlice = 0;
            rtvdesc.Texture2DArray.MipSlice = 0;
        }
        else
        {
            rtvdesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
            rtvdesc.Texture2D.MipSlice = 0;
        }
        result = m_device->CreateRenderTargetView(impl.m_texture, &rtvdesc,
                                                  &impl.m_rtv);
        if (FAILED(result))
            return nullptr;
    }

    // Dsv
    if (binding.is(ResourceBinding::DepthStencil))
    {
        D3D11_DEPTH_STENCIL_VIEW_DESC dsvdesc;
        dsvdesc.Format = dx11FixFormatDSV(desc.Format);
        dsvdesc.Flags = 0;
        if (numArraySlices > 1)
        {
            dsvdesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DARRAY;
            dsvdesc.Texture2DArray.ArraySize = numArraySlices;
            dsvdesc.Texture2DArray.FirstArraySlice = 0;
            dsvdesc.Texture2DArray.MipSlice = 0;
        }
        else
        {
            dsvdesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
            dsvdesc.Texture2D.MipSlice = 0;
        }
        result = m_device->CreateDepthStencilView(impl.m_texture, &dsvdesc,
                                                  &impl.m_dsv);
        if (FAILED(result))
            return nullptr;
    }

    // Reader
    if (binding.is(ResourceBinding::Readable))
    {
        desc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
        desc.Usage = D3D11_USAGE_STAGING;
        desc.BindFlags = 0;
        desc.MipLevels = 1;
        desc.ArraySize = 1;
        desc.MiscFlags = 0;

        result = m_device->CreateTexture2D(&desc, data,
                                           &impl.m_reader);
        if (FAILED(result))
            return nullptr;
    }

    guard.deactivate();
    lock_guard<mutex> ts(m_mutex);
    m_defaultSampler->AddRef();
    return m_pooledTextures2D.construct(impl);
}
Texture2DHandle Dx11Renderer::createTexture2D(const uint numArraySlices, const uint numMipLevels,
                                              const uint width, const uint height,
                                              const Format format,
                                              const ResourceUsage usage,
                                              const FlagSet<ResourceBinding> binding,
                                              const void* const data[])
{
    vector<D3D11_SUBRESOURCE_DATA> src;
    if (data)
    {
        src.resize(numArraySlices * numMipLevels);
        for (uint sliceIndex = 0; sliceIndex < numArraySlices; ++sliceIndex)
        {
            for (uint minIndex = 0; minIndex < numMipLevels; ++minIndex)
            {
                const uint idx = sliceIndex * numMipLevels + minIndex;
                const uint mipWidth = max<uint>(1, width >> minIndex);
                const uint mipHeight = max<uint>(1, height >> minIndex);
                src[idx].pSysMem = data[idx];
                formatPitch(format, mipWidth, mipHeight,
                            src[idx].SysMemPitch, src[idx].SysMemSlicePitch);
            }
        }
    }
    return createTexture2D(numArraySlices, numMipLevels, width, height,
                           format, dx11Format(format), usage, binding, 
                           data ? src.data() : nullptr);
}
void Dx11Renderer::destroyTexture2D(Texture2DHandle textureId)
{
    lock_guard<mutex> ts(m_mutex);
    destroyResource(m_pooledTextures2D, textureId);
}
Texture2DHandle Dx11Renderer::loadTexture2D(const TextureStorage& source,
                                            const FlagSet<ResourceBinding> binding)
{
    const Format format = gliUnformat(source.format());
    const TextureStorage::texelcoord_type imageSize = source.dimensions();

    // Init data
    vector<D3D11_SUBRESOURCE_DATA> initial(source.levels() * source.layers());
    for (uint layer = 0; layer < source.layers(); ++layer)
        for (uint level = 0; level < source.levels(); ++level)
        {
            const uint index = layer * source.levels() + level;
            auto subimage = source[layer][level];
            glm::ivec3 subimageSize = subimage.dimensions();

            initial[index].pSysMem = subimage.data();
            formatPitch(format, subimageSize.x, subimageSize.y,
                        initial[index].SysMemPitch, initial[index].SysMemSlicePitch);
        }

    // Create
    return createTexture2D(source.layers(), source.levels(), imageSize.x, imageSize.y,
                           format, static_cast<DXGI_FORMAT>(gli::dx().translate(source.format()).DXGIFormat.DDS),
                           ResourceUsage::Constant, binding, initial.data());
}
void Dx11Renderer::writeTexture2D(Texture2DHandle textureId,
                                  const uint arraySlice, const uint mipLevel,
                                  const uint offsetX, const uint offsetY,
                                  const uint width, const uint height,
                                  const void* data)
{
    debug_assert(textureId);
    debug_assert(data);

    Texture2DImpl& impl = textureId.get<Texture2DImpl>();
    debug_assert(arraySlice < impl.numSlices);
    debug_assert(mipLevel < impl.numMips);
    debug_assert(offsetX + width <= (impl.width >> mipLevel));
    debug_assert(offsetY + height <= (impl.height >> mipLevel));

    const UINT subresource = D3D11CalcSubresource(mipLevel, arraySlice, impl.numMips);

    D3D11_BOX box;
    box.left = offsetX;
    box.top = offsetY;
    box.front = 0;
    box.right = offsetX + width;
    box.bottom = offsetY + height;
    box.back = 1;

    uint rowPitch, slicePitch;
    formatPitch(impl.format, width, height,
                rowPitch, slicePitch);
    m_context->UpdateSubresource(impl.m_texture, subresource, &box, 
                                 data, rowPitch, slicePitch);
}
void Dx11Renderer::readTexture2D(Texture2DHandle textureId, const uint arraySlice, const uint mipLevel,
                                 const uint x, const uint y,
                                 const uint width, const uint height,
                                 void* data)
{
    debug_assert(textureId);
    debug_assert(data);

    Texture2DImpl& impl = textureId.get<Texture2DImpl>();
    debug_assert(impl.m_reader);
    debug_assert(arraySlice < impl.numSlices);
    debug_assert(mipLevel < impl.numMips);
    debug_assert(x + width <= max<uint>(1, impl.width >> mipLevel));
    debug_assert(y + height <= max<uint>(1, impl.height >> mipLevel));

    // Copy to reader
    const UINT subresource = D3D11CalcSubresource(mipLevel, arraySlice, impl.numMips);
    D3D11_BOX box;
    box.left = x;
    box.top = y;
    box.front = 0;
    box.right = x + width;
    box.bottom = y + height;
    box.back = 1;
    m_context->CopySubresourceRegion(impl.m_reader, 0, 0, 0, 0,
                                     impl.m_texture, subresource, &box);

    // Map
    D3D11_MAPPED_SUBRESOURCE mapped;
    m_context->Map(impl.m_reader, 0, D3D11_MAP_READ, 0, &mapped);

    // Get dest pitch
    uint rowPitch, slicePitch;
    formatPitch(impl.format, width, height,
                rowPitch, slicePitch);
    debug_assert(rowPitch <= mapped.RowPitch);

    // Copy
    const ubyte* src = static_cast<const ubyte*>(mapped.pData);
    ubyte* dest = static_cast<ubyte*>(data);
    for (uint row = 0; row < height; ++row)
    {
        memcpy(dest, src, rowPitch);
        src += mapped.RowPitch;
        dest += rowPitch;
    }

    // Unmap
    m_context->Unmap(impl.m_reader, 0);
}
void Dx11Renderer::copyTexture2D(Texture2DHandle destId, Texture2DHandle sourceId,
                                 const uint destSlice, const uint sourceSlice,
                                 const uint destMip, const uint sourceMip,
                                 const uint destX, const uint destY,
                                 const uint sourceX, const uint sourceY,
                                 const uint width, const uint height)
{
    debug_assert(destId);
    debug_assert(sourceId);

    Texture2DImpl& dest = destId.get<Texture2DImpl>();
    Texture2DImpl& source = sourceId.get<Texture2DImpl>();

    debug_assert(destSlice < dest.numSlices);
    debug_assert(destX + width <= max<uint>(1, dest.width >> destMip));
    debug_assert(destY + height <= max<uint>(1, dest.height >> destMip));
    debug_assert(sourceSlice < source.numSlices);
    debug_assert(sourceX + width <= max<uint>(1, source.width >> sourceMip));
    debug_assert(sourceY + height <= max<uint>(1, source.height >> sourceMip));

    const UINT destSubresource = D3D11CalcSubresource(destMip, destSlice, dest.numMips);
    const UINT srcSubresource = D3D11CalcSubresource(sourceMip, sourceSlice, source.numMips);
    D3D11_BOX box;
    box.left = sourceX;
    box.top = sourceY;
    box.front = 0;
    box.right = sourceX + width;
    box.bottom = sourceY + height;
    box.back = 1;
    m_context->CopySubresourceRegion(dest.m_texture, destSubresource, destX, destY, 0,
                                     source.m_texture, srcSubresource, &box);
}
void Dx11Renderer::generateMipmaps(Texture2DHandle textureId)
{
    debug_assert(textureId);
    Texture2DImpl& texture = textureId.get<Texture2DImpl>();
    debug_assert(texture.m_srv && texture.m_rtv);

    m_context->GenerateMips(texture.m_srv);
}


// Sampler
namespace
{
    static D3D11_FILTER dx11Filter(const Filter filter, const bool shadow)
    {
        if (!shadow)
        {
            switch (filter)
            {
            case Filter::Point: return D3D11_FILTER_MIN_MAG_MIP_POINT;
            case Filter::PointLinear: return D3D11_FILTER_MIN_MAG_POINT_MIP_LINEAR;
            case Filter::LinearPoint: return D3D11_FILTER_MIN_MAG_LINEAR_MIP_POINT;
            case Filter::Linear: return D3D11_FILTER_MIN_MAG_MIP_LINEAR;
            case Filter::Anisotropic: return D3D11_FILTER_ANISOTROPIC;
            default: return D3D11_FILTER_MIN_MAG_MIP_POINT;
            }
        }
        else
        {
            switch (filter)
            {
            case Filter::Point: return D3D11_FILTER_COMPARISON_MIN_MAG_MIP_POINT;
            case Filter::PointLinear: return D3D11_FILTER_COMPARISON_MIN_MAG_POINT_MIP_LINEAR;
            case Filter::LinearPoint: return D3D11_FILTER_COMPARISON_MIN_MAG_LINEAR_MIP_POINT;
            case Filter::Linear: return D3D11_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR;
            case Filter::Anisotropic: return D3D11_FILTER_COMPARISON_ANISOTROPIC;
            default: return D3D11_FILTER_COMPARISON_MIN_MAG_MIP_POINT;
            }
        }
    }
    static D3D11_TEXTURE_ADDRESS_MODE dx11Addressing(const Addressing addressing)
    {
        switch (addressing)
        {
        case Addressing::Wrap: return D3D11_TEXTURE_ADDRESS_WRAP;
        case Addressing::Mirror: return D3D11_TEXTURE_ADDRESS_MIRROR;
        case Addressing::Clamp: return D3D11_TEXTURE_ADDRESS_CLAMP;
        case Addressing::MirrorOnce: return D3D11_TEXTURE_ADDRESS_MIRROR_ONCE;
        default: return D3D11_TEXTURE_ADDRESS_WRAP;
        }
    }
}
void Dx11Renderer::setTexture2DSampler(Texture2DHandle textureId,
                                       const Filter filter, const Addressing addressing,
                                       const uint anisotropyLevel,
                                       const float minLod, const float maxLod, const float lodBias,
                                       const bool shadow, const Comparison comparison)
{
    debug_assert(textureId);

    D3D11_SAMPLER_DESC desc;
    desc.Filter = dx11Filter(filter, shadow);
    desc.AddressU = desc.AddressV = desc.AddressW = dx11Addressing(addressing);
    desc.MipLODBias = lodBias;
    desc.MaxAnisotropy = anisotropyLevel;
    desc.ComparisonFunc = dx11Comparison(comparison);
    desc.BorderColor[0] = desc.BorderColor[1] = desc.BorderColor[2] = desc.BorderColor[3] = 0.0f;
    desc.MinLOD = minLod;
    desc.MaxLOD = maxLod;

    Texture2DImpl& texture = textureId.get<Texture2DImpl>();
    safeRelease(texture.m_sampler);
    HRESULT result = m_device->CreateSamplerState(&desc, &texture.m_sampler);
    if (FAILED(result))
        return;
}


// Render Target
RenderTargetHandle Dx11Renderer::createRenderTarget(const uint numColorChannels,
                                                    Texture2DHandle depthTextureId)
{
    RenderTargetImpl impl;
    if (!!depthTextureId)
    {
        Texture2DImpl& depthTexture = depthTextureId.get<Texture2DImpl>();
        debug_assert(depthTexture.m_dsv);
        impl.depthTexture = depthTextureId;
        impl.m_dsv = depthTexture.m_dsv;
        impl.implResize(depthTexture.width, depthTexture.height);
    }
    impl.numColorChannels = numColorChannels;
    fill_n(impl.m_rtvs.begin(), MaxRenderTargets, nullptr);

    return m_pooledRenderTargets.construct(impl);
}
void Dx11Renderer::attachColorTexture2D(RenderTargetHandle renderTargetId,
                                        const uint target, Texture2DHandle textureId)
{
    debug_assert(renderTargetId);
    debug_assert(textureId);
    RenderTargetImpl& renderTarget = renderTargetId.get<RenderTargetImpl>();
    debug_assert(target < renderTarget.numColorChannels);
    debug_assert(!renderTarget.colorTextures[target]);

    Texture2DImpl& texture = textureId.get<Texture2DImpl>();

    renderTarget.colorTextures[target] = textureId;
    renderTarget.m_rtvs[target] = texture.m_rtv;

    ++renderTarget.numAttached;
    const bool success = renderTarget.implResize(texture.width, texture.height);
    debug_assert(success);
}
void Dx11Renderer::detachColorTarget(RenderTargetHandle renderTargetId, const uint target)
{
    debug_assert(renderTargetId);
    RenderTargetImpl& renderTarget = renderTargetId.get<RenderTargetImpl>();
    debug_assert(target < renderTarget.numColorChannels);
    debug_assert(!!renderTarget.colorTextures[target]);

    renderTarget.colorTextures[target] = nullptr;
    renderTarget.m_rtvs[target] = nullptr;

    --renderTarget.numAttached;
    renderTarget.implDrop();
}
void Dx11Renderer::destroyRenderTarget(RenderTargetHandle renderTargetId)
{
    destroyResource(m_pooledRenderTargets, renderTargetId);
}


// Bind states
void Dx11Renderer::bindRenderTargetRegion(RenderTargetHandle renderTargetId,
                                          const uint x, const uint y,
                                          const uint w, const uint h)
{
    if (!!renderTargetId)
    {
        RenderTargetImpl& renderTarget = renderTargetId.get<RenderTargetImpl>();

        D3D11_VIEWPORT vp;
        vp.TopLeftX = static_cast<float>(x);
        vp.TopLeftY = static_cast<float>(y);
        vp.Width = static_cast<float>(w ? w : renderTarget.width);
        vp.Height = static_cast<float>(h ? h : renderTarget.height);
        vp.MaxDepth = 1.0f;
        vp.MinDepth = 0.0f;
        m_context->RSSetViewports(1, &vp);

        m_context->OMSetRenderTargets(MaxRenderTargets,
                                      renderTarget.m_rtvs.data(),
                                      renderTarget.m_dsv);
    }
    else
    {
        D3D11_VIEWPORT vp;
        vp.TopLeftX = static_cast<float>(x);
        vp.TopLeftY = static_cast<float>(y);
        vp.Width = static_cast<float>(w ? w : m_width);
        vp.Height = static_cast<float>(h ? h : m_height);
        vp.MaxDepth = 1.0f;
        vp.MinDepth = 0.0f;
        m_context->RSSetViewports(1, &vp);

        m_context->OMSetRenderTargets(1,
                                      &m_defaultRenderTarget,
                                      nullptr);
    }
}
void Dx11Renderer::clearColor(RenderTargetHandle renderTargetId, const uint target, const float4& color)
{
    if (!!renderTargetId)
    {
        RenderTargetImpl& renderTarget = renderTargetId.get<RenderTargetImpl>();
        m_context->ClearRenderTargetView(renderTarget.m_rtvs[target], color.value());
    }
    else
    {
        m_context->ClearRenderTargetView(m_defaultRenderTarget, color.value());
    }
}
void Dx11Renderer::clearDepthStencil(RenderTargetHandle renderTargetId, const float depth, const ubyte stencil)
{
    if (!!renderTargetId)
    {
        RenderTargetImpl& renderTarget = renderTargetId.get<RenderTargetImpl>();
        m_context->ClearDepthStencilView(renderTarget.m_dsv, 
                                         D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL,
                                         depth, stencil);
    }
}
void Dx11Renderer::bindGeometry(const uint numVertexBuffers, const BufferHandle vertexBuffers[],
                                BufferHandle indexBuffer, IndexFormat indexFormat)
{
    debug_assert(m_currentLayout);
    debug_assert(numVertexBuffers < MaxStreams);

    // Fill array
    uint beginSlot = 0, endSlot = 0;
    for (uint bufferIndex = 0; bufferIndex < numVertexBuffers; ++bufferIndex)
    {
        ID3D11Buffer* vb = !vertexBuffers[bufferIndex]
            ? nullptr
            : vertexBuffers[bufferIndex].get<BufferImpl>().m_buffer;

        // Skip same
        if (endSlot == 0
            && m_currentStrides == m_boundStrides
            && m_currentVertexBuffers[bufferIndex] == vb)
            continue;

        // Set
        m_currentVertexBuffers[bufferIndex] = vb;
        if (endSlot == 0)
            beginSlot = bufferIndex;
        endSlot = bufferIndex + 1;
    }

    // Set vbs
    static const uint offsets[MaxStreams] = { 0 };
    if (beginSlot != endSlot)
    {
        m_context->IASetVertexBuffers(beginSlot, endSlot - beginSlot,
                                      m_currentVertexBuffers.data() + beginSlot,
                                      m_currentStrides + beginSlot,
                                      offsets + beginSlot);
        m_boundStrides = m_currentStrides;
    }

    // Set ibs
    ID3D11Buffer* ib = !indexBuffer ? nullptr : indexBuffer.get<BufferImpl>().m_buffer;
    if (m_currentIndexBuffer != ib)
    {
        m_context->IASetIndexBuffer(ib, dx11IndexType(indexFormat), 0);
        m_currentIndexBuffer = ib;
    }
}


// Bind effect
void Dx11Renderer::setResource(const uint slot, const ShaderType shader,
                               ID3D11ShaderResourceView* resource, ID3D11SamplerState* sampler)
{
    switch (shader)
    {
    case ShaderType::Vertex:
        m_context->VSSetShaderResources(slot, 1, &resource);
        if (sampler)
            m_context->VSSetSamplers(slot, 1, &sampler);
        break;
    case ShaderType::TessControl:
        m_context->HSSetShaderResources(slot, 1, &resource);
        if (sampler)
            m_context->HSSetSamplers(slot, 1, &sampler);
        break;
    case ShaderType::TessEval:
        m_context->DSSetShaderResources(slot, 1, &resource);
        if (sampler)
            m_context->DSSetSamplers(slot, 1, &sampler);
        break;
    case ShaderType::Geometry:
        m_context->GSSetShaderResources(slot, 1, &resource);
        if (sampler)
            m_context->GSSetSamplers(slot, 1, &sampler);
        break;
    case ShaderType::Pixel:
        m_context->PSSetShaderResources(slot, 1, &resource);
        if (sampler)
            m_context->PSSetSamplers(slot, 1, &sampler);
        break;
    default:
        break;
    }
}
void Dx11Renderer::bindState(StateHandle stateId, const ubyte stencilRef, const float4& blendColor)
{
    debug_assert(stateId);
    StateImpl& state = stateId.get<StateImpl>();

    // Set states
    if (m_currentBlendState != state.m_bs || m_currentBlendColor != blendColor)
    {
        m_context->OMSetBlendState(state.m_bs, blendColor.value(), 0xffffffff);
        m_currentBlendState = state.m_bs;
        m_currentBlendColor = blendColor;
    }
    if (m_currentDepthStencilState != state.m_dss || m_currentStencilRef != stencilRef)
    {
        m_context->OMSetDepthStencilState(state.m_dss, stencilRef);
        m_currentDepthStencilState = state.m_dss;
        m_currentStencilRef = stencilRef;
    }
    if (m_currentRasterizerState != state.m_rs)
    {
        m_context->RSSetState(state.m_rs);
        m_currentRasterizerState = state.m_rs;
    }

    // Set effect
    ProgramImpl& program = state.program.get<ProgramImpl>();
    EffectImpl& effect = *program.m_effect;
    if (m_currentEffect != &effect)
    {
        m_context->VSSetConstantBuffers(0, MaxShaderUniformBuffers, state.m_uniformBuffers);
        m_context->HSSetConstantBuffers(0, MaxShaderUniformBuffers, state.m_uniformBuffers);
        m_context->DSSetConstantBuffers(0, MaxShaderUniformBuffers, state.m_uniformBuffers);
        m_context->GSSetConstantBuffers(0, MaxShaderUniformBuffers, state.m_uniformBuffers);
        m_context->PSSetConstantBuffers(0, MaxShaderUniformBuffers, state.m_uniformBuffers);
        m_currentEffect = &effect;
    }

    // Set program
    if (m_currentProgram != &program)
    {
        m_context->VSSetShader(state.m_vertexShader, nullptr, 0);
        m_context->HSSetShader(state.m_hullShader, nullptr, 0);
        m_context->DSSetShader(state.m_domainShader, nullptr, 0);
        m_context->GSSetShader(state.m_geometryShader, nullptr, 0);
        m_context->PSSetShader(state.m_pixelShader, nullptr, 0);

        m_currentProgram = &program;
    }

    // Set topology
    if (m_currentTopology != state.m_topology)
    {
        m_context->IASetPrimitiveTopology(state.m_topology);
        m_currentTopology = state.m_topology;
        m_currentPrimitiveType = (uint) state.primitive;
    }

    // Set layout
    if (m_currentLayout != state.m_layout)
    {
        m_context->IASetInputLayout(state.m_layout);
        m_currentLayout = state.m_layout;
        m_currentStrides = state.m_strides;
    }
}
bool Dx11Renderer::bindTexture2D(EffectResourceHandle resourceId,
                                 Texture2DHandle textureId,
                                 const ShaderType shader)
{
    if (!resourceId)
        return false;
    EffectResourceImpl& resource = resourceId.get<EffectResourceImpl>();
    debug_assert(resource.effect == m_currentEffect);

    m_currentBucket.reset();
    m_currentBucket.addResource(resource.slot, textureId, shader);

    if (!!textureId)
    {
        Texture2DImpl& texture = textureId.get<Texture2DImpl>();
        debug_assert(texture.m_srv);
        setResource(resource.slot, shader, 
                    texture.m_srv, texture.m_sampler);
    }
    else
    {
        setResource(resource.slot, shader,
                    nullptr, nullptr);
    }
    return true;
}
void Dx11Renderer::bindResources(const ResourceBucket& bucket)
{
    // Buffers
    static const uint NumShaders = (uint) ShaderType::COUNT;
    ID3D11ShaderResourceView* srvs[NumShaders][MaxShaderResources] = { nullptr };
    ID3D11SamplerState* samplers[NumShaders][MaxShaderResources] = { nullptr };
    uint beginSlot[NumShaders] = { 0 };
    uint endSlot[NumShaders] = { 0 };

    // Unbind all
    if (bucket.numResources == 0)
    {
        m_context->VSSetSamplers(0, MaxShaderResources, &samplers[0][0]);
        m_context->VSSetShaderResources(0, MaxShaderResources, &srvs[0][0]);
        m_context->HSSetSamplers(0, MaxShaderResources, &samplers[0][0]);
        m_context->HSSetShaderResources(0, MaxShaderResources, &srvs[0][0]);
        m_context->DSSetSamplers(0, MaxShaderResources, &samplers[0][0]);
        m_context->DSSetShaderResources(0, MaxShaderResources, &srvs[0][0]);
        m_context->GSSetSamplers(0, MaxShaderResources, &samplers[0][0]);
        m_context->GSSetShaderResources(0, MaxShaderResources, &srvs[0][0]);
        m_context->PSSetSamplers(0, MaxShaderResources, &samplers[0][0]);
        m_context->PSSetShaderResources(0, MaxShaderResources, &srvs[0][0]);
        return;
    }

    // Init
    for (uint resourceIndex = 0; resourceIndex < bucket.numResources; ++resourceIndex)
    {
        ShaderType shaderType = bucket.shaders[resourceIndex];
        const uint shaderIndex = (uint) shaderType;
        Texture2DHandle resource = bucket.resources[resourceIndex];
        const uint slot = bucket.slots[resourceIndex];

        // Skip
        if (endSlot[shaderIndex] == 0
            && resourceIndex < m_currentBucket.numResources
            && m_currentBucket.resources[resourceIndex] == resource
            && m_currentBucket.slots[resourceIndex] == slot
            && m_currentBucket.shaders[resourceIndex] == shaderType)
            continue;

        // Set resource
        if (!resource)
        {
            srvs[shaderIndex][slot] = nullptr;
            samplers[shaderIndex][slot] = nullptr;
        }
        else
        {
            Texture2DImpl& impl = resource.get<Texture2DImpl>();
            srvs[shaderIndex][slot] = impl.m_srv;
            samplers[shaderIndex][slot] = impl.m_sampler;
        }

        // Update slots
        if (endSlot[shaderIndex] == 0)
            beginSlot[shaderIndex] = slot;
        endSlot[shaderIndex] = slot + 1;
    }

    // Update device
    m_currentBucket = bucket;
#define SET_RESOURCES(S, SHADER)                                                \
    if (endSlot[(uint) ShaderType::SHADER] != 0)                                \
    {                                                                           \
        const uint shader = (uint) ShaderType::SHADER;                          \
        const uint startSlot = beginSlot[shader];                               \
        const uint numSlots = endSlot[shader] - startSlot;                      \
        m_context->S##SSetSamplers(startSlot, numSlots, &samplers[shader][startSlot]);      \
        m_context->S##SSetShaderResources(startSlot, numSlots, &srvs[shader][startSlot]);   \
    }

    SET_RESOURCES(V, Vertex);
    SET_RESOURCES(H, TessControl);
    SET_RESOURCES(D, TessEval);
    SET_RESOURCES(G, Geometry);
    SET_RESOURCES(P, Pixel);
#undef SET_RESOURCES
}


// Draw
void Dx11Renderer::draw(const uint startVertex, const uint numVertices)
{
    m_stats.numDrawnPrimitives[m_currentPrimitiveType] += numVertices;
    ++m_stats.numDrawCalls;
    m_context->Draw(numVertices, startVertex);
}
void Dx11Renderer::drawInstanced(const uint startVertex, const uint numVertices,
                                 const uint startInstance, const uint numInstances)
{
    m_stats.numDrawnPrimitives[m_currentPrimitiveType] += numVertices * numInstances;
    ++m_stats.numDrawCalls;
    m_context->DrawInstanced(numVertices, numInstances, startVertex, startInstance);
}
void Dx11Renderer::drawIndexed(const uint startIndex, const uint numIndices,
                               const uint baseVertex)
{
    m_stats.numDrawnPrimitives[m_currentPrimitiveType] += numIndices;
    ++m_stats.numDrawCalls;
    m_context->DrawIndexed(numIndices, startIndex, baseVertex);
}
void Dx11Renderer::drawIndexedInstanced(const uint startIndex, const uint numIndices,
                                        const uint baseVertex,
                                        const uint startInstance, const uint numInstances)
{
    m_stats.numDrawnPrimitives[m_currentPrimitiveType] += numIndices * numInstances;
    ++m_stats.numDrawCalls;
    m_context->DrawIndexedInstanced(numIndices, numInstances, startIndex, baseVertex, startInstance);
}


// Gets
usl::TargetLanguage Dx11Renderer::effectLanguage() const
{
    return usl::TargetLanguage::HLSL30;
}
#endif