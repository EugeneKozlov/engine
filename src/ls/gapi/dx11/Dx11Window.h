/// @file ls/gapi/dx11/Dx11Window.h
/// @brief DX11 window implemenation
#pragma once

#include "ls/common.h"

#if LS_COMPILE_DX11 != 0

#include "ls/gapi/WindowInterface.h"
#include "ls/gapi/dx11/Dx11Renderer.h"
#include "ls/app/common.h"
#include <glfw/glfw3.h>
#include <glfw/glfw3native.h>
#include <dx11.h>

namespace ls
{
    namespace gapi
    {
        /// @brief DX11 window implemenation
        class Dx11Window
            : public WindowInterface
        {
        public:
            /// @brief Ctor
            Dx11Window(GLFWwindow* holder, const uint width, const uint height);
            /// @brief Dtor
            virtual ~Dx11Window();
        public:
            virtual void swap() override;
            virtual void resize(const uint width, const uint height) override;
            virtual Renderer& renderer() override;
        private:
            void initWindow();
            void destroyWindow();

            void initDevice();
            void destroyDevice();

            void initRenderTarget();
        private:
            /// @brief GLFW holder
            GLFWwindow* m_holder;
            /// @brief Parent handle
            HWND m_parent;
            /// @brief Child HDC
            HDC m_hDC;
            /// @brief Child HWND
            HWND m_handle;
            bool m_locked = false;
            uint m_width = 0;
            uint m_height = 0;

            /// @brief Device
            ID3D11Device* m_device = nullptr;
            /// @brief Device context
            ID3D11DeviceContext* m_context = nullptr;
            /// @brief Feature level
            D3D_FEATURE_LEVEL m_featureLevel = (D3D_FEATURE_LEVEL) 0;
            /// @brief Swap chain
            IDXGISwapChain* m_swapChain = nullptr;
            /// @brief Render target
            ID3D11RenderTargetView* m_renderTarget = nullptr;

            /// @brief Renderer
            unique_ptr<Dx11Renderer> m_renderer = nullptr;
        };
    }
}

#endif