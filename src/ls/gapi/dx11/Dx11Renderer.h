/// @file ls/gapi/dx11/Dx11Renderer.h
/// @brief DX11 renderer implementation
#pragma once

#include "ls/common.h"
#include "ls/gapi/Renderer.h"
#include "ls/gapi/GapiAllocator.h"

#if LS_COMPILE_DX11 != 0
#include "ls/gapi/detail/TextureLoader.h"
#include <dx11.h>

namespace ls
{
    namespace gapi
    {
        /// @brief Safe release IUnknown
        template <class Resource>
        void safeRelease(Resource*& resource)
        {
            if (resource)
            {
                resource->Release();
                resource = nullptr;
            }
        }
        /// @brief DX11 renderer implementation
        class Dx11Renderer
            : public Renderer
        {
        public:
            /// @brief Ctor
            Dx11Renderer(ID3D11Device* device, ID3D11DeviceContext* context);
            /// @brief Dtor
            virtual ~Dx11Renderer();
            /// @brief Resize
            void setDefaultRenderTarget(ID3D11RenderTargetView* defaultRenderTarget)
            {
                m_defaultRenderTarget = defaultRenderTarget;
            }
        public:
            // Effect
            virtual Stream compileEffect(usl::Effect& source) override;
            virtual EffectHandle createEffect(StreamInterface& compiled) override;
            virtual void destroyEffect(EffectHandle effectId) override;
            virtual bool attachUniformBuffer(EffectHandle effectId,
                                             BufferHandle bufferId,
                                             const char* uniformBufferName) override;
            virtual ProgramHandle handleProgram(EffectHandle effectId,
                                                const char* programName) override;
            virtual EffectResourceHandle handleResource(EffectHandle effectId,
                                                        const char* resourceName) override;

            // State
            virtual StateHandle createState(const StateDesc& desc) override;
            virtual void destroyState(StateHandle stateId) override;

            // Buffer
            virtual BufferHandle createBuffer(const uint size, const BufferType type, const ResourceUsage usage,
                                              const void* data) override;
            virtual void destroyBuffer(BufferHandle bufferId) override;
            virtual void* mapBuffer(BufferHandle bufferId, const MappingFlag type) override;
            virtual void unmapBuffer(BufferHandle bufferId) override;
            virtual void writeBuffer(BufferHandle bufferId, const uint offset, const uint size,
                                     const void* data) override;

            // Layout
            virtual LayoutHandle createLayout(ProgramHandle programId,
                                              const uint strides[], const uint numStreams,
                                              const VertexFormat format[], const uint formatSize) override;
            virtual void destroyLayout(LayoutHandle layoutId) override;

            // Texture 2D
            virtual Texture2DHandle createTexture2D(const uint numArraySlices, const uint numMipLevels,
                                                    const uint width, const uint height,
                                                    const Format format,
                                                    const ResourceUsage usage,
                                                    const FlagSet<ResourceBinding> binding,
                                                    const void* const data[]) override;
            virtual void destroyTexture2D(Texture2DHandle textureId) override;
            virtual Texture2DHandle loadTexture2D(const TextureStorage& source,
                                                  const FlagSet<ResourceBinding> binding) override;
            virtual void writeTexture2D(Texture2DHandle textureId,
                                        const uint arraySlice, const uint mipLevel,
                                        const uint offsetX, const uint offsetY,
                                        const uint width, const uint height,
                                        const void* data) override;
            virtual void readTexture2D(Texture2DHandle textureId,
                                       const uint arraySlice, const uint mipLevel,
                                       const uint x, const uint y,
                                       const uint width, const uint height,
                                       void* data) override;
            virtual void copyTexture2D(Texture2DHandle destId, Texture2DHandle sourceId,
                                       const uint destSlice, const uint sourceSlice,
                                       const uint destMip, const uint sourceMip,
                                       const uint destX, const uint destY,
                                       const uint sourceX, const uint sourceY,
                                       const uint width, const uint height) override;
            virtual void generateMipmaps(Texture2DHandle textureId) override;

            virtual void setTexture2DSampler(Texture2DHandle textureId,
                                             const Filter filter, const Addressing addressing,
                                             const uint anisotropyLevel,
                                             const float minLod, const float maxLod, const float lodBias,
                                             const bool shadow, const Comparison comparison) override;

            virtual RenderTargetHandle createRenderTarget(const uint numColorChannels,
                                                          Texture2DHandle depthTextureId) override;
            virtual void attachColorTexture2D(RenderTargetHandle renderTargetId,
                                              const uint target, Texture2DHandle textureId) override;
            virtual void detachColorTarget(RenderTargetHandle renderTargetId, const uint target) override;
            virtual void destroyRenderTarget(RenderTargetHandle renderTargetId) override;

            virtual void bindRenderTargetRegion(RenderTargetHandle renderTargetId,
                                                const uint x, const uint y,
                                                const uint w, const uint h) override;
            virtual void clearColor(RenderTargetHandle renderTargetId,
                                    const uint target, const float4& color) override;
            virtual void clearDepthStencil(RenderTargetHandle renderTargetId,
                                           const float depth, const ubyte stencil) override;
            virtual void bindGeometry(const uint numVertexBuffers, const BufferHandle vertexBuffers[],
                                      BufferHandle indexBuffer, const IndexFormat indexFormat) override;
            virtual void bindState(StateHandle stateId, const ubyte stencilRef, const float4& blendColor);
            virtual bool bindTexture2D(EffectResourceHandle resourceId,
                                       Texture2DHandle textureId,
                                       const ShaderType shader) override;
            virtual void bindResources(const ResourceBucket& bucket) override;

            virtual void draw(const uint startVertex, const uint numVertices) override;
            virtual void drawInstanced(const uint startVertex, const uint numVertices,
                                       const uint startInstance, const uint numInstances) override;
            virtual void drawIndexed(const uint startIndex, const uint numIndices,
                                     const uint baseVertex) override;
            virtual void drawIndexedInstanced(const uint startIndex, const uint numIndices,
                                              const uint baseVertex,
                                              const uint startInstance, const uint numInstances) override;

            // Gets
            virtual usl::TargetLanguage effectLanguage() const override;
        private:
            template <class Desc, class Impl>
            void destroyResource(GapiAllocator<Desc, Impl>& pool, GapiPointer<Desc> handle)
            {
                Impl& impl = handle.template get<Impl>();
                impl.dispose();
                pool.destroy(handle);
            }
            Texture2DHandle createTexture2D(const uint numArraySlices, const uint numMipLevels,
                                            const uint width, const uint height,
                                            Format format, DXGI_FORMAT dxgiFormat,
                                            ResourceUsage usage,
                                            FlagSet<ResourceBinding> binding,
                                            D3D11_SUBRESOURCE_DATA data[]);
            void setResource(const uint slot, ShaderType shader,
                             ID3D11ShaderResourceView* resource, ID3D11SamplerState* sampler);
        private:
            ID3D11Device* m_device;
            ID3D11DeviceContext* m_context;
            ID3D11RenderTargetView* m_defaultRenderTarget = nullptr;
            ID3D11SamplerState* m_defaultSampler = nullptr;
            mutex m_mutex; ///< Make create functions thread-safe

            // Pools
            struct EffectImpl;
            struct ProgramImpl : ProgramDesc
            {
                EffectImpl* m_effect = nullptr;
                ID3D11VertexShader* m_vertexShader = nullptr;
                ID3D11HullShader* m_hullShader = nullptr;
                ID3D11DomainShader* m_domainShader = nullptr;
                ID3D11GeometryShader* m_geometryShader = nullptr;
                ID3D11PixelShader* m_pixelShader = nullptr;
            };
            struct EffectResourceImpl : EffectResourceDesc
            {
                EffectImpl* m_effect = nullptr;
            };
            struct EffectImpl : EffectDesc, DisposableConcept
            {
                vector<ID3D11DeviceChild*> m_shaders;
                vector<ProgramImpl> m_programs;
                vector<EffectResourceImpl> m_resources;

                array<ID3D11Buffer*, MaxShaderUniformBuffers> m_cachedBuffers;
                void dispose()
                {
                    for (ID3D11DeviceChild* shader : m_shaders)
                        safeRelease(shader);
                    m_shaders.clear();
                    m_programs.clear();
                    m_resources.clear();

                    for (ID3D11Buffer*& buffer : m_cachedBuffers)
                        safeRelease(buffer);
                }
                template <class Shader>
                Shader* findShader(sint index)
                {
                    if (index < 0)
                        return nullptr;
                    return static_cast<Shader*>(m_shaders[(uint) index]);
                }
            };
            GapiAllocator<EffectDesc, EffectImpl> m_pooledEffects;

            struct StateImpl : StateDesc, DisposableConcept
            {
                // Effect
                ID3D11Buffer* const* m_uniformBuffers = nullptr;

                ID3D11VertexShader* m_vertexShader = nullptr;
                ID3D11HullShader* m_hullShader = nullptr;
                ID3D11DomainShader* m_domainShader = nullptr;
                ID3D11GeometryShader* m_geometryShader = nullptr;
                ID3D11PixelShader* m_pixelShader = nullptr;

                // States
                ID3D11BlendState* m_bs = nullptr;
                ID3D11RasterizerState* m_rs = nullptr;
                ID3D11DepthStencilState* m_dss = nullptr;

                D3D11_PRIMITIVE_TOPOLOGY m_topology = D3D11_PRIMITIVE_TOPOLOGY_UNDEFINED;
                ID3D11InputLayout* m_layout = nullptr;
                const uint* m_strides = nullptr;
                void dispose()
                {
                    safeRelease(m_bs);
                    safeRelease(m_rs);
                    safeRelease(m_dss);
                }
            };
            GapiAllocator<StateDesc, StateImpl> m_pooledStates;

            struct BufferImpl : BufferDesc, DisposableConcept
            {
                D3D11_BUFFER_DESC m_desc;
                ID3D11Buffer* m_buffer = nullptr;
                void dispose()
                {
                    safeRelease(m_buffer);
                }
            };
            GapiAllocator<BufferDesc, BufferImpl> m_pooledBuffers;

            struct LayoutImpl : LayoutDesc, DisposableConcept
            {
                ID3D11InputLayout* m_layout = nullptr;
                void dispose()
                {
                    safeRelease(m_layout);
                }
            };
            GapiAllocator<LayoutDesc, LayoutImpl> m_pooledLayouts;

            struct Texture2DImpl : Texture2DDesc, DisposableConcept
            {
                ID3D11SamplerState* m_sampler = nullptr;
                ID3D11Texture2D* m_texture = nullptr;
                ID3D11ShaderResourceView* m_srv = nullptr;
                ID3D11RenderTargetView* m_rtv = nullptr;
                ID3D11DepthStencilView* m_dsv = nullptr;
                ID3D11Texture2D* m_reader = nullptr;
                void dispose()
                {
                    safeRelease(m_texture);
                    safeRelease(m_srv);
                    safeRelease(m_rtv);
                    safeRelease(m_dsv);
                    safeRelease(m_reader);
                    safeRelease(m_sampler);
                }
            };
            GapiAllocator<Texture2DDesc, Texture2DImpl> m_pooledTextures2D;

            struct RenderTargetImpl : RenderTargetDesc, DisposableConcept
            {
                ID3D11DepthStencilView* m_dsv = nullptr;
                array<ID3D11RenderTargetView*, MaxRenderTargets> m_rtvs;
                void dispose()
                {
                }
            };
            GapiAllocator<RenderTargetDesc, RenderTargetImpl> m_pooledRenderTargets;

            // Currents
            RenderTargetImpl* m_renderTarget = nullptr;

            // Cached currents
            ID3D11BlendState* m_currentBlendState = nullptr;
            float4 m_currentBlendColor = float4(0.0f);

            ID3D11RasterizerState* m_currentRasterizerState = nullptr;

            ID3D11DepthStencilState* m_currentDepthStencilState = nullptr;
            ubyte m_currentStencilRef = 0;

            uint m_currentPrimitiveType = 0;
            D3D11_PRIMITIVE_TOPOLOGY m_currentTopology = D3D11_PRIMITIVE_TOPOLOGY_UNDEFINED;
            ID3D11InputLayout* m_currentLayout = nullptr;
            const uint* m_currentStrides = nullptr;
            const uint* m_boundStrides = nullptr;
            array<ID3D11Buffer*, MaxStreams> m_currentVertexBuffers;
            ID3D11Buffer* m_currentIndexBuffer = nullptr;

            ResourceBucket m_currentBucket;

            EffectImpl* m_currentEffect = nullptr;
            ProgramImpl* m_currentProgram = nullptr;
        };
    }
}

#endif
