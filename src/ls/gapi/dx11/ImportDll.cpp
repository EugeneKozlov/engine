#include "pch.h"
#include "ls/common/config.h"

#if LS_COMPILE_DX11 != 0

#define INITGUID
#include "ls/gapi/dx11/ImportDll.h"

PFN_D3D11_CREATE_DEVICE D3D11CreateDeviceDll = nullptr;
PFN_D3D11_CREATE_DEVICE_AND_SWAP_CHAIN D3D11CreateDeviceAndSwapChainDll = nullptr;
PFN_CREATE_DXGI_FACTORY CreateDXGIFactoryDll = nullptr;

PFN_D3D_GET_INPUT_SIGNATURE_BLOB D3DGetInputSignatureBlobDll = nullptr;
PFN_D3D_COMPILE D3DCompileDll = nullptr;
PFN_D3D_REFLECT D3DReflectDll = nullptr;
PFN_D3D_DISASSEMBLE D3DDisassembleDll = nullptr;
const GUID iid_ID3D11ShaderReflection = IID_ID3D11ShaderReflection;
/// Helper struct
struct Direct3D11Holder
{
    HMODULE hDllD3D11 = NULL;
    HMODULE hDllDXGI = NULL;
    HMODULE hDllD3DCompiler = NULL;
    bool load()
    {
        if (isLoaded())
        {
            unload();
        }

        hDllDXGI = LoadLibrary("dxgi.dll");
        hDllD3D11 = LoadLibrary("d3d11.dll");
        hDllD3DCompiler = LoadLibrary(D3DCOMPILER_DLL_A);

        if (!hDllD3D11 || !hDllDXGI || !hDllD3DCompiler)
        {
            unload();
            return false;
        }

        D3D11CreateDeviceDll = (PFN_D3D11_CREATE_DEVICE)
            GetProcAddress(hDllD3D11, "D3D11CreateDevice");
        D3D11CreateDeviceAndSwapChainDll = (PFN_D3D11_CREATE_DEVICE_AND_SWAP_CHAIN)
            GetProcAddress(hDllD3D11, "D3D11CreateDeviceAndSwapChain");
        CreateDXGIFactoryDll = (PFN_CREATE_DXGI_FACTORY)
            GetProcAddress(hDllDXGI, "CreateDXGIFactory");

        D3DGetInputSignatureBlobDll = (PFN_D3D_GET_INPUT_SIGNATURE_BLOB)
            GetProcAddress(hDllD3DCompiler, "D3DGetInputSignatureBlob");
        D3DCompileDll = (PFN_D3D_COMPILE)
            GetProcAddress(hDllD3DCompiler, "D3DCompile");
        D3DReflectDll = (PFN_D3D_REFLECT)
            GetProcAddress(hDllD3DCompiler, "D3DReflect");
        D3DDisassembleDll = (PFN_D3D_DISASSEMBLE)
            GetProcAddress(hDllD3DCompiler, "D3DDisassemble");


        if (!isLoaded())
        {
            unload();
            return false;
        }

        return true;
    }
    bool isLoaded() const
    {
        return 
            D3D11CreateDeviceDll && D3D11CreateDeviceAndSwapChainDll && CreateDXGIFactoryDll &&
            D3DGetInputSignatureBlobDll && D3DCompileDll && D3DReflectDll && D3DDisassembleDll;
    }
    void unload()
    {
        D3DGetInputSignatureBlobDll = nullptr;
        D3DCompileDll = nullptr;
        D3DReflectDll = nullptr;

        D3D11CreateDeviceDll = nullptr;
        D3D11CreateDeviceAndSwapChainDll = nullptr;
        CreateDXGIFactoryDll = nullptr;

        if (hDllDXGI) FreeLibrary(hDllDXGI);
        if (hDllD3D11) FreeLibrary(hDllD3D11);
        if (hDllD3DCompiler) FreeLibrary(hDllD3DCompiler);
        hDllDXGI = NULL;
        hDllD3D11 = NULL;
        hDllD3DCompiler = NULL;
    }
    ~Direct3D11Holder()
    {
        unload();
    }
};

static Direct3D11Holder direct3D11Holder;
bool loadDirect3D11()
{
    return direct3D11Holder.load();
}
void unloadDirect3D11()
{
    direct3D11Holder.unload();
}

#endif