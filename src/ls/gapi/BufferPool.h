/// @file ls/gapi/BufferPool.h
/// @brief Buffer Pool
#pragma once

#include "ls/common.h"
#include "ls/core/StackAllocatorBase.h"
#include "ls/gapi/common.h"

namespace ls
{
    class Renderer;

    /// @addtogroup LsGapi
    /// @{

    /// @brief Buffer Pool Allocation
    struct BufferPoolAllocation
    {
    public:
        /// @brief Ctor
        BufferPoolAllocation() = default;
        /// @brief Ctor
        BufferPoolAllocation(nullptr_t)
        {
        }
        /// @brief Test
        bool operator !() const
        {
            return !buffer;
        }
        /// @brief Wrap
        template <class Object>
        ArrayWrapper<Object> wrap()
        {
            return ArrayWrapper<Object>(reinterpret_cast<Object*>(ptr), 
                                        size / sizeof(Object));
        }
    public:
        /// @brief Buffer
        BufferHandle buffer;
        /// @brief Block size (in bytes)
        uint size = 0;
        /// @brief Aligned offset (in blocks)
        uint offset = 0;
        /// @brief Number of allocated blocks
        uint count = 0;
        /// @brief Mapped pointer
        void* ptr = nullptr;
    };
    /// @brief Buffer Pool
    class BufferPool
        : Noncopyable
    {
    public:
        /// @brief Ctor
        BufferPool(Renderer& renderer, BufferType bufferType, 
                   uint bufferSize, uint resizeFactor);
        /// @brief Dtor
        ~BufferPool();

        /// @brief Allocate memory, aligned to @a size
        /// @param count
        ///   Number of blocks
        /// @param size
        ///   Block size
        BufferPoolAllocation allocate(uint count, uint size);
        /// @brief Commit buffers
        void commit();
        /// @brief Discard buffers (return @a true if buffers were purged due to resizing)
        bool discard();
        /// @brief Purge buffers
        void purge();
    private:
        struct BucketDesc
        {
            BucketDesc(Renderer& renderer, BufferType bufferType)
                : renderer(&renderer)
                , bufferType(bufferType)
            {
            }
            Renderer* renderer;
            BufferType bufferType;
        };
        struct Bucket : DisposableConcept
        {
            Bucket(const BucketDesc& desc, uint size);
            void* data();
            void commit();
            void dispose();
        public:
            Renderer* renderer;

            BufferHandle buffer;
            void* ptr = nullptr;
        };
    private:
        StackAllocatorBase<Bucket, BucketDesc> impl;
    };
    /// @}
}

