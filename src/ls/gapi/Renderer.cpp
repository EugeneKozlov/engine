#include "pch.h"
#include "ls/core/GlobalVariableT.h"
#include "ls/gapi/Renderer.h"
#include "ls/gapi/detail/TextureLoader.h"

using namespace ls;
// Config
static GlobalVariableT<uint> g_vertexBufferChunkSize(
    "render:vbPoolChunkSize", "", GlobalVariableFlag::ReadOnly,
    "Vertex buffer chunk size (kb)\n"
    "Must be not less than 1\n",
    128, [](uint value) { return value >= 1; });
static GlobalVariableT<bool> g_logRenderer(
    "logger:rrAll", "log:rrAll", GlobalVariableFlag::Modifiable | GlobalVariableFlag::Console,
    "Set to enable renderer logging\n"
    "Must be not less than 1\n",
    false);


// Vertex formats
VertexFormat const ColoredVertex::Layout[] = {
    VertexFormat(Format::R32G32B32A32_FLOAT, "Position", 0, 0, false),
    VertexFormat(Format::R8G8B8A8_UNORM, "Color", 0, 16, false)
};
uint const ColoredVertex::LayoutSize = 2;

VertexFormat const TexturedVertex::Layout[] = {
    VertexFormat(Format::R32G32B32A32_FLOAT, "Position", 0, 0, false),
    VertexFormat(Format::R32G32_FLOAT, "TexCoord", 0, 16, false),
    VertexFormat(Format::R8G8B8A8_UNORM, "Color", 0, 24, false)
};
uint const TexturedVertex::LayoutSize = 3;

VertexFormat const LightedVertex::Layout[] = {
    VertexFormat(Format::R32G32B32_FLOAT, "Position", 0, 0, false),
    VertexFormat(Format::R32G32_FLOAT, "TexCoord", 0, 12, false),
    VertexFormat(Format::R32G32B32_FLOAT, "Normal", 0, 20, false)
};
uint const LightedVertex::LayoutSize = 3;


// Renderer
Renderer::Renderer()
    : Nameable<Renderer>("Renderer", InvisibleIndex)
    , m_hasLogging(g_logRenderer.value())
{
}
Renderer::~Renderer()
{
}
void Renderer::initialize()
{
    deinitialize();

    vertexBufferPool = make_unique<BufferPool>(*this, 
                                               BufferType::Vertex, 
                                               1024 * g_vertexBufferChunkSize.value(),
                                               2);
}
void Renderer::deinitialize()
{
    vertexBufferPool.reset();
}


// Stuff
void Renderer::unloadTexture2D(TextureStorage& dest, Texture2DHandle textureId)
{
    Texture2DDesc& texture = textureId.get<Texture2DDesc>();
    reconstruct(dest, 
                gapi::gliFormat(texture.format),
                gli::texture2DArray::texelcoord_type(texture.width,
                                                     texture.height),
                texture.numSlices, texture.numMips);
    for (uint slice = 0; slice < texture.numSlices; ++slice)
    {
        for (uint mipLevel = 0; mipLevel < texture.numMips; ++mipLevel)
        {
            uint width = max<uint>(1, texture.width >> mipLevel);
            uint height = max<uint>(1, texture.height >> mipLevel);
            gli::image image = dest[slice][mipLevel];

            readTexture2D(textureId, slice, mipLevel, 
                          0, 0, width, height, image.data());
        }
    }
}
void Renderer::addEffectLibraryChunk(const string& name, const string& text)
{
    m_effectLibrary.emplace(name, text);
}


// Gets
void Renderer::resize(const uint width, const uint height) noexcept
{
    m_width = width;
    m_height = height;
}
const uint Renderer::width() const noexcept
{
    return m_width;
}
const uint Renderer::height() const noexcept
{
    return m_height;
}
bool Renderer::hasTessellation() const
{
    return m_tessellationSupport;
}
const usl::Library& Renderer::effectLibrary() const
{
    return m_effectLibrary;
}
RendererStatistic Renderer::getStats() const
{
    RendererStatistic ret = m_stats;
    ret.numLines = 0;
    ret.numLines += ret.numDrawnIndices(PrimitiveType::Line) / 2;
    if (ret.numDrawnIndices(PrimitiveType::LineStrip) >= 1)
        ret.numLines += ret.numDrawnIndices(PrimitiveType::LineStrip) - 1;
    ret.numTriangles = 0;
    ret.numTriangles += ret.numDrawnIndices(PrimitiveType::Triangle) / 3;
    if (ret.numDrawnIndices(PrimitiveType::TriangleStrip) >= 2)
        ret.numTriangles += ret.numDrawnIndices(PrimitiveType::TriangleStrip) - 2;
    ret.numTriangles += ret.numDrawnIndices(PrimitiveType::Patch3) / 3;
    ret.numTriangles += ret.numDrawnIndices(PrimitiveType::Patch4) / 2;
    return ret;
}
void Renderer::resetStats()
{
    m_stats = RendererStatistic();
}