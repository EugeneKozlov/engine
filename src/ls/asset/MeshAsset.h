/// @file ls/asset/MeshAsset.h
/// @brief Mesh Asset
#pragma once

#include "ls/common.h"
#include "ls/asset/Asset.h"
#include "ls/gapi/common.h"

namespace ls
{
    class Renderer;

    /// @addtogroup LsAsset
    /// @{

    /// @brief Mesh Asset
    class MeshAsset
        : public AssetT<AssetType::Mesh, AssetCategory::Simple>
    {
    public:
        /// @brief Max number of subsets
        static const uint MaxSubsets = 8;
        /// @brief Max number of uniform vectors
        static const uint MaxUniforms = 16;
        /// @brief Uniform Buffer
        using UniformBuffer = array<float, MaxUniforms>;
        /// @brief Subset Description
        struct Subset
        {
            /// @brief Triangle offset
            uint offset;
            /// @brief Number of triangles
            uint numIndices;
            /// @brief Material ID
            uint materialIndex;
        };
    public:
        /// @brief Ctor
        MeshAsset();
        /// @brief Dtor
        ~MeshAsset();

        /// @name Construct mesh
        /// @{

        /// @brief Set model AABB
        void setAabb(const AABB& aabb);
        /// @brief Add model subset
        void addSubset(uint materialIndex, uint numIndices);
        /// @brief Set vertex data
        template <class Vertex>
        void setVertexData(const Vertex* vertices, uint numVertices)
        {
            const ubyte* data = reinterpret_cast<const ubyte*>(vertices);
            m_vertexStride = sizeof(Vertex);
            m_numVertices = numVertices;
            m_vertexData.assign(data, data + numVertices * m_vertexStride);
        }
        /// @brief Set index data
        template <class Index>
        void setIndexData(const Index* indices, uint numIndices)
        {
            const ubyte* data = reinterpret_cast<const ubyte*>(indices);
            m_indexStride = sizeof(Index);
            m_numIndices = numIndices;
            m_indexData.assign(data, data + numIndices * m_indexStride);

            switch (m_indexStride)
            {
            case 1: m_indexType = IndexFormat::Ubyte; break;
            case 2: m_indexType = IndexFormat::Ushort; break;
            case 4: m_indexType = IndexFormat::Uint; break;
            default: m_indexType = IndexFormat::Unknown; break;
            }
        }
        /// @brief Set parameters
        template <class Buffer>
        void setParameters(const Buffer& m_buffer)
        {
            m_parameterData.resize(sizeof(Buffer));
            std::copy_n(reinterpret_cast<const ubyte*>(&m_buffer), 
                        sizeof(Buffer), 
                        m_parameterData.begin());
        }
        /// @brief Set uniform buffer
        void setUniformBuffer(const UniformBuffer& buffer);
        /// @brief Set uniform buffer by object
        template <class Object>
        void setUniformBuffer(const Object& buffer)
        {
            static_assert(sizeof(Object) <= sizeof(UniformBuffer),
                          "Too many uniforms");
            static_assert(sizeof(Object) % sizeof(float) == 0,
                          "Uniform buffer must have multiple of 4 bytes size");
            m_uniformBuffer.fill(0.0f);
            std::copy_n(reinterpret_cast<const ubyte*>(&buffer),
                        sizeof(Object), 
                        reinterpret_cast<ubyte*>(m_uniformBuffer.data()));
        }
        /// @}

        /// @name GAPI
        /// @{

        /// @brief Drop buffers stored in RAM
        void dropRam();
        /// @brief Is GAPI ready?
        bool isGapiReady() const;
        /// @brief Get GAPI geometry
        /// @pre GAPI is ready
        const GeometryBucket& gapiGeometry() const;
        /// @}

        /// @name Gets
        /// @{

        /// @brief Get number of subsets
        uint numSubsets() const;
        /// @brief Get subset
        const Subset& subset(const uint subsetIndex) const;
        /// @brief Get AABB
        const AABB& aabb() const;

        /// @brief Get uniform buffer
        const UniformBuffer& uniformBuffer() const;
        /// @brief Get parameters
        template <class Buffer>
        Buffer parameters() const
        {
            Buffer buffer;
            ubyte* dest = reinterpret_cast<ubyte*>(&buffer);
            std::copy_n(m_parameterData.begin(), 
                        min(sizeof(Buffer), m_parameterData.size()), 
                        dest);
            return buffer;
        }

        /// @}
    private:
        /// @name Implement Asset
        /// @{
        virtual string extension() const override;
        virtual void doSerialize(Archive<Buffer> archive) override;
        virtual void doRestore() override;
        /// @}

        /// @brief Allocate GAPI buffers
        void allocateGapi(Renderer& renderer);
        /// @brief Deallocate GAPI buffers
        void deallocateGapi(Renderer& renderer);
    private:
        /// @name Data
        /// @{
        vector<ubyte> m_vertexData;
        uint m_numVertices = 0;
        uint m_vertexStride = 0;

        vector<ubyte> m_indexData;
        uint m_numIndices = 0;
        uint m_indexStride = 0;
        IndexFormat m_indexType = IndexFormat::Unknown;

        vector<ubyte> m_parameterData;

        array<Subset, MaxSubsets> m_subsets;
        uint m_numSubsets = 0;

        AABB m_aabb;
        UniformBuffer m_uniformBuffer;
        /// @}

        /// @name Renderer
        /// @{
        bool m_gapiReady = true;
        GeometryBucket m_geometry;
        /// @}
    };

    /// @}
}