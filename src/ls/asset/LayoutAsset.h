/// @file ls/asset/LayoutAsset.h
/// @brief Effect Library Asset
#pragma once

#include "ls/common.h"
#include "ls/asset/Asset.h"
#include "ls/core/AutoExpandingVector.h"
#include "ls/gapi/common.h"

namespace ls
{
    /// @addtogroup LsAsset
    /// @{

    /// @brief Layout Asset
    class LayoutAsset
        : public AssetT<AssetType::Layout, AssetCategory::Config>
    {
    public:
        /// @brief Ctor
        LayoutAsset();
        /// @brief Dtor
        ~LayoutAsset();
        /// @brief Get vertex format
        const vector<VertexFormat>& vertexFormat() const;
        /// @brief Get strides
        const vector<uint>& strides() const;
    private:
        /// @name Implement Asset
        /// @{
        virtual void doReadConfig(INIReader& ini) override;
        virtual void doSerializeConfig(Archive<Buffer> archive) override;
        virtual void doRestore() override;
        /// @}

        void computeStrides();
    private:
        vector<VertexFormat> m_format;
        AutoExpandingVector<uint> m_strides;
    };

    /// @}
}