/// @file ls/asset/ModelAsset.h
/// @brief Model Asset
#pragma once

#include "ls/common.h"
#include "ls/asset/Asset.h"
#include "ls/asset/MeshAsset.h"
#include "ls/asset/MaterialAsset.h"

namespace ls
{
    class Renderer;
    class Worker;

    /// @addtogroup LsAsset
    /// @{

    /// @brief Model Asset
    class ModelAsset
        : public AssetT<AssetType::Model, AssetCategory::Config>
    {
    public:
        /// @brief Max number of LODs
        static const uint MaxLods = 8;
    public:
        /// @brief Ctor
        ModelAsset();
        /// @brief Dtor
        ~ModelAsset();
        /// @brief Set AABB
        void setAabb(const AABB& aabb);

        /// @brief Request specified LOD and find nearest available
        /// @param minLevel
        ///   Minimal LOD to search
        /// @return @a MaxLods if no LOD is available
        uint findLevel(const uint minLodIndex);
        /// @brief Request model level to be loaded
        /// @param lodIndex
        ///   Requested LOD
        bool requestLevel(const uint lodIndex);
        /// @brief Load model level
        /// @param lodIndex
        ///   Requested LOD
        void loadLevel(const uint lodIndex);
        /// @brief Drop level
        /// @param lodIndex
        ///   LOD to drop
        void dropLevel(const uint lodIndex);

        /// @brief Get number of levels
        uint numLevels() const;
        /// @brief Get switch size
        float switchSize(const uint lodIndex) const;
        /// @brief Get in-out gap
        float inoutGap(const uint lodIndex) const;
        /// @brief Get mesh
        MeshAsset* mesh(const uint lodIndex);
        /// @brief Get number of materials
        uint numMaterials(const uint lodIndex) const;
        /// @brief Get material
        MaterialAsset* material(const uint lodIndex, const uint materialIndex);
        /// @brief AABB
        const AABB& aabb() const;
    private:
        /// @name Implement Asset
        /// @{
        virtual void doReadConfig(INIReader& ini) override;
        virtual void doSerializeConfig(Archive<Buffer> archive) override;
        virtual void doRestore() override;
        /// @}

        /// @brief Clamp lod level to maximum
        uint clampLevel(const uint level) const
        {
            debug_assert(m_numLods > 0);
            return std::min(level, m_numLods - 1);
        }
    private:
        enum class State
        {
            /// @brief Not loaded yet
            NonLoaded,
            /// @brief Asset is loading now
            Loading,
            /// @brief Asset is loaded (successfully or not)
            Loaded,
            /// @brief Asset is loaded and ready to use
            Ready,
            /// @brief Asset is not completely loaded due to error
            Incomplete
        };
        struct Lod
        {
            std::atomic<State> state {State::NonLoaded};
            bool corrupted = false;
            std::future<void> asyncResult;

            float switchSize = 1.0f; ///< Lod switch size
            float inoutGap = 0.0f; ///< Difference between fade-in and fade-out distances

            string meshName; ///< Mesh name
            shared_ptr<MeshAsset> mesh; ///< Mesh
            uint numMaterials = 0; ///< Number of materials
            array<string, MeshAsset::MaxSubsets> materialsNames; ///< Material names
            array<shared_ptr<MaterialAsset>, MeshAsset::MaxSubsets> materials; ///< Materials
        };

        void loadModelLevel(Lod& lod);
    private:
        uint m_numLods; ///< Number of LODs
        array<Lod, MaxLods> m_lods; ///< LODs
        AABB m_aabb = double3(0.0); ///< AABB
    };

    /// @}
}