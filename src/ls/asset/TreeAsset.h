/// @file ls/asset/TreeAsset.h
/// @brief Tree Asset
#pragma once

#include "ls/common.h"
#include "ls/asset/Asset.h"
#include "ls/asset/ModelAsset.h"
#include "ls/core/UniformRandom.h"
#include "ls/gapi/common.h"

namespace ls
{
    class Renderer;
    class MeshAsset;
    struct INISection;

    /// @addtogroup LsAsset
    /// @{

    /// @brief Tree Asset
    // #TODO Add branch levels
    // #TODO Indexed materials
    class TreeAsset
        : public AssetT<AssetType::Tree, AssetCategory::DataCompiled>
    {
    public:
        /// @brief Uniform Buffer parameters for model
        struct ModelUniformBuffer
        {
            /// @brief Dithering scale factor
            float dithering = 200.0f;

            /// @brief Edge main magnitude modifier
            float edgeMainMagnitude = 0.0f;
            /// @brief Edge turbulence magnitude modifier
            float edgeTurbulenceMagnitude = 0.0f;
            /// @brief Edge frequency
            float edgeFrequency = 0.0f;

            /// @brief Branch turbulence oscillation frequency
            float branchFrequency = 0.0f;
            /// @brief Branch main oscillation phase modifier
            float branchPhaseModifier = 0.0f;

            float branchMainMagnitude = 0.0f;
            /// @brief Trunk oscillation turbulence magnitude
            float branchTurbulenceMagnitude = 0.0f;

            /// @brief Trunk oscillation magnitude
            float trunkMagnitude = 0.0f;

            float reserve1[7];
        };
        /// @brief Uniform Buffer parameters for proxy
        struct ProxyUniformBuffer
        {
            /// @brief Dithering scale factor
            float dithering = 200.0f;
            /// @brief Proxy fade intervals begin
            float3 fadeBegin;

            /// @brief Proxy pad distance for shadow rendering
            float shadowPad = 0.0f;
            /// @brief Proxy fade intervals end
            float3 fadeEnd;

            /// @brief Trunk oscillation magnitude
            float trunkMagnitude = 0.0f;
            /// @brief Edge main magnitude modifier
            float edgeMainMagnitude = 0.0f;
            /// @brief Edge turbulence magnitude modifier
            float edgeTurbulenceMagnitude = 0.0f;
            /// @brief Edge frequency
            float edgeFrequency = 0.0f;

            float reserve1[4];
        };
        /// @brief Model Parameters
        struct ModelParameters
        {
            /// @brief Trunk strength
            float trunkStrength = 0.5f;
            /// @brief Radius of bounded cylinder
            float modelRadius = 0.0f;

            /// @brief Model uniform buffer
            ModelUniformBuffer model;
            /// @brief Proxy uniform buffer
            ProxyUniformBuffer proxy;
        };
        /// @brief Vertex type
        struct Vertex
        {
            /// @brief Default ctor
            Vertex() = default;
            /// @brief Lerp ctor
            Vertex(const Vertex& from, const Vertex& to, const float factor)
                : pos(lerp(from.pos, to.pos, factor))
                , uv(lerp(from.uv, to.uv, factor))

                , normal(lerp(from.normal, to.normal, factor))
                , tangent(lerp(from.tangent, to.tangent, factor))
                , binormal(lerp(from.binormal, to.binormal, factor))

                , mainAdherence(lerp(from.mainAdherence, to.mainAdherence, factor))
                , branchAdherence(lerp(from.branchAdherence, to.branchAdherence, factor))
                , phase(lerp(from.phase, to.phase, factor))
                , edgeOscillation(lerp(from.edgeOscillation, to.edgeOscillation, factor))
            {
            }

            float3 pos; ///< Position
            float2 uv; ///< Texture coordinates

            float3 normal; ///< Normal
            float3 tangent; ///< Tangent
            float3 binormal; ///< Binormal

            float mainAdherence; ///< Ground vertex adherence
            float branchAdherence; ///< Branch vertex adherence
            float phase; ///< Phase for oscillations
            float edgeOscillation; ///< Edge oscillation factor
        };
        /// @brief Function type
        enum class FunctionType
        {
            /// @brief Zero, y=0
            Zero,
            /// @brief Constant, y=1
            Constant,
            /// @brief Linear, y=x
            Linear,
            /// @brief Quad, y=x^2
            Quad,
            /// @brief Harmonic (smooth at 1), y=sin(pi*x/2)
            SmoothRight,
            /// @brief Harmonic (smooth at 0), y=1-cos(pi*x/2)
            SmoothLeft,
            /// @brief Harmonic (smooth at both 0 and 1), y=(1-cos(pi*x))/2
            SmoothBoth,
        };
        /// @brief Function used to compute radius or length
        struct Function
        {
            float2 beginValue = float2(0.0f, 0.0f); ///< Begin value
            float2 endValue = float2(1.0f, 1.0f); ///< End value
            FunctionType interpType = FunctionType::Constant; ///< Function type
        public:
            /// @brief Ctor
            Function() = default;
            /// @brief Ctor
            Function(const FunctionType type);
            /// @brief Ctor
            Function(const FunctionType type, const float value);
            /// @brief Ctor
            Function(const FunctionType type, const float begin, const float end);
            /// @brief Set value
            void set(const float beginX, const float beginY, const float endX, const float endY);
            /// @brief Set value
            void set(const float begin, const float end);
            /// @brief Set value
            void set(const float value);
            /// @brief Compute value
            float compute(const float input) const;
        private:
            /// @brief Initialize internal function object
            void initialize() const;
            /// @brief Internal function object
            mutable function<float(float)> m_interpFunction; ///< Function
        };
        /// @brief Material parameters
        struct Material
        {
            uint materialIndex = 0; ///< Material index
            float2 textureScale = float2(1.0f, 1.0f); ///< Texture scale
        };
        /// @brief Branch Distribution type
        enum class DistributionType
        {
            /// @brief Generate branches with specified rotation step
            Alternate,
        };
        /// @brief Branch Distribution
        struct BranchDistribution
        {
            DistributionType type = DistributionType::Alternate; ///< Distribution type

            float angleStep = 180.0f; ///< Angle step (alternate distribution)
            float angleRandomness = 0.0f; ///< Angle random (alternate distribution)
        };
        /// @brief Frond Type
        enum class FrondType
        {
            /// @brief Rag-like volume fronds at the end of branch
            ///
            /// Geometry:
            /// @code
            ///   ___ __     branch is here                  
            /// _|___|__| __/      x
            ///  |___|__|          |_z
            ///     ^              
            ///  branch ends here  
            ///
            ///           branch is here
            ///   /|\  __/ 
            ///  |/|\|
            ///  | | |  y
            ///  |/ \|  |_x
            ///
            ///            branch is here
            ///  ___    __/
            /// |   |\
            /// |___| |  y
            ///      \|  |_z
            ///     ^              
            ///  branch ends here  
            ///
            /// @endcode
            ///
            /// Vertices:
            /// @code
            ///           
            ///  6__7__8  u
            ///  |  |  |  |_v
            ///  |__|__|  
            ///  |3 |4 |5 x
            ///  |__|__|  |_z
            ///  0  1  2
            ///
            /// @endcode
            RagEnding,
            /// @brief Brush-like volume fronds at the end of branch
            BrushEnding
        };
        /// @brief Rag Ending Frond Parameters
        struct RagEndingFrondParameters
        {
            float locationOffset = 0.0f; ///< Absolute offset from branch ending
            float centerOffset = 0.0f; ///< Offset of the center vertex
            float zBendingFactor = 0.5f; ///< Bending factor along Z axis, set 0 to keep horizontal
            float xBendingFactor = 0.5f; ///< Bending factor along X axis, set 0 to keep horizontal

            float junctionOscillation = 0.1f; ///< Junction oscillation
            float edgeOscillation = 1.0f; ///< Edge oscillation
        };
        /// @brief Brush Ending Frond Parameters
        struct BrushEndingFrondParameters
        {
            float locationOffset = 0.0f; ///< Absolute offset from branch ending
            float centerOffset = 0.0f; ///< Offset of the center vertex along the normal
            float spreadAngle = 60.0f; ///< Spread angle

            float junctionOscillation = 0.1f; ///< Junction oscillation
            float edgeOscillation = 1.0f; ///< Edge oscillation
        };
        /// @brief Frond Group
        struct FrondGroup
        {
            FrondType type; ///< Frond type
            float2 sizeRange = 1.0f; ///< Min/max frond size

            RagEndingFrondParameters ragEndingParam; ///< Rag volume ending frond parameters
            BrushEndingFrondParameters brushEndingParam; ///< Brush volume ending frond parameters
        };
        /// @brief Single branch frequency constant
        static const uint SingleBranch = static_cast<uint>(-1);
        /// @brief Branch Group Parameters
        /// @note Set @a frequency to SingleBranch to create branch with specified position and direction
        struct BranchGroupParam
        {
            Material branchMaterial; ///< Branch material

            uint seed = 0; ///< Seed for generation, 0 to randomize
            uint frequency = SingleBranch; ///< Number of branches
            float geometryMultiplier = 1.0f; ///< Multiplier for geometry details

            /// @name Single branch
            /// @{
            float3 position = float3(0.0f, 0.0f, 0.0f); ///< Position
            float3 direction = float3(0.0f, 1.0f, 0.0f); ///< Direction
            /// @}

            BranchDistribution distrib; ///< Branches distribution
            Function distribFunction = FunctionType::Constant; ///< Branches distribution function
            float2 location = float2(0.0f, 1.0f); ///< Location on parent branch

            Function length = FunctionType::Constant; ///< Length function
            Function radius = FunctionType::Constant; ///< Radius function
            Function angle = Function(FunctionType::Constant, 90.0f); ///< Angle between branch and parent
            Function weight = Function(FunctionType::Constant, 0.0f); ///< Bends branch down

            Function branchAdherence = FunctionType::Linear; ///< Adherence of branches in group
            float phaseRandom = 0.0f; ///< Set to randomize branch phase
        };
        /// @brief Branch Group
        class BranchGroup
        {
        public:
            /// @brief Add children branch group
            BranchGroup& addBranchGroup();
            /// @brief Add children frond group
            FrondGroup& addFrondGroup();
            /// @brief Clear
            void clear()
            {
                m_childrenBranchGroups.clear();
            }
            /// @brief Get branches
            auto branches() const
            {
                return std::make_pair(m_childrenBranchGroups.begin(),
                                      m_childrenBranchGroups.end());
            }
            /// @brief Get fronds
            auto fronds() const
            {
                return std::make_pair(m_childrenFrondGroups.begin(),
                                      m_childrenFrondGroups.end());
            }
        public:
            BranchGroupParam p; ///< Parameters
        private:
            list<BranchGroup> m_childrenBranchGroups; ///< Children branch groups
            list<FrondGroup> m_childrenFrondGroups; ///< Children frond groups
        };
        /// @brief LOD info
        struct LodInfo
        {
            uint numLengthSegments = 5; ///< Number of length segments
            uint numRadialSegments = 5; ///< Number of radial segments
        };
        /// @brief Compute main adherence
        static float computeMainAdherence(const float relativeHeight, const float trunkStrength);
    public:
        /// @brief Ctor
        TreeAsset();
        /// @brief Dtor
        ~TreeAsset();

        /// @name Set/Get parameters
        /// @{

        /// @brief Set dithering
        void setDithering(const float dithering);
        /// @brief Set trunk strength
        void setTrunkStrength(const float trunkStrength);
        /// @brief Set trunk oscillation magnitude
        void setTrunkMagnitude(const float trunkMagnitude);
        /// @brief Set branch oscillation magnitude
        void setBranchMagnitude(const float branchMainMagnitude, 
                                const float branchTurbulenceMagnitude);
        /// @brief Set branch turbulence frequency
        void setBranchTurbulenceFrequency(const float branchFrequency);
        /// @brief Set branch main phase modifier
        void setBranchMainPhaseModifier(const float phaseModifier);
        /// @brief Set edge magnitude
        void setEdgeMagnitude(const float edgeMainMagnitude,
                              const float edgeTurbulenceMagnitude);
        /// @brief Set edge frequency
        void setEdgeFrequency(const float edgeFrequency);
        /// @brief Set switch fade interval
        /// @note Slices are fading @b in when eye.normal is [@a from -> @a to]
        void setSwitchFadeInterval(const float from, const float to);
        /// @brief Set vertical slices fade interval
        /// @note Vertical slices are fading @b out when eye.normal is [@a from -> @a to]
        void setVerticalFadeInterval(const float from, const float to);
        /// @brief Set skew slices fade interval
        /// @note Skew slices are fading @b in when eye.normal is [@a from -> @a to]
        void setSkewFadeInterval(const float from, const float to);
        /// @brief Set proxy edge magnitude
        void setProxyEdgeMagnitude(const float edgeMainMagnitude,
                                   const float edgeTurbulenceMagnitude);
        /// @brief Set proxy edge frequency
        void setProxyEdgeFrequency(const float edgeFrequency);
        /// @brief Set proxy shadow pad
        void setProxyShadowPad(const float shadowPad);

        /// @brief Horizontal slices visibility fade interval
        /// @}

        /// @brief Add new trunk group
        BranchGroup& getRootGroup();

        /// @brief Generate assets
        void generateMeshes(const vector<LodInfo>& lodInfos, 
                            vector<shared_ptr<MeshAsset>>& meshes);
    private:
        /// @name Implement Asset
        /// @{
        virtual string extension() const override;
        virtual void doCompileAndLoad(StreamInterface& source) override;
        virtual void doSerialize(Archive<Buffer> archive) override;
        void loadGlobalParameters(const INISection& section);
        BranchGroup& loadBranchGroup(const INIReader& source, const string& name);
        FrondGroup& loadFrondGroup(const INIReader& source, const string& name);
        /// @}

        /// @name Generator
        /// @{

        struct Point
        {
            Point() = default;
            Point(const Point& from, const Point& to, const float factor);

            float3 position; ///< Position
            float radius; ///< Radius
            float location; ///< Point location on branch

            float distance; ///< Distance from branch begin
            float3 direction; ///< This-to-next direction
            float segmentLength; ///< This-to-next distance
            float3 zAxis; ///< Average previous-to-this-to-next direction
            float3 xAxis; ///< X-axis
            float3 yAxis; ///< Y-axis

            float relativeDistance; ///< Relative distance from branch begin

            float branchAdherence; ///< Branch point adherence
            float phase; ///< Branch oscillation phase at point
        };
        struct Branch
        {
            float3 position; ///< Real branch position
            float3 direction; ///< Real branch direction
            float totalLength = 0.0f; ///< Total branch length
            float location = 1.0f; ///< Relative position on parent branch

            float parentRadius = 1.0f; ///< Parent radius at child location
            float parentLength = 1.0f; ///< Parent length
            float parentBranchAdherence = 0.0f; ///< Parent branch adherence
            float parentPhase = 0.0f; ///< Parent branch phase

            BranchGroup groupInfo; ///< Branch group info
            vector<Point> points; ///< Branch points
            vector<Branch> children; ///< Children branches

                                     /// @brief Add point
            void addPoint(const float location, const float3& position, const float radius,
                          const float phase, const float adherence);
            /// @brief Get point at specified location
            Point getPoint(const float location) const;
        };
        struct InternalGenerator;
        struct InternalBranchTriangulator;
        struct InternalFrondTriangulator;
        /// @brief Generate skeleton
        /// @param lodInfo
        ///   LOD info
        /// @param [out] trunksArray
        ///   Generated array of trunks
        /// @throw ArgumentException if branch group has no parent
        /// @throw ArgumentException if empty branch group has child group
        void generateSkeleton(const LodInfo& lodInfo, vector<Branch>& trunksArray) const;
        /// @brief Triangulate branches
        uint triangulateBranches(const LodInfo& lodInfo, const vector<Branch>& trunksArray,
                                 vector<Vertex>& vertices, vector<uint>& indices) const;
        /// @brief Triangulate fronds
        uint triangulateFronds(const LodInfo& lodInfo, const vector<Branch>& trunksArray,
                               vector<Vertex>& vertices, vector<uint>& indices) const;
        /// @brief Compute main adherence
        void computeMainAdherence(vector<Vertex>& vertices);
        /// @brief Normalize branch adherence
        void normalizeBranchesAdherence(vector<Vertex>& vertices);
        /// @brief Normalize phases
        void normalizePhases(vector<Vertex>& vertices);
        /// @}
    private:
        BranchGroup m_root; ///< Trunks list
        hamap<string, BranchGroup*> m_branchGroups; ///< All branch groups
        set<string> m_loadingBranchGroups; ///< Prevents circular references

        ModelParameters m_modelParameters;
    };


    /// @brief Parse string as function type
    template <class Stream>
    Stream& operator >>(Stream& stm, TreeAsset::FunctionType& value)
    {
        const static map<string, TreeAsset::FunctionType> m = {
            make_pair(string("Zero"), TreeAsset::FunctionType::Zero),
            make_pair(string("Constant"), TreeAsset::FunctionType::Constant),
            make_pair(string("Linear"), TreeAsset::FunctionType::Linear),
            make_pair(string("Quad"), TreeAsset::FunctionType::Quad),
            make_pair(string("SmoothLeft"), TreeAsset::FunctionType::SmoothLeft),
            make_pair(string("SmoothRight"), TreeAsset::FunctionType::SmoothRight),
            make_pair(string("SmoothBoth"), TreeAsset::FunctionType::SmoothBoth),
        };
        return defaultEnumReader(stm, value, m);
    }
    /// @brief Parse string as distribution type
    template <class Stream>
    Stream& operator >>(Stream& stm, TreeAsset::DistributionType& value)
    {
        const static map<string, TreeAsset::DistributionType> m = {
            make_pair(string("Alternate"), TreeAsset::DistributionType::Alternate),
        };
        return defaultEnumReader(stm, value, m);
    }
    /// @brief Parse string as frond type
    template <class Stream>
    Stream& operator >>(Stream& stm, TreeAsset::FrondType& value)
    {
        const static map<string, TreeAsset::FrondType> m = {
            make_pair(string("RagEnding"), TreeAsset::FrondType::RagEnding),
            make_pair(string("BrushEnding"), TreeAsset::FrondType::BrushEnding),
        };
        return defaultEnumReader(stm, value, m);
    }
    /// @brief Parse string as function
    template <class Stream>
    Stream& operator >>(Stream& stm, TreeAsset::Function& value)
    {
        static const auto MaxIgnore = numeric_limits<std::streamsize>::max();

        stm >> value.interpType;

        // Read parameters
        vector<float> params;
        float token;
        while (stm && (stm >> token))
        {
            stm.ignore(MaxIgnore, ',');
            params.push_back(token);
        }
        stm.clear();

        // Init function
        if (params.size() == 0)
        {
            // do nothing
        }
        else if (params.size() == 1)
        {
            value.set(params[0]);
        }
        else if (params.size() == 2)
        {
            value.set(params[0], params[1]);
        }
        else if (params.size() == 4)
        {
            value.set(params[0], params[1], params[2], params[3]);
        }
        else
        {
            LS_THROW(0, FormatException, "Incorrect number of parameters");
        }

        return stm;
    }
    /// @brief Parse string as vector
    template <class Stream, class T, uint N>
    Stream& operator >>(Stream& stm, Vector<T, N>& value)
    {
        static const auto MaxIgnore = numeric_limits<std::streamsize>::max();
        for (uint idx = 0; idx < N; ++idx)
        {
            const bool success = !!(stm >> value[idx]);
            LS_THROW(success, FormatException, "Cannot read #", idx, " element");
            if (idx + 1 != N)
            {
                stm.ignore(MaxIgnore, ',');
            }
        }
        return stm;
    }
    /// @}

}