#include "pch.h"
#include "ls/asset/EffectLibraryAsset.h"
#include "ls/io/FileSystemInterface.h"
#include "ls/gapi/Renderer.h"
#include "ls/gapi/usl/usl.h"

using namespace ls;
EffectLibraryAsset::EffectLibraryAsset()
{
}
EffectLibraryAsset::~EffectLibraryAsset()
{

}


// Implement Asset
string EffectLibraryAsset::extension() const
{
    return ".cxx";
}
void EffectLibraryAsset::doSerialize(Archive<Buffer> archive)
{
    if (archive.isWrite())
    {
        archive.stream().writeText(m_text);
    }
    else
    {
        archive.stream().readText(m_text);
    }
}
void EffectLibraryAsset::doRestore()
{
    if (renderer())
    {
        hamap<string, string> includes;
        try
        {
            usl::extractRegionsFromSource(m_text, includes);
        }
        catch (const FormatException& ex)
        {
            LS_THROW(0, IoException, ex.what());
        }
        for (const auto& item : includes)
        {
            renderer()->addEffectLibraryChunk(item.first, item.second);
        }
    }
}
