/// @file ls/asset/serialize/BipedWalkAnimationSerializer.h
/// @brief Biped Walk Animation Serializer
#pragma once

#include "ls/asset/AssetManagerOld.h"
#include "ls/asset/serializer/NodeAnimationSerializer.h"

#include "ls/ik/BipedSkeleton.h"
#include "ls/ik/BipedWalkAnimation.h"
#include "ls/ik/BipedWalkAnalyzer.h"
#include "ls/geom/NodeHierarchy.h"
#include "ls/import/model_import.h"

using namespace ls;

namespace ls
{
    /// @addtogroup LsAsset
    /// @{

    /// @brief Biped Walk Animation Serializer
    template <>
    struct AssetSerializer<BipedWalkAnimation> 
        : AssetSerializerBase<BipedWalkAnimation, shared_ptr<BipedWalkAnimation>>
    {
        /// @brief Ctor
        AssetSerializer() = delete;
        /// @brief Ctor
        using AssetSerializerBase::AssetSerializerBase;

        /// @brief Type name
        const string type = "BipedWalkAnimation";
        /// @brief Folder name
        const string folder = "cinematics/biped_walk";
        /// @brief File extension
        const string ext = ".xml";

        /// @brief Write asset to XML
        static void writeToXml(XmlNode dest, const Asset& asset)
        {
            using namespace pugi;

            xml_node animationNode = dest.append_child("animation");

            // Write info
            xml_node infoNode = animationNode.append_child("info");
            infoNode.append_attribute("skeleton").set_value(asset.biped().name().c_str());
            infoNode.append_attribute("unit_period").set_value(asset.unitPeriod());
            infoNode.append_attribute("base_phase").set_value(asset.basePhase());

            // Write pelvis animation
            auto& pelvisAnimation = asset.pelvis();
            xml_node pelvisNode = animationNode.append_child("pelvis");
            for (auto& pose : pelvisAnimation.track)
            {
                xml_node keyNode = pelvisNode.append_child("key");
                keyNode.append_attribute("x").set_value(pose.x);
                keyNode.append_attribute("y").set_value(pose.y);
                keyNode.append_attribute("z").set_value(pose.z);
            }

            // Write feet animation
            for (uint footIndex = BipedSkeleton::LeftFoot;
            footIndex < BipedSkeleton::NumFeet;
                ++footIndex)
            {
                auto& footAnimation = asset.foot((BipedSkeleton::FootIndex) footIndex);
                xml_node footNode = animationNode.append_child("foot");
                footNode.append_attribute("phase").set_value(footAnimation.phase);
                for (auto& pose : footAnimation.track)
                {
                    xml_node keyNode = footNode.append_child("key");
                    keyNode.append_attribute("toe_x").set_value(pose.toePosition.x);
                    keyNode.append_attribute("toe_y").set_value(pose.toePosition.y);
                    keyNode.append_attribute("toe_z").set_value(pose.toePosition.z);
                    keyNode.append_attribute("knee_rot").set_value(pose.kneeRotation);
                    keyNode.append_attribute("t2f_x").set_value(pose.toe2foot.x);
                    keyNode.append_attribute("t2f_y").set_value(pose.toe2foot.y);
                    keyNode.append_attribute("t2f_z").set_value(pose.toe2foot.z);
                }
            }

            // Write spine animation
            if (asset.hasSpine())
            {
                auto& spineAnimation = asset.spine();
                xml_node spineNode = animationNode.append_child("spine");
                AssetSerializer<NodeAnimation>::writeToXml(spineNode, spineAnimation);
            }

            // Write hands animation
            if (asset.hasHands())
            {
                auto& handsAnimation = asset.hands();
                xml_node handsNode = animationNode.append_child("hands");
                AssetSerializer<NodeAnimation>::writeToXml(handsNode, handsAnimation);
            }
        }
        /// @brief Write asset to stream
        void write(const string& name, Handle asset)
        {
            // Open
            Stream dest = LS_OPEN_OUTPUT_ASSET_FILE(name);

            // Serialize
            pugi::xml_document doc;
            writeToXml(doc, *asset);

            // Save
            auto writer = XmlWriter(&*dest);
            doc.save(writer);
        }
        /// @brief Read asset from stream
        static Handle readFromXml(AssetManagerOld& manager, XmlNode source)
        {
            using namespace pugi;

            xml_node animationNode = loadNode(source, "animation");
            string name = loadAttrib(animationNode, "name").as_string();

            // Read info
            xml_node infoNode = loadNode(animationNode, "info");
            string skeletonName = loadAttrib(infoNode, "skeleton").as_string();
            float unitPeriod = loadAttrib(infoNode, "unit_period").as_float();
            float basePhase = loadAttrib(infoNode, "base_phase").as_float();

            // Read pelvis
            xml_node pelvisNode = loadNode(animationNode, "pelvis");
            BipedWalkAnimation::Pelvis pelvisAnimation;
            for (auto& keyNode : pelvisNode.children("key"))
            {
                float x = loadAttrib(keyNode, "x").as_float();
                float y = loadAttrib(keyNode, "y").as_float();
                float z = loadAttrib(keyNode, "z").as_float();
                pelvisAnimation.track.emplace_back(x, y, z);
            }

            // Read feet
            BipedWalkAnimation::Foot feetAnimation[BipedSkeleton::NumFeet];
            uint footIndex = BipedSkeleton::LeftFoot;
            for (auto& footNode : animationNode.children("foot"))
            {
                LS_THROW(footIndex < BipedSkeleton::NumFeet,
                         IoException,
                         "Too many feet");

                auto& footAnimation = feetAnimation[footIndex];
                footAnimation.phase = loadAttrib(footNode, "phase").as_float();
                for (auto& keyNode : footNode.children("key"))
                {
                    BipedWalkAnimation::FootPose pose;
                    pose.toePosition.x = loadAttrib(keyNode, "toe_x").as_float();
                    pose.toePosition.y = loadAttrib(keyNode, "toe_y").as_float();
                    pose.toePosition.z = loadAttrib(keyNode, "toe_z").as_float();
                    pose.kneeRotation = loadAttrib(keyNode, "knee_rot").as_float();
                    pose.toe2foot.x = loadAttrib(keyNode, "t2f_x").as_float();
                    pose.toe2foot.y = loadAttrib(keyNode, "t2f_y").as_float();
                    pose.toe2foot.z = loadAttrib(keyNode, "t2f_z").as_float();
                    footAnimation.track.push_back(pose);
                }
                ++footIndex;
            }
            LS_THROW(footIndex == BipedSkeleton::NumFeet,
                     IoException,
                     "Too few feet");

            // Read spine
            xml_node spineNode = animationNode.child("spine");
            shared_ptr<NodeAnimation> spineAnimation;
            if (!spineNode.empty())
            {
                spineAnimation = AssetSerializer<NodeAnimation>::readFromXml(manager, spineNode);
            }

            // Read hands
            xml_node handsNode = animationNode.child("hands");
            shared_ptr<NodeAnimation> handsAnimation;
            if (!handsNode.empty())
            {
                handsAnimation = AssetSerializer<NodeAnimation>::readFromXml(manager, handsNode);
            }

            // Load
            auto skeleton = manager.get<Skeleton>(skeletonName);
            LS_THROW(skeleton,
                     IoException,
                     "Skeleton [", skeletonName, "] is not found");
            auto asset = make_shared<Asset>(
                skeleton,
                unitPeriod,
                basePhase,
                pelvisAnimation,
                feetAnimation[BipedSkeleton::LeftFoot],
                feetAnimation[BipedSkeleton::RightFoot],
                spineAnimation,
                handsAnimation);

            return asset;
        }
        /// @brief Read asset from stream
        Handle read(const string& name)
        {
            Stream src = LS_OPEN_INPUT_ASSET_FILE(name);
            auto document = readXml(*src);
            Handle asset = readFromXml(manager, *document);

            return asset;
        }
        /// @brief Import asset
        Handle import(const string& name)
        {
            using namespace pugi;

            // Try to open
            string importFileName = folder + "/.src/" + name + ".xml";
            Stream importFile = manager.fileSystem().openStream(importFileName);
            LS_THROW(importFile,
                     IoException,
                     "File [", importFileName, "] is not found");

            // Try to parse
            auto xmlDocument = readXml(*importFile);

            // Parse
            xml_node rootNode = loadNode(*xmlDocument, "biped_walk");
            string skeletonName = loadNodeAttrib(rootNode, "skeleton", "name").as_string();
            xml_node sourceNode = loadNode(rootNode, "source");
            string animationFileName = loadAttrib(sourceNode, "file").as_string();
            string markingFileName = loadAttrib(sourceNode, "marking").as_string();
            string animationName = loadAttrib(sourceNode, "animation").as_string();
            string animationRootName = loadAttrib(sourceNode, "root").as_string();
            xml_node analyzeNode = loadNode(rootNode, "analyze");

            // Load skeleton
            auto skeleton = manager.get<Skeleton>(skeletonName);
            LS_THROW(skeleton,
                     IoException,
                     "Skeleton [", skeletonName, "] is not found");
            auto bipedSkeleton = skeleton->isBiped();
            LS_THROW(bipedSkeleton,
                     IoException,
                     "Skeleton [", skeletonName, "] must be biped");

            // Load marking
            string rootName = "root";
            map<string, string> name2bone, bone2name;
            if (!markingFileName.empty())
            {
                Stream markingFile = manager.fileSystem().openStream(markingFileName);
                LS_THROW(markingFile,
                         IoException,
                         "Marking file [", markingFileName, "] is not found");
                auto markingDocument = readXml(*markingFile);

                name2bone = loadMap(loadNode(*markingDocument, "map"));
                bone2name = loadInverseMap(loadNode(*markingDocument, "map"));

                auto iterPelvisNode = findOrDefault(name2bone, "root");
                if (iterPelvisNode)
                    rootName = *iterPelvisNode;
            }

            // Load animation
            Stream animationFile = manager.fileSystem().openStream(animationFileName);
            LS_THROW(animationFile,
                     IoException,
                     "Animation file [", animationFileName, "] is not found");

            auto importer = importModel(*animationFile);
            auto hierarchy = importer->loadHierarchy(rootName, true);
            LS_THROW(hierarchy,
                     IoException,
                     "Cannot load hierarchy rooted from [", rootName, "] node");
            auto animation = importer->loadAnimation(*hierarchy,
                                                     animationName,
                                                     animationRootName);
            LS_THROW(animation,
                     IoException,
                     "Cannot load animation [", animationName, "] rooted from [", animationRootName, "] node");

            // Rename hierarchy nodes
            hierarchy->rename(bone2name);

            // Analyze and get
            BipedWalkAnalyzer::Parameters param;
            bool cleverAnalyzis = loadAttrib(analyzeNode, "clever").as_bool();
            if (cleverAnalyzis)
            {
                param = BipedWalkAnalyzer::Parameters{
                    loadAttrib(analyzeNode, "animation_begin").as_double(),
                    loadAttrib(analyzeNode, "animation_end").as_double(),
                    asRange(readVector<float>(
                        loadAttrib(analyzeNode, "move_direction").as_string())),
                    loadAttrib(analyzeNode, "recovery_segment_duration").as_double(),
                    loadAttrib(analyzeNode, "movement_threshold").as_float(),
                    loadAttrib(analyzeNode, "footstep_weld_distance").as_float()
                };
            }
            else
            {
                param = BipedWalkAnalyzer::Parameters{
                    loadAttrib(analyzeNode, "animation_begin").as_double(),
                    loadAttrib(analyzeNode, "animation_end").as_double(),
                    asRange(readVector<float>(
                        loadAttrib(analyzeNode, "move_direction").as_string())),
                    loadAttrib(analyzeNode, "recovery_segment_duration").as_double(),
                    loadAttrib(analyzeNode, "base_phase").as_double(),
                    loadAttrib(analyzeNode, "weld_time").as_double(),
                };
            }
            auto analyzer = make_shared<BipedWalkAnalyzer>(skeleton, *animation, param);
            auto analyzed = analyzer->result();
            analyzed->debugInit(analyzer);

            return analyzed;
        }
    };
    /// @}
}
