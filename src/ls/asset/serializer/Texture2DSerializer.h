/// @file ls/asset/serialize/Texture2DSerializer.h
/// @brief Texture 2D Serializer
#pragma once

#include "ls/asset/AssetManagerOld.h"
#include "ls/asset/AssetStorage.h"

#include "ls/render/Texture2D.h"

using namespace ls;

namespace ls
{
    /// @addtogroup LsAsset
    /// @{

    /// @brief Texture 2D Serializer
    template <>
    struct AssetSerializer<Texture2D> 
        : AssetSerializerBase<Texture2D, shared_ptr<Texture2D>>
    {
        /// @brief Ctor
        AssetSerializer() = delete;
        /// @brief Ctor
        using AssetSerializerBase::AssetSerializerBase;

        /// @brief Type name
        const string type = "Texture2D";
        /// @brief Folder name
        const string folder = "graphical/texture";
        /// @brief File extension
        const string ext = ".dds";

        /// @brief Write asset to stream
        void write(const string& name, Handle asset)
        {
            Stream dest = LS_OPEN_OUTPUT_ASSET_FILE(name);
            asset->serialize(*dest, Compression::No);
        }
        /// @brief Read asset from stream
        Handle read(const string& name)
        {
            Stream src = LS_OPEN_INPUT_ASSET_FILE(name);
            Handle asset = make_shared<Asset>();
            asset->deserialize(*src);
            return asset;
        }
        /// @brief Import asset
        Handle import(string const& /*name*/)
        {
            return nullptr;
        }
    };
    /// @}
}