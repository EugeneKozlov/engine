/// @file ls/asset/serialize/GraphicalMaterialSerializer.h
/// @brief Graphical Material Serializer
#pragma once

#include "ls/asset/AssetManagerOld.h"
#include "ls/core/INIReader.h"

#include "ls/scene/graphical/GraphicalMaterial.h"

using namespace ls;

namespace ls
{
    /// @addtogroup LsAsset
    /// @{

    /// @brief Graphical Material Serializer
    template <>
    struct AssetSerializer<Material>
        : AssetSerializerBase<Material, shared_ptr<Material>>
    {
        /// @brief Ctor
        AssetSerializer() = delete;
        /// @brief Ctor
        using AssetSerializerBase::AssetSerializerBase;

        /// @brief Type name
        const string type = "GraphicalMaterial";
        /// @brief Folder name
        const string folder = "graphical/material";
        /// @brief File extension
        const string ext = ".ini";

        /// @brief Write asset to stream
        void write(string const& /*name*/, Handle /*asset*/)
        {
            LS_THROW(0, NotImplementedException);
        }
        /// @brief Read asset from INI
        Handle readFromINI(const string& name)
        {
            // Check section
            LS_THROW(m_config.hasSection(name),
                     ArgumentException,
                     "Section [", name, "] is not found in INI files from [", folder, "]");
            const INISection& section = m_config.section(name);

            // Create asset
            string surfaceShader = getOrThrow<IoException>(
                section, "shader", "Variable [shader] is missing");
            Handle asset = make_shared<Material>(manager, surfaceShader);

            // Load vars
            asset->isTransparent = fromString<bool>(getOrDefault(section, "transparent", "0"));
            asset->isTranslucent = fromString<bool>(getOrDefault(section, "translucent", "0"));
            asset->isWrapped = fromString<bool>(getOrDefault(section, "wrapped", "0"));

            for (auto& var : section)
            {
                // Process vars
                const string& variable = var.first;
                const string& value = var.second;
                if (variable.size() < 4 || variable[1] != 's' || variable[2] != '.')
                    continue;

                // Read shader
                ShaderType shader = ShaderType::COUNT;
                switch (variable[0])
                {
                case 'v': shader = ShaderType::Vertex; break;
                case 'h': shader = ShaderType::TessControl; break;
                case 'd': shader = ShaderType::TessEval; break;
                case 'g': shader = ShaderType::Geometry; break;
                case 'p': shader = ShaderType::Pixel; break;
                default:
                    LS_THROW(0, IoException,
                             "Unknown shader type required by variable [", variable, "]");
                    break;
                }

                // Read slot
                uint slot = fromString<uint>(variable.substr(3));

                bool added = asset->internalAddTexture(shader, slot, value);
                LS_THROW(added, IoException, "Too many textures");
            }
            return asset;
        }
        /// @brief Read asset from stream
        Handle read(const string& name)
        {
            if (!m_initialized)
            {
                FileSystem fs = fileSystem.openSystem(folder);
                fs->iterateItems([this](StreamInterface& src)
                {
                    m_config.parse(src.text());
                    return true;
                });
            }

            return readFromINI(name);
        }
        /// @brief Import asset
        Handle import(string const& /*name*/)
        {
            return nullptr;
        }
    private:
        bool m_initialized = false;
        INIReader m_config;
    };
    /// @}
}
