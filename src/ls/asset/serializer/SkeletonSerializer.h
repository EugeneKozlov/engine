/// @file ls/asset/serialize/SkeletonSerializer.h
/// @brief Skeleton Serializer
#pragma once

#include "ls/asset/AssetManagerOld.h"

#include "ls/ik/BipedSkeleton.h"
#include "ls/geom/NodeHierarchy.h"
#include "ls/import/model_import.h"

using namespace ls;

namespace ls
{
    /// @addtogroup LsAsset
    /// @{

    /// @brief Skeleton Serializer
    template <>
    struct AssetSerializer<Skeleton> 
        : AssetSerializerBase<Skeleton, shared_ptr<Skeleton>>
    {
        /// @brief Ctor
        AssetSerializer() = delete;
        /// @brief Ctor
        using AssetSerializerBase::AssetSerializerBase;

        /// @brief Type name
        const string type = "Skeleton";
        /// @brief Folder name
        const string folder = "skeleton";
        /// @brief File extension
        const string ext = ".xml";

        /// @brief Write asset to XML
        static void writeToXml(XmlNode dest, const Asset& asset)
        {
            using namespace pugi;

            xml_node skeletonNode = dest.append_child("skeleton");

            // Write info
            xml_node infoNode = skeletonNode.append_child("info");

            string type = "unspecified";
            if (asset.isBiped())
                type = "biped";
            infoNode.append_attribute("type").set_value(type.c_str());

            // Write bones
            xml_node bonesNode = skeletonNode.append_child("bones");
            const NodeHierarchy& hierarchy = asset.hierarchy();
            for (GeometryNode const& node : hierarchy)
            {
                xml_node bodeNode = bonesNode.append_child("bone");
                bodeNode.append_attribute("index")
                    .set_value(node.index);
                bodeNode.append_attribute("parent")
                    .set_value(node.parent->index);
                bodeNode.append_attribute("name")
                    .set_value(node.name.c_str());
                bodeNode.append_attribute("local")
                    .set_value(writeMatrix(node.local.matrix()).c_str());
            }
        }
        /// @brief Write asset to stream
        void write(const string& name, Handle asset)
        {
            // Open
            Stream dest = LS_OPEN_OUTPUT_ASSET_FILE(name);

            // Serialize
            pugi::xml_document doc;
            writeToXml(doc, *asset);

            // Save
            auto writer = XmlWriter(&*dest);
            doc.save(writer);
        }
        /// @brief Read asset from stream
        static Handle readFromXml(AssetManagerOld& /*manager*/, XmlNode source)
        {
            using namespace pugi;

            xml_node skeletonNode = loadNode(source, "skeleton");
            string name = loadAttrib(skeletonNode, "name").as_string();

            // Load tags
            xml_node infoNode = loadNode(skeletonNode, "info");
            string type = loadAttrib(infoNode, "type").as_string();

            // Load hierarchy
            auto hierarchy = make_unique<NodeHierarchy>();
            xml_node bonesNode = loadNode(skeletonNode, "bones");
            uint testIndex = 0;
            for (auto& boneNode : bonesNode.children())
            {
                uint index = loadAttrib(boneNode, "index").as_uint();
                uint parent = loadAttrib(boneNode, "parent").as_int();
                string boneName = loadAttrib(boneNode, "name").as_string();
                float4x4 local = readMatrix(loadAttrib(boneNode, "local").as_string());

                LS_THROW(index == testIndex,
                         IoException,
                         "Out-of-order index [", index, "]");

                hierarchy->addNode(boneName.c_str(), local, parent);
                ++testIndex;
            }

            // Init skeleton
            Handle asset = Skeleton::construct(std::move(hierarchy),
                                               Skeleton::type(type));

            return asset;
        }
        /// @brief Read asset from stream
        Handle read(const string& name)
        {
            Stream src = LS_OPEN_INPUT_ASSET_FILE(name);
            auto document = readXml(*src);
            Handle asset = readFromXml(manager, *document);

            return asset;
        }
        /// @brief Import asset
        Handle import(const string& name)
        {
            using namespace pugi;

            // Try to open
            string importFileName = folder + "/.src/" + name + ".xml";
            Stream importFile = fileSystem.openStream(importFileName);
            LS_THROW(importFile,
                     FileSystemException,
                     "File [", importFileName, "] is not found");

            // Try to parse
            auto xmlDocument = readXml(*importFile);

            // Parse
            xml_node rootNode = loadNode(*xmlDocument, "skeleton");
            xml_node sourceNode = loadNode(rootNode, "source");
            string geometryFileName = loadAttrib(sourceNode, "file").as_string();
            string markingFileName = loadAttrib(sourceNode, "marking").as_string();
            float4x4 fixMatrix = readMatrix(loadNodeAttrib(rootNode, "fix_pose", "matrix").as_string());
            string skeletonTypeName = loadNodeAttrib(rootNode, "type", "value").as_string();
            Skeleton::Type skeletonType = Skeleton::type(skeletonTypeName);

            // Load marking
            string rootName = "root";
            map<string, string> name2bone, bone2name;
            if (!markingFileName.empty())
            {
                Stream markingFile = fileSystem.openStream(markingFileName);
                LS_THROW(markingFile,
                         IoException,
                         "Marking file [", markingFileName, "] is not found");
                auto markingDocument = readXml(*markingFile);

                name2bone = loadMap(loadNode(*markingDocument, "map"));
                bone2name = loadInverseMap(loadNode(*markingDocument, "map"));

                auto iterPelvisNode = findOrDefault(name2bone, "root");
                if (iterPelvisNode)
                    rootName = *iterPelvisNode;
            }

            // Import geometry
            Stream geometryFile = fileSystem.openStream(geometryFileName);
            LS_THROW(geometryFile,
                     IoException,
                     "Geometry file [", geometryFileName, "] is not found");
            auto geometryImporter = importModel(*geometryFile);
            auto geometryHierarchy = geometryImporter->loadHierarchy(rootName, true);
            LS_THROW(geometryHierarchy,
                     IoException,
                     "Cannot load hierarchy rooted from [", rootName, "] node");

            // Read hierarchy
            auto skeletonHierarchy = make_unique<NodeHierarchy>();
            for (uint iterNode = 0; iterNode < geometryHierarchy->numNodes(); ++iterNode)
            {
                auto& geometryNode = geometryHierarchy->getNode(iterNode);
                string internalName = geometryNode.name;

                // Get local matrix
                float4x4 localMatrix = geometryNode.local.matrix();
                if (internalName == rootName)
                {
                    localMatrix *= fixMatrix;
                }

                // Add node
                skeletonHierarchy->addNode(internalName.c_str(),
                                           localMatrix,
                                           geometryNode.parent->index);
            }
            skeletonHierarchy->rename(bone2name);

            // Construct skeleton
            auto skeleton = Skeleton::construct(std::move(skeletonHierarchy),
                                                skeletonType);

            return skeleton;
        }
    };
    /// @}
}
