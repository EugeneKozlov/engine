/// @file ls/asset/serialize/GraphicalModelSerializer.h
/// @brief Graphical Model Serializer
#pragma once

#include "ls/asset/AssetManagerOld.h"
#include "ls/core/INIReader.h"

#include "ls/scene/graphical/GraphicalModel.h"

using namespace ls;

namespace ls
{
    /// @addtogroup LsAsset
    /// @{

    /// @brief Graphical Model Serializer
    template <>
    struct AssetSerializer<GraphicalModel>
        : AssetSerializerBase<GraphicalModel, shared_ptr<GraphicalModel>>
    {
        /// @brief Ctor
        AssetSerializer() = delete;
        /// @brief Ctor
        using AssetSerializerBase::AssetSerializerBase;

        /// @brief Type name
        const string type = "GraphicalModel";
        /// @brief Folder name
        const string folder = "graphical/model";
        /// @brief File extension
        const string ext = ".ini";

        /// @brief Write asset to stream
        void write(string const& /*name*/, Handle /*asset*/)
        {
            LS_THROW(0, NotImplementedException);
        }
        /// @brief Read asset from INI
        Handle readFromINI(const string& name)
        {
            Handle asset = make_shared<GraphicalModel>(manager);

            bool stop = false;
            for (uint lodIndex = 0; lodIndex < MaxModelLods; ++lodIndex)
            {
                // Find section
                string sectionName = name + "." + toString(lodIndex);
                bool isLoaded = m_config.hasSection(sectionName);
                if (!isLoaded)
                {
                    LS_THROW(lodIndex != 0, IoException, "First LOD is not specified");
                    stop = true;
                    continue;
                }
                LS_THROW(!stop, IoException, "LOD #", lodIndex - 1, " is missed");
                const INISection& section = m_config.section(sectionName);

                // Read variables
                GraphicalModel::Lod desc;
                desc.fadeIn = fromString<double>(getOrThrow<IoException>(
                    section, "fade_in", "Variable [fade_in] is not found in section [", sectionName, "]"));
                desc.fadeOut = fromString<double>(getOrThrow<IoException>(
                    section, "fade_out", "Variable [fade_out] is not found in section [", sectionName, "]"));
                string meshName = getOrThrow<IoException>(
                    section, "mesh", "Variable [mesh] is not found in section [", sectionName, "]");

                // Read materials
                uint numSubsets = 0;
                GraphicalModelMaterialsArray materials;
                for (uint subsetIndex = 0; subsetIndex < MaxModelSubsets; ++subsetIndex)
                {
                    string materialName = getOrDefault(section, "material." + toString(subsetIndex));
                    if (materialName.empty())
                        continue;

                    auto material = manager.get<Material>(materialName);
                    LS_THROW(!!material, IoException,
                             "Material [", materialName, "] "
                             "of subset #", subsetIndex, " "
                             "of LOD #", lodIndex, " is not found");
                    materials[subsetIndex] = material;
                    numSubsets = max(numSubsets, subsetIndex + 1);
                }

                // Save
                asset->addLod(desc, meshName, numSubsets, materials);
            }

            return asset;
        }
        /// @brief Read asset from stream
        Handle read(const string& name)
        {
            if (!m_initialized)
            {
                FileSystem fs = fileSystem.openSystem(folder);
                fs->iterateItems([this](StreamInterface& src)
                {
                    m_config.parse(src.text());
                    return true;
                });
            }

            return readFromINI(name);
        }
        /// @brief Import asset
        Handle import(string const& /*name*/)
        {
            return nullptr;
        }
    private:
        bool m_initialized = false;
        INIReader m_config;
    };
    /// @}
}
