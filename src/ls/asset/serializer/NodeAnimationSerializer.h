/// @file ls/asset/serialize/NodeAnimationSerializer.h
/// @brief Node Animation Serializer
#pragma once

#include "ls/asset/AssetManagerOld.h"

#include "ls/geom/GeometryNode.h"
#include "ls/geom/NodeAnimation.h"
#include "ls/import/model_import.h"

using namespace ls;

namespace ls
{
    /// @addtogroup LsAsset
    /// @{

    /// @brief Node Animation Serializer
    template <>
    struct AssetSerializer<NodeAnimation> 
        : AssetSerializerBase<NodeAnimation, shared_ptr<NodeAnimation>>
    {
        /// @brief Ctor
        AssetSerializer() = delete;
        /// @brief Ctor
        using AssetSerializerBase::AssetSerializerBase;

        /// @brief Type name
        const string type = "NodeAnimation";
        /// @brief Folder name
        const string folder = "animation/skeleton";
        /// @brief File extension
        const string ext = ".xml";

        /// @brief Write asset to XML
        static void writeToXml(XmlNode dest, const Asset& asset)
        {
            using namespace pugi;

            xml_node animationNode = dest.append_child("animation");

            uint numNodes = asset.numNodes();
            bool isGlobal = asset.isGlobal();

            // Write tags
            xml_node infoNode = animationNode.append_child("info");
            infoNode.append_attribute("num_nodes").set_value(numNodes);
            infoNode.append_attribute("global").set_value(isGlobal);
            infoNode.append_attribute("duration").set_value(asset.duration());
            infoNode.append_attribute("step").set_value(asset.step());

            // Write bones
            xml_node keysNode = animationNode.append_child("keys");
            for (auto& key : asset)
            {
                xml_node keyNode = keysNode.append_child("key");
                keyNode.append_attribute("time").set_value(key.first);
                for (uint nodeIndex = 0; nodeIndex < numNodes; ++nodeIndex)
                {
                    if (!asset.isAnimated(nodeIndex))
                        continue;
                    auto& node = key.second[nodeIndex];
                    xml_node nodeNode = keyNode.append_child("node");
                    nodeNode.append_attribute("index")
                        .set_value(nodeIndex);
                    nodeNode.append_attribute("local")
                        .set_value(writeMatrix(node.local.matrix()).c_str());
                    if (isGlobal)
                    {
                        nodeNode.append_attribute("global")
                            .set_value(writeMatrix(node.global.matrix()).c_str());
                    }
                }
            }
        }
        /// @brief Write asset to stream
        void write(const string& name, Handle asset)
        {
            // Open
            Stream dest = LS_OPEN_OUTPUT_ASSET_FILE(name);

            // Serialize
            pugi::xml_document doc;
            writeToXml(doc, *asset);

            // Save
            auto writer = XmlWriter(&*dest);
            doc.save(writer);
        }
        /// @brief Read asset from stream
        static Handle readFromXml(AssetManagerOld& /*manager*/, XmlNode source)
        {
            using namespace pugi;

            xml_node animationNode = loadNode(source, "animation");

            // Load info
            xml_node infoNode = loadNode(animationNode, "info");
            uint numNodes = loadAttrib(infoNode, "num_nodes").as_uint();
            bool isGlobal = loadAttrib(infoNode, "global").as_bool();

            // Init
            Handle asset = make_shared<Asset>(numNodes);

            // Keys
            xml_node keysNode = loadNode(animationNode, "keys");
            for (auto& keyNode : keysNode.children())
            {
                double keyTime = loadAttrib(keyNode, "time").as_double();
                for (auto& nodeNode : keyNode.children())
                {
                    uint index = loadAttrib(nodeNode, "index").as_uint();
                    float4x4 local = readMatrix(loadAttrib(nodeNode, "local").as_string());
                    if (isGlobal)
                    {
                        float4x4 global = readMatrix(loadAttrib(nodeNode, "global").as_string());
                        asset->addKey(keyTime, index, Transform(local), Transform( global ));
                    }
                    else
                    {
                        asset->addKey(keyTime, index, Transform(local), none);
                    }
                }
            }

            return asset;
        }
        /// @brief Read asset from stream
        Handle read(const string& name)
        {
            Stream src = LS_OPEN_INPUT_ASSET_FILE(name);
            auto document = readXml(*src);
            Handle asset = readFromXml(manager, *document);

            return asset;
        }
        /// @brief Import asset
        Handle import(const string& name)
        {
            using namespace pugi;

            // Try to open
            string importFileName = folder + "/.src/" + name + ".xml";
            Stream importFile = fileSystem.openStream(importFileName);
            LS_THROW(importFile,
                     FileSystemException,
                     "File [", importFileName, "] is not found");

            // Try to parse
            auto xmlDocument = readXml(*importFile);

            // Parse
            xml_node rootNode = loadNode(*xmlDocument, "node_anim");
            xml_node sourceNode = loadNode(rootNode, "source");
            string animationFileName = loadAttrib(sourceNode, "file").as_string();
            string animationName = loadAttrib(sourceNode, "animation").as_string();
            string skeletonRootName = loadAttrib(sourceNode, "skeleton_root").as_string();
            string animationRootName = loadAttrib(sourceNode, "animation_root").as_string();

            // Import animation
            Stream animationFile = fileSystem.openStream(animationFileName);
            LS_THROW(animationFile,
                     IoException,
                     "Animation file [", animationFileName, "] is not found");
            auto animationImporter = importModel(*animationFile);
            auto hierarchy = animationImporter->loadHierarchy(skeletonRootName, true);
            LS_THROW(hierarchy,
                     IoException,
                     "Cannot load hierarchy rooted from [", skeletonRootName, "] node");
            auto animation = animationImporter->loadAnimation(*hierarchy,
                                                              animationName,
                                                              animationRootName);
            LS_THROW(animation,
                     IoException,
                     "Cannot load animation [", animationName, "] "
                     "rooted from [", animationRootName, "] node");

            // Finalize

            return animation;
        }
    };
    /// @}
}