/// @file ls/asset/serialize/MeshSerializer.h
/// @brief Mesh Serializer
#pragma once

#include "ls/asset/AssetManagerOld.h"

#include "ls/render/Mesh.h"
#include "ls/import/model_import.h"

using namespace ls;

namespace ls
{
    /// @addtogroup LsAsset
    /// @{

    /// @brief Mesh Serializer
    template <>
    struct AssetSerializer<Mesh> 
        : AssetSerializerBase<Mesh, shared_ptr<Mesh>>
    {
        /// @brief Ctor
        AssetSerializer() = delete;
        /// @brief Ctor
        using AssetSerializerBase::AssetSerializerBase;

        /// @brief Type name
        const string type = "Mesh";
        /// @brief Folder name
        const string folder = "graphical/mesh";
        /// @brief File extension
        const string ext = ".bin";

        /// @brief Write asset to stream
        void write(const string& name, Handle asset)
        {
            Stream dest = LS_OPEN_OUTPUT_ASSET_FILE(name);
            asset->serialize(*dest);
        }
        /// @brief Read asset from stream
        Handle read(const string& name)
        {
            Stream src = LS_OPEN_INPUT_ASSET_FILE(name);
            Handle asset = make_shared<Asset>();
            asset->deserialize(*src);
            return asset;
        }
        /// @brief Import asset
        Handle import(string const& /*name*/)
        {
            return nullptr;
        }
    };
    /// @}
}
