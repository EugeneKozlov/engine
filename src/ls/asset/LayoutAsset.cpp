#include "pch.h"
#include "ls/asset/LayoutAsset.h"

#include "ls/core/INIReader.h"
#include "ls/io/FileSystemInterface.h"
#include "ls/gapi/Renderer.h"

using namespace ls;
namespace
{
    Format formatFromString(const string& name)
    {
        static map<string, Format> m = {
            make_pair("Int8", Format::R8_INT),
            make_pair("Uint8", Format::R8_UINT),
            make_pair("Norm8", Format::R8_SNORM),
            make_pair("Unorm8", Format::R8_UNORM),
            make_pair("Int8x2", Format::R8G8_INT),
            make_pair("Uint8x2", Format::R8G8_UINT),
            make_pair("Norm8x2", Format::R8G8_SNORM),
            make_pair("Unorm8x2", Format::R8G8_UNORM),
            make_pair("Int8x4", Format::R8G8B8A8_INT),
            make_pair("Uint8x4", Format::R8G8B8A8_UINT),
            make_pair("Norm8x4", Format::R8G8B8A8_SNORM),
            make_pair("Unorm8x4", Format::R8G8B8A8_UNORM),
            make_pair("Int16", Format::R16_INT),
            make_pair("Uint16", Format::R16_UINT),
            make_pair("Norm16", Format::R16_SNORM),
            make_pair("Unorm16", Format::R16_UNORM),
            make_pair("Float16", Format::R16_FLOAT),
            make_pair("Int16x2", Format::R16G16_INT),
            make_pair("Uint16x2", Format::R16G16_UINT),
            make_pair("Norm16x2", Format::R16G16_SNORM),
            make_pair("Unorm16x2", Format::R16G16_UNORM),
            make_pair("Float16x2", Format::R16G16_FLOAT),
            make_pair("Int16x4", Format::R16G16B16A16_INT),
            make_pair("Uint16x4", Format::R16G16B16A16_UINT),
            make_pair("Norm16x4", Format::R16G16B16A16_SNORM),
            make_pair("Unorm16x4", Format::R16G16B16A16_UNORM),
            make_pair("Float16x4", Format::R16G16B16A16_FLOAT),
            make_pair("Int32", Format::R32_INT),
            make_pair("Uint32", Format::R32_UINT),
            make_pair("Float32", Format::R32_FLOAT),
            make_pair("Int32x2", Format::R32G32_INT),
            make_pair("Uint32x2", Format::R32G32_UINT),
            make_pair("Float32x2", Format::R32G32_FLOAT),
            make_pair("Int32x3", Format::R32G32B32_INT),
            make_pair("Uint32x3", Format::R32G32B32_UINT),
            make_pair("Float32x3", Format::R32G32B32_FLOAT),
            make_pair("Int32x4", Format::R32G32B32A32_INT),
            make_pair("Uint32x4", Format::R32G32B32A32_UINT),
            make_pair("Float32x4", Format::R32G32B32A32_FLOAT),
        };
        return getOrDefault(m, name, Format::Unknown);
    }
}
LayoutAsset::LayoutAsset()
{
}
LayoutAsset::~LayoutAsset()
{

}


// Implement Asset
void LayoutAsset::doReadConfig(INIReader& ini)
{
    // #TODO Rewrite
    const INISection& header = ini.section("");

    // Read layout
    m_format.clear();
    for (auto& item : header)
    {
        const string name = chop(item.first);
        const string value = chop(item.second);
        const vector<string> arg = split(value, ',');
        if (name[0] == '$')
            continue;
        const bool isInstanced = name[0] == '*';
        LS_THROW(arg.size() >= 0, FormatException, "Bad layout string");

        VertexFormat fmt;
        fmt.name = !isInstanced ? name : name.substr(1);
        fmt.stream = fromString<uint>(chop(arg[0]));
        fmt.offset = fromString<uint>(chop(arg[1]));
        fmt.type = formatFromString(chop(arg[2]));
        fmt.isInstanced = isInstanced;
        m_format.push_back(fmt);
    }

    // Process
    std::sort(m_format.begin(), m_format.end(),
              [](const VertexFormat& lhs, const VertexFormat& rhs)
    {
        if (lhs.stream == rhs.stream)
            return lhs.stream < rhs.stream;
        return lhs.offset < rhs.offset;
    });
    computeStrides();
}
void LayoutAsset::doSerializeConfig(Archive<Buffer> archive)
{
    uint numAttributes = m_format.size();
    serializeBinary<uint>(archive, numAttributes);
    m_format.resize(numAttributes);
    for (VertexFormat& attrib : m_format)
    {
        serializeBinary<Format>(archive, attrib.type);
        serializeBinary<string>(archive, attrib.name);
        serializeBinary<uint>(archive, attrib.stream);
        serializeBinary<uint>(archive, attrib.offset);
        serializeBinary<bool>(archive, attrib.isInstanced);
    }
}
void LayoutAsset::doRestore()
{
    computeStrides();
}


// Stuff
void LayoutAsset::computeStrides()
{
    m_strides.clear();
    for (const VertexFormat& fmt : m_format)
    {
        m_strides[fmt.stream] = std::max(m_strides[fmt.stream], 
                                         fmt.offset + formatStride(fmt.type));
    }
}


// Gets
const vector<VertexFormat>& LayoutAsset::vertexFormat() const
{
    return m_format;
}
const vector<uint>& LayoutAsset::strides() const
{
    return m_strides;
}
