#include "pch.h"
#include "ls/asset/EffectAsset.h"

#include "ls/asset/LayoutAsset.h"
#include "ls/gapi/Renderer.h"
#include "ls/gapi/usl/usl.h"
#include "ls/io/FileSystemInterface.h"

using namespace ls;
EffectAsset::EffectAsset()
{

}
EffectAsset::~EffectAsset()
{
    // Release GAPI
    if (renderer() && !!m_gapiEffect)
    {
        renderer()->destroyEffect(m_gapiEffect);
    }

    // Release RAM
    dropCache();
}


// Implement Asset
string EffectAsset::extension() const
{
    return ".cxx";
}
void EffectAsset::doSerialize(Archive<Buffer> archive)
{
    if (archive.isWrite())
    {
        LS_THROW(!m_compiledEffect.empty() && !m_compiledEffect.empty(),
                 LogicException, "No data is stored in RAM");
    }

    serializeBinary(archive, m_compiledEffect);
}
void EffectAsset::doCompileAndLoad(StreamInterface& source)
{
    // Prepare source
    string text;
    text += "#define ";
    text += toString(renderer()->effectLanguage());
    text += "\n";
    text += getOrDefault(renderer()->effectLibrary(), ".Default");
    text += source.text();

    // Try to compile effect
    unique_ptr<usl::Effect> effectData
        = usl::loadEffect(text, renderer()->effectLibrary());
    Stream binaryFile = renderer()->compileEffect(*effectData);
    LS_THROW(binaryFile, IoException, "Unknown error");

    // Save complied effect to buffer
    binaryFile->seek(0);
    m_compiledEffect = binaryFile->data();
}
void EffectAsset::doRestore()
{
    if (renderer())
    {
        Stream stream = StreamInterface::open(std::move(m_compiledEffect));
        m_compiledEffect.clear();
        m_gapiEffect = renderer()->createEffect(*stream);
    }
}


// Cache
const EffectHandle& EffectAsset::gapiEffect() const
{
    debug_assert(isGapiReady());
    return m_gapiEffect;
}
LayoutHandle EffectAsset::gapiCachedLayout(ProgramHandle program,
                                           const LayoutAsset& layoutAsset)
{
    debug_assert(isGapiReady());
    LayoutHandle& layout = m_cachedLayouts[program];
    if (!layout)
    {
        const vector<VertexFormat>& vertexFormat = layoutAsset.vertexFormat();
        const vector<uint>& strides = layoutAsset.strides();
        layout = renderer()->createLayout(program,
                                          strides.data(), strides.size(),
                                          vertexFormat.data(), vertexFormat.size());
    }
    return layout;
}
void EffectAsset::dropCache()
{
    debug_assert(renderer());

    // Drop layouts
    for (auto& item : m_cachedLayouts)
    {
        renderer()->destroyLayout(item.second);
    }
    m_cachedLayouts.clear();
}


// Gets
void EffectAsset::dropRam()
{
    m_compiledEffect.clear();
    m_compiledEffect.shrink_to_fit();
}
bool EffectAsset::isGapiReady() const
{
    return !!m_gapiEffect;
}
