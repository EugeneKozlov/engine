/// @file ls/asset/WorldRegion.h
/// @brief World Region

#pragma once

#include "ls/common.h"
#include "ls/asset/Asset.h"
#include "ls/core/AsyncStorageProvider.h"
#include "ls/core/Blob.h"
#include "ls/core/FlatArray.h"
#include "ls/io/Buffer.h"
#include "ls/io/StreamInterface.h"

namespace ls
{
    /// @addtogroup LsWorld
    /// @{
    
    // #TODO Move these templates somewhere
    /// @brief Dummy universal function
    template <class TReturnType = void>
    struct DummyFunction
    {
        /// @brief Ctor
        DummyFunction(TReturnType returnValue = TReturnType())
            : m_returnValue(returnValue)
        {
        }
        /// @brief Empty call
        template <class ... Args>
        TReturnType operator () (Args && ... /*args*/)
        {
            return m_returnValue;
        }
        /// @brief Empty const call
        template <class ... Args>
        TReturnType operator () (Args && ... /*args*/) const
        {
            return m_returnValue;
        }
    private:
        TReturnType m_returnValue; ///< Held return value
    };
    /// @brief Dummy universal function without return value
    template <>
    struct DummyFunction<void>
    {
        /// @brief Ctor
        DummyFunction()
        {
        }
        /// @brief Empty call
        template <class ... Args>
        void operator () (Args && ... /*args*/)
        {
        }
        /// @brief Empty const call
        template <class ... Args>
        void operator () (Args && ... /*args*/) const
        {
        }
    };
    /// @brief World region
    class WorldRegion
        : Noncopyable
        , public AsyncStorageProvider
        , public std::enable_shared_from_this<WorldRegion>
    {
    public:
        /// @brief Region Callback
        using Callback = std::function<void(WorldRegion&)>;
        /// @brief Callbacks
        struct Callbacks
        {
            Callback preLoadRegion = DummyFunction<>();
            Callback asyncLoadRegion = DummyFunction<>();
            Callback postLoadRegion = DummyFunction<>();
            Callback unloadRegion = DummyFunction<>();
            Callback preLoadChunks = DummyFunction<>();
            Callback asyncLoadChunks = DummyFunction<>();
            Callback postLoadChunks = DummyFunction<>();
            Callback unloadChunks = DummyFunction<>();
        };
        /// @brief Config
        class Config
        {
        public:
            /// @brief Compute all
            void compute()
            {
                LS_THROW(chunkWidth > 0,
                         LogicException,
                         "Too small chunk size");
                LS_THROW(chunkWidth <= regionWidth,
                         LogicException,
                         "Too small region size");
                LS_THROW(regionWidth % chunkWidth == 0,
                         LogicException,
                         "Incompatible chunk and region size");
                regionInChunks = regionWidth / chunkWidth;
            }
        public:
            uint chunkWidth; ///< Chunk width (in units)
            uint regionWidth; ///< Region width (in units)
            uint regionInChunks; ///< Region width (in chunks)

            uint regionHeightMap; ///< Region height map size
            uint regionNormalMap; ///< Region normal map size
            uint regionColorMap; ///< Region color map size
        };
        /// @brief Max number of Blend Layers
        static const uint MaxBlendLayers = 4;
        /// @brief Terrain Data
        class TerrainData
        {
        public:
            /// @brief Height data
            using Height = float;
            /// @brief Normal data
            struct Normal
            {
                /// @brief Ctor
                Normal() = default;
                /// @brief Ctor
                Normal(sbyte x, sbyte y, sbyte z, ubyte m)
                    : vec(x, y, z)
                    , material(m)
                {
                }
                /// @brief Normal direction
                Vector<sbyte, 3> vec = { 0, 127, 0 };
                /// @brief Material index
                ubyte material = 0;
            };
            /// @brief Color data
            using Color = Vector<ubyte, 4>;
            /// @brief Blending data
            using Blend = Vector<ubyte, 2>;
        public:
            /// @brief Ctor
            TerrainData() = default;
            /// @brief Ctor
            TerrainData(const uint width,
                        const uint heightWidth, const uint normalWidth,
                        const uint colorWidth, const uint blendWidth);
            /// @brief Init
            void init(ubyte* buffer, const uint bufferSize);

            /// @brief Get buffer size
            uint bufferSize() const;
            /// @brief Get min
            double min() const
            {
                return bounds.x;
            }
            /// @brief Get max
            double max() const
            {
                return bounds.y;
            }

            /// @brief Serialize
            void serialize(Archive<Buffer> archive);
        protected:
            /// @brief Compute (x + 1)^2
            static uint computeSize(uint width)
            {
                return (width + 1) * (width + 1);
            }
        public:
            /// @brief Is initialized?
            bool isInitialized = false;

            /// @brief Width
            uint width;
            /// @brief Height map width
            uint heightWidth;
            /// @brief Normal map width
            uint normalWidth;
            /// @brief Color map width
            uint colorWidth;
            /// @brief Blend map width
            uint blendWidth;

            /// @brief Upper and lower bounds
            double2 bounds;

            /// @brief Height map
            FlatArrayWrapper<Height> heightMap;
            /// @brief Normal map
            FlatArrayWrapper<Normal> normalMap;
            /// @brief Color map
            FlatArrayWrapper<Color> colorMap;
            /// @brief Blend maps
            array<FlatArrayWrapper<Blend>, MaxBlendLayers> blendMap;
        };
        /// @brief Region Data
        class RegionData
            : Noncopyable
            , public TerrainData
        {
        public:
            /// @brief Ctor
            RegionData(const Config& config);
        private:
            vector<ubyte> m_buffer;
        };
        /// @brief Region Data Pointer
        using RegionDataPtr = unique_ptr<RegionData>;
        /// @brief Chunk
        class Chunk
            : public TerrainData
        {
        public:
            /// @brief Degenerate
            Chunk() = default;
            /// @brief Ctor
            Chunk(const Config& config);
            /// @brief Initialize
            void init(const int2& chunkIndex, ubyte* buffer, uint bufferSize);
            /// @brief Update blend data
            bool updateBlend(const int2& layerMaterials, const int2& begin, const int2& end,
                             const FlatArrayWrapper<float3>& data, FlatArray<Blend>& buffer);
            /// @brief Update color data
            void updateColor(const vector<float4>& materialColors);

            /// @brief Serialize
            void serialize(Archive<Buffer> archive);

            /// @brief Get min height
            double min() const
            {
                return bounds.x;
            }
            /// @brief Get max height
            double max() const
            {
                return bounds.y;
            }
            /// @brief Get AABB
            AABB aabb() const;
            /// @brief Compute height in point
            float height(const float2& local) const;
        public:
            /// @brief Chunk index
            int2 index;

            /// @brief Density 
            uint density;
            /// @brief Number of layers
            uint numLayers = 0;
            /// @brief Terrain materials
            array<int2, MaxBlendLayers> materials;
            /// @brief Layer weights
            array<uint, MaxBlendLayers> layerWeights;
        };
        /// @brief Iterator over chunks
        using ChunkIterator = FlatArray<Chunk>::Iterator;
        /// @brief World Region Detail
        class ChunksData
            : Noncopyable
        {
        public:
            /// @brief Ctor
            ChunksData(const Config& config, const int2& regionIndex);

            /// @brief Serialize
            void serialize(Archive<Buffer> archive);

            /// @brief Begin iterator
            ChunkIterator begin()
            {
                return m_chunks.begin();
            }
            /// @brief End iterator
            ChunkIterator end()
            {
                return m_chunks.end();
            }
            /// @brief Get array size
            int2 size() const
            {
                return m_chunks.size();
            }
            /// @brief Get chunk by local index
            Chunk& operator[](const int2& local)
            {
                return m_chunks[local];
            }
            /// @brief Get chunk by local index
            const Chunk& operator[](const int2& local) const
            {
                return m_chunks[local];
            }
            /// @brief Set static objects data
            void staticObjects(const Buffer& buffer)
            {
                m_staticObjects = buffer;
            }
            /// @brief Get static objects data
            Buffer& staticObjects()
            {
                return m_staticObjects;
            }
        private:
            const Config m_config;
            FlatArray<Chunk> m_chunks;
            uint m_stride;
            vector<ubyte> m_buffer;

            Buffer m_staticObjects;
        };
        /// @brief Chunks Data Pointer
        using ChunksDataPtr = unique_ptr<ChunksData>;
    public:
        /// @brief Ctor
        /// @param index
        ///   Region location
        /// @param config
        ///   Global config
        /// @param callbacks
        ///   Load/unload callbacks
        /// @param fileSystem
        ///   File system to save/load region files
        /// @param discardSaved
        ///   Set to discard saved region and overwrite file
        /// @note If not @a discardSaved, stream is opened here.
        ///       If @a discardSaved, stream is opened on save.
        ///       In both cases, no async file system calls are performed.
        WorldRegion(const int2& index, 
                    const Config& config, 
                    const Callbacks& callbacks,
                    FileSystemInterface& fileSystem, 
                    Worker& worker,
                    const bool discardSaved);
        /// @brief Dtor
        ~WorldRegion();
        /// @brief Load region data (async)
        void loadRegion();
        /// @brief Unload region data
        /// @note implicitly calls @a unloadChunks
        void unloadRegion();
        /// @brief Load chunks data (async)
        /// @note implicitly calls @a loadRegion
        void loadChunks();
        /// @brief Unload chunks data
        void unloadChunks();
        /// @brief Flush pending futures
        void flush();
        /// @brief Wait for all pending async tasks
        void wait();
        /// @brief Save data
        void save();

        /// @brief Get chunks
        /// @note May be called during async loading
        pair<ChunkIterator, ChunkIterator> chunks()
        {
            ChunksData* ptr = m_chunksData.load();
            debug_assert(ptr);
            return make_pair(ptr->begin(), ptr->end());
        }

        /// @name Gets
        /// @{

        /// @brief Get region index
        int2 index() const;
        /// @brief Get region config
        const Config& config() const;
        /// @brief Is region data requested?
        bool isRegionDataRequested() const;
        /// @brief Is region data ready?
        bool isRegionDataReady() const;
        /// @brief Is chunks data requested?
        bool isChunksDataRequested() const;
        /// @brief Is chunks data ready?
        bool isChunksDataReady() const;
        /// @brief Get region data
        /// @pre Region data must be loaded
        /// @note May be called during async loading
        RegionData& regionData();
        /// @brief Get chunks data
        /// @pre Chunks data must be loaded
        /// @note May be called during async loading
        ChunksData& chunksData();
        /// @}

        /// @name Index operations
        /// @{

        /// @brief Get chunk global index
        int2 local2global(const int2& local) const;
        /// @brief Get chunk local index
        int2 global2local(const int2& global) const;
        /// @brief Get begin chunk index
        int2 beginChunk() const;
        /// @brief Get end chunk index
        int2 endChunk() const;
        /// @brief Make file name
        string makeFileName() const;
        /// @}

        /// @name Data access
        /// @{

        /// @brief Find chunk by global pose
        /// @note May be called during async loading
        Chunk* findChunkByIndex(const int2& globalIndex);
        /// @brief Find chunk by position
        /// @note May be called during async loading
        Chunk* findChunkByPosition(const double2& position);
        /// @brief Get vertex height
        float getHeight(const double2& pos);
        /// @}
    private:

        /// @brief Open streams
        void openStreams(const StreamAccessFlag flag);
    private:
        const int2 m_index; ///< Region index
        const Config m_config; ///< Config
        const Callbacks m_callbacks; ///< Callbacks
        FileSystemInterface& m_fileSystem; ///< File system
        Worker& m_worker; ///< Worker for async tasks
        Stream m_regionStream; ///< Stream for region data
        Stream m_chunksStream; ///< Stream for chunks data

        std::atomic<RegionData*> m_regionData;
        RegionDataPtr m_regionDataHolder;
        std::future<RegionDataPtr> m_regionDataFuture;

        std::atomic<ChunksData*> m_chunksData;
        ChunksDataPtr m_chunksDataHolder;
        std::future<ChunksDataPtr> m_chunksDataFuture;
    };
    /// @}
}