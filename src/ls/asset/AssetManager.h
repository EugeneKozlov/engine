/// @file ls/asset/AssetManager.h
/// @brief Asset Manager
#pragma once

#include "ls/common.h"
#include "ls/asset/Asset.h"
#include "ls/io/common.h"
#include "ls/io/MultiFileSystem.h"
#include "ls/lua/BuiltinModule.h"

namespace ls
{
    class LuaApi;
    class MaterialCache;
    class Renderer;
    class Worker;

    /// @addtogroup LsAsset
    /// @{

    /// @brief Asset Manager
    class AssetManager
        : Noncopyable
        , public BuiltinModule
    {
    public:
        /// @brief Ctor
        /// @param assetFileSystem,cacheFileSystem
        ///   Asset file systems
        /// @param renderer
        ///   Optional renderer
        /// @param worker
        ///   Worker to perform async tasks. Uses new one-threaded worker if null.
        AssetManager(FileSystem assetFileSystem, FileSystem cacheFileSystem,
                     Renderer* renderer,
                     shared_ptr<Worker> worker);
        /// @brief Dtor
        ~AssetManager();
        /// @brief Generate folder
        void generateFolder(const string& folderName);
        /// @brief Clean folder
        void cleanFolder(const string& folderName);

        /// @name Assets
        /// @{

        /// @brief Load asset
        /// @throw IoException if cannot
        /// @return Guaranteed asset
        Asset::Ptr loadAsset(const AssetType type, const string& name, 
                             const string& currentFolder = "");
        /// @brief Create asset
        Asset::Ptr createAsset(const AssetType type, const string& name);
        /// @}

        /// @name Gets
        /// @{

        /// @brief Get file system
        FileSystemInterface& fileSystem();
        /// @brief Get worker
        Worker& worker();
        /// @brief Get renderer
        Renderer* renderer();
        /// @brief Get material cache
        MaterialCache* materialCache();
        /// @}
    private:
        /// @brief Heal asset name
        /// @throw ArgumentException if asset is placed outside working directory
        string healName(const string& name);
        /// @brief Flush all generated assets
        void flushGeneratedAssets();

        /// @name Internal Lua VM for generators
        /// @{
        LuaApi& luaApi();
        void intializeLua();
        bool luaIsFileExist(const string& fileName);
        /// @throw ArgumentException if asset type is unknown
        Asset::Ptr luaCreateAsset(const uint type, const string& fileName);
        /// @return Asset (guaranteed)
        /// @throw std::exception thrown by @a loadAsset
        Asset::Ptr luaLoadAsset(const uint type, const string& fileName);
        /// @brief Flush all generated assets
        void luaFlushGeneratedAssets();

        /// @throw ArgumentException if asset is not found
        /// @throw std::exception thrown by @a loadAsset
        float4 luaGetColor4f(const string& paletteName, const string& colorName);
        /// @throw ArgumentException if asset is not a texture
        void luaSetWrapping(Asset::Ptr asset, const bool enabled);
        /// @}

        /// @name Implement BuiltinModule
        /// @{
        virtual void exportToLua(LuaState& state) override;
        void luaImportModule(const string& name);
        void luaDisableTextureCompression();
        void luaCleanFolder(const string& packageName);
        void luaGenerateFolder(const string& packageName);
        /// @}
    private:
        /// @name Context
        /// @{
        FileSystem m_assetFileSystem;
        FileSystem m_cacheFileSystem;
        MultiFileSystem m_fileSystem;
        Renderer* m_renderer;
        shared_ptr<Worker> m_worker;
        unique_ptr<MaterialCache> m_materialCache;
        /// @}

        /// @name Assets
        /// @{
        hamap<string, std::weak_ptr<Asset>> m_assets; ///< Assets
        std::recursive_mutex m_mutex; ///< Locks package I/O
        /// @}

        /// @name Generator helpers
        /// @{
        vector<string> m_importedModules;
        unique_ptr<LuaApi> m_luaApi;

        bool m_disableTextureCompression = false;
        string m_currentFolderName;
        vector<Asset::Ptr> m_currentAssets;
        vector<Asset::Ptr> m_heldAssets;
        /// @}    
    };
    /// @}
}