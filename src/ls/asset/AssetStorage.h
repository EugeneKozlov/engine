/// @file ls/asset/AssetStorage.h
/// @brief Asset Storage
#pragma once

#include "ls/common.h"
#include "ls/io/FileSystemInterface.h"
#include "ls/xml/common.h"

namespace ls
{
    class AssetManagerOld;

    /// @addtogroup LsAsset
    /// @{

    /// @brief Asset Serializer Base
    template <class A, class H>
    struct AssetSerializerBase
        : Noncopyable
    {
    public:
        /// @brief Asset type
        using Asset = A;
        /// @brief Shared asset handle type
        using Handle = H;

        /// @brief Ctor
        AssetSerializerBase(AssetManagerOld& manager, FileSystemInterface& fileSystem)
            : manager(manager)
            , fileSystem(fileSystem)
        {
        }

        /// @brief Save asset to file
        void save(const string& name, Handle asset) = delete;
        /// @brief Load asset from file
        Handle read(const string& name) = delete;
        /// @brief Import asset
        Handle import(const string& name) = delete;
    public:
        /// @brief Manager
        AssetManagerOld& manager;
        /// @brief File system
        FileSystemInterface& fileSystem;
    };
    /// @brief Open file for asset
    template <class Serializer>
    Stream openAssetFile(Serializer& self, const string& name, StreamAccessFlag access)
    {
        string fileName = self.folder + "/" + name + self.ext;
        Stream stream = self.fileSystem.openStream(fileName, access);

        LS_THROW(!!stream, IoException, "Cannot open file [", fileName, "]");
        return stream;
    }
    /// @brief Open output asset file
#define LS_OPEN_OUTPUT_ASSET_FILE(NAME) openAssetFile(*this, NAME, StreamAccess::WriteNew);
    /// @brief Open input asset file
#define LS_OPEN_INPUT_ASSET_FILE(NAME) openAssetFile(*this, NAME, StreamAccess::Read);
    /// @brief Asset Serializer
    template <class Asset>
    struct AssetSerializer;
    /// @brief Asset importer, loader and storage
    template <class A, class H>
    class AssetStorage
        : public Nameable<AssetStorage<A, H>>
        , public AssetStorageInterface
    {
    public:
        /// @brief Asset type
        using Asset = A;
        /// @brief Handle type
        using Handle = H;
        /// @brief Test
        static_assert(is_same<Handle, typename AssetSerializer<Asset>::Handle>::value,
                      "Handle type mismatch");
    public:
        /// @brief Ctor
        AssetStorage(AssetManagerOld& manager)
            : Nameable<AssetStorage<A, H>>()
            , AssetStorageInterface(manager)
            , m_serializer(m_manager, m_fileSystem)
        {
            this->debugRename(m_serializer.type);
        }
        /// @brief Get
        virtual any get(const string& name) override
        {
            Handle asset = receive(name);
            return asset;
        }
    private:
        /// @brief Generate this class name
        string generateName() const
        {
            return m_serializer.type + "Loader";
        }
        /// @brief Receive asset
        Handle receive(const string& name)
        {
            // Return exist
            auto iter = m_database.find(name);
            if (iter != m_database.end())
                return iter->second;

            // Load new
            auto asset = load(name);

            // Import on fail
            if (!asset)
            {
                asset = import(name);
                if (!asset)
                    return nullptr;

                // Save
                save(name, asset);
            }

            // Save and return
            m_database.emplace(name, asset);

            return asset;
        }
        /// @brief Save asset
        void save(const string& name, Handle asset)
        {
            using namespace pugi;

            // Save
            try
            {
                m_serializer.write(name, asset);
                logImport("Saved [", name, "]");
            }
            catch (const Exception& ex)
            {
                logError("Cannot save [", name, "] : ", ex.what());
            }
        }
        /// @brief Load asset
        Handle load(const string& name)
        {
            using namespace pugi;

            // Try to read
            try
            {
                Handle asset = m_serializer.read(name);
                debug_assert(!!asset, "Return asset or throw");
                return asset;
            }
            catch (const Exception& ex)
            {
                logError("Cannot load [", name, "] : ", ex.what());
                return nullptr;
            }
        }
        /// @brief Import asset
        Handle import(const string& name)
        {
            try
            {
                Handle asset = m_serializer.import(name);
                if (!asset)
                {
                    logError("Cannot import [", name, "]");
                    return nullptr;
                }

                logImport("Imported [", name, "]");
                return asset;
            }
            catch (const Exception& ex)
            {
                logError("Cannot import [", name, "] : ", ex.what());
                return nullptr;
            }
        }
        /// @brief Log asset loading
        template <class ... Args>
        void logLoad(const Args& ... args) const
        {
            m_manager.logLoad(this->debugName(), " #", this->debugIndex(), ": ", args...);
        }
        /// @brief Log asset save/import
        template <class ... Args>
        void logImport(const Args& ... args) const
        {
            m_manager.logImport(this->debugName(), " #", this->debugIndex(), ": ", args...);
        }
        using Nameable<AssetStorage<A, H>>::logError;
    private:
        /// @brief Asset storage
        map<string, Handle> m_database;
        /// @brief Serializer
        AssetSerializer<Asset> m_serializer;
    };
    /// @}
}
