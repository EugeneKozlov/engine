#include "pch.h"
#include "ls/asset/AssetManager.h"

#include "ls/asset/Asset.h"
#include "ls/asset/MaterialCache.h"
#include "ls/asset/all.h"
#include "ls/core/Worker.h"
#include "ls/gen/proxy/ProxyGenerator.h"
#include "ls/gen/texture/TextureGenerator.h"
#include "ls/gen/tree/TreeGenerator.h"
#include "ls/gen/world/WorldGenerator.h"
#include "ls/lua/LuaApi.h"

#include <boost/filesystem.hpp>

using namespace ls;


// Main
AssetManager::AssetManager(FileSystem assetFileSystem,
                           FileSystem cacheFileSystem,
                           Renderer* renderer,
                           shared_ptr<Worker> worker)
    : m_assetFileSystem(assetFileSystem)
    , m_cacheFileSystem(cacheFileSystem)
    , m_fileSystem({ cacheFileSystem, assetFileSystem })
    , m_renderer(renderer)
    , m_worker(worker)
{
    if (!m_worker)
    {
        m_worker = make_shared<Worker>(1);
    }
    if (m_renderer)
    {
        m_materialCache = make_unique<MaterialCache>(*m_renderer);
    }
}
AssetManager::~AssetManager()
{
    m_luaApi.reset();
    m_materialCache.reset();
}
void AssetManager::generateFolder(const string& folderName)
{
    // Thread-safe
    std::lock_guard<std::recursive_mutex> guard(m_mutex);

    // Should contain Lua script
    // #TODO Move name to constant
    const string scriptName = "main.lua";
    const bool isValid = m_fileSystem.isRegularFile(folderName + "/" + scriptName);
    LS_THROW(isValid, 
             IoException,
             "Folder [", folderName, "] has no ", scriptName, " file");

    // Open folder
    FileSystem folderFileSystem = m_fileSystem
        .openMultisystem(folderName, "", StreamAccess::ReadWriteNew);

    // Start Lua VM
    LuaApi& luaInterface = luaApi();
    LuaState& luaState = luaInterface.luaState();

    // Push current folder (auto-pop in dtor)
    LuaFolderHolder folderHolder(luaInterface, folderFileSystem);
    m_currentFolderName = folderName;

    // Reset entry point
    luaState.push(nullptr);
    luaState.setGlobal("main");

    // Import script
    luaInterface.import(scriptName);

    // Call entry point
    luaState.getGlobal("main");
    LS_THROW(luaState.isFunction(-1),
             ScriptException, "'main' entry point is not found");

    // Call main function
    if (luaState.pcall(0, 0, 0) != 0)
    {
        const string error = luaState.popValue<string>();
        LS_THROW(0, ScriptException,
                    "'main' call error: ", error);
    }

    // Flush all generated assets
    flushGeneratedAssets();

    // Clean up
    m_currentFolderName.clear();
}
void AssetManager::cleanFolder(const string& folderName)
{

}
void AssetManager::flushGeneratedAssets()
{
    debug_assert(!m_currentFolderName.empty());
    for (Asset::Ptr& asset : m_currentAssets)
    {
        if (!asset->name().empty())
        {
            try
            {
                asset->save();
            }
            catch (const std::exception& ex)
            {
                logError(ex.what());
            }
        }
    }
    m_currentAssets.clear();
}
string AssetManager::healName(const string& name)
{
    boost::filesystem::path p = name;
    string result;
    for (auto& item : p)
    {
        if (item == ".")
        {
            // Skip dot
            continue;
        }
        else if (item == "..")
        {
            // Collapse dot-dot
            LS_THROW(!result.empty(), IoException, "Asset [", name, "] is outside working directory");
            const size_t lastSlash = result.find_last_of('/');
            if (lastSlash != std::string::npos)
            {
                result.erase(lastSlash);
            }
            else
            {
                result.clear();
            }
        }
        else
        {
            // Append name
            if (!result.empty())
            {
                result += '/';
            }
            result += item.string();
        }
    }
    return result;
}


// Assets
Asset::Ptr AssetManager::loadAsset(const AssetType type, const string& name, const string& currentFolder)
{
    // Thread-safe
    std::lock_guard<std::recursive_mutex> guard(m_mutex);

    // Try to find loaded
    Asset::Ptr asset = getOrDefault(m_assets, name).lock();
    if (asset)
    {
        return asset;
    }

    // Log
    if (currentFolder.empty())
    {
        logDebug("Load [", type, "] asset [", name, "]");
    }
    else
    {
        logDebug("Load [", type, "] asset [", name, "] from [", currentFolder, "] ");
    }

    // Create asset
    asset = Asset::construct(type);

    // Try to find asset by relative path
    const string extendedName = currentFolder + "/" + name;
    const bool extendedValid = !currentFolder.empty() && asset->exist(m_fileSystem, extendedName);

    // Load asset
    string realName;
    try
    {
        realName = healName(extendedValid ? extendedName : name);

        asset->initialize(*this, realName);
        asset->load();
    }
    catch (const std::exception& ex)
    {
        LS_THROW(0, IoException, ex.what());
    }
    

    // Put to container
    m_assets.emplace(realName, asset);
    return asset;
}
Asset::Ptr AssetManager::createAsset(const AssetType type, const string& name)
{
    // Thread-safe
    std::lock_guard<std::recursive_mutex> guard(m_mutex);

    // Log
    logDebug("Create [", type, "] asset [", name, "]");

    // Create asset
    Asset::Ptr asset = Asset::construct(type);
    asset->initialize(*this, name);

    return asset;
}


// Gets
FileSystemInterface& AssetManager::fileSystem()
{
    return m_fileSystem;
}
Worker& AssetManager::worker()
{
    return *m_worker;
}
Renderer* AssetManager::renderer()
{
    return m_renderer;
}
MaterialCache* AssetManager::materialCache()
{
    return m_materialCache.get();
}


// Internal Lua interface for generators
LuaApi& AssetManager::luaApi()
{
    // Initialize
    if (!m_luaApi)
    {
        intializeLua();
    }
    return *m_luaApi;
}
void AssetManager::intializeLua()
{
    m_luaApi = make_unique<LuaApi>("AssetGenerator");
    LuaState& state = m_luaApi->luaState();

    // Register assets
    function<Asset::Ptr(const uint, const string&)> funLoadAsset =
        bind(&AssetManager::luaLoadAsset, this, _1, _2);
    function<Asset::Ptr(const uint, const string&)> funCreateAsset =
        bind(&AssetManager::luaCreateAsset, this, _1, _2);
    function<bool(const string&)> funIsFileExist =
        bind(&AssetManager::luaIsFileExist, this, _1);
    function<void()> funFlushGeneratedAssets =
        bind(&AssetManager::luaFlushGeneratedAssets, this);
    function<float4(const string&, const string&)> funGetColor4f =
        bind(&AssetManager::luaGetColor4f, this, _1, _2);
    function<void(Asset::Ptr, const bool)> funSetWrapping =
        bind(&AssetManager::luaSetWrapping, this, _1, _2);
    LuaBinding(state)
        .beginClass<Asset::Ptr>("Asset")
        .endClass()
        .beginModule("cg")
        /**/.beginModule("Asset")
        /**//**/.addConstant("Effect", static_cast<uint>(AssetType::Effect))
        /**//**/.addConstant("EffectLibrary", static_cast<uint>(AssetType::EffectLibrary))
        /**//**/.addConstant("Material", static_cast<uint>(AssetType::Material))
        /**//**/.addConstant("Layout", static_cast<uint>(AssetType::Layout))
        /**//**/.addConstant("Mesh", static_cast<uint>(AssetType::Mesh))
        /**//**/.addConstant("Model", static_cast<uint>(AssetType::Model))
        /**//**/.addConstant("Texture", static_cast<uint>(AssetType::Texture))
        /**//**/.addConstant("Palette", static_cast<uint>(AssetType::Palette))
        /**//**/.addConstant("Text", static_cast<uint>(AssetType::Text))
        /**//**/.addConstant("Texture", static_cast<uint>(AssetType::Texture))
        /**//**/.addConstant("Tree", static_cast<uint>(AssetType::Tree))
        /**//**/.addConstant("World", static_cast<uint>(AssetType::World))
        /**/.endModule()
        /**/.addFunction("loadAsset", funLoadAsset)
        /**/.addFunction("createAsset", funCreateAsset)
        /**/.addFunction("isFileExist", funIsFileExist)
        /**/.addFunction("flushGeneratedAssets", funFlushGeneratedAssets)
        /**/.addFunction("getColor4f", funGetColor4f)
        /**/.addFunction("setWrapping", funSetWrapping)
        .endModule();

    // Register modules
    m_luaApi->importModule<TreeGenerator>();
    if (m_renderer)
    {
        // #TODO Add generator reset
        TextureGenerator& textureGenerator
            = m_luaApi->importModule<TextureGenerator>(*m_renderer, m_disableTextureCompression);
        m_luaApi->importModule<ProxyGenerator>(*this, textureGenerator);
        m_luaApi->importModule<WorldGenerator>(*this);
    }

    // Load Lua modules
    for (const string& moduleName : m_importedModules)
    {
        logInfo("Import module [", moduleName, "]");

        // Load asset
        Asset::Ptr asset = loadAsset(AssetType::Text, moduleName, "");
        shared_ptr<TextAsset> textAsset = Asset::cast<TextAsset>(asset);
        if (!textAsset)
        {
            logError("Text asset [", moduleName, "] cannot be loaded");
            continue;
        }

        // Import text
        if (state.doString(textAsset->text().c_str()))
        {
            logError("Import has failed: ", state.getString(-1));
        }
    }
}
Asset::Ptr AssetManager::luaCreateAsset(const uint type, const string& fileName)
{
    debug_assert(!m_currentFolderName.empty());

    LS_THROW(type < NumAssetTypes, ArgumentException, "Invalid asset type");
    Asset::Ptr asset = Asset::construct(static_cast<AssetType>(type));
    if (m_currentFolderName.empty() || fileName.empty())
    {
        asset->initialize(*this, fileName);
    }
    else
    {
        asset->initialize(*this, m_currentFolderName + "/" + fileName);
    }

    m_currentAssets.push_back(asset);
    return asset;
}
bool AssetManager::luaIsFileExist(const string& fileName)
{
    return m_fileSystem.isRegularFile(m_currentFolderName + "/" + fileName);
}
Asset::Ptr AssetManager::luaLoadAsset(const uint type, const string& fileName)
{
    LS_THROW(type < NumAssetTypes, ArgumentException, "Invalid asset type");
    Asset::Ptr asset = loadAsset(static_cast<AssetType>(type), fileName, m_currentFolderName);
    m_heldAssets.push_back(asset);
    return asset;
}
void AssetManager::luaFlushGeneratedAssets()
{
    try
    {
        flushGeneratedAssets();
    }
    catch (const IoException& ex)
    {
        logError(ex.what());
    }
}
float4 AssetManager::luaGetColor4f(const string& paletteName, const string& colorName)
{
    // Load 
    Asset::Ptr asset = loadAsset(AssetType::Palette, paletteName, m_currentFolderName);
    shared_ptr<PaletteAsset> palette = Asset::cast<PaletteAsset>(asset);
    LS_THROW(palette, 
             ArgumentException, 
             "Asset [", paletteName, "] is not a palette");
    return palette->getColor4f(colorName);
}
void AssetManager::luaSetWrapping(Asset::Ptr asset, const bool enabled)
{
    shared_ptr<TextureAsset> texture = Asset::cast<TextureAsset>(asset);
    LS_THROW(texture,
             ArgumentException,
             "Asset [", texture->name(), "] is not a texture");
    texture->setWrapping(enabled);
}


// Script API
void AssetManager::exportToLua(LuaState& state)
{
    function<void()> funDisableTextureCompression =
        bind(&AssetManager::luaDisableTextureCompression, this);
    function<void(const string&)> funImportModule =
        bind(&AssetManager::luaImportModule, this, _1);
    function<void(const string&)> funCleanFolder =
        bind(&AssetManager::luaCleanFolder, this, _1);
    function<void(const string&)> funGenerateFolder =
        bind(&AssetManager::luaGenerateFolder, this, _1);
    LuaBinding(state)
        .beginClass<Asset>("Asset")
        .endClass()
        .beginModule("e")
        /**/.addFunction("disableTextureCompression", funDisableTextureCompression)
        /**/.addFunction("importModule", funImportModule)
        /**/.addFunction("cleanFolder", funCleanFolder)
        /**/.addFunction("generateFolder", funGenerateFolder)
        .endModule();
}
void AssetManager::luaDisableTextureCompression()
{
    LS_THROW(!m_luaApi, ArgumentException, "Lua VM is already constructed");
    m_disableTextureCompression = true;
}
void AssetManager::luaImportModule(const string& name)
{
    LS_THROW(!m_luaApi, ArgumentException, "Lua VM is already constructed");
    auto iter = std::find(m_importedModules.begin(), m_importedModules.end(), name);
    LS_THROW(iter == m_importedModules.end(), 
             ArgumentException, 
             "Duplicate module [", name, "] to import");
    m_importedModules.push_back(name);
}
void AssetManager::luaCleanFolder(const string& packageName)
{
    m_cacheFileSystem->deleteItem(packageName);
}
void AssetManager::luaGenerateFolder(const string& packageName)
{
    try
    {
        generateFolder(packageName);
    }
    catch (const IoException& ex)
    {
        logError(ex.what());
    }
    catch (const LogicException& ex)
    {
        logError(ex.what());
    }
}
