/// @file ls/asset/WorldAsset.h
/// @brief World Asset

#pragma once

#include "ls/common.h"
#include "ls/asset/Asset.h"
#include "ls/asset/WorldRegion.h"
#include "ls/core/Blob.h"
#include "ls/core/FlatArray.h"
#include "ls/io/Buffer.h"
#include "ls/render/Mesh.h"

namespace ls
{
    /// @addtogroup LsWorld
    /// @{
    
    /// @brief World Asset
    class WorldAsset
        : public AssetT<AssetType::World, AssetCategory::Simple>
    {
    public:
        /// @brief Region Container
        using RegionContainer = hamap<int2, shared_ptr<WorldRegion>>;
        /// @brief Region Iterator
        using RegionIterator = RegionContainer::iterator;
    public:
        /// @brief Ctor
        WorldAsset();
        /// @brief Set callbacks
        /// @pre World should be completely unloaded
        void setCallbacks(const WorldRegion::Callbacks& callbacks);

        /// @name Region and chunk data management
        /// @{

        /// @brief Load region
        void loadRegion(const int2& regionIndex);
        /// @brief Unload region
        void unloadRegion(const int2& regionIndex);
        /// @brief Load region chunks
        void loadRegionChunks(const int2& regionIndex);
        /// @brief Unload region chunks
        void unloadRegionChunks(const int2& regionIndex);
        /// @brief Flush pending futures
        void flushAll();
        /// @brief Wait for all pending async tasks
        void waitAll();
        /// @brief Unload all regions
        void unloadAll();
        /// @brief Save data
        virtual void save() override;
        /// @}

        /// @name Chunk and region data access
        /// @{

        /// @brief Global chunk to region
        void chunk2region(const int2& chunkIndex, int2& regionIndex, int2& localIndex) const;
        /// @brief Global chunk to region
        int2 chunk2region(const int2& chunkIndex) const;
        /// @brief Get all regions
        std::pair<RegionIterator, RegionIterator> regions();
        /// @brief Get region by index
        WorldRegion* findRegion(const int2& regionIndex);
        /// @brief Get chunk by index
        WorldRegion::Chunk* findChunk(const int2& chunkIndex);
        /// @brief Get vertex height
        float getHeight(const double2& pos);
        /// @}
    private:
        /// @name Implement Asset
        /// @{
        virtual string extension() const override;
        virtual void doPostInitialize() override;
        virtual void doSerialize(Archive<Buffer> archive) override;
        /// @}

        /// @brief Find or create region
        WorldRegion& findOrCreate(const int2& regionIndex);
    public:
        /// @brief Config
        WorldRegion::Config config;
        /// @brief Surface materials
        vector<string> materials;
        /// @brief Global components
        Buffer globalComponents;
        /// @brief Initial dynamic entities
        Buffer dynamicEntities;
    private:
        FileSystem m_fileSystem; ///< File system contains world data file
        WorldRegion::Callbacks m_callbacks; ///< Load/unload callbacks

        hamap<int2, shared_ptr<WorldRegion>> m_regions; ///< World regions

    };
    /// @}
}