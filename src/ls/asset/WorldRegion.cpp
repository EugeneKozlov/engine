#include "pch.h"
#include "ls/asset/WorldRegion.h"

#include "ls/core/Worker.h"
#include "ls/io/FileSystemInterface.h"

using namespace ls;

namespace ls
{
    template <class TObject, class TStream>
    void serializeBinary(Archive<TStream>& archive, FlatArrayWrapper<TObject>& object)
    {
        TStream& stream = archive.stream();
        if (archive.isWrite())
        {
            stream.write(object.data(), object.length() * object.stride());
        }
        else
        {
            stream.read(object.data(), object.length() * object.stride());
        }
    }
}


// World Terrain Data
WorldRegion::TerrainData::TerrainData(const uint width,
                                      const uint heightWidth, const uint normalWidth,
                                      const uint colorWidth, const uint blendWidth)
    : width(width)
    , heightWidth(heightWidth)
    , normalWidth(normalWidth)
    , blendWidth(blendWidth)
    , colorWidth(colorWidth)

    , bounds(0.0f)
{
}
uint WorldRegion::TerrainData::bufferSize() const
{
    uint size = 0;
    size += computeSize(heightWidth) * sizeof(Height);
    size += computeSize(normalWidth) * sizeof(Normal);
    if (colorWidth != 0)
        size += computeSize(colorWidth) * sizeof(Color);
    if (blendWidth != 0)
        size += computeSize(blendWidth) * sizeof(Blend) * MaxBlendLayers;
    return size;
}
void WorldRegion::TerrainData::init(ubyte* buffer, uint bufferSize)
{
    debug_assert(!isInitialized);
    isInitialized = true;

    // Update pointers
    uint offset = 0;
    heightMap.reset(heightWidth + 1, heightWidth + 1, reinterpret_cast<Height*>(&buffer[offset]));
    offset += heightMap.length() * heightMap.stride();
    normalMap.reset(normalWidth + 1, normalWidth + 1, reinterpret_cast<Normal*>(&buffer[offset]));
    offset += normalMap.length() * normalMap.stride();
    colorMap.reset(colorWidth + 1, colorWidth + 1, reinterpret_cast<Color*>(&buffer[offset]));
    offset += colorMap.length() * colorMap.stride();
    if (blendWidth > 0)
    {
        for (uint idx = 0; idx < MaxBlendLayers; ++idx)
        {
            blendMap[idx].reset(blendWidth + 1, blendWidth + 1, reinterpret_cast<Blend*>(&buffer[offset]));
            offset += blendMap[idx].length() * blendMap[idx].stride();
        }
    }

    // Check
    debug_assert(offset == bufferSize, "buffer size mismatch");

    // Fill
    heightMap.fill(0.0f);
    normalMap.fill(Normal(0, 0x7f, 0, 0));
    colorMap.fill(Color(0x7f));
    for (uint idx = 0; idx < MaxBlendLayers; ++idx)
        blendMap[idx].fill(Blend(0, 0xff));
}
void WorldRegion::TerrainData::serialize(Archive<Buffer> archive)
{
    debug_assert(isInitialized);

    serializeBinary(archive, bounds);
    serializeBinary(archive, heightMap);
    serializeBinary(archive, normalMap);
    if (colorWidth != 0)
    {
        serializeBinary(archive, colorMap);
    }
    if (blendWidth != 0)
    {
        for (uint idx = 0; idx < MaxBlendLayers; ++idx)
        {
            serializeBinary(archive, blendMap[idx]);
        }
    }
}


// World Region Data
WorldRegion::RegionData::RegionData(const Config& config)
    : TerrainData(config.regionWidth,
                  config.regionHeightMap, config.regionNormalMap, config.regionColorMap, 0)
    , m_buffer(bufferSize())
{
    init(m_buffer.data(), m_buffer.size());
}


// Chunk
WorldRegion::Chunk::Chunk(const Config& config)
    : TerrainData(config.chunkWidth,
                  config.chunkWidth, config.chunkWidth,
                  config.chunkWidth, config.chunkWidth)
{
    numLayers = 1;
    materials.fill(int2(0));

    layerWeights.fill(0);
    layerWeights[0] = (width + 1) * (width + 1);
}
void WorldRegion::Chunk::init(const int2& chunkIndex, ubyte* buffer, uint bufferSize)
{
    TerrainData::init(buffer, bufferSize);
    index = chunkIndex;
    density = 1;
}
bool WorldRegion::Chunk::updateBlend(const int2& layerMaterials, const int2& begin, const int2& end,
                                     const FlatArrayWrapper<float3>& data, FlatArray<Blend>& buffer)
{
    debug_assert(blendWidth == width);

    // Find exist
    uint existLayer = std::find(materials.begin(), materials.end(), layerMaterials) - materials.begin();
    FlatArrayWrapper<Blend>* source = existLayer != MaxBlendLayers ? &blendMap[existLayer] : nullptr;

    // Fill temporary buffer
    buffer.reset(width + 1, width + 1);
    if (source)
    {
        std::copy(source->begin(), source->end(), buffer.begin());
    }
    else
    {
        buffer.fill(Blend(0, 0));
    }
    int2 chunkBegin = index * width;
    int2 localBegin = begin - chunkBegin;
    int2 localEnd = end - chunkBegin;
    for (int2 idx : makeRange(ls::max(int2(0), localBegin), ls::min(int2(width + 1), localEnd + 1)))
    {
        // New texel value
        float2 dest = float2(data[idx - localBegin]);
        float mixFactor = data[idx - localBegin].z;

        // Blend with old
        if (!source)
        {
            dest.y *= mixFactor;
        }
        else
        {
            // Source 'color'
            Blend sourceTexel = (*source)[idx].x;
            float sourceAlpha = unorm2real<float>(sourceTexel.y);
            float2 sourceValue = unorm2real<float>(sourceTexel.x);
            sourceValue.x = 1.0f - sourceValue.y;
            sourceValue *= sourceAlpha;

            // Dest 'color'
            float destAlpha = dest.y;
            float2 destValue = float2(1.0f - dest.x, dest.x);
            destValue *= destAlpha;

            // Blend
            float2 finalValue = lerp(sourceValue, destValue, mixFactor);
            float finalAlpha = ls::min(1.0f, finalValue.x + finalValue.y);
            if (finalAlpha < epsilon())
            {
                dest = float2(0.0f, 0.0f);
            }
            else
            {
                dest.x = finalValue.y / finalAlpha;
                dest.y = finalAlpha;
            }
        }

        // Store
        buffer[idx].x = real2unorm<ubyte>(dest.x);
        buffer[idx].y = real2unorm<ubyte>(dest.y);
    }

    // Copy to existing
    if (source)
    {
        std::copy(buffer.begin(), buffer.end(), source->begin());
    }
    else
    {
        debug_assert(numLayers < MaxBlendLayers);
        std::copy(buffer.begin(), buffer.end(), blendMap[numLayers].begin());
        materials[numLayers] = layerMaterials;
        ++numLayers;
    }

    return true;
}
void WorldRegion::Chunk::updateColor(const vector<float4>& materialColors)
{
    auto hermi = [](float t)
    {
        return t * t * (3.0 - 2.0 * t);
    };
    for (int2 idx : makeRange(int2(0), int2(width)))
    {
        float4 resultColor = 0.0f;
        for (uint layer = 0; layer < numLayers; ++layer)
        {
            float4 firstTextureColor = materialColors[materials[layer].x];
            float4 secondTextureColor = materialColors[materials[layer].y];
            Blend blend = blendMap[layer][idx];
            float intermix = unorm2real<float>(blend.x);
            float4 layerColor = lerp(firstTextureColor, secondTextureColor, hermi(intermix));
            float alpha = unorm2real<float>(blend.y);
            resultColor = lerp(resultColor, layerColor, hermi(alpha));
        }
        colorMap[idx] = Color(real2unorm<ubyte>(resultColor.x),
                              real2unorm<ubyte>(resultColor.y),
                              real2unorm<ubyte>(resultColor.z),
                              real2unorm<ubyte>(resultColor.w));
    }
}
void WorldRegion::Chunk::serialize(Archive<Buffer> archive)
{
    serializeBinary(archive, numLayers);
    serializeBinary(archive, materials);

    TerrainData::serialize(archive);
}
float WorldRegion::Chunk::height(const float2& local) const
{
    sint chunkWidth = (sint) width;
    sint x0 = clamp(static_cast<sint>(floor(local.x)), 0, chunkWidth);
    sint z0 = clamp(static_cast<sint>(floor(local.y)), 0, chunkWidth);
    sint x1 = clamp(x0 + 1, 0, chunkWidth);
    sint z1 = clamp(z0 + 1, 0, chunkWidth);
    float dx = local.x - x0;
    float dz = local.y - z0;

    float h00 = heightMap[int2(x0, z0)];
    float h01 = heightMap[int2(x0, z1)];
    float h10 = heightMap[int2(x1, z0)];
    float h11 = heightMap[int2(x1, z1)];

    if (dz > dx)
        return h01 + (1.0f - dz) * (h00 - h01) + dx * (h11 - h01);
    else
        return h10 + dz * (h11 - h10) + (1.0f - dx) * (h00 - h10);
}
AABB WorldRegion::Chunk::aabb() const
{
    const double2 beginPosition = double2(static_cast<double>(index.x) * width,
                                          static_cast<double>(index.y) * width);
    return AABB(double3(beginPosition.x, min(), beginPosition.y),
                double3(beginPosition.x + width, max(), beginPosition.y + width));
}


// World Region Detail
WorldRegion::ChunksData::ChunksData(const Config& config, const int2& regionIndex)
    : m_config(config)
    , m_chunks(m_config, m_config.regionInChunks, m_config.regionInChunks)
    , m_stride(m_chunks(0, 0).bufferSize())
    , m_buffer(m_chunks.length() * m_stride)
{
    // Initializer
    uint offset = 0;
    int2 baseIndex = regionIndex * m_chunks.size();
    auto initializer = [this, &offset, baseIndex](Chunk& chunk, const int2& chunkIndex)
    {
        chunk.init(chunkIndex + baseIndex, &m_buffer[offset], m_stride);
        offset += m_stride;
    };

    // Init
    m_chunks.process(initializer);
}
void WorldRegion::ChunksData::serialize(Archive<Buffer> archive)
{
    serializeBinary(archive, m_staticObjects);
    for (Chunk& chunk : m_chunks)
    {
        chunk.serialize(archive);
    }
}


// Region
WorldRegion::WorldRegion(const int2& index, 
                         const Config& config, 
                         const Callbacks& callbacks,
                         FileSystemInterface& fileSystem, 
                         Worker& worker, 
                         const bool discardSaved)
    : m_index(index)
    , m_config(config)
    , m_callbacks(callbacks)
    , m_fileSystem(fileSystem)
    , m_worker(worker)
{
    if (!discardSaved)
    {
        openStreams(StreamAccess::Read);
    }
}
WorldRegion::~WorldRegion()
{
    wait();
    flush();
    unloadRegion();
}


// Region load/save
void WorldRegion::openStreams(const StreamAccessFlag flag)
{
    const string baseName = makeFileName();
    m_regionStream = m_fileSystem.openStream(baseName + ".dat", flag);
    m_chunksStream = m_fileSystem.openStream(baseName + ".bin", flag);
}
void WorldRegion::loadRegion()
{
    // Skip redundant call
    if (isRegionDataRequested())
    {
        return;
    }

    // Start async loading
    auto asyncLoad = [this]()
    {
        RegionDataPtr region = make_unique<RegionData>(m_config);
        if (m_regionStream)
        {
            Buffer buffer = Buffer::pull(*m_regionStream);
            region->serialize(Archive<Buffer>(buffer, ArchiveRead));
        }
        m_regionData = region.get();
        m_callbacks.asyncLoadRegion(*this);
        return region;
    };
    m_callbacks.preLoadRegion(*this);
    m_regionDataFuture = m_worker.schedule(asyncLoad);
}
void WorldRegion::unloadRegion()
{
    // Skip redundant call
    if (!isRegionDataRequested())
    {
        return;
    }

    // Unload chunks
    unloadChunks();

    // Unload all
    m_callbacks.unloadRegion(*this);
    m_regionDataFuture = std::future<RegionDataPtr>();
    m_regionData = nullptr;
    m_regionDataHolder = nullptr;
}
void WorldRegion::loadChunks()
{
    // Skip redundant call
    if (isChunksDataRequested())
    {
        return;
    }

    // Load region data
    loadRegion();

    // Start async loading
    auto asyncLoad = [this]()
    {
        ChunksDataPtr chunks = make_unique<ChunksData>(m_config, m_index);
        if (m_chunksStream)
        {
            Buffer buffer = Buffer::pull(*m_chunksStream);
            chunks->serialize(Archive<Buffer>(buffer, ArchiveRead));
        }
        m_chunksData = chunks.get();
        m_callbacks.asyncLoadChunks(*this);
        return chunks;
    };
    m_callbacks.preLoadChunks(*this);
    m_chunksDataFuture = m_worker.schedule(asyncLoad);
}
void WorldRegion::unloadChunks()
{
    // Skip redundant call
    if (!isChunksDataRequested())
    {
        return;
    }

    // Unload all
    m_callbacks.unloadChunks(*this);
    m_chunksDataFuture = std::future<ChunksDataPtr>();
    m_chunksData = nullptr;
    m_chunksDataHolder = nullptr;
}
void WorldRegion::flush()
{
    if (isReady(m_regionDataFuture))
    {
        m_regionDataHolder = m_regionDataFuture.get();
        m_callbacks.postLoadRegion(*this);
    }
    if (isReady(m_chunksDataFuture))
    {
        m_chunksDataHolder = m_chunksDataFuture.get();
        m_callbacks.postLoadChunks(*this);
    }
}
void WorldRegion::wait()
{
    if (m_regionDataFuture.valid())
    {
        m_regionDataFuture.wait();
    }
    if (m_chunksDataFuture.valid())
    {
        m_chunksDataFuture.wait();
    }

    flush();
}
void WorldRegion::save()
{
    // Load all
    loadRegion();
    loadChunks();
    wait();

    // Open streams for save
    openStreams(StreamAccess::WriteNew);

    // Save region data
    Buffer regionData;
    m_regionDataHolder->serialize(Archive<Buffer>(regionData, ArchiveWrite));
    regionData.push(*m_regionStream);

    // Save chunks data
    Buffer chunksData;
    m_chunksDataHolder->serialize(Archive<Buffer>(chunksData, ArchiveWrite));
    chunksData.push(*m_chunksStream);

    // Re-open streams
    openStreams(StreamAccess::Read);
}


// Gets
int2 WorldRegion::index() const
{
    return m_index;
}
const WorldRegion::Config& WorldRegion::config() const
{
    return m_config;
}
bool WorldRegion::isRegionDataRequested() const
{
    return m_regionDataFuture.valid() || !!m_regionDataHolder;
}
bool WorldRegion::isRegionDataReady() const
{
    return !!m_regionDataHolder;
}
bool WorldRegion::isChunksDataRequested() const
{
    return m_chunksDataFuture.valid() || !!m_chunksDataHolder;
}
bool WorldRegion::isChunksDataReady() const
{
    return !!m_chunksDataHolder;
}
WorldRegion::RegionData& WorldRegion::regionData()
{
    RegionData* ptr = m_regionData.load();
    debug_assert(ptr);
    return *ptr;
}
WorldRegion::ChunksData& WorldRegion::chunksData()
{
    ChunksData* ptr = m_chunksData.load();
    debug_assert(ptr);
    return *ptr;
}


// Index operations
int2 WorldRegion::local2global(const int2& local) const
{
    return local + m_index * m_config.regionInChunks;
}
int2 WorldRegion::global2local(const int2& global) const
{
    return global - m_index * m_config.regionInChunks;
}
int2 WorldRegion::beginChunk() const
{
    return m_index * m_config.regionInChunks;
}
int2 WorldRegion::endChunk() const
{
    return (m_index + 1) * m_config.regionInChunks;
}
string WorldRegion::makeFileName() const
{
    return format("{*}{*}", Hex(4, true),
                  static_cast<ushort>(m_index.x),
                  static_cast<ushort>(m_index.y));
}


// Data Access
WorldRegion::Chunk* WorldRegion::findChunkByIndex(const int2& globalIndex)
{
    const sint width = static_cast<sint>(m_config.regionInChunks);
    const int2 localIndex = globalIndex - m_index * width;
    if (localIndex.x < 0 || localIndex.y < 0 || localIndex.x >= width || localIndex.y >= width)
        return nullptr;

    return &(*m_chunksData)[localIndex];
}
WorldRegion::Chunk* WorldRegion::findChunkByPosition(const double2& position)
{
    const double2 local = position / m_config.regionWidth - static_cast<double2>(m_index);
    if (local.x < 0 || local.y < 0 || local.x >= 1 || local.y >= 1)
        return nullptr;
    const sint widthInChunks = static_cast<sint>(m_config.regionInChunks);
    const int2 chunkIndex = static_cast<int2>(local * static_cast<double>(widthInChunks));
    return &(*m_chunksData)[min(chunkIndex, int2(widthInChunks - 1))];
}
float WorldRegion::getHeight(const double2& pos)
{
    static const float Invalid = 0.0f;
    const float chunkSize = static_cast<float>(m_config.chunkWidth);

    const double2 vertex = floor(pos);
    const double2 chunkFactor = floor(vertex / chunkSize);
    const int2 chunkIndex = static_cast<int2>(chunkFactor);

    WorldRegion::Chunk* chunk = findChunkByIndex(chunkIndex);
    if (!chunk)
        return Invalid;

    const double2 localPos = pos - static_cast<double2>(chunkIndex) * chunkSize;
    return chunk->height(static_cast<float2>(localPos));
}
