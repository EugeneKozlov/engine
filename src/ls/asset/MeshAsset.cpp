#include "pch.h"
#include "ls/asset/MeshAsset.h"
#include "ls/gapi/Renderer.h"

using namespace ls;
MeshAsset::MeshAsset()
{

}
MeshAsset::~MeshAsset()
{
    // Release GAPI
    Renderer* rr = renderer();
    if (rr)
    {
        deallocateGapi(*rr);
    }

    // Release RAM
    dropRam();
}


// Implement Asset
string MeshAsset::extension() const
{
    return ".bin";
}
void MeshAsset::doSerialize(Archive<Buffer> archive)
{
    if (archive.isWrite())
    {
        LS_THROW(!m_vertexData.empty() && !m_indexData.empty(),
                 LogicException, "No data stored in RAM");
    }

    serializeBinary<uint>(archive, m_numVertices);
    serializeBinary<uint>(archive, m_numIndices);
    serializeBinary<uint>(archive, m_vertexStride);
    serializeBinary<uint>(archive, m_indexStride);
    serializeBinary<IndexFormat>(archive, m_indexType);
    serializeBinary<uint>(archive, m_numSubsets);
    serializeBinary(archive, m_subsets);
    serializeBinary<AABB>(archive, m_aabb);
    serializeBinary(archive, m_uniformBuffer);
    serializeBinary(archive, m_parameterData);
    serializeBinary(archive, m_vertexData);
    serializeBinary(archive, m_indexData);
}
void MeshAsset::doRestore()
{
    Renderer* rr = renderer();
    if (rr)
    {
        allocateGapi(*rr);
    }
}


// Update
void MeshAsset::setAabb(const AABB& aabb)
{
    m_aabb = aabb;
}
void MeshAsset::addSubset(uint materialIndex, uint numIndices)
{
    debug_assert(m_numSubsets < MaxSubsets);

    Subset subset;
    subset.offset = m_numSubsets == 0
        ? 0
        : (m_subsets[m_numSubsets - 1].offset + m_subsets[m_numSubsets - 1].numIndices);
    subset.numIndices = numIndices;
    subset.materialIndex = materialIndex;

    m_subsets[m_numSubsets++] = subset;
}
void MeshAsset::setUniformBuffer(const UniformBuffer& buffer)
{
    m_uniformBuffer = buffer;
}


// GAPI
void MeshAsset::allocateGapi(Renderer& renderer)
{
    BufferHandle vertexBuffer = renderer
        .createBuffer(m_vertexData.size(), BufferType::Vertex, 
                       ResourceUsage::Constant, m_vertexData.data());
    BufferHandle indexBuffer = renderer
        .createBuffer(m_indexData.size(), BufferType::Index,
                       ResourceUsage::Constant, m_indexData.data());

    m_geometry.vertexBuffers[0] = vertexBuffer;
    m_geometry.numVertexBuffers = 1;
    m_geometry.indexBuffer = indexBuffer;
    m_geometry.indexFormat = m_indexType;

    m_gapiReady = true;
}
void MeshAsset::deallocateGapi(Renderer& renderer)
{
    // Destroy VBs
    for (uint vbIndex = 0; vbIndex < m_geometry.numVertexBuffers; ++vbIndex)
    {
        BufferHandle buffer = m_geometry.vertexBuffers[vbIndex];
        if (!!buffer)
        {
            renderer.destroyBuffer(buffer);
        }
    }

    // Destroy IBs
    if (!!m_geometry.indexBuffer)
    {
        renderer.destroyBuffer(m_geometry.indexBuffer);
    }

    m_geometry = GeometryBucket();
    m_gapiReady = false;
}
void MeshAsset::dropRam()
{
    m_vertexData.clear();
    m_vertexData.shrink_to_fit();
    m_indexData.clear();
    m_indexData.shrink_to_fit();
}
bool MeshAsset::isGapiReady() const
{
    return m_gapiReady;
}
const GeometryBucket& MeshAsset::gapiGeometry() const
{
    debug_assert(isGapiReady());
    return m_geometry;
}


// Gets
uint MeshAsset::numSubsets() const
{
    return m_numSubsets;
}
const MeshAsset::Subset& MeshAsset::subset(const uint subsetIndex) const
{
    debug_assert(subsetIndex < m_numSubsets);
    return m_subsets[subsetIndex];
}
const AABB& MeshAsset::aabb() const
{
    return m_aabb;
}
const MeshAsset::UniformBuffer& MeshAsset::uniformBuffer() const
{
    return m_uniformBuffer;
}
