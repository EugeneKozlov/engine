/// @file ls/asset/EffectLibraryAsset.h
/// @brief Effect Library Asset
#pragma once

#include "ls/common.h"
#include "ls/asset/Asset.h"
#include "ls/gapi/common.h"

namespace ls
{
    /// @addtogroup LsAsset
    /// @{

    /// @brief Effect Library Asset
    class EffectLibraryAsset
        : public AssetT<AssetType::EffectLibrary, AssetCategory::Simple>
    {
    public:
        /// @brief Ctor
        EffectLibraryAsset();
        /// @brief Dtor
        ~EffectLibraryAsset();
    private:
        /// @name Implement Asset
        /// @{
        virtual string extension() const override;
        virtual void doSerialize(Archive<Buffer> archive) override;
        virtual void doRestore() override;
        /// @}
    private:
        string m_text;
    };

    /// @}
}