/// @file ls/asset/MaterialCache.h
/// @brief Material Cache
#pragma once

#include "ls/common.h"
#include "ls/asset/MaterialAsset.h"
#include "ls/gapi/common.h"

namespace ls
{
    struct MaterialCacheContext;
    struct MaterialCacheMaterial;

    /// @addtogroup LsAsset
    /// @{

    /// @brief Material Cache
    /// 
    /// Provides cache to combine 'material' part of GAPI pipeline state 
    /// and 'context' part.
    /// All shaders are stored until cache reset.
    class MaterialCache
    {
    public:
        /// @brief Ctor
        MaterialCache(Renderer& renderer);
        /// @brief Dtor
        ~MaterialCache();
        /// @brief Register context
        /// @param handle
        ///   Internal handle to manage context
        /// @param desc
        ///   Pipeline state description
        /// @param mode
        ///   Shader mode used to switch shader program
        MaterialCacheContext* registerContext(const StateDesc& stateDesc,
                                              const MaterialAsset::ShaderMode mode);
        /// @brief Unregister context
        void unregisterContext(MaterialCacheContext* handle);
        /// @brief Find or create internal material
        /// @note Thread-safe
        MaterialCacheMaterial* findOrCreateMaterial(MaterialAsset& materialAsset);
        /// @brief Get material state
        StateHandle gapiState(MaterialCacheContext* context, MaterialCacheMaterial* material);
        /// @brief Reset cache
        void reset();
    private:
    private:
        Renderer& m_renderer; ///< Renderer

        list<MaterialCacheContext> m_contexts;

        list<MaterialCacheMaterial> m_materials;
        std::mutex m_mutex;
    };

    /// @}
}