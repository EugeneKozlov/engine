#include "pch.h"
#include "ls/asset/ModelAsset.h"

#include "ls/core/INIReader.h"
#include "ls/core/Worker.h"

using namespace ls;
ModelAsset::ModelAsset()
{

}
ModelAsset::~ModelAsset()
{
}
void ModelAsset::setAabb(const AABB& aabb)
{
    m_aabb = aabb;
}


// Implement Asset
void ModelAsset::doReadConfig(INIReader& ini)
{
    // #TODO Rewrite
    const INISection& header = ini.section("");

    // Load LODs info
    m_numLods = 0;
    for (uint lodIndex = 0; lodIndex < MaxLods; ++lodIndex)
    {
        // Make LoD 'name'
        const string lodIndexStr = toString(lodIndex);

        // Get LoD
        Lod& lod = m_lods[m_numLods];

        // Get mesh name
        lod.meshName = getOrDefault(header, lodIndexStr);
        if (lod.meshName.empty())
            break;

        // New level
        lod.switchSize = fromString<float>(getOrThrow<IoException>(
            header, lodIndexStr + ".switchSize", "[LOD switch size is not specified]"));
        lod.inoutGap = fromString<float>(getOrThrow<IoException>(
            header, lodIndexStr + ".inoutGap", "[LOD in-out gap is not specified]"));

        // Load materials
        const string materials = getOrDefault(header, lodIndexStr + ".materials");
        vector<string> materialNames = filterNonEmpty(split(materials, ','));
        LS_THROW(materialNames.size() <= MeshAsset::MaxSubsets,
                 IoException, "Too many subsets for LOD #", lodIndex);

        lod.numMaterials = materialNames.size();
        std::copy(materialNames.begin(), materialNames.end(), lod.materialsNames.begin());

        // Count LoD
        ++m_numLods;
    }

    // Invalid number of LoDs
    LS_THROW(m_numLods > 0, IoException, "Model has no LoDs");

    // No extra fields should exist
    LS_THROW(header.hasUnused(), IoException, "Unknown field [", header.firstUnused(), "]");

    // Load first LOD
    loadLevel(0);
    LS_THROW(m_lods[0].mesh,
             ArgumentException,
             "Model [", name(), "] failed to load base LOD");

    // Update AABB
    m_aabb = m_lods[0].mesh->aabb();
}
void ModelAsset::doSerializeConfig(Archive<Buffer> archive)
{
    serializeBinary<AABB>(archive, m_aabb);
    serializeBinary<uint>(archive, m_numLods);
    for (uint lodIndex = 0; lodIndex < m_numLods; ++lodIndex)
    {
        Lod& lod = m_lods[lodIndex];
        serializeBinary<float>(archive, lod.switchSize);
        serializeBinary<float>(archive, lod.inoutGap);
        serializeBinary<string>(archive, lod.meshName);
        serializeBinary<uint>(archive, lod.numMaterials);
        for (uint materialIndex = 0; materialIndex < lod.numMaterials; ++materialIndex)
        {
            serializeBinary<string>(archive, lod.materialsNames[materialIndex]);
        }
    }
}
void ModelAsset::doRestore()
{
}


// Load/unload
void ModelAsset::loadModelLevel(Lod& lod)
{
    debug_assert(lod.state == State::Loading);
    lod.corrupted = false;

    // Load mesh
    lod.mesh = loadAssetSilent<MeshAsset>(lod.meshName);
    lod.corrupted |= !lod.mesh;

    // For each material
    for (uint subsetIndex = 0; subsetIndex < lod.numMaterials; ++subsetIndex)
    {
        // Load
        lod.materials[subsetIndex]
            = loadAssetSilent<MaterialAsset>(lod.materialsNames[subsetIndex]);
        lod.corrupted |= !lod.materials[subsetIndex];
    }

    // Lod is loaded
    lod.state = State::Loaded;
    // #TODO Add logging
}
uint ModelAsset::findLevel(const uint minLodIndex)
{
    // Request all LoDs starting from minimum
    for (uint lodIndex = clampLevel(minLodIndex); lodIndex < m_numLods; ++lodIndex)
    {
        requestLevel(lodIndex);
    }
    // Get first loaded LoD starting from minimum
    for (uint lodIndex = clampLevel(minLodIndex); lodIndex < m_numLods; ++lodIndex)
    {
        if (m_lods[lodIndex].state == State::Ready)
            return lodIndex;
    }
    return MaxLods;
}
bool ModelAsset::requestLevel(const uint lodIndex)
{
    Lod& lod = m_lods[clampLevel(lodIndex)];
    switch (lod.state)
    {
    case State::Ready:
        // Ready to use
        return true;
    case State::Incomplete:
        // Will never ready to use
        return false;
    case State::Loading:
        // Just wait, async task is performing now
        return false;
    case State::Loaded:
        // Asset is already loaded, close event
        lod.asyncResult.get();
        lod.state = !lod.corrupted ? State::Ready : State::Incomplete;
        return true;
    case State::NonLoaded:
        // Start loading
        lod.state = State::Loading;
        lod.asyncResult = worker().schedule([&lod, this]()
        {
            loadModelLevel(lod);
        });
        return false;
    default:
        return false;
    }
}
void ModelAsset::loadLevel(const uint lodIndex)
{
    Lod& lod = m_lods[clampLevel(lodIndex)];

    // Ready to use or will never ready to use
    if (lod.state == State::Ready || lod.state == State::Incomplete)
        return;

    // Load level if not loaded
    if (lod.state == State::NonLoaded)
    {
        lod.state = State::Loading;
        loadModelLevel(lod);
        debug_assert(lod.state == State::Loaded);
    }

    // Async task is performing right now, wait
    if (lod.state == State::Loading)
    {
        debug_assert(lod.asyncResult.valid());
        lod.asyncResult.wait();
        debug_assert(lod.state == State::Loaded);
    }

    // Asset is loaded, close event
    debug_assert(lod.state == State::Loaded);
    if (lod.asyncResult.valid())
    {
        lod.asyncResult.get();
    }
    lod.state = !lod.corrupted ? State::Ready : State::Incomplete;
}
void ModelAsset::dropLevel(const uint lodIndex)
{
    Lod& lod = m_lods[lodIndex];

    // Load completely if async task is performing now
    if (lod.state == State::Loading)
    {
        loadLevel(lodIndex);
    }

    // Release holders
    lod.mesh = nullptr;
    for (uint materialIndex = 0; materialIndex < lod.numMaterials; ++materialIndex)
    {
        lod.materials[materialIndex] = nullptr;
    }
    lod.state = State::NonLoaded;
}


// Gets
uint ModelAsset::numLevels() const
{
    return m_numLods;
}
float ModelAsset::switchSize(const uint lodIndex) const
{
    debug_assert(lodIndex < m_numLods);
    return m_lods[lodIndex].switchSize;
}
float ModelAsset::inoutGap(const uint lodIndex) const
{
    debug_assert(lodIndex < m_numLods);
    return m_lods[lodIndex].inoutGap;
}
MeshAsset* ModelAsset::mesh(const uint lodIndex)
{
    debug_assert(lodIndex < m_numLods);
    return m_lods[lodIndex].state == State::Ready
        ? m_lods[lodIndex].mesh.get()
        : nullptr;
}
uint ModelAsset::numMaterials(const uint lodIndex) const
{
    debug_assert(lodIndex < m_numLods);
    return m_lods[lodIndex].state == State::Ready
        ? m_lods[lodIndex].numMaterials
        : 0;
}
MaterialAsset* ModelAsset::material(const uint lodIndex, const uint materialIndex)
{
    debug_assert(lodIndex < m_numLods);
    return m_lods[lodIndex].state == State::Ready
        ? m_lods[lodIndex].materials[materialIndex].get()
        : nullptr;
}
const AABB& ModelAsset::aabb() const
{
    return m_aabb;
}