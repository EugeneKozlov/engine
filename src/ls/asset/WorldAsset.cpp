#include "pch.h"
#include "ls/asset/WorldAsset.h"

#include "ls/io/FileSystemInterface.h"

using namespace ls;


// World
WorldAsset::WorldAsset()
    : config()
{

}
void WorldAsset::setCallbacks(const WorldRegion::Callbacks& callbacks)
{
    debug_assert(m_regions.empty());
    m_callbacks = callbacks;
}


// World - Implement Asset
string WorldAsset::extension() const
{
    return ".dat";
}
void WorldAsset::doPostInitialize()
{
    m_fileSystem = fileSystem().openSystem(name() + ".dir");
}
void WorldAsset::doSerialize(Archive<Buffer> archive)
{
    serializeBinary(archive, config);
    serializeBinary(archive, materials);
    serializeBinary(archive, globalComponents);
    serializeBinary(archive, dynamicEntities);
}


// World loading
WorldRegion& WorldAsset::findOrCreate(const int2& regionIndex)
{
    auto iter = m_regions.find(regionIndex);
    if (iter == m_regions.end())
    {
        // #TODO Fix 'discardSaved' param
        auto newRegion = make_shared<WorldRegion>(
            regionIndex, config, m_callbacks, *m_fileSystem, worker(), false);
        iter = m_regions.emplace(regionIndex, newRegion).first;
    }
    return *iter->second;
}
void WorldAsset::loadRegion(const int2& regionIndex)
{
    // Load region
    findOrCreate(regionIndex).loadRegion();
}
void WorldAsset::unloadRegion(const int2& regionIndex)
{
    // Unload region if exist
    if (m_regions.count(regionIndex) != 0)
    {
        m_regions[regionIndex]->unloadRegion();
        m_regions.erase(regionIndex);
    }
}
void WorldAsset::loadRegionChunks(const int2& regionIndex)
{
    // Load chunks
    findOrCreate(regionIndex).loadChunks();
}
void WorldAsset::unloadRegionChunks(const int2& regionIndex)
{
    // Unload chunks if exist
    if (m_regions.count(regionIndex) != 0)
    {
        m_regions[regionIndex]->unloadChunks();
    }
}
void WorldAsset::flushAll()
{
    for (auto& iter : m_regions)
    {
        WorldRegion& region = *iter.second;
        region.flush();
    }
}
void WorldAsset::waitAll()
{
    for (auto& iter : m_regions)
    {
        WorldRegion& region = *iter.second;
        region.wait();
    }
}
void WorldAsset::unloadAll()
{
    m_regions.clear();
}
void WorldAsset::save()
{
    // Save asset data
    Asset::save();

    // Save all regions
    for (auto& iter : m_regions)
    {
        WorldRegion& region = *iter.second;
        region.save();
    }
}


// World gets
void WorldAsset::chunk2region(const int2& chunkIndex, int2& regionIndex, int2& localIndex) const
{
    regionIndex = upcast(chunkIndex, config.regionInChunks, &localIndex);
}
int2 WorldAsset::chunk2region(const int2& chunkIndex) const
{
    int2 regionIndex, localIndex;
    chunk2region(chunkIndex, regionIndex, localIndex);
    return regionIndex;
}
std::pair<WorldAsset::RegionIterator, WorldAsset::RegionIterator> WorldAsset::regions()
{
    return make_pair(m_regions.begin(), m_regions.end());
}
WorldRegion* WorldAsset::findRegion(const int2& regionIndex)
{
    // Find region
    auto iterRegion = m_regions.find(regionIndex);
    if (iterRegion == m_regions.end())
        return nullptr;
    
    return iterRegion->second.get();
}
WorldRegion::Chunk* WorldAsset::findChunk(const int2& chunkIndex)
{
    int2 localChunkIndex;
    const int2 regionIndex = upcast(chunkIndex,
                                    config.regionInChunks,
                                    &localChunkIndex);
    
    // Find region
    auto iterRegion = m_regions.find(regionIndex);
    if (iterRegion == m_regions.end())
        return nullptr;
    
    // Find chunk
    WorldRegion& region = *iterRegion->second;
    if (!region.isChunksDataReady())
        return nullptr;

    return &region.chunksData()[localChunkIndex];
}
float WorldAsset::getHeight(const double2& pos)
{
    const int2 regionIndex = static_cast<int2>(floor(pos / config.regionWidth));
    auto region = findOrDefault(m_regions, regionIndex);
    if (!*region)
        return 0.0f;

    return (**region).getHeight(pos);
}
