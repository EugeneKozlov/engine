/// @file ls/asset/TextureAsset.h
/// @brief Texture Asset
#pragma once

#include "ls/common.h"
#include "ls/asset/Asset.h"
#include "ls/gapi/common.h"

namespace ls
{
    class Renderer;

    /// @addtogroup LsAsset
    /// @{

    /// @brief Texture Asset
    class TextureAsset
        : public AssetT<AssetType::Texture, AssetCategory::ConfigData>
    {
    public:
        /// @brief Sampling Mode
        enum class Sampling : ubyte
        {
            /// @brief Clamp texture coordinates to [0.5/size, 1 - 0.5/size]
            Clamp,
            /// @brief Wrap texture coordinates to range [0, 1]
            Wrap
        };
        /// @brief Max anisotropy filtering level
        static const uint MaxAnisotropy = 16;
    public:
        /// @brief Ctor
        TextureAsset();
        /// @brief Dtor
        ~TextureAsset();

        /// @brief Set wrapping
        void setWrapping(const bool enabled);

        /// @brief Compress internal storage
        /// @throw LogicException if asset is not ready
        /// @throw LogicException if texture is empty
        /// @throw LogicException if texture is not RGBA8
        void compress(const Compression compression);
        /// @brief Get RAM storage
        TextureStorage& storage();
        /// @brief Drop buffers stored in RAM
        void dropRam();
        /// @brief Refresh GAPI
        void refreshGapi();

        /// @brief Is GAPI ready?
        bool isGapiReady() const;
        /// @brief Get GAPI resource
        /// @pre GAPI is ready
        Texture2DHandle gapiResource() const;
    private:
        /// @name Implement Asset
        /// @{
        virtual string extension() const override;
        virtual void doReadConfig(INIReader& ini) override;
        virtual void doSerializeConfig(Archive<Buffer> archive) override;
        virtual void doSerialize(Archive<Buffer> archive) override;
        virtual void doRestore() override;
        /// @}

        /// @brief Allocate GAPI buffers
        void allocateGapi(Renderer& renderer);
        /// @brief Deallocate GAPI buffers
        void deallocateGapi(Renderer& renderer);
    private:
        /// @name Config
        /// @{
        Sampling m_sampling = Sampling::Clamp;
        bool m_linearFilter = true;
        sbyte m_anisotropyLevel = -1; ///< Anisotropy level, 0 to disable, -1 to use global
        /// @}

        TextureStorage m_storage; ///< RAM storage

        bool m_gapiReady = false; 
        Texture2DHandle m_texture = nullptr; ///< GAPI storage
    };

    /// @}
}