#include "pch.h"
#include "ls/asset/TextureAsset.h"

#include "ls/core/GlobalVariableT.h"
#include "ls/core/INIReader.h"
#include "ls/gapi/Renderer.h"
#include "ls/gapi/detail/TextureLoader.h"

#include <squish/squish.h>

using namespace ls;
// Config
GlobalVariableT<uint> g_defaultAnisotropy(
    "render:defaultAnisotropy", "rrAnisotropy", 
    GlobalVariableFlag::Modifiable | GlobalVariableFlag::Console,
    "Default anisotropy filtering level\n"
    "Must be in range [0, MaxAnisotropy]\n"
    "MaxAnisotropy = 16\n",
    4, [](const uint value) { return value >= 0 && value <= TextureAsset::MaxAnisotropy; });


// Main
TextureAsset::TextureAsset()
{
}
TextureAsset::~TextureAsset()
{
    // Release GAPI
    Renderer* rr = renderer();
    if (rr)
    {
        deallocateGapi(*rr);
    }

    // Release RAM
    dropRam();
}


// Sets
void TextureAsset::setWrapping(const bool enabled)
{
    m_sampling = enabled
        ? TextureAsset::Sampling::Wrap
        : TextureAsset::Sampling::Clamp;
}


// Implement Asset
string TextureAsset::extension() const
{
    return ".dds";
}
void TextureAsset::doReadConfig(INIReader& ini)
{
    // #TODO Rewrite
    const INISection& header = ini.section("");

    // Read sampling
    const string isWrappedValue = getOrDefault(header, "wrap", "");
    if (!isWrappedValue.empty())
    {
        m_sampling = isWrappedValue == "1" ? Sampling::Wrap : Sampling::Clamp;
    }

    // Read filtering
    const string linearFilterValue = getOrDefault(header, "filter", "");
    if (!linearFilterValue.empty())
    {
        m_linearFilter = linearFilterValue == "1";
    }

    // Read anisotropy
    const string anisotropyValue = getOrDefault(header, "anisotropy", "");
    if (!anisotropyValue.empty())
    {
        m_anisotropyLevel = fromString<sbyte>(anisotropyValue);
        LS_THROW(m_anisotropyLevel >= 0 && m_anisotropyLevel <= MaxAnisotropy,
                 FormatException,
                 "Anisotropy must be in [0, ", static_cast<uint>(MaxAnisotropy), "] range");
    }
}
void TextureAsset::doSerializeConfig(Archive<Buffer> archive)
{
    serializeBinary<Sampling>(archive, m_sampling);
    serializeBinary<bool>(archive, m_linearFilter);
    serializeBinary<sbyte>(archive, m_anisotropyLevel);
}
void TextureAsset::doSerialize(Archive<Buffer> archive)
{
    if (archive.isWrite())
    {
        LS_THROW(!m_storage.empty(), IoException, "Texture is empty (GAPI only?)");
        // Save flat texture
        if (m_storage.layers() == 1)
        {
            gli::texture2D newStorage = m_storage[0];
            gapi::saveDDS(archive.stream(), newStorage);
        }
        else
        {
            gapi::saveDDS(archive.stream(), m_storage);
        }
    }
    else
    {
        gli::texture storage = gapi::loadDDS(archive.stream());
        reconstruct(m_storage, storage);
    }
}
void TextureAsset::doRestore()
{
    Renderer* rr = renderer();
    if (rr)
    {
        allocateGapi(*rr);
    }
}


// Compression
void TextureAsset::compress(const Compression compression)
{
    LS_THROW(!m_storage.empty(), LogicException, "Nothing to compress");
    // Do nothing
    if (compression == Compression::No)
        return;

    // Compress
    LS_THROW(m_storage.format() == gli::FORMAT_RGBA8_UNORM_PACK8, 
             LogicException, "Only RGBA8 texture can be compressed");
    const int flags = gapi::squishFlags(compression);
    TextureStorage compressedStorage(gapi::compressedFormat(compression),
                                     m_storage.dimensions(),
                                     m_storage.layers(), m_storage.levels() - 2);
    for (uint slice = 0; slice < m_storage.layers(); ++slice)
    {
        for (uint mipLevel = 0; mipLevel < compressedStorage.levels(); ++mipLevel)
        {
            const uint width = m_storage.dimensions().x >> mipLevel;
            const uint height = m_storage.dimensions().y >> mipLevel;
            gli::image argb = m_storage[slice][mipLevel];
            gli::image dxt = compressedStorage[slice][mipLevel];
            squish::CompressImage(static_cast<const ubyte*>(argb.data()), width, height,
                                  dxt.data(), flags);
        }
    }

    // Save
    reconstruct(m_storage, compressedStorage);
}


// GAPI
void TextureAsset::allocateGapi(Renderer& renderer)
{
    if (!m_storage.empty())
    {
        m_texture = renderer.loadTexture2D(m_storage);
        const Filter filter = m_linearFilter ? Filter::Linear : Filter::Point;
        const Addressing addressing = m_sampling == Sampling::Clamp
            ? Addressing::Clamp
            : Addressing::Wrap;
        const uint anisotropyLevel = m_anisotropyLevel >= 0
            ? m_anisotropyLevel
            : g_defaultAnisotropy.value();
        renderer.setTexture2DSampler(m_texture, filter, addressing, anisotropyLevel);

        m_gapiReady = true;
    }
}
void TextureAsset::deallocateGapi(Renderer& renderer)
{
    if (m_gapiReady)
    {
        renderer.destroyTexture2D(m_texture);
        m_gapiReady = false;
    }
}
TextureStorage& TextureAsset::storage()
{
    return m_storage;
}
void TextureAsset::dropRam()
{
    reconstruct(m_storage);
}
void TextureAsset::refreshGapi()
{
    Renderer* rr = renderer();
    if (rr)
    {
        deallocateGapi(*rr);
        allocateGapi(*rr);
    }
}


// Gets
bool TextureAsset::isGapiReady() const
{
    return m_gapiReady;
}
Texture2DHandle TextureAsset::gapiResource() const
{
    debug_assert(isGapiReady());
    return m_texture;
}
