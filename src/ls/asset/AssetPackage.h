/// @file ls/asset/AssetPackage.h
/// @brief Asset Package
#pragma once

#include "ls/common.h"
#include "ls/core/INIReader.h"
#include "ls/asset/Asset.h"
#include "ls/io/common.h"
#include "ls/io/Buffer.h"
#include "ls/io/MultiFileSystem.h"

namespace ls
{
    class AssetManager;
    class MaterialCache;
    class Worker;

    /// @addtogroup LsAsset
    /// @{

    /// @brief Asset Package
    // #TODO External add/remove asset and save
    // #TODO Keep some assets
    class AssetPackage
        : public Noncopyable
    {
    public:
        /// @brief Current Version
        static const uint Version = 5;
        /// @brief INI file name
        static const string iniName;
        /// @brief DAT file name
        static const string datName;
    public:
        /// @name Package management
        /// @{

        /// @brief Ctor
        AssetPackage(AssetManager& packageManager, 
                     FileSystem fsAsset, FileSystem fsCache);
        /// @brief Dtor
        ~AssetPackage();
        /// @brief Generate assets
        void generateAssets();
        /// @brief Load package
        /// @throw IoException if cannot read or write file
        /// @throw IoException if neither package.ini nor package.dat are not found
        /// @throw LogicException on circular reference
        void loadPackage();
        /// @brief Save package
        /// @throw IoException if cannot write file
        void savePackage();
        /// @brief Is loaded?
        bool isLoaded() const;
        /// @}

        /// @name Asset management
        /// @{

        /// @brief Create new asset
        /// @param name,type
        ///   Asset name and type
        /// @param fileName
        ///   Asset file name
        /// @throw ArgumentException if name is used
        /// @return New asset
        /// @note Thread-safe
        void createAsset(const string& name, const AssetType type,
                         const string& fileName);
        /// @brief Load asset
        /// @return Asset (guaranteed)
        /// @throw ArgumentException if asset is not found
        /// @throw std::exception thrown by asset loading
        /// @note Thread-safe
        Asset::Ptr loadAsset(const string& name);
        /// @brief Load asset silently
        /// @return Asset (may be null)
        /// @note Thread-safe
        /// @note Never throws, return @a nullptr on error
        Asset::Ptr loadAssetSilent(const string& name) noexcept;
        /// @}

        /// @name Gets
        /// @{

        /// @brief Get manager
        AssetManager& manager();
        /// @brief Get worker
        Worker& worker();
        /// @brief Get renderer
        Renderer* renderer();
        /// @brief Get material cache
        MaterialCache* materialCache();
        /// @brief Get file system
        FileSystemInterface& fileSystem();
        /// @}
    private:
        struct AssetInfo
        {
            AssetInfo() = default;
            AssetInfo(const AssetInfo& another) = default;
            AssetInfo(AssetInfo && another) noexcept = default;

            AssetType type; ///< Asset type
            string name; ///< Asset name
            string fileName; ///< Asset file name (may be empty)

            INISection headerConfig; ///< Header (config)
            bool headerDataReady = false; ///< Is header data ready?
            Buffer headerData; ///< Header (binary)

            weak_ptr<Asset> asset; ///< Asset
        };

        /// @brief Parse package INI file and read asset data
        void parsePackageIni();
        /// @brief Serialize package binary
        void serializePackage(Archive<Buffer>& archive);
        /// @brief Write package DAT file
        void writePackageDat(const bool cache);
        /// @brief Read package DAT file
        void readPackageDat();
        /// @brief Load asset from cache or file
        /// @throw std::exception thrown by @a Asset::doLoadData
        Asset::Ptr loadAssetCached(AssetInfo& info);
    private:
        AssetManager& m_packageManager;
        FileSystem m_fsAsset;
        FileSystem m_fsCache;
        MultiFileSystem m_fsAny;
        bool m_loaded = false;

        vector<AssetInfo> m_assetsInfo;
        hamap<string, uint> m_assetByName;

        /// @brief Lock asset ops
        static recursive_mutex assetMutex;
    };

    /// @}
}