#include "pch.h"
#include "ls/asset/AssetManagerOld.h"
#include "ls/asset/AssetStorage.h"
#include "ls/io/FileSystemInterface.h"
#include "ls/io/StreamInterface.h"

#include "ls/asset/serializer/Texture2DSerializer.h"
#include "ls/asset/serializer/MeshSerializer.h"
#include "ls/asset/serializer/GraphicalMaterialSerializer.h"
#include "ls/asset/serializer/GraphicalModelSerializer.h"
#include "ls/asset/serializer/SkeletonSerializer.h"
#include "ls/asset/serializer/NodeAnimationSerializer.h"
#include "ls/asset/serializer/BipedWalkAnimationSerializer.h"

using namespace ls;
AssetManagerOld::AssetManagerOld(FileSystem assetFileSystem, FileSystem cacheFileSystem, 
                           Worker& parallelWorker)
    : Nameable("AssetManager")
    , m_fileSystem({assetFileSystem, cacheFileSystem})
    , m_worker(parallelWorker)
{
    TupleTypesIterator iter(*this);
    for_each_type<ImplementedSerializers>(iter);
}
AssetManagerOld::~AssetManagerOld()
{
}
