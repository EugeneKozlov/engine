/// @file ls/asset/TextAsset.h
/// @brief Text Asset
#pragma once

#include "ls/common.h"
#include "ls/asset/Asset.h"

namespace ls
{
    /// @addtogroup LsAsset
    /// @{

    /// @brief Text Asset
    class TextAsset
        : public AssetT<AssetType::Text, AssetCategory::Simple>
    {
    public:
        /// @brief Ctor
        TextAsset();
        /// @brief Dtor
        ~TextAsset();

        /// @brief Get text
        const string& text();
        /// @brief Set text
        void text(const string& value);
    private:
        /// @name Implement Asset
        /// @{
        virtual string extension() const override;
        virtual void doSerialize(Archive<Buffer> archive) override;
        virtual void doRestore() override;
        /// @}
    private:
        string m_text; ///< Value
    };

    /// @}
}