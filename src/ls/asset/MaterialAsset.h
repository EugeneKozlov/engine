/// @file ls/asset/MaterialAsset.h
/// @brief Material Asset
#pragma once

#include "ls/common.h"
#include "ls/asset/Asset.h"
#include "ls/asset/EffectAsset.h"
#include "ls/asset/LayoutAsset.h"
#include "ls/asset/MeshAsset.h"
#include "ls/asset/TextureAsset.h"
#include "ls/gapi/common.h"

namespace ls
{
    class Renderer;
    class TextureAsset;
    struct MaterialCacheMaterial;
    struct MaterialCacheContext;

    /// @addtogroup LsAsset
    /// @{

    /// @brief Material Asset
    ///
    /// Shader modes
    /// ====================================
    ///
    /// @warning Programs for each shader mode shall have same layout
    ///
    /// Geometry and Tesselated Geometry
    /// ------------------------------------
    ///
    /// All maps are passed to pixel shader.
    /// Height map is also passed to domain shader if tesselated.
    ///
    /// Program must write all G-Buffer channels.
    ///
    /// Resource slots:
    ///
    /// Map           | PS slot | DS slot
    /// --------------|---------|---------
    /// Albedo        | 0       | -
    /// Normal        | 1       | -
    /// Specular      | 2       | -
    /// Height        | 3       | 3
    /// Occlusion     | 4       | -
    /// Emission      | 5       | -
    /// Detail Albedo | 6       | -
    /// Detail Normal | 7       | -
    ///
    ///
    /// Depth-only, simple and tesselated
    /// ------------------------------------
    ///
    /// Only albedo map is passed to pixel shader.
    /// Height map is also passed to domain shader if tesselated.
    ///
    /// Program must write depth only.
    ///
    /// Map           | PS slot | DS slot
    /// --------------|---------|---------
    /// Albedo        | 0       | -
    /// Height        | -       | 3
    ///
    ///
    /// Raw material mode
    /// ------------------------------------
    ///
    /// All maps are passed exactly as specified.
    ///
    /// Rendering modes
    /// ====================================
    ///
    /// - <b>Opacity</b> is used for fully non-transparent geometry
    ///
    /// - <b>Cutout</b> geometry is rendered without alpha blending,
    ///    but it may have transparent regions
    ///
    /// - <b>Transparent</b> geometry shall be rendered in another pass
    ///
    ///
    /// Culling
    /// ====================================
    ///
    /// Culling depends on material culling mode and passed renderer state description:
    ///
    /// Actual culling mode is shown in table:
    ///
    /// Renderer culling | Enabled  | Disabled  | Reverse
    /// -----------------|----------|-----------|-----------
    /// Culling::Back    | Back     | None      | Front
    /// Culling::None    | None     | None      | None
    /// Culling::Front   | Front    | None      | Back
    ///
    class MaterialAsset
        : public AssetT<AssetType::Material, AssetCategory::Config>
    {
    public:
        /// @name States
        /// @{

        /// @brief Culling mode
        enum class CullingMode : ubyte
        {
            /// @brief Enabled
            Enabled,
            /// @brief Disabled
            Disabled,
            /// @brief Reverse
            Reverse
        };
        /// @brief Rendering Mode
        enum class RenderingMode : ubyte
        {
            /// @brief Opacity
            Opacity,
            /// @brief Cutout
            Cutout,
            /// @brief Transparent
            Transparent
        };
        /// @brief Shader mode
        enum class ShaderMode
        {
            /// @brief Proxy
            Proxy,
            /// @brief Full geometry
            Geometry,
            /// @brief Depth only
            Depth,
            /// @brief Counter
            COUNT
        };
        /// @}

        /// @name Buffers
        /// @{

        /// @brief Uniform Buffers
        enum class UniformBuffer
        {
            /// @brief Global Uniform Buffer
            Global = 0,
            /// @brief Model Uniform Buffer
            Model = 1,
            /// @brief Counter
            COUNT
        };
        /// @brief Global Uniform Buffer
        struct GlobalUniformBuffer
        {
            static const char* name()
            {
                return "GlobalUniformBuffer";
            }
            float4x4 mView; ///< View matrix
            float4x4 mProjection; ///< Projection matrix
            float4 vCameraPosition; ///< Camera position
            float4 vTime; ///< Time vector: sec mod period, sec mod 1s, msec mod period, msec mod 1s
            float4 vAmbientColor; ///< Ambient color
            float2 vAmbientFadeIntensity; ///< Ambient intensity at 0 and at end
            float vAmbientFadeDistance; ///< Ambient fade max distance
            float _reserve0;
        };
        /// @brief Model Uniform Buffer
        struct ModelUniformBuffer
        {
            static const char* name()
            {
                return "ModelUniformBuffer";
            }
            MeshAsset::UniformBuffer vParam; ///< Uniform buffer
        };
        /// @brief Simple Instance Buffer
        struct SimpleInstanceBuffer
        {
            float3x4 modelMatrix; ///< Model matrix

            half fade1; ///< Fade factor #1
            half fade2; ///< Fade factor #2

            ubyte3 windDirection; ///< Wind direction
            ubyte _reserved1;

            half windMain; ///< Wind main strength
            half windPulseMagnitude; ///< Wind pulse magnitude
            half windPulseFrequency; ///< Wind pulse frequency
            half windTurbulence; ///< Wind turbulence
        };
        /// @}

        /// @name Resources
        /// @{

        /// @brief Standard Maps
        enum class Map
        {
            /// @brief Albedo (diffuse) map
            Albedo,
            /// @brief Normal map
            Normal,
            /// @brief Smoothness and metal map
            Specular,
            /// @brief Height map
            Height,
            /// @brief Occlusion map
            Occlusion,
            /// @brief Emission map
            Emission,
            /// @brief Second albedo (diffuse) map
            DetailAlbedo,
            /// @brief Second normal map
            DetailNormal,
            /// @brief Counter
            COUNT
        };
        /// @}

        /// @brief Number of shader modes
        static const uint NumShaderModes = static_cast<uint>(ShaderMode::COUNT);
        /// @brief Number of uniform buffers
        static const uint NumUniformBuffers = static_cast<uint>(UniformBuffer::COUNT);
        /// @brief Number of maps
        static const uint NumMaps = static_cast<uint>(Map::COUNT);
    public:
        /// @brief Ctor
        MaterialAsset();
        /// @brief Dtor
        ~MaterialAsset();

        /// @brief Drop buffers stored in RAM
        void dropRam();
        /// @brief Is GAPI ready?
        bool isGapiReady() const;

        /// @name GAPI functions
        /// @pre GAPI is ready
        /// @{

        /// @brief Get GAPI resources
        const ResourceBucket& gapiResources(const ShaderMode shaderMode) const;
        /// @brief Get GAPI effect
        EffectHandle gapiEffect();
        /// @brief Get GAPI program
        ProgramHandle gapiProgram(const ShaderMode shaderMode);
        /// @brief Get GAPI state from cache
        StateHandle gapiState(MaterialCacheContext* context);
        /// @brief Get GAPI layout
        LayoutHandle gapiLayout(const ShaderMode shaderMode);
        /// @brief Attach global uniform buffer
        void attachUniformBuffer(const UniformBuffer type, BufferHandle buffer, const char* name);
        /// @brief Detach global uniform buffer
        void detachUniformBuffer(const UniformBuffer type, const char* name);
        /// @}

        /// @name Gets
        /// @{

        /// @brief Get culling mode
        CullingMode cullingMode() const;
        /// @brief Get primitive type
        PrimitiveType primitiveType() const;
        /// @brief Get layout
        shared_ptr<LayoutAsset> layout() const;
        /// @brief Get effect
        shared_ptr<EffectAsset> effect() const;
        /// @brief Is raw material?
        bool isRaw() const;
        /// @}
    private:
        /// @name Implement Asset
        /// @{
        virtual void doReadConfig(INIReader& ini) override;
        virtual void doSerializeConfig(Archive<Buffer> archive) override;
        virtual void doRestore() override;
        /// @}
    private:
        /// @name Info
        /// @{
        CullingMode m_cullingMode = CullingMode::Enabled; ///< Culling
        RenderingMode m_renderingMode = RenderingMode::Opacity; ///< Rendering mode
        PrimitiveType m_primitiveType = PrimitiveType::Triangle; ///< Primitive
        bool m_rawResources = true;
        string m_layout;
        string m_effect;
        array<string, NumShaderModes> m_shaders;
        /// @}

        /// @name Shaders
        /// @{
        shared_ptr<LayoutAsset> m_layoutAsset;
        shared_ptr<EffectAsset> m_effectAsset;
        /// @}

        /// @name Resources for classic mapping
        /// @{
        struct ClassicMaps
        {
            array<string, NumMaps> names; ///< Names
            array<shared_ptr<TextureAsset>, NumMaps> textures; ///< Links
        } m_classic;
        /// @}

        /// @name Resources for raw mapping
        /// @{
        struct RawMaps
        {
            uint count = 0;
            array<uint, MaxShaderResources> slots;
            array<ShaderType, MaxShaderResources> shaders;
            array<string, MaxShaderResources> names;
            array<shared_ptr<TextureAsset>, MaxShaderResources> textures;
        } m_raw;
        /// @}

        /// @name GAPI
        /// @{
        bool m_gapiReady = false;
//         array<ResourceBucket, NumShaderModes> m_resources; ///< GAPI Resources
        ResourceBucket m_resources; ///< GAPI Resources
        MaterialCacheMaterial* m_cachedMaterial = nullptr;

        LayoutHandle m_layoutHandle = nullptr; ///< Material layout (lazy init)
        array<ProgramHandle, NumShaderModes> m_shaderPrograms; ///< Material programs
        array<BufferHandle, NumUniformBuffers> m_uniformBuffers; ///< Attached uniform buffers
        /// @}
    };

    /// @}
}