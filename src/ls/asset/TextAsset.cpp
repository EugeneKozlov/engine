#include "pch.h"
#include "ls/asset/TextAsset.h"

using namespace ls;

// Main
TextAsset::TextAsset()
{
}
TextAsset::~TextAsset()
{
}


// Implement Asset
string TextAsset::extension() const
{
    // #TODO fix me
    return ".lua";
}
void TextAsset::doSerialize(Archive<Buffer> archive)
{
    if (archive.isWrite())
    {
        archive.stream().writeText(m_text);
    }
    else
    {
        archive.stream().readText(m_text);
    }
}
void TextAsset::doRestore()
{
}


// Get/set
const string& TextAsset::text()
{
    return m_text;
}
void TextAsset::text(const string& value)
{
    m_text = value;
}
