#include "pch.h"
#include "ls/asset/PaletteAsset.h"

#include "ls/core/INIReader.h"

#include <regex>

using namespace ls;
PaletteAsset::PaletteAsset()
{

}
PaletteAsset::~PaletteAsset()
{

}


// Implement Asset
void PaletteAsset::doReadConfig(INIReader& ini)
{
    // #TODO Rewrite
    const INISection& header = ini.section("");

    static const std::regex colorRegex(R"(\s*(\d+)?\#([0-9a-fA-F]{6})\s*)");
    std::smatch matches;
    for (const auto& item : header)
    {
        // Parse color
        const string colorName = item.first;
        const string colorValue = item.second;
        const bool validColor = std::regex_match(colorValue, matches, colorRegex);
        LS_THROW(validColor, 
                 IoException, 
                 "Invalid color string : '", colorValue, "'");
        
        // Read alpha
        const uint alphaComponent = matches[1].matched 
            ? fromString<uint>(matches[1].str())
            : 100;
        LS_THROW(alphaComponent >= 0 && alphaComponent <= 100, 
                 IoException, 
                 "Alpha should be in range [0, 100] : '", colorValue, "'");

        // Read color
        const uint rgbComponent = fromString<uint>(matches[2].str(), std::hex);

        // Make color
        ubyte4 color;
        color.x = static_cast<ubyte>((rgbComponent & 0x00ff0000) >> 16);
        color.y = static_cast<ubyte>((rgbComponent & 0x0000ff00) >> 8);
        color.z = static_cast<ubyte>((rgbComponent & 0x000000ff) >> 0);
        color.w = static_cast<ubyte>(alphaComponent * 255 / 100);

        // Save item
        m_colors.emplace(colorName, color);
    }
}
void PaletteAsset::doSerializeConfig(Archive<Buffer> archive)
{
    serializeBinary(archive, m_colors);
}
void PaletteAsset::doRestore()
{
}
ubyte4 PaletteAsset::getColor4u(const string& name) const
{
    return getOrThrow<ArgumentException>(m_colors, name, 
                                         "Color [", name, "] is not found");
}
float4 PaletteAsset::getColor4f(const string& name) const
{
    const ubyte4 color = getColor4u(name);
    return float4(unorm2real<float>(color.x),
                  unorm2real<float>(color.y),
                  unorm2real<float>(color.z),
                  unorm2real<float>(color.w));
}
float3 PaletteAsset::getColor3f(const string& name) const
{
    return static_cast<float3>(getColor4f(name));
}
