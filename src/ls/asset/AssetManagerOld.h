/// @file ls/asset/AssetManager.h
/// @brief Asset importer, loader and container
#pragma once

#include "ls/common.h"
#include "ls/core/Nameable.h"
#include "ls/io/MultiFileSystem.h"
#include "ls/asset/AssetStorageInterface.h"

namespace ls
{
    class Worker;

    class Texture2D;
    class Mesh;

    class Material;
    class GraphicalModel;

    class Skeleton;
    class NodeAnimation;
    class BipedWalkAnimation;

    template <class A, class H>
    class AssetStorage;

    /// @addtogroup LsAsset
    /// @{

    /// @brief Asset importer, loader and container.
    class AssetManagerOld
        : Noncopyable
        , public Nameable<AssetManagerOld>
    {
    public:
        /// @brief Implemented serializer types
        using ImplementedSerializers = tuple
            < Texture2D
            , Mesh
            , Material
            , GraphicalModel
            , Skeleton
            , NodeAnimation
            , BipedWalkAnimation
            >;
        /// @brief Implemented serializer handles
        using SerializerHandles = tuple
            < shared_ptr<Texture2D>
            , shared_ptr<Mesh>
            , shared_ptr<Material>
            , shared_ptr<GraphicalModel>
            , shared_ptr<Skeleton>
            , shared_ptr<NodeAnimation>
            , shared_ptr<BipedWalkAnimation>
            >;
        /// @brief Asset Handle
        template <class Asset>
        using AssetHandle = std::tuple_element_t<TupleTypeIndex<Asset, ImplementedSerializers>::value, SerializerHandles>;
    public:
        /// @brief Ctor
        AssetManagerOld(FileSystem assetFileSystem, FileSystem cacheFileSystem,
                     Worker& parallelWorker);
        /// @brief Dtor
        ~AssetManagerOld();

        /// @brief Get asset of type by @a name
        template <class Asset>
        AssetHandle<Asset> get(const string& name)
        {
            auto iterStorage = m_storage.find(typeid(Asset));
            LS_THROW(iterStorage != m_storage.end(),
                     NotImplementedException,
                     "Asset Loader of required type is not implemented");

            AssetStorageInterface& storage = *iterStorage->second;
            any asset = storage.get(name);
            try
            {
                return any_cast<AssetHandle<Asset>>(asset);
            }
            catch (const boost::bad_any_cast&)
            {
                debug_assert(0, "Internal fail");
                return AssetHandle<Asset>();
            }
        }
        /// @brief Get file system
        FileSystemInterface& fileSystem() noexcept
        {
            return m_fileSystem;
        }
        /// @brief Get worker
        Worker& worker() noexcept
        {
            return m_worker;
        }

        /// @brief Log asset loading
        template <class ... Args>
        inline void logLoad(const Args& ... args) const
        {
            logDebug(args...);
        }
        /// @brief Log asset save/import
        template <class ... Args>
        inline void logImport(const Args& ... args) const
        {
            logDebug(args...);
        }
    private:
        struct TupleTypesIterator : Noncopyable
        {
            TupleTypesIterator(AssetManagerOld& self)
                : self(self)
            {
            }
            AssetManagerOld& self;
            template <class Asset>
            bool operator() (TypeWrapper<Asset>)
            {
                self.m_storage.emplace(typeid(Asset),
                                       make_unique<AssetStorage<Asset, AssetHandle<Asset>>>(self));
                return true;
            }
        };
    private:
        MultiFileSystem m_fileSystem;
        hamap<type_index, unique_ptr<AssetStorageInterface>> m_storage;
        Worker& m_worker;
    };
    /// @}
}

