/// @file ls/asset/all.h
/// @brief All assets

#pragma once
#include "ls/asset/EffectAsset.h"
#include "ls/asset/EffectLibraryAsset.h"
#include "ls/asset/LayoutAsset.h"
#include "ls/asset/MaterialAsset.h"
#include "ls/asset/MeshAsset.h"
#include "ls/asset/ModelAsset.h"
#include "ls/asset/PaletteAsset.h"
#include "ls/asset/TextAsset.h"
#include "ls/asset/TextureAsset.h"
#include "ls/asset/TreeAsset.h"
#include "ls/asset/WorldAsset.h"
