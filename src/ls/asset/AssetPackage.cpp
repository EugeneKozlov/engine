#include "pch.h"
#include "ls/asset/AssetPackage.h"
#include "ls/asset/AssetManager.h"
#include "ls/core/Worker.h"
#include "ls/io/Codec.h"
#include "ls/io/FileSystemInterface.h"

using namespace ls;
const string AssetPackage::iniName = "package.ini";
const string AssetPackage::datName = "package.dat";
recursive_mutex AssetPackage::assetMutex;
AssetPackage::AssetPackage(AssetManager& packageManager, 
                           FileSystem fsAsset, FileSystem fsCache)
    : m_packageManager(packageManager)
    , m_fsAsset(fsAsset)
    , m_fsCache(fsCache)
    , m_fsAny({ fsCache, fsAsset })
{
}
AssetPackage::~AssetPackage()
{
    lock_guard<recursive_mutex> guard(assetMutex);

    // All assets must be expired
    for (AssetInfo& info : m_assetsInfo)
    {
        if (info.asset.expired())
            continue;
        logError("Asset [", info.name, "] has ", info.asset.use_count(), " references when unloading");
    }
}


// Save/load package
void AssetPackage::loadPackage()
{
    // Try to read binary
    bool binaryOutOfDate = m_fsAny.lastWriteTime(iniName) > m_fsAny.lastWriteTime(datName);
    bool binaryValid = m_fsAny.isRegularFile(datName);
    bool binaryLoaded = false;
    if (binaryValid && !binaryOutOfDate)
    {
        try
        {
            readPackageDat();
            binaryLoaded = true;
        }
        catch (const std::exception& ex)
        {
            logError("Cannot load package: ", ex.what());
            m_assetsInfo.clear();
            m_assetByName.clear();
        }
    }
    
    // Read INI-file if cache is out-of-date or not exist
    if (!binaryLoaded)
    {
        parsePackageIni();
        writePackageDat(true);
    }

    // Build assets map
    for (uint idx = 0; idx < m_assetsInfo.size(); ++idx)
    {
        m_assetByName.emplace(m_assetsInfo[idx].name, idx);
    }

    // Finalize
    m_loaded = true;
}
bool AssetPackage::isLoaded() const
{
    return m_loaded;
}
void AssetPackage::parsePackageIni()
{
    Stream iniStream = m_fsAsset->openStream(iniName);
    LS_THROW(iniStream, IoException, "Cannot open [", iniName, "] for reading");

    INIReader iniReader;
    iniReader.parse(iniStream->text());
    for (const auto& section : iniReader.sections())
    {
        if (section.first[0] == '$')
            continue;

        // Add to vector
        m_assetsInfo.emplace_back();
        AssetInfo& info = m_assetsInfo.back();
        info.name = section.first;

        // Load info
        const string assetTypeName = getOrDefault(section.second, "$type");
        info.type = fromString<AssetType>(assetTypeName);
        info.fileName = getOrDefault(section.second, "$file");

        // Load config data
        info.headerConfig = section.second;

        // Remove items started from '$'
        erase_if(info.headerConfig,
                 [](const INISection::value_type& item)
        {
            return item.first[0] == '$';
        });
        info.headerDataReady = false;
    }
}
void AssetPackage::serializePackage(Archive<Buffer>& archive)
{
    // Serialize version
    uint version = Version;
    serializeBinary<uint>(archive, version);

    // Check version
    LS_THROW(version == Version, IoException, "Package file has invalid version");

    // Serialize number of assets
    uint numAssets = m_assetsInfo.size();
    serializeBinary<uint>(archive, numAssets);
    m_assetsInfo.resize(numAssets);

    // Serialize assets
    for (AssetInfo& info : m_assetsInfo)
    {
        // Header data MUST BE ready if write
        debug_assert(info.headerDataReady || archive.isRead());

        // Serialize data
        serializeBinary<AssetType>(archive, info.type);
        serializeBinary<string>(archive, info.name);
        serializeBinary<string>(archive, info.fileName);
        serializeBinary<Buffer>(archive, info.headerData);

        // Header data IS ready if read
        info.headerDataReady = archive.isRead();
    }
}
void AssetPackage::writePackageDat(const bool cache)
{
    // Load all asset headers
    for (AssetInfo& info : m_assetsInfo)
    {
        try
        {
            loadAssetCached(info);
        }
        catch (const std::exception& /*ex*/)
        {
            // No matter if asset is not loaded.
            // However, we need binary headers.
            if (!info.headerDataReady)
            {
                throw;
            }
        }
    }

    // Write data
    Buffer buffer;
    serializePackage(Archive<Buffer>(buffer, ArchiveWrite));

    // Save package to file
    FileSystemInterface& fileSystem = cache ? *m_fsCache : *m_fsAsset;
    Stream datStream = fileSystem.openStream(datName, StreamAccess::ReadWriteNew);
    LS_THROW(datStream, IoException, "Cannot open [", iniName, "] for writing");
    buffer.writeToStream(*datStream);
}
void AssetPackage::readPackageDat()
{
    // Read data buffer
    Stream stream = m_fsAny.openStream(datName, StreamAccess::Read);
    LS_THROW(stream, IoException, "Cannot open [", datName, "] for reading");
    Buffer buffer = Buffer::readFromStream(*stream);

    // Read package
    serializePackage(Archive<Buffer>(buffer, ArchiveRead));
}
void AssetPackage::savePackage()
{
    writePackageDat(false);
}
void AssetPackage::generateAssets()
{
    m_packageManager.generatePackageAssets(m_fsAny, *this);
}


// Save/load assets
Asset::Ptr AssetPackage::loadAssetCached(AssetInfo& info)
{
    Asset::Ptr asset;

    // Try to get asset from package
    asset = info.asset.lock();
    if (asset)
    {
        // Asset must be completely loaded
        LS_THROW(asset->isReady(), 
                 IoException, 
                 "Asset was not loaded because of occurred error");

        // Return cached asset
        return asset;
    }

    // Create new asset
    asset = Asset::construct(info.type);
    asset->initialize(*this, info.fileName);

    // Load header
    if (info.headerDataReady)
    {
        // Read binary header
        info.headerData.seek(0);
        asset->serializeHeader(Archive<Buffer>(info.headerData, ArchiveRead));
    }
    else
    {
        // Load header from INI
        asset->loadHeader(info.headerConfig);

        // Generate binary header
        info.headerData.release();
        asset->serializeHeader(Archive<Buffer>(info.headerData, ArchiveWrite));
        info.headerDataReady = true;
    }

    // Store asset in cache
    info.asset = asset;

    // Load asset data, throw on error
    asset->loadData();

    // Return asset
    return asset;
}



// Gets
void AssetPackage::createAsset(const string& name, const AssetType type,
                               const string& fileName)
{
    // Thread-safe
    std::lock_guard<std::recursive_mutex> guard(assetMutex);

    // Throw on duplicate
    for (const AssetInfo& info : m_assetsInfo)
    {
        LS_THROW(info.name != name, 
                 ArgumentException, "Asset with name [", name, "] is already exist");
    }

    // Create
    m_assetsInfo.emplace_back();
    AssetInfo& info = m_assetsInfo.back();
    info.type = type;
    info.name = name;
    info.fileName = fileName;
}
Asset::Ptr AssetPackage::loadAsset(const string& name)
{
    // Thread-safe
    std::lock_guard<std::recursive_mutex> guard(assetMutex);

    // Parse name
    pair<string, string> assetPackageName = Asset::parseName(name);

    if (assetPackageName.first.empty())
    {
        // Load asset from this package
        const uint* idx = findOrDefault(m_assetByName, name);
        LS_THROW(idx, ArgumentException, "Asset [", name, "] is not found");
        return loadAssetCached(m_assetsInfo[*idx]);
    }
    else
    {
        // Load asset by global name
        return m_packageManager
            .loadPackage(assetPackageName.first)
            .loadAsset(assetPackageName.second);
    }
}
Asset::Ptr AssetPackage::loadAssetSilent(const string& name) noexcept
{
    try
    {
        return loadAsset(name);
    }
    catch (const std::exception& ex)
    {
        logError("Asset [", name, "] cannot be loaded: ", ex.what());
        return nullptr;
    }
}
AssetManager& AssetPackage::manager()
{
    return m_packageManager;
}
Worker& AssetPackage::worker()
{
    return m_packageManager.worker();
}
Renderer* AssetPackage::renderer()
{
    return m_packageManager.renderer();
}
MaterialCache* AssetPackage::materialCache()
{
    return m_packageManager.materialCache();
}
FileSystemInterface& AssetPackage::fileSystem()
{
    return m_fsAny;
}
