#include "pch.h"
#include "ls/asset/TreeAsset.h"

#include "ls/core/INIReader.h"
#include "ls/gen/utility/geometry.h"
#include "ls/io/StreamInterface.h"

#include <boost/algorithm/string.hpp>

using namespace ls;


// Branch Group
TreeAsset::BranchGroup& TreeAsset::BranchGroup::addBranchGroup()
{
    m_childrenBranchGroups.emplace_back();
    TreeAsset::BranchGroup& group = m_childrenBranchGroups.back();

    // Inherit something
    group.p.branchMaterial = p.branchMaterial;
    return group;
}
TreeAsset::FrondGroup& TreeAsset::BranchGroup::addFrondGroup()
{
    m_childrenFrondGroups.emplace_back();
    return m_childrenFrondGroups.back();
}


// Function
TreeAsset::Function::Function(const FunctionType type) 
    : interpType(type)
{
}
TreeAsset::Function::Function(const FunctionType type, const float value)
    : interpType(type)
{
    set(value);
}
TreeAsset::Function::Function(const FunctionType type, 
                              const float begin, const float end)
    : interpType(type)
{
    set(begin, end);
}
void TreeAsset::Function::set(const float beginX, const float beginY, const float endX, const float endY)
{
    beginValue.x = beginX;
    beginValue.y = beginY;
    endValue.x = endX;
    endValue.y = endY;
}
void TreeAsset::Function::set(const float begin, const float end)
{
    set(0.0f, begin, 1.0f, end);
}
void TreeAsset::Function::set(const float value)
{
    set(value, value);
}
void TreeAsset::Function::initialize() const
{
    if (m_interpFunction == nullptr)
    {
        switch (interpType)
        {
        default:
        case TreeAsset::FunctionType::Zero:
            m_interpFunction = [](const float /*x*/) { return 0.0f; };
            break;
        case TreeAsset::FunctionType::Constant:
            m_interpFunction = [](const float /*x*/) { return 1.0f; };
            break;
        case TreeAsset::FunctionType::Linear:
            m_interpFunction = [](const float x) { return x; };
            break;
        case TreeAsset::FunctionType::Quad:
            m_interpFunction = [](const float x) { return x * x; };
            break;
        case TreeAsset::FunctionType::SmoothRight:
            m_interpFunction = [](const float x) { return sin(pi*x / 2); };
            break;
        case TreeAsset::FunctionType::SmoothLeft:
            m_interpFunction = [](const float x) { return 1 - cos(pi*x / 2); };
            break;
        case TreeAsset::FunctionType::SmoothBoth:
            m_interpFunction = [](const float x) { return (1 - cos(pi*x)) / 2; };
            break;
        }
    }
}
float TreeAsset::Function::compute(const float input) const
{
    initialize();

    const float unlerpedInput = unlerp(beginValue.x, endValue.x, input);
    const float rawOutput = m_interpFunction(unlerpedInput);
    return lerp(beginValue.y, endValue.y, rawOutput);
}


// Helpers
TreeAsset::Point::Point(const Point& from, const Point& to, const float factor)
    : position(lerp(from.position, to.position, factor))
    , radius(lerp(from.radius, to.radius, factor))
    , location(lerp(from.location, to.location, factor))

    , distance(lerp(from.distance, to.distance, factor))
    , direction(lerp(from.direction, to.direction, factor))
    , segmentLength(lerp(from.segmentLength, to.segmentLength, factor))
    , zAxis(lerp(from.zAxis, to.zAxis, factor))
    , xAxis(lerp(from.xAxis, to.xAxis, factor))
    , yAxis(lerp(from.yAxis, to.yAxis, factor))

    , relativeDistance(lerp(from.relativeDistance, to.relativeDistance, factor))

    , branchAdherence(lerp(from.branchAdherence, to.branchAdherence, factor))
    , phase(lerp(from.phase, to.phase, factor))
{
}
void TreeAsset::Branch::addPoint(const float location,
                                 const float3& position, const float radius,
                                 const float phase, const float adherence)
{
    const uint last = points.size();
    points.emplace_back();

    points[last].location = location;
    points[last].position = position;
    points[last].radius = radius;
    points[last].branchAdherence = adherence;
    points[last].phase = phase;

    // Reset and skip if first
    if (last == 0)
    {
        points[0].distance = 0.0f;
        points[0].direction = 0.0f;
        points[0].segmentLength = 0.0f;
        points[0].zAxis = 0.0f;
        points[0].xAxis = 0.0f;
        points[0].yAxis = 0.0f;
        points[0].relativeDistance = 0.0f;
        return;
    }

    // Compute distance
    points[last].distance = points[last - 1].distance + points[last - 1].segmentLength;

    // Compute direction
    const float3 dir = points[last].position - points[last - 1].position;
    points[last - 1].direction = normalize(dir);
    points[last - 1].segmentLength = length(dir);
    points[last].direction = points[last - 1].direction;
    points[last].segmentLength = 0.0f;

    // Compute relative distance
    ///                L        ln(r1/r2)
    /// u1 = u0 + ----------- * ---------
    ///           2 * pi * r0   r1/r0 - 1
    const float u0 = points[last - 1].relativeDistance;
    const float L = points[last - 1].segmentLength;
    const float r0 = points[last - 1].radius;
    const float r1 = points[last].radius;
    const float rr = r1 / r0;
    /// ln(x) / (x - 1) ~= 1 - (x - 1) / 2 + ...
    const float k = 1 - (rr - 1) / 2;
    points[last].relativeDistance = u0 + k * L / (2 * pi * r0);

    // Function to compute axises
    auto computeAxisXY = [](Point& p)
    {
        p.xAxis = makeOrthoVector(p.zAxis);
        p.yAxis = normalize(cross(p.zAxis, p.xAxis));

        // Flip
        if (p.yAxis.y < 0.0f)
        {
            p.yAxis = -p.yAxis;
            p.xAxis = cross(p.yAxis, p.zAxis);
        }
    };

    // Compute axises for last point
    points[last].zAxis = points[last].direction;
    computeAxisXY(points[last]);

    // Compute axises for first point
    if (last == 1)
    {
        points[0].zAxis = points[0].direction;
        computeAxisXY(points[0]);
        return;
    }

    // Compute axises for middle points
    points[last - 1].zAxis = normalize(points[last - 2].direction + points[last - 1].direction);
    computeAxisXY(points[last - 1]);
}
TreeAsset::Point TreeAsset::Branch::getPoint(const float location) const
{
    // Compute points
    uint first = 0;
    uint second = 0;
    while (second + 1 < points.size() && points[second].location < location)
        ++second;
    first = second == 0 ? 0 : second - 1;

    // Compute factor
    float factor = first == second
        ? 1.0f
        : unlerp(points[first].location, points[second].location, location);

    // Generate point
    return Point(points[first], points[second], factor);

}


// Generation
struct TreeAsset::InternalGenerator
{
    void generateBranchGroup(const LodInfo& lodInfo, const BranchGroup& branchGroup,
                             vector<Branch>& branchesArray, const Branch* parent)
    {
        m_random = UniformRandom(branchGroup.p.seed == 0 
                                 ? m_seedGenerator.random()
                                 : branchGroup.p.seed);
        if (branchGroup.p.frequency == 0)
        {
            // Actually this group is special case for root group

            // Generate children
            for (const BranchGroup& childGroup : branchGroup.branches())
            {
                LS_THROW(childGroup.p.frequency == SingleBranch,
                         IoException, "Empty branch group must have only single branches");
                generateBranchGroup(lodInfo, childGroup, branchesArray, nullptr);
            }
        }
        else if (branchGroup.p.frequency == SingleBranch)
        {
            branchesArray.emplace_back();
            Branch& branch = branchesArray.back();
            branch.position = branchGroup.p.position;
            branch.direction = branchGroup.p.direction;
            generateBranch(branch, lodInfo, branchGroup);

            // Generate children
            for (const BranchGroup& childGroup : branchGroup.branches())
            {
                generateBranchGroup(lodInfo, childGroup, branch.children, &branch);
            }
        }
        else
        {
            LS_THROW(parent, ArgumentException, "Branch group must have parent");

            // Compute locations
            vector<float> locations = computeLocations(branchGroup.p.distribFunction,
                                                       branchGroup.p.location,
                                                       branchGroup.p.frequency);
            const float parentLength = parent->totalLength;

            // Emit branches
            const uint baseIndex = m_random.random() % 2;
            for (uint idx = 0; idx < branchGroup.p.frequency; ++idx)
            {
                const float location = locations[idx];

                // Allocate
                branchesArray.emplace_back();
                Branch& branch = branchesArray.back();
                branch.parentLength = parentLength;

                // Get connection point
                const Point connectionPoint = parent->getPoint(location);
                branch.location = connectionPoint.location;
                branch.parentRadius = connectionPoint.radius;
                branch.parentBranchAdherence = connectionPoint.branchAdherence;
                branch.parentPhase = connectionPoint.phase;
                branch.position = connectionPoint.position;

                // Init branch direction
                initBranch(branch, connectionPoint, 
                           baseIndex + idx, branchGroup);

                // Apply angle
                const float angle = deg2rad(branchGroup.p.angle.compute(location));
                branch.direction = cos(angle) * connectionPoint.zAxis + sin(angle) * branch.direction;

                // Generate branch
                generateBranch(branch, lodInfo, branchGroup);

                // Generate children
                for (const BranchGroup& childGroup : branchGroup.branches())
                {
                    generateBranchGroup(lodInfo, childGroup, branch.children, &branch);
                }
            }
        }
    }
    static uint fixNumberOfSegments(const float num)
    {
        return max<uint>(3, static_cast<uint>(num));
    }
    void generateBranch(Branch& branch,
                        const LodInfo& lodInfo,
                        const BranchGroup& branchGroupInfo)
    {
        branch.groupInfo = branchGroupInfo;
        branch.groupInfo.clear();

        // Compute length
        branch.totalLength = branch.parentLength * branchGroupInfo.p.length.compute(branch.location);

        // Compute oscillation phase
        const float phaseOffset = (m_random.randomReal_01() - 0.5f) * branchGroupInfo.p.phaseRandom;

        // Compute number of segments and step length
        const uint numSegments = fixNumberOfSegments(lodInfo.numLengthSegments * branchGroupInfo.p.geometryMultiplier);
        const float step = branch.totalLength / numSegments;

        // Compute weight offset
        const float weightOffset = branchGroupInfo.p.weight.compute(branch.location) / numSegments;

        // Emit points
        float3 position = branch.position;
        float3 direction = branch.direction;
        for (uint idx = 0; idx <= numSegments; ++idx)
        {
            const float location = static_cast<float>(idx) / numSegments;
            const float radius = branchGroupInfo.p.radius.compute(location) * branch.parentRadius;
            const float localAdherence = branch.groupInfo.p.branchAdherence.compute(location);
            const float phase = branch.parentPhase + phaseOffset;
            const float adherence = localAdherence + branch.parentBranchAdherence;
            branch.addPoint(location, position, radius, phase, adherence);

            direction = normalize(direction + float3(0.0f, -1.0f, 0.0f) * weightOffset);
            position += direction * step;
        }
    }
    void initBranch(Branch& branch, const Point& connectionPoint,
                    const uint idx, const BranchGroup& branchGroupInfo)
    {
        switch (branchGroupInfo.p.distrib.type)
        {
        case TreeAsset::DistributionType::Alternate:
            initAlternateBranch(branch, connectionPoint, idx, branchGroupInfo);
            break;
        default:
            break;
        }
    }
    void initAlternateBranch(Branch& branch, const Point& connectionPoint,
                             const uint idx, const BranchGroup& branchGroupInfo)
    {
        debug_assert(branchGroupInfo.p.distrib.type == TreeAsset::DistributionType::Alternate);

        branch.location = connectionPoint.location;
        branch.parentRadius = connectionPoint.radius;
        branch.position = connectionPoint.position;

        const float angle = deg2rad(branchGroupInfo.p.distrib.angleRandomness * m_random.randomReal_11()
                                    + branchGroupInfo.p.distrib.angleStep * idx);
        branch.direction = connectionPoint.xAxis * cos(angle) + connectionPoint.yAxis * sin(angle);
    }
    vector<float> computeLocations(const Function& distribFunction, 
                                   const float2& locationRange, const uint count)
    {
        // Compute sum
        vector<float> factors = { 0 };
        float integralSum = 0.0f;
        for (uint idx = 0; idx < count; ++idx)
        {
            const float factor = static_cast<float>(idx) / count;
            integralSum += distribFunction.compute(factor);
            factors.push_back(integralSum);
        }

        // Normalize and recenter factors
        vector<float> locations;
        for (uint idx = 0; idx < count; ++idx)
        {
            const float normalizedLocation = (factors[idx] + factors[idx + 1]) / (2 * integralSum);
            locations.push_back(lerp(locationRange[0],
                                     locationRange[1],
                                     normalizedLocation));
        }

        return locations;
    }

    UniformRandom m_seedGenerator;
    UniformRandom m_random;
};


// Triangulation
struct TreeAsset::InternalBranchTriangulator
{
    struct PPPoint : Point
    {
        /// @brief Copy ctor
        PPPoint(const Point& base)
        {
            static_cast<Point&>(*this) = base;
        }

        uint numVertices = 0; ///< Number of vertices in circle
        uint startVertex = 0; ///< Start vertex of circle
    };
    void preprocessPoints(const Branch& branch, const LodInfo& lodInfo,
                          vector<PPPoint>& points)
    {
        const uint numPoints = branch.points.size();
        debug_assert(numPoints >= 2);

        points.assign(branch.points.begin(), branch.points.end());

        // Compute number of radial segments
        uint startVertex = 0;
        for (uint idx = 0; idx < numPoints; ++idx)
        {
            const float rawNumSegments = lodInfo.numRadialSegments * branch.groupInfo.p.geometryMultiplier;
            points[idx].numVertices = max<uint>(3, static_cast<uint>(rawNumSegments));
            points[idx].startVertex = startVertex;
            startVertex += points[idx].numVertices + 1;
        }
    }
    void triangulateBranch(const Branch& branch, const LodInfo& lodInfo,
                           vector<Vertex>& vertices, vector<uint>& indices)
    {
        vector<PPPoint> points;
        preprocessPoints(branch, lodInfo, points);
        generateBranchGeometry(branch, points, vertices, indices);

        // Triangulate children
        for (const Branch& child : branch.children)
        {
            triangulateBranch(child, lodInfo, vertices, indices);
        }
    }
    void generateBranchGeometry(const Branch& branch, const vector<PPPoint>& points,
                                vector<Vertex>& vertices, vector<uint>& indices)
    {
        const uint baseVertex = vertices.size();
        for (const PPPoint& point : points)
        {
            // Add points
            for (uint idx = 0; idx <= point.numVertices; ++idx)
            {
                const float factor = static_cast<float>(idx) / point.numVertices;
                const float angle = factor * 2 * pi;
                const float3 normal = normalize(point.xAxis * cos(angle) + point.yAxis * sin(angle));

                Vertex vertex;
                vertex.pos = point.position + point.radius * normal;
                vertex.normal = normal;
                vertex.tangent = point.zAxis;
                vertex.binormal = cross(vertex.normal, vertex.tangent);
                vertex.uv = float2(factor, point.relativeDistance)
                    / branch.groupInfo.p.branchMaterial.textureScale;
                vertex.branchAdherence = point.branchAdherence;
                vertex.phase = point.phase;
                vertex.edgeOscillation = 0.0f;

                vertices.push_back(vertex);
            }
        }

        // Connect point circles
        for (uint pointIndex = 0; pointIndex < points.size() - 1; ++pointIndex)
        {
            const PPPoint& pointA = points[pointIndex];
            const PPPoint& pointB = points[pointIndex + 1];
            const uint numA = pointA.numVertices;
            const uint numB = pointB.numVertices;
            const uint baseVertexA = baseVertex + pointA.startVertex;
            const uint baseVertexB = baseVertex + pointB.startVertex;

            uint idxA = 0;
            uint idxB = 0;
            const uint numTriangles = pointA.numVertices + pointB.numVertices;
            for (uint triangleIndex = 0; triangleIndex < numTriangles; ++triangleIndex)
            {
                if (idxA * numB <= idxB * numA)
                {
                    // A-based triangle
                    indices.push_back(baseVertexA + idxA % (numA + 1));
                    indices.push_back(baseVertexA + (idxA + 1) % (numA + 1));
                    indices.push_back(baseVertexB + idxB % (numB + 1));
                    ++idxA;
                }
                else
                {
                    // B-based triangle
                    indices.push_back(baseVertexB + (idxB + 1) % (numB + 1));
                    indices.push_back(baseVertexB + idxB % (numB + 1));
                    indices.push_back(baseVertexA + idxA % (numA + 1));
                    ++idxB;
                }
            }
        }
    }
};
struct TreeAsset::InternalFrondTriangulator
{
    void triangulateBranch(const Branch& branch, const LodInfo& lodInfo,
                           vector<Vertex>& vertices, vector<uint>& indices)
    {
        // Triangulate frond groups
        for (const FrondGroup& group : branch.groupInfo.fronds())
        {
            triangulateFrondGroup(branch, group, lodInfo, vertices, indices);
        }

        // Iterate over children
        for (const Branch& child : branch.children)
        {
            triangulateBranch(child, lodInfo, vertices, indices);
        }
    }
    void triangulateFrondGroup(const Branch& branch, const FrondGroup& frondGroup,
                               const LodInfo& lodInfo,
                               vector<Vertex>& vertices, vector<uint>& indices)
    {
        const float size = m_random.randomReal(frondGroup.sizeRange.x,
                                               frondGroup.sizeRange.y);
        switch (frondGroup.type)
        {
        case TreeAsset::FrondType::RagEnding:
            triangulateRagEndingFrondGroup(branch, frondGroup, size, lodInfo, vertices, indices);
            break;
        case TreeAsset::FrondType::BrushEnding:
            triangulateBrushEndingFrondGroup(branch, frondGroup, size, lodInfo, vertices, indices);
            break;
        default:
            break;
        }
    }
    /// @name Ending
    /// @{
    void triangulateRagEndingFrondGroup(const Branch& branch, const FrondGroup& frondGroup,
                                        const float size, const LodInfo& lodInfo,
                                        vector<Vertex>& vertices, vector<uint>& indices)
    {
        const RagEndingFrondParameters& param = frondGroup.ragEndingParam;
        const float halfSize = size / 2;
        const float relativeOffset = param.locationOffset / branch.totalLength;
        const Point location = branch.getPoint(1.0f - relativeOffset);
        const float3 zyAxis = normalize(lerp(location.zAxis, -location.yAxis, param.zBendingFactor));
        array<Vertex, 9> vers;

        // Center
        vers[4].pos = location.position
            + normalize(location.zAxis + location.yAxis) * param.centerOffset;
        vers[4].uv = float2(0.5f, 0.5f);
        vers[4].edgeOscillation = param.junctionOscillation;

        // Center -+z
        vers[3].pos = vers[4].pos - halfSize * location.zAxis;
        vers[3].uv = float2(0.5f, 0.0f);
        vers[3].edgeOscillation = param.edgeOscillation;
        vers[5].pos = vers[4].pos + halfSize * zyAxis;
        vers[5].uv = float2(0.5f, 1.0f);
        vers[5].edgeOscillation = param.edgeOscillation;

        // Center -+x
        vers[1].pos = vers[4].pos - halfSize * normalize(lerp(location.xAxis, location.yAxis, param.xBendingFactor));
        vers[1].uv = float2(0.0f, 0.5f);
        vers[1].edgeOscillation = param.edgeOscillation;
        vers[7].pos = vers[4].pos + halfSize * normalize(lerp(location.xAxis, -location.yAxis, param.xBendingFactor));
        vers[7].uv = float2(1.0f, 0.5f);
        vers[7].edgeOscillation = param.edgeOscillation;

        // Center -x -+z
        vers[0].pos = vers[1].pos - halfSize * location.zAxis;
        vers[0].uv = float2(0.0f, 0.0f);
        vers[0].edgeOscillation = param.edgeOscillation;
        vers[2].pos = vers[1].pos + halfSize * zyAxis;
        vers[2].uv = float2(0.0f, 1.0f);
        vers[2].edgeOscillation = param.edgeOscillation;

        // Center +x -+z
        vers[6].pos = vers[7].pos - halfSize * location.zAxis;
        vers[6].uv = float2(1.0f, 0.0f);
        vers[6].edgeOscillation = param.edgeOscillation;
        vers[8].pos = vers[7].pos + halfSize * zyAxis;
        vers[8].uv = float2(1.0f, 1.0f);
        vers[8].edgeOscillation = param.edgeOscillation;

        // Set wind
        for (Vertex& vertex : vers)
        {
            vertex.branchAdherence = location.branchAdherence;
            vertex.phase = location.phase;
        }

        // Triangles
        const uint base = vertices.size();
        appendQuadIndices<uint>(indices, base, 0, 1, 3, 4, true);
        appendQuadIndices<uint>(indices, base, 1, 2, 4, 5, true);
        appendQuadIndices<uint>(indices, base, 3, 4, 6, 7, true);
        appendQuadIndices<uint>(indices, base, 4, 5, 7, 8, true);
        vertices.insert(vertices.end(), vers.begin(), vers.end());
    }
    void triangulateBrushEndingFrondGroup(const Branch& branch, const FrondGroup& frondGroup,
                                          const float size, const LodInfo& lodInfo,
                                          vector<Vertex>& vertices, vector<uint>& indices)
    {
        const BrushEndingFrondParameters& param = frondGroup.brushEndingParam;
        const float halfSize = size / 2;
        const float relativeOffset = param.locationOffset / branch.totalLength;
        const float xyAngle = deg2rad(param.spreadAngle / 2);
        const Point location = branch.getPoint(1.0f - relativeOffset);

        // Build volume geometry
        static const uint NumQuads = 3;
        for (uint idx = 0; idx < NumQuads; ++idx)
        {
            const float zAngle = 2.0f * pi * idx / NumQuads;
            const float3 position = location.position;
            const float3 side = location.xAxis * cos(zAngle) + location.yAxis * sin(zAngle);
            const float3 direction = location.zAxis * matrix::rotationAxis(side, xyAngle);
            const float3 normal = cross(side, direction);

            // triangulate
            array<Vertex, 5> vers;
            vers[0].pos = position - halfSize * side;
            vers[0].uv = float2(0.0f, 0.0f);
            vers[0].edgeOscillation = param.junctionOscillation;
            vers[1].pos = position + halfSize * side;
            vers[1].uv = float2(1.0f, 0.0f);
            vers[1].edgeOscillation = param.junctionOscillation;
            vers[2].pos = position - halfSize * side + size * direction;
            vers[2].uv = float2(0.0f, 1.0f);
            vers[2].edgeOscillation = param.edgeOscillation;
            vers[3].pos = position + halfSize * side + size * direction;
            vers[3].uv = float2(1.0f, 1.0f);
            vers[3].edgeOscillation = param.edgeOscillation;
            vers[4] = Vertex(vers[0], vers[3], 0.5f);
            vers[4].pos += normal * size * param.centerOffset;

            // Set wind
            for (Vertex& vertex : vers)
            {
                vertex.branchAdherence = location.branchAdherence;
                vertex.phase = location.phase;
            }

            // put data to buffers
            const uint base = vertices.size();
            if (param.centerOffset == 0)
            {
                appendQuadIndices<uint>(indices, base, 0, 1, 2, 3, true);
                vertices.insert(vertices.end(), vers.begin(), vers.end() - 1);
            }
            else
            {
                vertices.insert(vertices.end(), vers.begin(), vers.end());
                indices.insert(indices.end(), { base + 0, base + 1, base + 4 });
                indices.insert(indices.end(), { base + 1, base + 3, base + 4 });
                indices.insert(indices.end(), { base + 3, base + 2, base + 4 });
                indices.insert(indices.end(), { base + 2, base + 0, base + 4 });
            }
        }
    }
    /// @}

    UniformRandom m_random;
};


// Main
TreeAsset::TreeAsset()
{
    // Root has zero frequency
    m_root.p.frequency = 0;
}
TreeAsset::~TreeAsset()
{
}


// Get hierarchy root
TreeAsset::BranchGroup& TreeAsset::getRootGroup()
{
    return m_root;
}


// Generate
void TreeAsset::generateSkeleton(const LodInfo& lodInfo, vector<Branch>& trunksArray) const
{
    InternalGenerator generator;
    generator.generateBranchGroup(lodInfo, m_root, trunksArray, nullptr);
}


// Triangulate
uint TreeAsset::triangulateBranches(const LodInfo& lodInfo,
                                    const vector<Branch>& trunksArray, 
                                    vector<Vertex>& vertices, vector<uint>& indices) const
{
    const uint startIndex = indices.size();
    InternalBranchTriangulator triangulator;
    for (const Branch& branch : trunksArray)
    {
        triangulator.triangulateBranch(branch, lodInfo, vertices, indices);
    }
    const uint numTriangles = (indices.size() - startIndex) / 3;
    return numTriangles;
}
uint TreeAsset::triangulateFronds(const LodInfo& lodInfo,
                                  const vector<Branch>& trunksArray, 
                                  vector<Vertex>& vertices, vector<uint>& indices) const
{
    // Triangulate
    const uint startIndex = indices.size();
    InternalFrondTriangulator triangulator;
    for (const Branch& branch : trunksArray)
    {
        triangulator.triangulateBranch(branch, lodInfo, vertices, indices);
    }

    // Generate TBN
    const uint numTriangles = (indices.size() - startIndex) / 3;
    computeNormalSpace(vertices.data(), vertices.size(), 
                       indices.data() + startIndex, numTriangles);
    normalizeNormalSpace(vertices.data(), vertices.size());
    computeTangentSpace(vertices.data(), vertices.size(),
                        indices.data() + startIndex, numTriangles);

    // Return number of triangles
    return numTriangles;
}


// Wind stuff
float TreeAsset::computeMainAdherence(const float relativeHeight, const float trunkStrength)
{
    return pow(max(relativeHeight, 0.0f),
               1 / (1 - trunkStrength));
}
void TreeAsset::computeMainAdherence(vector<Vertex>& vertices)
{
    // Compute max height
    float maxHeight = 0.0f;
    for (Vertex& vertex : vertices)
    {
        maxHeight = max(maxHeight, vertex.pos.y);
    }

    // Compute wind
    auto compute = [this](const float relativeHeight) -> float
    {
        return computeMainAdherence(relativeHeight, 
                                    m_modelParameters.trunkStrength);
    };
    for (Vertex& vertex : vertices)
    {
        vertex.mainAdherence = compute(vertex.pos.y / maxHeight);
    }
}
void TreeAsset::normalizeBranchesAdherence(vector<Vertex>& vertices)
{
    // Compute max adherence
    float maxAdherence = numeric_limits<float>::epsilon();
    for (Vertex& vertex : vertices)
    {
        maxAdherence = max(maxAdherence, vertex.branchAdherence);
    }

    // Compute wind
    for (Vertex& vertex : vertices)
    {
        vertex.branchAdherence /= maxAdherence;
    }
}
void TreeAsset::normalizePhases(vector<Vertex>& vertices)
{
    for (Vertex& vertex : vertices)
    {
        vertex.phase = fract(vertex.phase);
    }
}


// Main generator
void TreeAsset::generateMeshes(const vector<LodInfo>& lodInfos, 
                               vector<shared_ptr<MeshAsset>>& meshes)
{
    // Generate each LOD
    debug_assert(lodInfos.size() == meshes.size());
    for (uint idx = 0; idx < lodInfos.size(); ++idx)
    {
        const LodInfo& lodInfo = lodInfos[idx];
        MeshAsset& mesh = *meshes[idx];

        // Generate branches
        // #TODO It is possible to move branch generation out of the loop
        vector<TreeAsset::Branch> branches;
        generateSkeleton(lodInfo, branches);

        // Triangulate all
        vector<TreeAsset::Vertex> vertices;
        vector<uint> indices;
        const uint numBranchTriangles = triangulateBranches(lodInfo, branches, vertices, indices);
        const uint numFrondTriangles = triangulateFronds(lodInfo, branches, vertices, indices);
        computeMainAdherence(vertices);
        normalizeBranchesAdherence(vertices);
        normalizePhases(vertices);

        // Compute AABB and radius
        const AABB aabb = computeAabb(vertices.data(), vertices.size());
        const float3 aabbCenter = static_cast<float3>(aabb.center());
        m_modelParameters.modelRadius = 0.0f;
        for (const Vertex& vertex : vertices)
        {
            const float dist = length(swiz<X, O, Z>(vertex.pos - aabbCenter));
            m_modelParameters.modelRadius = max(m_modelParameters.modelRadius, dist);
        }

        // Save
        mesh.setVertexData(vertices.data(), vertices.size());
        mesh.setIndexData(indices.data(), indices.size());
        mesh.setParameters(m_modelParameters);
        mesh.addSubset(0, primitiveIndices(PrimitiveType::Triangle, numBranchTriangles));
        mesh.addSubset(1, primitiveIndices(PrimitiveType::Triangle, numFrondTriangles));
        mesh.setAabb(aabb);
        mesh.setUniformBuffer(m_modelParameters.model);
    }
}


// Implement Asset
string TreeAsset::extension() const
{
    return ".ini";
}
void TreeAsset::doCompileAndLoad(StreamInterface& source)
{
    INIReader ini;
    ini.parse(source.text());

    // Load parameters
    if (ini.hasSection(""))
    {
        loadGlobalParameters(ini.section(""));
        ini.dropSection("");
    }

    // Read groups
    static const string BranchGroupName = "branch_";
    static const string FrondGroupName = "frond_";
    static const BranchGroupParam BranchGroupDefault{};
    static const FrondGroup FrondGroupDefault{};
    for (const auto& item : ini.sections())
    {
        const string& name = item.first;
        const INISection& section = item.second;

        if (name[0] == '$')
        {
            // Skip implementation sections
        }
        else if (boost::starts_with(name, BranchGroupName))
        {
            loadBranchGroup(ini, name);
        }
        else if (boost::starts_with(name, FrondGroupName))
        {
            loadFrondGroup(ini, name);
        }
        else
        {
            LS_THROW(0, IoException, "Odd section [", name, "]");
        }
    }
}
void TreeAsset::doSerialize(Archive<Buffer> archive)
{
    if (archive.isRead())
    {
        throw IoException();
        // #TODO implement it
        debug_assert(0);
    }
}
void TreeAsset::loadGlobalParameters(const INISection& section)
{
    setDithering(section.get("dithering", 200.0f));
    setTrunkStrength(section.get("trunk_strength", 0.5f));
    setTrunkMagnitude(section.get("trunk_magnitude", 0.0f));
    setBranchMagnitude(section.get("branch_magnitude_m", 0.0f),
                        section.get("branch_magnitude_t", 0.0f));
    setBranchTurbulenceFrequency(section.get("branch_turbulence_frequency", 0.0f));
    setBranchMainPhaseModifier(section.get("branch_main_phase_modifier", 0.0f));
    setEdgeMagnitude(section.get("edge_magnitude_m", 0.0f),
                        section.get("edge_magnitude_t", 0.0f));
    setEdgeFrequency(section.get("edge_frequency", 0.0f));

    const float2 switchFadeInterval = section.get("switch_fade_interval", float2(0.0f));
    const float2 verticalFadeInterval = section.get("vertical_fade_interval", float2(0.0f));
    const float2 skewFadeInterval = section.get("skew_fade_interval", float2(0.0f));

    setSwitchFadeInterval(switchFadeInterval.x, switchFadeInterval.y);
    setVerticalFadeInterval(verticalFadeInterval.x, verticalFadeInterval.y);
    setSkewFadeInterval(skewFadeInterval.x, skewFadeInterval.y);

    setProxyEdgeMagnitude(section.get("proxy_edge_magnitude_m", 0.0f),
                            section.get("proxy_edge_magnitude_t", 0.0f));
    setProxyEdgeFrequency(section.get("proxy_edge_frequency", 0.0f));
    setProxyShadowPad(section.get("proxy_shadow_pad", 0.0f));

    LS_THROW(!section.hasUnused(), IoException,
             "Unknown variable [", section.firstUnused(), "]");
}
TreeAsset::BranchGroup& TreeAsset::loadBranchGroup(const INIReader& source, const string& name)
{
    // Group is already created
    if (m_branchGroups[name])
    {
        return *m_branchGroups[name];
    }

    // Lock group
    m_loadingBranchGroups.emplace(name);
    
    // Error if there is no such group
    LS_THROW(source.hasSection(name), IoException, "Branch Group [", name, "] is not found");
    const INISection& section = source.section(name);

    // Get parent branch group name
    const string parentGroupName = section.get<string>("parent");
    LS_THROW(m_loadingBranchGroups.count(parentGroupName) == 0,
             IoException, 
             "Circular reference with [", parentGroupName, "] <- [", name, "]");

    // Load parent branch group
    BranchGroup* parentGroup = parentGroupName.empty() 
        ? nullptr 
        : &loadBranchGroup(source, parentGroupName);

    // Add new group
    BranchGroup& branchGroup = (parentGroup ? *parentGroup : m_root).addBranchGroup();

    // Load group from section
    section.load("branch_mat.id", branchGroup.p.branchMaterial.materialIndex);
    section.load("branch_mat.scale", branchGroup.p.branchMaterial.textureScale);

    section.load("seed", branchGroup.p.seed);
    section.load("frequency", branchGroup.p.frequency);
    section.load("geometry_multiplier", branchGroup.p.geometryMultiplier);

    section.load("position", branchGroup.p.position);
    section.load("direction", branchGroup.p.direction);

    section.load("distrib.type", branchGroup.p.distrib.type);
    section.load("distrib.angle_step", branchGroup.p.distrib.angleStep);
    section.load("distrib.angle_rand", branchGroup.p.distrib.angleRandomness);

    section.load("distrib_function", branchGroup.p.distribFunction);
    section.load("location", branchGroup.p.location);

    section.load("length", branchGroup.p.length);
    section.load("radius", branchGroup.p.radius);
    section.load("angle", branchGroup.p.angle);
    section.load("weight", branchGroup.p.weight);

    section.load("branch_adherence", branchGroup.p.branchAdherence);
    section.load("phase_random", branchGroup.p.phaseRandom);

    LS_THROW(!section.hasUnused(), IoException,
             "Unknown variable [", section.firstUnused(), "]");

    // Unlock group
    m_loadingBranchGroups.erase(name);
    m_branchGroups[name] = &branchGroup;
    return branchGroup;
}
TreeAsset::FrondGroup& TreeAsset::loadFrondGroup(const INIReader& source, const string& name)
{
    // Error if there is no such group
    LS_THROW(source.hasSection(name), IoException, "Frond Group [", name, "] is not found");
    const INISection& section = source.section(name);

    // Get parent branch group name
    const string parentGroupName = section.get<string>("parent");
    LS_THROW(!parentGroupName.empty(), IoException, "Frond Group [", name, "] has no parent");

    // Load parent branch group
    BranchGroup& parentGroup = loadBranchGroup(source, parentGroupName);

    // Create frond group
    FrondGroup& frondGroup = parentGroup.addFrondGroup();

    section.load("type", frondGroup.type);
    section.load("size", frondGroup.sizeRange);

    switch (frondGroup.type)
    {
    case TreeAsset::FrondType::RagEnding:
        section.load("location_offset", frondGroup.ragEndingParam.locationOffset);
        section.load("center_offset", frondGroup.ragEndingParam.centerOffset);
        section.load("z_bending", frondGroup.ragEndingParam.zBendingFactor);
        section.load("x_bending", frondGroup.ragEndingParam.xBendingFactor);
        section.load("junction_oscillation", frondGroup.ragEndingParam.junctionOscillation);
        section.load("edge_oscillation", frondGroup.ragEndingParam.edgeOscillation);
    case TreeAsset::FrondType::BrushEnding:
        section.load("location_offset", frondGroup.brushEndingParam.locationOffset);
        section.load("center_offset", frondGroup.brushEndingParam.centerOffset);
        section.load("spread_angle", frondGroup.brushEndingParam.spreadAngle);
        section.load("junction_oscillation", frondGroup.brushEndingParam.junctionOscillation);
        section.load("edge_oscillation", frondGroup.brushEndingParam.edgeOscillation);
    default:
        break;
    }

    LS_THROW(!section.hasUnused(), IoException,
             "Unknown variable [", section.firstUnused(), "]");

    return frondGroup;
}


// Set/Get parameters
void TreeAsset::setDithering(const float dithering)
{
    m_modelParameters.model.dithering = dithering;
    m_modelParameters.proxy.dithering = dithering;
}
void TreeAsset::setTrunkStrength(const float trunkStrength)
{
    m_modelParameters.trunkStrength = trunkStrength;
}
void TreeAsset::setTrunkMagnitude(const float trunkMagnitude)
{
    m_modelParameters.model.trunkMagnitude = trunkMagnitude;
    m_modelParameters.proxy.trunkMagnitude = trunkMagnitude;
}
void TreeAsset::setBranchMagnitude(const float branchMainMagnitude,
                                   const float branchTurbulenceMagnitude)
{
    m_modelParameters.model.branchMainMagnitude = branchMainMagnitude;
    m_modelParameters.model.branchTurbulenceMagnitude = branchTurbulenceMagnitude;
}
void TreeAsset::setBranchTurbulenceFrequency(const float branchFrequency)
{
    m_modelParameters.model.branchFrequency = branchFrequency;
}
void TreeAsset::setBranchMainPhaseModifier(const float phaseModifier)
{
    m_modelParameters.model.branchPhaseModifier = phaseModifier;
}
void TreeAsset::setEdgeMagnitude(const float edgeMainMagnitude,
                                 const float edgeTurbulenceMagnitude)
{
    m_modelParameters.model.edgeMainMagnitude = edgeMainMagnitude;
    m_modelParameters.model.edgeTurbulenceMagnitude = edgeTurbulenceMagnitude;
}
void TreeAsset::setEdgeFrequency(const float edgeFrequency)
{
    m_modelParameters.model.edgeFrequency = edgeFrequency;
}
void TreeAsset::setSwitchFadeInterval(const float from, const float to)
{
    m_modelParameters.proxy.fadeBegin.x = from;
    m_modelParameters.proxy.fadeEnd.x = to;
}
void TreeAsset::setVerticalFadeInterval(const float from, const float to)
{
    m_modelParameters.proxy.fadeBegin.y = from;
    m_modelParameters.proxy.fadeEnd.y = to;
}
void TreeAsset::setSkewFadeInterval(const float from, const float to)
{
    m_modelParameters.proxy.fadeBegin.z = from;
    m_modelParameters.proxy.fadeEnd.z = to;
}
void TreeAsset::setProxyEdgeMagnitude(const float edgeMainMagnitude,
                                      const float edgeTurbulenceMagnitude)
{
    m_modelParameters.proxy.edgeMainMagnitude = edgeMainMagnitude;
    m_modelParameters.proxy.edgeTurbulenceMagnitude = edgeTurbulenceMagnitude;
}
void TreeAsset::setProxyEdgeFrequency(const float edgeFrequency)
{
    m_modelParameters.proxy.edgeFrequency = edgeFrequency;
}
void TreeAsset::setProxyShadowPad(const float shadowPad)
{
    m_modelParameters.proxy.shadowPad = shadowPad;
}
