#include "pch.h"
#include "ls/asset/Asset.h"

#include "ls/asset/AssetManager.h"
#include "ls/asset/all.h"
#include "ls/core/INIReader.h"
#include "ls/core/Worker.h"
#include "ls/io/FileSystemInterface.h"
#include "ls/io/StreamInterface.h"

using namespace ls;
template <>
AssetType ls::fromString<AssetType>(const char* name)
{
    static map<string, AssetType> m = {
        make_pair("Effect", AssetType::Effect),
        make_pair("EffectLibrary", AssetType::EffectLibrary),
        make_pair("Layout", AssetType::Layout),
        make_pair("Material", AssetType::Material),
        make_pair("Mesh", AssetType::Mesh),
        make_pair("Model", AssetType::Model),
        make_pair("Palette", AssetType::Palette),
        make_pair("Text", AssetType::Text),
        make_pair("Texture", AssetType::Texture),
        make_pair("Tree", AssetType::Tree),
        make_pair("World", AssetType::World),
    };
    debug_assert(m.size() + 1 == NumAssetTypes);
    return getOrDefault(m, name, AssetType::Unknown);
}
const char* ls::asString(const AssetType type)
{
    switch (type)
    {
    case AssetType::Effect:
        return "Effect";
    case AssetType::EffectLibrary:
        return "EffectLibrary";
    case AssetType::Layout:
        return "Layout";
    case AssetType::Material:
        return "Material";
    case AssetType::Mesh:
        return "Mesh";
    case AssetType::Model:
        return "Model";
    case AssetType::Palette:
        return "Palette";
    case AssetType::Text:
        return "Text";
    case AssetType::Texture:
        return "Texture";
    case AssetType::Tree:
        return "Tree";
    default:
        return "";
    }
}
Asset::Ptr Asset::construct(const AssetType type)
{
    switch (type)
    {
    case AssetType::Effect:
        return make_shared<EffectAsset>();
    case AssetType::EffectLibrary:
        return make_shared<EffectLibraryAsset>();
    case AssetType::Layout:
        return make_shared<LayoutAsset>();
    case AssetType::Material:
        return make_shared<MaterialAsset>();
    case AssetType::Mesh:
        return make_shared<MeshAsset>();
    case AssetType::Model:
        return make_shared<ModelAsset>();
    case AssetType::Palette:
        return make_shared<PaletteAsset>();
    case AssetType::Text:
        return make_shared<TextAsset>();
    case AssetType::Texture:
        return make_shared<TextureAsset>();
    case AssetType::Tree:
        return make_shared<TreeAsset>();
    case AssetType::World:
        return make_shared<WorldAsset>();
    default:
        return nullptr;
    }
}


// Main
const string Asset::Ini = ".ini";
const string Asset::Dat = ".dat";
const string Asset::Bin = ".bin";
Asset::Asset(const AssetType type)
    : m_type(type)
{

}
Asset::~Asset()
{
}


// Create asset
bool Asset::exist(FileSystemInterface& fileSystem, const string& name) const
{
    const bool isConfigExist 
        = fileSystem.isRegularFile(name + Ini)
        || fileSystem.isRegularFile(name + Dat);
    const bool isDataExist 
        = fileSystem.isRegularFile(name + extension())
        || fileSystem.isRegularFile(name + Bin);

    switch (category())
    {
    case AssetCategory::Simple:
    case AssetCategory::DataCompiled:
        return isDataExist;
    case AssetCategory::Config:
        return isConfigExist;
    case AssetCategory::ConfigData:
        return isConfigExist && isDataExist;
    default:
        debug_assert(0);
        return false;
    }
}
void Asset::initialize(AssetManager& assetManager, const string& name)
{
    m_assetManager = &assetManager;
    m_fileSystem = &m_assetManager->fileSystem();
    m_assetName = name;
    doPostInitialize();
}


// Load asset
void Asset::load()
{
    LS_THROW(!m_assetName.empty(), LogicException, "Asset has no name");
    debug_assert(extension().empty() == (category() == AssetCategory::Config));

    // Load asset data
    switch (category())
    {
    case AssetCategory::Simple:
        loadData();
        break;
    case AssetCategory::Config:
        loadConfig();
        break;
    case AssetCategory::ConfigData:
        loadConfig();
        loadData();
        break;
    case AssetCategory::DataCompiled:
        loadDataCompiled();
        break;
    default:
        debug_assert(0);
    };

    // Restore
    doRestore();
}
void Asset::loadConfig()
{
    // Compile and load config
    compileAndLoad(m_assetName + Ini, m_assetName + Dat,
                   std::bind(&Asset::compileLoadConfig, this, _1, _2),
                   std::bind(&Asset::pureLoadConfig, this, _1));
}
void Asset::loadData()
{
    // Find data file
    const string dataName = m_assetName + extension();
    Stream dataStream = m_fileSystem->openStream(dataName);
    LS_THROW(dataStream, IoException, "Data file [", dataName, "] is not found");

    // Read asset data
    pureLoadData(*dataStream);
}
void Asset::loadDataCompiled()
{
    // Compile and load data
    compileAndLoad(m_assetName + extension(), m_assetName + Bin,
                   std::bind(&Asset::compileLoadData, this, _1, _2),
                   std::bind(&Asset::pureLoadData, this, _1));
}


// Load - common helpers
template <class F, class G>
void Asset::compileAndLoad(const string& sourceName, const string& compiledName, 
                           F compileLoad, G load)
{
    const bool isSourceExist = m_fileSystem->isRegularFile(sourceName);
    const bool isCompiledExist = m_fileSystem->isRegularFile(compiledName);
    const bool isCompiledOutOfDate = m_fileSystem->lastWriteTime(sourceName) > m_fileSystem->lastWriteTime(compiledName);

    // Try to load compiled if possible
    if (isCompiledExist && !isCompiledOutOfDate)
    {
        // Just load
        Stream compiledStream = m_fileSystem->openStream(compiledName);
        LS_THROW(compiledStream, IoException, "Compiled file [", compiledName, "] is not found");
        try
        {
            load(*compiledStream);
            return;
        }
        catch (const std::exception& ex)
        {
            logError(ex.what());
        }
    }

    // Compile if not exist, out-of-date or failed to load
    Stream sourceStream = m_fileSystem->openStream(sourceName);
    Stream compiledStream = m_fileSystem->openStream(compiledName, StreamAccess::ReadWriteNew);
    LS_THROW(sourceStream, IoException, "Source file [", sourceName, "] is not found");
    LS_THROW(compiledStream, IoException, "Destination file [", compiledName, "] cannot be opened");

    compileLoad(*sourceStream, *compiledStream);
}
void Asset::compileLoadConfig(StreamInterface& sourceStream, StreamInterface& compiledStream)
{
    // Read INI
    INIReader iniReader;
    iniReader.parse(sourceStream.text());

    // Check type
    const AssetType type = fromString<AssetType>(iniReader.pull("", "$type", ""));
    LS_THROW(type == m_type, 
             IoException, 
             "Asset type mismatch: request=", m_type, " file=", type);

    // Load asset
    try
    {
        doReadConfig(iniReader);
    }
    catch (const FormatException& ex)
    {
        LS_THROW(0, IoException, ex.what());
    }

    // Save INI to output
    pureSaveConfig(compiledStream);
}
void Asset::pureSaveConfig(StreamInterface& destStream)
{
    Buffer buffer;
    doSerializeConfig(Archive<Buffer>(buffer, ArchiveWrite));
    buffer.push(destStream);
}
void Asset::pureLoadConfig(StreamInterface& compiledStream)
{
    Buffer buffer = Buffer::pull(compiledStream);
    doSerializeConfig(Archive<Buffer>(buffer, ArchiveRead));
}
void Asset::compileLoadData(StreamInterface& sourceStream, StreamInterface& compiledStream)
{
    // Compile asset
    doCompileAndLoad(sourceStream);

    // Save data
    pureSaveData(compiledStream);
}
void Asset::pureSaveData(StreamInterface& destStream)
{
    Buffer buffer;
    doSerialize(Archive<Buffer>(buffer, ArchiveWrite));
    buffer.push(destStream);
}
void Asset::pureLoadData(StreamInterface& compiledStream)
{
    Buffer buffer = Buffer::pull(compiledStream);
    doSerialize(Archive<Buffer>(buffer, ArchiveRead));
}


// Load - defaults
void Asset::doPostInitialize()
{

}
void Asset::doSerialize(Archive<Buffer> /*archive*/)
{
    LS_THROW(0, LogicException, "This type of asset cannot be serialized: failed to load [", m_assetName, "]");
}
void Asset::doReadConfig(INIReader& /*ini*/)
{
    LS_THROW(0, LogicException, "This type of asset has no header: failed to load [", m_assetName, "]");
}
void Asset::doSerializeConfig(Archive<Buffer> /*archive*/)
{
    LS_THROW(0, LogicException, "This type of asset has no header: failed to load [", m_assetName, "]");
}
void Asset::doCompileAndLoad(StreamInterface& /*source*/)
{
    LS_THROW(0, LogicException, "This type of asset cannot be compiled: failed to load [", m_assetName, "]");
}


// Save asset
void Asset::save()
{
    debug_assert(m_fileSystem && !m_assetName.empty());

    // Re-direct
    switch (category())
    {
    case AssetCategory::Simple:
        saveSimple(extension());
        break;
    case AssetCategory::Config:
        saveConfig();
        break;
    case AssetCategory::ConfigData:
        saveSimple(extension());
        saveConfig();
        break;
    case AssetCategory::DataCompiled:
        saveSimple(Bin);
        break;
    default:
        debug_assert(0);
    };
}
void Asset::saveSimple(const string& ext)
{
    // Open destination
    const string destName = m_assetName + ext;
    Stream destStream = m_fileSystem->openStream(destName, StreamAccess::WriteNew);
    LS_THROW(destStream, IoException, "Cannot open destination [", destName, "]");

    // Save asset data
    pureSaveData(*destStream);
}
void Asset::saveConfig()
{
    // Open destination
    const string destName = m_assetName + Dat;
    Stream destStream = m_fileSystem->openStream(destName, StreamAccess::WriteNew);
    LS_THROW(destStream, IoException, "Cannot open destination [", destName, "]");

    // Save asset data
    pureSaveConfig(*destStream);
}


// Restore
void Asset::doRestore()
{
    // Do nothing
}


// Gets
AssetType Asset::type() const
{
    return m_type;
}


// Interface
Asset::Ptr Asset::loadAsset(const AssetType type, const string& name, const bool silent)
{
    const string folderName = m_assetName.substr(0, m_assetName.find_last_of('/'));
    return m_assetManager->loadAsset(type, name, folderName);
}
AssetManager* Asset::assetManager()
{
    return m_assetManager;
}
Renderer* Asset::renderer()
{
    return m_assetManager->renderer();
}
MaterialCache* Asset::materialCache()
{
    return m_assetManager->materialCache();
}
Worker& Asset::worker()
{
    return m_assetManager->worker();
}
FileSystemInterface& Asset::fileSystem()
{
    return *m_fileSystem;
}
const string& Asset::name() const
{
    return m_assetName;
}
