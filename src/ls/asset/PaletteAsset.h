/// @file ls/asset/PaletteAsset.h
/// @brief Palette Asset
#pragma once

#include "ls/common.h"
#include "ls/asset/Asset.h"

namespace ls
{
    /// @addtogroup LsAsset
    /// @{

    /// @brief Palette Asset
    class PaletteAsset
        : public AssetT<AssetType::Palette, AssetCategory::Config>
    {
    public:
        /// @brief Ctor
        PaletteAsset();
        /// @brief Dtor
        ~PaletteAsset();

        /// @brief Get ubyte4 color
        /// @throw ArgumentException if not found
        ubyte4 getColor4u(const string& name) const;
        /// @brief Get float4 color
        /// @throw ArgumentException if not found
        float4 getColor4f(const string& name) const;
        /// @brief Get float3 color
        /// @throw ArgumentException if not found
        float3 getColor3f(const string& name) const;
    private:
        /// @name Implement Asset
        /// @{
        virtual void doReadConfig(INIReader& ini) override;
        virtual void doSerializeConfig(Archive<Buffer> archive) override;
        virtual void doRestore() override;
        /// @}
    private:
        hamap<string, ubyte4> m_colors;
    };

    /// @}
}