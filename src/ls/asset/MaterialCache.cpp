#include "pch.h"
#include "ls/asset/MaterialCache.h"

#include "ls/gapi/Renderer.h"

using namespace ls;
namespace 
{
    Culling cullingToGapi(const MaterialAsset::CullingMode material, const Culling base)
    {
        const uint materialCulling = static_cast<uint>(material);
        const uint baseCulling = static_cast<uint>(base);
        debug_assert(materialCulling < 3 && baseCulling < 3);
        static const Culling m[3][3] =
        {
            { Culling::Back, Culling::None, Culling::Front },
            { Culling::None, Culling::None, Culling::None },
            { Culling::Front, Culling::None, Culling::Back },
        };
        return m[baseCulling][materialCulling];
    }
}
/// @brief Material Cache - Context
struct ls::MaterialCacheContext
{
    StateDesc stateDesc;
    MaterialAsset::ShaderMode mode;
};
/// @brief Material Cache - Material
struct ls::MaterialCacheMaterial
{
    MaterialAsset::CullingMode cullingMode = MaterialAsset::CullingMode::Enabled;
    PrimitiveType primitiveType = PrimitiveType::Triangle;

    shared_ptr<LayoutAsset> layoutAsset;
    shared_ptr<EffectAsset> effectAsset;

    LayoutHandle layoutHandle;
    EffectHandle effectHandle;
    array<ProgramHandle, MaterialAsset::NumShaderModes> shaderPrograms;

    /// @brief Compare
    bool operator == (const MaterialCacheMaterial& another)
    {
        for (uint idx = 0; idx < MaterialAsset::NumShaderModes; ++idx)
        {
            if (shaderPrograms[idx] != another.shaderPrograms[idx])
            {
                return false;
            }
        }
        return cullingMode == another.cullingMode
            && primitiveType == another.primitiveType
            && layoutAsset == another.layoutAsset
            && effectAsset == another.effectAsset;
    }
    /// @brief Cache Key
    struct Item
    {
        MaterialCacheContext* key;
        StateHandle state;
        operator MaterialCacheContext* ()
        {
            return key;
        }
    };
    /// @brief Sorted array of cached states
    vector<Item> cachedStates;
    /// @brief Add new element
    void addToCache(MaterialCacheContext* key, StateHandle state)
    {
        cachedStates.push_back(Item{ key, state });
        std::sort(cachedStates.begin(), cachedStates.end());
    }
    /// @brief Find in cache
    StateHandle findInCache(MaterialCacheContext* key)
    {
        auto iter = std::lower_bound(cachedStates.begin(), cachedStates.end(), key);
        return (iter != cachedStates.end() && iter->key == key) 
            ? iter->state 
            : nullptr;
    }
    /// @brief Erase element from array
    void removeFromCache(MaterialCacheContext* key, Renderer& renderer)
    {
        auto iter = std::find(cachedStates.begin(), cachedStates.end(), key);
        if (iter != cachedStates.end())
        {
            renderer.destroyState(iter->state);
            cachedStates.erase(iter);
        }
    }
};


// Main
MaterialCache::MaterialCache(Renderer& renderer)
    : m_renderer(renderer)
{

}
MaterialCache::~MaterialCache()
{
    reset();
}
MaterialCacheContext* MaterialCache::registerContext(const StateDesc& stateDesc,
                                                     const MaterialAsset::ShaderMode mode)
{
    // Add new
    m_contexts.emplace_back();
    MaterialCacheContext& contextDesc = m_contexts.back();

    // Fill
    contextDesc.stateDesc = stateDesc;
    contextDesc.mode = mode;
    return &contextDesc;
}
void MaterialCache::unregisterContext(MaterialCacheContext* handle)
{
    // Materials must be thread-safe
    std::lock_guard<std::mutex> guard(m_mutex);

    // Remove all links
    for (MaterialCacheMaterial& material : m_materials)
    {
        material.removeFromCache(handle, m_renderer);
    }

    // Unregister context
    for (auto iter = m_contexts.begin(); iter != m_contexts.end(); ++iter)
    {
        if (&*iter == handle)
        {
            m_contexts.erase(iter);
            return;
        }
    }

    // Must be found
    debug_assert(0);
}
MaterialCacheMaterial* MaterialCache::findOrCreateMaterial(MaterialAsset& materialAsset)
{
    // Must be thread-safe
    std::lock_guard<std::mutex> guard(m_mutex);

    // Fill material desc
    MaterialCacheMaterial key;
    key.cullingMode = materialAsset.cullingMode();
    key.primitiveType = materialAsset.primitiveType();

    key.layoutAsset = materialAsset.layout();
    key.effectAsset = materialAsset.effect();

    // Load programs
    for (uint idx = 0; idx < MaterialAsset::NumShaderModes; ++idx)
    {
        auto shaderMode = static_cast<MaterialAsset::ShaderMode>(idx);
        key.shaderPrograms[idx] = materialAsset.gapiProgram(shaderMode);
    }

    // Try to find
    auto iter = std::find(m_materials.begin(), m_materials.end(), key);
    if (iter != m_materials.end())
    {
        return &*iter;
    }

    // Check material
    auto programIter = std::find_if(key.shaderPrograms.begin(),
                                    key.shaderPrograms.end(),
                                    [](ProgramHandle program) { return !!program; });
    if (programIter == key.shaderPrograms.end())
    {
        return nullptr;
    }

    // Should create new
    m_materials.push_back(key);
    MaterialCacheMaterial& desc = m_materials.back();

    // Load handles
    desc.effectHandle = desc.effectAsset->gapiEffect();
    // It is pretty safe to use any program because all programs shall have same layout
    desc.layoutHandle = desc.effectAsset->gapiCachedLayout(*programIter, *desc.layoutAsset);

    return &desc;
}
StateHandle MaterialCache::gapiState(MaterialCacheContext* context, MaterialCacheMaterial* material)
{
    debug_assert(context);
    debug_assert(material);

    // Find in cache
    StateHandle state = material->findInCache(context);
    if (!!state)
    {
        return state;
    }

    // Create new state
    StateDesc desc = context->stateDesc;
    desc.program = material->shaderPrograms[static_cast<uint>(context->mode)];
    desc.layout = material->layoutHandle;
    desc.primitive = material->primitiveType;
    desc.rasterizer.culling = cullingToGapi(material->cullingMode, desc.rasterizer.culling);
    state = m_renderer.createState(desc);

    // Store in cache
    material->addToCache(context, state);

    return state;
}
void MaterialCache::reset()
{
    // Must be thread-safe
    std::lock_guard<std::mutex> guard(m_mutex);

    // Destroy all states
    for (MaterialCacheMaterial& material : m_materials)
    {
        for (MaterialCacheMaterial::Item& item : material.cachedStates)
        {
            m_renderer.destroyState(item.state);
        }
    }

    // Drop arrays
    m_materials.clear();
}
