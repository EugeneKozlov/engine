/// @file ls/asset/EffectAsset.h
/// @brief Effect Asset
#pragma once

#include "ls/common.h"
#include "ls/asset/Asset.h"
#include "ls/gapi/common.h"

namespace ls
{
    class LayoutAsset;

    /// @addtogroup LsAsset
    /// @{

    /// @brief Effect Asset
    class EffectAsset
        : public AssetT<AssetType::Effect, AssetCategory::DataCompiled>
    {
    public:
        /// @brief Ctor
        EffectAsset();
        /// @brief Dtor
        ~EffectAsset();

        /// @brief Drop buffers stored in RAM
        void dropRam();
        /// @brief Is GAPI ready?
        bool isGapiReady() const;

        /// @name GAPI
        /// @pre GAPI is ready
        /// @{

        /// @brief Get GAPI effect
        const EffectHandle& gapiEffect() const;
        /// @brief Get GAPI layout (cached)
        LayoutHandle gapiCachedLayout(ProgramHandle program,
                                      const LayoutAsset& layoutAsset);
        /// @}
    private:
        /// @name Implement Asset
        /// @{
        virtual string extension() const override;
        virtual void doSerialize(Archive<Buffer> archive) override;
        virtual void doCompileAndLoad(StreamInterface& source) override;
        virtual void doRestore() override;
        /// @}

        void dropCache();
    private:
        /// @name RAM storage
        /// @{
        vector<ubyte> m_compiledEffect;
        /// @}

        /// @name GAPI storage
        /// @{
        EffectHandle m_gapiEffect;
        map<ProgramHandle, LayoutHandle> m_cachedLayouts; ///< Layout Cache
        /// @}
    };

    /// @}
}