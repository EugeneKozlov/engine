/// @file ls/asset/Asset.h
/// @brief Asset
#pragma once

#include "ls/common.h"
#include "ls/io/common.h"
#include "ls/io/Buffer.h"

namespace ls
{
    class AssetManager;
    class Buffer;
    class INIReader;
    class MaterialCache;
    class Renderer;
    class Worker;

    /// @addtogroup LsAsset
    /// @{

    /// @brief Asset Type
    enum class AssetType : uint
    {
        /// @brief Unknown
        Unknown = 0,
        /// @brief Effect
        Effect,
        /// @brief Effect Library
        EffectLibrary,
        /// @brief Material
        Material,
        /// @brief Layout
        Layout,
        /// @brief Mesh
        Mesh,
        /// @brief Model
        Model,
        /// @brief Palette
        Palette,
        /// @brief Text
        Text,
        /// @brief Texture
        Texture,
        /// @brief Tree
        Tree,
        /// @brief World
        World,

        /// @brief Counter
        COUNT
    };
    /// @brief Asset type Category
    enum class AssetCategory
    {
        /// @brief Simple file, loaded as-is.
        Simple,
        /// @brief INI file (may be compiled)
        /// @note Extension for INI config is '.ini'
        /// @note Extension for compiled config is '.dat'
        /// @note @a extension must return empty string
        Config,
        /// @brief INI file (may be compiled) and data file
        /// @note Extension for INI config is '.ini', don't use it
        /// @note Extension for compiled config is '.dat', don't use it
        /// @note Extension must not be empty
        ConfigData,
        /// @brief Data file (may be compiled)
        /// @note Extension for compiled file is '.bin', don't use it
        /// @note Extension must not be empty
        DataCompiled,
    };

    /// @brief Number of asset types
    static const uint NumAssetTypes = static_cast<uint>(AssetType::COUNT);
    /// @brief Asset Type by string
    template <>
    AssetType fromString<AssetType>(const char* name);
    /// @brief Asset Type to string
    const char* asString(const AssetType type);

    /// @brief Asset
    class Asset
        : Noncopyable
    {
    public:
        /// @brief Binary Version
        static const uint BinaryVersion = 1;
        /// @brief INI config extension
        static const string Ini;
        /// @brief Compiled config extension
        static const string Dat;
        /// @brief Compiled data extension
        static const string Bin;
        /// @brief Asset Pointer
        using Ptr = shared_ptr<Asset>;
        /// @brief Enable If Asset
        template <class DerivedAsset, class Type = void>
        using EnableIfAsset = enable_if_t<is_base_of<Asset, DerivedAsset>::value, Type>;

        /// @name Casts
        /// @{

        /// @brief Cast to asset
        template <class DerivedAsset, class = EnableIfAsset<DerivedAsset>>
        static shared_ptr<DerivedAsset> cast(Ptr asset)
        {
            if (!asset || asset->type() != DerivedAsset::Type)
                return nullptr;
            else
                return std::static_pointer_cast<DerivedAsset>(asset);
        }
        /// @brief Cast to asset
        template <class DerivedAsset, class = EnableIfAsset<DerivedAsset>>
        static shared_ptr<const DerivedAsset> cast(shared_ptr<const Asset> asset)
        {
            if (!asset || asset->type() != DerivedAsset::Type)
                return nullptr;
            else
                return std::static_pointer_cast<const DerivedAsset>(asset);
        }
        /// @brief Cast to asset
        template <class DerivedAsset, class = EnableIfAsset<DerivedAsset>>
        static DerivedAsset& cast(Asset& asset)
        {
            debug_assert(asset.type() == DerivedAsset::Type);
            return static_cast<DerivedAsset&>(asset);
        }
        /// @brief Cast to asset
        template <class DerivedAsset, class = EnableIfAsset<DerivedAsset>>
        static const DerivedAsset& cast(const Asset& asset)
        {
            debug_assert(asset.type() == DerivedAsset::Type);
            return static_cast<const DerivedAsset&>(asset);
        }
        /// @brief Convert asset array
        template <class DerivedAsset>
        static void convert(const vector<Asset::Ptr>& src,
                            vector<shared_ptr<DerivedAsset>>& dest)
        {
            std::transform(src.begin(), src.end(),
                           std::back_inserter(dest),
                           (shared_ptr<DerivedAsset>(*) (Asset::Ptr)) &Asset::cast<DerivedAsset>);
        }
        /// @}
    protected:
        /// @brief Ctor
        Asset(const AssetType type);
    public:
        /// @brief Dtor
        virtual ~Asset();

        /// @brief Get asset category
        virtual AssetCategory category() const = 0;
        /// @brief Get data file extension (with dot)
        virtual string extension() const = 0;

        /// @name Initialization
        /// @note Never call these function from outside
        /// @{

        /// @brief Construct by type
        static Ptr construct(const AssetType type);
        /// @brief Returns true if folder contains some files related to asset (*.ini, *.dat or *.ext)
        bool exist(FileSystemInterface& fileSystem, const string& name) const;
        /// @brief Initialize parameters
        /// @param fileSystem
        ///   Asset file system
        /// @param name
        ///   Asset file name without extension, may be empty
        void initialize(AssetManager& assetManager, const string& name = "");
        /// @brief Load asset
        /// @throw IoException if failed to load
        /// @throw LogicException if asset has no name
        void load();
        /// @}

        /// @brief Save asset
        /// @throw LogicException if asset has no name
        virtual void save();

        /// @name Gets
        /// @{

        /// @brief Get type
        AssetType type() const;
        /// @brief Get asset manager
        AssetManager* assetManager();
        /// @brief Get renderer
        Renderer* renderer();
        /// @brief Get material cache
        MaterialCache* materialCache();
        /// @brief Get worker
        Worker& worker();
        /// @brief Get file system
        FileSystemInterface& fileSystem();
        /// @brief Get asset name
        const string& name() const;
        /// @}
    protected:
        /// @brief Load asset by name
        Ptr loadAsset(const AssetType type, const string& name, const bool silent);
        /// @brief Load asset by name
        template <class DerivedAsset>
        shared_ptr<DerivedAsset> loadAsset(const string& name)
        {
            return cast<DerivedAsset>(loadAsset(DerivedAsset::Type, name, false));
        }
        /// @brief Load asset by name (silent)
        template <class DerivedAsset>
        shared_ptr<DerivedAsset> loadAssetSilent(const string& name)
        {
            return cast<DerivedAsset>(loadAsset(DerivedAsset::Type, name, true));
        }
    private:
        /// @brief Post-initialize callback
        virtual void doPostInitialize();
        /// @brief Serialize asset data
        /// @throw IoException if something goes wrong
        /// @throw LogicException by default
        virtual void doSerialize(Archive<Buffer> archive);
        /// @brief Read asset config from INI
        /// @throw LogicException by default
        /// @throw FormatException is also allowed
        virtual void doReadConfig(INIReader& ini);
        /// @brief Serialize asset config
        /// @throw LogicException by default
        virtual void doSerializeConfig(Archive<Buffer> archive);
        /// @brief Compile and load source
        /// @throw LogicException by default
        virtual void doCompileAndLoad(StreamInterface& source);

        /// @brief Restore asset
        /// @note Asset should be fully loaded after this call
        /// @brief IoException if asset file not found
        virtual void doRestore();
    private:
        /// @brief Implements compilation concept
        /// @throw IoException if something is not found
        template <class F, class G>
        void compileAndLoad(const string& sourceName, const string& compiledName,
                            F compileLoad, G load);
        /// @brief Compile and load config
        void compileLoadConfig(StreamInterface& sourceStream, StreamInterface& compiledStream);
        /// @brief Only save config
        void pureSaveConfig(StreamInterface& destStream);
        /// @brief Only load config
        void pureLoadConfig(StreamInterface& compiledStream);
        /// @brief Compile and load data
        void compileLoadData(StreamInterface& sourceStream, StreamInterface& compiledStream);
        /// @brief Only save data
        void pureSaveData(StreamInterface& destStream);
        /// @brief Only load data
        void pureLoadData(StreamInterface& compiledStream);

        /// @name Implement loading
        /// @{
        void loadConfig();
        void loadData();
        void loadDataCompiled();
        /// @}

        /// @name Implement saving
        /// @{
        void saveSimple(const string& ext);
        void saveConfig();
        /// @}
    private:
        const AssetType m_type;

        AssetManager* m_assetManager = nullptr;
        FileSystemInterface* m_fileSystem = nullptr;
        string m_assetName;
    };

    /// @brief Asset template helper
    template <AssetType T, AssetCategory C>
    class AssetT
        : public Asset
    {
    public:
        /// @brief Type
        static const AssetType Type = T;
        /// @brief Category
        static const AssetCategory Category = C;
    public:
        /// @brief Ctor
        AssetT()
            : Asset(Type)
        {
        }
    public:
        virtual AssetCategory category() const override final
        {
            return Category;
        }
    };
    /// @brief Asset template helper for AssetCategory::Config
    template <AssetType T>
    class AssetT<T, AssetCategory::Config>
        : public Asset
    {
    public:
        /// @brief Type
        static const AssetType Type = T;
        /// @brief Category
        static const AssetCategory Category = AssetCategory::Config;
    public:
        /// @brief Ctor
        AssetT()
            : Asset(Type)
        {
        }
    public:
        virtual AssetCategory category() const override final
        {
            return Category;
        }
        virtual string extension() const override final
        {
            return "";
        }
    };


    /// @}
}
