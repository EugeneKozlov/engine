#include "pch.h"
#include "ls/asset/MaterialAsset.h"

#include "ls/asset/Asset.h"
#include "ls/asset/MaterialCache.h"
#include "ls/core/INIReader.h"
#include "ls/gapi/Renderer.h"

using namespace ls;
namespace
{
    MaterialAsset::CullingMode cullingByString(const string& str)
    {
        sint value = fromString<sint>(str);
        switch (value)
        {
        case -1:
            return MaterialAsset::CullingMode::Reverse;
        case 0:
            return MaterialAsset::CullingMode::Disabled;
        case 1:
        default:
            return MaterialAsset::CullingMode::Enabled;
        }
    }
    MaterialAsset::RenderingMode renderingModeByString(const string& str)
    {
        if (str == "opacity")
        {
            return MaterialAsset::RenderingMode::Opacity;
        }
        else if (str == "cutout")
        {
            return MaterialAsset::RenderingMode::Cutout;
        }
        else if (str == "transparent")
        {
            return MaterialAsset::RenderingMode::Transparent;
        }
        else
        {
            LS_THROW(0, FormatException, "Invalid rendering mode '", str, "'");
            return MaterialAsset::RenderingMode::Opacity;
        }
    }
}


// Main
MaterialAsset::MaterialAsset()
{

}
MaterialAsset::~MaterialAsset()
{

}


// Implement Asset
void MaterialAsset::doReadConfig(INIReader& ini)
{
    // #TODO Rewrite
    const INISection& header = ini.section("");

    // Load info
    m_cullingMode = cullingByString(getOrDefault(header, "culling", "1"));
    m_renderingMode = renderingModeByString(getOrDefault(header, "rendermode", "opacity"));
    m_rawResources = getOrDefault(header, "resource", "raw") == "raw";
    m_layout = getOrDefault(header, "layout");
    m_effect = getOrDefault(header, "effect");
    m_shaders[static_cast<uint>(ShaderMode::Proxy)] = getOrDefault(header, "shader_proxy");
    m_shaders[static_cast<uint>(ShaderMode::Geometry)] = getOrDefault(header, "shader_geometry");
    m_shaders[static_cast<uint>(ShaderMode::Depth)] = getOrDefault(header, "shader_depth");

    // Load textures
    for (auto& var : header)
    {
        // Process vars
        const string& variable = var.first;
        const string& value = var.second;
        if (variable.size() < 4 || variable[1] != 's' || variable[2] != '.')
            continue;

        // Read shader
        ShaderType shader = ShaderType::COUNT;
        switch (variable[0])
        {
        case 'v': shader = ShaderType::Vertex; break;
        case 'h': shader = ShaderType::TessControl; break;
        case 'd': shader = ShaderType::TessEval; break;
        case 'g': shader = ShaderType::Geometry; break;
        case 'p': shader = ShaderType::Pixel; break;
        default:
            LS_THROW(0, IoException,
                     "Unknown shader type required in [", variable, "]");
            break;
        }

        // Read slot
        const uint slot = fromString<uint>(variable.substr(3));

        LS_THROW(m_raw.count < MaxShaderResources,
                 IoException, "Too many textures");
        m_raw.slots[m_raw.count] = slot;
        m_raw.shaders[m_raw.count] = shader;
        m_raw.names[m_raw.count] = value;
        ++m_raw.count;
    }
}
void MaterialAsset::doSerializeConfig(Archive<Buffer> archive)
{
    serializeBinary<CullingMode>(archive, m_cullingMode);
    serializeBinary<RenderingMode>(archive, m_renderingMode);
    serializeBinary<bool>(archive, m_rawResources);
    serializeBinary<string>(archive, m_layout);
    serializeBinary<string>(archive, m_effect);
    for (string& str : m_shaders)
    {
        serializeBinary<string>(archive, str);
    }

    if (isRaw())
    {
        serializeBinary<uint>(archive, m_raw.count);
        LS_THROW(m_raw.count < MaxShaderResources,
                 IoException, "Too many textures");
        for (uint idx = 0; idx < m_raw.count; ++idx)
        {
            serializeBinary<uint>(archive, m_raw.slots[idx]);
            serializeBinary<string>(archive, m_raw.names[idx]);
            serializeBinary<ShaderType>(archive, m_raw.shaders[idx]);
        }
    }
}
void MaterialAsset::doRestore()
{
    if (renderer())
    {
        // Load effect
        m_layoutAsset = loadAsset<LayoutAsset>(m_layout);
        LS_THROW(!!m_layoutAsset,
                 IoException,
                 "Cannot load layout asset [", m_layout, "]");
        m_effectAsset = loadAsset<EffectAsset>(m_effect);
        LS_THROW(!!m_effectAsset,
                 IoException,
                 "Cannot load effect asset [", m_effect, "]");
        EffectHandle effect = m_effectAsset->gapiEffect();

        // Load programs
        for (uint idx = 0; idx < NumShaderModes; ++idx)
        {
            const string& name = m_shaders[idx];
            if (name.empty())
                continue;

            m_shaderPrograms[idx] = renderer()->handleProgram(effect, name.c_str());
            LS_THROW(!!m_shaderPrograms[idx],
                     IoException,
                     "Cannot load program [", m_effect, "]:[", name, "]");
        }
    }

    // Restore textures
    debug_assert(m_resources.numResources == 0);
    for (uint idx = 0; idx < m_raw.count; ++idx)
    {
        // Try to load
        m_raw.textures[idx] = loadAsset<TextureAsset>(m_raw.names[idx]);
        LS_THROW(!!m_raw.textures[idx],
                 IoException, "Cannot load texture asset [", m_raw.names[idx], "]");

        // Init GAPI stuff
        if (renderer())
        {
            debug_assert(m_raw.textures[idx]->isGapiReady());
            m_resources.addResource(m_raw.slots[idx],
                                    m_raw.textures[idx]->gapiResource(),
                                    m_raw.shaders[idx]);
        }
    }

    m_gapiReady = !!renderer();

    if (materialCache())
    {
        // Find internal handle in the cache
        m_cachedMaterial = materialCache()->findOrCreateMaterial(*this);
        LS_THROW(m_cachedMaterial,
                 IoException,
                 "Material should have at least one valid shader program");
    }
}


// GAPI
void MaterialAsset::dropRam()
{
    for (uint idx = 0; idx < m_raw.count; ++idx)
    {
        if (!m_raw.textures[idx])
            continue;

        m_raw.textures[idx]->dropRam();
    }
}
bool MaterialAsset::isGapiReady() const
{
    return m_gapiReady;
}
const ResourceBucket& MaterialAsset::gapiResources(const ShaderMode shaderMode) const
{
    debug_assert(isGapiReady());
    return m_resources;
}
EffectHandle MaterialAsset::gapiEffect()
{
    debug_assert(isGapiReady());
    return !!m_effectAsset ? m_effectAsset->gapiEffect() : nullptr;
}
ProgramHandle MaterialAsset::gapiProgram(const ShaderMode shaderMode)
{
    debug_assert(isGapiReady());
    return m_shaderPrograms[static_cast<uint>(shaderMode)];
}
StateHandle MaterialAsset::gapiState(MaterialCacheContext* context)
{
    return materialCache()->gapiState(context, m_cachedMaterial);
}
LayoutHandle MaterialAsset::gapiLayout(const ShaderMode shaderMode)
{
    debug_assert(isGapiReady());

    // Skip invalid
    if (!m_effectAsset || !m_layoutAsset)
    {
        return nullptr;
    }

    // Create if needed
    if (!m_layoutHandle)
    {
        ProgramHandle program = m_shaderPrograms[static_cast<uint>(shaderMode)];
        m_layoutHandle = m_effectAsset->gapiCachedLayout(program, *m_layoutAsset);
    }

    // Get
    return m_layoutHandle;
}
void MaterialAsset::attachUniformBuffer(const UniformBuffer type,
                                        BufferHandle buffer, const char* name)
{
    debug_assert(isGapiReady());
    if (m_uniformBuffers[static_cast<uint>(type)] != buffer && !!m_effectAsset)
    {
        m_uniformBuffers[static_cast<uint>(type)] = buffer;
        renderer()->attachUniformBuffer(m_effectAsset->gapiEffect(), buffer, name);
    }
}
void MaterialAsset::detachUniformBuffer(const UniformBuffer type,
                                        const char* name)
{
    debug_assert(isGapiReady());
    if (!!m_uniformBuffers[static_cast<uint>(type)] && !!m_effectAsset)
    {
        m_uniformBuffers[static_cast<uint>(type)] = nullptr;
        renderer()->detachUniformBuffer(m_effectAsset->gapiEffect(), name);
    }
}


// Gets
bool MaterialAsset::isRaw() const
{
    return m_rawResources;
}
MaterialAsset::CullingMode MaterialAsset::cullingMode() const
{
    return m_cullingMode;
}
PrimitiveType MaterialAsset::primitiveType() const
{
    return m_primitiveType;
}
shared_ptr<LayoutAsset> MaterialAsset::layout() const
{
    return m_layoutAsset;
}
shared_ptr<EffectAsset> MaterialAsset::effect() const
{
    return m_effectAsset;
}
