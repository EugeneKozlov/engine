/// @file ls/asset/AssetStorageInterface.h
/// @brief Asset Storage Interface
#pragma once

#include "ls/common.h"
#include "ls/io/FileSystemInterface.h"
#include "ls/xml/common.h"

namespace ls
{
    class AssetManagerOld;

    /// @addtogroup LsAsset
    /// @{

    /// @brief Asset Storage Interface
    class AssetStorageInterface
        : Noncopyable
    {
    public:
        /// @brief Ctor
        AssetStorageInterface(AssetManagerOld& manager);
        /// @brief Dtor
        virtual ~AssetStorageInterface()
        {
        }
        /// @brief Get asset
        virtual any get(const string& name) = 0;
    protected:
        /// @brief Manager
        AssetManagerOld& m_manager;
        /// @brief File system
        FileSystemInterface& m_fileSystem;
    };
    /// @}
}
