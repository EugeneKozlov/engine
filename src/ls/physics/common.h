/// @file ls/physics/common.h"
/// @brief Common physics stuff
#pragma once

#include "ls/common.h"
#include "ls/geom/Transform.h"

#pragma warning(push, 0)
#include <btBulletCollisionCommon.h>
#include <btBulletDynamicsCommon.h>

#include <BulletSoftBody/btSoftRigidDynamicsWorld.h>
#include <BulletCollision/CollisionShapes/btHeightfieldTerrainShape.h>
#pragma warning(pop)

namespace ls
{
    /// @addtogroup LsPhysics
    /// @{

    /// @brief Rigid body construction info
    using btRigidBodyCI = btRigidBody::btRigidBodyConstructionInfo;
    /// @brief Px->engine 3d vector
    inline float3 convert(const btVector3& vec)
    {
        return float3(vec.x(), vec.y(), vec.z());
    }
    /// @brief Engine->px 3d vector
    inline btVector3 convert(const float3& vec)
    {
        return btVector3(vec.x, vec.y, vec.z);
    }
    /// @brief Px->engine 3d quaternion
    inline quat convert(const btQuaternion& rot)
    {
        return quat(rot.x(), rot.y(), rot.z(), rot.w());
    }
    /// @brief Engine->px 3d quaternion
    inline btQuaternion convert(const quat& rot)
    {
        return btQuaternion(rot.x, rot.y, rot.z, rot.w);
    }
    /// @brief Px->engine 3x3 matrix
    inline float3x3 convert(const btMatrix3x3& vec)
    {
        float3x3 ret;
        for (uint i = 0; i < 3; ++i)
            for (uint j = 0; j < 3; ++j)
            {
                ret.element(i, j) = vec[i].m_floats[j];
            }
        return ret;
    }
    /// @brief Engine->px 3x3 matrix
    inline btMatrix3x3 convert(const float3x3& vec)
    {
        btMatrix3x3 ret;
        for (uint i = 0; i < 3; ++i)
        {
            btVector3& value = ret[i];
            for (uint j = 0; j < 3; ++j)
                value.m_floats[j] = vec.element(i, j);
        }
        return ret;
    }
    /// @}
}
