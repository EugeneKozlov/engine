/** @file ls/ik/BipedWalkAnalyzer.h
 *  @brief Biped walk analyzer
 */
#pragma once
#include "ls/common.h"
#include "ls/geom/Transform.h"
#include "ls/geom/AnimationRectifier.h"
#include "ls/ik/BipedWalkAnimation.h"

namespace ls
{
    /** @addtogroup LsIk
     *  @{
     */
    /** Walk analyzer
     */
    class BipedWalkAnalyzer
    : Noncopyable
    {
    public:
        /// @brief Analyze parameters
        struct Parameters
        {
            /// @brief Invalid ctor
            Parameters() = default;
            /// @brief Simple ctor
            Parameters(double animBegin, double animEnd,
                       float3 moveDirection, double recoverySegmentLength,
                       double weldTime, double basePhase)
                : animBegin(animBegin)
                , animEnd(animEnd)
                , moveDirection(moveDirection)
                , recoverySegmentLength(recoverySegmentLength)

                , clever(false)
                , weldTime(weldTime)
                , basePhase(basePhase)

                , movementThreshold(0)
                , weldFootstep(0)
            {
            }
            /// @brief Clever ctor
            Parameters(double animBegin, double animEnd,
                       float3 moveDirection, double recoverySegmentLength,
                       float movementThreshold, float weldFootstep)
                : animBegin(animBegin)
                , animEnd(animEnd)
                , moveDirection(moveDirection)
                , recoverySegmentLength(recoverySegmentLength)

                , clever(true)
                , weldTime(-1)
                , basePhase(-1)

                , movementThreshold(movementThreshold)
                , weldFootstep(weldFootstep)
            {
            }

            /// @brief Cut animation begin time
            double animBegin;
            /// @brief Cut animation end time
            double animEnd;
            /// @brief Move direction : {0,0,1} is look direction, {0,1,0} is up direction
            float3 moveDirection;
            /// @brief Duration of direct segment on restoring (in seconds)
            double recoverySegmentLength;

            /// @brief Set to use clever extraction
            bool clever;

            /** @name Simple
             *  @{
             */
            /// @brief Weld animation percent
            double weldTime;
            /// @brief Base phase offset
            double basePhase;
            /// @}

            /** @name Clever
             *  @{
             */
            /// @brief Stand/move movement threshold
            float movementThreshold;
            /// @brief Weld footstep with almost same positions (distance)
            float weldFootstep;
            /// @}
        };
        /// @brief Footstep info
        struct Footstep
        {
            /// @brief Ctor
            Footstep() = default;
            /// @brief Ctor
            Footstep(const float3& toePosition)
                : toePosition(toePosition)
            {
            }
            /// @brief Is @a time inside
            bool isInside(double time) const
            {
                return time >= firstTime && time <= lastTime;
            }
            /// @brief Toe position
            float3 toePosition;
            /// @brief First frame of footstep
            uint firstFrame = 0;
            /// @brief Last frame of footstep
            uint lastFrame = 0;
            /// @brief First time of footstep
            double firstTime = 0;
            /// @brief Last time of footstep
            double lastTime = 0;
        };
        /// @brief Foot track
        struct Foot
        {
            /// @brief Ctor
            Foot() = default;
            /// @brief Find footstep by @a time
            const Footstep* findFootstep(double time) const;
            /// @brief Fing footsteps by @a time
            double findFootsteps(double time, Footstep& left, Footstep& right)
            {
                // Too left
                if (time < footsteps[0].firstTime || footsteps[0].isInside(time))
                {
                    left = right = footsteps[0];
                    return 0;
                }
                for (uint iterFootstep = 1; iterFootstep < footsteps.size(); ++iterFootstep)
                {
                    if (time < footsteps[iterFootstep].firstFrame)
                    {
                        left = footsteps[iterFootstep - 1];
                        right = footsteps[iterFootstep];
                        return (time - left.lastTime) / (right.firstTime - left.lastTime);
                    }
                    if (iterFootstep + 1 == footsteps.size() || footsteps[iterFootstep].isInside(time))
                    {
                        left = right = footsteps[iterFootstep];
                        return 0;
                    }
                }
                debug_assert(0);
                return -1;
            }
            /// @brief Get position
            float3 lerpPosition(double time)
            {
                Footstep left, right;
                double factor = findFootsteps(time, left, right);
                return lerp(left.toePosition, right.toePosition, static_cast<float>(factor));
            }
            /// @brief Footsteps
            vector<Footstep> footsteps;
        };
    public:
        /** Ctor.
         *  @param skeleton
         *    Biped skeleton
         *  @param animation
         *    Biped animation
         *  @param params
         *    Animation parameters
         */
        BipedWalkAnalyzer(shared_ptr<Skeleton> skeleton,
                          const NodeAnimation& animation,
                          const Parameters& params);
        /// @brief Dtor
        ~BipedWalkAnalyzer();
        /// @brief Draw source animation
        void drawSourceAnimation(Plotter& plotter, float factor, bool smooth);
        /// @brief Draw restored animation
        void drawRestoredAnimation(Plotter& plotter, float factor, bool smooth);
        /// @brief Draw centered animation
        void drawCenteredAnimation(Plotter& plotter, float factor, bool smooth);
        /// @brief Draw periodical animation
        void drawPeriodicalAnimation(Plotter& plotter, float factor, bool smooth);

        /** @name Gets
         *  @{
         */
        /// @brief Get result animation
        shared_ptr<BipedWalkAnimation> result() const noexcept
        {
            return m_animation;
        }
        /// @brief Get periodical animation
        shared_ptr<NodeAnimation> periodicalAnimation() const noexcept
        {
            return m_periodicalAnimation;
        }
        /// @brief Get foot by @a index
        Foot& getFoot(uint index);
        /// @brief Get average root speed
        double getAverageRootSpeed() const;
        /// @}
    private:
        /// @brief Prepare clever
        void prepareClever();
        /// @brief Prepare footsteps
        void prepareFootsteps();
        /// @brief Prepare simple
        void prepareSimple();

        /// @brief Analyze pelvis
        void analyzePelvis();
        /// @brief Analyze simple walk
        void analyzeSimpleWalk();
        /// @brief Analyze clever walk
        void analyzeCleverWalk();
        /// @brief Analyze foot walk pose
        BipedWalkAnimation::FootPose
        analyzeFootWalkPose(const float3& root,
                            const float3& thigh, const float3& calf,
                            const float3& foot, const float3& toe) const;
        /// @brief Analyze spine
        void analyzeSpine();
        /// @brief Analyze hand animation
        void analyzeHands();

        /// @brief Finalize and create animation
        void finalize();

        /// @brief Draw animation
        double drawAnimation(Plotter& plotter,
                             const float3& boneColor,
                             const NodeAnimation& animation,
                             float factor, bool smooth);
    public:
        /// @brief Analyze parameters
        const Parameters p;
        /// @brief Default forward vector
        const float3 DefaultForward = float3{0, 0, 1};

        /// @brief Debug draw tag color
        float3 tagColor = float3{1.0f, 0.0f, 0.0f};
        /// @brief Debug draw root size
        float rootSize = 0.05f;
        /// @brief Debug draw bone size
        float boneSize = 0.01f;
    private:
        /// @brief Skeleton holder
        shared_ptr<Skeleton> m_skeletonHolder;
        /// @brief Skeleton
        BipedSkeleton& m_skeleton;
        /// @brief Hierarchy
        NodeHierarchy& m_hierarchy;
        /// @brief Source animation
        shared_ptr<NodeAnimation> m_sourceAnimation;
        /// @brief Helper
        AnimationRectifier m_helper;
        /// @brief Restored animation
        shared_ptr<NodeAnimation> m_restoredAnimation;

        /// @brief Foots tracks
        vector<Foot> m_foots;

        /// @brief Periodical animation
        shared_ptr<NodeAnimation> m_periodicalAnimation;

        /// @brief Period duration
        double m_period = 0;
        /// @brief Unit period
        double m_unitPeriod = 0;
        /// @brief Base phase offset
        double m_basePhase = 0;

        /// @brief Pelvis animation
        BipedWalkAnimation::Pelvis m_pelvisAnimation;
        /// @brief Foot animation
        array<BipedWalkAnimation::Foot, BipedSkeleton::NumFeet> m_feetAnimation;
        /// @brief Spine animation
        shared_ptr<NodeAnimation> m_spineAnimation;
        /// @brief Hands animation
        shared_ptr<NodeAnimation> m_handsAnimation;

        /// @brief Animation
        shared_ptr<BipedWalkAnimation> m_animation;
    };
    /// @}
}

