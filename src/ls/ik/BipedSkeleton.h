/** @file ls/ik/BipedSkeleton.h
 *  @brief Biped IK controller
 */
#pragma once
#include "ls/common.h"
#include "ls/ik/Skeleton.h"

namespace ls
{
    /** @addtogroup LsIk
     *  @{
     */
    /** Biped skeleton
     *
     *  -# @b Hierarchy
     *    - Biped must be Z-positive oriented
     *    - Biped foots must be placed in idle position
     *
     *  -# @b Foot
     *
     *    - Structure
     *      + @b Thigh is attached to pelvis (intermediary)
     *      + @b Calf is attached to thigh and describes knee joint
     *      + @b Foot is attached to calf and describes first foot control point
     *      + @b Toe is attached to foot and describes second foot control point
     *
     *  -# @b Hand
     *
     */
    class BipedSkeleton
    : public Skeleton
    {
    public:
        /// @brief Foot names
        enum FootIndex
        {
            LeftFoot,
            RightFoot,
            NumFeet
        };
        /// @brief Foot description
        class Foot
        {
        public:
            /// @brief Ctor
            Foot() = default;
            /// @brief Ctor
            Foot(NodeHierarchy& biped, FootIndex index,
                 const string& thighName,
                 const string& calfName,
                 const string& footName,
                 const string& toeName);
        public:
            /// @brief Index
            FootIndex index = NumFeet;
            /// @brief Biped
            NodeHierarchy* biped = nullptr;
            /// @brief Thigh node
            GeometryNode* thigh = nullptr;
            /// @brief Calf node
            GeometryNode* calf = nullptr;
            /// @brief Foot node
            GeometryNode* foot = nullptr;
            /// @brief Toe node
            GeometryNode* toe = nullptr;
            /// @brief Thigh length
            float thighLength = 0;
            /// @brief Calf length
            float calfLength = 0;
            /// @brief Foot length
            float footLength = 0;
        };
        /// @brief Hand names
        enum HandIndex
        {
            LeftHand,
            RightHand,
            NumHands
        };
        /// @brief Hand description
        class Hand
        {
        public:
            /// @brief Ctor
            Hand() = default;
            /// @brief Ctor
            Hand(NodeHierarchy& biped,
                 const string& upperarmName,
                 const string& forearmName,
                 const string& clavicleName = "");
        public:
            /// @brief Biped
            NodeHierarchy* biped = nullptr;
            /// @brief Thigh node
            GeometryNode* clavicle = nullptr;
            /// @brief Calf node
            GeometryNode* upperarm = nullptr;
            /// @brief Foot node
            GeometryNode* forearm = nullptr;
        };
        /// @brief Max number of spine nodes
        static const uint NumSpine = 32;
    public:
        /// @brief Ctor
        BipedSkeleton(unique_ptr<NodeHierarchy>&& hierarchy);
        /// @brief Dtor
        ~BipedSkeleton();
        /// @brief Reset by start poses
        void resetStart();

        /** @name Gets
         *  @{
         */
        /// @brief Get pelvis
        GeometryNode& pelvis() noexcept
        {
            return *m_pelvis;
        }
        /// @brief Get foot
        Foot& foot(FootIndex footIndex) noexcept
        {
            return m_feet[footIndex];
        }
        /// @brief Get hand
        Hand& hand(HandIndex handIndex) noexcept
        {
            return m_hands[handIndex];
        }
        /// @brief Get spine
        vector<GeometryNode*>& spine() noexcept
        {
            return m_spine;
        }
        /// @brief Get spine bones
        const vector<uint>& spineBones() const noexcept
        {
            return m_spineBones;
        }
        /// @brief Get feet
        array<Foot, NumFeet>& feet() noexcept
        {
            return m_feet;
        }
        /// @brief Get hands
        array<Hand, NumHands>& hands() noexcept
        {
            return m_hands;
        }
        /// @brief Get hands bones
        const vector<uint>& handBones() const noexcept
        {
            return m_handsBones;
        }
        /// @brief Test if hands have clavicles
        bool hasClavicles() const noexcept
        {
            return !!m_hands[LeftHand].clavicle;
        }
        /// @}
    private:
        /// @brief Get spine name
        string spineName(uint index);
    private:
        /// @brief Pelvis
        GeometryNode* m_pelvis = nullptr;
        /// @brief Spine
        vector<GeometryNode*> m_spine;
        /// @brief Spine nodes
        vector<uint> m_spineBones;
        /// @brief Feet
        array<Foot, NumFeet> m_feet;
        /// @brief Hands
        array<Hand, NumHands> m_hands;
        /// @brief Hands nodes
        vector<uint> m_handsBones;
    };
    /// @}
}

