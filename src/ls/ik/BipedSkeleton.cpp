#include "pch.h"
#include "ls/ik/BipedSkeleton.h"
#include "ls/ik/BipedWalkAnimation.h"

using namespace ls;
BipedSkeleton::Foot::Foot(NodeHierarchy& biped, FootIndex index,
                          const string& thighName,
                          const string& calfName,
                          const string& footName, const string& toeName)
    : index(index)
    , biped(&biped)
{
    // Load bones
    thigh = biped.findNode(thighName.c_str());
    LS_THROW(thigh, IoException, "Invalid [", thighName, "] node");
    calf = thigh->findChild(calfName.c_str());
    LS_THROW(calf, IoException, "Invalid [", calfName, "] node");
    foot = calf->findChild(footName.c_str());
    LS_THROW(foot, IoException, "Invalid [", footName, "] node");
    toe = foot->findChild(toeName.c_str());
    LS_THROW(toe, IoException, "Invalid [", toeName, "] node");

    // Init length
    thighLength = distance(thigh->global.position(), calf->global.position());
    calfLength = distance(calf->global.position(), foot->global.position());
    footLength = distance(foot->global.position(), toe->global.position());
}
BipedSkeleton::Hand::Hand(NodeHierarchy& biped,
                          const string& upperarmName,
                          const string& forearmName,
                          const string& clavicleName)
    : biped(&biped)
{
    clavicle = biped.findNode(clavicleName.c_str());
    if (clavicle)
    {
        upperarm = clavicle->findChild(upperarmName.c_str());
        LS_THROW(upperarm, IoException, "Invalid [", upperarmName, "] node");
        forearm = upperarm->findChild(forearmName.c_str());
        LS_THROW(forearm, IoException, "Invalid [", forearmName, "] node");
    }
    else
    {
        upperarm = biped.findNode(upperarmName.c_str());
        LS_THROW(upperarm, IoException, "Invalid [", upperarmName, "] node");
        forearm = upperarm->findChild(forearmName.c_str());
        LS_THROW(forearm, IoException, "Invalid [", forearmName, "] node");
    }
}
string BipedSkeleton::spineName(uint index)
{
    return "spine_" + toString(index);
}
BipedSkeleton::BipedSkeleton(unique_ptr<NodeHierarchy>&& hierarchy)
    : Skeleton(std::move(hierarchy), Skeleton::Biped)
{
    // Get root
    m_pelvis = m_hierarchy->findNode("root");
    LS_THROW(m_pelvis && m_pelvis->isRoot(), IoException, "Incorrect [root] node");
    m_spineBones.push_back(m_pelvis->index);

    // Get feet
    m_feet[LeftFoot] = Foot{*m_hierarchy, LeftFoot,
        "left_foot_thigh", "left_foot_calf", "left_foot_foot", "left_foot_toe"};
    m_feet[RightFoot] = Foot{*m_hierarchy, RightFoot,
        "right_foot_thigh", "right_foot_calf", "right_foot_foot", "right_foot_toe"};

    // Get spine
    uint segmentIndex = 0;
    GeometryNode* spineNode = nullptr;
    while ((spineNode = m_hierarchy->findNode(spineName(segmentIndex).c_str())))
    {
        m_spine.push_back(spineNode);
        m_spineBones.push_back(spineNode->index);
        ++segmentIndex;
    }
    // Get neck
    auto neckNode = m_hierarchy->findNode("neck");
    if (neckNode)
    {
        m_spine.push_back(neckNode);
        m_spineBones.push_back(neckNode->index);
        ++segmentIndex;
    }
    LS_THROW(segmentIndex < NumSpine, IoException, "Too many spine nodes");

    // Get hands
    m_hands[LeftHand] = Hand{*m_hierarchy,
        "left_upperarm", "left_forearm", "left_clavicle"};
    m_hands[RightHand] = Hand{*m_hierarchy,
        "right_upperarm", "right_forearm", "right_clavicle"};
    // Test clavicles
    bool hasClavicle = !!m_hands[LeftHand].clavicle;
    for (uint handIndex = RightHand; handIndex < NumHands; ++handIndex)
    {
        auto& hand = m_hands[handIndex];
        LS_THROW(!!hand.clavicle == hasClavicle, IoException,
                 "Invalid hand [", handIndex, "] skeleton : HasClavicle = ", hasClavicle);
    }

    // Update hands bones
    for (uint handIndex = LeftHand; handIndex < NumHands; ++handIndex)
    {
        auto& hand = m_hands[handIndex];
        if (hasClavicle)
            m_handsBones.push_back(hand.clavicle->index);
        m_handsBones.push_back(hand.upperarm->index);
        m_handsBones.push_back(hand.forearm->index);
    }
}
BipedSkeleton::~BipedSkeleton()
{
}
void BipedSkeleton::resetStart()
{
    m_hierarchy->reset();
}
