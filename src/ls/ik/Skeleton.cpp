#include "pch.h"
#include "ls/ik/Skeleton.h"

using namespace ls;
Skeleton::Skeleton(unique_ptr<NodeHierarchy>&& hierarchy, Type type)
    : m_type(type)
    , m_hierarchy(std::move(hierarchy))
{
}
Skeleton::~Skeleton()
{
}
void Skeleton::rotateNode(GeometryNode& node, GeometryNode& child,
                          const float3& newPosition)
{
    float3 oldNode = node.global.position();
    float3 oldChild = child.global.position();

    quat rotation(normalize(oldChild - oldNode),
                  normalize(newPosition - oldNode));
    float4x4 transform
        = matrix::translate(-oldNode)
        * matrix::rotationQuaternion4(rotation)
        * matrix::translate(oldNode);

    node.local = node.global * transform * inverse(node.parent->global.matrix());
    node.updateGlobal();

    if (!node.local.isFinite())
        logWarning("Skeleton: Infinite pose");
}




#include "ls/ik/BipedSkeleton.h"
Skeleton::Type Skeleton::type(const string& name)
{
    if (name == "unspecified")
        return Unspecified;
    else if (name == "biped")
        return Biped;
    else
        LS_THROW(0, IoException, "Invalid skeleton type : ", name);

    return Unspecified;
}
shared_ptr<Skeleton> Skeleton::construct(unique_ptr<NodeHierarchy>&& hierarchy, Type type)
{
    switch (type)
    {
    case Unspecified:
        return make_shared<Skeleton>(std::move(hierarchy));
    case Biped:
        return make_shared<BipedSkeleton>(std::move(hierarchy));
    default:
        return nullptr;
    }
}
BipedSkeleton* Skeleton::isBiped() noexcept
{
    return dynamic_cast<BipedSkeleton*> (this);
}
const BipedSkeleton* Skeleton::isBiped() const noexcept
{
    return dynamic_cast<const BipedSkeleton*> (this);
}
