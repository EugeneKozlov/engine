#include "pch.h"
#include "ls/ik/BipedWalkAnimation.h"
#include "ls/geom/NodeAnimation.h"
#include "ls/geom/math.h"

using namespace ls;
BipedWalkAnimation::FootPose::Positions
BipedWalkAnimation::FootPose::compute(const float3& basePosition,
                                      const float3& thighPosition,
                                      const float3& forwardVec,
                                      float thighLength,
                                      float calfLength,
                                      float footLength) const
{
    using namespace math3;

    // Axises
    auto yAxis = float3{0, 1, 0};
    auto zAxis = forwardVec;
    auto xAxis = cross(yAxis, zAxis);

    // Toe
    auto toePos = toePosition.x * xAxis + toePosition.y * yAxis + toePosition.z * zAxis;
    toePos += basePosition;

    // Foot
    auto footDirection = toe2foot.x * xAxis + toe2foot.y * yAxis + toe2foot.z * zAxis;
    auto footPos = toePos + footDirection;

    // Fix foot and toe
    auto minDistance = 0.0f;
    auto maxDistance = thighLength + calfLength;
    auto thigh2foot = segment{thighPosition, footPos};
    auto len = clamp(thigh2foot.length(), minDistance, maxDistance);
    footPos = thighPosition + thigh2foot.direction() * len;
    toePos = footPos - footDirection;

    // Compute knee
    auto kneePlaceRet = intersectSphereSphere(sphere{footPos, calfLength},
                                              sphere{thighPosition, thighLength});
    debug_assert(kneePlaceRet);

    auto kneePlace = *kneePlaceRet;
    auto rightDir = normalize(cross(forwardVec, thigh2foot.direction()));
    auto forwardDir = normalize(cross(thigh2foot.direction(), rightDir));
    auto kneeFlatPos = float2(sinf(this->kneeRotation), cosf(this->kneeRotation)) * kneePlace.r;
    auto kneePos = kneeFlatPos.x * rightDir + kneeFlatPos.y * forwardDir + kneePlace.p;

    // Return
    Positions pose;
    pose.thigh = thighPosition;
    pose.calf = kneePos;
    pose.foot = footPos;
    pose.toe = toePos;
    return pose;
}
BipedWalkAnimation::BipedWalkAnimation(shared_ptr<Skeleton> skeleton,
                                       float period, float basePhase,
                                       const Pelvis& rootAnimation,
                                       const Foot& leftFoot,
                                       const Foot& rightFoot,
                                       const shared_ptr<NodeAnimation> spineAnimation,
                                       shared_ptr<NodeAnimation> handsAnimation)

    : m_skeletonHolder(skeleton)
    , m_biped(*m_skeletonHolder->isBiped())
    , m_hierarchy(m_biped.hierarchy())

    , m_unitPeriod(period)
    , m_basePhase(basePhase)
    , m_pelvisAnimation(rootAnimation)
    , m_feetAnimation()
    , m_spineAnimation(spineAnimation)
    , m_handsAnimation(handsAnimation)
{
    m_feetAnimation[BipedSkeleton::LeftFoot] = leftFoot;
    m_feetAnimation[BipedSkeleton::RightFoot] = rightFoot;

    // Test skeleton
    LS_THROW(m_spineAnimation->numNodes() == m_biped.spineBones().size(),
             IoException,
             "Number of spine bones mismatch "
             "[", m_spineAnimation->numNodes(), "] "
             "vs [", m_biped.spineBones().size(), "]");
    LS_THROW(m_handsAnimation->numNodes() == m_biped.handBones().size(),
             IoException,
             "Number of hands bones mismatch "
             "[", m_handsAnimation->numNodes(), "] "
             "vs [", m_biped.handBones().size(), "]");
}
BipedWalkAnimation::~BipedWalkAnimation()
{
}
void BipedWalkAnimation::applyRoot(ComposedNodeArray& pose,
                                   const AnimationEnvironment& info,
                                   float factor, bool needUpdate)
{
    // Fixed factor
    float fixedFactor = fract(factor + m_basePhase);

    // Reset
    pose = m_hierarchy.getStartPose();

    // Pelvis
    auto pelvisIndex = m_biped.pelvis().index;
    float4x4& pelvisMatrix = pose[pelvisIndex].local;

    // Rotate
    pelvisMatrix.row<3>(3, {0, 0, 0});
    static const float3 defaultForward = {0, 0, 1};
    pelvisMatrix *= matrix::rotationQuaternion4(quat(defaultForward, info.look));

    // Move
    float3 pelvisPosition = info.position + m_pelvisAnimation.lerp(fixedFactor);
    pelvisMatrix.row<3>(3, pelvisPosition);

    // Apply
    if (needUpdate)
        m_hierarchy.update(pose);
}
void BipedWalkAnimation::applySpine(ComposedNodeArray& pose,
                                    const AnimationEnvironment& info, float factor, bool needUpdate)
{
    auto pelvisIndex = m_biped.pelvis().index;
    float3 pelvisPosition = pose[pelvisIndex].local.row<3>(3);

    // Fixed factor
    float fixedFactor = fract(factor + m_basePhase);

    // Animate
    m_spineAnimation->lerpPoses(fixedFactor, pose, m_biped.spineBones());

    // Fix pelvis
    static const float3 defaultForward = {0, 0, 1};
    float4x4& pelvisMatrix = pose[pelvisIndex].local;
    pelvisMatrix *= matrix::rotationQuaternion4(quat(defaultForward, info.look));
    pelvisMatrix.row<3>(3, pelvisPosition);

    // Apply
    if (needUpdate)
        m_hierarchy.update(pose);
}
void BipedWalkAnimation::applyFoot(ComposedNodeArray& pose,
                                   const AnimationEnvironment& info, BipedSkeleton::FootIndex foot, float factor)
{
    // Fixed factor
    float fixedFactor = fract(factor + m_basePhase);

    // Init
    auto& footController = m_biped.foot(foot);
    auto& footAnimation = m_feetAnimation[foot];

    // Indices
    auto baseIndex = footController.thigh->parent->index;
    auto thighIndex = footController.thigh->index;
    auto calfIndex = footController.calf->index;
    auto footIndex = footController.foot->index;
    auto toeIndex = footController.toe->index;

    // Get pose
    auto thighPosition = pose[thighIndex].globalPosition();
    auto animationState = footAnimation.lerp(fixedFactor);
    auto footPose = animationState.compute(info.position, thighPosition, info.look,
                                           footController.thighLength, footController.calfLength, footController.footLength);

    // Apply pose
    matchNode(pose, baseIndex, thighIndex, calfIndex, footPose.calf);
    m_hierarchy.update(pose, thighIndex);
    matchNode(pose, thighIndex, calfIndex, footIndex, footPose.foot);
    m_hierarchy.update(pose, calfIndex);
    matchNode(pose, calfIndex, footIndex, toeIndex, footPose.toe);
    m_hierarchy.update(pose, footIndex);
}
void BipedWalkAnimation::applyHands(ComposedNodeArray& pose,
                                    float factor)
{
    // Fixed factor
    float fixedFactor = fract(factor + m_basePhase);

    // Animate
    auto& handBones = m_biped.handBones();
    m_handsAnimation->lerpPoses(fixedFactor, pose, handBones);

    // Update
    for (uint handIndex = BipedSkeleton::LeftHand;
        handIndex < BipedSkeleton::NumHands;
        ++handIndex)
    {
        auto& hand = m_biped.hand((BipedSkeleton::HandIndex) handIndex);
        uint firstIndex = hand.clavicle
            ? hand.clavicle->index
            : hand.upperarm->index;
        m_hierarchy.update(pose, firstIndex);
    }
}
void BipedWalkAnimation::apply(ComposedNodeArray& pose,
                               const AnimationEnvironment& info, float factor)
{
    applyRoot(pose, info, factor, false);
    applySpine(pose, info, factor, true);
    applyFoot(pose, info, BipedSkeleton::LeftFoot, factor);
    applyFoot(pose, info, BipedSkeleton::RightFoot, factor);
    applyHands(pose, factor);
}
