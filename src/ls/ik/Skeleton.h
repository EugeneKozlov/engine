/** @file ls/ik/Skeleton.h
 *  @brief Skeleton of creature
 */
#pragma once
#include "ls/common.h"
#include "ls/core/Nameable.h"
#include "ls/geom/NodeHierarchy.h"

namespace ls
{
    class BipedSkeleton;

    /** @addtogroup LsIk
     *  @{
     */
    /** Skeleton of creature
     */
    class Skeleton
    : public Named
    {
    public:
        /// @brief Skeleton traits
        enum Type
        {
            /// @brief Unspecified
            Unspecified = 0,
            /// @brief Biped flag
            Biped
        };
        /// @brief Get type by name
        static Type type(const string& name);
        /// @brief Construct custom skeleton
        static shared_ptr<Skeleton> construct(unique_ptr<NodeHierarchy>&& hierarchy,
                                              Type type);
        /// @brief Rotate @a node trying to move @a child into @a newPosition
        static void rotateNode(GeometryNode& node,
                               GeometryNode& child,
                               const float3& newPosition);
    public:
        /// @brief Ctor
        Skeleton(unique_ptr<NodeHierarchy>&& hierarchy, Type type = Unspecified);
        /// @brief Dtor
        virtual ~Skeleton();

        /** @name Gets
         *  @{
         */
        /// @brief Get hierarchy
        NodeHierarchy& hierarchy() noexcept
        {
            return *m_hierarchy;
        }
        /// @brief Get const hierarchy
        const NodeHierarchy& hierarchy() const noexcept
        {
            return *m_hierarchy;
        }
        /// @brief Get biped
        BipedSkeleton* isBiped() noexcept;
        /// @brief Get const biped
        const BipedSkeleton* isBiped() const noexcept;
        /// @}
    protected:
        /// @brief Type
        Type m_type;
        /// @brief Hierarchy
        unique_ptr<NodeHierarchy> m_hierarchy;
    };
    /// @}
}

