#include "pch.h"
#include "ls/ik/BipedWalkAnalyzer.h"
#include "ls/ik/BipedSkeleton.h"
#include "ls/geom/NodeAnimation.h"
#include "ls/geom/GeometryNode.h"
#include "ls/geom/NodeHierarchy.h"
#include "ls/geom/math.h"
#include "ls/render/Plotter.h"

using namespace ls;
BipedWalkAnalyzer::BipedWalkAnalyzer(shared_ptr<Skeleton> skeleton,
                                     const NodeAnimation& animation,
                                     const Parameters& params)
    : p(params)
    , m_skeletonHolder(skeleton)
    , m_skeleton(*skeleton->isBiped())
    , m_hierarchy(skeleton->hierarchy())
    , m_sourceAnimation(make_shared<NodeAnimation>(animation))
    , m_helper(m_hierarchy, animation, m_skeleton.pelvis().index,
               p.animBegin, p.animEnd)
{
    m_helper.rectify(p.recoverySegmentLength, p.moveDirection);
    m_helper.center();
    m_restoredAnimation = m_helper.restoredAnimation();
    if (p.clever)
    {
        prepareClever();
        analyzePelvis();
        analyzeCleverWalk();
        analyzeSpine();
        analyzeHands();
    }
    else
    {
        prepareSimple();
        analyzePelvis();
        analyzeSimpleWalk();
        analyzeSpine();
        analyzeHands();
    }
    finalize();
}
BipedWalkAnalyzer::~BipedWalkAnalyzer()
{
}


// Prepare
void BipedWalkAnalyzer::prepareClever()
{
    // Find footsteps
    prepareFootsteps();

    // Compute period
    auto& leftFoot = m_foots[BipedSkeleton::LeftFoot];
    auto& rightFoot = m_foots[BipedSkeleton::RightFoot];

    double leftPeriod = leftFoot.footsteps[1].lastTime - leftFoot.footsteps[0].lastTime;
    double rightPeriod = rightFoot.footsteps[1].lastTime - rightFoot.footsteps[0].lastTime;

    m_period = (leftPeriod + rightPeriod) / 2;
    m_unitPeriod = m_helper.averageSpeed() * m_period;

    // Compute base phase offset
    double zeroPhase = leftFoot.footsteps[0].lastTime;
    double basePhase = (leftFoot.footsteps[0].lastTime + leftFoot.footsteps[1].firstTime) / 2;
    m_basePhase = (basePhase - zeroPhase) / m_period;

    // Clear animation
    auto traceFrom = leftFoot.footsteps[0].lastFrame;
    auto weldTo = leftFoot.footsteps[1].firstFrame;
    auto weldFrom = leftFoot.footsteps[1].lastFrame;
    auto traceTo = leftFoot.footsteps[2].firstFrame;

    // Compute
    m_helper.periodizeSoft(traceFrom, weldTo, weldFrom, traceTo);
    m_periodicalAnimation = m_helper.periodicalAnimation();
}
void BipedWalkAnalyzer::prepareFootsteps()
{
    vector<Transform> poseTrace;
    auto getMovement = [&poseTrace](uint from, uint to)
    {
        return length(poseTrace[from].position() - poseTrace[to].position());
    };

    // For each foot
    for (auto& foot : m_skeleton.feet())
    {
        m_foots.emplace_back();
        auto& track = m_foots.back();

        // Trace
        poseTrace.clear();
        m_restoredAnimation->traceGlobalPoses(foot.toe->index, poseTrace);
        uint numPoints = (uint) poseTrace.size();
        for (uint iterPoint = 1; iterPoint < numPoints; ++iterPoint)
        {
            if (getMovement(iterPoint - 1, iterPoint) < p.movementThreshold)
            {
                // Init footstep
                auto position = poseTrace[iterPoint - 1].position();
                Footstep footstep{position};
                footstep.firstFrame = iterPoint - 1;
                footstep.firstTime = footstep.firstFrame * m_restoredAnimation->step();

                // Wait until end
                while (iterPoint + 1 < numPoints && getMovement(iterPoint, iterPoint + 1) < p.movementThreshold)
                    ++iterPoint;

                footstep.lastFrame = min(numPoints, iterPoint);
                footstep.lastTime = footstep.lastFrame * m_restoredAnimation->step();

                track.footsteps.push_back(footstep);
            }
        }

        // Weld
        auto& footsteps = track.footsteps;
        uint footstepIndex = 1;
        while (footstepIndex < footsteps.size())
        {
            auto& prev = footsteps[footstepIndex - 1];
            auto& next = footsteps[footstepIndex];
            float diff = length(prev.toePosition - next.toePosition);
            if (diff < p.weldFootstep)
            {
                prev.lastFrame = next.lastFrame;
                prev.lastTime = next.lastTime;
                footsteps.erase(footsteps.begin() + footstepIndex);
            }
            else
                ++footstepIndex;
        }

        // Check
        LS_THROW(footsteps.size() >= 3, IoException, "Too few footsteps were found");
    }
}
void BipedWalkAnalyzer::prepareSimple()
{
    // Compute constants
    m_period = m_helper.centeredAnimation()->duration();
    m_unitPeriod = m_helper.averageSpeed() * m_period;
    m_basePhase = p.basePhase;

    // Clear
    m_helper.periodizeRaw(0, m_helper.centeredAnimation()->duration(), p.weldTime);
    m_periodicalAnimation = m_helper.periodicalAnimation();
}


// Analyze
void BipedWalkAnalyzer::analyzePelvis()
{
    uint pelvisIndex = m_skeleton.pelvis().index;
    vector<Transform> poseTrace;
    m_periodicalAnimation->traceGlobalPoses(pelvisIndex, poseTrace);
    for (auto& pose : poseTrace)
        m_pelvisAnimation.track.push_back(pose.position());
}
void BipedWalkAnalyzer::analyzeSimpleWalk()
{
    for (uint footIndex = BipedSkeleton::LeftFoot; footIndex < BipedSkeleton::NumFeet; ++footIndex)
    {
        auto& footAnimation = m_feetAnimation[footIndex];
        auto& footController = m_skeleton.foot((BipedSkeleton::FootIndex) footIndex);

        // Get foot movement
        vector<Transform> thighTrack;
        vector<Transform> calfTrack;
        vector<Transform> footTrack;
        vector<Transform> toeTrack;

        m_periodicalAnimation->traceGlobalPoses(footController.thigh->index, thighTrack);
        m_periodicalAnimation->traceGlobalPoses(footController.calf->index, calfTrack);
        m_periodicalAnimation->traceGlobalPoses(footController.foot->index, footTrack);
        m_periodicalAnimation->traceGlobalPoses(footController.toe->index, toeTrack);

        // Process foot movement
        auto& track = footAnimation.track;
        uint numKeys = m_periodicalAnimation->numKeys();
        for (uint iterKey = 0; iterKey < numKeys; ++iterKey)
        {
            track.push_back(analyzeFootWalkPose(m_helper.linearMovement()[iterKey],
                                                thighTrack[iterKey].position(),
                                                calfTrack[iterKey].position(),
                                                footTrack[iterKey].position(),
                                                toeTrack[iterKey].position()));
        }

        // No phase
        footAnimation.phase = 0;
    }
}
void BipedWalkAnalyzer::analyzeCleverWalk()
{
    for (uint footIndex = BipedSkeleton::LeftFoot; footIndex < BipedSkeleton::NumFeet; ++footIndex)
    {
        auto& footAnimation = m_feetAnimation[footIndex];
        auto& footInfo = m_foots[footIndex];
        auto& footController = m_skeleton.foot((BipedSkeleton::FootIndex) footIndex);

        LS_THROW(footInfo.footsteps.size() >= 3, IoException, "Too few footsteps were found");

        // Get foot movement
        vector<Transform> thighTrack;
        vector<Transform> calfTrack;
        vector<Transform> footTrack;
        vector<Transform> toeTrack;

        m_restoredAnimation->traceGlobalPoses(footController.thigh->index, thighTrack);
        m_restoredAnimation->traceGlobalPoses(footController.calf->index, calfTrack);
        m_restoredAnimation->traceGlobalPoses(footController.foot->index, footTrack);
        m_restoredAnimation->traceGlobalPoses(footController.toe->index, toeTrack);

        auto traceFrom = footInfo.footsteps[0].lastFrame;
        auto weldTo = footInfo.footsteps[1].firstFrame;
        auto weldFrom = footInfo.footsteps[1].lastFrame;
        auto traceTo = footInfo.footsteps[2].firstFrame;

        // Process foot movement
        auto& track = footAnimation.track;
        for (uint iterKey = traceFrom; iterKey <= traceTo; ++iterKey)
        {
            track.push_back(analyzeFootWalkPose(m_helper.linearMovement()[iterKey],
                                                thighTrack[iterKey].position(),
                                                calfTrack[iterKey].position(),
                                                footTrack[iterKey].position(),
                                                toeTrack[iterKey].position()));
        }
        uint numWeld = min(traceTo - weldFrom, weldTo - traceFrom);
        for (uint iterWeld = 0; iterWeld < numWeld; ++iterWeld)
        {
            float lerpFactor = static_cast<float>(iterWeld) / numWeld;
            track[iterWeld] = BipedWalkAnimation::FootPose{track[weldFrom - traceFrom + iterWeld], track[iterWeld], lerpFactor};
        }
        track.erase(track.begin() + (weldFrom - traceFrom + 1), track.end());

        // Update phase
        auto& leftFootInfo = m_foots[BipedSkeleton::LeftFoot];
        double timeOffset = footInfo.footsteps[1].firstTime - leftFootInfo.footsteps[1].firstTime;
        float period = static_cast<float>(m_period);
        while (timeOffset < 0)
            timeOffset += period;
        footAnimation.phase = static_cast<float>(fmod(timeOffset, period) / period);
    }
}
BipedWalkAnimation::FootPose
BipedWalkAnalyzer::analyzeFootWalkPose(const float3& root,
                                       const float3& thigh, const float3& calf,
                                       const float3& foot, const float3& toe) const
{
    using namespace math3;

    static const normal defRight{1, 0, 0};
    static const normal defUp{0, 1, 0};
    static const normal defForward{0, 0, 1};

    // Knee rotation
    segment thigh2foot{thigh, foot};
    point kneeProj = thigh2foot.put(projectPointOnSegment(thigh2foot, calf));
    normal kneeDir = normalize(calf - kneeProj);
    normal forwardDir = normalize(cross(thigh2foot.direction(), defRight));
    scalar kneeRotation = angleVectorVector(forwardDir, kneeDir, -thigh2foot.direction());

    // Return
    BipedWalkAnimation::FootPose ret;
    ret.toePosition = toe - root;
    ret.toe2foot = foot - toe;
    ret.kneeRotation = kneeRotation;
    return ret;
}
void BipedWalkAnalyzer::analyzeSpine()
{
    auto& spineBones = m_skeleton.spineBones();

    m_spineAnimation = m_periodicalAnimation->extractReindex(spineBones);
}
void BipedWalkAnalyzer::analyzeHands()
{
    auto& handsBones = m_skeleton.handBones();

    m_handsAnimation = m_periodicalAnimation->extractReindex(handsBones);
}


// Finalize
void BipedWalkAnalyzer::finalize()
{
    m_animation = make_shared<BipedWalkAnimation>(
        m_skeletonHolder,
        static_cast<float>(m_unitPeriod), static_cast<float>(m_basePhase),
        m_pelvisAnimation,
        m_feetAnimation[BipedSkeleton::LeftFoot], m_feetAnimation[BipedSkeleton::RightFoot],
        m_spineAnimation, m_handsAnimation);
}


// Gets
BipedWalkAnalyzer::Foot& BipedWalkAnalyzer::getFoot(uint index)
{
    return m_foots[index];
}
double BipedWalkAnalyzer::getAverageRootSpeed() const
{
    auto& linear = m_helper.linearMovement();
    return distance(linear.front(), linear.back()) / m_restoredAnimation->duration();
}
void BipedWalkAnalyzer::drawSourceAnimation(Plotter& plotter,
                                            float factor, bool smooth)
{
    // Draw anim
    drawAnimation(plotter,{0.7f, 0.7f, 0.7f}, *m_sourceAnimation, factor, smooth);
}
void BipedWalkAnalyzer::drawRestoredAnimation(Plotter& plotter,
                                              float factor, bool smooth)
{
    // Draw anim
    auto time = drawAnimation(plotter,{1.0f, 1.0f, 1.0f}, *m_restoredAnimation, factor, smooth);

    // Draw footsteps
    for (auto& foot : m_foots)
    {
        auto footstep = foot.findFootstep(time);
        if (footstep)
            plotter.plotWireSphere(matrix::translate(footstep->toePosition),
                                   0xffff0000, 0.1f);
    }
}
void BipedWalkAnalyzer::drawCenteredAnimation(Plotter& plotter,
                                              float factor, bool smooth)
{
    // Draw anim
    drawAnimation(plotter,{0.0f, 0.0f, 1.0f}, *m_helper.centeredAnimation(), factor, smooth);
}
void BipedWalkAnalyzer::drawPeriodicalAnimation(Plotter& plotter,
                                                float factor, bool smooth)
{
    // Draw anim
    drawAnimation(plotter,{0.0f, 1.0f, 0.0f}, *m_periodicalAnimation, factor, smooth);
}
double BipedWalkAnalyzer::drawAnimation(Plotter& plotter,
                                        const float3& boneColor,
                                        const NodeAnimation& animation,
                                        float factor, bool smooth)
{
    // Compute time
    auto time = fract(factor) * animation.duration();

    // Draw anim
    ComposedNodeArray nodes;
    if (smooth)
        animation.lerpPoses(time, nodes);
    else
        animation.getPoses(time, nodes);
    m_hierarchy.reset(nodes);
    m_hierarchy.debugDraw(plotter, tagColor, boneColor, rootSize, boneSize);

    return time;
}


// Nested
const BipedWalkAnalyzer::Footstep*
BipedWalkAnalyzer::Foot::findFootstep(double time) const
{
    for (auto& footstep : footsteps)
    {
        if (footstep.firstTime > time)
            return nullptr;
        if (footstep.lastTime > time)
            return &footstep;
    }
    return nullptr;
}
