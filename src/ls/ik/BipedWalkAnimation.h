/** @file ls/ik/BipedWalkAnimation.h
 *  @brief Biped skeleton walk animation
 */
#pragma once
#include "ls/common.h"
#include "ls/geom/NodeHierarchy.h"
#include "ls/ik/BipedSkeleton.h"

namespace ls
{
    class NodeAnimation;
    class BipedWalkAnalyzer;

    /** @addtogroup LsIk
     *  @{
     */
    /** Animation global parameters.
     */
    struct AnimationEnvironment
    {
        /// @brief Ctor
        AnimationEnvironment() = default;
        /// @brief Ctor
        AnimationEnvironment(const float3& position, const float3& look)
            : position(position)
            , look(look)
        {
        }
        /// @brief Base position
        float3 position;
        /// @brief Look direction
        float3 look;
    };
    /** Biped skeleton walk animation
     */
    class BipedWalkAnimation
        : Noncopyable
        , public Named
    {
    public:
        /// @brief Pelvis animation
        struct Pelvis
        {
            /// @brief Track
            vector<float3> track;
            /// @brief Lerp
            float3 lerp(float factor) const
            {
                return interpolateArrayFactor<float3>(track, track.size() - 1, factor);
            }
        };
        /// @brief Foot pose
        struct FootPose
        {
            /// @brief Positions
            struct Positions
            {
                /// @brief Thigh position
                float3 thigh;
                /// @brief Calf position
                float3 calf;
                /// @brief Foot position
                float3 foot;
                /// @brief Toe position
                float3 toe;
            };
            /// @brief Ctor
            FootPose() = default;
            /// @brief Lerp
            FootPose(const FootPose& lhs, const FootPose& rhs, float factor)
                : toePosition(lerp(lhs.toePosition, rhs.toePosition, factor))
                , toe2foot(lerp(lhs.toe2foot, rhs.toe2foot, factor))
                , kneeRotation(lerp(lhs.kneeRotation, rhs.kneeRotation, factor))
            {
            }
            /// @brief Compute
            Positions compute(const float3& basePosition,
                              const float3& thighPosition, const float3& forwardVec,
                              float thighLength, float calfLength, float footLength) const;
            /// @brief Toe position
            float3 toePosition = float3();
            /// @brief Toe-to-foot vector
            float3 toe2foot = float3();
            /// @brief Knee rotation around pelvis-foot axis
            float kneeRotation = 0;
        };
        /// @brief Foot animation
        struct Foot
        {
            /// @brief Track
            vector<FootPose> track;
            /// @brief Phase offset
            float phase = 0;
            /// @brief Lerp
            FootPose lerp(float factor) const
            {
                float movedFactor = fract(factor + phase);
                return interpolateArrayFactor<FootPose>(track, track.size() - 1,
                    movedFactor);
            }
        };
    public:
        /** Ctor
         *  @param skeleton
         *    Animated skeleton
         *  @param period
         *    Animation period
         *  @param base
         *    Animation base phase
         *  @param rootAnimation
         *    Root animation track
         *  @param leftFoot,rightFoot
         *    Left and right foot track
         *  @param spineAnimation
         *    Biped spine animation
         *  @param handsAnimation
         *    Biped hands animation
         */
        BipedWalkAnimation(shared_ptr<Skeleton> skeleton,
                           float period, float base,
                           const Pelvis& rootAnimation, const Foot& leftFoot, const Foot& rightFoot,
                           shared_ptr<NodeAnimation> spineAnimation = nullptr,
                           shared_ptr<NodeAnimation> handsAnimation = nullptr);
        /// @brief Dtor
        ~BipedWalkAnimation();

        /** @name Animation
         *  @{
         */
        /// @brief Reset poses and apply root pose and look rotation
        void applyRoot(ComposedNodeArray& pose, const AnimationEnvironment& info,
                       float factor, bool needUpdate);
        /// @brief Apply spine animation
        void applySpine(ComposedNodeArray& pose, const AnimationEnvironment& info,
                        float factor, bool needUpdate);
        /// @brief Apply foot animation
        void applyFoot(ComposedNodeArray& pose, const AnimationEnvironment& info,
                       BipedSkeleton::FootIndex foot, float factor);
        /// @brief Apply hands
        void applyHands(ComposedNodeArray& pose, float factor);
        /// @brief Apply all
        void apply(ComposedNodeArray& pose, const AnimationEnvironment& info,
                   float factor);
        /// @}

        /** @name Gets
         *  @{
         */
        /// @brief Get animation period
        float unitPeriod() const noexcept
        {
            return m_unitPeriod;
        }
        /// @brief Get animation base phase offset
        float basePhase() const noexcept
        {
            return m_basePhase;
        }
        /// @brief Get pelvis animation
        const Pelvis& pelvis() const noexcept
        {
            return m_pelvisAnimation;
        }
        /// @brief Get foot animation
        const Foot& foot(BipedSkeleton::FootIndex index) const noexcept
        {
            return m_feetAnimation[index];
        }
        /// @brief Test if has spine animation
        bool hasSpine() const noexcept
        {
            return !!m_spineAnimation;
        }
        /// @brief Get spine animation
        const NodeAnimation& spine() const noexcept
        {
            return *m_spineAnimation;
        }
        /// @brief Test if has hands animation
        bool hasHands() const noexcept
        {
            return !!m_handsAnimation;
        }
        /// @brief Get hands animation
        const NodeAnimation& hands() const noexcept
        {
            return *m_handsAnimation;
        }
        /// @brief Get biped
        const BipedSkeleton& biped() const noexcept
        {
            return m_biped;
        }
        /// @}

        /** @name Debug
         *  @{
         */
        /// @brief Set debug importer
        void debugInit(shared_ptr<BipedWalkAnalyzer> importer)
        {
            m_debugImporter = importer;
        }
        /// @brief Get debug importer
        shared_ptr<BipedWalkAnalyzer> debugAnalyzer() const
        {
            return m_debugImporter;
        }
        /// @}
    private:
        /// @brief Skeleton holder
        shared_ptr<Skeleton> m_skeletonHolder;
        /// @brief Biped skeleton
        BipedSkeleton& m_biped;
        /// @brief Biped hierarchy
        NodeHierarchy& m_hierarchy;

        /// @brief Animation period for unit speed
        float m_unitPeriod;
        /// @brief Animation base phase offset
        float m_basePhase;

        /// @brief Pelvis animation
        Pelvis m_pelvisAnimation;
        /// @brief Feet animation
        array<Foot, BipedSkeleton::NumFeet> m_feetAnimation;
        /// @brief Spine animation
        shared_ptr<NodeAnimation> m_spineAnimation;
        /// @brief Hands animation
        shared_ptr<NodeAnimation> m_handsAnimation;

        /// @brief Temporary array
        ComposedNodeArray m_tempNodes;
        /// @brief Debug importer
        shared_ptr<BipedWalkAnalyzer> m_debugImporter;
    };
    /// @}
}

