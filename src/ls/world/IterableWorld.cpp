#include "pch.h"
#include "ls/world/IterableWorld.h"
#include "ls/io/FileSystemInterface.h"

using namespace ls;


// Main
IterableWorld::IterableWorld(bool resetSaved, 
                             shared_ptr<WorldAsset> world)
    : Nameable<IterableWorld>("IterableWorld")
    
    , resetSaved(resetSaved)

    , m_world(world)
{
}
IterableWorld::~IterableWorld()
{
    m_region.reset();
}


// Load/unload
void IterableWorld::load(const int2& regionIndex)
{
    finalize();
    m_lastRegion = regionIndex;

    // Open region
    m_world->loadRegionChunks(regionIndex);
    m_world->waitAll();
    m_world->flushAll();

    m_region = m_world->findRegion(regionIndex)->shared_from_this();
    debug_assert(m_region);

    // Info
    for (Observer* observer : m_observers)
    {
        observer->onLoadRegionData(*m_region);
        observer->onPreloadChunksData(*m_region);
        observer->onAsyncLoadChunksData(*m_region);
        observer->onPostloadChunksData(*m_region);
    }
}
void IterableWorld::finalize()
{
    // Finalize last region
    if (!m_region)
        return;

    // Info
    for (Observer* observer : m_observers)
    {
        observer->onUnloadChunksData(*m_region);
        observer->onUnloadRegionData(*m_region);
    }

    // Save
    m_region->save();
    m_region->unloadRegion();

    m_region = nullptr;
}


// Observers
void IterableWorld::doAddObserver(Observer& observer)
{
    observer.onMoveToRegion(m_lastRegion);
    observer.onMoveToChunk(m_lastRegion * m_world->config.regionInChunks);
    observer.onRecenter(int2(0, 0), int2(0, 0), double3(0.0));

    if (m_region)
    {
        observer.onLoadRegionData(*m_region);
        observer.onPreloadChunksData(*m_region);
        observer.onAsyncLoadChunksData(*m_region);
        observer.onPostloadChunksData(*m_region);
    }
}
void IterableWorld::doRemoveObserver(Observer& observer)
{
    if (m_region)
    {
        observer.onUnloadChunksData(*m_region);
        observer.onUnloadRegionData(*m_region);
    }
}

