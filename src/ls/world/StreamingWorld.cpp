#include "pch.h"
#include "ls/world/StreamingWorld.h"
#include "ls/core/Worker.h"
#include "ls/io/FileSystemInterface.h"
using namespace ls;

// Ctor
StreamingWorld::StreamingWorld(shared_ptr<WorldAsset> world,
                               const WorldStreamingConfig& streamingConfig)
    : Nameable<StreamingWorld>("StreamingWorld")

    , worldConfig(world->config)
    , streamingConfig(streamingConfig)

    , chunkSize(worldConfig.chunkWidth)
    , regionWidth(worldConfig.regionInChunks)

    , m_world(world)
{
    WorldRegion::Callbacks callbacks;
    callbacks.postLoadRegion = std::bind(&StreamingWorld::loadRegionData, this, _1);
    callbacks.unloadRegion = std::bind(&StreamingWorld::unloadRegionData, this, _1);
    callbacks.preLoadChunks = std::bind(&StreamingWorld::preloadChunksData, this, _1);
    callbacks.asyncLoadChunks = std::bind(&StreamingWorld::loadChunksData, this, _1);
    callbacks.postLoadChunks = std::bind(&StreamingWorld::postloadChunksData, this, _1);
    callbacks.unloadChunks = std::bind(&StreamingWorld::unloadChunksData, this, _1);
    m_world->setCallbacks(callbacks);
}
StreamingWorld::~StreamingWorld()
{
    discardAll();
}


// Observers
void StreamingWorld::doAddObserver(Observer& observer)
{
    // Recenter
    observer.onRecenter(int2(0), int2(0), double3(0.0));

    // Skip if position is undefined
    if (!m_initialized)
        return;

    // Move
    observer.onMoveToRegion(m_currentRegion);
    observer.onMoveToChunk(m_currentChunk);
    observer.onUpdateAllChunksData(m_currentRegion);
    observer.onUpdateAllRegionData(m_currentRegion);

    // Load regions
    for (auto& iter : m_world->regions())
    {
        shared_ptr<WorldRegion> region = iter.second;

        if (region->isRegionDataReady())
        {
            observer.onLoadRegionData(*region);
        }

        if (region->isChunksDataReady())
        {
            observer.onPreloadChunksData(*region);
            observer.onAsyncLoadChunksData(*region);
            observer.onPostloadChunksData(*region);
        }
    }
}
void StreamingWorld::doRemoveObserver(Observer& observer)
{
    stable();
    if (!m_initialized)
        return;

    // Unload regions
    for (auto& iter : m_world->regions())
    {
        shared_ptr<WorldRegion> region = iter.second;

        if (region->isChunksDataReady())
        {
            observer.onUnloadChunksData(*region);
        }

        if (region->isRegionDataReady())
        {
            observer.onUnloadRegionData(*region);
        }
    }
}


// Streaming control
void StreamingWorld::updateByChunk(const int2& chunkIndex)
{
    updateByPosition((static_cast<double2>(chunkIndex) + 0.5) * worldConfig.chunkWidth);
}
void StreamingWorld::updateByPosition(const double2& position)
{
    if (!m_initialized || m_lastPosition != position)
    {
        m_lastPosition = position;
        updateData(position);
        m_initialized = true;
    }
}
void StreamingWorld::flush()
{
    flushTerrain();
}
void StreamingWorld::forceFlush()
{
    stable();
    flush();
}
void StreamingWorld::saveAll()
{
    stable();

    for (auto& iter : m_world->regions())
    {
        shared_ptr<WorldRegion> region = iter.second;

        region->save();
    }

    logInfo("Terrain is fully saved");
}
void StreamingWorld::discardAll()
{
    stable();

    m_world->unloadAll();

    logInfo("Terrain is fully discarded");
}
void StreamingWorld::stable()
{
    m_world->waitAll();
    flushTerrain();
}


// Loading control
double StreamingWorld::computeDistance(const int2& chunk, double chunkSize, const double2& point)
{
    double2 center = (static_cast<double2>(chunk) + 0.5) * chunkSize;
    double2 a = abs(center - point) - chunkSize;
    return sqrt(square_length(max(a, double2(0.0))));
}
void StreamingWorld::updateData(const double2& position)
{
    // This region
    int2 thisChunk = static_cast<int2>(floor(position / static_cast<double>(worldConfig.chunkWidth)));
    int2 thisRegion = static_cast<int2>(floor(position / static_cast<double>(worldConfig.regionWidth)));

    // Recenter
    if (distance(swiz<X, Z>(m_currentCenterPosition), position) > *streamingConfig.recenteringStep)
    {
        m_currentCenterPosition = static_cast<double3>(swiz<X, O, Y>(thisChunk)) * worldConfig.chunkWidth;
        recenter(m_currentCenter, thisChunk, m_currentCenterPosition);
        m_currentCenter = thisChunk;
    }

    // Move
    if (!m_initialized || thisRegion != m_currentRegion)
    {
        m_currentRegion = thisRegion;
        moveToRegion(thisRegion);
    }
    if (!m_initialized || thisChunk != m_currentChunk)
    {
        m_currentChunk = thisChunk;
        moveToChunk(thisChunk);
    }

    // Reload regions
    if (!m_initialized 
        || distance(position, m_lastRegionPosition) >= *streamingConfig.previewLoadingStep)
    {
        m_lastRegionPosition = position;
        updateRegionData(thisRegion, position);

        // Callback
        for (Observer* observer : m_observers)
        {
            observer->onUpdateAllChunksData(m_currentRegion);
        }
    }

    // Reload details
    if (!m_initialized 
        || distance(position, m_lastChunkPosition) >= *streamingConfig.detailLoadingStep)
    {
        m_lastChunkPosition = position;
        updateChunksData(thisRegion, position);

        // Callback
        for (Observer* observer : m_observers)
        {
            observer->onUpdateAllRegionData(m_currentRegion);
        }
    }
}
void StreamingWorld::updateRegionData(const int2& thisRegion, const double2& position)
{
    // Get distance
    double loadingDistance = *streamingConfig.previewLoadingDist + *streamingConfig.previewLoadingStep * 2;
    sint width = static_cast<sint>(ceil(loadingDistance / worldConfig.regionWidth));

    // Load 
    int2 regionIndex;
    for (regionIndex.x = thisRegion.x - width; regionIndex.x <= thisRegion.x + width; ++regionIndex.x)
    {
        for (regionIndex.y = thisRegion.y - width; regionIndex.y <= thisRegion.y + width; ++regionIndex.y)
        {
            updateSingleRegionData(regionIndex, position, loadingDistance);
        }
    }

    // Unload
    for (auto& iterRegion : m_world->regions())
    {
        updateSingleRegionData(iterRegion.first, position, loadingDistance);
    }
}
void StreamingWorld::updateSingleRegionData(const int2& regionIndex, 
                                         const double2& position, double distance)
{
    const double regionDistance = computeDistance(regionIndex, worldConfig.regionWidth, position);
    if (regionDistance < distance)
    {
        m_world->loadRegion(regionIndex);
    }
    else
    {
        m_world->unloadRegion(regionIndex);
    }
}
void StreamingWorld::updateChunksData(const int2& thisRegion, const double2& position)
{
    // Get distance
    double loadingDistance = *streamingConfig.detailLoadingDist + *streamingConfig.detailLoadingStep * 2;
    sint width = static_cast<sint>(ceil(loadingDistance / worldConfig.regionWidth));

    // Load regions
    int2 regionIndex;
    for (regionIndex.x = thisRegion.x - width; regionIndex.x <= thisRegion.x + width; ++regionIndex.x)
    {
        for (regionIndex.y = thisRegion.y - width; regionIndex.y <= thisRegion.y + width; ++regionIndex.y)
        {
            updateSingleChunksData(regionIndex, position, loadingDistance);
        }
    }

    // Unload
    for (auto& iterRegion : m_world->regions())
    {
        updateSingleChunksData(iterRegion.first, position, loadingDistance);
    }
}
void StreamingWorld::updateSingleChunksData(const int2& regionIndex,
                                        const double2& position, double distance)
{
    double regionDistance = computeDistance(regionIndex, worldConfig.regionWidth, position);

    if (regionDistance < distance)
    {
        m_world->loadRegionChunks(regionIndex);
    }
    else
    {
        m_world->unloadRegionChunks(regionIndex);
    }
}
void StreamingWorld::flushTerrain()
{
    m_world->flushAll();
}


// Loading trans
void StreamingWorld::loadRegionData(WorldRegion& region)
{
    for (Observer* observer : m_observers)
    {
        observer->onLoadRegionData(region);
    }
}
void StreamingWorld::unloadRegionData(WorldRegion& region)
{
    for (Observer* observer : m_observers)
    {
        observer->onUnloadRegionData(region);
    }
}

void StreamingWorld::preloadChunksData(WorldRegion& region)
{
    for (Observer* observer : m_observers)
    {
        observer->onPreloadChunksData(region);
    }
}
void StreamingWorld::loadChunksData(WorldRegion& region)
{
    for (Observer* observer : m_observers)
    {
        observer->onAsyncLoadChunksData(region);
    }
}
void StreamingWorld::postloadChunksData(WorldRegion& region)
{
    for (Observer* observer : m_observers)
    {
        observer->onPostloadChunksData(region);
    }
}
void StreamingWorld::unloadChunksData(WorldRegion& region)
{
    for (Observer* observer : m_observers)
    {
        observer->onUnloadChunksData(region);
    }
}

void StreamingWorld::moveToChunk(const int2& chunkIndex)
{
    for (Observer* observer : m_observers)
    {
        observer->onMoveToChunk(chunkIndex);
    }
}
void StreamingWorld::moveToRegion(const int2& regionIndex)
{
    for (Observer* observer : m_observers)
    {
        observer->onMoveToRegion(regionIndex);
    }
}
void StreamingWorld::recenter(const int2& oldCenter, const int2& newCenter,
                              const double3& newCenterPosition)
{
    for (Observer* observer : m_observers)
    {
        observer->onRecenter(oldCenter, newCenter, newCenterPosition);
    }
}


// Index stuff
void StreamingWorld::chunk2region(const int2& chunk, int2& region, int2& local) const
{
    m_world->chunk2region(chunk, region, local);
}
int2 StreamingWorld::chunk2region(const int2& chunk) const
{
    return m_world->chunk2region(chunk);
}
int2 StreamingWorld::projectChunkToRegion(const int2& chunk, const int2& region)
{
    int2 regionBegin = region * regionWidth;
    int2 regionEnd = (region + 1) * regionWidth - 1;
    return clamp(chunk, regionBegin, regionEnd);
}
