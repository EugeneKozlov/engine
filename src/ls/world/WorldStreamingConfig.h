/// @file ls/world/WorldStreamingConfig.h
/// @brief World streaming config
#pragma once

#include "ls/common.h"
#include "ls/app/ConfigManager.h"

namespace ls
{
    /// @addtogroup LsWorld
    /// @{

    /// @brief World Streaming Config
    class WorldStreamingConfig
        : public ConfigPrototype
    {
    public:
        /// @brief Compute all
        void compute()
        {
            staticPhysicsRadius.set(*staticPhysicsPad + *activePhysicsRadius);
        }
        /// @brief Operate
        template <class Operator>
        void operate(Operator op)
        {
            op(logChunks);
            op(logPacks);
            op(logMovement);

            op(recenteringStep);
            op(detailLoadingDist);
            op(detailLoadingStep);
            op(previewLoadingDist);
            op(previewLoadingStep);


            op(activePhysicsRadius);
            op(activePhysicsRadiusMin);
            op(activePhysicsRadiusMax);
            op(activePhysicsRadiusPad);
            op(activePhysicsInertia);
            op(staticPhysicsPad);

            op(recenteringDistance);
            op(packLoadingDist);
            op(packLoadingInertia);
            op(regionLoadingDist);
            op(regionLoadingInertia);

            op(chunkCacheSize);
            op(regionCacheSize);
        }
    public:
        /// @brief Set to log terrain chunks messages
        ConfigProperty<bool> logChunks
            = { "config.world_streaming.log_chunks" };
        /// @brief Set to log terrain packs messages
        ConfigProperty<bool> logPacks
            = { "config.world_streaming.log_packs" };
        /// @brief Set to log actor movement messages
        ConfigProperty<bool> logMovement
            = { "config.world_streaming.log_movement" };

        /// @brief World re-centering step (m); 0 to disable
        ConfigProperty<double> recenteringStep
            = { "config.world_streaming.recentering_step" };
        /// @brief Detail terrain geometry loading distance (m)
        ConfigProperty<double> detailLoadingDist
            = { "config.world_streaming.detail_load_dist" };
        /// @brief Detail terrain geometry loading step (m)
        ConfigProperty<double> detailLoadingStep
            = { "config.world_streaming.detail_load_step" };
        /// @brief Preview terrain geometry loading distance (m)
        ConfigProperty<double> previewLoadingDist
            = { "config.world_streaming.preview_load_dist" };
        /// @brief Preview terrain geometry loading step (m)
        ConfigProperty<double> previewLoadingStep
            = { "config.world_streaming.preview_load_step" };

        /// @brief Active physics area size
        ConfigProperty<uint> activePhysicsRadius
            = { "config.world_streaming.physics.radius" };
        /// @brief Min active physics area size
        ConfigProperty<uint> activePhysicsRadiusMin
            = { "config.world_streaming.physics.radius_min" };
        /// @brief Max active physics area size
        ConfigProperty<uint> activePhysicsRadiusMax
            = { "config.world_streaming.physics.radius_max" };
        /// @brief Active physics area size pad
        ConfigProperty<uint> activePhysicsRadiusPad
            = { "config.world_streaming.physics.radius_pad" };
        /// @brief Static physics area radius padding
        ConfigProperty<uint> staticPhysicsPad
            = { "config.world_streaming.physics.static_pad" };
        /// @brief Inertia of physical and network interaction, use small numbers,
        /// but zero inertia is not recommended
        ConfigProperty<uint> activePhysicsInertia
            = { "config.world_streaming.physics.inertia" };
        /// @brief Static physics area size
        /// #activePhysicsRadius + #staticPhysicsPad
        ConfigProperty<uint> staticPhysicsRadius;

        /// @brief World re-centering distance
        ConfigProperty<uint> recenteringDistance
            = { "config.world_streaming.loading.recentering" };

        /// @brief Packs loading distance (in packs)
        ConfigProperty<uint> packLoadingDist
            = { "config.world_streaming.loading.pack_dist" };
        /// @brief Packs loading/unloading distance pad (in packs)
        ConfigProperty<uint> packLoadingInertia
            = { "config.world_streaming.loading.pack_inertia" };
        /// @brief Regions loading distance
        ConfigProperty<uint> regionLoadingDist
            = { "config.world_streaming.loading.region_dist" };
        /// @brief Regions loading/unloading distance pad
        ConfigProperty<uint> regionLoadingInertia
            = { "config.world_streaming.loading.region_inertia" };

        /// @brief Chunk cache size
        ConfigProperty<uint> chunkCacheSize
            = { "config.world_streaming.rendering.chunk_cache" };
        /// @brief Region cache size
        ConfigProperty<uint> regionCacheSize
            = { "config.world_streaming.rendering.region_cache" };
    };
    /// @}
}