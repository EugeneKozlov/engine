/// @file ls/world/WorldObserverInterface.h
/// @brief World observer interface
#pragma once

#include "ls/common.h"
#include "ls/asset/WorldRegion.h"

namespace ls
{
    /// @addtogroup LsWorld
    /// @{

    /// @brief World observer interface
    /// @note Chunk is valid between add and remove calls.
    class WorldObserverInterface
    {
    public:
        /// @name Region data callbacks
        /// @{

        /// @brief Load region data
        virtual void onLoadRegionData(WorldRegion& /*region*/)
        {
        }
        /// @brief Unload region data
        virtual void onUnloadRegionData(WorldRegion& /*region*/)
        {
        }
        /// @}

        /// @name Chunks data callbacks
        /// @{

        /// @brief Pre-load chunks data
        virtual void onPreloadChunksData(WorldRegion& /*region*/)
        {
        }
        /// @brief Async load chunks data
        virtual void onAsyncLoadChunksData(WorldRegion& /*region*/)
        {
        }
        /// @brief Post-load chunks data
        virtual void onPostloadChunksData(WorldRegion& /*region*/)
        {
        }
        /// @brief Unload @a region preview
        virtual void onUnloadChunksData(WorldRegion& /*region*/)
        {
        }
        /// @}

        /// @name Other callbacks
        /// @{

        /// @brief Move @a chunk callback
        virtual void onMoveToChunk(const int2& /*chunk*/)
        {
        }
        /// @brief Move @a region callback
        virtual void onMoveToRegion(const int2& /*region*/)
        {
        }
        /// @brief Called when region data is updated because of large movement
        virtual void onUpdateAllRegionData(const int2& /*currentRegion*/)
        {
        }
        /// @brief Called when chunks data is updated because of large movement
        virtual void onUpdateAllChunksData(const int2& /*currentRegion*/)
        {
        }
        /// @brief Called on re-centering renderer from @a oldCenter to @a newCenter
        virtual void onRecenter(const int2& /*oldCenter*/,
                                const int2& /*newCenter*/,
                                const double3& /*newCenterPosition*/)
        {
        }
        /// @}
    };
    /// @brief World observer with specified async storage
    /// @tparam TStorage Type of async storage
    template <class TStorage>
    class WorldObserver
        : public WorldObserverInterface
    {
    private:
        /// @name Implement WorldObserverInterface
        /// @{
        virtual void onPreloadChunksData(WorldRegion& region) override final
        {
            region.allocateAsyncStorage<TStorage>(this);
            doLoadChunksPre(region, region.getAsyncStorage<TStorage>(this));
        }
        virtual void onAsyncLoadChunksData(WorldRegion& region) override final
        {
            doLoadChunks(region, region.getAsyncStorage<TStorage>(this));
        }
        virtual void onPostloadChunksData(WorldRegion& region) override final
        {
            doLoadChunksPost(region, region.getAsyncStorage<TStorage>(this));
            region.releaseAsyncStorage(this);
        }
        /// @}

        /// @name New callbacks
        /// @{
        virtual void doLoadChunksPre(WorldRegion& region, TStorage& storage) = 0;
        virtual void doLoadChunks(WorldRegion& region, TStorage& storage) = 0;
        virtual void doLoadChunksPost(WorldRegion& region, TStorage& storage) = 0;
        /// @}
    };
    /// @}
}