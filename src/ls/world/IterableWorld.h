/// @file ls/world/IterableWorld.h
/// @brief Iterable World geometry

#pragma once
#include "ls/common.h"
#include "ls/core/Nameable.h"
#include "ls/io/common.h"
#include "ls/asset/WorldAsset.h"
#include "ls/world/WorldStreamingConfig.h"
#include "ls/world/WorldObserverInterface.h"

namespace ls
{
    /// @addtogroup LsWorld
    /// @{

    /// @brief Iterable World geometry
    class IterableWorld
        : public ObservableInterface<WorldObserverInterface>
        , public Nameable<IterableWorld>
    {
    public:
        /// @brief Ctor
        /// @param resetSaved
        ///   Whether old data should be reset
        /// @param source
        ///   World file system
        /// @param folderName
        ///   World folder name
        /// @param worldConfig
        ///   Configuration
        IterableWorld(bool resetSaved, 
                      shared_ptr<WorldAsset> world);
        /// @brief Dtor
        virtual ~IterableWorld();

        /// @brief Load region at position @a regionIndex and save another
        void load(const int2& regionIndex);
        /// @brief Finalize generation
        void finalize();
        /// @brief Process all regions
        template <class Process>
        void process(const int2& begin, const int2& end, Process processRegion)
        {
            int2 regionIndex;
            for (regionIndex.x = begin.x; regionIndex.x < end.x; ++regionIndex.x)
                for (regionIndex.y = begin.y; regionIndex.y < end.y; ++regionIndex.y)
                {
                    load(regionIndex);
                    processRegion(*m_region);
                    finalize();
                }
        }

        /// @brief World
        shared_ptr<WorldAsset> world() noexcept
        {
            return m_world;
        }
    private:
        virtual void doAddObserver(Observer& observer) override;
        virtual void doRemoveObserver(Observer& observer) override;
    public:
        /// @brief Whether old data should be reset
        const bool resetSaved;
    private:
        shared_ptr<WorldAsset> m_world;

        int2 m_lastRegion;
        shared_ptr<WorldRegion> m_region;
    };
    /// @}
}