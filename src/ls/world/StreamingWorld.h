/// @file ls/world/StreamingWorld.h
/// @brief Streaming world geometry
#pragma once

#include "ls/common.h"
#include "ls/core/Nameable.h"
#include "ls/io/common.h"
#include "ls/asset/WorldAsset.h"
#include "ls/world/WorldStreamingConfig.h"
#include "ls/world/WorldObserverInterface.h"

namespace ls
{
    class Worker;

    /// @addtogroup LsWorld
    /// @{
    
    /// @brief Streaming World loader
    class StreamingWorld
        : public ObservableInterface<WorldObserverInterface>
        , public Nameable<StreamingWorld>
    {
    public:
        /// @brief Ctor
        /// @param worker
        ///   Worker
        /// @param source
        ///   World file system
        /// @param folderName
        ///   World folder name
        /// @param worldConfig,streamingConfig
        ///   Configuration
        /// @param initialChunk
        ///   Initial chunk
        /// @param useSquare
        ///   Set to use square distance instead of round
        /// @param loadAll
        ///   Set to load always whole region
        StreamingWorld(shared_ptr<WorldAsset> world,
                       const WorldStreamingConfig& streamingConfig);
        /// @brief Dtor
        virtual ~StreamingWorld();

        /// @brief Move to @a chunk and update world
        void updateByChunk(const int2& chunkIndex);
        /// @brief Move to @a position and update world
        void updateByPosition(const double2& position);
        /// @brief Flush incoming
        void flush();
        /// @brief Wait for loading and flush all
        void forceFlush();
        /// @brief Save all
        void saveAll();
        /// @brief Discard all
        void discardAll();

        /// @brief Global chunk to region
        void chunk2region(const int2& chunk, int2& region, int2& local) const;
        /// @brief Global chunk to region
        int2 chunk2region(const int2& chunk) const;
        /// @brief Get chunk in region which is nearest to specified
        int2 projectChunkToRegion(const int2& chunk, const int2& region);

        /// @brief Get current chunk
        const int2& currentChunk() const noexcept
        {
            return m_currentChunk;
        }
        /// @brief Get world center
        const int2& worldCenter() const noexcept
        {
            return m_currentCenter;
        }
        /// @brief Get world center position
        const double3& worldCenterPosition() const noexcept
        {
            return m_currentCenterPosition;
        }
    private:
        virtual void doAddObserver(Observer& observer) override;
        virtual void doRemoveObserver(Observer& observer) override;
    private:
        // Control
        void stable();

        // Loading
        static double computeDistance(const int2& chunk, double chunkSize, const double2& point);
        void updateData(const double2& position);
        void updateRegionData(const int2& thisRegion, const double2& position);
        void updateSingleRegionData(const int2& regionIndex,
                                    const double2& position, double distance);
        void updateChunksData(const int2& thisRegion, const double2& position);
        void updateSingleChunksData(const int2& regionIndex,
                                    const double2& position, double distance);
        void flushTerrain();

        // Callbacks
        void loadRegionData(WorldRegion& region);
        void unloadRegionData(WorldRegion& region);

        void preloadChunksData(WorldRegion& region);
        void loadChunksData(WorldRegion& region);
        void postloadChunksData(WorldRegion& region);
        void unloadChunksData(WorldRegion& region);

        void moveToChunk(const int2& chunkIndex);
        void moveToRegion(const int2& regionIndex);
        void recenter(const int2& oldCenter, const int2& newCenter, 
                      const double3& newCenterPosition);

        // Log
        template <class ... Args>
        void logMovement(const Args& ... args)
        {
            if (*streamingConfig.logMovement)
                logDebug(args...);
        }
    public:
        /// @brief World config
        const WorldRegion::Config worldConfig;
        /// @brief Streaming config
        const WorldStreamingConfig streamingConfig;

        /// @brief Chunk width
        const uint chunkSize;
        /// @brief Region width in chunks
        const uint regionWidth;
    private:
        shared_ptr<WorldAsset> m_world;

        bool m_initialized = false;
        double2 m_lastPosition;
        double2 m_lastChunkPosition;
        double2 m_lastRegionPosition;
        int2 m_currentChunk;
        int2 m_currentRegion;

        int2 m_currentCenter = int2(0);
        double3 m_currentCenterPosition = double3(0.0);
    };
    /// @}
}