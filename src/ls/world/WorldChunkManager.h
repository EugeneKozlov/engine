/// @file ls/world/WorldChunkManager.h
/// @brief World chunks manager and helper classes
#pragma once

#include "ls/common.h"
#include "ls/core/Nameable.h"
#include "ls/misc/SlidingArray2D.h"
#include "ls/world/StreamingWorld.h"
#include "ls/network/common.h"

namespace ls
{
    /// @addtogroup LsWorld
    /// @{

    /// @brief N-component position.
    template <uint N>
    class MultiPosition
    {
    public:
        /// @brief Absolute coordinates
        array<int2, N> centers;
        /// @brief Current index
        sint index = 0;
    public:
        /// @brief Discrete round distance
        static uint distanceRound2(const int2& lhs, const int2& rhs) noexcept
        {
            uint dx = (uint) max(abs(lhs.x - rhs.x), 0);
            uint dy = (uint) max(abs(lhs.y - rhs.y), 0);
            return dx * dx + dy*dy;
        }
        /// @brief Discrete square distance
        static uint distanceSquare(const int2& lhs, const int2& rhs) noexcept
        {
            return max<uint>(abs(lhs.x - rhs.x), abs(lhs.y - rhs.y));
        }
        /// @brief Get minimal distance between @a position and centers
        uint minDistance2(const int2& position) const noexcept

        {
            auto dist2 = distanceRound2(position, centers[0]);
            for (uint i = 1; i < N; ++i)
                dist2 = min(dist2, distanceRound2(position, centers[i]));
            return dist2;
        }
        /// @brief Get outside factor (0 - inside, any - outside)
        uint outsideFactor(const int2& position, uint distance) const noexcept
        {
            auto realDistance = sqrtf((float) minDistance2(position));
            if (realDistance < (float) distance)
                return 0;
            else
                return max<uint>(1, (uint) floor(realDistance - distance) + 1);
        }
        /// @brief Test if @a position is center
        bool isCenter(const int2& position) const noexcept
        {
            for (uint i = 0; i < N; ++i)
                if (centers[i] == position)
                    return true;

            return false;
        }
        /// @brief Test if @a position is inside in area
        /// min{distance(@a position, center[j])} < @a distance
        bool isInside(const int2& position, uint distance) const noexcept
        {
            auto dist2 = distance * distance;

            for (uint i = 0; i < N; ++i)
                if (minDistance2(position) < dist2)
                    return true;

            return false;
        }
        /// @brief Add center at @a position, exist centers further than
        /// the @a distance will be cleared, return index
        sint addCenter(const int2& position, uint distance) noexcept
        {
            // Already placed
            for (uint i = 0; i < N; ++i)
                if (centers[i] == position)
                    return -1;

            // Reset far
            for (uint i = 0; i < N; ++i)
                if (distanceSquare(centers[i], position) > distance)
                    centers[i] = position;

            // Insert
            if (++index == N) index = 0;
            centers[index] = position;

            return index;
        }
        /// @brief Copy centers in right order to @a dest array
        void orderedCenters(array<int2, N>& dest) const noexcept
        {
            for (uint i = 0; i < N; ++i)
                dest[i] = centers[(index + 1 + i) % N];
        }
        /// @brief Get center
        int2 center() const noexcept
        {
            return (centers[0] + centers[1] + centers[2] + centers[3]) / N;
        }
    };
    /// @brief 4-component position.
    using MultiPosition4 = MultiPosition<4>;
    /// @brief Chunk observer.
    /// Adds and removes chunks by distance.
    ///
    /// Chunks inside inner distance will be added
    /// Chunks outside outer distance will be unloaded
    /// I recommend you to set @a outerDistancePad
    /// (i.e. difference between inner and outer) to 1
    class WorldChunkObserver
        : Noncopyable
    {
    public:
        /// @brief Add chunk callback
        function<void(const int2&)> onAddChunk;
        /// @brief Remove chunk callback
        function<void(const int2&)> onRemoveChunk;
    public:
        /// @brief Ctor.
        /// @param maxDistance
        ///   Inner distance limit
        /// @param outerDistancePad
        ///   Chunks outside will be unloaded
        /// @param borderSize
        ///   If you are going to use @a outsideFactor up to @a N, set this pad into @a N
        /// @param innerDistance
        ///   Chunks inside will be loaded (0 to set later)
        WorldChunkObserver(uint maxDistance,
                           uint outerDistancePad = 1,
                           uint borderSize = 0,
                           uint innerDistance = 0);
        /// @brief Move ctor
        WorldChunkObserver(WorldChunkObserver&& another) noexcept;
        /// @brief Dtor
        ~WorldChunkObserver();
        /// @brief Shift area center to @a target
        void shiftTo(const int2& target);
        /// @brief Move center to @a target position and change @a innerDistance (0 to use current value)
        void moveTo(const int2& target, uint innerDistance = 0);
        /// @brief Move and shift
        void move(const int2& target, uint innerDistance = 0);
        /// @brief Remove this (i.e. reset to default value)
        void remove();
        /// @brief Test if @a chunk is added (@a pad is max allowed outside factor)
        bool isAdded(const int2& chunk, uint pad = 0) const noexcept;
        /// @brief Get inside radius
        uint radius() const noexcept;
        /// @brief Get outside distance
        ubyte outsideDistance(const int2& chunk) const noexcept;
        /// @brief Get centers
        array<int2, 4> centers() const noexcept;
        /// @brief Get position
        const MultiPosition4& position() const noexcept
        {
            return m_position;
        }
        /// @brief Get debug text plot
        string debugText() const;
    private:
        /// @brief Update distances
        void updateDistances(uint innerDistance);
    public:
        /// @brief Inner distance limit
        const uint maxDistance;
        /// @brief Border size
        const uint borderSize;
        /// @brief Stored distance (maxDistance + borderSize)
        const uint storedDistance;
        /// @brief Outer distance pad
        const uint outerDistancePad;
        /// @brief Area size
        const uint areaWidth;
    private:
        /// @brief Position is empty
        bool m_empty = true;

        /// @brief This position
        MultiPosition4 m_position;

        /// @brief Observing distance
        uint m_innerDistance = 0;
        /// @brief Forgetting distance
        uint m_outerDistance = 0;

        /// @brief Map
        SlidingArray2D<ubyte> m_map;
    };
    /// @brief Chunk owner.
    /// Adds and removes chunks by distance and ownership priority.
    /// @see WorldChunkObserver
    class WorldChunkOwner
        : Noncopyable
    {
    public:
        /// @brief Add chunk callback
        function<void(const int2&)> onAddChunk;
        /// @brief Remove chunk callback
        function<void(const int2&)> onRemoveChunk;
        /// @brief Own chunk callback
        function<void(const int2& chunk)> onOwnChunk;
        /// @brief Disown chunk callback
        function<void(const int2& chunk)> onDisownChunk;
    public:
        WorldChunkOwner(ClientId thisClient, uint maxDistance, uint outerDistancePad);
        /// @brief Dtor.
        ~WorldChunkOwner();
        /// @brief Move @a client to @a chunk with inner @a distance
        void moveClient(ClientId client, const int2& chunk, uint distance);
        /// @brief Remove @a client
        void removeClient(ClientId client);

        /// @brief Get @a chunk owner
        ClientId owner(const int2& chunk) const noexcept
        {
            if (m_owners.isInside(chunk))
                return m_owners[chunk];
            else
                return NetInvalidClient;
        }
        /// @brief Test if @a chunk is owned
        bool isOwned(const int2& chunk) const noexcept
        {
            return owner(chunk) == thisClient;
        }
        /// @brief Test if @a chunk is observed by @a client with specified distance @a pad
        bool isObserved(const int2& chunk, ClientId client, uint pad) const noexcept;
        /// @brief Get all @a chunk observers to @a dest
        uint observers(const int2& chunk, vector<ClientId>& dest, uint pad = 0) const;
        /// @brief Get @a client position
        MultiPosition4 position(ClientId client) const;
        /// @brief Get @a client simulation distance
        uint radius(ClientId client) const;
    public:
        /// @brief This owner
        const ClientId thisClient;
        /// @brief Inner distance limit
        const uint maxDistance;
        /// @brief Outer distance pad
        const uint outerDistancePad;
        /// @brief Area size
        const uint areaWidth;
    private:
        void addChunk(ClientId client, const int2& chunk);
        void removeChunk(ClientId client, const int2& chunk);
        /// @brief Get observer
        WorldChunkObserver& getClient(ClientId client);
        /// @brief Shift everybody
        void shiftAll(const int2& chunk);
        /// @brief Get chunk owner
        ClientId calcOwner(const int2& chunk) const;
        /// @brief Update chunk owning
        void updateOwninig(const int2& chunk);
        /// @brief Reset owning
        void resetChunk(const int2& chunk);
    private:
        /// @brief Observers
        map<ClientId, WorldChunkObserver> m_clients;
        /// @brief Owners
        SlidingArray2D<ClientId> m_owners;
        /// @brief Offset
        int2 m_offset = int2(0);
        /// @brief Suppress owner calculation (set during shift or destruction)
        bool m_suppressFlag = false;
    };
    /// @brief Chunk manager.
    /// Manages chunks loading/unloading, owning and simulation.
    ///
    /// Observing area is larger than owning area.
    /// Move function receive @b owning area size.
    /// Observing means that chunk is simulated or should be simulated.
    ///
    /// Simulation condition:
    /// - Chunk must be @b observed
    /// - Chunk geometry must be loaded
    /// - Chunk objects must be loaded
    ///
    /// Owning condition:
    /// - Chunk must be @b owned (territorially)
    /// - Chunk must be simulated
    class WorldChunkManager
        : Noncopyable
        , public Nameable<WorldChunkManager>
        , public WorldObserverInterface
    {
    public:
        /// @brief Enter simulation callback
        function<void(const int2& chunk) > onEnter;
        /// @brief Leave simulation callback
        function<void(const int2& chunk) > onLeave;
    public:
        /// @brief Ctor.
        /// @param thisClient
        ///   This observer/owner client ID
        /// @param maxDistance
        ///   Max observing and owning distance
        /// @param observingDistancePad
        ///   Difference between owning and observing distances
        /// @param outerDistancePad
        ///   Distance inertia pad for both observer and owner;
        ///   I strongly recommend you set this value into @a 1
        /// @param logAll
        ///   Set to log all stuff
        WorldChunkManager(ClientId thisClient,
                          uint maxDistance,
                          uint observingDistancePad,
                          uint outerDistancePad,
                          bool logAll);
        /// @brief Dtor
        ~WorldChunkManager();
        /// @brief Move @a client to @a chunk with owning @a distance
        void moveClient(ClientId client, const int2& chunk, uint distance);
        /// @brief Remove @a client
        void removeClient(ClientId client);
        /// @brief Prepare @a chunk to simulation; this means that chunk's objects are fully loaded
        void prepare(const int2& chunk);

        /// @brief Get this client
        ClientId thisClient() const noexcept
        {
            return m_owner.thisClient;
        }
        /// @brief Get clients list
        const set<ClientId>& clients() const noexcept
        {
            return m_clients;
        }
        /// @brief Get @a client position
        MultiPosition4 position(ClientId client) const noexcept
        {
            return m_owner.position(client);
        }
        /// @brief Get @a client simulation distance
        uint radius(ClientId client) const noexcept
        {
            return m_owner.radius(client);
        }
        /// @brief Test if @a chunk is simulated
        bool isSimulated(const int2& chunk) const noexcept
        {
            return m_simulatedChunks.count(chunk) == 1;
        }
        /// @brief Test if @a chunk is owned
        bool isOwned(const int2& chunk) const noexcept
        {
            return m_owner.isOwned(chunk);
        }
        /// @brief Get @a chunk owner
        ClientId owner(const int2& chunk) const noexcept
        {
            return m_owner.owner(chunk);
        }
        /// @brief Get owned chunks
        const set<int2>& owned() const noexcept
        {
            return m_ownedChunks;
        }
        /// @brief Copy list of @a chunk observers to @a dest
        void observers(const int2& chunk, vector<ClientId>& dest) const noexcept
        {
            m_owner.observers(chunk, dest, observingDistancePad);
        }
        /// @brief Test if @a chunk is observed by @a client
        bool isObserved(const int2& chunk, ClientId client) const noexcept
        {
            return m_owner.isObserved(chunk, client, observingDistancePad);
        }
        /// @brief Test if @a chunk is observed by this client
        bool isObserved(const int2& chunk) const noexcept
        {
            return m_owner.isObserved(chunk, thisClient(), observingDistancePad);
        }

        /// @brief Debug draw
        void debugDraw(function<void(const char* text, const int2& chunk) > draw,
                       const int2& center,
                       uint distance);
    private:
        virtual void onPostloadChunksData(WorldRegion& region) override;
        virtual void onUnloadChunksData(WorldRegion& region) override;
    private:
        void observeChunk(const int2& chunk);
        void lostChunk(const int2& chunk);
        void ownChunk(const int2& chunk);
        void disownChunk(const int2& chunk);
        /// @brief Log info
        template <class ... Args>
        inline void log(const Args& ... args)
        {
            if (hasLogging)
                logDebug(args...);
        }
        /// @brief Try to simulate chunk
        void trySimulate(const int2& chunk, bool logFailure);
    public:
        /// @brief Difference between owning and observing distances
        const uint observingDistancePad;
        /// @brief All management logging
        const bool hasLogging;
    private:
        /// @brief All clients list
        set<ClientId> m_clients;

        /// @brief Loaded chunks (geometry is loaded)
        set<int2> m_loadedChunks;
        /// @brief Prepared chunks (objects is loaded)
        set<int2> m_preparedChunk;
        /// @brief Chunks need to be simulated
        set<int2> m_toSimulateChunks;
        /// @brief Simulated chunks
        set<int2> m_simulatedChunks;
        /// @brief Owned chunks
        set<int2> m_ownedChunks;

        /// @brief Observer
        WorldChunkObserver m_observer;
        /// @brief Owner
        WorldChunkOwner m_owner;
    };
    /// @}
}
