#include "pch.h"
#include "ls/world/WorldChunkManager.h"

using namespace ls;


// Chunk observer
WorldChunkObserver::WorldChunkObserver(uint maxDistance,
                                       uint outerDistancePad,
                                       uint borderSize,
                                       uint innerDistance)
    : maxDistance(maxDistance)
    , borderSize(borderSize)
    , storedDistance(maxDistance + borderSize)
    , outerDistancePad(outerDistancePad)
    , areaWidth(storedDistance * 2 + 1)
    , m_map(-int2((sint) storedDistance), 255, areaWidth, areaWidth)
{
    if (innerDistance)
        updateDistances(innerDistance);
}
void WorldChunkObserver::updateDistances(uint innerDistance)
{
    debug_assert(innerDistance > 0);
    m_innerDistance = innerDistance;
    m_outerDistance = innerDistance + outerDistancePad;
    debug_assert(m_outerDistance <= maxDistance, innerDistance);
}
WorldChunkObserver::WorldChunkObserver(WorldChunkObserver&& another) noexcept
    : onAddChunk(std::move(another.onAddChunk))
    , onRemoveChunk(std::move(another.onRemoveChunk))

    , maxDistance(another.maxDistance)
    , borderSize(another.borderSize)
    , storedDistance(another.storedDistance)
    , outerDistancePad(another.outerDistancePad)
    , areaWidth(another.areaWidth)

    , m_empty(another.m_empty)
    , m_position(another.m_position)
    , m_innerDistance(another.m_innerDistance)
    , m_outerDistance(another.m_outerDistance)

    , m_map(std::move(another.m_map))
{
}
WorldChunkObserver::~WorldChunkObserver()
{
    remove();
}
void WorldChunkObserver::shiftTo(const int2& target)
{
    // Move map
    auto leave = [this](ubyte& cell, const int2& index)
    {
        if (cell == 0)
            onRemoveChunk(index);
    };
    auto enter = [this](ubyte& cell, const int2& index)
    {
        cell = (ubyte) min<uint>(255, m_position.outsideFactor(index, m_innerDistance));
        if (cell == 0)
            onAddChunk(index);
    };
    m_map.slip(target - int2((sint) storedDistance), leave, enter);
}
void WorldChunkObserver::moveTo(const int2& target, uint innerDistance)
{
    if (!m_empty && m_position.isCenter(target) && innerDistance == 0)
        return;
    m_empty = false;
    if (innerDistance)
        updateDistances(innerDistance);

    // Update
    m_position.addCenter(target, outerDistancePad);
    auto update = [this](ubyte& cell, const int2& index)
    {
        // States
        bool wasEnabled = cell == 0;
        ubyte outsideFactor = (ubyte) min<uint>(255, m_position.outsideFactor(index, m_innerDistance));
        bool isEnabled = outsideFactor == 0;
        // Inform
        cell = outsideFactor;
        if (wasEnabled && !isEnabled)
            onRemoveChunk(index);
        if (!wasEnabled && isEnabled)
            onAddChunk(index);
    };
    m_map.process(update);
}
void WorldChunkObserver::move(const int2& target, uint innerDistance)
{
    shiftTo(target);
    moveTo(target, innerDistance);
}
void WorldChunkObserver::remove()
{
    m_map.process([this](ubyte& cell, const int2& index)
    {
        if (cell == 0)
            onRemoveChunk(index);
        cell = 255;
    });
    m_position = MultiPosition4{};
}
bool WorldChunkObserver::isAdded(const int2& chunk, uint pad) const noexcept
{
    return outsideDistance(chunk) <= pad;
}
uint WorldChunkObserver::radius() const noexcept
{
    return m_innerDistance;
}
ubyte WorldChunkObserver::outsideDistance(const int2& chunk) const noexcept
{
    if (!m_map.isInside(chunk))
        return false;
    return m_map.element(chunk);
}
array<int2, 4> WorldChunkObserver::centers() const noexcept
{
    array<int2, 4> dest;
    m_position.orderedCenters(dest);
    return dest;
}
string WorldChunkObserver::debugText() const
{
    string ret;
    int2 begin, end;
    m_map.bounds(begin, end);
    int2 chunk;
    for (chunk.y = end.y - 1; chunk.y >= begin.y; --chunk.y)
    {
        for (chunk.x = begin.x; chunk.x < end.x; ++chunk.x)
        {
            auto el = m_map.element(chunk);
            if (chunk == int2(0))
                ret += 'o';
            else if (el == 0)
                ret += '.';
            else if (el <= 9)
                ret += '0' + el;
            else
                ret += 'x';
        }
        ret += '\n';
    }
    return ret;
}


// Chunk owner
WorldChunkOwner::WorldChunkOwner(ClientId thisClient,
                                 uint maxDistance,
                                 uint outerDistancePad)
    : thisClient(thisClient)
    , maxDistance(maxDistance)
    , outerDistancePad(outerDistancePad)
    , areaWidth(maxDistance * 2 + 1)
    , m_owners(-int2((sint) maxDistance), NetInvalidClient, areaWidth, areaWidth)
{
}
WorldChunkOwner::~WorldChunkOwner()
{
    m_suppressFlag = true;
    m_clients.clear();
    m_owners.process([this](ClientId& cell, const int2& index)
    {
        if (cell == thisClient)
            onDisownChunk(index);
        cell = NetInvalidClient;
    });
}
WorldChunkObserver& WorldChunkOwner::getClient(ClientId client)
{
    auto iterClient = m_clients.find(client);
    if (iterClient == m_clients.end())
    {
        iterClient = m_clients.emplace(client,
                                       WorldChunkObserver(maxDistance, outerDistancePad)).first;
        iterClient->second.onAddChunk = bind(&WorldChunkOwner::addChunk,
                                             this, client, _1);
        iterClient->second.onRemoveChunk = bind(&WorldChunkOwner::removeChunk,
                                                this, client, _1);
        iterClient->second.shiftTo(m_offset);
    }
    return iterClient->second;
}
ClientId WorldChunkOwner::calcOwner(const int2& chunk) const
{
    for (auto& client : m_clients)
        if (client.second.isAdded(chunk))
            return client.first;
    return NetInvalidClient;
}
void WorldChunkOwner::shiftAll(const int2& chunk)
{
    m_suppressFlag = true;
    for (auto& client : m_clients)
        client.second.shiftTo(chunk);
    m_suppressFlag = false;

    // Shift priority
    auto leave = [this](ClientId& cell, const int2& index)
    {
        if (cell == thisClient)
            onDisownChunk(index);
    };
    auto enter = [this](ClientId& cell, const int2& index)
    {
        cell = calcOwner(index);
        if (cell == thisClient)
            onOwnChunk(index);
    };
    m_owners.slip(chunk - int2((sint) maxDistance), leave, enter);
}
void WorldChunkOwner::moveClient(ClientId client,
                                 const int2& chunk,
                                 uint distance)
{
    if (client == thisClient)
    {
        m_offset = chunk;
        shiftAll(chunk);
    }
    getClient(client).moveTo(chunk, distance);
}
void WorldChunkOwner::removeClient(ClientId client)
{
    m_clients.erase(client);
}
void WorldChunkOwner::addChunk(ClientId client, const int2& chunk)
{
    if (client == thisClient && onAddChunk)
        onAddChunk(chunk);
    updateOwninig(chunk);
}
void WorldChunkOwner::removeChunk(ClientId client, const int2& chunk)
{
    if (client == thisClient && onRemoveChunk)
        onRemoveChunk(chunk);
    updateOwninig(chunk);
}
void WorldChunkOwner::updateOwninig(const int2& chunk)
{
    if (m_suppressFlag)
        return;
    bool wasOwned = m_owners[chunk] == thisClient;
    m_owners[chunk] = calcOwner(chunk);
    bool isOwned = m_owners[chunk] == thisClient;
    if (wasOwned && !isOwned)
        onDisownChunk(chunk);
    else if (!wasOwned && isOwned)
        onOwnChunk(chunk);
}
void WorldChunkOwner::resetChunk(const int2& chunk)
{
    bool wasOwned = m_owners[chunk] == thisClient;
    if (wasOwned)
        onDisownChunk(chunk);
}
bool WorldChunkOwner::isObserved(const int2& chunk,
                                 ClientId client,
                                 uint pad) const noexcept
{
    auto iterClient = m_clients.find(client);
    if (iterClient == m_clients.end())
        return false;
    return iterClient->second.outsideDistance(chunk) <= pad;
}
uint WorldChunkOwner::observers(const int2& chunk,
                                vector<ClientId>& dest,
                                uint pad) const
{
    uint startIndex = (uint) dest.size();

    for (auto& client : m_clients)
        if (client.second.isAdded(chunk, pad))
            dest.push_back(client.first);

    return (uint) dest.size() - startIndex;
}
MultiPosition4 WorldChunkOwner::position(ClientId client) const
{
    auto iterClient = m_clients.find(client);
    debug_assert(iterClient != m_clients.end(), client);

    return iterClient->second.position();
}
uint WorldChunkOwner::radius(ClientId client) const
{
    auto iterClient = m_clients.find(client);
    debug_assert(iterClient != m_clients.end(), client);

    return iterClient->second.radius();
}


// Chunk manager
WorldChunkManager::WorldChunkManager(ClientId thisClient,
                                     uint maxDistance,
                                     uint observingDistancePad,
                                     uint outerDistancePad,
                                     bool logAll)
    : Nameable("ChunkManager")
    , observingDistancePad(observingDistancePad)
    , hasLogging(logAll)
    , m_observer(maxDistance, outerDistancePad)
    , m_owner(thisClient, maxDistance, outerDistancePad)
{
    m_observer.onAddChunk = bind(&WorldChunkManager::observeChunk, this, _1);
    m_observer.onRemoveChunk = bind(&WorldChunkManager::lostChunk, this, _1);
    m_owner.onOwnChunk = bind(&WorldChunkManager::ownChunk, this, _1);
    m_owner.onDisownChunk = bind(&WorldChunkManager::disownChunk, this, _1);
}
WorldChunkManager::~WorldChunkManager()
{
}
void WorldChunkManager::moveClient(ClientId client,
                                   const int2& chunk,
                                   uint distance)
{
    if (client == thisClient())
        m_observer.move(chunk, distance + observingDistancePad);
    m_owner.moveClient(client, chunk, distance);
    m_clients.insert(client);
}
void WorldChunkManager::removeClient(ClientId client)
{
    if (client == thisClient())
        m_observer.remove();
    m_owner.removeClient(client);
    m_clients.erase(client);
}
void WorldChunkManager::trySimulate(const int2& chunk, bool logFailure)
{
    bool isLoaded = m_loadedChunks.count(chunk) == 1;
    bool isPrepared = m_preparedChunk.count(chunk) == 1;
    bool needSimulate = m_toSimulateChunks.count(chunk) == 1;

    if (!isLoaded && logFailure)
        log("Chunk ", chunk, " world data is still not loaded, waiting...");
    else if (!isPrepared && logFailure)
        log("Chunk ", chunk, " object data is still not loaded, waiting...");
    else if (!needSimulate && logFailure)
        (void) "Just nothing to do";
    else if (isLoaded && isPrepared && needSimulate)
    {
        m_toSimulateChunks.erase(chunk);
        m_simulatedChunks.insert(chunk);
        m_preparedChunk.erase(chunk);

        log("Chunk ", chunk, " entered the simulation");
        onEnter(chunk);
    }
}
void WorldChunkManager::prepare(const int2& chunk)
{
    log("Chunk ", chunk, " is prepared to simulation");

    m_preparedChunk.insert(chunk);
    trySimulate(chunk, true);
}
void WorldChunkManager::observeChunk(const int2& chunk)
{
    log("Chunk ", chunk, " is observed");

    m_toSimulateChunks.insert(chunk);
    trySimulate(chunk, true);
}
void WorldChunkManager::lostChunk(const int2& chunk)
{
    if (m_simulatedChunks.erase(chunk))
    {
        if (m_toSimulateChunks.count(chunk) == 1)
            logWarning("Incorrect chunk ", chunk, " is lost");
        else
        {
            log("Chunk ", chunk, " lost the simulation");
            onLeave(chunk);
        }
    }
    else if (m_toSimulateChunks.erase(chunk) == 1)
    {
        log("Non-simulated chunk ", chunk, " is lost");
    }
    else
    {
        logWarning("Incorrect chunk ", chunk, " is lost");
    }
}
void WorldChunkManager::ownChunk(const int2& chunk)
{
    log("Chunk ", chunk, " is owned");

    if (!m_ownedChunks.insert(chunk).second)
    {
        logWarning("Owned chunk ", chunk, " is owned");
    }
}
void WorldChunkManager::disownChunk(const int2& chunk)
{
    log("Chunk ", chunk, " is disowned");

    if (m_ownedChunks.erase(chunk) != 1)
    {
        logWarning("Non-owned chunk ", chunk, " is disowned");
    }
}
void WorldChunkManager::onPostloadChunksData(WorldRegion& region)
{
    for (WorldRegion::Chunk& chunk : region.chunks())
    {
        int2 index = chunk.index;
        bool success = m_loadedChunks.insert(index).second;
        debug_assert(success, index);
        trySimulate(index, false);
    }
}
void WorldChunkManager::onUnloadChunksData(WorldRegion& region)
{
    for (WorldRegion::Chunk& chunk : region.chunks())
    {
        int2 index = chunk.index;
        bool success = m_loadedChunks.erase(index) == 1;
        debug_assert(success, index);

        if (m_toSimulateChunks.erase(index) + m_simulatedChunks.erase(index) != 0)
        {
            logWarning("Simulated chunk ", index, " is unloaded");
        }
    }
}
void WorldChunkManager::debugDraw(function<void(const char* text, const int2& chunk) > draw,
                                  const int2& center,
                                  uint distance)
{
    int2 begin = center - int2(distance);
    int2 end = center + int2(distance);

    int2 chunk;
    char buf[64];
    string text;
    vector<ClientId> observers;

    for (chunk.x = begin.x; chunk.x <= end.x; ++chunk.x)
    {
        for (chunk.y = begin.y; chunk.y <= end.y; ++chunk.y)
        {
            text.clear();
            observers.clear();

            bool isCenter = m_observer.position().isCenter(chunk);
            ClientId owner = m_owner.owner(chunk);
            m_owner.observers(chunk, observers, observingDistancePad);

            bool isSimulated = m_simulatedChunks.count(chunk) == 1;
            bool isToSimulate = m_toSimulateChunks.count(chunk) == 1;
            bool isPrepared = m_preparedChunk.count(chunk) == 1;
            bool isOwned = m_owner.isOwned(chunk);

            char tag = ' ';
            if (isSimulated && !isToSimulate && isOwned)
                tag = 'O'; // Simulated and owned
            else if (isSimulated && !isToSimulate && !isOwned)
                tag = 'S'; // Simulated
            else if (!isSimulated && isToSimulate)
                tag = 'I'; // Waiting for simulation
            else if (!isSimulated && !isToSimulate && !isOwned && isPrepared)
                tag = '+'; // Loaded
            else if (!isSimulated && !isToSimulate && !isOwned && !isPrepared)
                tag = ' '; // Empty
            else
                tag = '!'; // Error

            char tag1 = isCenter ? '<' : '[', tag2 = isCenter ? '>' : ']';
            if (owner == NetInvalidClient)
                sprintf(buf, "(%d,%d)%cX%c%c\n", chunk.x, chunk.y, tag1, tag2, tag);
            else
                sprintf(buf, "(%d,%d)%c%u%c%c\n", chunk.x, chunk.y, tag1, owner, tag2, tag);
            text += buf;

            bool isFirst = true;
            ClientId counter = 0;
            for (auto client : observers)
            {
                sprintf(buf, "%s%u", isFirst ? "" : ", ", client);
                text += buf;
                ++counter;
                isFirst = false;
                if (!(counter % 7))
                {
                    text += "\n";
                    isFirst = true;
                }
            }

            draw(text.c_str(), chunk);
        }
    }
}
