#include "pch.h"
#include "ls/engine/server/Server.h"
#include "ls/engine/app/Config.h"
#include "ls/network/Web.h"
#include "ls/scene/ObjectsManager.h"
#include "ls/scene/ObjectsFactory.h"
#include "ls/world/WorldChunkManager.h"

using namespace ls;
// Main
Server::Server(Web& web, ObjectsFactory& factory)
    : Nameable("Server")

    , logTime(*config().server().logTime)
    , logActions(*config().server().logActions)
    , logInfoRaycasts(*config().server().logInfoRaycast)
    , logRealRaycasts(*config().server().logRealRaycast)
    , chunkWidth(*config().world().chunkWidth)
    , regionWidth(*config().world().regionInChunks)

    , m_web(web)
    , m_config(config().server())

    , m_timeUpdateTimer(*config().server().timeUpdPeriod, false)
    , m_pingUpdateTimer(*config().server().pingUpdPeriod, false)
    , m_actionUpdateTimer(*config().server().actionTick, false)
    , m_raycastInfoUpdateTimer(*config().server().raycastInfoTick, false)
    , m_raycastRealUpdateTimer(*config().server().raycastRealDrop, false)

    , m_storage(make_unique<ObjectsManager>(chunkWidth, regionWidth, true))
    , m_factory(factory)

    , temp(new Temp())

    , m_numRaycastRegions((sint) *config().server().numRaycastRegion)
    , m_raycastRegionWidth(*config().server().raycastRegion)
    , m_infoRaycastLifetime(*config().server().regionLifetime / *config().server().raycastInfoTick)
{
}
Server::~Server()
{
}


// Simulation
void Server::start()
{
    debug_assert(!isStarted());

    m_startedFlag = true;
    m_time = *config().server().initialTime;
    m_actionTime = m_time;

    // Start all
    while (!m_inactiveNodes.empty())
        startNode(m_inactiveNodes.begin()->first);
}
void Server::startNode(ClientId id)
{
    debug_assert(isStarted());

    // Activate
    auto inactiveNode = m_inactiveNodes.find(id);
    debug_assert(inactiveNode != m_inactiveNodes.end(), id);
    ActiveNode& node = m_activeNodes.emplace(id, ActiveNode(inactiveNode->second))
        .first->second;
    m_inactiveNodes.erase(inactiveNode);

    // Start
    StartMessage startupMessage(m_time,
                                *config().server().framePeriod,
                                *config().server().actionPeriod,
                                *config().server().actionTick,
                                *config().server().controlLag,

                                *config().server().timeMismatch,
                                *config().world().chunkWidth,
                                *config().server().maxSimDistance,
                                *config().server().simPadDistance,

                                *config().server().trustActors,
                                *config().server().trustRaycasts);
    m_web.sendTcp(id, send(startupMessage));

    // Begin observe
    node.chunksObserver = make_shared<WorldChunkObserver>(*config().server().maxSimDistance);
    node.chunksObserver->onAddChunk = [&node](int2 const& chunk)
    {
        node.chunkToUpload.insert(chunk);
    };
    node.chunksObserver->onRemoveChunk = [&node](int2 const& chunk)
    {
        node.chunkToUpload.erase(chunk);
    };

    // Move this
    node.chunksObserver->move(node.lastChunk,
                              node.simDistance + *config().server().simPadDistance);
    m_web.sendTcp(id, send(MoveReportMessage(node.lastChunk, node.simDistance, id)));

    // Move others
    for (auto& iterOtherNode : m_activeNodes)
    {
        ClientId otherId = iterOtherNode.first;
        if (otherId == id)
            continue;
        ActiveNode& otherNode = iterOtherNode.second;
        array<int2, 4> otherPlace = otherNode.chunksObserver->centers();

        for (int i = 0; i < 4; ++i)
            m_web.sendTcp(id, send(MoveReportMessage(otherPlace[i], otherNode.simDistance, otherId)));
        m_web.sendTcp(otherId, send(MoveReportMessage(node.lastChunk, node.simDistance, id)));
    }

    // HACK init actor
    node.actorUptime = time();
    node.actor = m_storage->addObject();

    ObjectPrototype& actorProto = m_factory.findPrototype("player");
    m_factory.addParameter("position", double3(10.0, 2.0, 10.0) + double3(random(-2.0, 2.0), 0, random(-2.0, 2.0)));
    ObjectState actorState;
    m_factory.build(actorState, node.actor->id(), actorProto);
    node.actor->reconstruct(actorState);

    // HACK send info
    ServerReportMessage thisInfo(0);
    encode<Encoding::Short>(thisInfo.tail, id);
    encode<Encoding::Int>(thisInfo.tail, node.actor->id());
    encode<Encoding::VectorFixed4>(thisInfo.tail, node.actor->position());
    m_web.sendTcp(id, send<ServerReportMessage>(thisInfo));
    for (auto& iterNode : m_activeNodes)
    {
        ClientId otherId = iterNode.first;
        if (otherId == id)
            continue;
        ActiveNode& otherNode = iterNode.second;

        ServerReportMessage otherInfo(0);
        encode<Encoding::Short>(thisInfo.tail, otherId);
        encode<Encoding::Int>(thisInfo.tail, otherNode.actor->id());
        encode<Encoding::VectorFixed4>(thisInfo.tail, otherNode.actor->position());
        m_web.sendTcp(id, send<ServerReportMessage>(otherInfo));
        m_web.sendTcp(otherId, send<ServerReportMessage>(thisInfo));
    }
}
void Server::stop()
{
    debug_assert(isStarted());

    // Stop all
    while (!m_activeNodes.empty())
        stopNode(m_activeNodes.begin()->first);

    m_startedFlag = false;
    m_time = 0;
}
void Server::stopNode(ClientId id)
{
    debug_assert(isStarted());

    // Deactivate
    auto iterNode = m_activeNodes.find(id);
    debug_assert(iterNode != m_activeNodes.end(), id);
    m_inactiveNodes.emplace(id, static_cast<InactiveNode> (iterNode->second));
    m_activeNodes.erase(iterNode);

    // Inform
    m_web.sendTcp(id, send(StopMessage{}));

}


// Gets
Server::ActiveNode* Server::findActiveNode(ClientId id)
{
    return findOrDefault(m_activeNodes, id);
}
void Server::addNode(ClientId id)
{
    m_inactiveNodes.emplace(id, InactiveNode(id));

    if (isStarted())
        startNode(id);

    // Log
    logInfo("[", id, "] Added");
}
void Server::removeNode(ClientId id)
{
    if (isStarted())
        stopNode(id);

    m_inactiveNodes.erase(id);

    // Log
    logInfo("[", id, "] Removed");
}


// Messages
bool Server::processTcpMessgae(ClientId client,
                               NetworkMessage const& message)
{
    switch (message.id())
    {
    case MessageCode::MoveRequest:
        processMoveRequest(client, receive<MoveRequestMessage>(message));
        break;
    case MessageCode::ClientRequest:
        processClientRequest(client, receive<ClientRequestMessage>(message));
        break;
    case MessageCode::ActionRequest:
        processActionRequest(client, receive<ActionRequestMessage>(message));
        break;
    case MessageCode::RaycastRequest:
        processRaycastRequest(client, receive<RaycastRequestMessage>(message));
        break;
    case MessageCode::RaycastOrder:
        processRaycastOrder(client, receive<RaycastOrderMessage>(message));
        break;
    case MessageCode::CorrectionData:
        processCorrectionData(client, receive<CorrectionDataMessage>(message));
        break;
    case MessageCode::DynamicData:
        processDynamicData(client, receive<DynamicDataMessage>(message));
        break;
    case MessageCode::RaytestHitSure:
        processRaytestHit(client, receive<RaytestHitSureMessage>(message), false);
        break;
    case MessageCode::RaytestHitWeak:
        processRaytestHit(client, receive<RaytestHitWeakMessage>(message), true);
        break;
    case MessageCode::RaytestDullSure:
        processRaytestDull(client, receive<RaytestDullSureMessage>(message), false);
        break;
    case MessageCode::RaytestDullWeak:
        processRaytestDull(client, receive<RaytestDullWeakMessage>(message), true);
        break;
    case MessageCode::RaytestMiss:
        processRaytestMiss(client, receive<RaytestMissMessage>(message));
        break;
    default:
        return false;
    }
    return true;
}
bool Server::processUdpMessgae(ClientId client,
                               NetworkMessage const& message)
{
    switch (message.id())
    {
    case MessageCode::ActionRequest:
        processActionRequest(client, receive<ActionRequestMessage>(message));
        break;
    case MessageCode::CorrectionData:
        processCorrectionData(client, receive<CorrectionDataMessage>(message));
        break;
    case MessageCode::DynamicData:
        processDynamicData(client, receive<DynamicDataMessage>(message));
        break;
    default:
        return false;
    }
    return true;
}
void Server::processMoveRequest(ClientId client,
                                MoveRequestMessage const& message)
{
    ActiveNode* node = findActiveNode(client);
    if (!node)
    {
        logWarning("[", client, "] is inactive and sent [MoveRequest] message");
        return;
    }

    int2 chunk = *message.h.chunk;
    ushort dist = *message.h.dist;

    uint distancePad = *config().server().simPadDistance;

    if (node->lastChunk == chunk)
        return;

    // Move
    node->simDistance = dist;
    node->lastChunk = chunk;
    node->raycastRegion = computeRaycastRegion(chunk);
    node->chunksObserver->move(chunk, node->simDistance + distancePad);

    // Inform
    NetworkMessage report = send(MoveReportMessage(chunk, dist, client));
    for (auto& iterNode : m_activeNodes)
    {
        ClientId nodeId = iterNode.first;
        m_web.sendTcp(nodeId, report);
    }

    logInfo("[", client, "] Moved to ", chunk, ":", dist);
}
void Server::processClientRequest(ClientId client,
                                  ClientRequestMessage&& message)
{
    uint type = *message.h.requestType;
    switch (type)
    {
    case 0:
    {
        /*auto position = message.read<float3>();
        auto velocity = message.read<float3>();

        auto object = m_storage->allocateObject(m_factory.findPrototype("Shitblock"));
        m_factory.resetParameter();
        m_factory.addParameter("position", position);
        m_factory.addParameter("velocity", velocity);
        if (!m_factory.build(*object))
        {
            m_storage->removeObject(object);
            logWarning("[", client, "] Cannot build temporary object");
            return;
        }

        m_addedTinyObjects.insert(object);
        logDebug("[", client, "] Request is processed : Temp object at ", position);*/
        break;
    }
    default:
        logWarning("[", client, "] Request [", type, "] is ignored");
        break;
    }
}
void Server::processActionRequest(ClientId client,
                                  ActionRequestMessage const& message)
{
    ActiveNode* node = findActiveNode(client);
    if (!node)
    {
        logWarning("[", client, "] is inactive and sent [ActionRequest] message");
        return;
    }
    if (!node->actor)
    {
        logWarning("[", client, "] has no active actor and sent [ActionRequest] message");
        return;
    }

    ActorMovement movement = message.h.movement;
    DiscreteTime actionTime = *message.h.time;

    // Delay
    sint tick = (sint) *config().server().actionTick;
    DiscreteTime nextActionTime = m_actionTime + tick;
    sint delay = (sint) (nextActionTime - actionTime) / tick;
    *movement.delay += (sbyte) clamp(delay, -128, 127);

    // Add
    node->movementsMap.emplace(actionTime, movement);
    node->lastMovement = (--node->movementsMap.end())->second;
    traceAction("[", client, "] action is added "
                ": MessageTime = ", actionTime,
                "; NextActionTime = ", nextActionTime,
                "; Delay = ", delay * tick,
                "; Movement = ", *movement.type,
                "; Position = ", *movement.position);

    // Raycast
    if (message.h.hasRaycast)
    {
        addRaycastInfo(client, *node->actor,
                       *message.h.raycastSource, *message.h.raycastType,
                       *message.h.raycastDirection,
                       *message.h.time + *message.h.raycastTime);
    }
}
void Server::processRaycastRequest(ClientId client,
                                   RaycastRequestMessage const& message)
{
    // Check client
    ActiveNode* node = findActiveNode(client);
    if (!node)
    {
        logWarning("[", client, "] is inactive and sent [RaycastRequest] message");
        return;
    }
    if (!node->actor)
    {
        logWarning("[", client, "] has no active actor and sent [RaycastRequest] message");
        return;
    }

    // Generate info
    RaycastInfo ray;
    ray.invoker = client;
    ray.time = *message.h.time;
    ray.object = node->actor->id();
    ray.source = *message.h.source;
    ray.type = *message.h.type;
    ray.direction = *message.h.direction;

    addRaycast(ray);

    // Log
    traceRealRaycast("[", client, "] raycast is requested "
                     ": MessageTime = ", ray.time,
                     "; Object = ", ray.object,
                     "; Source = ", ray.source,
                     "; Type = ", ray.type,
                     "; Direction = ", ray.direction);
}
void Server::processRaycastOrder(ClientId client,
                                 RaycastOrderMessage const& message)
{
    // Check client
    ActiveNode* node = findActiveNode(client);
    if (!node)
    {
        logWarning("[", client, "] is inactive and sent [RaycastOrder] message");
        return;
    }
    if (!*m_config.trustRaycasts)
    {
        logWarning("[", client, "] sent [RaycastOrder] message, but it is not allowed");
        return;
    }

    // Check object
    ObjectId hitObjectId;
    ubyte hitType;
    message.get(hitObjectId, hitType);
    ObjectIterator object = m_storage->findObject(hitObjectId);
    if (!object)
    {
        logWarning("[", client, "] sent [RaycastOrder] about unknown object "
                       ": Object = ", hitObjectId,
                       "; HitType = ", hitType,
                       "; CasterClientId = ", client);
        return;
    }

    // Apply
    raycastHit(*object, hitType, client);
}
void Server::processCorrectionData(ClientId client,
                                   CorrectionDataMessage&& message)
{
    // No rights
    if (!m_web.isSuper(client))
    {
        logWarning("Basic client [", client, "] sent [CorrectionData] message!");
        return;
    }

    // Process
    ClientId clientId;
    DiscreteTime time;
    double3 position;
    while (message.read(clientId, time, position))
    {
        ActiveNode* clientPtr = findActiveNode(clientId);
        if (!clientPtr)
        {
            logDebug("[", client, "] unknown corrected client [", clientId, "]");
            continue;
        }

        // Move
        bool uptime = time > clientPtr->actorUptime;
        if (uptime)
        {
            clientPtr->actor->position(position);
            clientPtr->actorUptime = time;
        }
    }
}
void Server::processDynamicData(ClientId client,
                                DynamicDataMessage const& message)
{
    // Ignore server messages, warn if received from client
    if (!message.isWeak())
    {
        if (client != NetRootClient)
            logWarning("Client [", client, "] sent strong [DynamicData] message!");
        return;
    }
    // No rights
    if (!m_web.isSuper(client))
    {
        logWarning("Basic client [", client, "] sent [DynamicData] message!");
        return;
    }
    // Process
    auto reader = [this, client](ObjectState const& state)
    {
        ObjectIterator object = m_storage->findObject(state.id());
        if (!object || object->type() != state.type())
        {
            logDebug("(!) Client [", client, "] sent dynamics about "
                         "unknown object [", state.id(), ":", state.type(), "]");
            return;
        }
        if (!object || object->type() != state.type())
        {
            logDebug("(!) Client [", client, "] dynamics object "
                         "[", state.id(), ":", state.type(), "] type mismatch "
                         ": real type is [", object->type(), "]");
            return;
        }
        object->inject(state);
    };
    readObjectInfo(message, m_factory, reader);
}


// Raycast
void Server::addRaycast(RaycastInfo const& ray)
{
    // Find caster
    ObjectIterator object = m_storage->findObject(ray.object);
    if (!object)
    {
        logWarning("[", ray.invoker, "] tried to invoke raycast "
                   "from unknown object [", ray.object, "]");
        return;
    }

    // Get chunk and region
    int2 chunkIndex = object->chunk();
    int2 regionIndex = computeRaycastRegion(chunkIndex);
    RaytestRequest request(ray, chunkIndex);

    // Send
    NetworkMessage message = send(RaytestRequestMessage(ray));
    for (auto& iterNode : m_activeNodes)
    {
        // Super only
        ClientId clientId = iterNode.first;
        ActiveNode& client = iterNode.second;
        if (!m_web.isSuper(clientId))
            continue;

        // Check distance
        int2 delta = abs(client.raycastRegion - regionIndex);
        if (max(delta.x, delta.y) > m_numRaycastRegions)
            continue;

        // Send
        m_web.sendTcp(clientId, message);
        ++request.numSupers;
    }

    // Save
    RaycastKey key(request.ray);
    m_realRaycasts.emplace(key, request);

    traceRealRaycast("Raycast ", key, " is queued");
}
void Server::processRaytestHit(ClientId client,
                               RaytestHitBase const& message,
                               bool isWeak)
{
    // No rights
    if (!m_web.isSuper(client))
    {
        logWarning("Basic client [", client, "] sent [RaytestHit] message!");
        return;
    }

    // Drop unknwon
    RaycastKey key(*message.h.caster, *message.h.time);
    auto iterRequest = m_realRaycasts.find(key);
    if (iterRequest == m_realRaycasts.end())
        return;

    // Remove super
    RaytestRequest& request = iterRequest->second;
    if (request.numSupers > 0)
        --request.numSupers;

    // Fail
    ObjectId hitObjectId;
    ubyte hitType;
    message.get(hitObjectId, hitType);
    ObjectIterator object = m_storage->findObject(hitObjectId);
    if (!object)
    {
        logWarning("[", client, "] Sent [RaytestHitSure] about unknown object "
                       ": Object = ", hitObjectId,
                       "; HitType = ", hitType,
                       "; CasterClientId = ", key.invoker);
        return;
    }

    // Log
    traceRealRaycast("[", client, "] raycast ", key, " hit object "
                     ": Object = ", hitObjectId,
                     "; HitType = ", hitType,
                     "; Chunk = ", request.hitChunk,
                     "; IsWeak = ", isWeak);

    // Process
    request.updateRaycastHit(object, hitType, object->chunk());
    if (!isWeak || request.numSupers == 0)
    {
        acceptRayhit(request);
        m_realRaycasts.erase(iterRequest);
    }
}
void Server::processRaytestDull(ClientId client,
                                RaytestDullBase const& message,
                                bool isWeak)
{
    // No rights
    if (!m_web.isSuper(client))
    {
        logWarning("Basic client [", client, "] sent [RaytestDull] message!");
        return;
    }

    // Drop unknwon
    RaycastKey key(*message.h.caster, *message.h.time);
    auto iterRequest = m_realRaycasts.find(key);
    if (iterRequest == m_realRaycasts.end())
        return;

    // Remove super
    RaytestRequest& request = iterRequest->second;
    if (request.numSupers > 0)
        --request.numSupers;

    // Log
    traceRealRaycast("[", client, "] raycast ", key, " hit static geometry "
                     ": Chunk = ", request.hitChunk,
                     "; IsWeak = ", isWeak);

    // Process
    request.updateRaycastHit(ObjectIterator(), 0, *message.h.chunk);
    if (!isWeak || request.numSupers == 0)
    {
        acceptRayhit(request);
        m_realRaycasts.erase(iterRequest);
    }
}
void Server::processRaytestMiss(ClientId client,
                                RaytestMissMessage const& message)
{
    // No rights
    if (!m_web.isSuper(client))
    {
        logWarning("Basic client [", client, "] sent [RaytestMiss] message!");
        return;
    }

    // Drop unknwon
    RaycastKey key(*message.h.caster, *message.h.time);
    auto iterRequest = m_realRaycasts.find(key);
    if (iterRequest == m_realRaycasts.end())
        return;

    // Remove super
    RaytestRequest& request = iterRequest->second;
    if (request.numSupers > 0)
        --request.numSupers;

    // Log
    traceRealRaycast("[", client, "] raycast ", key, " miss");

    // Process
    if (request.numSupers == 0)
    {
        acceptRayhit(request);
        m_realRaycasts.erase(iterRequest);
    }
}
void Server::acceptRayhit(RaytestRequest const& request)
{
    if (!request.hit)
    {
        raycastMiss(request.ray);
    }
    else if (!request.hitObject)
    {
        raycastDull(request.hitChunk, request.ray);
    }
    else
    {
        raycastHit(*request.hitObject, request.hitType, request.ray.invoker);
    }
}
void Server::raycastMiss(RaycastInfo const& ray)
{
    logNotice("Client ", ray.invoker, " hit the sky!");
}
void Server::raycastDull(int2 const& chunk, RaycastInfo const& ray)
{
    logNotice("Client ", ray.invoker, " hit chunk ", chunk, "!");
}
void Server::raycastHit(ObjectInstance& hitObject, ubyte hitType, ClientId invoker)
{
    logNotice("Client ", invoker, " hit object ", hitObject.id(), " (", hitType, " hits)!");
}



// Update
void Server::update(DeltaTime deltaTime)
{
    if (!isStarted())
        return;

    m_time += deltaTime;
    m_timeUpdateTimer.advance(deltaTime);
    m_pingUpdateTimer.advance(deltaTime);
    m_actionUpdateTimer.advance(deltaTime);
    m_raycastInfoUpdateTimer.advance(deltaTime);
    m_raycastRealUpdateTimer.advance(deltaTime);

    if (m_timeUpdateTimer.alarm())
        updateTime();

    if (m_pingUpdateTimer.alarm())
        updatePings();

    if (m_actionUpdateTimer.alarm())
        updateActions();

    if (m_raycastInfoUpdateTimer.alarm())
        updateRaycastInfo();

    if (m_raycastRealUpdateTimer.alarm())
        updateRaycastProofs();
}
void Server::updateTime()
{
    for (auto& iterNode : m_activeNodes)
    {
        ClientId nodeId = iterNode.first;
        if (nodeId == NetRootClient)
            continue;
        DeltaTime ping = m_web.ping(nodeId);
        if (ping)
        {
            m_web.sendUdp(nodeId, send(TimeMessage(m_time, ping)));

            // Log
            if (logTime)
            {
                logTrace("[", nodeId, "] Time update "
                             ": Time = ", m_time,
                             "; Ping = ", ping);
            }
        }
    }
}
void Server::updatePings()
{
    // Prepare
    uint beginIndex = (uint) m_pool.size();
    PingDataMessage message = PingDataMessage{};
    for (auto& iterNode : m_activeNodes)
    {
        ClientId nodeId = iterNode.first;
        DeltaTime ping = m_web.ping(nodeId);
        if (!message.write(nodeId, ping))
        {
            m_pool.push_back(send(message));
            message = PingDataMessage{};
            message.write(nodeId, ping);
        }
    }
    if (message.num() > 0)
        m_pool.push_back(send(message));
    uint endIndex = (uint) m_pool.size();

    // Send
    for (auto& iterNode : m_activeNodes)
    {
        ClientId nodeId = iterNode.first;
        for (uint iterMessage = beginIndex; iterMessage < endIndex; ++iterMessage)
            m_web.sendUdp(nodeId, m_pool[iterMessage]);
    }

    // Clear
    m_pool.erase(m_pool.begin() + beginIndex, m_pool.end());
}
void Server::updateActions()
{
    m_pool.clear();

    m_actionTime = m_actionUpdateTimer.floor(m_time);

    uint numUploaded = 0;
    uint maxUploaded = m_activeNodes.size();
    uint actionTick = *config().server().actionTick;

    CinematicDataMessage dest = CinematicDataMessage(m_actionTime);
    auto put = [&dest, this](ClientId id, ActorMovement const& movement)
    {
        if (!dest.write(id, movement))
        {
            m_pool.push_back(send(dest));
            dest = CinematicDataMessage(m_actionTime);
            dest.write(id, movement);
        }
    };

    // Prepare
    ClientId onlyClient = NetInvalidClient;
    for (auto& iterNode : m_activeNodes)
    {
        ClientId nodeId = iterNode.first;
        ActiveNode& node = iterNode.second;
        for (auto& iterMovement : node.movementsMap)
        {
            DiscreteTime time = iterMovement.first;
            ActorMovement& movement = iterMovement.second;
            if (time + node.lastPing < m_actionTime)
            {
                traceAction("[", nodeId, "] out-of-time movement ignored ",
                            ": ActionTime = ", m_actionTime,
                            "; MessageTime = ", time);
                continue;
            }

            // The only
            if (onlyClient == NetInvalidClient)
                onlyClient = nodeId;
            else if (onlyClient != nodeId)
                onlyClient = NetBroadcastClient;

            // Send this
            put(nodeId, movement);
            ++numUploaded;

            // Log
            sint delay = *movement.delay * actionTick;
            traceAction("[", nodeId, "] movement uploaded "
                        ": MessageTime = ", time,
                        "; Delay = ", delay,
                        "; MovementType = ", *movement.type);
        }
        node.movementsMap.clear();
    }

    // Prepare last
    if (numUploaded)
    {
        m_pool.push_back(send(dest));
    }

    // Send
    for (auto& iterNode : m_activeNodes)
    {
        ClientId clientId = iterNode.first;
        if (onlyClient != clientId)
        {
            for (NetworkMessage& message : m_pool)
                m_web.sendUdp(clientId, message);
        }
    }

    // Clear
    m_pool.clear();

    // Log
    traceAction("ActionTime = ", m_actionTime,
                "; ", numUploaded, "/", maxUploaded, " clients are uploaded"
                "; CurrentTime = ", m_time);
}
void Server::updateRaycastInfo()
{
    m_raycastInfoTime = m_raycastInfoUpdateTimer.floor(m_time);

    // Finalize
    for (auto& iterRegion : m_infoRaycasts)
    {
        RaycastInfoRegion& region = iterRegion.second;
        if (region.message.numRays() > 0)
        {
            *region.message.h.time = m_raycastInfoTime;
            region.pool.push_back(send(region.message));
        }
    }

    // Send for each
    for (auto& iterNode : m_activeNodes)
    {
        ClientId clientId = iterNode.first;
        ObjectIterator& actorObject = iterNode.second.actor;
        ObjectId actorObjectId = actorObject ? actorObject->id() : InvalidObjectId;
        int2 center = iterNode.second.raycastRegion;

        // Send regions
        int2 index;
        sint num = m_numRaycastRegions;
        for (index.x = center.x - num; index.x <= center.x + num; ++index.x)
        {
            for (index.y = center.y - num; index.y <= center.y + num; ++index.y)
            {
                // No region
                auto iterRegion = m_infoRaycasts.find(index);
                if (iterRegion == m_infoRaycasts.end())
                    continue;
                RaycastInfoRegion& region = iterRegion->second;

                // Forever alone
                if (region.onlyCaster == actorObjectId)
                    continue;

                // Send
                for (NetworkMessage& message : region.pool)
                    m_web.sendUdp(clientId, message);
            }
        }
    }

    // Clear
    DiscreteTime nextTime = m_raycastInfoTime + m_raycastInfoUpdateTimer.period();
    uint numNotEmpty = 0;
    for (auto& iterRegion : m_infoRaycasts)
    {
        RaycastInfoRegion& region = iterRegion.second;
        if (region.pool.empty())
            ++region.unused;
        else
            ++numNotEmpty;
        region.pool.clear();
        region.message = RaycastDataMessage(nextTime);
        region.onlyCaster = InvalidObjectId;
    }

    // Drop unused
    auto dropUnused = [this](pair<int2 const, RaycastInfoRegion>& region)
    {
        return region.second.unused > m_infoRaycastLifetime;
    };
    erase_if(m_infoRaycasts, dropUnused);

    // Log
    if (numNotEmpty > 0)
    {
        traceInfoRaycast(numNotEmpty, " regions are sent "
                         ": Time = ", m_raycastInfoTime);
    }
}
void Server::addRaycastInfo(ClientId invoker, ObjectInstance& caster,
                            ubyte castSource, ubyte castType,
                            float3 const& direction, DiscreteTime raycastTime)
{
    // Compute raycast parameters
    ObjectId objectId = caster.id();
    DiscreteTime refTime = m_raycastInfoTime + m_raycastInfoUpdateTimer.period();
    int2 regionIndex = computeRaycastRegion(caster.position());

    // Add
    RaycastInfoRegion& region = m_infoRaycasts[regionIndex];
    if (!region.message.write(direction, objectId, castSource, castType, raycastTime, refTime))
    {
        *region.message.h.time = refTime;
        region.pool.push_back(send(region.message));
        region.message = RaycastDataMessage(refTime);
        region.message.write(direction, objectId, castSource, castType, raycastTime, refTime);
    }

    // Only caster
    if (region.onlyCaster == InvalidObjectId)
        region.onlyCaster = objectId;
    else if (region.onlyCaster != objectId)
        region.onlyCaster = InvalidObjectId - 1;

    // Log
    traceInfoRaycast("[", invoker, "] info raycast is added "
                     ": RaycastTime = ", raycastTime,
                     "; Object = ", objectId,
                     "; CastSource = ", castSource,
                     "; CastType = ", castType,
                     "; Direction = ", direction);
}
void Server::updateRaycastProofs()
{
    auto cleaner = [](pair<RaycastKey const, RaytestRequest>& request)
    {
        return request.second.shouldDestroy;
    };
    erase_if(m_realRaycasts, cleaner);
    for (auto& iterRequest : m_realRaycasts)
        iterRequest.second.shouldDestroy = true;
}


// Flush
void Server::flush()
{
    flushAddedObjects();
    flushChunks();

    debug_assert(m_pool.empty());
}
void Server::flushAddedObjects()
{
    if (m_addedTinyObjects.empty() && m_addedFatObjects.empty())
        return;
    uint startIndex = m_pool.size();

    auto flush = [this](DynamicDataMessage const& message)
    {
        m_pool.push_back(send(message));
    };
    auto process = [this](ObjectIterator object)
    {
        temp->objectSize = m_factory.encode(nullptr, object->state(),
                                            temp->objectData.data(),
                                            MaxObjectBytecode, true);
        logDebug("Object [", object->id(), "] is uploading");
        debug_assert(temp->objectSize, "object must be encoded");
    };
    auto put = [this](DynamicDataMessage& message, ObjectIterator /*object*/)
    {
        return message.write(temp->objectData.data(), temp->objectSize);
    };

    // Prepare tiny
    ObjectsList& tiny = m_addedTinyObjects;
    auto constructAny = []()
    {
        return DynamicDataMessage();
    };
    writeObjectData<DynamicDataMessage>(
        tiny.begin(), tiny.end(),
        constructAny, flush, process, put, nop(), nop());
    tiny.clear();
    uint fatIndex = m_pool.size();

    // Prepare fats
    ObjectsList& fat = m_addedFatObjects;
    auto constructChunked = []()
    {
        return DynamicDataMessage();
    };
    writeObjectData<DynamicDataMessage>(
        fat.begin(), fat.end(),
        constructChunked, flush, process, put, nop(), nop());
    fat.clear();

    // Flush tiny
    for (uint iterMessage = startIndex; iterMessage < fatIndex; ++iterMessage)
    {
        NetworkMessage& message = m_pool[iterMessage];
        for (auto& iterNode : m_activeNodes)
        {
            ClientId nodeId = iterNode.first;
            if (m_web.isSuper(nodeId))
                m_web.sendTcp(nodeId, message);
        }
    }

    // Flush fat
    for (uint iterMessage = fatIndex; iterMessage < m_pool.size(); ++iterMessage)
    {
        NetworkMessage& message = m_pool[iterMessage];
        for (auto& iterNode : m_activeNodes)
            m_web.sendTcp(iterNode.first, message);
    }

    // Clear
    m_pool.erase(m_pool.begin() + startIndex, m_pool.end());
}
void Server::flushChunks()
{
    for (auto& iterNode : m_activeNodes)
    {
        ClientId nodeId = iterNode.first;
        ActiveNode& node = iterNode.second;
        uint startIndex = m_pool.size();

        EmptyDataMessage emptyData;
        for (int2 chunk : node.chunkToUpload)
        {
            auto& tinyObjects = m_storage->chunk(chunk).objects();
            if (!tinyObjects.empty())
                flushChunkObjects(nodeId, tinyObjects, chunk);
            else
            {
                if (!emptyData.write(chunk))
                {
                    m_pool.push_back(send(emptyData));
                    emptyData = EmptyDataMessage{};
                    bool success = emptyData.write(chunk);
                    debug_assert(success);
                }
                logDebug("[", nodeId, "] Empty chunk ", chunk, " is uploading");
            }
        }
        if (emptyData.numChunks() > 0)
            m_pool.push_back(send(emptyData));
        node.chunkToUpload.clear();

        // Send empty
        for (uint iterMessage = startIndex; iterMessage < m_pool.size(); ++iterMessage)
            m_web.sendTcp(nodeId, m_pool[iterMessage]);

        // Clear
        m_pool.erase(m_pool.begin() + startIndex, m_pool.end());
    }
}
void Server::flushChunkObjects(ClientId nodeId,
                               ObjectsList const& objects, int2 const& chunk)
{
    uint startIndex = m_pool.size();

    // Prepare
    auto construct = [this, chunk]()
    {
        return DynamicDataMessage(chunk, m_time, false, false);
    };
    auto flush = [this](DynamicDataMessage const& message)
    {
        m_pool.push_back(send(message));
    };
    auto process = [this](ObjectIterator object)
    {
        temp->objectSize = m_factory.encode(nullptr, object->state(),
                                            temp->objectData.data(), MaxObjectBytecode, true);
        debug_assert(temp->objectSize, "object must be encoded");
    };
    auto put = [this](DynamicDataMessage& message, ObjectIterator /*object*/)
    {
        return message.write(temp->objectData.data(), temp->objectSize);
    };
    auto preAct = [](DynamicDataMessage & message)
    {
        *message.h.flags |= DynamicDataMessage::ClearFlag;
    };
    auto postAct = [](DynamicDataMessage & message)
    {
        *message.h.flags |= DynamicDataMessage::LastFlag;
    };
    writeObjectData<DynamicDataMessage>(
        objects.begin(), objects.end(),
        construct, flush, process, put, preAct, postAct);

    // Flush
    for (NetworkMessage& message : m_pool)
        m_web.sendTcp(nodeId, message);

    logDebug("[", nodeId, "] Chunk ", chunk, " "
                 "with [", objects.size(), "] objects is uploading");

    // Clear
    m_pool.erase(m_pool.begin() + startIndex, m_pool.end());
}


// Ray cast
int2 Server::computeRaycastRegion(double3 const& position)
{
    double2 flat = floor(swiz<X, Z>(position) / (double) m_raycastRegionWidth);
    return static_cast<int2>(flat);
}
int2 Server::computeRaycastRegion(int2 const& chunk)
{
    double2 chunkPosition = static_cast<double2>(chunk) * (double) chunkWidth;
    return computeRaycastRegion(swiz<X, O, Y>(chunkPosition));
}
