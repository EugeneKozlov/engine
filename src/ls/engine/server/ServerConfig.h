/** @file ls/engine/server/Server.h
 *  @brief Server
 */

#pragma once
#include "ls/common.h"

namespace ls
{
    /** @addtogroup LsEngineServer
     *  @{
     */
    /// Server config
    struct ServerConfig
    : public ConfigPrototype
    {
        /// Operate
        template <class Operator>
        void operate(Operator op)
        {
            op(initialTime);
            op(timeUpdPeriod);
            op(pingUpdPeriod);
            op(timeMismatch);

            op(maxSimDistance);
            op(simPadDistance);
            op(raycastRegion);
            op(numRaycastRegion);
            op(regionLifetime);

            op(framePeriod);
            op(actionPeriod);
            op(actionTick);
            op(raycastInfoTick);
            op(raycastRealDrop);
            op(controlLag);

            op(trustActors);
            op(trustRaycasts);

            op(logActions);
            op(logInfoRaycast);
            op(logRealRaycast);
        }

        /// Initial server time
        ConfigProperty<DiscreteTime> initialTime = "config.server.initial_time";
        /// Time update period
        ConfigProperty<DeltaTime> timeUpdPeriod = "config.server.time_upd_period";
        /// Ping update period
        ConfigProperty<DeltaTime> pingUpdPeriod = "config.server.ping_upd_period";
        /// Max allowed time mismatch
        ConfigProperty<DeltaTime> timeMismatch = "config.server.time_mismatch";

        /// Max simulation distance
        ConfigProperty<uint> maxSimDistance = "config.server.max_sim_distance";
        /// Extra chunks simulated to avoid errors
        ConfigProperty<uint> simPadDistance = "config.server.sim_pad_distance";
        /// Raycast region size (in units)
        ConfigProperty<uint> raycastRegion = "config.server.raycast_region";
        /// (n*2+1)^2 raycasts regions will be sent
        ConfigProperty<uint> numRaycastRegion = "config.server.num_raycast_region";
        /// Lifetime of unused region (ms)
        ConfigProperty<uint> regionLifetime = "config.server.region_lifetime";

        /// Frame timer period
        ConfigProperty<DeltaTime> framePeriod = "config.server.frame_period";
        /// Client action flush timer period
        ConfigProperty<DeltaTime> actionPeriod = "config.server.action_period";
        /// Client actions sending period
        ConfigProperty<DeltaTime> actionTick = "config.server.action_tick";
        /// Raycast info sending period
        ConfigProperty<DeltaTime> raycastInfoTick = "config.server.raycast_info_tick";
        /// Timeout of raycast request rejecting
        ConfigProperty<DeltaTime> raycastRealDrop = "config.server.raycast_real_drop";
        /// Client control lag
        ConfigProperty<DeltaTime> controlLag = "config.server.control_lag";

        /// Set to trust in actor positions
        ConfigProperty<bool> trustActors = "config.server.trust_actors";
        /// Set to trust in raycast results
        ConfigProperty<bool> trustRaycasts = "config.server.trust_raycasts";

        /// Set to trace time
        ConfigProperty<bool> logTime = "config.server.log_time";
        /// Set to trace actions
        ConfigProperty<bool> logActions = "config.server.log_actions";
        /// Set to trace info raycasts
        ConfigProperty<bool> logInfoRaycast = "config.server.log_info_raycast";
        /// Set to trace real raycasts
        ConfigProperty<bool> logRealRaycast = "config.server.log_real_raycast";
    };
    /// @}
}