/** @file ls/engine/server/Server.h
 *  @brief Server
 */

#pragma once
#include "ls/common.h"
#include "ls/forward.h"
#include "ls/core/Nameable.h"
#include "ls/core/Timer.h"
#include "ls/simulation/ActorMovement.h"
#include "ls/simulation/RaycastInfo.h"
#include "ls/world/WorldConfig.h"
#include "ls/engine/forward.h"
#include "ls/engine/server/ServerConfig.h"
#include "ls/engine/network/reflection.h"

namespace ls
{
    /** @addtogroup LsEngineServer
     *  @{
     */
    /** Server.
     *
     *  - @b Uploading
     *
     *    Objects can be uploaded by two ways:
     *
     *    + Tiny objects without global state should be sent for each super client
     *      which observe related chunk
     *
     *    + Fat object with global state should be send for each client @b at @b all
     */
    class Server
    : public Nameable<Server>
    {
    public:
        /** Ctor.
         */
        Server(Web& web, ObjectsFactory& factory);
        /// Dtor
        ~Server();
        /// Update
        void update(DeltaTime deltaTime);
        /// Flush all
        void flush();
        /// Start server
        void start();
        /// Stop server
        void stop();

        /// Add node by @a id
        void addNode(ClientId id);
        /// Remove node by @a id
        void removeNode(ClientId id);

        /** @name Input
         *  @return @c true if processed, @c false if ignored
         *  @{
         */
        /// Process any TCP @a message from @a client
        bool processTcpMessgae(ClientId client, NetworkMessage const& message);
        /// Process any UDP @a message from @a client
        bool processUdpMessgae(ClientId client, NetworkMessage const& message);
        /// @}

        /** @name Gets
         *  @{
         */
        /// Test if started
        bool isStarted() const noexcept
        {
            return m_startedFlag;
        }
        /// Get server time
        DiscreteTime time() const noexcept
        {
            return m_time;
        }
        /// Get objects storage
        ObjectsManager& objects() noexcept
        {
            return *m_storage;
        }
        /// @}
    private:
        template <class ... Args>
        inline void traceAction(Args const& ... args) const
        {
            if (logActions)
                this->logTrace(args...);
        }
        template <class ... Args>
        inline void traceInfoRaycast(Args const& ... args) const
        {
            if (logInfoRaycasts)
                this->logTrace(args...);
        }
        template <class ... Args>
        inline void traceRealRaycast(Args const& ... args) const
        {
            if (logRealRaycasts)
                this->logTrace(args...);
        }
    private:
        struct InactiveNode
        {
            /// Ctor
            InactiveNode(ClientId id)
                : id(id)
            {
            }
            /// Node ID
            ClientId id = NetInvalidClient;
        };
        struct ActiveNode : InactiveNode
        {
            /// Ctor
            ActiveNode(InactiveNode& node)
                : InactiveNode(node)
            {
            }

            /// Recently added chunks
            set<int2> chunkToUpload;
            /// Chunks observer
            shared_ptr<WorldChunkObserver> chunksObserver;
            /// Last received chunk
            int2 lastChunk = int2();
            /// Raycast region
            int2 raycastRegion = int2();
            /// Simulation distance
            ushort simDistance = 1;

            /// Current ping
            DeltaTime lastPing = std::numeric_limits<sint>::max();
            /// Received actions
            map<DiscreteTime, ActorMovement> movementsMap;
            /// Last received action
            ActorMovement lastMovement;

            /// Actor
            ObjectIterator actor;
            /// Actor uptime
            DiscreteTime actorUptime = 0;
        };
        struct RaytestRequest
        {
            RaytestRequest() = default;
            RaytestRequest(RaycastInfo const& ray, int2 const& casterChunk)
                : ray(ray)
                , casterChunk(casterChunk)
            {
            }
            void updateRaycastHit(ObjectIterator object, ubyte type,
                                  int2 const& chunk)
            {
                sint dist2 = distance2(casterChunk, chunk);
                if (!hit || dist2 < hitDistance2)
                {
                    hit = true;
                    hitObject = object;
                    hitType = type;
                    hitChunk = chunk;
                    hitDistance2 = dist2;
                }
            }
        public:
            RaycastInfo ray;
            int2 casterChunk;

            uint numSupers = 0;
            bool shouldDestroy = false;

            bool hit = false;
            ObjectIterator hitObject;
            ubyte hitType;
            int2 hitChunk;
            sint hitDistance2;
        };
        ActiveNode* findActiveNode(ClientId id);

        // Start/stop sim
        void startNode(ClientId id);
        void stopNode(ClientId id);

        // Process messages
        void processMoveRequest(ClientId client,
                                MoveRequestMessage const& message);
        void processClientRequest(ClientId client,
                                  ClientRequestMessage&& message);
        void processActionRequest(ClientId client,
                                  ActionRequestMessage const& message);
        void processRaycastRequest(ClientId client,
                                   RaycastRequestMessage const& message);
        void processRaycastOrder(ClientId client,
                                 RaycastOrderMessage const& message);
        void processCorrectionData(ClientId client,
                                   CorrectionDataMessage&& message);
        void processDynamicData(ClientId client,
                                DynamicDataMessage const& message);

        // Process raycast
        void processRaytestHit(ClientId client,
                               RaytestHitBase const& message,
                               bool isWeak);
        void processRaytestDull(ClientId client,
                                RaytestDullBase const& message,
                                bool isWeak);
        void processRaytestMiss(ClientId client,
                                RaytestMissMessage const& message);
        void addRaycast(RaycastInfo const& ray);
        void acceptRayhit(RaytestRequest const& request);
        void raycastMiss(RaycastInfo const& ray);
        void raycastDull(int2 const& chunk, RaycastInfo const& ray);
        void raycastHit(ObjectInstance& hitObject, ubyte hitType, ClientId invoker);

        // Timing and actions
        void updateTime();
        void updatePings();
        void updateActions();

        // Raycast broadcast
        void updateRaycastInfo();
        void addRaycastInfo(ClientId invoker, ObjectInstance& caster,
                            ubyte castSource, ubyte castType,
                            float3 const& direction, DiscreteTime raycastTime);
        void updateRaycastProofs();

        // Objects
        void flushAddedObjects();
        void flushChunks();
        void flushChunkObjects(ClientId nodeId,
                               ObjectsList const& objects,
                               int2 const& chunk);

        int2 computeRaycastRegion(double3 const& position);
        int2 computeRaycastRegion(int2 const& chunk);
    public:
        /// Will trace time if set
        bool const logTime;
        /// Will trace actions if set
        bool const logActions;
        /// Will trace info raycasts if set
        bool const logInfoRaycasts;
        /// Will trace real raycasts if set
        bool const logRealRaycasts;
        /// Logical chunk width
        uint const chunkWidth;
        /// Logical region width
        uint const regionWidth;
    private:
        Web& m_web;
        ServerConfig const& m_config;
        vector<NetworkMessage> m_pool;

        /// Started flag
        bool m_startedFlag = false;
        /// Current time
        DiscreteTime m_time = 0;

        // Timers
        Timer m_timeUpdateTimer;
        Timer m_pingUpdateTimer;
        Timer m_actionUpdateTimer;
        Timer m_raycastInfoUpdateTimer;
        Timer m_raycastRealUpdateTimer;

        DiscreteTime m_actionTime = 0;
        DiscreteTime m_raycastInfoTime = 0;

        // Storage
        unique_ptr<ObjectsManager> m_storage;
        ObjectsFactory& m_factory;

        // Objects info
        ObjectsList m_addedTinyObjects;
        ObjectsList m_addedFatObjects;

        // Temp
        struct Temp
        {
            array<ubyte, MaxObjectBytecode> objectData;
            uint objectSize = 0;
        };
        unique_ptr<Temp> temp;

        // Raycast
        struct RaycastInfoRegion
        {
            vector<NetworkMessage> pool;
            RaycastDataMessage message;
            ObjectId onlyCaster = InvalidObjectId;
            uint unused = 0;
        };
        sint m_numRaycastRegions;
        uint m_raycastRegionWidth;
        uint m_infoRaycastLifetime;
        hamap<int2, RaycastInfoRegion> m_infoRaycasts;
        hamap<RaycastKey, RaytestRequest> m_realRaycasts;

        hamap<ClientId, InactiveNode> m_inactiveNodes;
        hamap<ClientId, ActiveNode> m_activeNodes;
    };
    /// @}
}