/** @file ls/engine/client/Support.h
 ** @brief Simulation support server
 */
#pragma once
#include "ls/common.h"
#include "ls/forward.h"
#include "ls/core/Nameable.h"
#include "ls/engine/forward.h"

namespace ls
{
    /** Simulation support server processor.
     */
    class Support
    : public Noncopyable
    , public Nameable<Support>
    {
    public:
        /** Ctor.
         *  @param web
         *    Network web instance (only write, never read)
         *  @param simulation
         *    Simulation
         */
        Support(Web& web, Simulation& simulation);
        /// Dtor
        ~Support();

        /** Add actor correction data
         *  @param info
         *    Actor pose
         */
        void addCorrectedActor(ActorPosition const& info);
        /** Flush actors correction data
         */
        void flushCorrection();

        /** Perform ray-cast and send result to server
         *  @param info
         *    Ray info
         */
        void performRaycast(RaycastInfo const& info);

        /** Add simulated @a frame bound to @a chunk
         *  @param frame
         *    Object frame
         *  @param chunk
         *    Object chunk
         *  @note Time @b must be the same for each frames in portion
         */
        void addSimulatedFrame(ObjectState const& frame, int2 const& chunk);
        /** Flush dynamic frames
         */
        void flushFrames(DiscreteTime time);
    private:
        template <class ... Args>
        inline void traceRaycast(Args const& ... args) const
        {
            if (logRaycasts)
                this->logTrace(args...);
        }
    public:
        /// Set to trace raycasts
        bool const logRaycasts;
    private:
        Web& m_web;
        Simulation const& m_simulation;
        ObjectsFactory& m_factory;
        WorldChunkManager const& m_chunkManager;
        ClientId const m_thisClient;

        vector<NetworkMessage> m_pool;

        array<ubyte, MaxObjectBytecode> m_objectData;
        uint m_objectSize = 0;

        map<int2, deque<DynamicDataMessage>> m_uploadDynamics;
        vector<pair<int2, NetworkMessage>> m_uploadDynamicsMessages;

        vector<CorrectionDataMessage> m_correctionMessages;
    };
}

