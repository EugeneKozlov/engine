/** @file ls/engine/client/Client.h
 ** @brief Main client class
 **/
#pragma once
#include "ls/common.h"
#include "ls/forward.h"
#include "ls/core/Nameable.h"
#include "ls/simulation/RaycastInfo.h"
#include "ls/engine/forward.h"
#include "ls/engine/client/ClientConfig.h"

namespace ls
{
    class StreamingWorld;

    /** @addtogroup LsEngineClient
     *  @{
     */
    /** Client
     */
    class Client
    : public Noncopyable
    , public Nameable<Client>
    {
    public:
        /// Raycast callback
        function<void(ObjectInstance& caster, RaycastInfo const& ray) > onRaycast;
    public:
        /** Ctor.
         */
        Client(Web& web, ObjectsFactory& factory, StreamingWorld& worldData);
        /// Dtor
        ~Client();

        /// Add node by @a id
        void addNode(ClientId id);
        /// Remove node by @a id
        void removeNode(ClientId id);

        /** @name Actions
         *  @{
         */
        /// Register movement
        void registerMovement(ActorMovement const& movement);
        /// Invoke raycast
        void invokeRaycast(float3 const& direction, ubyte source, ubyte type);
        /// @}

        /** @name Input
         *  @return @c true if processed, @c false if ignored
         *  @{
         */
        /// Process any TCP @a message from @a client
        bool processTcpMessage(ClientId client, NetworkMessage const& message);
        /// Process any UDP @a message from @a client
        bool processUdpMessage(ClientId client, NetworkMessage const& message);
        /// @}

        /** @name Gets
         *  @{
         */
        /// Get this client ID
        ClientId thisClient() const noexcept;
        /// Get web
        Web& web() noexcept
        {
            return m_web;
        }
        /// Get const web
        Web const& web() const noexcept
        {
            return m_web;
        }
        /// Test if simulated
        bool isSimulated() const noexcept
        {
            return !!m_simulation;
        }
        /// Get simulation
        Simulation& sim() noexcept
        {
            return *m_simulation;
        }
        /// Get time
        DiscreteTime time() const noexcept;
        /// Get lag
        DeltaTime lag() const noexcept;
        /// @}
    private:
        template <class ... Args>
        inline void traceTime(Args const& ... args) const
        {
            if (*m_config.logTime)
                this->logTrace(args...);
        }
    private:
        /// Node
        struct Node
        {
            /// Ctor
            Node(ClientId id)
                : id(id)
            {
            }
            /// This ID
            ClientId id = NetInvalidClient;
            /// Is node active?
            bool isActive = false;
        };

        /// Get node
        Node& getNode(ClientId id);
        /// Find node
        Node* findNode(ClientId id);

        /// Process start
        void processStart(ClientId client, StartMessage const& message);
        /// Process stop
        void processStop(ClientId client, StopMessage const& message);
        /// Process time
        void processTime(ClientId client, TimeMessage const& message);
        /// Process ping data
        void processPingData(ClientId client, PingDataMessage&& message);
        /// Process move report
        void processMoveReport(ClientId client, MoveReportMessage const& message);
        /// Process server report
        void processServerReport(ClientId client, ServerReportMessage&& message);
        /// Process cinematic data
        void processCinematicData(ClientId client, CinematicDataMessage&& message);
        /// Process correction data
        void processCorrectionData(ClientId client, CorrectionDataMessage&& message);
        /// Process dynamic data
        void processDynamicData(ClientId client, DynamicDataMessage const& message, bool tcp);
        /// Process empty data
        void processEmptyData(ClientId client, EmptyDataMessage&& message);
        /// Process raycast data
        void processRaycastData(ClientId client, RaycastDataMessage&& message);
        /// Process dynamic data
        void processRaytestRequest(ClientId client, RaytestRequestMessage const& message);

        /// Send action
        void sendAction(DiscreteTime time, ActorMovement const& movement);
        /// Send raycast request
        void sendRaycast(RaycastInfo const& info);
    private:
        ClientConfig const& m_config;

        Web& m_web;
        StreamingWorld& m_worldData;
        ObjectsFactory& m_factory;

        hamap<ClientId, Node> m_nodes;

        unique_ptr<WorldChunkObserver> m_chunksObserver;
        unique_ptr<WorldChunkOwner> m_chunkOwner;

        unique_ptr<Simulation> m_simulation;
        unique_ptr<Support> m_support;

        optional<RaycastInfo> m_currentRaycast;
    };
    /// @}
}

