/** @file ClientConfig.h
 *  @brief
 */

#pragma once
#include "ls/common.h"
#include "ls/app/ConfigManager.h"

namespace ls
{
    /** Client config
     */
    struct ClientConfig
        : public ConfigPrototype
    {
        /// Operate
        template <class Operator>
        void operate(Operator op)
        {
            op(simDistance);
            op(outerDistancePad);

            op(timesetStamps);

            op(actorsTracing);
            op(correctionDelay);

            op(lagError);
            op(minLag);
            op(extraLag);

            op(logActors);
            op(logChunks);
        }

        /// Simulation distance (in chunks)
        ConfigProperty<uint> simDistance = "config.client.sim_distance";
        /// Outer distance pad (1)
        ConfigProperty<uint> outerDistancePad = "config.client.outer_distance_pad";

        /// Max number of time deltas in median computer
        ConfigProperty<uint> timesetStamps = "config.client.timeset_stamps";

        /// Actor tracing time (~1 sec)
        ConfigProperty<DeltaTime> actorsTracing = "config.client.actors_tracing";
        /// Correction delay
        ConfigProperty<DeltaTime> correctionDelay = "config.client.correction_delay";

        /// Max lag error
        ConfigProperty<DeltaTime> lagError = "config.client.lag_error";
        /// Min lag value
        ConfigProperty<DeltaTime> minLag = "config.client.min_lag";
        /// Small lag value that should fix ping instability
        ConfigProperty<DeltaTime> extraLag = "config.client.extra_lag";

        /// Set to trace actors movements
        ConfigProperty<bool> logActors = "config.client.log_actors";
        /// Set to trace chunk manager messages
        ConfigProperty<bool> logChunks = "config.client.log_chunks";
        /// Set to trace time updates
        ConfigProperty<bool> logTime = "config.client.log_time";
        /// Set to trace computed raycasts
        ConfigProperty<bool> logRaycasts = "config.client.log_raycasts";
    };
}