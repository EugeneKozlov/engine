#include "pch.h"
#include "ls/engine/app/Config.h"
#include "ls/engine/client/Client.h"
#include "ls/engine/client/Support.h"
#include "ls/engine/network/reflection.h"
#include "ls/simulation/Simulation.h"
#include "ls/simulation/ActorSimulation.h"
#include "ls/world/WorldChunkManager.h"
#include "ls/scene/ObjectsManager.h"
#include "ls/scene/ObjectsFactory.h"
#include "ls/network/Web.h"

using namespace ls;
Client::Client(Web& web, ObjectsFactory& factory, StreamingWorld& worldData)
    : Nameable("Client")

    , onRaycast([](ObjectInstance&, const RaycastInfo&)
    {
    })
, m_config(config().client())
, m_web(web)
, m_worldData(worldData)
, m_factory(factory)
, m_simulation()
, m_support()
{
}
Client::~Client()
{
}
Client::Node& Client::getNode(ClientId id)
{
    return getOrFail(m_nodes, id);
}
Client::Node* Client::findNode(ClientId id)
{
    return findOrDefault(m_nodes, id);
}


// Others
void Client::addNode(ClientId id)
{
    // Add
    m_nodes.emplace(id, Node{id});

    // Log
    logInfo("[", id, "] Added");
}
void Client::removeNode(ClientId id)
{
    // Remove
    m_nodes.erase(id);
    if (isSimulated())
        m_simulation->removeClient(id);

    // Log
    logInfo("[", id, "] Removed");
}


// Messages
bool Client::processTcpMessage(ClientId client, NetworkMessage const& message)
{
    switch (message.id())
    {
    case MessageCode::Start:
        processStart(client, receive<StartMessage>(message));
        break;
    case MessageCode::Stop:
        processStop(client, receive<StopMessage>(message));
        break;
    case MessageCode::MoveReport:
        processMoveReport(client, receive<MoveReportMessage>(message));
        break;
    case MessageCode::ServerReport:
        processServerReport(client, receive<ServerReportMessage>(message));
        break;
    case MessageCode::CinematicData:
        processCinematicData(client, receive<CinematicDataMessage>(message));
        break;
    case MessageCode::CorrectionData:
        processCorrectionData(client, receive<CorrectionDataMessage>(message));
        break;
    case MessageCode::DynamicData:
        processDynamicData(client, receive<DynamicDataMessage>(message), true);
        break;
    case MessageCode::EmptyData:
        processEmptyData(client, receive<EmptyDataMessage>(message));
        break;
    case MessageCode::RaytestRequest:
        processRaytestRequest(client, receive<RaytestRequestMessage>(message));
        break;
    default:
        return false;
    }
    return true;
}
void Client::processStart(ClientId client, StartMessage const& message)
{
    if (isSimulated() || client != NetRootClient)
    {
        logWarning("[", client, "] sent [Start] message");
        return;
    }

    // Start
    SimulationInfo info;
    info.thisClient = thisClient();

    info.framePeriod = *message.h.framePeriod;
    info.actionPeriod = *message.h.actionPeriod;
    info.actionTick = *message.h.actionTick;
    info.controlLag = *message.h.controlLag;

    info.timeMismatch = *message.h.timeMismatch;
    info.chunkWidth = *message.h.chunkWidth;
    info.maxSimDistance = *message.h.maxSimDistance;
    info.simDistancePad = *message.h.simDistancePad;
    info.outerDistancePad = *config().client().outerDistancePad;

    info.numTimeStamps = *config().client().timesetStamps;
    info.tracingDuration = *config().client().actorsTracing;
    info.correctionDelay = *config().client().correctionDelay;

    info.minLag = *config().client().minLag;
    info.extraLag = *config().client().extraLag;
    info.lagError = *config().client().lagError;

    info.trustActors = message.areActorsTrusted();
    info.trustRaycasts = message.areRaycastsTrusted();

    info.traceActors = *config().client().logActors;
    info.traceChunks = *config().client().logChunks;
    info.traceTime = *config().client().logTime;

    m_simulation.reset(new Simulation(info, m_factory, m_worldData, *message.h.time));

    // Actions
    m_simulation->onAct = [this](DiscreteTime time, ActorMovement const& movement)
    {
        sendAction(time, movement);
    };
    m_simulation->onCorrectActor = [this](ActorPosition const& frame)
    {
        m_support->addCorrectedActor(frame);
    };
    m_simulation->onFlushCorrection = [this]()
    {
        m_support->flushCorrection();
    };

    // Frames
    m_simulation->onSimulateFrame = [this](ObjectState const& frame, int2 const& chunk)
    {
        m_support->addSimulatedFrame(frame, chunk);
    };
    m_simulation->onFlushFrames = [this](DiscreteTime time)
    {
        m_support->flushFrames(time);
    };

    m_support.reset(new Support(m_web, *m_simulation));

    // Log
    logInfo("Started");
}
void Client::processStop(ClientId client, StopMessage const& /*message*/)
{
    if (!isSimulated() || client != NetRootClient)
    {
        logWarning("[", client, "] sent [Stop] message");
        return;
    }

    // Stop
    m_support.reset();
    m_simulation.reset();

    // Log
    logInfo("Stopped");
}
void Client::processTime(ClientId client, TimeMessage const& message)
{
    if (!isSimulated())
        return;

    if (client != NetRootClient)
    {
        logWarning("[", client, "] sent [Time] message");
        return;
    }

    Node& thisNode = getNode(thisClient());

    DiscreteTime serverTime = *message.h.time;
    DeltaTime thisPing = *message.h.ping;
    DiscreteTime thisTime = serverTime + thisPing / 2;
    m_simulation->setTime(thisTime);
    m_simulation->setPing(thisClient(), thisPing);

    traceTime("Fix time "
              ": Time = ", thisTime,
              "; Ping = ", thisPing,
              "; LocalTime = ", m_simulation->time());
}
void Client::processPingData(ClientId client, PingDataMessage&& message)
{
    if (client != NetRootClient)
    {
        logWarning("[", client, "] sent [PingData] message");
        return;
    }

    ClientId clientId;
    DeltaTime ping;
    while (message.read(clientId, ping))
    {
        Node* clientPtr = findNode(clientId);
        if (!clientPtr)
        {
            logDebug("Unknown client [", clientId, "] ping is ignored");
            continue;
        }

        // Set
        m_simulation->setPing(clientId, ping);
    }
}
void Client::processMoveReport(ClientId client, MoveReportMessage const& message)
{
    ClientId movedClient = *message.h.client;
    int2 chunk = *message.h.chunk;
    ushort dist = *message.h.dist;
    Node* clientPtr = findNode(movedClient);

    if (client != NetRootClient)
    {
        logWarning("[", client, "] sent [MoveReport] message");
        return;
    }
    if (!isSimulated())
    {
        logWarning("Message [MoveReport] is received, "
                       "but this client is not simulated yet");
        return;
    }
    if (!clientPtr)
    {
        logWarning("Moved client [", movedClient, "] is not registered");
        return;
    }

    // Process
    clientPtr->isActive = true;
    m_simulation->moveClient(movedClient, chunk, dist);

    // Log
    logInfo("[", movedClient, "] Moved to ", chunk, ":", dist);
}
void Client::processServerReport(ClientId client, ServerReportMessage&& message)
{
    if (client != NetRootClient)
    {
        logWarning("[", client, "] sent [ServerReport] message");
        return;
    }

    // Process
    switch (*message.h.reportType)
    {
        /// HACK
    case 0:
    {
        ClientId clientId;
        ObjectId objectId;
        double3 position;
        decode<Encoding::Short>(message.tail, clientId);
        decode<Encoding::Int>(message.tail, objectId);
        decode<Encoding::VectorFixed4>(message.tail, position);
        ObjectsManager& objects = m_simulation->objectsManager();
        if (objects.isObject(objectId))
        {
            objects.removeObject(objectId);
        }
        ObjectPrototype& proto = m_factory.findPrototype("player");
        ObjectIterator object = objects.addObject(objectId);

        m_factory.addParameter("position", position);

        ObjectState state;
        m_factory.build(state, objectId, proto);
        object->reconstruct(state);

        m_simulation->attachActor(clientId, objectId);
        break;
    }
    default:
        break;
    }
}
void Client::processCinematicData(ClientId client,
                                  CinematicDataMessage&& message)
{
    if (!isSimulated())
        return;

    if (client != NetRootClient)
    {
        logWarning("[", client, "] sent [CinematicData] message");
        return;
    }

    DiscreteTime time = *message.h.time;
    ClientId id;
    ActorMovement movement;
    while (!message.isEmpty())
    {
        message.read(id, movement);
        m_simulation->receiveCinematic(id, time, movement);
    }
}
void Client::processCorrectionData(ClientId client,
                                   CorrectionDataMessage&& message)
{
    if (!isSimulated())
        return;

    if (client == thisClient())
        return;

    if (!m_web.isSuper(client))
    {
        logWarning("[", client, "] sent [CorrectionData] message");
        return;
    }

    DiscreteTime time;
    ClientId id;
    double3 position;
    while (!message.isEmpty())
    {
        message.read(id, time, position);
        m_simulation->correctCinematic(client, id, time, position);
    }
}
void Client::processDynamicData(ClientId client,
                                DynamicDataMessage const& message, bool tcp)
{
    if (!isSimulated())
    {
        if (tcp)
            logWarning("[", client, "] sent [DynamicData] message");
        return;
    }

    DiscreteTime time = *message.h.time;
    int2 chunk = *message.h.chunk;
    // Server
    if (!message.isWeak())
    {
        // Only from root, only by TCP
        if (client != NetRootClient || !tcp)
        {
            const char* tag = tcp ? "" : " by UDP";
            logWarning("[", client, "] Strong dynamics data is received", tag);
            return;
        }
        // Clear or/and sim
        if (message.isChunked())
        {
            // Clear
            if (message.needClear())
            {
                m_simulation->clearChunk(chunk);
            }
            // Simulate
            if (message.isLast())
            {
                m_simulation->prepareChunk(chunk);
            }
        }
        // Process
        readObjectInfo(message, m_factory, [this, chunk](ObjectState const& frame)
        {
            m_simulation->receiveServerDynamic(frame, chunk);
        });
    }
        // Support
    else
    {
        // Bad flag set
        if (!message.isLast() || !message.isChunked() || message.needClear())
        {
            logWarning("[", client, "] Invalid dynamics flag set");
        }
        // Ignore this
        if (client == m_simulation->thisClient)
        {
            return;
        }
        // Just read
        readObjectInfo(message, m_factory, [this, client, chunk](ObjectState const& frame)
        {
            m_simulation->receiveSupportDynamic(client, frame, chunk);
        });
    }
}
void Client::processEmptyData(ClientId client,
                              EmptyDataMessage&& message)
{
    if (!isSimulated() || client != NetRootClient)
    {
        logWarning("[", client, "] sent [EmptyData] message");
        return;
    }

    // Read
    uint numChunks = message.numChunks();
    for (uint iterChunk = 0; iterChunk < numChunks; ++iterChunk)
    {
        int2 chunk = message.read();
        m_simulation->clearChunk(chunk);
        m_simulation->prepareChunk(chunk);
    }
}
void Client::processRaycastData(ClientId client,
                                RaycastDataMessage&& message)
{
    if (!isSimulated() || client != NetRootClient)
    {
        logWarning("[", client, "] sent [RaycastData] message");
        return;
    }

    // This actor
    ObjectIterator thisActor = m_simulation->actors().thisActor();
    ObjectId thisActorId = thisActor ? thisActor->id() : InvalidObjectId;

    // Read raycasts
    ObjectsManager& objectsManager = m_simulation->objectsManager();
    uint numRays = message.numRays();
    while (numRays > 0)
    {
        --numRays;

        RaycastInfo info;
        info.invoker = client;
        message.read(info.direction, info.object, info.source, info.type, info.time);

        // Callback
        ObjectIterator casterObject = objectsManager.findObject(info.object);
        if (info.object != thisActorId && !!casterObject)
            onRaycast(*casterObject, info);
    }
}
void Client::processRaytestRequest(ClientId client,
                                   RaytestRequestMessage const& message)
{
    if (!isSimulated() || client != NetRootClient)
    {
        logWarning("[", client, "] sent [RaytestRequest] message");
        return;
    }

    RaycastInfo info;
    message.get(info);
    info.object = m_simulation->actors().actorObject(info.invoker);
    m_support->performRaycast(info);
}
bool Client::processUdpMessage(ClientId client,
                               NetworkMessage const& message)
{
    switch (message.id())
    {
    case MessageCode::Time:
        processTime(client, receive<TimeMessage>(message));
        break;
    case MessageCode::PingData:
        processPingData(client, receive<PingDataMessage>(message));
        break;
    case MessageCode::CorrectionData:
        processCorrectionData(client, receive<CorrectionDataMessage>(message));
        break;
    case MessageCode::CinematicData:
        processCinematicData(client, receive<CinematicDataMessage>(message));
        break;
    case MessageCode::DynamicData:
        processDynamicData(client, receive<DynamicDataMessage>(message), false);
        break;
    case MessageCode::RaycastData:
        processRaycastData(client, receive<RaycastDataMessage>(message));
        break;
    default:
        return false;
    }
    return true;
}


// Input
void Client::registerMovement(ActorMovement const& movement)
{
    if (!isSimulated())
        return;

    m_simulation->actors().moveThisActor(movement);
}
void Client::invokeRaycast(float3 const& direction, ubyte source, ubyte type)
{
    if (!isSimulated())
        return;
    ObjectIterator thisActor = m_simulation->actors().thisActor();
    if (!thisActor)
    {
        logWarning("Try to invoke raycast without current actor");
        return;
    }

    RaycastInfo info;
    info.invoker = thisClient();
    info.time = time();
    info.object = thisActor->id();
    info.source = source;
    info.type = type;
    info.direction = direction;
    m_currentRaycast = info;

    onRaycast(*thisActor, info);
}


// This actions
void Client::sendAction(DiscreteTime time, ActorMovement const& movement)
{
    ActionRequestMessage action(time, movement);
    if (m_currentRaycast.is_initialized())
    {
        RaycastInfo& info = *m_currentRaycast;
        sendRaycast(info);

        action.h.hasRaycast = true;
        action.h.raycastTime = (sbyte) clamp<sint>(info.time - time, MinByte, MaxByte);
        action.h.raycastSource = info.source;
        action.h.raycastType = info.type;
        action.h.raycastDirection = info.direction;

        m_currentRaycast.reset();
    }
    m_web.sendUdp(NetRootClient, send(action));
}
void Client::sendRaycast(RaycastInfo const& info)
{
    if (!isSimulated())
        return;
    bool trustRaycasts = m_simulation->info.trustRaycasts;

    // Perform raycast
    RayhitInfo hit = m_simulation->raycastByInvoker(info);

    // Log
    if (*m_config.logRaycasts)
    {
        RaycastKey key(info);
        if (hit.isMiss())
            logTrace("My ray ", key, " misses");
        else if (hit.isDull())
            logTrace("My ray ", key, " hits chunk ", hit.chunk);
        else if (hit.isHit())
            logTrace("My ray ", key, " hits object ", hit.objectId);
    }

    if (trustRaycasts)
    {
        if (hit.isHit())
        {
            // Send order
            RaycastOrderMessage raycast(info.time, hit.objectId, hit.type);
            m_web.sendTcp(NetRootClient, send(raycast));
        }
    }
    else
    {
        // Send request
        RaycastRequestMessage raycast(info);
        m_web.sendTcp(NetRootClient, send(raycast));
    }
}


// Gets
ClientId Client::thisClient() const noexcept
{
    return m_web.id();
}
DiscreteTime Client::time() const noexcept
{
    if (isSimulated())
        return m_simulation->time();
    else
        return 0;
}
DeltaTime Client::lag() const noexcept
{
    if (isSimulated())
        return m_simulation->lag();
    else
        return 0;
}
