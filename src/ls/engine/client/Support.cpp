#include "pch.h"
#include "ls/engine/app/Config.h"
#include "ls/engine/client/Support.h"
#include "ls/engine/network/reflection.h"
#include "ls/world/StreamingWorld.h"
#include "ls/world/WorldChunkManager.h"
#include "ls/scene/ObjectsFactory.h"
#include "ls/simulation/Simulation.h"
#include "ls/network/Web.h"

using namespace ls;


// Main
Support::Support(Web& web, Simulation& simulation)
    : Nameable("Support")
    , logRaycasts(*config().client().logRaycasts)

    , m_web(web)
    , m_simulation(simulation)
    , m_factory(simulation.objectsFactory())
    , m_chunkManager(simulation.chunkManager())
    , m_thisClient(simulation.thisClient)
{
}
Support::~Support()
{
}


// Actors
void Support::addCorrectedActor(ActorPosition const& info)
{
    // Make sure that we can
    if (m_correctionMessages.empty() || m_correctionMessages.back().isFull())
    {
        m_correctionMessages.emplace_back();
    }
    // Put
    CorrectionDataMessage& last = m_correctionMessages.back();
    last.write(info.client, info.time, info.position);
}
void Support::flushCorrection()
{
    // Flush
    uint beginIndex = (uint) m_pool.size();
    for (CorrectionDataMessage& message : m_correctionMessages)
        m_pool.push_back(send(message));
    uint endIndex = (uint) m_pool.size();

    // Send
    for (ClientId client : m_chunkManager.clients())
    {
        bool isThis = client == m_chunkManager.thisClient();
        bool isRoot = client == NetRootClient;
        if (isThis && !isRoot)
            continue;

        for (uint iterMessage = beginIndex; iterMessage < endIndex; ++iterMessage)
            m_web.sendUdp(client, m_pool[iterMessage]);
    }

    // Clear
    m_pool.erase(m_pool.begin() + beginIndex, m_pool.end());
    m_correctionMessages.clear();
}


// Raycast
void Support::performRaycast(RaycastInfo const& info)
{
    RaycastKey key(info);
    RayhitInfo hit = m_simulation.raycastByInvoker(info);
    NetworkMessage message;
    if (hit.isMiss())
    {
        message = send(RaytestMissMessage(info.time, info.invoker));
        traceRaycast("Ray ", key, " misses");
    }
    else if (hit.isDull())
    {
        if (hit.isSure)
        {
            message = send(RaytestDullSureMessage(info.time, info.invoker, hit.chunk));
        }
        else
        {
            message = send(RaytestDullWeakMessage(info.time, info.invoker, hit.chunk));
        }
        traceRaycast("Ray ", key, " hits chunk ", hit.chunk,
                     hit.isSure ? " (sure)" : "");
    }
    else if (hit.isHit())
    {
        if (hit.isSure)
        {
            message = send(RaytestHitSureMessage(info.time, info.invoker,
                                                 hit.objectId, hit.type));
        }
        else
        {
            message = send(RaytestHitWeakMessage(info.time, info.invoker,
                                                 hit.objectId, hit.type));
        }
        traceRaycast("Ray ", key, " hits object [", hit.objectId, "]",
                     hit.isSure ? " (sure)" : "");
    }
    else
    {
        debug_assert(0);
    }
    m_web.sendTcp(NetRootClient, message);
}


// Frames
void Support::addSimulatedFrame(ObjectState const& frame, int2 const& chunk)
{
    // Encode
    m_objectSize = m_factory.encode(nullptr, frame,
                                    m_objectData.data(), MaxObjectBytecode, false);
    // Write
    auto& deck = m_uploadDynamics[chunk];
    if (deck.empty() || !deck.back().write(m_objectData.data(), m_objectSize))
    {
        deck.push_back(DynamicDataMessage{chunk, 0, false, true, true});
        bool success = deck.back().write(m_objectData.data(), m_objectSize);

        debug_assert(success);
    }
}
void Support::flushFrames(DiscreteTime time)
{
    // Compile
    for (auto& iterPortion : m_uploadDynamics)
    {
        int2 chunk = iterPortion.first;
        // Flush if owned or orphan
        bool owned = m_chunkManager.isOwned(chunk);
        bool orphan = !m_web.isSuper(m_chunkManager.owner(chunk));
        bool observed = m_chunkManager.isObserved(chunk);
        if (owned || (orphan && observed))
        {
            for (auto& message : iterPortion.second)
            {
                *message.h.time = time;
                *message.h.flags |= DynamicDataMessage::TimedFlag;
                m_uploadDynamicsMessages.emplace_back(chunk, send(message));
            }
        }
        iterPortion.second.clear();
    }

    // Clear unused
    for (auto it = m_uploadDynamics.begin(); it != m_uploadDynamics.end();)
    {
        if (it->second.empty())
            it = m_uploadDynamics.erase(it);
        else
        {
            it->second.clear();
            ++it;
        }
    }

    // Empty
    if (m_uploadDynamicsMessages.empty())
        return;

    // Send
    uint numMessages = (uint) m_uploadDynamicsMessages.size();
    auto pool = m_uploadDynamicsMessages.data();
    int2 thisCenter = m_chunkManager.position(m_thisClient).center();
    uint thisRadius = m_chunkManager.radius(m_thisClient);
    for (ClientId client : m_chunkManager.clients())
    {
        int2 anotherCenter = m_chunkManager.position(client).center();
        uint anotherRadius = m_chunkManager.radius(client);
        uint dist2 = (uint) distance2(thisCenter, anotherCenter);
        uint maxDist = 2 + m_chunkManager.observingDistancePad + thisRadius + anotherRadius;

        bool tooFar = dist2 > maxDist*maxDist;
        bool isRoot = client == NetRootClient;
        bool isThis = client == m_thisClient;
        if ((tooFar || isThis) && !isRoot)
            continue;

        for (uint iterMessage = 0; iterMessage < numMessages;)
        {
            // Test
            auto& prepared = pool[iterMessage];
            int2 chunk = prepared.first;
            bool isObserved = m_chunkManager.isObserved(chunk, client);

            // Send
            if (isRoot || isObserved)
            {
                while (iterMessage < numMessages && pool[iterMessage].first == chunk)
                {
                    m_web.sendUdp(client, pool[iterMessage].second);
                    ++iterMessage;
                }
            }
                // Skip
            else
            {
                while (iterMessage < numMessages && pool[iterMessage].first == chunk)
                    ++iterMessage;
            }
        }
    }
    m_uploadDynamicsMessages.clear();
}
