/** @file ls/engine/forward.h
 *  @brief Forward declaration of @a Ls.Engine
 */

#pragma once

namespace ls
{
    /** @addtogroup LsEngineApp
     *  @{
     */
    class Config;
    /// @}

    /** @addtogroup LsEngineClient
     *  @{
     */
    class Client;
    /// @}

    /** @addtogroup LsEngineNetwork
     *  @{
     */
    class StartMessage;
    class StopMessage;
    class TimeMessage;
    class PingDataMessage;
    class MoveRequestMessage;
    class MoveReportMessage;
    class ClientRequestMessage;
    class ServerReportMessage;
    class ActionRequestMessage;
    class RaycastRequestMessage;
    class RaycastOrderMessage;
    class CinematicDataMessage;
    class CorrectionDataMessage;
    class DynamicDataMessage;
    class EmptyDataMessage;
    class RaycastDataMessage;
    class RaytestRequestMessage;
    class RaytestHitBase;
    class RaytestHitSureMessage;
    class RaytestHitWeakMessage;
    class RaytestDullBase;
    class RaytestDullSureMessage;
    class RaytestDullWeakMessage;
    class RaytestMissMessage;

    class Instance;
    /// @}

    /** @addtogroup LsEngineServer
     *  @{
     */
    class Server;
    /// @}

    /** @addtogroup LsEngineSimulation
     *  @{
     */
    class Simulation;
    class Support;
    /// @}
}