#include "pch.h"
#include "ls/engine/network/reflection.h"

using namespace ls;

sint const DynamicDataMessage::UnspecifiedChunk;
DiscreteTime const DynamicDataMessage::UnspecifiedTime;
