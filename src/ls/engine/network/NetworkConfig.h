/** @file ls/engine/network/NetworkConfig.h
 *  @brief Network config
 */

#pragma once
#include "ls/common.h"
#include "ls/app/ConfigManager.h"

namespace ls
{
    /** @addtogroup LsEngineNetwork
     *  @{
     */
    /// Network config
    struct NetworkConfig
    : public ConfigPrototype
    {
        /// Operate
        template <class Operator>
        void operate(Operator op)
        {
            op(updatePeriod);
            op(flushPeriod);
            op(speedPeriod);
            op(pingPeriod);
            op(udpPeriod);
        }
        /// Network update period (in ms)
        ConfigProperty<uint> updatePeriod = "config.network.update_period";
        /// Network flush period (in ms)
        ConfigProperty<uint> flushPeriod = "config.network.flush_period";
        /// Data transferring speed update period (in ms)
        ConfigProperty<uint> speedPeriod = "config.network.speed_period";
        /// Ping interval (in ms)
        ConfigProperty<uint> pingPeriod = "config.network.ping_period";
        /// UDP re-establishing connection period (ms)
        ConfigProperty<uint> udpPeriod = "config.network.re_udp_period";
    };
    /// @}
}