/** @file ls/engine/network/reflection.h
 *  @brief Client/server network messages reflection
 */
#pragma once
#include "ls/common.h"
#include "ls/io/Encoded.h"
#include "ls/scene/ObjectState.h"
#include "ls/network/message.h"
#include "ls/network/NetworkReflection.h"
#include "ls/simulation/ActorMovement.h"
#include "ls/simulation/RaycastInfo.h"
#include "ls/engine/network/message.h"

namespace ls
{
    /** @addtogroup LsEngineNetwork
     *  @{
     */
    /** #MessageCode::Start reflection
     */
    class StartMessage
    : public MessageReflection<MessageCode::Start>
    {
    public:
        /// Header
        struct Header
        : public OperableConcept
        {
            /// Initial time
            Encoded<DiscreteTime, Encoding::Int> time = 0;

            /// Frame timer period
            Encoded<DeltaTime, Encoding::Short> framePeriod = 0;
            /// Action timer period
            Encoded<DeltaTime, Encoding::Short> actionPeriod = 0;
            /// Action tick
            Encoded<DeltaTime, Encoding::Short> actionTick = 0;
            /// Client control lag
            Encoded<DeltaTime, Encoding::Short> controlLag = 0;

            /// Time mismatch
            Encoded<DeltaTime, Encoding::Short> timeMismatch = 0;
            /// Chunk width
            Encoded<uint, Encoding::Short> chunkWidth = 0;
            /// Max sim distance
            Encoded<uint, Encoding::Short> maxSimDistance = 0;
            /// Sim distance pad
            Encoded<uint, Encoding::Short> simDistancePad = 0;

            /// Simulation flags
            Encoded<ushort, Encoding::Short> flags = 0;

            /// Operate
            template<class Function>
            inline void operate(Function& foo)
            {
                foo(time);

                foo(framePeriod);
                foo(actionPeriod);
                foo(actionTick);
                foo(controlLag);

                foo(timeMismatch);
                foo(chunkWidth);
                foo(maxSimDistance);
                foo(simDistancePad);

                foo(flags);
            }
        } h; ///< Header
    public:
        /// Default ctor
        StartMessage() = default;
        /// Empty ctor
        StartMessage(DiscreteTime time,
                     DeltaTime framePeriod, 
                     DeltaTime actionPeriod, DeltaTime actionTick, 
                     DeltaTime controlLag, 
                     DeltaTime timeMismatch,
                     uint chunkWidth, uint maxSimDistance, uint simDistancePad,
                     bool confidingActors, bool confidingRaycasts)
        {
            *h.time = time;

            *h.framePeriod = framePeriod;
            *h.actionPeriod = actionPeriod;
            *h.actionTick = actionTick;
            *h.controlLag = controlLag;

            *h.timeMismatch = timeMismatch;
            *h.chunkWidth = chunkWidth;
            *h.maxSimDistance = maxSimDistance;
            *h.simDistancePad = simDistancePad;

            *h.flags |= uint(confidingActors) << 0;
            *h.flags |= uint(confidingRaycasts) << 1;
        }

        /** @name Gets
         *  @{
         */
        /// Test if we trust in actors positions
        bool areActorsTrusted() const noexcept
        {
            return !!(*h.flags & (1 << 0));
        }
        /// Test if we trust in actors raycasts
        bool areRaycastsTrusted() const noexcept
        {
            return !!(*h.flags & (1 << 1));
        }
        /// @}
    };
    /** #MessageCode::Stop reflection
     */
    class StopMessage
    : public MessageReflection<MessageCode::Stop>
    {
    };
    /** #MessageCode::Time reflection
     */
    class TimeMessage
    : public MessageReflection<MessageCode::Time>
    {
    public:
        /// Header
        struct Header
        : public OperableConcept
        {
            /// Time
            Encoded<DiscreteTime, Encoding::Int> time = 0;
            /// Ping
            Encoded<DeltaTime, Encoding::Int> ping = 0;
            /// Operate
            template <class Function>
            inline void operate(Function& foo)
            {
                foo(time);
                foo(ping);
            }
        } h; ///< Header
    public:
        /// Empty ctor
        TimeMessage(DiscreteTime time = 0, DeltaTime ping = 1)
        {
            *h.time = time;
            *h.ping = ping;
        }
    };
    /** #MessageCode::PingData reflection
     */
    class PingDataMessage
    : public MessageReflection<MessageCode::PingData, 480>
    {
    public:
        /** Ctor.
         */
        PingDataMessage()
        {
        }

        /** @name Using data
         *  @{
         */
        /// Write @a ping for @a client
        bool write(ClientId client, uint ping)
        {
            if (!tail.canWrite(4))
                return false;

            static uint const MaxPing = std::numeric_limits<ushort>::max();
            encode<Encoding::Short>(tail, client);
            encode<Encoding::Short>(tail, (ushort) min<uint>(ping, MaxPing));
            return true;
        }
        /// Get number of pings
        uint num() const noexcept
        {
            return tail.numWritten() / 4;
        }
        /// Read @a client and @a ping 
        bool read(ClientId& client, uint& ping)
        {
            if (!tail.canRead(4))
                return false;

            decode<Encoding::Short>(tail, client);
            decode<Encoding::Short>(tail, ping);
            return true;
        }
        /// @}
    };
    /** #MessageCode::MoveRequest reflection
     */
    class MoveRequestMessage
    : public MessageReflection<MessageCode::MoveRequest>
    {
    public:
        /// Header
        struct Header
        : public OperableConcept
        {
            /// Chunk
            Encoded<int2, Encoding::VectorShort> chunk = int2(0, 0);
            /// Distance
            Encoded<ushort, Encoding::Short> dist = 0;
            /// Operate
            template <class Function>
            inline void operate(Function& foo)
            {
                foo(chunk);
                foo(dist);
            }
        } h; ///< Header
    public:
        /// Empty ctor
        MoveRequestMessage() = default;
        /// Ctor by @a chunk, @a distance and @a client ID
        MoveRequestMessage(int2 const& chunk, ushort distance)
        {
            h.chunk = chunk;
            h.dist = distance;
        }
    };
    /** #MessageCode::MoveReport reflection
     */
    class MoveReportMessage
    : public MessageReflection<MessageCode::MoveReport>
    {
    public:
        /// Header
        struct Header
        : public OperableConcept
        {
            /// Chunk
            Encoded<int2, Encoding::VectorShort> chunk = int2(0, 0);
            /// Distance
            Encoded<ushort, Encoding::Short> dist = 0;
            /// Client
            Encoded<ClientId, Encoding::Short> client = NetInvalidClient;
            /// Operate
            template <class Function>
            inline void operate(Function& foo)
            {
                foo(chunk);
                foo(dist);
                foo(client);
            }
        } h; ///< Header
    public:
        /// Empty ctor
        MoveReportMessage() = default;
        /// Ctor by @a chunk, @a distance and @a client ID
        MoveReportMessage(int2 const& chunk, ushort distance, ClientId client)
        {
            h.chunk = chunk;
            h.dist = distance;
            h.client = client;
        }
    };
    /** #MessageCode::ClientRequest reflection
     */
    class ClientRequestMessage
    : public MessageReflection<MessageCode::ClientRequest, 480>
    {
    public:
        /// Header
        struct Header
        : public OperableConcept
        {
            /// Request type
            Encoded<uint, Encoding::Int> requestType;
            /// Operate
            template <class Function>
            inline void operate(Function& foo)
            {
                foo(requestType);
            }
        } h; ///< Header
    public:
        /// Empty ctor
        ClientRequestMessage() = default;
        /// Ctor by @a requestType
        ClientRequestMessage(uint requestType)
        {
            h.requestType = requestType;
        }
        /// Write @a object
        template <class Object>
        void write(Object const& object)
        {
            encode(tail, object);
        }
        /// Read @a object
        template <class Object>
        Object read()
        {
            Object ret;
            decode(tail, ret);
            return ret;
        }
    };
    /** #MessageCode::ServerReport reflection
     */
    class ServerReportMessage
    : public MessageReflection<MessageCode::ServerReport, 480>
    {
    public:
        /// Header
        struct Header
        : public OperableConcept
        {
            /// Report type
            Encoded<uint, Encoding::Int> reportType;
            /// Operate
            template <class Function>
            inline void operate(Function& foo)
            {
                foo(reportType);
            }
        } h; ///< Header
    public:
        /// Ctor by @a reportType
        ServerReportMessage(uint reportType = 0)
        {
            h.reportType = reportType;
        }
    };
    /** #MessageCode::ActionRequest reflection
     */
    class ActionRequestMessage
    : public MessageReflection<MessageCode::ActionRequest>
    {
    public:
        /// Header
        struct Header
        : public OperableConcept
        {
            /// Message length
            bool hasRaycast = false;

            /// Action time
            Encoded<DiscreteTime, Encoding::Int> time;
            /// Movement type
            ActorMovement movement;

            /// Ray cast direction
            Encoded<float3, Encoding::DirectionSphere> raycastDirection;
            /// Ray cast source
            Encoded<ubyte, Encoding::Byte> raycastSource;
            /// Ray cast type
            Encoded<ubyte, Encoding::Byte> raycastType;
            /// Ray cast time
            Encoded<sbyte, Encoding::Byte> raycastTime;

            /// Operate
            template <class Function>
            inline void operate(Function& foo)
            {
                foo(time);
                foo(movement);

                if (hasRaycast)
                {
                    foo(raycastDirection);
                    foo(raycastSource);
                    foo(raycastType);
                    foo(raycastTime);
                }
            }
        } h; ///< Header
    public:
        /// Impl conditional encoding
        void preDeserialize(uint messageLength)
        {
            uint minSize = NetworkMessage::MinSize + sizeOf(h.time) + sizeOf(h.movement);
            h.hasRaycast = messageLength > minSize;
        }
    public:
        /// Empty ctor
        ActionRequestMessage() = default;
        /// Ctor by action @a time and @a movement
        ActionRequestMessage(DiscreteTime time, 
                             ActorMovement const& movement)
        {
            *h.time = time;
            h.movement = movement;
        }
    };
    /** #MessageCode::RaycastRequest reflection
     */
    class RaycastRequestMessage
    : public MessageReflection<MessageCode::RaycastRequest>
    {
    public:
        /// Header
        struct Header
        : public OperableConcept
        {
            /// Action time
            Encoded<DiscreteTime, Encoding::Int> time;
            /// Ray cast direction
            Encoded<float3, Encoding::DirectionSphere> direction;
            /// Ray cast source
            Encoded<ubyte, Encoding::Byte> source;
            /// Ray cast type
            Encoded<ubyte, Encoding::Byte> type;

            /// Operate
            template <class Function>
            inline void operate(Function& foo)
            {
                foo(time);
                foo(direction);
                foo(source);
                foo(type);
            }
        } h; ///< Header
    public:
        /// Empty ctor
        RaycastRequestMessage() = default;
        /// Ctor by action @a time and @a movement
        RaycastRequestMessage(RaycastInfo const& info)
        {
            *h.time = info.time;
            *h.direction = info.direction;
            *h.source = info.source;
            *h.type = info.type;
        }
    };
    /** #MessageCode::RaycastOrder reflection
     */
    class RaycastOrderMessage
    : public MessageReflection<MessageCode::RaycastOrder>
    {
    public:
        /// Header
        struct Header
        : public OperableConcept
        {
            /// Action time
            Encoded<DiscreteTime, Encoding::Int> time;
            /// Hit info
            Encoded<uint, Encoding::Int> hit;

            /// Operate
            template <class Function>
            inline void operate(Function& foo)
            {
                foo(time);
                foo(hit);
            }
        } h; ///< Header
    public:
        /// Empty ctor
        RaycastOrderMessage() = default;
        /// Ctor by action @a time and @a movement
        RaycastOrderMessage(DiscreteTime time, ObjectId hitObject, ubyte hitType)
        {
            *h.time = time;
            *h.hit = ObjectInfo(hitObject, hitType).encode();
        }
        /// Get hit info
        void get(ObjectId& hitObject, ubyte& hitType) const
        {
            ObjectInfo encoder(*h.hit);
            hitObject = encoder.id;
            hitType = encoder.state;
        }
    };
    /** #MessageCode::CinematicData reflection
     */
    class CinematicDataMessage
    : public MessageReflection<ls::MessageCode::CinematicData, 480>
    {
    public:
        /// Header
        struct Header
        : public OperableConcept
        {
            /// Message time
            Encoded<DiscreteTime, Encoding::Int> time = 0;
            /// Operate
            template <class Function>
            inline void operate(Function& foo)
            {
                foo(time);
            }
        } h; ///< Header
    public:
        /** Create non-chunked message for list of unrelated objects.
         */
        CinematicDataMessage(DiscreteTime time = 0)
            : MessageReflection()
        {
            *h.time = time;
        }

        /** @name Using data
         *      @{
         */
        /** Write action data.
         *  @param clientId 
         *    Acted client ID
         *  @param movement 
         *    Client movement
         *  @return @c true on success, @c false if fill
         */
        bool write(ClientId clientId, ActorMovement const& movement)
        {
            static uint const dataSize = sizeof (ClientId) + sizeOf(ActorMovement());
            if (!tail.canWrite(dataSize))
                return false;

            encode<Encoding::Short>(tail, clientId);
            encode(tail, movement);
            return true;
        }
        /// Get number of cinematic actors
        uint num() const noexcept
        {
            static uint const dataSize = sizeof (ClientId) + sizeOf(ActorMovement());
            return tail.size() / dataSize;
        }
        /// Test if empty
        bool isEmpty() const noexcept
        {
            return tail.remaning() == 0;
        }
        /** Read action data.
         *  @param [out] clientId 
         *    Acted client ID
         *  @param [out] movement 
         *    Client movement
         *  @return @c true on success, @c false if empty
         */
        bool read(ClientId& clientId, ActorMovement& movement)
        {
            static uint const dataSize = sizeof (ClientId) + sizeOf(ActorMovement());
            if (!tail.canRead(dataSize))
                return false;

            decode<Encoding::Short>(tail, clientId);
            decode(tail, movement);
            return true;
        }
        /// @}
    };
    /** #MessageCode::CorrectionData reflection
     */
    class CorrectionDataMessage
    : public MessageReflection<ls::MessageCode::CorrectionData, 480>
    {
    public:
        /// Portion size
        static uint const PortionSize = sizeof (ClientId) + sizeof (DiscreteTime) + 12;
        /** Create non-chunked message for list of unrelated objects.
         */
        CorrectionDataMessage()
            : MessageReflection()
        {
        }

        /** @name Using data
         *  @{
         */
        /** Write action data.
         *  @param clientId 
         *    Corrected client ID
         *  @param time 
         *    Sample time
         *  @param position 
         *    Sample position
         *  @return @c true on success, @c false if fill
         */
        bool write(ClientId clientId, DiscreteTime time, double3 const& position)
        {
            if (!tail.canWrite(PortionSize))
                return false;

            encode<Encoding::Short>(tail, clientId);
            encode<Encoding::Int>(tail, time);
            encodeObject(Encoding::VectorFixed3, position, tail);
            return true;
        }
        /// Get number of cinematic actors
        uint num() const noexcept
        {
            return tail.size() / PortionSize;
        }
        /// Test if empty
        bool isEmpty() const noexcept
        {
            return !tail.canRead(PortionSize);
        }
        /// Test if full
        bool isFull() const noexcept
        {
            return !tail.canWrite(PortionSize);
        }
        /** Read action data
         *  @param [out] clientId 
         *    Corrected client ID
         *  @param [out] time 
         *    Sample time
         *  @param [out] position 
         *    Sample position
         *  @return @c true on success, @c false if empty
         */
        bool read(ClientId& clientId, DiscreteTime& time, double3& position)
        {
            if (!tail.canRead(PortionSize))
                return false;

            decode<Encoding::Short>(tail, clientId);
            decode<Encoding::Int>(tail, time);
            decode<Encoding::VectorFixed3>(tail, position);
            return true;
        }
        /// @}
    };
    /** #MessageCode::DynamicData reflection
     */
    class DynamicDataMessage
    : public MessageReflection<ls::MessageCode::DynamicData, 480>
    {
    public:
        /// Unspecified chunk (const)
        static constexpr sint UnspecifiedChunk = MinShort;
        /// Unspecified time
        static constexpr DiscreteTime UnspecifiedTime = 0;
        /// Flags
        enum EnumFlags
        {
            /** Set if this message is related to chunk, reset if chunk is invalid
             */
            ChunkedFlag = 1 << 0,
            /** Set if this message is related to time, reset if time is invalid
             */
            TimedFlag = 1 << 1,
            /** Set if this message should cause chunk clearing
             */
            ClearFlag = 1 << 2,
            /** Set if this message is last of messages with same chunk and same time
             */
            LastFlag = 1 << 3,
            /** Set if this message contains tiny object data and may be lost without world corruption
             */
            WeakFlag = 1 << 4
        };
    public:
        /// Header
        struct Header
        : public OperableConcept
        {
            /// Message time
            Encoded<DiscreteTime, Encoding::Int> time = UnspecifiedTime;
            /// Chunk
            Encoded<int2, Encoding::VectorShort> chunk = int2(UnspecifiedChunk);
            /// Flags
            Encoded<ubyte, Encoding::Byte> flags = 0;
            /// Operate
            template<class Function>
            inline void operate(Function& foo)
            {
                foo(time);
                foo(chunk);
                foo(flags);
            }
        } h; ///< Header
    public:
        /** Create non-chunked message for list of unrelated objects.
         */
        DynamicDataMessage(DiscreteTime time = 0)
        {
            *h.flags = LastFlag;
            *h.time = time;
            if (time > 0)
                *h.flags |= TimedFlag;
        }
        /** Create chunked message.
         *  @param chunk 
         *    Chunk to update
         *  @param time 
         *    Message time, set 0 to reset #TimedFlag
         *  @param needClear 
         *    Sets #ClearFlag
         *  @param isLast 
         *    Sets #LastFlag
         *  @param isWeak 
         *    Sets #WeakFlag 
         */
        DynamicDataMessage(int2 const& chunk, DiscreteTime time,
                           bool needClear, bool isLast, bool isWeak = false)
        {
            h.time = time;
            h.chunk = chunk;
            h.flags = ChunkedFlag;
            if (time > 0)
                *h.flags |= TimedFlag;
            if (needClear)
                *h.flags |= ClearFlag;
            if (isLast)
                *h.flags |= LastFlag;
            if (isWeak)
                *h.flags |= WeakFlag;
        }

        /** @name Gets
         *  @{
         */
        /// Test if chunk needs clear
        bool needClear() const noexcept
        {
            return !!(*h.flags & ClearFlag);
        }
        /// Test if last
        bool isLast() const noexcept
        {
            return !!(*h.flags & LastFlag);
        }
        /// Test if chunked
        bool isChunked() const noexcept
        {
            return !!(*h.flags & ChunkedFlag);
        }
        /// Test if weak
        bool isWeak() const noexcept
        {
            return !!(*h.flags & WeakFlag);
        }
        /// Test if any data is contained.
        bool isEmpty() const noexcept
        {
            return tail.size() == 0;
        }
        /// @}

        /** @name Using data
         *  @{
         */
        /** Write object info.
         *  @param data 
         *    Object data
         *  @param dataSize 
         *    Object data size
         *  @return @c true on success, @c false otherwise
         */
        bool write(const void* data, uint dataSize)
        {
            if (!tail.canWrite(dataSize))
                return false;

            tail.write(data, dataSize);
            return true;
        }
        /// Get @b all objects info.
        const ubyte* data() const noexcept
        {
            return tail.constData();
        }
        /// Get objects info size
        uint dataSize() const noexcept
        {
            return tail.numWritten();
        }
        /// @}
    };
    /** #MessageCode::EmptyData reflection
     */
    class EmptyDataMessage
    : public MessageReflection<MessageCode::EmptyData, 480>
    {
    public:
        /** Ctor.
         */
        EmptyDataMessage()
        {
        }

        /** @name Using data
         *  @{
         */
        /// Write @a chunk, return @c true on success
        bool write(int2 const& chunk)
        {
            if (!tail.canWrite(4))
                return false;

            encode<Encoding::VectorShort>(tail, chunk);
            return true;
        }
        /// Get number of chunks
        uint numChunks() const noexcept
        {
            return tail.numWritten() / 4;
        }
        /// Read chunk.
        int2 read()
        {
            debug_assert(tail.canRead(4));

            int2 chunk;
            decode<Encoding::VectorShort>(tail, chunk);
            return chunk;
        }
        /// @}
    };
    /** #MessageCode::RaycastData reflection
     */
    class RaycastDataMessage
        : public MessageReflection<MessageCode::RaycastData, 480>
    {
    public:
        /// Time precision
        static sint const timePrecision = 10;
        /// Size of portion
        static uint portionSize()
        {
            static uint const size 
                = sizeOf<Encoding::DirectionSphere, float3>() 
                + sizeof(uint) 
                + 2 * sizeof(ubyte);
            return size;
        }
        /// Header
        struct Header
            : public OperableConcept
        {
            /// Message time
            Encoded<DiscreteTime, Encoding::Int> time = 0;
            /// Operate
            template <class Function>
            inline void operate(Function& foo)
            {
                foo(time);
            }
        } h; ///< Header
    public:
        /** Ctor.
         */
        RaycastDataMessage(DiscreteTime time = 0)
        {
            *h.time = time;
        }

        /** @name Using data
         *  @{
         */
        /// Write data
        bool write(float3 const& direction,
                   ObjectId object, ubyte source, ubyte castType,
                   DiscreteTime time,
                   DiscreteTime refTime)
        {
            if (!tail.canWrite(portionSize()))
                return false;

            sint fixedTime = sint(time - refTime) / timePrecision;

            encode<Encoding::Int>(tail, ObjectInfo(object, source).encode());
            encode<Encoding::DirectionSphere>(tail, direction);
            encode<Encoding::Byte>(tail, castType);
            encode<Encoding::Byte>(tail, (sbyte) fixedTime);
            return true;
        }
        /// Get number of chunks
        uint numRays() const noexcept
        {
            return tail.numWritten() / portionSize();
        }
        /// Read data
        void read(float3& direction, ObjectId& object, ubyte& source, ubyte& castType, DiscreteTime& time)
        {
            debug_assert(tail.canRead(portionSize()));

            // Object and source
            uint encodedCaster;
            decode<Encoding::Int>(tail, encodedCaster);
            ObjectInfo objectAndSource(encodedCaster);

            decodeObject(Encoding::DirectionSphere, direction, tail);
            decode<Encoding::Byte>(tail, castType);

            // Time
            sbyte encodedTime;
            decode<Encoding::Byte>(tail, encodedTime);

            object = objectAndSource.id;
            source = objectAndSource.state;
            time = *h.time + encodedTime * timePrecision;
        }
        /// @}
    };
    /** #MessageCode::RaytestRequest reflection
     */
    class RaytestRequestMessage
        : public MessageReflection<MessageCode::RaytestRequest>
    {
    public:
        /// Header
        struct Header
            : public OperableConcept
        {
            /// Action time
            Encoded<DiscreteTime, Encoding::Int> time;
            /// Ray cast direction
            Encoded<float3, Encoding::DirectionSphere> direction;
            /// Ray cast object and source
            Encoded<ClientId, Encoding::Short> caster;
            /// Ray cast source
            Encoded<ubyte, Encoding::Byte> source;
            /// Ray cast type
            Encoded<ubyte, Encoding::Byte> type;

            /// Operate
            template <class Function>
            inline void operate(Function& foo)
            {
                foo(time);
                foo(direction);
                foo(caster);
                foo(source);
                foo(type);
            }
        } h; ///< Header
    public:
        /// Empty ctor
        RaytestRequestMessage() = default;
        /// Ctor by raycast @a info
        RaytestRequestMessage(RaycastInfo const& info)
        {
            *h.time = info.time;
            *h.direction = info.direction;
            *h.caster = info.invoker;
            *h.source = info.source;
            *h.type = info.type;
        }
        /// Get all
        void get(RaycastInfo& info) const
        {
            info.invoker = *h.caster;
            info.time = *h.time;
            info.object = InvalidObjectId;
            info.source = *h.source;
            info.type = *h.type;
            info.direction = *h.direction;
        }
    };
    /** #MessageCode::RaytestHitSure and #MessageCode::RaytestHitWeak reflection
     */
    class RaytestHitBase
    {
    public:
        /// Header
        struct Header
            : public OperableConcept
        {
            /// Action time
            Encoded<DiscreteTime, Encoding::Int> time;
            /// Ray cast object and source
            Encoded<ClientId, Encoding::Short> caster;
            /// Hit object and hit type
            Encoded<uint, Encoding::Int> hit;

            /// Operate
            template <class Function>
            inline void operate(Function& foo)
            {
                foo(time);
                foo(caster);
                foo(hit);
            }
        } h; ///< Header
    public:
        /// Empty ctor
        RaytestHitBase() = default;
        /// Ctor by caster and hit
        RaytestHitBase(DiscreteTime time, ClientId caster,
                       ObjectId hitObject, ubyte hitType)
        {
            *h.time = time;
            *h.caster = caster;
            *h.hit = ObjectInfo(hitObject, hitType).encode();
        }
        /// Test sender
        bool testCaster(DiscreteTime time, ClientId caster) const
        {
            return *h.time == time && *h.caster == caster;
        }
        /// Get
        void get(ObjectId& hitObject, ubyte& hitType) const
        {
            ObjectInfo decoder(*h.hit);
            hitObject = decoder.id;
            hitType = decoder.state;
        }
    };
    /** #MessageCode::RaytestHitSure reflection
     */
    class RaytestHitSureMessage
        : public MessageReflection<MessageCode::RaytestHitSure>
        , public RaytestHitBase
    {
    public:
        using RaytestHitBase::RaytestHitBase;
        using RaytestHitBase::h;
    };
    /** #MessageCode::RaytestHitWeak reflection
     */
    class RaytestHitWeakMessage
        : public MessageReflection<MessageCode::RaytestHitWeak>
        , public RaytestHitBase
    {
    public:
        using RaytestHitBase::RaytestHitBase;
        using RaytestHitBase::h;
    };
    /** #MessageCode::RaytestDullSure and #MessageCode::RaytestDullWeak reflection
     */
    class RaytestDullBase
    {
    public:
        /// Header
        struct Header
            : public OperableConcept
        {
            /// Action time
            Encoded<DiscreteTime, Encoding::Int> time;
            /// Ray cast object and source
            Encoded<ClientId, Encoding::Short> caster;
            /// Hit chunk
            Encoded<int2, Encoding::VectorShort> chunk;

            /// Operate
            template <class Function>
            inline void operate(Function& foo)
            {
                foo(time);
                foo(caster);
                foo(chunk);
            }
        } h; ///< Header
    public:
        /// Empty ctor
        RaytestDullBase() = default;
        /// Ctor by caster and hit
        RaytestDullBase(DiscreteTime time, ClientId caster,
                        int2 const& chunk)
        {
            *h.time = time;
            *h.caster = caster;
            *h.chunk = chunk;
        }
        /// Test sender
        bool testCaster(DiscreteTime time, ClientId caster) const
        {
            return *h.time == time && *h.caster == caster;
        }
    };
    /** #MessageCode::RaytestDullSure reflection
     */
    class RaytestDullSureMessage
        : public MessageReflection<MessageCode::RaytestDullSure>
        , public RaytestDullBase
    {
    public:
        using RaytestDullBase::RaytestDullBase;
        using RaytestDullBase::h;
    };
    /** #MessageCode::RaytestDullWeak reflection
     */
    class RaytestDullWeakMessage
        : public MessageReflection<MessageCode::RaytestDullWeak>
        , public RaytestDullBase
    {
    public:
        using RaytestDullBase::RaytestDullBase;
        using RaytestDullBase::h;
    };
    /** #MessageCode::RaytestMiss reflection
     */
    class RaytestMissMessage
        : public MessageReflection<MessageCode::RaytestMiss>
    {
    public:
        /// Header
        struct Header
            : public OperableConcept
        {
            /// Action time
            Encoded<DiscreteTime, Encoding::Int> time;
            /// Ray cast object and source
            Encoded<ClientId, Encoding::Short> caster;

            /// Operate
            template <class Function>
            inline void operate(Function& foo)
            {
                foo(time);
                foo(caster);
            }
        } h; ///< Header
    public:
        /// Empty ctor
        RaytestMissMessage() = default;
        /// Ctor by caster
        RaytestMissMessage(DiscreteTime time, ClientId caster)
        {
            *h.time = time;
            *h.caster = caster;
        }
        /// Test sender
        bool testCaster(DiscreteTime time, ClientId caster) const
        {
            return *h.time == time && *h.caster == caster;
        }
    };
    /** Write objects to messages.
     *  
     */
    template <class Message, class Iterator
    , class MessageFactory, class MessageFlusher
    , class ObjectProcessor, class ObjectPutter
    , class PreAction, class PostAction
    >
    inline void writeObjectData(Iterator begin, Iterator end,
                                MessageFactory constructMessage, MessageFlusher flush,
                                ObjectProcessor process, ObjectPutter put,
                                PreAction preAct, PostAction postAct)
    {
        // Nothing to do
        if (begin == end)
            return;
        // First object's chunk
        Message message = constructMessage();
        preAct(message);
        for (Iterator iterObject = begin; iterObject != end; ++iterObject)
        {
            auto& object = *iterObject;
            // Write
            process(object);
            // Put info
            if (!put(message, object))
            {
                // Full
                flush(message);
                // Reset
                message = constructMessage();
                // Put info
                bool success = !!put(message, object);
                debug_assert(success);
            }
        }
        // Tail
        postAct(message);
        flush(message);
    }
    /** Read objects from message.
     */
    template <class Message, class Callback, class Factory>
    void readObjectInfo(Message const& message, Factory& factory, Callback callback)
    {
        auto time = *message.h.time;
        auto data = message.data();
        uint remaning = message.dataSize();
        while (remaning > 0)
        {
            ObjectState frame;
            auto numRead = factory.decode(frame, data, remaning);

            LS_THROW(numRead != 0,
                     InvalidDataException,
                     "Fail");

            frame.resetTime(time);
            callback(frame);

            remaning -= numRead;
            data += numRead;
        }
    }
    /// @}
}