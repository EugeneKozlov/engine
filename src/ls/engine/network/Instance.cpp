#include "pch.h"
#include "ls/engine/network/Instance.h"
#include "ls/engine/network/reflection.h"
#include "ls/engine/server/Server.h"
#include "ls/engine/client/Client.h"
#include "ls/engine/app/config.h"
#include "ls/simulation/Simulation.h"

using namespace ls;
Instance::Instance(ObjectsFactory& factory,
                   shared_ptr<StreamingWorld> world,
                   string const& address, ushort port,
                   Web::Type type,
                   uint connectionTimeout,
                   const char* name)
    : m_factory(factory)
    , m_world(world)
    , m_web(connectionTimeout, address, port, type)

    , m_name(name)
    , m_server()
    , m_client()

    , m_networkTimer(*config().network().updatePeriod, true)
    , m_pingTimer(*config().network().pingPeriod, true)
    , m_speedTimer(*config().network().speedPeriod, true)
    , m_flushTimer(*config().network().flushPeriod, true)
    , m_udpReestablisherTimer(*config().network().udpPeriod, false)

{
    m_web.onAddNode = bind(&Instance::registerClient, this, _1);
    m_web.onRemoveNode = bind(&Instance::deregisterClient, this, _1);
    m_web.onReceiveTcp = bind(&Instance::processTcpMessage, this, _1, _2);
    m_web.onReceiveUdp = bind(&Instance::processUdpMessage, this, _1, _2);

    if (m_web.isRoot())
    {
        m_server = make_unique<Server>(m_web, m_factory);
        m_server->start();
    }
    m_client = make_unique<Client>(m_web, m_factory, *m_world);
}
Instance::~Instance()
{
}



void Instance::registerClient(ClientId id)
{
    if (m_server)
        m_server->addNode(id);
    m_client->addNode(id);
}
void Instance::deregisterClient(ClientId id)
{
    if (m_server)
        m_server->removeNode(id);
    m_client->removeNode(id);
}

void Instance::processTcpMessage(ClientId id, NetworkMessage const& message)
{
    if (m_server)
        if (!m_server->processTcpMessgae(id, message))
            m_server->processUdpMessgae(id, message);
    if (!m_client->processTcpMessage(id, message))
        m_client->processUdpMessage(id, message);
}
void Instance::processUdpMessage(ClientId id, NetworkMessage const& message)
{
    if (m_server)
        m_server->processUdpMessgae(id, message);
    m_client->processUdpMessage(id, message);
}



bool Instance::update(DeltaTime deltaTime)
{
    // Check
    if (!isValid())
        return false;

    // Update
    bool needUpdate = m_client->isSimulated();
    if (needUpdate)
    {
        m_client->sim().advance(deltaTime);
        m_client->sim().flush();
    }
    m_web.update();
    if (m_server)
    {
        m_server->update(deltaTime);
        m_web.update();
        if (needUpdate)
        {
            m_client->sim().forceTime(m_server->time());
        }
    }
    if (needUpdate)
    {
        m_client->sim().simulate();
    }

    // Timing
    m_networkTimer.advance(deltaTime);
    m_pingTimer.advance(deltaTime);
    m_speedTimer.advance(deltaTime);
    m_udpReestablisherTimer.advance(deltaTime);
    m_flushTimer.advance(deltaTime);

    // Network period
    if (!m_networkTimer.alarm())
        return true;
    // Re-establish UDP
    if (m_udpReestablisherTimer.alarm())
        m_web.reestablishUdp();
    // Update
    if (!m_web.update())
        return false;
    // Update root connection
    if (m_pingTimer.alarm() && isServer())
    {
        m_web.pingAll();
        //m_server->relocate();
    }
    // Output
    if (m_flushTimer.alarm())
    {
        if (m_server)
            m_server->flush();
        // Flush output
        m_web.flush();
    }
    // Finalize
    if (!m_web.update())
        return false;

    // Speed
    if (m_speedTimer.alarm())
    {
        m_inputTcp = m_web.inputTcp() * 1000 / m_speedTimer.period();
        m_inputUdp = m_web.inputUdp() * 1000 / m_speedTimer.period();
        m_outputTcp = m_web.outputTcp() * 1000 / m_speedTimer.period();
        m_outputUdp = m_web.outputUdp() * 1000 / m_speedTimer.period();
        m_web.resetTraffic();
    }
    return true;

}
void Instance::move(int2 const& position)
{
    if (m_lastPosition == position)
        return;
    m_lastPosition = position;
    // Send to serevr
    MoveRequestMessage message(position, (ushort) *config().client().simDistance);
    m_web.sendTcp(NetRootClient, send(MoveRequestMessage(message)));
}



bool Instance::isValid() const
{
    return m_web.isValid();
}
