/** @file ls/engine/network/message.h
 *  @brief Client/server network message IDs
 */
#pragma once
#include "ls/common.h"
#include "ls/network/message.h"

namespace ls
{
    /** @addtogroup LsEngineNetwork
     *  @{
     */
    namespace MessageCode
    {
        /// Internal
        enum EnumClientServer : MessageId
        {
            /** Start simulation message.
             *
             *  <b>Header:</b>
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 4     | u16u8u8   |                           |
             *  | 4     | u32       | Initial time (in ms)      |
             *  | ?     | ?         | Simulation parameters     |
             */
            Start = NumCommonMessage,
            /** Stop simulation message.
             *
             *  <b>Header:</b>
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 4     | u16u8u8   |                           |
             */
            Stop,
            /** Time and ping message.
             *
             *  <b>Header:</b>
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 4     | u16u8u8   |                           |
             *  | 4     | u32       | Current time (in ms)      |
             *  | 4     | u32       | Receiver ping (in ms)     |
             */
            Time,
            /** Time and ping all nodes data.
             *
             *  <b>Header:</b>
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 4     | u16u8u8   |                           |
             *
             *  <b>Per node:</b>
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 2     | ClientId  | Client ID                 |
             *  | 2     | u16       | Client ping (in ms)       |
             */
            PingData,
            /// @}

            /** Client movement request.
             *  Redistributes affection areas.
             *
             *  <b>Header:</b>
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 4     | u16u8u8   |                           |
             *  | 4     | s16[2]    | Position (in chunks)      |
             *  | 2     | u16       | Sim distance (in chunks)  |
             */
            MoveRequest,
            /** Move client to other chunk.
             *
             *  <b>Header:</b>
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 4     | u16u8u8   |                           |
             *  | 4     | s16[2]    | Position (in chunks)      |
             *  | 2     | u16       | Sim distance (in chunks)  |
             *  | 2     | ClientId  | Client ID                 |
             */
            MoveReport,
            /** Client action request.
             *  Transmit some request data to server.
             *
             *  <b>Header:</b>
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 4     | u16u8u8   |                           |
             *  | 4     | u32       | Request type ID           |
             *  | ?     | u8[?]     | Any data                  |
             */
            ClientRequest,
            /** Server report.
             *  Transmit some data from server.
             *
             *  <b>Header:</b>
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 4     | u16u8u8   |                           |
             *  | 4     | u32       | Report type ID            |
             *  | ?     | u8[?]     | Any data                  |
             */
            ServerReport,
            /** Client action request.
             *
             *  <b>Header:</b>
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 4     | u16u8u8   |                           |
             *  | 4     | u32       | Action time               |
             *  | 22    | Movement  | Movement                  |
             *  | 7     | RcRequest | Ray-cast request          |
             *
             *  <b>Movement:</b>
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 12    | fix3[3]   | Actor position            |
             *  | 2     | u16       | Movement type             |
             *  | 1     | u8        | Movement flags            |
             *  | 1     | u8        | Movement delay in ticks   |
             *  | 6     | u16[3]    | Movement base direction   |
             *
             *  <b>Ray-cast request:</b>
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 4     | u16[2]    | Ray cast direction        |
             *  | 1     | u8        | Ray cast source, e.g. gun |
             *  | 1     | u8        | Ray cast type             |
             *  | 1     | s8        | Ray cast time (relative)  |
             */
            ActionRequest,
            /** Ray-cast request.
             *
             *  <b>Header:</b>
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 4     | u16u8u8   |                           |
             *  | 4     | u32       | Ray-cast time             |
             *  | 4     | u16[2]    | Ray direction             |
             *  | 1     | u8        | Ray-cast source           |
             *  | 1     | u8        | Ray-cast type             |
             *
             *  @see ActionRequest
             */
            RaycastRequest,
            /** Ray-cast order.
             *
             *  <b>Header:</b>
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 4     | u16u8u8   |                           |
             *  | 4     | u32       | Ray-cast time             |
             *  | 4     | u24u8     | Hit object and hit type   |
             *
             *  @see ActionRequest
             */
            RaycastOrder,

            /** Cinematic objects data.
             *
             *  Contains each actor movement data
             *
             *  <b>Header:</b>
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 4     | u16u8u8   |                           |
             *  | 4     | u32       | Action time               |
             *
             *  <b>Per actor:</b>
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 2     | ClientId  | Actor ID                  |
             *  | 22    | Movement  | Action movement           |
             *
             *  @see ActionRequest
             */
            CinematicData,
            /** Cinematic correction data.
             *
             *  <b>Header:</b>
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 4     | u16u8u8   |                           |
             *
             *  <b>Per actor:</b>
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 2     | ClientId  | Actor ID                  |
             *  | 4     | u32       | Correction time           |
             *  | 12    | fix3[3]   | Actor position            |
             */
            CorrectionData,
            /** Dynamic objects data.
             *
             *   There are two sort of such messages:
             *
             *   - <b>Server -> Any</b> (strong)
             *     + Have to be transmitted by TCP
             *     + Have to contain all object data
             *     + May contain time
             *
             *   - <b>Support -> Any</b> (weak)
             *     + Mustn't contain fat data
             *     + Can be transmitted by either TCP or UDP
             *     + Have to contain valid time and chunk
             *
             *  So:
             *  Server should ignore @b strong messages received from root.
             *  Client should ignore @b weak messages received from self.
             *
             *  <b>Header:</b>
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 4     | u16u8u8   |                           |
             *  | 4     | u32       | Message time              |
             *  | 4     | s16[2]    | Updated chunk             |
             *  | 1     | u8        | Flags                     |
             *
             *  <b>Per object:</b>
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | ?     | u8[?]     | Encoded object data       |
             */
            DynamicData,
            /** Clear specified chunks.
             *
             *  <b>Header:</b>
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 4     | u16u8u8   |                           |
             *
             *  <b>Per chunk:</b>
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 4     | s16[2]    | Updated chunk             |
             */
            EmptyData,
            /** Ray-cast data broadcast
             *
             *  <b>Header:</b>
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 4     | u16u8u8   |                           |
             *  | 4     | u32       | Base ray-cast time        |
             *
             *  <b>Per ray:</b>
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 4     | u24u8     | Caster object and source  |
             *  | 4     | u16[2]    | Ray direction             |
             *  | 1     | u8        | Ray-cast type             |
             *  | 1     | s8        | Ray-cast time (relative)  |
             */
            RaycastData,

            /** Ray test request to super client.
             *
             *  <b>Header:</b>
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 4     | u16u8u8   |                           |
             *  | 4     | u32       | Ray-cast time             |
             *  | 4     | u16[2]    | Ray direction             |
             *  | 2     | ClientId  | Caster client             |
             *  | 1     | u8        | Ray-cast source           |
             *  | 1     | u8        | Ray-cast type             |
             */
            RaytestRequest,
            /** Guaranteed object hit.
             *
             *  <b>Header:</b>
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 4     | u16u8u8   |                           |
             *  | 4     | u32       | Ray-cast time             |
             *  | 2     | ClientId  | Caster client             |
             *  | 4     | u24u8     | Hit object and hit type   |
             */
            RaytestHitSure,
            /** Non-guaranteed object hit.
             *
             *  @see RaytestHitSure
             */
            RaytestHitWeak,
            /** Ray miss.
             *
             *  <b>Header:</b>
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 4     | u16u8u8   |                           |
             *  | 4     | u32       | Ray-cast time             |
             *  | 2     | ClientId  | Caster client             |
             */
            RaytestMiss,
            /** Guaranteed static geometry hit.
             *
             *  <b>Header:</b>
             *  | Bytes | Type      | Description               |
             *  |:------|:----------|:--------------------------|
             *  | 4     | u16u8u8   |                           |
             *  | 4     | u32       | Ray-cast time             |
             *  | 2     | ClientId  | Caster client             |
             *  | 4     | s16[2]    | Hit chunk                 |
             */
            RaytestDullSure,
            /** Non-guaranteed static geometry hit.
             *
             *  @see RaytestDullSure
             */
            RaytestDullWeak,
        };
    };
    /// @}
}