/** @file ls/engine/network/Instance.h
 *  @brief Instance
 */

#pragma once
#include "ls/common.h"
#include "ls/forward.h"
#include "ls/core/Timer.h"
#include "ls/network/Web.h"
#include "ls/engine/forward.h"

namespace ls
{
    class StreamingWorld;

    /** Instance
     */
    class Instance
    {
    public:
        /** Ctor
         */
        Instance(ObjectsFactory& factory,
                 shared_ptr<StreamingWorld> world,
                 string const& address, ushort port,
                 Web::Type type,
                 uint connectionTimeout,
                 const char* name);
        /// Dtor
        virtual ~Instance();
        /** Update client.
         *  @param deltaTime
         *    Time increment
         *  @return @c true on success, @c false if client is invalid.
         *    Invalid client should be destroyed.
         */
        bool update(DeltaTime deltaTime);
        /** Move distribution to new position.
         *  @param position
         *    New client position
         */
        void move(int2 const& position);

        /** @name Gets
         *  @{
         */
        /// Test if client is valid
        bool isValid() const;
        /// Test if server
        bool isServer() const noexcept
        {
            return !!m_server;
        }
        /// Get server
        Server& server() noexcept
        {
            return *m_server;
        }
        /// Get client
        Client& client() noexcept
        {
            return *m_client;
        }
        /// Get web
        Web& web() noexcept
        {
            return m_web;
        }
        /// Get input TCP speed (kbytes/sec)
        double inputSpeedTcp() const noexcept
        {
            return m_inputTcp / 1024.0;
        }
        /// Get input UDP speed (kbytes/sec)
        double inputSpeedUdp() const noexcept
        {
            return m_inputUdp / 1024.0;
        }
        /// Get output TCP speed (kbytes/sec)
        double outputSpeedTcp() const noexcept
        {
            return m_outputTcp / 1024.0;
        }
        /// Get output UDP speed (kbytes/sec)
        double outputSpeedUdp() const noexcept
        {
            return m_outputUdp / 1024.0;
        }
        /// @}
    private:
        void registerClient(ClientId id);
        void deregisterClient(ClientId id);
        void processTcpMessage(ClientId id, NetworkMessage const& message);
        void processUdpMessage(ClientId id, NetworkMessage const& message);
    private:
        ObjectsFactory& m_factory;
        shared_ptr<StreamingWorld> m_world;
        Web m_web;
        string m_name;

        unique_ptr<Server> m_server;
        unique_ptr<Client> m_client;

        Timer m_networkTimer;
        Timer m_pingTimer;
        Timer m_speedTimer;
        Timer m_flushTimer;
        Timer m_udpReestablisherTimer;

        int2 m_lastPosition = GhostChunk;

        ulong m_inputTcp = 0;
        ulong m_inputUdp = 0;
        ulong m_outputTcp = 0;
        ulong m_outputUdp = 0;
    };
}

