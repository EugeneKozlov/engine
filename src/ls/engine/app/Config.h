/// @file ls/engine/app/Config.h
/// @brief Engine configuration storage and loader
#pragma once

#include "ls/common.h"
#include "ls/app/ConfigManager.h"

#include "ls/core/LoggerConfig.h"
#include "ls/world/WorldStreamingConfig.h"
#include "ls/render/GraphicConfig.h"
#include "ls/engine/server/ServerConfig.h"
#include "ls/engine/client/ClientConfig.h"
#include "ls/engine/network/NetworkConfig.h"

namespace ls
{
    /// @addtogroup LsEngineApp
    /// @{

    /// @brief All configs storage
    class Config
        : public ConfigManager
    <
        LoggerConfig,
        WorldStreamingConfig,
        WorldStreamingConfig,
        WorldStreamingConfig,
        GraphicConfig,
        ServerConfig,
        ClientConfig,
        NetworkConfig
    >
    {
    public:
        /// @brief Get instance.
        /// @return Singleton instance
        static Config& instance() noexcept
        {
            static Config config;
            return config;
        }
    public:
        /// @brief Get Logging Config
        const LoggerConfig& logger() const noexcept
        {
            return get<LoggerConfig, 0>();
        }
        /// @brief Get World data Streaming Config
        const WorldStreamingConfig& worldStreaming() const noexcept
        {
            return get<WorldStreamingConfig, 2>();
        }
        /// @brief Get Renderer Config
        const GraphicConfig& graphic() const noexcept
        {
            return get<GraphicConfig, 4>();
        }
        /// @brief Get Server Config
        const ServerConfig& server() const noexcept
        {
            return get<ServerConfig, 5>();
        }
        /// @brief Get Client Config
        const ClientConfig& client() const noexcept
        {
            return get<ClientConfig, 6>();
        }
        /// @brief Get Network Config
        const NetworkConfig& network() const noexcept
        {
            return get<NetworkConfig, 7>();
        }
        /// @}
    };
    /// @brief Get config
    inline Config& config()
    {
        return Config::instance();
    }
    /// @}
}
