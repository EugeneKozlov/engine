/// @file ls/world/LoggerConfig.h
/// @brief Logging Config
#pragma once

#include "ls/common.h"
#include "ls/app/ConfigManager.h"

namespace ls
{
    /// @addtogroup LsCore
    /// @{

    /// @brief Logging Config
    class LoggerConfig
        : public ConfigPrototype
    {
    public:
        /// @brief Singleton
        static LoggerConfig& instance()
        {
            static LoggerConfig inst;
            return inst;
        }
        /// @brief Compute all
        void compute()
        {
            instance() = *this;
        }
        /// @brief Operate
        template <class Operator>
        void operate(Operator op)
        {
            op(logObjectManagerObject);
            op(logObjectManagerConstantObject);
            op(logObjectManagerManagement);
        }
    public:
        /// @brief Log Object Manager object messages
        ConfigProperty<bool> logObjectManagerObject
            = { "logger|omObject" };
        /// @brief Log Object Manager constant objects (stored in region)
        ConfigProperty<bool> logObjectManagerConstantObject
            = { "logger|omConstantObject" };
        /// @brief Log Object Manager object movement messages
        ConfigProperty<bool> logObjectManagerMovement
            = { "logger|omMovement" };
        /// @brief Log Object Manager managemant messages
        ConfigProperty<bool> logObjectManagerManagement
            = { "logger|omManagement" };
    };

    /// @brief Declare conditional logger function
#define LS_DECLARE_LOGGER(CLASS, NAME, VERBOSITY)       \
public:                                                 \
    template <class ... Args>                           \
    inline void log##NAME(const Args& ... args)         \
    {                                                   \
        if (*LoggerConfig::instance().log##CLASS##NAME) \
            log##VERBOSITY(args...);                    \
    }
    /// @}
}