/// @file ls/core/UniformRandom.h
/// @brief Uniform random generator
#pragma once

#include "ls/common.h"
#include <random>

namespace ls
{
    /// @addtogroup LsCore
    /// @{

    /// @brief Uniform Random generator
    class UniformRandom
    {
    public:
        /// @brief Default ctor
        UniformRandom() = default;
        /// @brief Ctor
        UniformRandom(const uint seed)
            : generator(seed)
        {
        }
        /// @brief Get random number
        uint random()
        {
            return generator();
        }
        /// @brief Get random integer
        sint randomInt(sint min, sint max)
        {
            if (min > max)
                std::swap(min, max);
            std::uniform_int_distribution<sint> uniformInt(min, max);
            return uniformInt(generator);
        }
        /// @brief Get random real
        float randomReal(float min, float max)
        {
            if (min > max)
                std::swap(min, max);
            std::uniform_real_distribution<float> uniformReal(min, max);
            return uniformReal(generator);
        }
        /// @brief Get random real 0..1
        float randomReal_01()
        {
            return randomReal(0.0, 1.0);
        }
        /// @brief Get random real -1..1
        float randomReal_11()
        {
            return randomReal(-1.0, 1.0);
        }
    private:
        std::mt19937 generator;
    };
    /// @}
}

