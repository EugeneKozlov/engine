/// @file ls/core/StackAllocator.h
/// @brief Stack Allocator
#pragma once

#include "ls/core/Blob.h"
#include "ls/core/StackAllocatorBase.h"

namespace ls
{
    /// @addtogroup LsCore
    /// @{
    
    /// @brief Stack Allocator
    class StackAllocator
        : Noncopyable
    {
    public:
        /// @brief Resize factor
        static const uint ResizeFactor = 2;
    public:
        /// @brief Ctor
        /// @param bucketSize
        ///   Bucket size
        /// @param alignment
        ///   Data alignment
        StackAllocator(const uint bucketSize, const uint alignment = 16);
        /// @brief Dtor
        ~StackAllocator();
        /// @brief Allocate memory
        /// @param size
        ///   Memory size
        Blob allocate(const uint size);
        /// @brief Release momory
        void release();
        /// @brief Purge momory
        void purge();
    private:
        struct BucketDesc
        {
            BucketDesc(const uint alignment)
                : alignment(alignment)
            {
            }
            uint alignment;
        };
        struct Bucket : DisposableConcept
        {
            Bucket(const BucketDesc& desc, const uint size)
                : m_storage(new ubyte[size + desc.alignment])
            {
                uint alignedSize = size + desc.alignment;
                m_data = m_storage;
                std::align(desc.alignment, size, *(void**) &m_data, alignedSize);
                debug_assert(alignedSize >= size);
                debug_assert(uint(m_data) % desc.alignment == 0);
            }
            void* data()
            {
                return m_data;
            }
            void commit()
            {
            }
            void dispose()
            {
                delete[] m_storage;
                m_storage = nullptr;
                m_data = nullptr;
            }
        private:
            ubyte* m_storage = nullptr;
            ubyte* m_data = nullptr;
        };
    private:
        StackAllocatorBase<Bucket, BucketDesc> impl;
        uint m_alignment;
    };
    /// @}
}
