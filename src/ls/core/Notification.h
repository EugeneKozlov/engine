/// @file ls/core/Notification.h
/// @brief Notification concept
#pragma once

#include "ls/common.h"
#include <boost/variant.hpp>

namespace ls
{
    /// @addtogroup LsCore
    /// @{

    /// @brief Notification template
    template <class ... Args>
    using NotificationT = boost::variant<Args...>;
    /// @brief Notification Receiver concept
    template <class MyType, class Notif>
    class NotificationReceiver
        : public boost::static_visitor<>
    {
    public:
        /// @brief Notification type
        using Notification = Notif;
    public:
        /// @brief Send notification
        /// @note Thread-safe
        void sendNotification(Notification && notif)
        {
            std::lock_guard<std::mutex> guard(m_mutex);
            m_incomingNotifs.push_back(std::move(notif));
        }
        /// @brief Send notification
        /// @note Thread-safe
        void sendNotification(const Notification& notif)
        {
            std::lock_guard<std::mutex> guard(m_mutex);
            m_incomingNotifs.push_back(notif);
        }
        /// @brief Process notifications
        void processNotifications()
        {
            // Pull notifications
            {
                std::lock_guard<std::mutex> guard(m_mutex);
                debug_assert(m_processingNotifs.empty());
                std::swap(m_incomingNotifs, m_processingNotifs);
            }

            // Process them
            for (Notification& notif : m_processingNotifs)
            {
                boost::apply_visitor(*this, notif);
            }
            m_processingNotifs.clear();
        }
        /// @brief Notification processor
        template <class SpecifiedNotification>
        void operator()(SpecifiedNotification& notif)
        {
            static_cast<MyType&>(*this).processNotification(notif);
        }
    private:
        mutable std::mutex m_mutex; ///< Mutex to guard thread interactions
        vector<Notification> m_incomingNotifs; ///< Pending notifications
        vector<Notification> m_processingNotifs; ///< Processing notifications
    };

    /// @}
}