#include "pch.h"
#include "ls/core/StackAllocator.h"

using namespace ls;
StackAllocator::StackAllocator(const uint bucketSize, const uint alignment)
    : impl(bucketSize, ResizeFactor, BucketDesc(alignment))
    , m_alignment(alignment)
{
}
StackAllocator::~StackAllocator()
{
}
Blob StackAllocator::allocate(const uint size)
{
    void* data = impl.allocate(size, m_alignment).ptr;
    debug_assert(reinterpret_cast<uint>(data) % m_alignment == 0);
    return Blob(size, data);
}
void StackAllocator::release()
{
    impl.discard();
}
void StackAllocator::purge()
{
    impl.purge();
}
