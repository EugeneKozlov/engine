#include "pch.h"
#include "ls/core/Worker.h"

using namespace ls;
Worker::Worker(const uint numThreads)
    : worker(service)
{
    auto runner = [this]()
    {
        service.run();
    };
    for (uint threadIndex = 0; threadIndex < numThreads; ++threadIndex)
    {
        pool.emplace_back(runner);
    }
}
Worker::~Worker()
{
    service.stop();
    for (thread& one : pool)
        one.join();
}
void Worker::wait()
{
    unique_lock<mutex> lock(counterMutex);
    if (counter > 0)
        condition.wait(lock);
}
