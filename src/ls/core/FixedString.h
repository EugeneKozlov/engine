/// @file ls/core/FixedString.h
/// @brief Fixed String
#pragma once

#include "ls/common.h"

namespace ls
{
    /// @addtogroup LsCore
    /// @{

    /// @brief Fixed String
    template <uint N>
    class FixedString : public array<ubyte, N>
    {
    public:
        /// @brief Invalid
        FixedString()
        {
            this->fill('\0');
        }
        /// @brief Assign string
        void operator = (const string& str)
        {
            this->fill('\0');
            m_size = min<uint>(N - 1, str.size());
            memcpy(this->data(), str.data(), m_size);
        }
        /// @brief Get const string
        const char* text() const
        {
            return reinterpret_cast<const char*>(this->data());
        }
        /// @brief Get const string
        string str() const
        {
            return text();
        }
        /// @brief Get length
        uint length() const
        {
            return m_size;
        }
        /// @brief Cast to base for encoding/decoding
        array<ubyte, N>& cast()
        {
            return *this;
        }
        /// @brief Cast to base for encoding/decoding
        const array<ubyte, N>& cast() const
        {
            return *this;
        }
    private:
        uint m_size = 0;
    };
    /// @}
}
