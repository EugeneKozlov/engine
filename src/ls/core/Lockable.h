/// @file ls/core/Lockable.h
/// @brief Locker and lockable interfaces
#pragma once

#include "ls/common.h"

namespace ls
{
    /// @addtogroup LsCore
    /// @{

    /// @brief Lockable class
    class Lockable
        : Noncopyable
    {
    public:
        /// @brief Ctor
        Lockable() noexcept
            : m_lockCounter(0)
        {
        }
        /// @brief Lock this
        void lock() const noexcept
        {
            ++m_lockCounter;
        }
        /// @brief Unlock this
        void unlock() const noexcept
        {
            debug_assert(m_lockCounter != 0);
            --m_lockCounter;
        }
        /// @brief Test if locked
        bool isLocked() const noexcept
        {
            return m_lockCounter != 0;
        }
    private:
        Lockable(const Lockable&) = delete;
        Lockable& operator =(const Lockable&) = delete;
    private:
        /// @brief Counter
        mutable atomic<uint> m_lockCounter;
    };
    /// @brief Locker class.
    /// @brief Locks @b Lockable during construction, unlock during destruction
    template <class Lockable>
    class Locker
    {
    public:
        /// @brief Lock
        Locker(const Lockable& lockable)
            : m_lockable(lockable)
        {
            m_lockable.lock();
        }
        /// @brief Unlock
        ~Locker()
        {
            m_lockable.unlock();
        }
    private:
        Locker(const Locker&) = delete;
        Locker& operator =(const Locker&) = delete;
    private:
        /// @brief Locked lockable
        const Lockable& m_lockable;
    };
    /// @}
}
