/// @file ls/core/INIReader.h
/// @brief INI config Reader
/// https://github.com/benhoyt/inih

#pragma once
#include "ls/common.h"

namespace ls
{
    /// @addtogroup LsCore
    /// @{

    /// @brief INI config Section
    struct INISection : hamap<string, string>
    {
        /// @brief Compute list of keys
        void computeKeysList()
        {
            for (const pair<const string, string>& item : *this)
            {
                m_keys.emplace(item.first);
            }
        }
        /// @brief Get value from section
        template <class Type>
        Type get(const string& key, const Type& def = Type()) const
        {
            m_keys.erase(key);
            const string* value = findOrDefault(*this, key);
            return value ? fromString<Type>(*value) : def;
        }
        /// @brief Load value from section to variable
        template <class Type>
        void load(const string& key, Type& value) const
        {
            value = get(key, value);
        }
        /// @brief Has unused variables
        /// @note return value depends on 'get' calls
        bool hasUnused() const
        {
            return !m_keys.empty();
        }
        /// @brief Get first unused variable
        const string& firstUnused() const
        {
            debug_assert(!m_keys.empty());
            return *m_keys.begin();
        }
    private:
        mutable set<string> m_keys;
    };
    /// @brief INI Config data
    using INIConfig = hamap<string, INISection>;
    /// @brief INI config Reader
    class INIReader
    {
    public:
        /// @brief Construct INIReader and parse given text. See ini.h for more info
        /// @brief about the parsing.
        INIReader();
        /// @brief Parse given text
        void parse(const string& text);
        /// @brief Return the result of ini_parse(): 0 on success, line number of
        /// @brief first error on parse error, or -1 on file open error.
        int parseError();
        /// @brief Drop section
        void dropSection(const string& name);

        /// @brief Has specified section?
        bool hasSection(const string& name) const;
        /// @brief Get section, fail if not exist
        const INISection& section(const string& name) const;
        /// @brief Get all sections
        const INIConfig& sections() const;
        /// @brief Get a string value from INI file, returning default_value if not found.
        string get(const string& section, const string& name,
                   const string& default_value) const;
        /// @brief Get-and-remove a string value from INI file, returning default_value if not found.
        string pull(const string& section, const string& name,
                    const string& default_value);
    private:
        int m_error = 0;
        INIConfig m_sections;
        string m_lastSection;
        string m_lastVariable;

        static int sectionHandler(void* user, const char* section, const char* parent);
        static int variableHandler(void* user, const char* name, const char* value);
    };
    /// @}
}
