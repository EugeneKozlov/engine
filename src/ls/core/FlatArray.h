/// @file ls/core/FlatArray.h
/// @brief 2D array class template
#pragma once

#include "ls/common.h"

namespace ls
{
    /// @addtogroup LsCore
    /// @{

    /// @brief Flat Array Wrapper
    template <class T>
    class FlatArrayWrapper
    {
    public:
        /// @brief Element type
        using Element = T;
        /// @brief Element iterator
        using Iterator = Element*;
        /// @brief Element constant iterator
        using ConstIterator = const Element*;
    public:
        /// @brief Empty
        FlatArrayWrapper() = default;
        /// @brief Copy
        FlatArrayWrapper(const FlatArrayWrapper& another)
            : m_width(another.m_width)
            , m_height(another.m_height)
            , m_data(another.m_data)
        {
        }
        /// @brief Store
        FlatArrayWrapper(uint width, uint height, Element* data)
            : m_width(width)
            , m_height(height)
            , m_data(data)
        {
            debug_assert(data);
        }
        /// @brief Dtor
        ~FlatArrayWrapper()
        {
        }

        /// @brief Get element by @a index
        Element& operator [](const int2& index)
        {
            debug_assert(inside(index), index);

            return m_data[index.x + index.y * m_width];
        }
        /// @brief Get element by @a index
        const Element& operator [](const int2& index) const
        {
            debug_assert(inside(index), index);

            return m_data[index.x + index.y * m_width];
        }
        /// @brief Get element by @a x and @a y
        Element& operator ()(uint x, uint y)
        {
            debug_assert(inside(x, y), x, ", ", y);

            return m_data[x + y * m_width];
        }
        /// @brief Get element by @a x and @a y
        const Element& operator ()(uint x, uint y) const
        {
            debug_assert(inside(x, y), x, ", ", y);

            return m_data[x + y * m_width];
        }
        /// @brief Get begin iterator
        Iterator begin()
        {
            return m_data;
        }
        /// @brief Get begin const iterator
        ConstIterator begin() const
        {
            return m_data;
        }
        /// @brief Get end iterator
        Iterator end()
        {
            return m_data + length();
        }
        /// @brief Get end const iterator
        ConstIterator end() const
        {
            return m_data + length();
        }

        /// @brief Clear
        void clear()
        {
            m_data = nullptr;
            m_width = 0;
            m_height = 0;
        }
        /// @brief Fill by @a value
        void fill(const Element& value)
        {
            std::fill(begin(), end(), value);
        }
        /// @brief Resize to @a width * @a height and rewrite by @a data
        void reset(uint width, uint height, Element* data)
        {
            debug_assert(data);

            m_width = width;
            m_height = height;
            m_data = data;
        }

        /// @brief Process each element in area from @a begin (inclusive) to @a end (exclusive)
        /// @brief by specified @a processor
        template <class Function>
        void process(int2 begin, int2 end, Function processor)
        {
            begin = max(begin, int2(0));
            end = min(end, size());
            int2 elemIndex;
            for (elemIndex.y = begin.y; elemIndex.y < end.y; ++elemIndex.y)
                for (elemIndex.x = begin.x; elemIndex.x < end.x; ++elemIndex.x)
                {
                    processor((*this)[elemIndex], elemIndex);
                }
        }
        /// @brief Process each element by specified @a processor
        template <class Function>
        void process(Function processor)
        {
            process(int2(0), size(), processor);
        }

        /// @brief Is empty?
        bool isEmpty() const
        {
            return !m_data;
        }
        /// @brief Get data
        Element* data()
        {
            return m_data;
        }
        /// @brief Get const data
        const Element* data() const
        {
            return m_data;
        }
        /// @brief Get data starting from @a begin index
        Element* get(const int2& begin = int2(0))
        {
            return !m_data ? nullptr : &*(m_data + begin.x + begin.y * m_width);
        }
        /// @brief Get constant data starting from @a begin index
        const Element* get(const int2& begin = int2(0)) const
        {
            return !m_data ? nullptr : &*(m_data + begin.x + begin.y * m_width);
        }
        /// @brief Get full array length (in elements)
        uint length() const noexcept
        {
            return m_width * m_height;
        }
        /// @brief Get full array size (in bytes)
        int2 size() const noexcept
        {
            return int2((sint) m_width, (sint) m_height);
        }
        /// @brief Get array stride (in bytes)
        uint stride() const noexcept
        {
            return sizeof(Element);
        }
        /// @brief Get array width
        uint width() const noexcept
        {
            return m_width;
        }
        /// @brief Get array height
        uint height() const noexcept
        {
            return m_height;
        }
        /// @brief Get array row pitch (in bytes)
        uint pitch() const noexcept
        {
            return width() * stride();
        }
        /// @brief Test if index @a x and @a y is inside
        bool inside(uint x, uint y) const noexcept
        {
            return x < m_width && y < m_height;
        }
        /// @brief Test if @a index is inside
        bool inside(const int2& index) const noexcept
        {
            return inside(static_cast<uint>(index.x), static_cast<uint>(index.y));
        }
    protected:
        /// @brief Width
        uint m_width = 0;
        /// @brief Height
        uint m_height = 0;
        /// @brief Data array
        Element* m_data = nullptr;
    };
    /// @brief Flat Array
    template <class T, template<class> class Allocator = StdAllocator>
    class FlatArray
    {
    public:
        /// @brief Element type
        using Element = T;
        /// @brief Element iterator
        using Iterator = typename vector<Element>::iterator;
        /// @brief Element constant iterator
        using ConstIterator = typename vector<Element>::const_iterator;
    public:
        /// @brief Degenerate
        FlatArray() = default;
        /// @brief Copy
        FlatArray(const FlatArray& another)
            : m_width(another.m_width)
            , m_height(another.m_height)
            , m_data(another.m_data)
        {
        }
        /// @brief Move
        FlatArray(FlatArray && another) = default;
        /// @brief New @a width * @a height array
        FlatArray(const uint width, const uint height)
            : m_width(width)
            , m_height(height)
            , m_data(length())
        {
        }
        /// @brief New @a width * @a height array with per-element @a value
        FlatArray(const Element& value, const uint width, const uint height)
            : m_width(width)
            , m_height(height)
            , m_data(length(), value)
        {
        }
        /// @brief Copy existing @a width * @a height array from @a data
        FlatArray(const uint width, const uint height, const Element* data)
            : m_width(width)
            , m_height(height)
            , m_data(width * height)
        {
            debug_assert(data);

            copy(data, data + length(), m_data.begin());
        }
        /// @brief Swap with @a another
        void swap(FlatArray<Element>& another) noexcept
        {
            std::swap(m_width, another.m_width);
            std::swap(m_height, another.m_height);
            std::swap(m_data, another.m_data);
        }
        /// @brief Dtor
        ~FlatArray()
        {
        }

        /// @brief Get element by @a index
        Element& operator [] (const int2& index)
        {
            debug_assert(inside(index), index);

            return m_data[index.x + index.y * m_width];
        }
        /// @brief Get element by @a index
        const Element& operator [] (const int2& index) const
        {
            debug_assert(inside(index), index);

            return m_data[index.x + index.y * m_width];
        }
        /// @brief Get element by @a x and @a y
        Element& operator () (const uint x, const uint y)
        {
            debug_assert(inside(x, y), x, ", ", y);

            return m_data[x + y * m_width];
        }
        /// @brief Get element by @a x and @a y
        const Element& operator () (const uint x, const uint y) const
        {
            debug_assert(inside(x, y), x, ", ", y);

            return m_data[x + y * m_width];
        }
        /// @brief Get begin iterator
        Iterator begin()
        {
            return m_data.begin();
        }
        /// @brief Get begin const iterator
        ConstIterator begin() const
        {
            return m_data.begin();
        }
        /// @brief Get end iterator
        Iterator end()
        {
            return m_data.end();
        }
        /// @brief Get end const iterator
        ConstIterator end() const
        {
            return m_data.end();
        }

        /// @brief Clear
        void clear()
        {
            m_data.clear();
            m_width = 0;
            m_height = 0;
        }
        /// @brief Fill by @a value
        void fill(const Element& value)
        {
            std::fill(m_data.begin(), m_data.end(), value);
        }
        /// @brief Resize to @a width * @a height and rewrite by default
        void reset(const uint width, const uint height)
        {
            m_width = width;
            m_height = height;
            m_data.resize(length());
        }
        /// @brief Resize to @a width * @a height and rewrite by @a data
        void reset(const uint width, const uint height, const Element* data)
        {
            debug_assert(data);

            m_width = width;
            m_height = height;
            m_data.assign(data, data + length());
        }
        /// @brief Resize to @a width * @a height and rewrite by @a value
        void reset(const Element& value, const uint width, const uint height)
        {
            m_width = width;
            m_height = height;
            m_data.resize(length());
            fill(value);
        }
        /// @brief Rewrite by @a source array
        void reset(const FlatArray<Element>& source)
        {
            m_width = source.m_width;
            m_height = source.m_height;
            m_data = source.m_data;
        }
        /// @brief Rewrite by @a source array with specified @a converter
        template <class Array, class Convertor>
        void reset(Array& source, Convertor converter)
        {
            m_width = source.m_width;
            m_height = source.m_height;
            m_data.resize(m_width * m_height);
            std::transform(source.m_data.begin(), source.m_data.end(),
                           m_data.begin(), converter);
        }

        /// @name Rewrite region
        /// @brief Rewrite region of array.
        /// @tparam Array Source data array type
        /// @tparam DataIterator Source data iterator type
        /// @param target Valid target position
        /// @param source Valid source array
        /// @param source,area Valid source area
        /// @param data,stride Source data pointer and data width <b>(in elements, not bytes!)</b>
        /// @param convertor Convertor functor
        /// @{

        /// @brief Copy elements to array
        template <class DataIterator>
        void copy(const int2& target, const int2& source, const int2& area,
                  DataIterator data, const uint stride)
        {
            copy2d(m_data.begin(), target, width(),
                   data, source, area, stride);
        }
        /// @brief Transform and copy elements to array
        template <class DataIterator, class Convertor>
        void copy(const int2& target, const int2& source, const int2& area,
                  DataIterator data, const uint stride, Convertor converter)
        {
            transform2d(m_data.begin(), target, width(),
                        data, source, area, stride,
                        converter);
        }
        /// @brief Copy elements to array
        template <class Array>
        void copy(const int2& target, Array& source)
        {
            copy(target,
                 int2(0), source.size(),
                 source.get(), source.width());
        }
        /// @brief Transform and copy elements to array
        template <class Array, class Convertor>
        void copy(const int2& target, Array& source, Convertor converter)
        {
            copy(target,
                 int2(0), source.size(),
                 source.get(), source.width(), converter);
        }
        /// @}

        /// @brief Rewrite by shifted elements (sizes must be equal)
        /// @param source Source array
        /// @param offset Shift
        /// @param leaveCallback Leave callback.
        ///   Called when element in old array has no place in new array.
        ///   @code void leave_callback(T& elem, int2 index_in_old_array) @endcode
        /// @param enterCallback Enter callback.
        ///   Called when element in new array has no prototype in old array.
        ///   @code void leave_callback(T& elem, int2 index_in_new_array) @endcode
        /// @{

        /// @brief Rewrite with callback (sizes must be equal)
        template <class Array, class LeaveHandler, class EnterHandler>
        void rewrite(Array& source, const int2& offset,
                     LeaveHandler leaveCallback, EnterHandler enterCallback)
        {
            debug_assert(source.m_width == m_width);
            debug_assert(source.m_height == m_height);
            int2 elemIndex;
            for (elemIndex.x = 0; elemIndex.x < m_width; ++elemIndex.x)
                for (elemIndex.y = 0; elemIndex.y < m_height; ++elemIndex.y)
                {
                    int2 newPos = elemIndex + offset;
                    if (inside(newPos))
                        (*this)[newPos] = source[elemIndex];
                    else
                    {
                        leaveCallback(source[elemIndex], elemIndex);
                        int2 newIdx;
                        upcast(newPos, size(), &newIdx);
                        enterCallback((*this)[newIdx], newIdx);
                    }
                }
        }
        /// @brief Rewrite other array without callback (sizes must be equal)
        template<class Array>
        void rewrite(Array& source, const int2& offset)
        {
            rewrite(source, offset, nop(), nop());
        }
        /// @}

        /// @brief Process each in area from @a begin (inclusive) to @a end (exclusive)
        /// @brief by specified @a processor
        template <class Function>
        void process(int2 begin, int2 end, Function processor)
        {
            begin = max(begin, int2(0));
            end = min(end, size());
            int2 elemIndex;
            for (elemIndex.y = begin.y; elemIndex.y < end.y; ++elemIndex.y)
                for (elemIndex.x = begin.x; elemIndex.x < end.x; ++elemIndex.x)
                {
                    processor((*this)[elemIndex], elemIndex);
                }
        }
        /// @brief Process each element by specified @a processor
        template <class Function>
        void process(Function processor)
        {
            process(int2(0), size(), processor);
        }

        /// @brief Is empty?
        bool isEmpty() const
        {
            return m_data.empty();
        }
        /// @brief Get data
        Element* data()
        {
            return m_data.empty() ? nullptr : m_data.data();
        }
        /// @brief Get const data
        const Element* data() const
        {
            return m_data.empty() ? nullptr : m_data.data();
        }
        /// @brief Get data starting from @a begin index
        Element* get(const int2& begin = int2(0))
        {
            return m_data.empty() ? nullptr : &*(m_data.begin() + begin.x + begin.y * m_width);
        }
        /// @brief Get constant data starting from @a begin index
        const Element* get(const int2& begin = int2(0)) const
        {
            return m_data.empty() ? nullptr : &*(m_data.begin() + begin.x + begin.y * m_width);
        }
        /// @brief Copy region from @a begin (inclusive) to @a end (exclusive) to @a dest iterator
        template <class DestIterator>
        void extract(const int2& begin, const int2& end, DestIterator dest)
        {
            debug_assert(begin.x <= end.x && begin.y <= end.y, begin);
            debug_assert(end.x <= m_width && end.y <= m_height, end);
            const int2 size = end - begin;
            copy2d(dest, int2(0), size.x, m_data.begin(), begin, size, width());
        }
        /// @brief Get full array length (in elements)
        uint length() const noexcept
        {
            return m_width * m_height;
        }
        /// @brief Get full array size (in bytes)
        int2 size() const noexcept
        {
            return int2((sint) m_width, (sint) m_height);
        }
        /// @brief Get array stride (in bytes)
        uint stride() const noexcept
        {
            return sizeof(Element);
        }
        /// @brief Get array width
        uint width() const noexcept
        {
            return m_width;
        }
        /// @brief Get array height
        uint height() const noexcept
        {
            return m_height;
        }
        /// @brief Get array row pitch (in bytes)
        uint pitch() const noexcept
        {
            return width() * stride();
        }
        /// @brief Test if index @a x and @a y is inside
        bool inside(const uint x, const uint y) const noexcept
        {
            return x < m_width && y < m_height;
        }
        /// @brief Test if @a index is inside
        bool inside(const int2& index) const noexcept
        {
            return inside(static_cast<uint>(index.x), static_cast<uint>(index.y));
        }
    public:
        /// @name Copy/transform elements from 2d array to 2d array.
        /// @tparam DestIterator,SourceIterator Input and output iterator types
        /// @tparam Copier Copy functor
        /// @param dest,destStride
        ///   Destination array info
        /// @param destPos
        ///   Destination position
        /// @param source,sourceStride
        ///   Source area info
        /// @param sourcePos,sourceArea
        ///   Source area region
        /// @param copier,convertor
        ///   Copy functors
        /// @{

        /// @brief Move array region to array region
        template <class DestIterator, class SourceIterator, class Copier>
        static void move2d(DestIterator dest,
                           const int2& destPos, uint destStride,
                           SourceIterator source,
                           const int2& sourcePos, const int2& sourceArea,
                           uint sourceStride, Copier copier)
        {
            SourceIterator s = source + sourcePos.x + sourcePos.y * sourceStride;
            DestIterator d = dest + destPos.x + destPos.y * destStride;
            for (sint y = 0; y < sourceArea.y; /*++y*/)
            {
                copier(s, s + sourceArea.x, d);
                if (++y != sourceArea.y)
                {
                    s += sourceStride;
                    d += destStride;
                }
            }
        }
        /// @brief Copy elements
        template<class DestIterator, class SourceIterator>
        static void copy2d(DestIterator dest,
                           const int2& destPos, uint destStride,
                           SourceIterator source,
                           const int2& sourcePos, const int2& sourceArea,
                           uint sourceStride)
        {
            move2d(dest, destPos, destStride,
                   source, sourcePos, sourceArea, sourceStride,
                   bind(&std::copy<SourceIterator, DestIterator>,
                        _1, _2, _3));
        }
        /// @brief Transform and copy elements to array.
        template<class DestIterator, class SourceIterator, class Copier>
        static void transform2d(DestIterator dest,
                                const int2& destPos, uint destStride,
                                SourceIterator source,
                                const int2& sourcePos, const int2& sourceArea,
                                uint sourceStride, Copier converter)
        {
            move2d(dest, destPos, destStride,
                   source, sourcePos, sourceArea, sourceStride,
                   bind(&std::transform<SourceIterator, DestIterator, Copier>,
                        _1, _2, _3, std::ref(converter)));
        }
        /// @}
    protected:
        /// @brief Width
        uint m_width = 0;
        /// @brief Height
        uint m_height = 0;
        /// @brief Data array
        vector<Element, Allocator<Element>> m_data;
    };
    /// @}
}

namespace std
{
    /// @brief Swap arrays
    template <class T>
    inline void swap(ls::FlatArray<T>& lhs, ls::FlatArray<T>& rhs)
    {
        lhs.swap(rhs);
    }
}
