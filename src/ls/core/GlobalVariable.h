/// @file ls/core/GlobalVariable.h
/// @brief Global Variable common interface
#pragma once

#include "ls/common.h"

namespace ls
{
    /// @addtogroup LsCore
    /// @{

    /// Global Variable Type
    enum class GlobalVariableFlag
    {
        /// Can be initialized only once when engine loads config
        ReadOnly = 0,
        /// Can be changed after initialization
        Modifiable = 1 << 0,
        /// Visible in console
        Console = 1 << 1
    };
    /// Global Variable common interface
    class GlobalVariable
        : Noncopyable
    {
    public:
        /// @brief Dtor
        virtual ~GlobalVariable()
        {
        }
        /// @brief Load from string
        virtual void load(const string& value) = 0;
        /// @brief Store to string
        virtual string store() const = 0;

        /// @brief Is initialized?
        virtual bool isInitialized() const = 0;
        /// @brief Get full name
        virtual const string& fullName() const = 0;
        /// @brief Get short name
        virtual const string& shortName() const = 0;
        /// @brief Test flag
        virtual bool is(const GlobalVariableFlag flag) const = 0;
        /// @brief Get full description
        virtual const string& description() const = 0;
    };

    /// @}
}