/// @file ls/core/utf.h
/// @brief UTF strong support
#pragma once

#include "ls/common.h"

namespace ls
{
    /// @addtogroup LsCore
    /// @{

    /// @brief Wide string type
    using WideString = std::basic_string<uint>;
    /// @brief Convert UTF32 to UTF8
    /// @param symbol
    ///   UTF32 symbol
    /// @return Pair of UTF8 symbol and its size
    pair<array<char, 4>, ubyte> UTF32toUTF8(const uint symbol);
    /// @brief Convert UTF8 to UTF32
    /// @param symbol
    ///   UTF8 symbol
    /// @return Pair of UTF32 symbol and UTF8 symbol size
    pair<uint, ubyte> UTF8toUTF32(const char* symbol);
    /// @brief Convert UTF32 string to UTF8
    string wideToString(const WideString& text);
    /// @brief Convert UTF8 string to UTF32
    WideString stringToWide(const string& text);
    /// @}
}