/// @file ls/core/EnumTrait
/// @brief Enum Traits
#pragma once

#include "ls/common.h"

namespace ls
{
    /// @addtogroup LsCore
    /// @{

    /// @brief Begin enum traits list
#define LS_BEGIN_ENUM_TRAITS(NUM, NAME)     \
    static const Trait& NAME(uint idx)      \
    {                                       \
        static const uint Num = (uint) NUM; \
        static const Trait list[] =         \

    /// @brief Add shader trait (preserve enum order!)
#define LS_ADD_ENUM_TRAIT(SHADER, ...)          \
            { SHADER, __VA_ARGS__ }             \

    /// @brief End shader traits list
#define LS_END_ENUM_TRAITS(NUM); \
        static_assert((uint) NUM == Num, "count mismatch");             \
        static_assert(sizeof(list) / sizeof(Trait) == Num,              \
                      "count mismatch");                                \
        debug_assert(idx < Num && idx == (uint) list[idx].thisType);    \
        return list[idx];                                               \
    }                                                                   \

    /// @}
}