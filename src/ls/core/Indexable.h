/// @file ls/core/Indexable.h
/// @brief Template of class with indexed instances
#pragma once

#include "ls/common.h"

namespace ls
{
    /// @addtogroup LsCore
    /// @{

	/// @brief Indexable class template.
	/// @brief Each class instance will have unique index.
	/// @tparam Derived Derived class
    template<class Derived>
    class Indexable
    {
    public:
        /// @brief Ctor
        Indexable() noexcept
			: m_debugIndex(m_counter)
        {
            ++m_counter;
        }
        /// @brief Ctor with specified @a index
        Indexable(const uint index) noexcept
			: m_debugIndex(index)
        {
        }
        /// @brief Get index.
        uint debugIndex() const noexcept
        {
            return m_debugIndex;
        }
    private:
        /// @brief Counter
        static uint m_counter;
    private:
        /// @brief Index
        uint m_debugIndex;
    };
    template<class Derived>
    uint Indexable<Derived>::m_counter = 0;
    /// @}
}
