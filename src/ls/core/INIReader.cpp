#include "pch.h"
#include "ls/core/INIReader.h"
#include <ini/ini.h>

#include <cctype>
#include <cstdlib>

using namespace ls;
INIReader::INIReader()
{
}
void INIReader::parse(const string& text)
{
    // Parse
    int err = ini_parse_file(text.c_str(), variableHandler, sectionHandler, this);
    if (!m_error)
        m_error = err;

    // Finalize sections
    for (pair<const string, INISection>& section : m_sections)
    {
        section.second.computeKeysList();
    }
}
int INIReader::parseError()
{
    return m_error;
}
void INIReader::dropSection(const string& name)
{
    m_sections.erase(name);
}


// Gets
bool INIReader::hasSection(const string& name) const
{
    return m_sections.count(name) > 0;
}
INISection const& INIReader::section(const string& name) const
{
    return getOrFail(m_sections, name);
}
INIConfig const& INIReader::sections() const
{
    return m_sections;
}
string INIReader::get(const string& section, const string& name, const string& default_value) const
{
    auto iterSection = m_sections.find(section);
    if (iterSection == m_sections.cend())
        return default_value;

    return getOrDefault(iterSection->second, name, default_value);
}
string INIReader::pull(const string& section, const string& name, const string& default_value)
{
    auto iterSection = m_sections.find(section);
    if (iterSection == m_sections.cend())
        return default_value;

    const string value = getOrDefault(iterSection->second, name, default_value);
    iterSection->second.erase(name);
    return value;
}
int INIReader::sectionHandler(void* user, const char* section, const char* parent)
{
    INIReader* reader = static_cast<INIReader*>(user);
    INISection& dest = reader->m_sections[section];
    if (dest.size() > 0)
        return 0;

    reader->m_lastSection = section;
    if (parent)
        dest = reader->m_sections[parent];
    return 1;
}
int INIReader::variableHandler(void* user, const char* name, const char* value)
{
    INIReader* reader = static_cast<INIReader*>(user);
    if (name)
        reader->m_lastVariable = name;

    string& dest = reader->m_sections[reader->m_lastSection][reader->m_lastVariable];
    if (!name)
        dest += value;
    else
        dest = value;

    return 1;
}
