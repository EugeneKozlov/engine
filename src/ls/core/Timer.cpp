#include "pch.h"
#include "ls/core/Timer.h"

using namespace ls;
// Simple timer
Timer::Timer(const DeltaTime interval,
             const bool startAlarm,
             const bool isEnabled,
             const bool isAdditive) noexcept
    : m_interval(interval)
    , m_additiveFlag(isAdditive)
    , m_enabledFlag(isEnabled)
    , m_time(0)
    , m_numEvents(startAlarm)
{
    debug_assert(interval > 0);
}
void Timer::reset() noexcept
{
    m_time = 0;
}
void Timer::advance(const DeltaTime timeDelta) noexcept
{
    if (!m_enabledFlag)
        return;
    m_time += timeDelta;
    while (m_time >= m_interval)
    {
        ++m_numEvents;
        m_time -= m_interval;
    }
}
bool Timer::alarm() noexcept
{
    if (!m_numEvents)
        return false;
    if (m_additiveFlag)
        m_numEvents--;
    else
        m_numEvents = 0;
    return true;
}
void Timer::enable() noexcept
{
    m_enabledFlag = true;
}
void Timer::disable() noexcept
{
    m_enabledFlag = false;
}


// Incremental timer
IncrementalTimer::IncrementalTimer(const DeltaTime interval,
                                   const DiscreteTime startValue,
                                   const bool isEnabled) noexcept
    : Timer(interval, false, isEnabled, true)
    , m_tick(floor(startValue))
{
    m_time = startValue;
}
void IncrementalTimer::set(const DiscreteTime time) noexcept
{
    if (time >= m_time)
    {
        m_time = time;
    }
    else
    {
        m_sleepTime = m_time - time;
    }
}
void IncrementalTimer::advance(DeltaTime timeDelta) noexcept
{
    auto amortTime = min(timeDelta, m_sleepTime);
    timeDelta -= amortTime;
    m_sleepTime -= amortTime;

    m_time += timeDelta;
    auto newTick = floor(m_time);
    m_numEvents += (newTick - m_tick) / m_interval;
}
bool IncrementalTimer::alarm() noexcept
{
    bool hasNewEvent = Timer::alarm();
    if (hasNewEvent)
        m_tick += m_interval;
    return hasNewEvent;
}
bool IncrementalTimer::alarmAll() noexcept
{
    if (!alarm())
        return false;
    while (alarm());
    return true;
}
