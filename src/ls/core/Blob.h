/// @file ls/core/Blob.h
/// @brief Data Blob
#pragma once

#include "ls/common.h"

namespace ls
{
	/// @addtogroup LsCore
	/// @{

	/// @brief Fixed data blob.
	///
	/// Casting:
	/// | FixedBlob        | Object           |
	/// |------------------|------------------|
	/// | Size (4b uint)   | @b Must be same  |
	/// | Data (Nb array)  | Custom data      |
    template <uint S>
    class FixedBlob
		: Noncopyable
    {
    public:
        /// @brief Size
        static const uint Size = S;
        /// @brief Ctor
        FixedBlob() = default;
        /// @brief Copy
        FixedBlob(const FixedBlob<S>& another)
            : m_buffer(another.m_buffer)
        {
        }
        /// @brief Cast to object
        template <class Object>
        Object& cast() noexcept
        {
            static_assert(sizeof (Object) <= Size, "too large object");
            debug_assert(sizeof (Object) <= m_size, "too large object");
            return *reinterpret_cast<Object*>(data() - sizeof(uint));
        }
        /// @brief Const cast to object
        template <class Object>
        const Object& cast() const noexcept
        {
            static_assert(sizeof (Object) <= Size, "too large object");
            debug_assert(sizeof (Object) <= m_size, "too large object");
            return *reinterpret_cast<const Object*>(data() - sizeof(uint));
        }
        /// @brief Data
        ubyte* data() noexcept
        {
            return m_buffer.data();
        }
        /// @brief Const data
        const ubyte* data() const noexcept
        {
            return m_buffer.data();
        }
        /// @brief Size
        uint size() const noexcept
        {
            return m_size;
        }
    protected:
        /// @brief Size
        uint m_size = Size;
        /// @brief Data buffer
        array<ubyte, Size> m_buffer;
    };
	/// @brief Data Blob
    class Blob
    {
    public:
        /// @brief Ctor
        Blob() = default;
        /// @brief Ctor
        Blob(nullptr_t)
        {
        }
        /// @brief Ctor
        Blob(const uint size, void* data) noexcept
			: m_size(size)
			, m_data(static_cast<ubyte*>(data))
        {
        }
        /// @brief Dtor
        ~Blob() = default;
        /// @brief Cast to object
        template <class Object>
        Object& cast() noexcept
        {
            debug_assert(m_data && sizeof(Object) <= m_size, "too large object");
            return *reinterpret_cast<Object*>(m_data);
        }
        /// @brief Const cast to object
        template <class Object>
        const Object& cast() const noexcept
        {
            debug_assert(m_data && sizeof(Object) <= m_size, "too large object");
            return *reinterpret_cast<const Object*>(m_data);
        }
        /// @brief Wrap
        template <class Object>
        ArrayWrapper<Object> wrap()
        {
            return ArrayWrapper<Object>(reinterpret_cast<Object*>(m_data),
                                        m_size / sizeof(Object));
        }
        /// @brief Data
        ubyte* data() noexcept
        {
            return m_data;
        }
        /// @brief Const data
        const ubyte* data() const noexcept
        {
            return m_data;
        }
        /// @brief Size
        uint size() const noexcept
        {
            return m_size;
        }
        /// @brief Cast to bool
        operator bool() const noexcept
        {
            return !!m_data;
        }
    protected:
        /// @brief Size
        uint m_size = 0;
        /// @brief Data
        ubyte* m_data = nullptr;
    };
	/// @brief Static size Data Blob 
    template <uint S>
    class StaticBlob
		: public Blob
    {
    public:
        /// @brief Size
        static const uint Size = S;
        /// @brief Ctor
        StaticBlob() noexcept
			: Blob(Size, m_buffer.data())
        {
        }
        /// @brief Copy
        StaticBlob(const StaticBlob<Size>& another) noexcept
			: Blob(Size, m_buffer.data())
        {
            copy_n(another.m_buffer.data(), Size, m_buffer.data());
        }
        /// @brief Assign
        void operator =(const StaticBlob<Size>& another) noexcept
        {
            copy_n(another.m_buffer, Size, m_buffer);
        }
    private:
        array<ubyte, Size> m_buffer;
    };
    /// @brief Dynamic size Data Blob
    class DynamicBlob
        : public Blob
    {
    public:
        /// @brief Ctor
        DynamicBlob() noexcept
            : Blob(0, nullptr)
        {
        }
        /// @brief Ctor
        DynamicBlob(const uint size) noexcept
            : Blob(size, nullptr)
            , m_buffer(size, 0)
        {
            m_data = m_buffer.data();
        }
        /// @brief Ctor
        DynamicBlob(vector<ubyte>&& data) noexcept
            : Blob(data.size(), nullptr)
            , m_buffer(move(data))
        {
            m_data = m_buffer.empty() ? nullptr : m_buffer.data();
        }
        /// @brief Copy
        DynamicBlob(const DynamicBlob& another) noexcept
            : DynamicBlob(another.m_buffer.size())
        {
            copy_n(another.m_buffer.data(), m_size, m_data);
        }
        /// @brief Move
        DynamicBlob(DynamicBlob&& another) noexcept
            : Blob(another.m_buffer.size(), nullptr)
            , m_buffer(move(another.m_buffer))
        {
            m_data = m_buffer.data();

            another.m_data = nullptr;
            another.m_size = 0;
        }

        /// @brief Assign
        void operator = (const DynamicBlob& another) noexcept
        {
            m_buffer = another.m_buffer;
            m_size = another.m_size;
            m_data = m_buffer.data();
        }
    private:
        vector<ubyte> m_buffer;
    };
    /// @}
}
