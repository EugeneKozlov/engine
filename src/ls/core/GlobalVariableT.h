/// @file ls/core/GlobalVariableT.h
/// @brief Global Variable specialization
#pragma once

#include "ls/common.h"
#include "ls/core/GlobalVariable.h"
#include "ls/core/GlobalVariableManager.h"

namespace ls
{
    /// @addtogroup LsCore
    /// @{

    /// Global Variable specialization
    template <class T>
    class GlobalVariableT
        : public GlobalVariable
    {
    public:
        /// @brief Ctor
        /// @param fullName
        ///   Full variable name used to load it from config
        /// @param shortName
        ///   Variable can be found using its short name
        /// @param flags
        ///   Variable flags
        /// @param description
        ///   Full variable description
        /// @param initialValue
        ///   Initial value
        GlobalVariableT(const string& fullName, const string& shortName,
                        const FlagSet<GlobalVariableFlag> flags = GlobalVariableFlag::ReadOnly,
                        const string& description = "",
                        const T& initialValue = T(),
                        function<bool(const T&)> condition = [](const T& value) { return true; })
            : m_fullName(fullName)
            , m_shortName(shortName)
            , m_flags(flags)
            , m_description(description)
            , m_initialValue(initialValue)
            , m_condition(condition)
            , m_value(m_initialValue)
        {
            GlobalVariableManager::instance().registerVariable(*this);
        }
        /// @brief Get reference to value (stable is guaranteed)
        const T& value() const 
        {
            return m_value;
        }
    public:
        /// @name Implement GlobalVariable
        /// @{
        virtual void load(const string& value) override
        {
            T newValue = fromString<T>(value);
            LS_THROW(m_condition(newValue), IoException,
                     "New value [", value, "] for variable [", fullName(), "] does not meet the condition");
            m_value = newValue;
        }
        virtual string store() const override
        {
            return toString(m_value);
        }
        virtual bool isInitialized() const override
        {
            return m_initialized;
        }
        virtual const string& fullName() const override
        {
            return m_fullName;
        }
        virtual const string& shortName() const override
        {
            return m_shortName;
        }
        virtual bool is(const GlobalVariableFlag flag) const override
        {
            return m_flags.is(flag);
        }
        virtual const string& description() const override
        {
            return m_description;
        }
        /// @}
    private:
        const string m_fullName;
        const string m_shortName;
        const FlagSet<GlobalVariableFlag> m_flags;
        const string m_description;
        const T m_initialValue;
        function<bool(const T&)> m_condition;

        bool m_initialized = false;
        T m_value;
    };

    /// @}
}