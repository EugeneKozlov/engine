/// @file ls/core/StackAllocatorBase.h
/// @brief Stack Allocator Base
#pragma once

#include "ls/common.h"

namespace ls
{
    /// @addtogroup LsCore
    /// @{
    
    /// @brief Stack Allocator
    ///
    /// @tparam Bucket
    template <class Bucket, class BucketDesc, 
    class = enable_if_t<is_base_of<DisposableConcept, Bucket>::value>>
    class StackAllocatorBase
        : Noncopyable
    {
    public:
        /// @brief Bucket Impl
        struct BucketImpl : Bucket
        {
            /// @brief Ctor
            BucketImpl(BucketDesc& desc, const uint size)
                : Bucket(desc, size)
            {
            }
            /// @brief Bucket size
            uint size;
            /// @brief Bucket offset
            uint offset;
            /// @brief Next bucket
            BucketImpl* next;
            /// @brief Get aligned offset
            uint getBlock(const uint alignment)
            {
                return offset / alignment + !!(offset % alignment);
            }
            /// @brief Get remaining
            uint getRemaining(const uint alignment)
            {
                const uint alignedOffset = getBlock(alignment) * alignment;
                return (alignedOffset >= size) ? 0 : (size - alignedOffset);
            }
        };
        /// @brief Allocation data
        struct Allocation
        {
            /// @brief Pointer
            void* ptr;
            /// @brief Start block
            uint startBlock;
            /// @brief Bucket
            Bucket* bucket;
        };
        /// @brief Ctor
        /// @param bucketSize
        ///   Bucket size
        /// @param resizeFactor
        ///   Factor of buffer resizing
        StackAllocatorBase(const uint bucketSize, const uint resizeFactor,
                           const BucketDesc bucketDesc)
            : m_bucketSize(bucketSize)
            , m_resizeFactor(resizeFactor)
            , m_bucketDesc(bucketDesc)
            , m_currentBucket(addBucket(m_bucketSize))
        {
            debug_assert(resizeFactor > 0);
        }
        /// @brief Dtor
        ~StackAllocatorBase()
        {
            purge();
        }
        /// @brief Allocate memory
        /// @param size
        ///   Memory size
        /// @param alignment
        ///   Memory alignment
        Allocation allocate(const uint size, const uint alignment)
        {
            // While too few memory
            while (m_currentBucket->getRemaining(alignment) < size)
            {
                // Get next
                m_currentBucket = m_currentBucket->next;

                // Or allocate new
                if (!m_currentBucket)
                {
                    // Resize buckets
                    if (size * m_resizeFactor >= m_bucketSize)
                    {
                        m_forceResize = true;
                        m_bucketSize = m_bucketSize * m_resizeFactor;

                        // Warn
                        logWarning("Stack Allocator: reallocation is required; "
                                   "optimal bucket size is [", m_bucketSize, "]");
                    }
                    // Allocate bucket
                    m_currentBucket = addBucket(m_bucketSize);
                    break;
                }
            }

            // Allocate
            Allocation info;
            info.startBlock = m_currentBucket->getBlock(alignment);
            info.ptr = static_cast<ubyte*>(m_currentBucket->data()) + info.startBlock * alignment;
            info.bucket = m_currentBucket;

            m_currentBucket->offset = info.startBlock * alignment + size;
            return info;
        }
        /// @brief Commit all written data
        void commit()
        {
            for (BucketImpl& bucket : m_buckets)
                bucket.commit();
        }
        /// @brief Discard all allocated memory
        bool discard()
        {
            if (m_forceResize)
            {
                purge();
                m_forceResize = false;
                m_currentBucket = addBucket(m_bucketSize);
                return true;
            }
            else
            {
                for (BucketImpl& bucket : m_buckets)
                    bucket.offset = 0;
                m_currentBucket = m_buckets.empty() ? nullptr : &m_buckets.front();
                return false;
            }
        }
        /// @brief Purge all allocated memory
        void purge()
        {
            for (BucketImpl& bucket : m_buckets)
                bucket.dispose();
            m_buckets.clear();
            m_currentBucket = nullptr;
        }
    private:
        BucketImpl* addBucket(uint maxSize)
        {
            BucketImpl* prevBucket = m_buckets.empty() ? nullptr : &m_buckets.back();
            m_buckets.emplace_back(m_bucketDesc, maxSize);

            // Init
            BucketImpl& newBucket = m_buckets.back();
            newBucket.size = maxSize;
            newBucket.offset = 0;
            newBucket.next = nullptr;

            // Link
            if (prevBucket)
                prevBucket->next = &newBucket;
            return &newBucket;
        }
    private:
        /// @brief Bucket desc
        BucketDesc m_bucketDesc;
        /// @brief Bucket size
        uint m_bucketSize;
        /// @brief Factor of buffer resizing
        uint m_resizeFactor;
        /// @brief Set to force resize all buckets
        bool m_forceResize = false;

        /// @brief Buckets list
        list<BucketImpl> m_buckets;
        /// @brief Current bucket
        BucketImpl* m_currentBucket = nullptr;
    };
    /// @}
}
