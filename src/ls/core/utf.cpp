#include "pch.h"
#include "ls/core/utf.h"
#include <utf/ConvertUTF.h>

using namespace ls;
pair<array<char, 4>, ubyte> ls::UTF32toUTF8(const uint symbol)
{
    UTF8 buf[4] = {0};
    const UTF32* src = reinterpret_cast<const UTF32*>(&symbol);
    UTF8* dest = buf;
    ConvertUTF32toUTF8(&src, src + 1, &dest, dest + 4,
                       strictConversion);

    pair<array<char, 4>, ubyte> ret;
    fill(ret.first.data(), ret.first.data() + 4, 0);
    copy(buf, dest, ret.first.data());
    ret.second += (ubyte) (dest - buf);
    return ret;
}
pair<uint, ubyte> ls::UTF8toUTF32(const char* symbol)
{
    pair<uint, ubyte> ret;

    const UTF8* src = reinterpret_cast<const UTF8*>(symbol);
    UTF32* dest = &ret.first;
    ConvertUTF8toUTF32(&src, src + 4, &dest, dest + 1,
                       strictConversion);

    ret.second = (ubyte) (src - reinterpret_cast<const UTF8*>(symbol));
    return ret;
}
string ls::wideToString(const WideString& text)
{
    uint len = text.length();
    vector<char> buf(len * 4, 0);

    const UTF32* src = reinterpret_cast<const UTF32*>(text.c_str());
    UTF8* dest = reinterpret_cast<UTF8*>(buf.data());

    ConvertUTF32toUTF8(&src, src + len, &dest, dest + len * 4,
                       strictConversion);
    return string(buf.begin(), buf.end());
}
WideString ls::stringToWide(const string& text)
{
    uint len = text.length();
    vector<uint> buf(len, 0);

    const UTF8* src = reinterpret_cast<const UTF8*>(text.c_str());
    UTF32* dest = reinterpret_cast<UTF32*>(buf.data());

    ConvertUTF8toUTF32(&src, src + len, &dest, dest + len * 4,
                       strictConversion);
    return WideString(buf.begin(), buf.end());
}