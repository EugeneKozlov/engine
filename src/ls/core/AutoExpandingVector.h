/// @file ls/core/AutoExpandingVector.h
/// @brief Auto Expanding Vector
#pragma once

#include "ls/common.h"

namespace ls
{
    /// @addtogroup LsCore
    /// @{

    /// @brief Auto-expanding vector
    template <class T>
    class AutoExpandingVector : public vector<T>
    {
    public:
        using vector<T>::vector;
        /// @brief Access
        T& operator [] (const uint idx)
        {
            if (idx >= this->size())
            {
                this->resize(std::max(this->size(), idx + 1), T{});
            }
            return this->at(idx);
        }
    };
    /// @}
}
