/// @file ls/core/Timer.h
/// @brief Timer class and time functions
#pragma once

#include "ls/common.h"

namespace ls
{
    /// @addtogroup LsCore
    /// @{

    /// @brief Auto-resetting Timer
    class Timer
    {
    public:
        /// @brief Empty ctor
        Timer() = default;
        /// @brief Ctor.
        /// @param interval
        ///   Timer period (in milliseconds)
        /// @param startAlarm
        ///   Will timer alarms immediately?
        /// @param isEnabled
        ///   Is timer enabled
        /// @param isAdditive
        ///   Will timer events accumulate?
        Timer(const DeltaTime interval, const bool startAlarm,
              const bool isEnabled = true,
              const bool isAdditive = false) noexcept;
        /// @brief Reset time (no events reset)
        void reset() noexcept;
        /// @brief Advance time by @a timeDelta
        void advance(const DeltaTime timeDelta) noexcept;
        /// @brief Are there new events?
        bool alarm() noexcept;
        /// @brief Enable timer
        void enable() noexcept;
        /// @brief Disable timer
        void disable() noexcept;

        /// @brief Get timer period
        DeltaTime period() const noexcept
        {
            return m_interval;
        }
        /// @brief Get current time
        DiscreteTime time() const noexcept
        {
            return m_time;
        }
        /// @brief Is timer enabled?
        bool isEnabeld() const noexcept
        {
            return m_enabledFlag;
        }
        /// @brief Floor @a time to period
        DiscreteTime floor(const DiscreteTime time) const noexcept
        {
            return time / m_interval * m_interval;
        }
        /// @brief Ceil @a time to period
        DiscreteTime ceil(const DiscreteTime time) const noexcept
        {
            return (time + m_interval - 1) / m_interval * m_interval;
        }
    protected:
        /// @brief Timer period
        DeltaTime m_interval = 1;
        /// @brief Is timer additive
        bool m_additiveFlag = false;
        /// @brief Is timer enabled
        bool m_enabledFlag = false;
        /// @brief Elapsed time
        DiscreteTime m_time = 0;
        /// @brief Number of new events
        uint m_numEvents = 0;
    };
    /// @brief Incremental timer.
    ///
    /// Timer may be locked.
    /// Locked timer will never decrease time even if it is ordered by @a set.
    /// So, it will be frozen until negative delta will be amortized.
    ///
    /// Trigger values reached during @a set call will @b not alarm.
    class IncrementalTimer
        : public Timer
    {
    public:
        /// @brief Empty ctor
        IncrementalTimer() = default;
        /// @brief Ctor.
        /// @param interval Timer period (in milliseconds)
        /// @param startValue Start timer value
        /// @param isEnabled Is timer enabled?
        IncrementalTimer(const DeltaTime interval, const DiscreteTime startValue, const bool isEnabled = true) noexcept;
        /// @brief Reset @a time
        void set(const DiscreteTime time) noexcept;
        /// @brief Advance time by non-negative @a timeDelta
        void advance(DeltaTime timeDelta) noexcept;
        /// @brief Check for new events, clear @b one and increase round time by period if there is new event.
        bool alarm() noexcept;
        /// @brief Check for new events, clear @b all and increase round time by periods sum
        bool alarmAll() noexcept;

        /// @brief Get timer period
        DeltaTime period() const noexcept
        {
            return m_interval;
        }
        /// @brief Get time rounded to lower alarm date
        DiscreteTime tick() const noexcept
        {
            return m_tick;
        }
        /// @brief Is timer enabled?
        bool isEnabeld() const noexcept
        {
            return m_enabledFlag;
        }
    protected:
        /// @brief Reset is not allowed
        using Timer::reset;
    protected:
        /// @brief Sleeping time
        DeltaTime m_sleepTime = 0;
        /// @brief Round elapsed time
        DiscreteTime m_tick = 0;
    };
    /// @}
}
