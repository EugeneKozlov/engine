/// @file ls/core/GlobalVariableManager.h
/// @brief Global Variable Manager
#pragma once

#include "ls/common.h"
#include "ls/core/GlobalVariable.h"

namespace ls
{
    /// @addtogroup LsCore
    /// @{

    /// @brief Global Variable Manager (singleton)
    class GlobalVariableManager
    {
    private:
        /// @brief Ctor
        GlobalVariableManager();
        /// @brief Dtor
        ~GlobalVariableManager();
    public:
        /// @brief Get instance
        static GlobalVariableManager& instance();
        /// @brief Load INI config
        void loadIni(const string& text);

        /// @brief Register variable
        void registerVariable(GlobalVariable& variable);
        /// @brief Load variable
        uint loadVariable(const string& fullName, const string& value);
        /// @brief Update variable
        uint updateVariable(const string& shortName, const string& value);
    private:
        map<string, vector<GlobalVariable*>> m_variableByFullName;
        map<string, vector<GlobalVariable*>> m_variableByShortName;
    };

    /// @}
}