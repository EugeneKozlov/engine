/// @file ls/core/PropertyArray.h
/// @brief Property Array
#pragma once

#include "ls/common.h"

namespace ls
{
    /// @addtogroup LsCore
    /// @{

    /// @brief Property Array
    ///
    /// Allow safely store enum properties
    template <class P, uint N>
    class PropertyArray
    {
    public:
        /// @brief Property Array size
        static const uint Size = N;
        /// @brief Property type
        using Property = P;
        /// @brief Container
        using Container = array<optional<Property>, Size>;
        /// @brief Iterator
        using Iterator = typename Container::const_iterator;
        /// @brief Ctor
        PropertyArray(initializer_list<Property> list)
        {
            for (Property const& prop : list)
            {
                uint key = (uint) prop.key;
                debug_assert(key < Size, "bad property key");
                debug_assert(m_array[key] == none, "duplicate property");
                m_array[key] = prop;
            }
            for (auto& prop : m_array)
            {
                debug_assert(!!prop, "uninitialzed property");
            }
        }
        /// @brief Get
        template <class Key>
        const Property& operator[] (const Key key) const
        {
            return *m_array[(uint) key];
        }
        /// @brief Begin
        Iterator begin() const
        {
            return m_array.begin();
        }
        /// @brief End
        Iterator end() const
        {
            return m_array.end();
        }
    private:
        Container m_array;
    };
    /// @}
}