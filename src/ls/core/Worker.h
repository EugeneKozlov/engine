/// @file ls/core/Worker.h
/// @brief Worker interface
#pragma once

#include "ls/common.h"

namespace ls
{
    /// @addtogroup LsCore
    /// @{
    
    /// @brief Worker Interface
    class Worker
    {
    public:
        /// @brief Ctor.
        /// @param numThreads
        ///   Number of used threads
        Worker(const uint numThreads = 1);
        /// @brief Dtor.
        ~Worker();
        /// @brief Schedule the task
        /// @param task
        ///   Task
        template <class Function>
        auto schedule(Function task)
        {
            using ReturnType = decltype(task());
            using Task = packaged_task<ReturnType()>;
            {
                lock_guard<mutex> lock(counterMutex);
                ++counter;
            }
            shared_ptr<Task> sharedTask = allocate_shared<Task>(FastAllocator<Task>(),
                                                                task);
            auto work = [sharedTask, this]()
            {
                (*sharedTask)();
                lock_guard<mutex> lock(counterMutex);
                --counter;
                if (counter <= 0)
                    condition.notify_all();
            };
            service.post(work);

            return sharedTask->get_future();
        }
        /// @brief Wait until completion
        void wait();
    private:
        boost::asio::io_service service; ///< ASIO service
        boost::asio::io_service::work worker; ///< Worker
        vector<thread> pool; ///< Thread pool
        condition_variable condition; ///< Wait
        sint counter = 0; ///< Counter
        mutex counterMutex; ///< Mutex
    };
    /// @}
}
