/// @file ls/core/StableVectorRef.h
/// @brief Stable Vector Reference
#pragma once

#include "ls/common.h"

namespace ls
{
    /// @addtogroup LsCore
    /// @{

    /// @brief Stable Vector Reference
	/// @tparam Container Container type
	template <class Container, class Element>
	struct StableVectorRef
	{
		/// @brief Ctor
		StableVectorRef() = default;
        /// @brief Ctor
        StableVectorRef(nullptr_t)
        {
        }
        /// @brief Ctor
		StableVectorRef(Container& pool, const uint index)
			: pool(&pool)
			, index(index)
		{
		}
        /// @brief Is invalid?
        bool operator ! () const
        {
            return !pool;
        }
		/// @brief Access
		Element* operator -> ()
		{
			return &pool->at(index);
		}
		/// @brief Access
		const Element* operator -> () const
		{
			return &pool->at(index);
		}
		/// @brief Get
		Element& operator * ()
		{
			return pool->at(index);
		}
		/// @brief Get
		const Element& operator * () const
		{
			return pool->at(index);
		}
	private:
		Container* pool = nullptr;
		uint index = 0;
	};
    /// @}
}
