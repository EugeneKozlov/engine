/// @file ls/core/Pool.h
/// @brief Pool container
#pragma once

#include "ls/common.h"

namespace ls
{
    /// @addtogroup LsCore
    /// @{
    
    /// @brief Object Pool
    template <typename T, typename UserAllocator = boost::default_user_allocator_new_delete>
    class Pool 
        : private boost::pool<UserAllocator>
    {
    public:
        typedef T element_type;
        typedef UserAllocator user_allocator;
        typedef typename boost::pool<UserAllocator>::size_type size_type;
        typedef typename boost::pool<UserAllocator>::difference_type difference_type;

    public:
        /// @brief Ctor
        explicit Pool(const size_type arg_next_size = 32, const size_type arg_max_size = 0)
            : boost::pool<UserAllocator>(sizeof(T), arg_next_size, arg_max_size)
        {
        }
        /// @brief Dtor
        ~Pool();
        /// @brief Is chunk from pool?
        bool isFrom(element_type* chunk) const
        {
            return store().is_from(chunk);
        }
        /// @brief Construct object
        element_type* construct()
        {
            element_type* ret = allocate();
            if (ret == 0)
                return ret;
            try 
            { 
                new (ret) element_type(); 
            }
            catch (...)
            { 
                deallocate(ret); 
                throw; 
            }
            return ret;
        }
        /// @brief Destroy object
        void destroy(element_type* chunk)
        {
            chunk->~T();
            deallocate(chunk);
        }

    protected:
        element_type* allocate()
        {
            return static_cast<element_type*>(store().malloc());
        }
        void deallocate(element_type* chunk)
        {
            store().free(chunk);
        }
        boost::pool<UserAllocator> & store()
        {
            return *this;
        }
        const boost::pool<UserAllocator> & store() const
        {
            return *this;
        }
        static void*& nextof(void* ptr)
        {
            return *(static_cast<void**>(ptr));
        }
    };

    template <typename T, typename UserAllocator>
    Pool<T, UserAllocator>::~Pool()
    {
#ifndef BOOST_POOL_VALGRIND
        // handle trivial case of invalid list.
        if (!this->list.valid())
            return;

        boost::details::PODptr<size_type> iter = this->list;
        boost::details::PODptr<size_type> next = iter;

        // Start 'freed_iter' at beginning of free list
        void * freed_iter = this->first;

        const size_type partition_size = this->alloc_size();

        do
        {
            // increment next
            next = next.next();

            // delete all contained objects that aren't freed.

            // Iterate 'i' through all chunks in the memory block.
            for (char * i = iter.begin(); i != iter.end(); i += partition_size)
            {
                // If this chunk is free,
                if (i == freed_iter)
                {
                    // Increment freed_iter to point to next in free list.
                    freed_iter = nextof(freed_iter);

                    // Continue searching chunks in the memory block.
                    continue;
                }

                // This chunk is not free (allocated), so call its destructor,
                static_cast<T *>(static_cast<void *>(i))->~T();
                // and continue searching chunks in the memory block.
            }

            // free storage.
            (UserAllocator::free)(iter.begin());

            // increment iter.
            iter = next;
        } while (iter.valid());

        // Make the block list empty so that the inherited destructor doesn't try to
        // free it again.
        this->list.invalidate();
#else
        // destruct all used elements:
        for (std::set<void*>::iterator pos = this->used_list.begin(); pos != this->used_list.end(); ++pos)
        {
            static_cast<T*>(*pos)->~T();
        }
        // base class will actually free the memory...
#endif
    }
    /// @}
}
