/// @file ls/core/AsyncStorageProvider.h
/// @brief Async Storage Provider
#pragma once

#include "ls/common.h"

namespace ls
{
    /// @addtogroup LsCore
    /// @{

    /// @brief Async Storage Provider
    class AsyncStorageProvider
    {
    public:
        /// @brief Allocate async storage
        template <class TStorage>
        void allocateAsyncStorage(const void* handle)
        {
            lock_guard<mutex> guard(m_storageMutex);
            m_storages[handle] = TStorage();
        }
        /// @brief Get async storage
        template <class TStorage>
        TStorage& getAsyncStorage(const void* handle)
        {
            lock_guard<mutex> guard(m_storageMutex);
            TStorage* storage = any_cast<TStorage>(&getOrFail(m_storages, handle));
            debug_assert(storage, "storage type mismatch");
            return *storage;
        }
        /// @brief Release async storage
        void releaseAsyncStorage(const void* handle)
        {
            lock_guard<mutex> guard(m_storageMutex);
            m_storages.erase(handle);
        }
    private:
        mutex m_storageMutex;
        hamap<const void*, any> m_storages;
    };
    /// @}
}
