#include "pch.h"
#include "ls/core/GlobalVariableManager.h"
#include "ls/core/INIReader.h"

using namespace ls;
GlobalVariableManager::GlobalVariableManager()
{

}
GlobalVariableManager::~GlobalVariableManager()
{

}
GlobalVariableManager& GlobalVariableManager::instance()
{
    static GlobalVariableManager inst;
    return inst;
}
void GlobalVariableManager::loadIni(const string& text)
{
    INIReader reader;
    reader.parse(text);
    for (const auto& iterSection : reader.sections())
    {
        const string& sectionName = iterSection.first;
        for (const auto& iterVariable : iterSection.second)
        {
            const string& variableName = iterVariable.first;
            const string& value = iterVariable.second;
            const string fullName = sectionName + ":" + variableName;
            loadVariable(fullName, value);
        }
    }
}
void GlobalVariableManager::registerVariable(GlobalVariable& variable)
{
    debug_assert(!variable.fullName().empty(), "Variable must have name");
    m_variableByFullName[variable.fullName()].push_back(&variable);
    if (!variable.shortName().empty())
    {
        m_variableByShortName[variable.fullName()].push_back(&variable);
    }
}
uint GlobalVariableManager::loadVariable(const string& fullName, const string& value)
{
    auto vars = findOrDefault(m_variableByFullName, fullName);
    if (!vars)
        return 0;

    for (GlobalVariable* var : *vars)
    {
        LS_THROW(!var->isInitialized(),
                 IoException, "Variable [", var->fullName(), "] is already initialized");
        var->load(value);
    }
    return vars->size();
}
uint GlobalVariableManager::updateVariable(const string& shortName, const string& value)
{
    auto vars = findOrDefault(m_variableByShortName, shortName);
    if (!vars)
        return 0;

    for (GlobalVariable* var : *vars)
    {
        if (!var->is(GlobalVariableFlag::Modifiable))
            continue;
        var->load(value);
    }
    return vars->size();
}