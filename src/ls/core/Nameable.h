/// @file ls/core/Nameable.h
/// @brief Nameable class template.
#pragma once

#include "ls/common.h"
#include "ls/core/Indexable.h"

namespace ls
{
    /// @addtogroup LsCore
    /// @{

    /// @brief Nameable class template.
    /// @brief Each class has debug name and debug @b index.
    /// @brief Each class has logger and log functions which can be disabled.
    /// @tparam Derived Derived nameable class
    template <class Derived>
    class Nameable
        : public Indexable<Derived>
    {
    public:
        /// @brief 'Invisible' index
        static const uint InvisibleIndex = uint(-1);
        /// @brief Ctor by @a name
        Nameable(const string& name = "Unnamed", const bool log = true)
            : Indexable<Derived>()
            , m_debugName(name)
            , m_logEnabled(log)
        {
        }
        /// @brief Ctor by @a name and @a index
        Nameable(const string& name,
                 const uint index,
                 const bool log = true)
            : Indexable<Derived>(index)
            , m_debugName(name)
            , m_logEnabled(log)
        {
        }
        /// @brief Get debug name
        const char* debugName() const noexcept
        {
            return m_debugName.c_str();
        }
        /// @brief Change debug name
        void debugRename(const string& name)
        {
            m_debugName = name;
        }

        /// @brief Log text as @a style
        template <class ... Args>
        void log(const LogMessageLevel style, const Args& ... args) const
        {
            if (!m_logEnabled)
                return;
            if (this->debugIndex() != InvisibleIndex)
                LogManager::instance().log(style, debugName(), " #",
                                           this->debugIndex(), ": ", args...);
            else
                LogManager::instance().log(style, debugName(), ": ", args...);
        }
        /// @brief Log as @a LogMessageLevel::Trace
        template <class ... Args>
        void logTrace(const Args& ... args) const
        {
            log(LogMessageLevel::Trace, args...);
        }
        /// @brief Log as @a LogMessageLevel::Debug
        template <class ... Args>
        void logDebug(const Args& ... args) const
        {
            log(LogMessageLevel::Debug, args...);
        }
        /// @brief Log as @a LogMessageLevel::Info
        template <class ... Args>
        void logInfo(const Args& ... args) const
        {
            log(LogMessageLevel::Info, args...);
        }
        /// @brief Log as @a LogMessageLevel::System
        template <class ... Args>
        void logSystem(const Args& ... args) const
        {
            log(LogMessageLevel::System, args...);
        }
        /// @brief Log as @a LogMessageLevel::Notice
        template <class ... Args>
        void logNotice(const Args& ... args) const
        {
            log(LogMessageLevel::Notice, args...);
        }
        /// @brief Log as @a LogMessageLevel::Warning
        template <class ... Args>
        void logWarning(const Args& ... args) const
        {
            log(LogMessageLevel::Warning, args...);
        }
        /// @brief Log as @a LogMessageLevel::Error
        template <class ... Args>
        void logError(const Args& ... args) const
        {
            log(LogMessageLevel::Error, args...);
        }
        /// @brief Log as @a LogMessageLevel::Message
        template <class ... Args>
        void logMessage(const Args& ... args) const
        {
            log(LogMessageLevel::Message, args...);
        }
    private:
        /// @brief Name
        string m_debugName;
        /// @brief Set to enable logging
        bool m_logEnabled;
    };
    /// @brief Unique-Named object prototype.
    class Named
    {
    public:
        /// @brief Ctor by @a name.
        Named(const string& name = "")
            : m_name(name)
        {
        }
        /// @brief Get name
        const string& name() const noexcept
        {
            return m_name;
        }
        /// @brief Change name
        void rename(const string& newName)
        {
            m_name = newName;
        }
    private:
        /// @brief Name
        string m_name;
    };
    /// @}
}
