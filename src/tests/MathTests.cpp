#include "pch.h"

#include <catch/catch.hpp>

#include "ls/common.h"
#include "ls/io/Buffer.h"
#include "ls/io/Codec.h"

using namespace ls;

TEST_CASE("Test Math.Common")
{
    REQUIRE(roundi<sint>(0.0) == 0);
    REQUIRE(roundi<sint>(1.5) == 2);
    REQUIRE(roundi<sint>(1.2) == 1);
    REQUIRE(roundi<sint>(-1.2) == -1);
    REQUIRE(roundi<sint>(-1.8) == -2);

    REQUIRE(fract(-1.25) == 0.75);
    REQUIRE(fract(1.5) == 0.5);

    REQUIRE(isFinite(1.5));
    REQUIRE(!isFinite(numeric_limits<float>::infinity()));
    REQUIRE(!isFinite(numeric_limits<float>::quiet_NaN()));

    REQUIRE(deg2rad(0.0) == 0);
    REQUIRE(almostEqual(deg2rad(180.0f), pi));
    REQUIRE(rad2deg(0.0) == 0);
    REQUIRE(almostEqual(rad2deg(pi), 180.0f));

    REQUIRE(lerp(0.0, 1.0, 0.75) == 0.75);
    REQUIRE(clamp(-1, 0, 2) == 0);
    REQUIRE(clamp(1, 0, 2) == 1);
    REQUIRE(clamp(4, 0, 2) == 2);
    REQUIRE(unlerp(0.0, 2.0, 1.0) == 0.5);

    REQUIRE(floor2p(15) == 8);
    REQUIRE(floor2p(32) == 32);
    REQUIRE(ceil2p(16) == 16);
    REQUIRE(ceil2p(17) == 32);

    REQUIRE(real2norm<ubyte>(0.0f) == 0);
    REQUIRE(real2norm<ubyte>(1.0f) == 255);
    REQUIRE(real2norm<sbyte>(0.0f) == 0);
    REQUIRE(real2norm<sbyte>(-1.0f) == -128);
    REQUIRE(real2norm<sbyte>(1.0f) == 127);
    REQUIRE(real2norm<ushort>(0.0f) == 0);
    REQUIRE(real2norm<ushort>(1.0f) == MaxUshort);
    REQUIRE(real2norm<sshort>(0.0f) == 0);
    REQUIRE(real2norm<sshort>(-1.0f) == MinShort);
    REQUIRE(real2norm<sshort>(1.0f) == MaxShort);
    REQUIRE(real2norm<uint>(0.0f) == 0);
    REQUIRE(real2norm<uint>(1.0f) == MaxUint);
    REQUIRE(real2norm<sint>(0.0f) == 0);
    REQUIRE(real2norm<sint>(-1.0f) == MinInt);
    REQUIRE(real2norm<sint>(1.0f) == MaxInt);

    REQUIRE((norm2real<float, ubyte>(0) == 0.0f));
    REQUIRE((norm2real<float, ubyte>(255) == 1.0f));
    REQUIRE((norm2real<float, sbyte>(0) == 0.0f));
    REQUIRE((norm2real<float, sbyte>(-128) == -1.0f));
    REQUIRE((norm2real<float, sbyte>(127) == 1.0f));
    REQUIRE((norm2real<float, ushort>(0) == 0.0f));
    REQUIRE((norm2real<float, ushort>(MaxUshort) == 1.0f));
    REQUIRE((norm2real<float, sshort>(0) == 0.0f));
    REQUIRE((norm2real<float, sshort>(MinShort) == -1.0f));
    REQUIRE((norm2real<float, sshort>(MaxShort) == 1.0f));
    REQUIRE((norm2real<float, uint>(0) == 0.0f));
    REQUIRE((norm2real<float, uint>(MaxUint) == 1.0f));
    REQUIRE((norm2real<float, sint>(0) == 0.0f));
    REQUIRE((norm2real<float, sint>(MinInt) == -1.0f));
    REQUIRE((norm2real<float, sint>(MaxInt) == 1.0f));
}

TEST_CASE("Test Math.Vector")
{
    REQUIRE(int2(1, 2) + int2(2, 3) == int2(3, 5));
    REQUIRE(int2(1, 2) - int2(2, 3) == int2(-1, -1));
    REQUIRE(int2(1, 2) * int2(2, 3) == int2(2, 6));
    REQUIRE(int2(6, 8) / int2(2, 3) == int2(3, 2));
    REQUIRE(int2(6, 8) % int2(2, 3) == int2(0, 2));
    REQUIRE(float2(6.0, 8.0) % float2(2.0, 3.0) == float2(0.0, 2.0));
    REQUIRE(int2(1, 2) + 2 == int2(3, 4));
    REQUIRE(int2(1, 2) - 2 == int2(-1, 0));
    REQUIRE(int2(1, 2) * 2 == int2(2, 4));
    REQUIRE(int2(6, 8) / 3 == int2(2, 2));
    REQUIRE(int2(6, 8) % 3 == int2(0, 2));
    REQUIRE(float2(6.0, 8.0) % 3.0 == float2(0.0, 2.0));

    REQUIRE((int2{ 1, 2, 3, 4 } == int2(1, 2)));
    REQUIRE((int3{ 1, 2, 3, 4 } == int3(1, 2, 3)));
    REQUIRE((int4{ 1, 2, 3, 4 } == int4(1, 2, 3, 4)));

    REQUIRE(int2() == int2(0, 0));
    REQUIRE(int2(1) == int2(1, 1));
    REQUIRE(int3() == int3(0, 0, 0));
    REQUIRE(int3(1) == int3(1, 1, 1));
    REQUIRE(int3(1, int2(2, 3)) == int3(1, 2, 3));
    REQUIRE(int3(int2(1, 2), 3) == int3(1, 2, 3));
    REQUIRE(int4() == int4(0, 0, 0, 0));
    REQUIRE(int4(1) == int4(1, 1, 1, 1));
    REQUIRE(int4(int2(1, 2), 3, 4) == int4(1, 2, 3, 4));
    REQUIRE(int4(int3(1, 2, 3), 4) == int4(1, 2, 3, 4));
    REQUIRE(int4(1, int3(2, 3, 4)) == int4(1, 2, 3, 4));
    REQUIRE(int4(1, 2, int2(3, 4)) == int4(1, 2, 3, 4));
    REQUIRE(int4(1, int2(2, 3), 4) == int4(1, 2, 3, 4));

    REQUIRE(floor(0.8f) == 0.0f);
    REQUIRE(floor(float2(0.8f, -0.4f)) == float2(0, -1));
    REQUIRE(ceil(0.8f) == 1.0f);
    REQUIRE(ceil(float2(0.8f, -0.4f)) == float2(1, 0));
    REQUIRE(abs(-0.8f) == 0.8f);
    REQUIRE(abs(float2(0.8f, -0.4f)) == float2(0.8f, 0.4f));

    REQUIRE(max(1, 2) == 2);
    REQUIRE(min(1, 2) == 1);
    REQUIRE(max(int2(1, 2), int2(2, 1)) == int2(2, 2));
    REQUIRE(min(int2(1, 2), int2(2, 1)) == int2(1, 1));

    REQUIRE(int2(int4::min()) == int2::min());
    REQUIRE(int2(int4::max()) == int2::max());

    REQUIRE(clamp(int2(4), int2(0), int2(2)) == int2(2));
    REQUIRE(unlerp(float2(0.0), float2(2.0), float2(1.0)) == float2(0.5));

    REQUIRE(inside(int2(0, 0), 0, 2));
    REQUIRE(inside(int2(0, 0), int2(0, 0), int2(2, 2)));
    REQUIRE(inside(int2(1, 1), int2(0, 0), int2(2, 2)));
    REQUIRE(!inside(int2(1, 2), int2(0, 0), int2(2, 2)));
    REQUIRE(!inside(int2(2, 2), int2(0, 0), int2(2, 2)));
    REQUIRE(insideInc(int2(2, 2), int2(0, 0), int2(2, 2)));
    REQUIRE(insideInc(int2(2, 2), 0, 2));

    REQUIRE(upcastLocal(int2(5, 5), 3) == int2(2, 2));
    REQUIRE(upcast(int2(5, 5), 3) == int2(1, 1));
    REQUIRE(upcast(float2(5.0f, 5.0f), 3.0f) == float2(1.0f, 1.0f));

    REQUIRE(dot(int2(1, 0), int2(-1, 0)) == -1);
    REQUIRE(dot(int2(1, 0), int2(0, -1)) == 0);
    REQUIRE(cross(int3(1, 0, 0), int3(0, 1, 0)) == int3(0, 0, 1));
}

TEST_CASE("Test Math.Matrix")
{
    int4x4 mi = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 };
    int4x4 m1 = {};
    int4x4 m2 = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 2, 3, 1 };
    int4x4 mt = { 1, 0, 0, 1, 0, 1, 0, 2, 0, 0, 1, 3, 0, 0, 0, 1 };
    int4x4 m3 = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, -1, -2, -3, 1 };
    int4x4 m4 = { 1, 0, 0, 0, 0, 2, 0, 0, 0, 0, 3, 0, 0, 0, 0, 1 };
    int4x4 m5 = { 1, 0, 0, 0, 0, 2, 0, 0, 0, 0, 3, 0, 1, 4, 9, 1 };
    int4x4 m6 = { 1, 0, 0, 0, 0, 2, 0, 0, 0, 0, 3, 0, 1, 2, 3, 1 };
    REQUIRE(mi == m1);
    REQUIRE(mt == transpose(m2));
    REQUIRE(mi == m3 * m2);
    REQUIRE(mi == m2 * m3);
    REQUIRE(m2 * m2 == m2 / m3);
    REQUIRE(m2 == inverse(m3));
    REQUIRE(mi == m2 / m2);
    REQUIRE(m6 == m4 * m2);
    REQUIRE(m5 == m2 * m4);
    REQUIRE((int4{ 2, 4, 6, 1 } == int4{ 1, 2, 3, 1 } * m2));
    REQUIRE((int4{ 0, 0, 0, 1 } == int4{ 1, 2, 3, 1 } * m3));
}

TEST_CASE("Test Math.Quaternion")
{
    quat q;
    quat qi = q.inverse();
    REQUIRE(q * qi == q);
    REQUIRE(q.asMatrix() == float3x3());
    REQUIRE(qi.asMatrix() == float3x3());
}

namespace
{
    template <Encoding E, class T>
    T pull(T obj)
    {
        Buffer buffer;
        Encoded<T, E> enc;
        *enc = obj;
        encode(buffer, enc);
        decode(buffer, enc);
        return *enc;
    }
}

TEST_CASE("Test Encoding")
{
    REQUIRE(almostEqual(pull<Encoding::RotationPaq>(quat(float3(-1.0f, 0.0f, 0.0f), pi / 4)), quat(float3(-1.0f, 0.0f, 0.0f), pi / 4), 0.01f));
    REQUIRE(almostEqual(pull<Encoding::RotationAxisAngle>(quat(float3(-1.0f, 0.0f, 0.0f), pi / 4)), quat(float3(-1.0f, 0.0f, 0.0f), pi / 4), 0.001f));
    REQUIRE(pull<Encoding::VectorFixed3>(double3(-9999.0f, 22222.0f, 44444.5f)) == double3(-9999.0f, 22222.0f, 44444.5f));
    REQUIRE(almostEqual(pull<Encoding::DirectionSphereLength>(float3(-3.0f, 0.0f, 0.0f)), float3(-3.0f, 0.0f, 0.0f), 0.001f));
    REQUIRE(almostEqual(pull<Encoding::DirectionSphere>(float3(-1.0f, 0.0f, 0.0f)), float3(-1.0f, 0.0f, 0.0f), 0.001f));
    REQUIRE(pull<Encoding::VectorFloat>(float3(-1.0f, 2.0f, 0.5f)) == float3(-1.0f, 2.0f, 0.5f));
    REQUIRE(pull<Encoding::VectorInt>(int2(-128009, 127009)) == int2(-128009, 127009));
    REQUIRE(pull<Encoding::VectorShort>(int2(-12800, 12700)) == int2(-12800, 12700));
    REQUIRE(pull<Encoding::VectorByte>(int2(-128, 127)) == int2(-128, 127));
}
