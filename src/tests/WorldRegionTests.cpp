#include "pch.h"

#include <catch/catch.hpp>

#include "ls/asset/WorldRegion.h"
#include "ls/core/Worker.h"
#include "ls/io/FileSystemInterface.h"

using namespace ls;

namespace
{
    WorldRegion::Config defaultTestConfig()
    {
        WorldRegion::Config config;
        config.chunkWidth = 32;
        config.regionColorMap = 128;
        config.regionHeightMap = 32;
        config.regionNormalMap = 32;
        config.regionWidth = 512;
        config.compute();
        return config;
    }
}

TEST_CASE("Test WorldRegion::Config computation", "")
{
    WorldRegion::Config config = defaultTestConfig();

    REQUIRE(config.regionInChunks == 16);
}

TEST_CASE("Test WorldRegion index conversion", "")
{
    Worker worker;
    WorldRegion::Config config = defaultTestConfig();
    FileSystem fileSystem = FileSystemInterface::open("./tests");

    // Several regions with different locations
    const int2 globalChunk1 = int2(4, 8) * 16 + int2(1, 3);
    WorldRegion region1(int2(4, 8), defaultTestConfig(), WorldRegion::Callbacks(), 
                        *fileSystem, worker, true);
    REQUIRE(region1.local2global(int2(1, 3)) == globalChunk1);
    REQUIRE(region1.global2local(globalChunk1) == int2(1, 3));
    REQUIRE(region1.beginChunk() == int2(4, 8) * 16);
    REQUIRE(region1.endChunk() == int2(5, 9) * 16);
    REQUIRE(region1.makeFileName() == "00040008");

    const int2 globalChunk2 = int2(-1, 0) * 16 + int2(9, 9);
    WorldRegion region2(int2(-1, 0), defaultTestConfig(), WorldRegion::Callbacks(), 
                        *fileSystem, worker, true);
    REQUIRE(region2.local2global(int2(9, 9)) == globalChunk2);
    REQUIRE(region2.global2local(globalChunk2) == int2(9, 9));
    REQUIRE(region2.beginChunk() == int2(-1, 0) * 16);
    REQUIRE(region2.endChunk() == int2(0, 1) * 16);
    REQUIRE(region2.makeFileName() == "FFFF0000");
}

TEST_CASE("Test WorldRegion save/load", "")
{
    static const int2 testRegionIndex = int2(4, 8);
    static const int2 testChunkIndex = testRegionIndex * 16 + 1;
    static const string fileName = "00040008";
    static const string fileNameDat = fileName + ".dat";
    static const string fileNameBin = fileName + ".bin";

    Worker worker;
    FileSystem fileSystem = FileSystemInterface::open("./tests");
    fileSystem->deleteItem(fileNameDat);
    fileSystem->deleteItem(fileNameBin);

    {
        // Empty WorldRegion with some valid config
        WorldRegion region(testRegionIndex, defaultTestConfig(), WorldRegion::Callbacks(),
                           *fileSystem, worker, true);

        REQUIRE(!region.isRegionDataRequested());
        REQUIRE(!region.isRegionDataReady());
        REQUIRE(!region.isChunksDataRequested());
        REQUIRE(!region.isChunksDataReady());

        // Test region data request
        region.loadRegion();

        REQUIRE(region.isRegionDataRequested());
        REQUIRE(!region.isRegionDataReady());
        REQUIRE(!region.isChunksDataRequested());
        REQUIRE(!region.isChunksDataReady());

        // Test region data flush
        region.wait();
        region.flush();

        REQUIRE(region.isRegionDataRequested());
        REQUIRE(region.isRegionDataReady());
        REQUIRE(!region.isChunksDataRequested());
        REQUIRE(!region.isChunksDataReady());

        // Test chunks data request
        region.loadChunks();

        REQUIRE(region.isRegionDataRequested());
        REQUIRE(region.isRegionDataReady());
        REQUIRE(region.isChunksDataRequested());
        REQUIRE(!region.isChunksDataReady());

        // Test chunks data flush
        region.wait();
        region.flush();

        REQUIRE(region.isRegionDataRequested());
        REQUIRE(region.isRegionDataReady());
        REQUIRE(region.isChunksDataRequested());
        REQUIRE(region.isChunksDataReady());

        // Test chunk acquire and initial data
        REQUIRE(region.findChunkByIndex(int2(0, 0)) == nullptr);
        REQUIRE(region.findChunkByIndex(int2(999, 999)) == nullptr);

        WorldRegion::Chunk* chunk = region.findChunkByIndex(testChunkIndex);
        REQUIRE(chunk != nullptr);
        REQUIRE(chunk->height(float2(0.5f, 0.5f)) == 0.0f);

        // Write some data to region
        WorldRegion::RegionData& regionData = region.regionData();
        regionData.heightMap(0, 0) = 1.0f;
        regionData.heightMap(1, 0) = 2.0f;
        regionData.heightMap(0, 1) = 3.0f;
        regionData.heightMap(1, 1) = 4.0f;

        // Write some data to chunk
        chunk->materials[0] = int2(0, 1);
        chunk->materials[1] = int2(2, 3);
        chunk->numLayers = 2;
        chunk->bounds = double2(42.0, 43.0);
        chunk->heightMap(0, 0) = 1.0f;
        chunk->heightMap(0, 1) = 1.0f;
        chunk->heightMap(1, 0) = 2.0f;
        chunk->heightMap(1, 1) = 2.0f;
        chunk->normalMap(1, 1).material = 66;
        chunk->normalMap(1, 1).vec.x = 1;
        chunk->normalMap(1, 1).vec.y = 2;
        chunk->normalMap(1, 1).vec.z = -3;
        chunk->colorMap(1, 1) = 5;
        chunk->blendMap[1](1, 1) = 6;

        // Test chunk height interpolation
        REQUIRE(almostEqual(chunk->height(float2(0.5f, 0.5f)), 1.5f));
        const double2 testPosition = static_cast<double2>(testChunkIndex) * 32 + 0.5;
        REQUIRE(almostEqual(region.getHeight(testPosition), 1.5f));

        // Save shall not change state
        region.save();

        REQUIRE(region.isRegionDataRequested());
        REQUIRE(region.isRegionDataReady());
        REQUIRE(region.isChunksDataRequested());
        REQUIRE(region.isChunksDataReady());

        // Unload chunks
        region.unloadChunks();

        REQUIRE(region.isRegionDataRequested());
        REQUIRE(region.isRegionDataReady());
        REQUIRE(!region.isChunksDataRequested());
        REQUIRE(!region.isChunksDataReady());

        // Unload region
        region.unloadRegion();

        REQUIRE(!region.isRegionDataRequested());
        REQUIRE(!region.isRegionDataReady());
        REQUIRE(!region.isChunksDataRequested());
        REQUIRE(!region.isChunksDataReady());
    }

    // Files must exist
    REQUIRE(fileSystem->isRegularFile(fileNameDat));
    REQUIRE(fileSystem->isRegularFile(fileNameBin));

    // Test loading from file
    {
        WorldRegion region(testRegionIndex, defaultTestConfig(), WorldRegion::Callbacks(), 
                           *fileSystem, worker, false);

        REQUIRE(!region.isRegionDataRequested());
        REQUIRE(!region.isRegionDataReady());
        REQUIRE(!region.isChunksDataRequested());
        REQUIRE(!region.isChunksDataReady());

        // Test region data request
        region.loadRegion();

        REQUIRE(region.isRegionDataRequested());
        REQUIRE(!region.isRegionDataReady());
        REQUIRE(!region.isChunksDataRequested());
        REQUIRE(!region.isChunksDataReady());

        // Test region data flush
        region.wait();
        region.flush();

        REQUIRE(region.isRegionDataRequested());
        REQUIRE(region.isRegionDataReady());
        REQUIRE(!region.isChunksDataRequested());
        REQUIRE(!region.isChunksDataReady());

        // Test chunks data request
        region.loadChunks();

        REQUIRE(region.isRegionDataRequested());
        REQUIRE(region.isRegionDataReady());
        REQUIRE(region.isChunksDataRequested());
        REQUIRE(!region.isChunksDataReady());

        // Test chunks data flush
        region.wait();
        region.flush();

        REQUIRE(region.isRegionDataRequested());
        REQUIRE(region.isRegionDataReady());
        REQUIRE(region.isChunksDataRequested());
        REQUIRE(region.isChunksDataReady());

        // Test saved to region data 
        WorldRegion::RegionData& regionData = region.regionData();
        REQUIRE(regionData.heightMap(0, 0) == 1.0f);
        REQUIRE(regionData.heightMap(1, 0) == 2.0f);
        REQUIRE(regionData.heightMap(0, 1) == 3.0f);
        REQUIRE(regionData.heightMap(1, 1) == 4.0f);

        // Test saved to chunk data 
        WorldRegion::Chunk* chunk = region.findChunkByIndex(testChunkIndex);
        REQUIRE(chunk != nullptr);
        REQUIRE(chunk->materials[0] == int2(0, 1));
        REQUIRE(chunk->materials[1] == int2(2, 3));
        REQUIRE(chunk->numLayers == 2);
        REQUIRE(chunk->bounds == double2(42.0, 43.0));
        REQUIRE(chunk->heightMap(0, 0) == 1.0f);
        REQUIRE(chunk->heightMap(0, 1) == 1.0f);
        REQUIRE(chunk->heightMap(1, 0) == 2.0f);
        REQUIRE(chunk->heightMap(1, 1) == 2.0f);
        REQUIRE(chunk->normalMap(1, 1).material == 66);
        REQUIRE(chunk->normalMap(1, 1).vec.x == 1);
        REQUIRE(chunk->normalMap(1, 1).vec.y == 2);
        REQUIRE(chunk->normalMap(1, 1).vec.z == -3);
        REQUIRE(chunk->colorMap(1, 1) == WorldRegion::TerrainData::Color(5));
        REQUIRE(chunk->blendMap[1](1, 1) == WorldRegion::TerrainData::Blend(6));
    }

    // Test discarding
    {
        // Load all
        WorldRegion region(testRegionIndex, defaultTestConfig(), WorldRegion::Callbacks(),
                           *fileSystem, worker, true);

        region.loadRegion();
        region.loadChunks();
        region.wait();
        region.flush();

        REQUIRE(region.isRegionDataRequested());
        REQUIRE(region.isRegionDataReady());
        REQUIRE(region.isChunksDataRequested());
        REQUIRE(region.isChunksDataReady());

        // Test saved region
        WorldRegion::RegionData& regionData = region.regionData();

        REQUIRE(regionData.heightMap(0, 0) == 0.0f);
        REQUIRE(regionData.heightMap(1, 0) == 0.0f);
        REQUIRE(regionData.heightMap(0, 1) == 0.0f);
        REQUIRE(regionData.heightMap(1, 1) == 0.0f);

        // Test saved chunk
        WorldRegion::Chunk* chunk = region.findChunkByIndex(testChunkIndex);
        REQUIRE(chunk != nullptr);
        REQUIRE(chunk->materials[0] == int2(0, 0));
        REQUIRE(chunk->materials[1] == int2(0, 0));
        REQUIRE(chunk->numLayers == 1);
        REQUIRE(chunk->bounds == double2(0.0, 0.0));
        REQUIRE(chunk->heightMap(0, 0) == 0.0f);
        REQUIRE(chunk->heightMap(0, 1) == 0.0f);
        REQUIRE(chunk->heightMap(1, 0) == 0.0f);
        REQUIRE(chunk->heightMap(1, 1) == 0.0f);
        REQUIRE(chunk->normalMap(1, 1).material == 0);
        REQUIRE(chunk->normalMap(1, 1).vec.x == 0);
        REQUIRE(chunk->normalMap(1, 1).vec.y == 0x7f);
        REQUIRE(chunk->normalMap(1, 1).vec.z == 0);
        REQUIRE(chunk->colorMap(1, 1) == WorldRegion::TerrainData::Color(0x7f));
        REQUIRE(chunk->blendMap[1](1, 1) == WorldRegion::TerrainData::Blend(0, 0xff));
    }

    // Remove test files
    fileSystem->deleteItem(fileNameDat);
    fileSystem->deleteItem(fileNameBin);
}
